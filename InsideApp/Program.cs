﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace InsideApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Outside_NganhHangModel();
                Outside_NhanHang();
                Log("A", "Thành công");
            }
            catch (Exception ex)
            {
                Log("I", ex.ToString());       
            }

        }
        private static void Outside_NganhHangModel()
        {
                using (var client = new HttpClient())
                {
                    DataTable Model = new DataTable();
                    Model.Columns.Add("IDModel", typeof(string));
                    Model.Columns.Add("Model", typeof(string));
                    Model.Columns.Add("IDNganhHang", typeof(string));
                    Model.Columns.Add("IDNhanHang", typeof(string));
                    Model.Columns.Add("TrangThaiSanPham", typeof(string));


                    string APINganhHang = ConfigurationManager.AppSettings["OutSide_APINganhHang"] + "page=1&size=1000";
                    client.BaseAddress = new Uri(APINganhHang);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        JObject ob = JObject.Parse(result);
                        string data = ob["data"].ToString();
                        var NganhHangAll = JsonConvert.DeserializeObject<dynamic>(data);
                        DataTable dtdata = new DataTable();
                        dtdata.Columns.Add("IDNganhHang", typeof(string));
                        dtdata.Columns.Add("NganhHang", typeof(string));
                        foreach (dynamic item in NganhHangAll)
                        {
                            if (!string.IsNullOrEmpty(item.uniqueId.ToString()))
                            {
                                DataRow dr = dtdata.NewRow();
                                dr["IDNganhHang"] = item.uniqueId;
                                dr["NganhHang"] = item.name;
                                dtdata.Rows.Add(dr);
                                //

                                //var url = "http://ci-pim-product-v2-node.ict.frt.local/api/v1/inside/products/search?page=" + 1 + "&size=10000&categoryId=" + IDNganhHang + "&brandId=" + IDNhanHang + "&modelName=" + Model + "&isActive=" + TrangThaiSanPham + "";

                                var clientModelDanghoatdong = new HttpClient();
                                string APIModelDanghoatdong = ConfigurationManager.AppSettings["OutSide_APIModel"] + "industryCode=" + item.uniqueId + "";
                                clientModelDanghoatdong.BaseAddress = new Uri(APIModelDanghoatdong);
                                clientModelDanghoatdong.DefaultRequestHeaders.Accept.Clear();
                                clientModelDanghoatdong.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                var responseModelDanghoatdong = clientModelDanghoatdong.GetAsync("").Result;
                                if (responseModelDanghoatdong.IsSuccessStatusCode)
                                {
                                    var ModelDanghoatdongString = responseModelDanghoatdong.Content.ReadAsStringAsync().Result;
                                    //JObject obModelDanghoatdong = JObject.Parse(ModelDanghoatdongString);
                                    //string metadataDanghoatdong = obModelDanghoatdong["metadata"].ToString();
                                    
                                    var ModelDanghoatdong = JsonConvert.DeserializeObject<dynamic>(ModelDanghoatdongString);
                                    //var ModelmetadataDanghoatdongDanghoatdong = JsonConvert.DeserializeObject<dynamic>(metadataDanghoatdong);

                                    foreach (dynamic itembrand in ModelDanghoatdong)
                                    {
                                            var ModelList = itembrand.models;
                                            foreach (dynamic itemModel in ModelList)
                                            {           
                                                DataRow drModel = Model.NewRow();
                                                drModel["IDNganhHang"] = item.uniqueId;
                                                drModel["IDNhanHang"] = itembrand.brandCode;
                                                drModel["IDModel"] = itemModel.modelCode;
                                                drModel["Model"] = itemModel.modelName;
                                                drModel["TrangThaiSanPham"] = bool.Parse(itemModel.isActive.ToString()) ? "Đang kinh doanh" : "Ngưng kinh doanh";
                                                Model.Rows.Add(drModel);
                                                
                                            }
                                        

                                    }

                                }
                            }
                        }
                        using (var connection = new SqlConnection(ConfigurationManager.AppSettings["db"]))
                        {
                            var queryParameters = new DynamicParameters();
                            queryParameters.Add("@Table", dtdata.AsTableValuedParameter());
                            queryParameters.Add("@Type", 0);
                            connection.Open();
                            var query = connection.Query<dynamic>("OutSide_NganhHang_Insert"
                                , queryParameters, commandTimeout: 1500
                                , commandType: CommandType.StoredProcedure);

                            var queryParametersModel = new DynamicParameters();
                            queryParametersModel.Add("@Table", Model.AsTableValuedParameter());
                            queryParametersModel.Add("@Type", 0);
                            var queryModel = connection.Query<dynamic>("OutSide_Model_Insert"
                                , queryParametersModel, commandTimeout: 1500
                                , commandType: CommandType.StoredProcedure);
                        }
                    }
                }
        }
        private static void Outside_NhanHang()
        {
                using (var client = new HttpClient())
                {
                    string InserCurr = ConfigurationManager.AppSettings["OutSide_APINhanHang"] + "page=1&size=10000";
                    client.BaseAddress = new Uri(InserCurr);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        JObject ob = JObject.Parse(result);

                        string data = ob["data"].ToString();
                        var NganhHangAll = JsonConvert.DeserializeObject<dynamic>(data);
                        DataTable dtdata = new DataTable();

                        dtdata.Columns.Add("IDNhanHang", typeof(string));
                        dtdata.Columns.Add("NhanHang", typeof(string));
                        foreach (dynamic item in NganhHangAll)
                        {
                            if (!string.IsNullOrEmpty(item.uniqueId.ToString()))
                            {
                                DataRow dr = dtdata.NewRow();
                                dr["IDNhanHang"] = item.uniqueId;
                                dr["NhanHang"] = item.name;
                                dtdata.Rows.Add(dr);
                            }
                        }
                        using (var connection = new SqlConnection(ConfigurationManager.AppSettings["db"]))
                        {
                            var queryParameters = new DynamicParameters();
                            queryParameters.Add("@Table", dtdata.AsTableValuedParameter());
                            queryParameters.Add("@Type", 0);
                            connection.Open();
                            var query = connection.Query<dynamic>("OutSide_NhanHang_Insert"
                                , queryParameters, commandTimeout: 1500
                                , commandType: CommandType.StoredProcedure);
                        }
                    }
                }
        }
        private static void Log(string status,string mess)
        {
                using (var connection = new SqlConnection(ConfigurationManager.AppSettings["db"]))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Status", status);
                if (mess.Length>= 900)
                {
                    queryParameters.Add("@Message", mess.Substring(0, 900));

                }
                if (mess.Length < 900)
                {
                    queryParameters.Add("@Message", mess);

                }
                connection.Open();
                    var query = connection.Query<dynamic>("INSERT dbo.Outside_HCHQCallAPI_Log(NgayChay,Status,Message)VALUES(GETDATE(),@Status,@Message)"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                }
        }
    }
}

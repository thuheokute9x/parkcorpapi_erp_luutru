﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Models.WIB.WIP;
using HR.API.Repository;
using HR.API.Repository.ERP.ExRateDaily;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.API.Repository.ERP;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Globalization;
using System.Data.SqlClient;

namespace Library
{
    public class PermissionController
    {
        public async Task<IList<object>> GetCurr(string database)
        {
            try
            {
                UnitOfWork<PARKERPEntities> unitOfWork = new UnitOfWork<PARKERPEntities>();
                GenericRepository<TTExRateDaily>  repository = new GenericRepository<TTExRateDaily>(unitOfWork);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://api.apilayer.com/exchangerates_data/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("apikey", "BOCmN5u0J5K6ne8qX8OGF8b3kmzHWUqj");//           fwueWt0CPp8dKZS4Xqg4hXeSX7LrmJv3
                    string today = DateTime.Now.ToString("yyyy-MM-dd");
                    string today_10 = DateTime.Now.AddDays(-10).ToString("yyyy-MM-dd");

                    var response = client.GetAsync("timeseries?start_date=" + today_10 + "&end_date=" + today + "&base=USD").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        dynamic jsonString = response.Content.ReadAsStringAsync();
                        List<TTExRateDaily> ListInsert = new List<TTExRateDaily>();
                        ExRateDailyModel data = JsonConvert.DeserializeObject<ExRateDailyModel>(jsonString.Result);
                        foreach (var item in data.rates)
                        {
                            foreach (var row in item.Value)
                            {
                                TTExRateDaily ob = new TTExRateDaily();
                                ob.BDate = DateTime.ParseExact(item.Name, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                ob.BCurrCD = row.Name;
                                ob.TCurrCD = "USD";
                                ob.ExRate = (Decimal)1 / (Decimal)row.Value;
                                ListInsert.Add(ob);
                            }
                        }
                        using (var connection = new SqlConnection(database))
                        {
                            var date = (from c in ListInsert
                                        group c by c.BDate into grp
                                        select grp.Key).ToList();

                            foreach (var item in date)
                            {
                                var queryParameters = new DynamicParameters();
                                connection.Open();
                                await connection.QueryAsync<EWIPModel>("DELETE dbo.TTExRateDaily WHERE BDate = '" + item.ToString("yyyy-MM-dd") + "'"
                                    , queryParameters
                                    , commandTimeout: 1500
                                    , commandType: CommandType.Text);
                                connection.Close();
                            }
                        }
                         await repository.BulkInsert(ListInsert);
                        //return ListInsert.Select(x=>new { BDate = x.BDate, BCurrCD = x.BCurrCD }).ToList();
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async void GetCurr_WithParam(string fromdateyyyy_MM_dd, string todateyyyy_MM_dd, string database)
        {
            UnitOfWork<PARKERPEntities> unitOfWork = new UnitOfWork<PARKERPEntities>();
            GenericRepository<TTExRateDaily> repository = new GenericRepository<TTExRateDaily>(unitOfWork);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://api.apilayer.com/exchangerates_data/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("apikey", "BOCmN5u0J5K6ne8qX8OGF8b3kmzHWUqj"); //   fwueWt0CPp8dKZS4Xqg4hXeSX7LrmJv3
                //string tomorow = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");

                var response = client.GetAsync("timeseries?start_date=" + fromdateyyyy_MM_dd + "&end_date=" + todateyyyy_MM_dd + "&base=USD").Result;
                if (response.IsSuccessStatusCode)
                {
                    dynamic jsonString = response.Content.ReadAsStringAsync();
                    List<TTExRateDaily> ListInsert = new List<TTExRateDaily>();
                    ExRateDailyModel data = JsonConvert.DeserializeObject<ExRateDailyModel>(jsonString.Result);
                    foreach (var item in data.rates)
                    {
                        foreach (var row in item.Value)
                        {
                            TTExRateDaily ob = new TTExRateDaily();
                            ob.BDate = DateTime.ParseExact(item.Name, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                            ob.BCurrCD = row.Name;
                            ob.TCurrCD = "USD";
                            ob.ExRate = (Decimal)1 / (Decimal)row.Value;
                            ListInsert.Add(ob);
                        }
                    }

                    using (var connection = new SqlConnection(database))
                    {
                        var date = (from c in ListInsert
                                    group c by c.BDate into grp
                                    select grp.Key).ToList();
                        foreach (var item in date)
                        {
                            var queryParameters = new DynamicParameters();
                            connection.Open();
                            await connection.QueryAsync<EWIPModel>("DELETE dbo.TTExRateDaily WHERE BDate = '" + item.ToString("yyyy-MM-dd") + "'"
                                , queryParameters
                                , commandTimeout: 1500
                                , commandType: CommandType.Text);
                            connection.Close();
                        }
                    }
                    await repository.BulkInsert(ListInsert);
                }
            }
        }
    }
}

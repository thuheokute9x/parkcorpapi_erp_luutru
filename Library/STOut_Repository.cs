﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class STOut_Repository
    {
        public async Task<bool> ERP_STOut_List_AdjustStock(string database,dynamic item,string Userlogin)
        {
            try
            {
                using (var connection = new SqlConnection(database))
                {
                    var queryParametersCheckSTBasicStock = new DynamicParameters();
                    queryParametersCheckSTBasicStock.Add("@CloseYear", int.Parse(item.Year.Value));
                    queryParametersCheckSTBasicStock.Add("@CloseMonth", int.Parse(item.Month.Value));
                    connection.Open();
                    STBasicStock dataCheckSTBasicStock = await connection.QueryFirstOrDefaultAsync<STBasicStock>("SELECT TOP 1 * FROM dbo.STBasicStock (NOLOCK) WHERE CloseYear=@CloseYear AND CloseMonth=@CloseMonth"
                        , queryParametersCheckSTBasicStock, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    if (dataCheckSTBasicStock == null)
                    {
                        throw new NotImplementedException("Please Transfer Data Closing Year " + item.Year.Value + " and Closing Month " + item.Month.Value + "");
                    }

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Outsourcing", "All");
                    queryParameters.Add("@Month", int.Parse(item.Month.Value));
                    queryParameters.Add("@Year", int.Parse(item.Year.Value));
                    queryParameters.Add("@CreatedBy", int.Parse(Userlogin));
                    var data = await connection.QueryAsync<STIn_List_Custom>(@"BEGIN TRY

        BEGIN TRAN

--  DECLARE @Month INT
--	SET @Month=1
--	DECLARE @Year INT
--	SET @Year=2024
--	DECLARE	@Outsourcing NVARCHAR(10)
--	SET @Outsourcing='ALL'
--	DECLARE @CreatedBy INT
--	SET @CreatedBy=1056

						DECLARE @NgayDauThang DATETIME,@NgayCuoiThang DATETIME
						SET @NgayDauThang= CONVERT(DATETIME,CAST(@Year AS varchar(4)) + '-' + CAST(@Month AS varchar(4)) + '-1')

						SET @NgayCuoiThang=DATEADD(DAY,-1,DATEADD(month,1,@NgayDauThang))


						SELECT OUTCD 
						INTO #tb_STOut
						FROM STOut (NOLOCK)
						WHERE ODATE=@NgayCuoiThang
						AND PLANTCD='WHADJ'
						AND FACTCD='WHADJ'

						DELETE dbo.STOut WHERE OUTCD IN (SELECT OUTCD FROM #tb_STOut)
						DELETE dbo.STOut_List WHERE OUTCD IN (SELECT OUTCD FROM #tb_STOut)

						DROP TABLE #tb_STOut








						-- khuc này lấy từ store ERP_Report_SQMatStockMonthly_List với type=1



						SELECT PInfoID,MatCD,PUnit,MfgVendor,CASE WHEN Incoterms='CIF' THEN  ISNULL(CIFPrice,0)/NULLIF(PriceLotSize,0)
							WHEN Incoterms='EXW' THEN ISNULL(EXWPrice,0)/NULLIF(PriceLotSize,0)
							WHEN Incoterms='FOB' THEN ISNULL(FOBPrice,0)/NULLIF(PriceLotSize,0)
							WHEN Incoterms='DAP' THEN ISNULL(CIFPrice,0)/NULLIF(PriceLotSize,0)
						ELSE ISNULL(FOBcPrice,0)/NULLIF(PriceLotSize,0) END CIFPrice
						,CASE WHEN Incoterms='CIF' THEN CIFCurr
							WHEN Incoterms='EXW' THEN EXWCurr
							WHEN Incoterms='FOB' THEN FOBCurr
							WHEN Incoterms='DAP' THEN CIFCurr
						ELSE FOBCurr END CIFCurr
						,CIFCurr CIFCurrTemp
						,EXWCurr EXWCurrTemp
						,FOBCurr FOBCurrTemp

						,ValidFrom
						,CASE WHEN Incoterms='CIF' THEN CIFPrice
							  WHEN Incoterms='EXW' THEN EXWPrice
							  WHEN Incoterms='FOB' THEN FOBPrice
							  WHEN Incoterms='DAP' THEN CIFPrice
							ELSE FOBPrice END PriceIncoterms
						,PriceLotSize
						INTO #TPInfo
						FROM dbo.TPInfo (NOLOCK)
						WHERE ValidFrom <= @NgayCuoiThang
						AND Discontinue=0
						OPTION (RECOMPILE)	

						--WHERE CIFPrice IS NOT NULL --Discontinue=0

						SELECT MAX(PInfoID) PInfoID,MatCD
						INTO #TPInfoGRID
						FROM #TPInfo
						GROUP BY MatCD


						SELECT MatCD,PUnit
						INTO #TPInfoGRPUnit
						FROM #TPInfo
						GROUP BY MatCD,PUnit
						ORDER BY MatCD

						SELECT MatCD
						INTO #TPInfoGRMutiPunit
						FROM #TPInfoGRPUnit
						GROUP BY MatCD
						HAVING COUNT(MatCD) > 1

						DROP TABLE #TPInfoGRPUnit

						SELECT a.MatCD
								,a.PUnit
								,a.MfgVendor
						--		,CASE WHEN a.CIFCurr IS NULL THEN 0
						--ELSE  a.CIFPrice END CIFPrice
								,a.CIFPrice
								,a.CIFCurr
								,a.ValidFrom
								,a.PriceLotSize
								,a.PriceIncoterms
								,b.ExRate
								,CASE WHEN c.MatCD IS NOT NULL AND a.PUNIT='SET' THEN 'PC'
									WHEN c.MatCD IS NOT NULL AND a.PUNIT='YRD' THEN 'M'
									WHEN c.MatCD IS NOT NULL AND a.PUNIT='SQM' THEN 'SQF'
									ELSE a.PUNIT END PUnitChuan
						INTO #TPInfoLaster
						FROM #TPInfo a
						LEFT JOIN dbo.TTExRateDaily b (NOLOCK) ON a.ValidFrom=b.BDate  AND a.CIFCurr=b.BCurrCD
						LEFT JOIN #TPInfoGRMutiPunit c ON a.MatCD=c.MatCD
						WHERE a.PInfoID IN (SELECT PInfoID FROM #TPInfoGRID)
						OPTION (RECOMPILE)	
	

						DROP TABLE #TPInfo,#TPInfoGRID











						SELECT c.PRID,a.BrandCD,a.ProdID
						INTO #PRIDBrandCD
						FROM TProd a (NOLOCK) 
						INNER JOIN  TProdInstItem b (NOLOCK) ON a.ProdID=b.ProdID
						INNER JOIN TPRProdInst c (NOLOCK) ON  b.ProdInstCD=c.ProdInstCD

						SELECT MAX(ProdID) ProdID,PRID,BrandCD 
						INTO #PRIDBrandCDGR
						FROM #PRIDBrandCD
						GROUP BY PRID,BrandCD

						SELECT ProdID,PRID,BrandCD 
						INTO #PRIDBrandCDGRFinal
						FROM #PRIDBrandCD
						WHERE ProdID IN (SELECT ProdID FROM #PRIDBrandCDGR)
						OPTION (RECOMPILE)	


						SELECT a.MatCD,MAX(b.BrandCD) BrandCD--,b.ProdID
						INTO #BrandCD
						FROM (
							SELECT MAX(a.PRID) PRID,a.MatCD
							FROM (
								SELECT PRID,MatCD,PRItemID 
								FROM dbo.TPRItem (NOLOCK) 
							) a
							GROUP BY a.MatCD
						) a
						INNER JOIN #PRIDBrandCDGRFinal b ON a.PRID=b.PRID
						GROUP BY a.MatCD--,b.ProdID--,b.BrandCD

						--SELECT MatCD,MAX(ProdID) ProdID
						--INTO #BrandCDGR
						--FROM #BrandCD
						--GROUP BY MatCD



						--SELECT a.MatCD,a.BrandCD 
						--INTO #BrandFinal
						--FROM #BrandCD a
						--INNER JOIN #BrandCDGR B ON a.MatCD=b.MatCD AND a.ProdID=b.ProdID

						DROP TABLE #PRIDBrandCDGR,#PRIDBrandCDGRFinal--,#BrandCD,#BrandCDGR


							SELECT  a.IVID
									,a.MatCD
									,a.PRICE
									,a.CURR
									,a.PUNIT
									,a.VCD
									,a.IVLISTNO
									,ISNULL(b.IDATE,ISNULL(a.WDATE,ISNULL(a.UDATE,a.CreatedOn))) IDATE
									,a.OUTCD
							INTO #GR 
							FROM STin_List a (NOLOCK)
							LEFT JOIN STin b (NOLOCK) ON a.IVID=b.IVID
							WHERE ISNULL(b.IDATE,ISNULL(a.WDATE,ISNULL(a.UDATE,a.CreatedOn))) <= @NgayCuoiThang
							OPTION (RECOMPILE)	

							--WHERE 
							--AND a.QTY > 0  AND a.OUTCD IS NULL-- AND a.MATCD='M0000220'


							SELECT MAX(IVLISTNO) IVLISTNO
								   ,MatCD 
							INTO #VendorGR
							FROM #GR
							GROUP BY MatCD

							SELECT  a.MatCD
								   ,b.VCD
							INTO #Vendor
							FROM #VendorGR a
							INNER JOIN #GR b ON a.IVLISTNO=b.IVLISTNO

							DROP TABLE #VendorGR

							--Giá lớn hơn 0
							SELECT MAX(IVLISTNO) IVLISTNO
								   ,MatCD 
							INTO #GRLatest
							FROM #GR
							WHERE PRICE > 0 AND CURR IS NOT NULL AND OUTCD IS NULL
							GROUP BY MatCD
							OPTION (RECOMPILE)	



							SELECT  a.MatCD
								   ,b.PRICE
								   ,b.PUNIT
								   ,b.CURR
								   ,c.ExRate
								   ,b.IDATE
							INTO #GRPriceLatest
							FROM #GRLatest a
							INNER JOIN #GR b ON a.IVLISTNO=b.IVLISTNO
							LEFT JOIN dbo.TTExRateDaily c (NOLOCK) ON c.BDate=b.IDATE AND c.BCurrCD=b.CURR

							--Giá 0

							SELECT MAX(a.IVLISTNO) IVLISTNO
								   ,a.MatCD 
							INTO #GRLatest_Price0
							FROM #GR a
							WHERE a.PRICE =0  AND a.CURR IS NOT NULL AND a.OUTCD IS NULL
							AND NOT EXISTS (SELECT * FROM #GRPriceLatest b WHERE a.MATCD=b.MATCD)
							GROUP BY MatCD
							OPTION (RECOMPILE)	


							INSERT #GRPriceLatest
							(
								MATCD,
								PRICE,
								PUNIT,
								CURR,
								ExRate,
								IDATE
							)
							SELECT  a.MatCD
								   ,b.PRICE
								   ,b.PUNIT
								   ,b.CURR
								   ,c.ExRate
								   ,b.IDATE
							FROM #GRLatest_Price0 a
							INNER JOIN #GR b ON a.IVLISTNO=b.IVLISTNO
							LEFT JOIN dbo.TTExRateDaily c (NOLOCK) ON c.BDate=b.IDATE AND c.BCurrCD=b.CURR


 
 						DROP TABLE #GR,#GRLatest,#GRLatest_Price0





						SELECT c.MatCD
								,SUM(CASE WHEN e.RSign='-' THEN -(c.OQTY)
								ELSE c.OQTY END) QTYGI
								,c.PUNIT
						INTO #GINotConvert
						FROM STOut_List c (NOLOCK)
						INNER JOIN (
							SELECT OUTCD
							FROM STOut (NOLOCK) 
							WHERE ODATE BETWEEN @NgayDauThang AND @NgayCuoiThang

						) g ON g.OUTCD=c.OUTCD
						INNER JOIN STReason e (NOLOCK) ON c.RCD=e.RCD
						WHERE c.MATCD IS NOT NULL
						GROUP BY c.MatCD,c.PUNIT
						OPTION (RECOMPILE)	

						--ORDER BY c.MatCD

							SELECT   a.MatCD
									,a.QTYGI
									,a.PUNIT
									,l.PUnitChuan
						INTO #GIPUnitChuan
						FROM #GINotConvert a
						LEFT JOIN #TPInfoLaster l ON l.MatCD = a.MATCD


						SELECT MatCD
							,PUnitChuan PUNIT
							,SUM(QTYGI*dbo.ConvQtyGR(ISNULL(PUNIT,''),ISNULL(PUnitChuan,0))) QTYGI
						INTO #GI
						FROM #GIPUnitChuan 
						GROUP BY MatCD,PUnitChuan


						DROP TABLE #GINotConvert,#GIPUnitChuan

						--DROP TABLE #STOut



						SELECT IVID 
						INTO #STIn_IVID
						FROM dbo.STIn (NOLOCK)
						WHERE IDATE BETWEEN @NgayDauThang AND @NgayCuoiThang
						OPTION (RECOMPILE)	


						SELECT  c.MatCD
								,c.QTY
								,c.PUNIT

								,l.PUnitChuan

								,c.IV
						INTO #GRSTIn_List
						FROM dbo.STIn_List c (NOLOCK)
						INNER JOIN #STIn_IVID g ON g.IVID=c.IVID
						LEFT JOIN #TPInfoLaster l ON c.MATCD=l.MatCD
						WHERE  c.MATCD IS NOT NULL
						AND c.OUTCD IS NULL
						OPTION (RECOMPILE)	
	

						DROP TABLE #STIn_IVID

						INSERT #GRSTIn_List
						(
							MATCD,
							QTY,
							PUNIT,
							PUnitChuan
						)
						SELECT  c.MatCD
							,c.QTY
							,c.PUNIT
							,l.PUnitChuan
						FROM dbo.STIn_List c (NOLOCK)
						INNER JOIN dbo.STInBad h (NOLOCK) ON c.OUTCD=h.OUTCD AND h.ODATE BETWEEN @NgayDauThang AND @NgayCuoiThang
						LEFT JOIN #TPInfoLaster l ON c.MATCD=l.MatCD
						WHERE c.MATCD IS NOT NULL AND c.OUTCD IS NOT NULL 
						--AND c.IV NOT IN (SELECT IV FROM #GRSTIn_List)
						OPTION (RECOMPILE)	


						SELECT MatCD,SUM(QTY*dbo.ConvQtyGR(ISNULL(PUNIT,''),ISNULL(PUnitChuan,0))) QTYGR,PUnitChuan PUNIT 
						INTO #GRConverPUNITfINAL
						FROM #GRSTIn_List 
						GROUP BY MatCD,PUnitChuan

						DROP TABLE #GRSTIn_List


						SELECT BYear,BCurrCD,TCurrCD,ExRate 
						INTO #TExRate
						FROM dbo.TExRate  (NOLOCK)

						SELECT MAX(BYear) BYear,BCurrCD
						INTO #TExRateGR
						FROM #TExRate
						GROUP BY BCurrCD


						SELECT a.BYear,a.BCurrCD,a.TCurrCD,a.ExRate
						INTO #TExRateGRTPInfor
						FROM #TExRate a
						INNER JOIN #TExRateGR b ON a.BYear=b.BYear AND a.BCurrCD=b.BCurrCD
						ORDER BY a.BYear
	
						DROP TABLE #TExRateGR

						SELECT MatCD,Qty,CreatedOn,StartYear,StartMonth,Unit,CloseYear,CloseMonth
						INTO #STBasicStock
						FROM dbo.STBasicStock (NOLOCK) 
						WHERE (StartYear=@Year AND StartMonth=@Month) 
						OR (StartYear=YEAR(DATEDIFF(MONTH,-1,@NgayCuoiThang)) 
						AND StartMonth=MONTH(DATEDIFF(MONTH,-1,@NgayCuoiThang)))
						OR (CloseYear=@Year AND CloseMonth=@Month)
						OPTION (RECOMPILE)	



						SELECT  a.MatCD
								,CASE WHEN a.Spec1 IS NULL AND a.Spec2 IS NOT NULL THEN a.MatName+ ' '+ a.Spec2
								WHEN a.Spec1 IS NOT NULL AND a.Spec2 IS NULL  THEN a.MatName+ ' '+ a.Spec1
								WHEN a.Spec1 IS NOT NULL AND a.Spec2 IS NOT NULL  THEN a.MatName+ ' '+ a.Spec1 + ' '+ a.Spec2
								ELSE a.MatName END MatDesc
								,a.Color
								,b.MH1
								,a.OutSourcing
								,CASE WHEN p.VCD IS NULL THEN c.MfgVendor 
									  ELSE p.VCD END Vendor
								,CASE WHEN m.MatCD IS NOT NULL THEN 1
								ELSE 0 END MultiUnit
								,c.PUnit PUnitLast
								,d.BrandCD

								,c.PriceLotSize PriceLotSizePInfo
								,c.PriceIncoterms PriceIncotermsPInfo
								,c.CIFCurr

								,e.PRICE*c.PriceLotSize PriceGR
								,e.CURR CURRGR



								,c.CIFPrice LastPricePInfo
								,e.PRICE LastPriceGR
								,e.PUNIT PUNITLastPriceGR
								,e.ExRate
								,e.IDATE
								,c.ExRate ExRateTPInfor
								,c.ValidFrom
								,f.Qty QtyBS
								,f.Unit PUnitBasicStock
								,h.QTYGR
								,h.PUNIT PUNITGR
								,g.QTYGI
								,g.PUNIT PUNITGI
								,(ISNULL(f.Qty,0) + ISNULL(h.QTYGR,0)) - ISNULL(g.QTYGI,0)  QtyEndingS
								,q.Qty QtyInspect
								,q.Unit PUnitInspect
								,q.CreatedOn DateInspect
						INTO #SQMatStockMonthly
						FROM  dbo.TMat a (NOLOCK) 
						LEFT JOIN  dbo.TMH b (NOLOCK) ON a.MHID=b.MHID
						LEFT JOIN #TPInfoLaster c ON a.MatCD=c.MatCD
						LEFT JOIN #BrandCD d ON a.MatCD=d.MatCD
						LEFT JOIN #GRPriceLatest e ON e.MATCD=a.MatCD
						LEFT JOIN #Vendor p ON a.MatCD=p.MATCD
						LEFT JOIN #STBasicStock f ON a.MatCD=f.MATCD AND f.StartYear=@Year AND f.StartMonth=@Month AND f.Qty<>0
						LEFT JOIN #GI g ON a.MatCD=g.MATCD
						LEFT JOIN #GRConverPUNITfINAL h ON a.MatCD=h.MATCD
						LEFT JOIN #TPInfoGRMutiPunit m ON a.MatCD=m.MatCD
						--LEFT JOIN #TExRate n (NOLOCK) ON n.BYear=e.YEAR AND e.CURR=n.BCurrCD
						--LEFT JOIN #TExRateGRTPInfor o (NOLOCK) ON  c.CIFCurr=o.BCurrCD
						--LEFT JOIN #STBasicStock q ON q.MatCD=a.MatCD AND q.StartYear=YEAR(DATEDIFF(MONTH,-1,@NgayCuoiThang)) 
						--AND q.StartMonth=MONTH(DATEDIFF(MONTH,-1,@NgayCuoiThang))
						LEFT JOIN #STBasicStock q ON q.MatCD=a.MatCD AND q.CloseYear=@Year AND q.CloseMonth=@Month AND q.Qty<>0 

						WHERE a.Discontinue=0 AND (@Outsourcing='ALL' OR (ISNULL(a.OutSourcing,0)=0 AND @Outsourcing='false') OR (ISNULL(a.OutSourcing,0)=1 AND @Outsourcing='true'))
						OPTION (RECOMPILE)	
	
						-- AND a.MatCD='M0000159'

						DROP TABLE #TPInfoGRMutiPunit,#TPInfoLaster,#GRPriceLatest,#GI,#GRConverPUNITfINAL,#TExRate,#TExRateGRTPInfor,#Vendor,#BrandCD,#STBasicStock

						SELECT   a.MatCD
								,a.MatDesc
								,a.Color
								,a.MH1
								,a.OutSourcing
								,a.Vendor
								,a.MultiUnit
								,a.PUnitLast
								,a.PUNITLastPriceGR
								,CASE   WHEN a.MultiUnit=1 AND a.PUnitLast='SET' THEN 'PC'
										WHEN a.MultiUnit=1 AND a.PUnitLast='YRD' THEN 'M'
										WHEN a.MultiUnit=1 AND a.PUnitLast='SQM' THEN 'SQF'
								ELSE a.PUnitLast END PUnitGRChuan
								,a.BrandCD

								--,a.PriceLotSizePInfo
								--,a.PriceIncotermsPInfo
								,a.LastPricePInfo
								,a.LastPriceGR


								,a.PriceLotSizePInfo
								,a.PriceIncotermsPInfo
								,a.CIFCurr
								,a.PriceGR
								,a.CURRGR


								,a.ExRateTPInfor
								,a.ValidFrom
								,a.ExRate
								,a.IDATE
								,a.QtyBS
								,a.PUnitBasicStock
								,a.QTYGR
								,a.PUNITGR
								,a.QTYGI
								,a.PUNITGI
								,a.QtyEndingS
								,a.QtyInspect
								,a.PUnitInspect
								,a.DateInspect			 
						INTO #SQMatStockMonthlyUnitChuan
						FROM #SQMatStockMonthly a

						DROP TABLE #SQMatStockMonthly

						SELECT   a.MatCD
								,a.MatDesc 
								,a.Color
								,a.MH1
								,a.OutSourcing
								,a.Vendor
								,a.MultiUnit
								,a.PUnitLast
								,ISNULL(a.PUnitGRChuan,a.PUnitGR) PUnitGRChuan
								,a.BrandCD

								,a.PriceLotSizePInfo
								,a.PriceIncotermsPInfo*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUnitLast,0)) PriceIncotermsPInfo
								,(a.LastPricePInfo*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUnitLast,0)))*a.ExRateTPInfor LastPricePInfo
								,a.CIFCurr
								,a.PriceGR*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)) PriceGR
								,a.CURRGR			
			

								--,CASE WHEN a.PriceIncotermsPInfo = 0 OR a.PriceIncotermsPInfo*a.ExRateTPInfor = 0  THEN NULL
								--		      WHEN a.CIFCurr=a.CURRGR  THEN ((a.PriceGR*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)))/a.PriceIncotermsPInfo)*100
								--		ELSE ((a.PriceGR*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0))*a.ExRate)/a.PriceIncotermsPInfo*a.ExRateTPInfor)*100 END Comp
			
								--,CASE WHEN a.PriceLotSizePInfo = 0 OR a.PriceGR = 0  THEN NULL
								--		WHEN a.CIFCurr=a.CURRGR  THEN (((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))
								--		- a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast,a.PUnitGRChuan) )
								--		/(a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan)))*100

								--ELSE (( (a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))*a.ExRate
								--- (a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast,a.PUnitGRChuan))*a.ExRateTPInfor)
								--/(a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))*a.ExRate)*100 END Comp

								,CASE WHEN a.PriceLotSizePInfo = 0 OR a.PriceGR = 0  THEN NULL
										WHEN a.CIFCurr=a.CURRGR  THEN ((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))		
								-(a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast,a.PUnitGRChuan)))
								/((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan)))*100 

								ELSE ((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))*a.ExRate 			
								-(a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast,a.PUnitGRChuan))*a.ExRateTPInfor)
								/((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))*a.ExRate)*100  END Comp
						
			
								,f.Payment



								--,a.PriceIncotermsPInfo
								--,a.CIFCurr
			
			
			
			
			
								,a.ValidFrom
								,a.LastPriceGR LastPriceGRtt
								,a.PUNITLastPriceGR
								,(a.LastPriceGR*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)))*a.ExRate LastPriceGR
								,a.IDATE
								,a.ExRate
								--,ISNULL(a.QtyBS,0) QtyBS
								,ISNULL(a.QtyBS,0)*dbo.ConvQtyGR(ISNULL(a.PUnitBasicStock,''),ISNULL(a.PUnitGRChuan,0)) QtyBS
								,a.QTYGR*dbo.ConvQtyGR(ISNULL(a.PUnitGR,''),ISNULL(a.PUnitGRChuan,0)) QTYGR
								--,a.PUnitGR
								,a.QTYGI*dbo.ConvQtyGR(ISNULL(a.PUNITGI,''),ISNULL(a.PUnitGRChuan,0)) QTYGI
								,a.PUNITGI
								--,ISNULL(a.QtyInspect,0) QtyInspect
								,ISNULL(a.QtyInspect,0)*dbo.ConvQtyGR(ISNULL(a.PUnitInspect,''),ISNULL(a.PUnitGRChuan,0)) QtyInspect
								,a.DateInspect	
						INTO #SQMatStockMonthlyFinal
						FROM #SQMatStockMonthlyUnitChuan a
						LEFT JOIN STCustomsMat f (NOLOCK) ON a.MatCD=f.MatCD
						--WHERE a.MatCD='M0046983'
						ORDER BY a.MatCD
						OPTION (RECOMPILE)	




						DROP TABLE #SQMatStockMonthlyUnitChuan

						SELECT MatCD
							   ,SUM(QtyUnCut+QtySubStore) QtyEU
						INTO #tb_TTWIPMat
						FROM dbo.TTWIPMat (NOLOCK)
						WHERE @Year=Year AND @Month=Month
						GROUP BY MatCD
						OPTION (RECOMPILE)	


						SELECT      IDENTITY( int ) AS RowID
									,a.MatCD
									,a.MatDesc 
									,a.Color
									,a.MH1
									,a.OutSourcing
									,a.Vendor
									--,a.MultiUnit
									--,a.PUnitLast
									,a.PUnitGRChuan PUnit
									,a.BrandCD

									--,a.PriceLotSizePInfo
								 --   ,a.PriceIncotermsPInfo
								,a.LastPricePInfo

								,a.PriceLotSizePInfo
								,a.PriceIncotermsPInfo
								,a.CIFCurr
								,a.PriceGR
								,a.CURRGR			
								,a.Comp
								,a.Payment



									,a.ValidFrom
									,a.LastPriceGR
									,a.IDATE
									--,ExRate
									,CASE WHEN a.LastPriceGR IS NULL THEN a.LastPricePInfo
									 ELSE a.LastPriceGR END UnitPrice
									,a.QtyBS
									,ISNULL(a.QTYGR,0)  QTYGR
									,ISNULL(a.QTYGI,0) QTYGI
									,ROUND((ISNULL(a.QtyBS,0) + ISNULL(a.QTYGR,0)) - ISNULL(a.QTYGI,0),5)  QtyEndingS 
									,a.QtyInspect
									,a.DateInspect	

									,b.QtyEU
						INTO #SQMatStockMonthlyRowID
						FROM #SQMatStockMonthlyFinal a
						LEFT JOIN #tb_TTWIPMat b ON a.MatCD=b.MatCD


						SELECT IDENTITY( int ) AS RowID
								,MatCD
								,MatDesc 
								,Color
								,MH1
								,OutSourcing
								,Vendor
								,PUnit
								,BrandCD

								--,PriceLotSizePInfo
								--,PriceIncotermsPInfo
								,LastPricePInfo
								,LastPriceGR

								,PriceLotSizePInfo
								,PriceIncotermsPInfo
								,CIFCurr
								,PriceGR
								,CURRGR			
								,ROUND(Comp,0) Comp
								,Payment


								,UnitPrice
								,QtyBS
								,QTYGR
								,QTYGI
								,QtyEndingS 
								,QtyBS*UnitPrice AmtBS
								,QTYGR*UnitPrice AmtGR
								,QTYGI*UnitPrice AmtGI
								,QtyEndingS*UnitPrice AmtES
								,QtyInspect
								,ROUND(QtyInspect - QtyEndingS,2) QtyDiff
								,DateInspect

								,QtyEU
								,(QtyEU+QtyEndingS) QtyESSum
								,QtyEU*UnitPrice AmtEU
								,(QtyEU+QtyEndingS)*UnitPrice AmtESSum
						INTO #SQMatStock
						FROM #SQMatStockMonthlyRowID
						ORDER BY MatCD

						DECLARE @SumAllQtyBS FLOAT,@SumAllQTYGR FLOAT,@SumAllQTYGI FLOAT,@SumAllQtyEndingS FLOAT,@SumAllAmtBS FLOAT,@SumAllAmtGR FLOAT
						,@SumAllAmtGI FLOAT,@SumAllAmtES FLOAT,@SumAllQtyInspect FLOAT,@SumAllQtyDiff FLOAT,@SumAllQtyEU FLOAT,@SumAllQtyESSum FLOAT
						,@SumAllAmtEU FLOAT,@SumAllAmtESSum FLOAT


						SELECT   @SumAllQtyBS=SUM(QtyBS)
								,@SumAllQTYGR=SUM(QTYGR) 
								,@SumAllQTYGI=SUM(QTYGI) 
								,@SumAllQtyEndingS=SUM(QtyEndingS) 
								,@SumAllAmtBS=SUM(AmtBS) 
								,@SumAllAmtGR=SUM(AmtGR) 
								,@SumAllAmtGI=SUM(AmtGI) 
								,@SumAllAmtES=SUM(AmtES) 
								,@SumAllQtyInspect=SUM(QtyInspect) 
								,@SumAllQtyDiff=SUM(QtyDiff) 
								,@SumAllQtyEU=SUM(QtyEU) 
								--,@SumAllQtyESSum=SUM(QtyESSum) 
								,@SumAllAmtEU=SUM(AmtEU) 
								,@SumAllAmtESSum=SUM(AmtESSum)  
						FROM #SQMatStock

						DROP TABLE #SQMatStockMonthlyFinal




						SELECT  
								MatCD
								,MatDesc
								,Color	
								,MH1
								,PUnit

								,OutSourcing
								,Vendor
								,BrandCD

								,PriceLotSizePInfo 'PriceLot'
								,PriceIncotermsPInfo 'PricePInfo'
								,CIFCurr 'Curr'

								,PriceGR
								,CURRGR	'Curr_Double0'
								,Comp 'Diff(%)_Percent0'
								,Payment
								,UnitPrice


								--,PriceLotSizePInfo
								--,PriceIncotermsPInfo
								--,LastPricePInfo
								--,LastPriceGR
								,QtyBS
								,QTYGR
								,QTYGI
								,QtyEndingS 'QtyES'
								,QtyEU
								,QtyESSum
								,AmtBS
								,AmtGR
								,AmtGI
								,AmtES
								,AmtEU
								,AmtESSum
								,QtyInspect
								,CONVERT(DECIMAL(27,12),QtyDiff) QtyDiff
								,REPLACE(convert(varchar, DateInspect, 106),' ','/') DateInspect

								,'Total' Total_Total_16
								,@SumAllQtyBS      Total_SumAllQtyBS_Format2_17
								,@SumAllQTYGR	   Total_SumAllQTYGR_Format2_18
								,@SumAllQTYGI	   Total_SumAllQTYGI_Format2_19
								,@SumAllQtyEndingS Total_SumAllQtyEndingS_Format2_20
								,@SumAllQtyEU	   Total_SumAllQtyEU_Format2_21
								,@SumAllQtyEndingS + @SumAllQtyEU  Total_SumAllQtyESSum_Format2_22
								,@SumAllAmtBS	   Total_SumAllAmtBS_Format2_23
								,@SumAllAmtGR	   Total_SumAllAmtGR_Format2_24
								,@SumAllAmtGI	   Total_SumAllAmtGI_Format2_25
								,@SumAllAmtES	   Total_SumAllAmtES_Format2_26
								,@SumAllAmtEU	   Total_SumAllAmtEU_Format2_27
								,@SumAllAmtESSum   Total_SumAllAmtESSum_Format2_28
								,@SumAllQtyInspect Total_SumAllQtyInspect_Format2_29
								,@SumAllQtyDiff	   Total_SumAllQtyDiff_Format2_30
                        INTO #SQMatStockFinal
						FROM #SQMatStock
						ORDER BY MatCD


	




						SELECT * 
						INTO #tb_StockFinal
						FROM #SQMatStockFinal		
						--WHERE QtyDiff >= 1 OR QtyDiff <= -1
						WHERE ROUND(QtyDiff,2,1) <> 0

						DROP TABLE #SQMatStock,#SQMatStockFinal

						DECLARE @OUTCD NVARCHAR(30),@EMPNO NVARCHAR(30)

						SELECT @OUTCD=CONVERT(NVARCHAR, GETDATE(),112) + REPLACE(CONVERT(NVARCHAR, GETDATE(),108),':','')

						SELECT @EMPNO=EmpNo FROM dbo.TEmp (NOLOCK) WHERE EmpID=@CreatedBy
			


						INSERT dbo.STOut
						(
						    OUTCD,
						    VKGRP,
						    PLANTCD,
						    FACTCD,
						    ODATE,
						    EMPNO,
						    REMPNO,
						    DISCONTINUE,
						    Remark,
						    CreatedBy,
						    CreatedOn
						)
						VALUES
						(   @OUTCD,     -- OUTCD - nvarchar(30)
						    'WHADJ' + CONVERT(NVARCHAR,@Year) + CONVERT(NVARCHAR,@Month),    -- VKGRP - nvarchar(100)
						    'WHADJ',    -- PLANTCD - nvarchar(20)
						    'WHADJ',    -- FACTCD - nvarchar(50)
						    @NgayCuoiThang,    -- ODATE - datetime
						    @EMPNO,    -- EMPNO - nvarchar(30)
						    @EMPNO,    -- REMPNO - nvarchar(30)
						    0, -- DISCONTINUE - bit
						    N'Monthly Adjust Stock',    -- Remark - nvarchar(100)
						    @CreatedBy,    -- CreatedBy - int
						    GETDATE()    -- CreatedOn - datetime
						)


						INSERT dbo.STOut_List
						(
						    OUTCD,
						    VKGRPCD,
						    MATCD,
						    PUNIT,
						    OQTY,
						    RCD,
						    CreatedBy,
						    CreatedOn
						)
                       SELECT 						    
					        @OUTCD,
						      'WHADJ' + CONVERT(NVARCHAR,@Year) + CONVERT(NVARCHAR,@Month),    -- VKGRP - nvarchar(100)
						    MatCD,
						    PUnit,
						    --QtyDiff,
							CASE WHEN QtyDiff > 0 THEN ROUND(QtyDiff,2)
							ELSE -(ROUND(QtyDiff,2)) END QtyDiff,
						    CASE WHEN QtyDiff > 0 THEN 811
							ELSE 812 END RCD,
						    @CreatedBy,    -- CreatedBy - int
						    GETDATE()    -- CreatedOn - datetime
							FROM #tb_StockFinal

                        DROP TABLE #tb_StockFinal





		                COMMIT TRAN

        END TRY
        BEGIN CATCH
				SELECT ERROR_MESSAGE() AS ErrorMessage;
                ROLLBACK TRAN

        END CATCH"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
            //try
            //{
            //    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            //    {
            //        var queryParametersCheckSTBasicStock = new DynamicParameters();
            //        queryParametersCheckSTBasicStock.Add("@CloseYear", int.Parse(item.Year.Value));
            //        queryParametersCheckSTBasicStock.Add("@CloseMonth", int.Parse(item.Month.Value));
            //        connection.Open();
            //        STBasicStock dataCheckSTBasicStock = await connection.QueryFirstOrDefaultAsync<STBasicStock>("SELECT TOP 1 * FROM dbo.STBasicStock (NOLOCK) WHERE CloseYear=@CloseYear AND CloseMonth=@CloseMonth"
            //            , queryParametersCheckSTBasicStock, commandTimeout: 1500
            //            , commandType: CommandType.Text);
            //        if (dataCheckSTBasicStock == null)
            //        {
            //            throw new NotImplementedException("Please Transfer Data Closing Year " + item.Year.Value + " and Closing Month " + item.Month.Value + "");
            //        }

            //        var queryParameters = new DynamicParameters();
            //        queryParameters.Add("@Month", item.Month.Value);
            //        queryParameters.Add("@Year", item.Year.Value);
            //        queryParameters.Add("@CreatedBy", int.Parse(GetUserlogin()));
            //        var data = await connection.QueryAsync<STIn_List_Custom>("ERP_STOut_List_AdjustStock"
            //            , queryParameters, commandTimeout: 1500
            //            , commandType: CommandType.StoredProcedure);
            //        return await Task.FromResult(true);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw new NotImplementedException(ex.Message.ToString());
            //}
        }
    }
}

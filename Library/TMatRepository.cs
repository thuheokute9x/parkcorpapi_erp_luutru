﻿using Dapper;
using HR.API.Models;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Web;
using HR.API.Models.ERP.TempModel.Development;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Library
{
    public class TMatRepository
    {
        public async Task<Data> ERP_TMat_Transfer(TDMat_Custom item,string database, string Userlogin)
        {
            try
            {
                Data kq = new Data();
                using (var connection = new SqlConnection(database))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatGrpCode", item.MatGrpCode);
                    connection.Open();
                    var data = await connection.QueryAsync<TDMat_Custom>("ERP_TMat_GetDataTransfer", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    foreach (TDMat_Custom ob in data.ToList())
                    {
                        if (!string.IsNullOrEmpty(ob.Incoterms))
                        {
                            //if (ob.CIFPrice == null || string.IsNullOrEmpty(ob.CIFCurr))
                            //{                            
                            //    kq.Type = 0;
                            //    kq.Results = ob.DMatCD + "-" + ob.Incoterms + "  Price or Currency";
                            //    return kq;
                            //}




                            //if (ob.Incoterms.Substring(ob.Incoterms.Length - 1, 1)=="c" || ob.CommissionRate==null)
                            //{
                            //    kq.Type = 0;
                            //    kq.Results = ob.DMatCD + " - " + ob.Incoterms + "  Commission Rate";
                            //    return kq;
                            //}

                            if ((ob.Incoterms == "CIF" && ob.CIFPrice == null) || (ob.Incoterms == "CIF" && string.IsNullOrEmpty(ob.CIFCurr)))
                            {
                                kq.Type = 0;
                                kq.Results = ob.DMatCD + " - " + ob.Incoterms + "  Price or Currency";
                                return kq;
                            }
                            if ((ob.Incoterms == "EXW" && ob.EXWPrice == null) || (ob.Incoterms == "EXW" && string.IsNullOrEmpty(ob.EXWCurr)))
                            {
                                kq.Type = 0;
                                kq.Results = ob.DMatCD + " - " + ob.Incoterms + "  Price or Currency";
                                return kq;
                            }
                            if ((ob.Incoterms == "FOB" && ob.FOBPrice == null) || (ob.Incoterms == "FOB" && string.IsNullOrEmpty(ob.FOBCurr)))
                            {
                                kq.Type = 0;
                                kq.Results = ob.DMatCD + " - " + ob.Incoterms + "  Price or Currency";
                                return kq;
                            }
                            if ((ob.Incoterms == "FOBc" && ob.FOBPrice == null) || (ob.Incoterms == "FOBc" && string.IsNullOrEmpty(ob.FOBCurr))
                                || (ob.Incoterms == "FOBc" && ob.CommissionRate == null))
                            {
                                kq.Type = 0;
                                kq.Results = ob.DMatCD + " - FOB Price, Currency, CommissionRate";
                                return kq;
                            }
                            if (ob.Costing)
                            {
                                kq.Type = 0;
                                kq.Results = ob.DMatCD + " is costing material. Please check again";
                                return kq;
                            }
                        }
                        if (string.IsNullOrEmpty(ob.Incoterms) || ob.MHID == null || ob.LossRate == null
                        || ob.MatShapeID == null || string.IsNullOrEmpty(ob.MfgVendor) || string.IsNullOrEmpty(ob.Shipper)
                        || ob.PGrpCD == null || ob.PLeadtime == null || ob.MOQ == null //|| ob.CommissionRate==null
                        || ob.PriceLotSize == null || ob.ValidFrom == null
                        )
                        {
                            kq.Type = 0;
                            kq.Results = ob.DMatCD + "-" + "  A field that requires to assign a value is unfilled";
                            return kq;
                        }


                    }
                    return await ERP_TMat_TransferTDMatToTMat(item,database,Userlogin);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Data> ERP_TMat_TransferTDMatToTMat(TDMat_Custom item, string database,string Userlogin)
        {
            try
            {
                using (var connection = new SqlConnection(database))
                {
                    Data kq = new Data();
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@CreatedBy", Userlogin);
                    queryParameters.Add("@MatGrpCode", item.MatGrpCode);
                    connection.Open();
                    var data = await connection.QueryAsync<TDMat_Custom>("ERP_TMat_TransferTDMatToTMat", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);

                    kq.Type = 1;
                    kq.Results = "Transfer Completed";



                    IDatabase db = ConnectRedis();
                    string Query = "SELECT MatCD,CASE WHEN Spec1 IS NULL AND Spec2 IS NOT NULL THEN MatName+ ' '+ Spec2 WHEN Spec1 IS NOT NULL AND Spec2 IS NULL  THEN MatName+ ' '+ Spec1 WHEN Spec1 IS NOT NULL AND Spec2 IS NOT NULL  THEN MatName+ ' '+ Spec1 + ' '+ Spec2 ELSE MatName END MatDesc  FROM dbo.TMat (NOLOCK)";
                    var List = new DynamicParameters();
                    var result = connection.Query<TMat_Basic>(Query, List, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    string value = JsonConvert.SerializeObject(result);
                    db.StringSet("TMatSelect_GetAll", value);


                    return kq;

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public IDatabase ConnectRedis()
        {
            //var host = Dns.GetHostEntry(Dns.GetHostName());
            //foreach (var ip in host.AddressList)
            //{
            //    if (ip.AddressFamily == AddressFamily.InterNetwork)
            //    {
            //        string a= ip.ToString();
            //    }
            //}
            bool isLocal = HttpContext.Current.Request.IsLocal;
            ConnectionMultiplexer redis = null;
            if (isLocal)
            {
                redis = ConnectionMultiplexer.Connect("localhost");
                return redis.GetDatabase();
            }
            redis = ConnectionMultiplexer.Connect("localhost,abortConnect=false,connectTimeout=30000,responseTimeout=30000");
            return redis.GetDatabase();
        }
    }
}

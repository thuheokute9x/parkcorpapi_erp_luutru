﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using SelectPdf;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class TMatPOItem_Repository
    {

        public Byte[] ERP_TMatPO_IssueReturnPDFNotExist_MKR(TMatPO_Custom item, List<TMatPOItem_Custom> itemchildrent,string company)
        {
            try
            {
                //đối với những trường hợp khác MKR Khi generate MPO thì 1 MPO là 1 shipper nhưng khi in thì 1 trang là 1 vendor
                var GroupVendor = itemchildrent.GroupBy(g => g.Vendor).Select(g => new { Vendor = g.Key }).ToList();

                string baseUrl = "";

                string pdf_page_size = "A4";
                PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                    pdf_page_size, true);
                int WebPageWidth = 950;
                HtmlToPdf converter = new HtmlToPdf();
                converter.Options.PdfPageSize = pageSize;
                converter.Options.WebPageWidth = WebPageWidth;
                SelectPdf.PdfDocument doc = null;
                decimal pagesizeAllVendor = 0;
                decimal pagesizeCurrent = 0;
                foreach (var Vendor in GroupVendor)
                {
                    pagesizeAllVendor = pagesizeAllVendor + 1 + Math.Ceiling((decimal)(itemchildrent.Where(x => x.Vendor == Vendor.Vendor).ToList().Count() - 21) / 27);
                }

                foreach (var Vendor in GroupVendor)
                {
                    pagesizeCurrent += 1;
                    string htmlPageOne = @"<html>
    <style>
      tr td {height:70px;}
    </style>
     <body>


    <div id='print_section' style='width: 1650px;height: 2350px; padding: 0px 30px 0px 30px;font-family: sans-serif; font-size: 26px;'>
      <br>
      <br>
      <br>
      <br>
      <div style='display: flex;flex-flow: row wrap; font-size: 38px;'>
        <div style='width:950px;'>";
                    if (company == "PCV")
                    {
                        if (item.PGrpCD != "MVN")
                        {
                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo1 + "</div>";
                        }
                        if (item.PGrpCD == "MVN")
                        {
                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo2 + "</div>";
                        }
                    }
                    if (company == "CYV")
                    {
                        if (item.PGrpCD != "MVN")
                        {
                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo1 + "</div>";
                        }
                        if (item.PGrpCD == "MVN")
                        {
                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo2 + "</div>";
                        }
                    }
                    if (company == "PHC")
                    {
                        if (item.PGrpCD != "MKH")
                        {
                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo1 + "</div>";
                        }
                        if (item.PGrpCD == "MKH")
                        {
                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo2 + "</div>";
                        }
                    }
                    if (company == "PBC")
                    {
                        htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo1 + "</div>";
                    }
                    if (company == "PAH")
                    {
                        htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo1 + "</div>";
                    }

                    htmlPageOne = htmlPageOne + @"</div>  
      <div style='width: 230px;font-weight: bold;'>     
      </div> 
      <div style='width: 370px;font-weight: bold;'>
        <div style='font-weight: 900;'>PURCHASE ORDER</div>               
      </div>       
      </div> 
    
      <div style='display: flex;flex-flow: row wrap;'>
        <div style='width:950px; margin-top: -9px;'>
        <div>";

                    if (((company == "PCV" || company == "CYV") && item.PGrpCD != "MVN")
                        || (company == "PHC" && item.PGrpCD != "MKH")
                        || (company == "PBC" && item.PGrpCD != "MBD")
                        || (company == "PAH")
                                    )
                    {
                        htmlPageOne = htmlPageOne + "145, Sagimakgol-ro,Jungwon-gu, Seongnam-si, Gyeonggi-do, Korea";
                    }

                    htmlPageOne = htmlPageOne + @"</div> </div>        
      </div> 
      <div style='display: flex;flex-flow: row wrap;padding-bottom: 6px; font-size: 28px;'>
        <div style='width:950px;'>
        <div style='font-weight: bold;margin-left: 10px;'>VENDOR (SELLER)</div>
       </div>  
       <div style='width: 230px;font-weight: bold;'>     
        <div style='font-weight: bold;margin-left: 10px;'>PO ISSUED</div>
      </div> 
      <div style='width: 370px;'>";
                    htmlPageOne = htmlPageOne + "<div>" + item.PostingDateReportConvert + " </div> ";
                    htmlPageOne = htmlPageOne + @" </div>       
      </div> 
      <div style='display: flex;flex-flow: row wrap;padding-bottom: 6px; font-size: 28px;'>
        <div style='width:950px;'>";

                    htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;'>" + item.VendorNamePrintMPO + "</div>";
                    htmlPageOne = htmlPageOne + @"</div>  
       <div style='width: 230px;font-weight: bold;'>     
        <div style='font-weight: bold;margin-left: 10px;'>PO NUMBER</div>
      </div> 
      <div style='width: 370px;'>";
                    htmlPageOne = htmlPageOne + "<div>" + item.MatPOCD + " </div>";
                    htmlPageOne = htmlPageOne + @"</div>
      </div>
                      <div style = 'display: flex;flex-flow: row wrap; font-size: 28px;'>
                         <div style = 'width:950px;'>
                          <div style = 'font-weight: bold;margin-left: 10px;'> SHIP TO </div>   
                              </div>
                              <div style = 'width: 230px;font-weight: bold;'>
                                <div style = 'font-weight: bold;margin-left: 10px;margin-bottom:6px;'> CURRENCY </div>
                               </div>
                               <div style = 'width: 370px;'>";
                    htmlPageOne = htmlPageOne + "<div>" + item.Currency + " </div>";
                    htmlPageOne = htmlPageOne + @"</div>
                                </div>
                                <div style = 'display: flex;flex-flow: row wrap;padding-bottom: 6px;'>
                                   <div style = 'width:950px;'>";

                    htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo3 + "</div>";
                    htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>"
                            + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>"
                            + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";

                    //if (company == "PCV")
                    //    {
                    //        if (item.PGrpCD != "MVN")
                    //        {
                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo1 + "</div>";
                    //        }
                    //        if (item.PGrpCD == "MVN")
                    //        {
                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo2 + "</div>";
                    //        }
                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";
                    //    }
                    //    if (company == "CYV")
                    //    {
                    //        if (item.PGrpCD != "MVN")
                    //        {
                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo1 + "</div>";
                    //        }
                    //        if (item.PGrpCD == "MVN")
                    //        {
                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo2 + "</div>";
                    //        }
                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";
                    //    }
                    //    if (company == "PHC")
                    //    {
                    //        if (item.PGrpCD != "MKH")
                    //        {
                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo1 + "</div>";
                    //        }
                    //        if (item.PGrpCD == "MKH")
                    //        {
                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo2 + "</div>";
                    //        }
                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";
                    //    }
                    //    if (company == "PBC")
                    //    {
                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo1 + "</div>";
                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";
                    //    }
                    //    if (company == "PAH")
                    //    {
                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo1 + "</div>";
                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";
                    //    }

                    htmlPageOne = htmlPageOne + "<div style='font-weight: bold;margin-left: 10px;width:950px;height: 33px; font-size: 28px;'>Prod Order # <span style='font-weight: 100;'>" + item.RefVKNo + "</span></div>";

                    htmlPageOne = htmlPageOne + @"</div>  
                                  <div style='width: 230px;font-weight: bold; font-size: 28px;'>     
                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>ORIG. ETD</div>
                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>ATTENTION TO</div>
                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>RANGE</div>
                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>VENDOR</div>
                                  </div> 
                                  <div style='width: 470px; font-size: 28px;'>";
                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:33px;white-space: nowrap;'>" + item.ReqETDReportConvert + " </div>";
                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:34px;white-space: nowrap;'>" + item.AttentionTo + "</div>";
                    if (!string.IsNullOrEmpty(item.Range) && item.Range.Length > 26)
                    {
                        item.Range = item.Range.Substring(0, 26);
                    }
                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:34px;white-space: nowrap;'>" + item.Range + " </div>";
                    string vendor = Vendor.Vendor;
                    if (!string.IsNullOrEmpty(vendor) && vendor.Length > 26)
                    {
                        vendor = vendor.Substring(0, 26);
                    }
                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:34px;white-space: nowrap;'>" + vendor + " </div>";

                    htmlPageOne = htmlPageOne + @"</div>       


                </div> 

                                  <div style='border-style: solid;padding: 5px 5px 5px 5px;margin-bottom: 5px;border-width: thin;";
                    if (company == "PHC")
                    {
                        htmlPageOne = htmlPageOne + "font-size:24px;'>";
                    }
                    if (company != "PHC")
                    {
                        htmlPageOne = htmlPageOne + "'>";
                    }
                    htmlPageOne = htmlPageOne + @"<div>The Supplier will deliver goods in time-mentioned as above. ";
                    if (company == "PCV" || company == "PBC" || company == "PAH")
                    {
                        htmlPageOne = htmlPageOne + "Park Company Ltd";
                    }
                    if (company == "PHC")
                    {
                        htmlPageOne = htmlPageOne + "PARK HANDBAG (CAMBODIA) Co., Ltd";
                    }
                    if (company == "CYV")
                    {
                        htmlPageOne = htmlPageOne + "CY Vina Co., Ltd";
                    }

                    htmlPageOne = htmlPageOne + @" reserves the right to examine goods after delivery.</div>  
                                  <div>The Supplier shall reimburse ";
                    if (company == "PCV" || company == "PBC" || company == "PAH")
                    {
                        htmlPageOne = htmlPageOne + "Park Company Ltd";
                    }
                    if (company == "PHC")
                    {
                        htmlPageOne = htmlPageOne + "PARK HANDBAG (CAMBODIA) Co., Ltd";
                    }
                    if (company == "CYV")
                    {
                        htmlPageOne = htmlPageOne + "CY Vina Co., Ltd";
                    }

                    htmlPageOne = htmlPageOne + @" immediately cost and expense involved in such delay or inferior quality.</div>
                                  <div>Original Invoice, Packing List, Bill of Lading for this MPO should be sent to consignee as 'ship to' address and attn above timely.";
                    if (!string.IsNullOrEmpty(item.Notice))
                    {
                        htmlPageOne = htmlPageOne + " </br> " + item.Notice;
                    }
                    htmlPageOne = htmlPageOne + @"</div>      
                                  </div>    
                                  <table style='border-collapse: collapse;width: 100%;font-size: 26px;'>";
                    if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
                    {
                        htmlPageOne = htmlPageOne + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px; font-size: 28px;'>
                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
                                  <th style='width: 12%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 35px;'>Code</th>
                                  <th style='width: 70%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
                                </tr>    
                            ";
                    }
                    if (item.PRINTTYPE == "2")
                    {
                        htmlPageOne = htmlPageOne + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
                                  <th style='width: 17%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 10px;'>Code/CM#(Coach)</th>
                                  <th style='width: 65%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
                                </tr>    
                            ";
                    }


                    string detailpageone = "";
                    List<TMatPOItem_Custom> ListChildrentPageOne = itemchildrent.Where(x => x.RowGRVendor <= 21 && x.Vendor == Vendor.Vendor).OrderBy(x => x.RowGRVendor).ToList();

                    decimal pdfpagesize = Math.Ceiling((decimal)(itemchildrent.Where(x => x.Vendor == Vendor.Vendor).Count() - 21) / 27);

                    foreach (TMatPOItem_Custom ob in ListChildrentPageOne)
                    {
                        if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
                        {
                            detailpageone = detailpageone + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: center;vertical-align: top;'> " + ob.MatCD + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
                        }
                        if (item.PRINTTYPE == "2")
                        {
                            detailpageone = detailpageone + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 10px;'> " + ob.MatCD + " <br> " + ob.CustItemNo1 + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
                        }
                    }
                    htmlPageOne = htmlPageOne + detailpageone + "</table></div><div style='width: 1650px;margin: 0px 30px 0px 30px;height: 6px;background-color: black;'></div><div style='font-weight: bold;text-align: center; margin-bottom: 5px;font-size: 25px;'>" + pagesizeCurrent.ToString() + " / " + (pagesizeAllVendor).ToString() + "</div></body></html>";


                    if (doc == null)
                    {
                        doc = converter.ConvertHtmlString(htmlPageOne, baseUrl);
                    }
                    else if (doc != null)
                    {
                        SelectPdf.PdfDocument docVendor = converter.ConvertHtmlString(htmlPageOne, baseUrl);
                        doc.Append(docVendor);
                    }


                    if (pdfpagesize > 0)
                    {
                        for (int i = 1; i <= pdfpagesize; i++)
                        {
                            pagesizeCurrent += 1;
                            HtmlToPdf convertertwo = new HtmlToPdf();

                            string htmlPageTwo = @"
                            <html>

                            <style>
                              tr td {height:70px;}
                            </style>
                             <body>
                                <div id='print_section' style='width: 1650px;height: 2350px; padding: 0px 30px 0px 30px;font-family: sans-serif; font-size: 26px;'>
                                  <br>
                                  <br>
                                  <br>
                                  <br>  
                                  <table style='border-collapse: collapse;width: 100%;font-size: 25px;'>";

                            if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
                            {
                                htmlPageTwo = htmlPageTwo + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;font-size: 28px;'>
                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
                                  <th style='width: 12%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 35px;'>Code</th>
                                  <th style='width: 70%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
                                </tr>    
                            ";
                            }
                            if (item.PRINTTYPE == "2")
                            {
                                htmlPageTwo = htmlPageTwo + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
                                  <th style='width: 17%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 10px;'>Code/CM#(Coach)</th>
                                  <th style='width: 65%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
                                </tr>    
                            ";
                            }
                            List<TMatPOItem_Custom> ListChildrentPagetwo = itemchildrent.Where(x => x.Vendor == Vendor.Vendor && x.RowGRVendor > 21 && x.RowGRVendor <= ((i * 27) + 21)).OrderBy(x => x.RowGRVendor).Skip(((i - 1) * 27)).Take(27).ToList();
                            string detailpagetwo = "";

                            foreach (TMatPOItem_Custom ob in ListChildrentPagetwo)
                            {
                                if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
                                {
                                    detailpagetwo = detailpagetwo + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: center;vertical-align: top;'> " + ob.MatCD + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
                                }
                                if (item.PRINTTYPE == "2")
                                {
                                    detailpagetwo = detailpagetwo + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 10px;'> " + ob.MatCD + " <br> " + ob.CustItemNo1 + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
                                }


                            }
                            htmlPageTwo = htmlPageTwo + detailpagetwo + "</table></div><div style='width: 1650px;margin: 0px 30px 0px 30px;height: 6px;background-color: black;'></div><div style='font-weight: bold;text-align: center; margin-bottom: 5px;font-size: 25px;'>" + pagesizeCurrent.ToString() + " / " + (pagesizeAllVendor).ToString() + " </div></body></html>";

                            convertertwo.Options.PdfPageSize = pageSize;
                            convertertwo.Options.WebPageWidth = WebPageWidth;

                            SelectPdf.PdfDocument doctwo = convertertwo.ConvertHtmlString(htmlPageTwo, baseUrl);
                            doc.Append(doctwo);


                        }
                    }




                }
                return doc.Save();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public Byte[] ERP_TMatPO_IssueReturnPDF_MKR(TMatPO_Custom item, List<TMatPOItem_Custom> itemchildrent)
        {
            try
            {
                //đối với những trường hợp MKR Khi generate MPO thì 1 MPO là 1 vendor nhưng khi in thì 1 trang là 1 vendor
                var GroupVendor = itemchildrent.GroupBy(g => g.Vendor).Select(g => new { Vendor = g.Key, VendorName = g.ToList().Max(d => d.VendorName) }).ToList();

                string baseUrl = "";

                string pdf_page_size = "A4";
                PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                    pdf_page_size, true);
                int WebPageWidth = 950;
                HtmlToPdf converter = new HtmlToPdf();
                converter.Options.PdfPageSize = pageSize;
                converter.Options.WebPageWidth = WebPageWidth;
                SelectPdf.PdfDocument doc = null;
                decimal pagesizeAllVendor = 0;
                decimal pagesizeCurrent = 0;
                foreach (var Vendor in GroupVendor)
                {
                    pagesizeAllVendor = pagesizeAllVendor + 1 + Math.Ceiling((decimal)(itemchildrent.Where(x => x.Vendor == Vendor.Vendor).ToList().Count() - 21) / 27);
                }
                foreach (var Vendor in GroupVendor)
                {
                    pagesizeCurrent += 1;
                    string htmlPageOne = @"<html>
    <style>
      tr td {height:70px;}
    </style>
     <body>


    <div id='print_section' style='width: 1650px;height: 2350px; padding: 0px 30px 0px 30px;font-family: sans-serif; font-size: 26px;'>
      <br>
      <br>
      <br>
      <br>
      <div style='display: flex;flex-flow: row wrap; font-size: 38px;'>
        <div style='width:950px;'>";
                    htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo1 + "</div>";

                    htmlPageOne = htmlPageOne + @"</div>  
      <div style='width: 230px;font-weight: bold;'>     
      </div> 
      <div style='width: 370px;font-weight: bold;'>
        <div style='font-weight: 900;'>PURCHASE ORDER</div>               
      </div>       
      </div> 
    
      <div style='display: flex;flex-flow: row wrap;'>
        <div style='width:950px; margin-top: -9px;'>
        <div>";

                    htmlPageOne = htmlPageOne + "145, Sagimakgol-ro,Jungwon-gu, Seongnam-si, Gyeonggi-do, Korea";

                    htmlPageOne = htmlPageOne + @"</div> </div>        
      </div> 
      <div style='display: flex;flex-flow: row wrap;padding-bottom: 6px; font-size: 28px;'>
        <div style='width:950px;'>
       </div>  
       <div style='width: 230px;font-weight: bold;'>     
        <div style='font-weight: bold;margin-left: 10px;'>PO ISSUED</div>
      </div> 
      <div style='width: 370px;'>";
                    htmlPageOne = htmlPageOne + "<div>" + item.PostingDateReportConvert + " </div> ";
                    htmlPageOne = htmlPageOne + @" </div>       
      </div> 
      <div style='display: flex;flex-flow: row wrap;padding-bottom: 6px; font-size: 28px;'>
        <div style='width:950px;'>";

                    htmlPageOne = htmlPageOne + @"</div>  
       <div style='width: 230px;font-weight: bold;'>     
        <div style='font-weight: bold;margin-left: 10px;'>PO NUMBER</div>
      </div> 
      <div style='width: 370px;'>";
                    htmlPageOne = htmlPageOne + "<div>" + item.MatPOCD + " </div>";
                    htmlPageOne = htmlPageOne + @"</div>
      </div>
                      <div style = 'display: flex;flex-flow: row wrap; font-size: 28px;'>
                         <div style = 'width:950px;'>
                              </div>
                              <div style = 'width: 230px;font-weight: bold;'>
                                <div style = 'font-weight: bold;margin-left: 10px;margin-bottom:6px;'> CURRENCY </div>
                               </div>
                               <div style = 'width: 370px;'>";
                    htmlPageOne = htmlPageOne + "<div>" + item.Currency + " </div>";
                    htmlPageOne = htmlPageOne + @"</div>
                                </div>
                                <div style = 'display: flex;flex-flow: row wrap;padding-bottom: 6px;'>
                                   <div style = 'width:950px;margin-top: -115px;'>";
                    htmlPageOne = htmlPageOne + "<div style='margin-left: 10px;'>ZIP CODE) 13202</div>";
                    htmlPageOne = htmlPageOne + "<div style='margin-left: 10px;'>TEL) 031-748-0304</div>";
                    htmlPageOne = htmlPageOne + "<div style='margin-left: 10px;'>FAX) 031-749-1057</div>";
                    htmlPageOne = htmlPageOne + "<div style='font-weight: bold;margin-left: 10px;margin-top: 41px;'>VENDOR (SELLER)</div>";
                    htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;margin-bottom: 41px;'>" + Vendor.VendorName + "</div>";




                    htmlPageOne = htmlPageOne + "<div style='font-weight: bold;margin-left: 10px;width:950px;height: 33px; font-size: 28px;'>Prod Order # <span style='font-weight: 100;'>" + item.RefVKNo + "</span></div>";

                    htmlPageOne = htmlPageOne + @"</div>  
                                  <div style='width: 230px;font-weight: bold; font-size: 28px;'>     
                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>ORIG. ETD</div>
                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>ATTENTION TO</div>
                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>RANGE</div>
                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>VENDOR</div>
                                  </div> 
                                  <div style='width: 470px; font-size: 28px;'>";
                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:33px;white-space: nowrap;'>" + item.ReqETDReportConvert + " </div>";
                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:34px;white-space: nowrap;'>" + item.AttentionTo + "</div>";
                    if (!string.IsNullOrEmpty(item.Range) && item.Range.Length > 26)
                    {
                        item.Range = item.Range.Substring(0, 26);
                    }
                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:34px;white-space: nowrap;'>" + item.Range + " </div>";
                    string vendor = Vendor.Vendor;
                    if (!string.IsNullOrEmpty(vendor) && vendor.Length > 26)
                    {
                        vendor = vendor.Substring(0, 26);
                    }
                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:34px;white-space: nowrap;'>" + vendor + " </div>";

                    htmlPageOne = htmlPageOne + @"</div>       


                </div> 

                                  <div style='border-style: solid;padding: 5px 5px 5px 5px;margin-bottom: 5px;border-width: thin;font-size: 25px !important;";

                    htmlPageOne = htmlPageOne + "'>";
                    htmlPageOne = htmlPageOne + @"<div>The Supplier will deliver goods in time-mentioned as above. ";
                    htmlPageOne = htmlPageOne + item.ShipTo1;

                    htmlPageOne = htmlPageOne + @" reserves the right to examine goods after delivery.</div>  
                                  <div>The Supplier shall reimburse ";
                    htmlPageOne = htmlPageOne + item.ShipTo1;


                    htmlPageOne = htmlPageOne + @" immediately cost and expense involved in such delay or inferior quality.</div>
                                  <div>Original Invoice, Packing List, Bill of Lading for this MPO should be sent to consignee as 'ship to' address and attn above timely.";
                    if (!string.IsNullOrEmpty(item.Notice))
                    {
                        htmlPageOne = htmlPageOne + " </br> " + item.Notice;
                    }
                    htmlPageOne = htmlPageOne + @"</div>      
                                  </div>    
                                  <table style='border-collapse: collapse;width: 100%;font-size: 26px;'>";
                    if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
                    {
                        htmlPageOne = htmlPageOne + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px; font-size: 28px;'>
                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
                                  <th style='width: 12%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 35px;'>Code</th>
                                  <th style='width: 70%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
                                </tr>    
                            ";
                    }
                    if (item.PRINTTYPE == "2")
                    {
                        htmlPageOne = htmlPageOne + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
                                  <th style='width: 17%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 10px;'>Code/CM#(Coach)</th>
                                  <th style='width: 65%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
                                </tr>    
                            ";
                    }


                    string detailpageone = "";
                    List<TMatPOItem_Custom> ListChildrentPageOne = itemchildrent.Where(x => x.RowGRVendor <= 21 && x.Vendor == Vendor.Vendor).OrderBy(x => x.RowGRVendor).ToList();

                    decimal pdfpagesize = Math.Ceiling((decimal)(itemchildrent.Where(x => x.Vendor == Vendor.Vendor).Count() - 21) / 27);

                    foreach (TMatPOItem_Custom ob in ListChildrentPageOne)
                    {
                        if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
                        {
                            detailpageone = detailpageone + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: center;vertical-align: top;'> " + ob.MatCD + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
                        }
                        if (item.PRINTTYPE == "2")
                        {
                            detailpageone = detailpageone + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 10px;'> " + ob.MatCD + " <br> " + ob.CustItemNo1 + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
                        }
                    }
                    htmlPageOne = htmlPageOne + detailpageone + "</table></div><div style='width: 1650px;margin: 0px 30px 0px 30px;height: 6px;background-color: black;'></div><div style='font-weight: bold;text-align: center; margin-bottom: 5px;font-size: 25px;'>" + pagesizeCurrent.ToString() + " / " + (pagesizeAllVendor).ToString() + "</div></body></html>";


                    if (doc == null)
                    {
                        doc = converter.ConvertHtmlString(htmlPageOne, baseUrl);
                    }
                    else if (doc != null)
                    {
                        SelectPdf.PdfDocument docVendor = converter.ConvertHtmlString(htmlPageOne, baseUrl);
                        doc.Append(docVendor);
                    }



                    if (pdfpagesize > 0)
                    {
                        for (int i = 1; i <= pdfpagesize; i++)
                        {
                            pagesizeCurrent += 1;
                            HtmlToPdf convertertwo = new HtmlToPdf();

                            string htmlPageTwo = @"
                            <html>

                            <style>
                              tr td {height:70px;}
                            </style>
                             <body>
                                <div id='print_section' style='width: 1650px;height: 2350px; padding: 0px 30px 0px 30px;font-family: sans-serif; font-size: 26px;'>
                                  <br>
                                  <br>
                                  <br>
                                  <br>  
                                  <table style='border-collapse: collapse;width: 100%;font-size: 25px;'>";

                            if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
                            {
                                htmlPageTwo = htmlPageTwo + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;font-size: 28px;'>
                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
                                  <th style='width: 12%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 35px;'>Code</th>
                                  <th style='width: 70%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
                                </tr>    
                            ";
                            }
                            if (item.PRINTTYPE == "2")
                            {
                                htmlPageTwo = htmlPageTwo + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
                                  <th style='width: 17%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 10px;'>Code/CM#(Coach)</th>
                                  <th style='width: 65%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
                                </tr>    
                            ";
                            }
                            List<TMatPOItem_Custom> ListChildrentPagetwo = itemchildrent.Where(x => x.Vendor == Vendor.Vendor && x.RowGRVendor > 21 && x.RowGRVendor <= ((i * 27) + 21)).OrderBy(x => x.RowGRVendor).Skip(((i - 1) * 27)).Take(27).ToList();
                            string detailpagetwo = "";

                            foreach (TMatPOItem_Custom ob in ListChildrentPagetwo)
                            {
                                if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
                                {
                                    detailpagetwo = detailpagetwo + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: center;vertical-align: top;'> " + ob.MatCD + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
                                }
                                if (item.PRINTTYPE == "2")
                                {
                                    detailpagetwo = detailpagetwo + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 10px;'> " + ob.MatCD + " <br> " + ob.CustItemNo1 + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
                                }


                            }
                            htmlPageTwo = htmlPageTwo + detailpagetwo + "</table></div><div style='width: 1650px;margin: 0px 30px 0px 30px;height: 6px;background-color: black;'></div><div style='font-weight: bold;text-align: center; margin-bottom: 5px;font-size: 25px;'>" + pagesizeCurrent.ToString() + " / " + (pagesizeAllVendor).ToString() + " </div></body></html>";

                            convertertwo.Options.PdfPageSize = pageSize;
                            convertertwo.Options.WebPageWidth = WebPageWidth;

                            SelectPdf.PdfDocument doctwo = convertertwo.ConvertHtmlString(htmlPageTwo, baseUrl);
                            doc.Append(doctwo);


                        }
                    }

                }
                return doc.Save();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        //        public Byte[] ERP_TMatPO_IssueReturnPDF_MKR(TMatPO_Custom item, List<TMatPOItem_Custom> itemchildrent) //code MKR tiếng hàn
        //        {
        //            try
        //            {
        //                //đối với những trường hợp MKR Khi generate MPO thì 1 MPO là 1 vendor nhưng khi in thì 1 trang là 1 vendor
        //                var GroupVendor = itemchildrent.GroupBy(g => g.Vendor).Select(g => new { Vendor = g.Key, VendorName = g.ToList().Max(d => d.VendorName) }).ToList();

        //                string baseUrl = "";

        //                string pdf_page_size = "A4";
        //                PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
        //                    pdf_page_size, true);
        //                int WebPageWidth = 950;
        //                HtmlToPdf converter = new HtmlToPdf();
        //                converter.Options.PdfPageSize = pageSize;
        //                converter.Options.WebPageWidth = WebPageWidth;
        //                SelectPdf.PdfDocument doc = null;
        //                decimal pagesizeAllVendor = 0;
        //                decimal pagesizeCurrent = 0;
        //                foreach (var Vendor in GroupVendor)
        //                {
        //                    pagesizeAllVendor = pagesizeAllVendor + 1 + Math.Ceiling((decimal)(itemchildrent.Where(x => x.Vendor == Vendor.Vendor).ToList().Count() - 21) / 27);
        //                }
        //                foreach (var Vendor in GroupVendor)
        //                {
        //                    pagesizeCurrent += 1;
        //                    string htmlPageOne = @"<html>
        //<style>
        //  tr td {height:70px;}
        //</style>
        // <body>
        //    <div id='print_section' style='width: 1650px;height: 2350px; padding: 0px 30px 0px 30px;font-family: sans-serif; font-size: 26px;'>
        //      <br>
        //      <br>
        //      <br>
        //      <br>
        //      <div style='display: flex;flex-flow: row wrap; font-size: 38px;'>
        //        <div style='width:850px;'>
        //        <div style='font-weight: bold;'>주식회사 한국파크</div>
        //       </div>  
        //       <div style='width: 200px;font-weight: bold;'>     
        //      </div> 
        //      <div style='width: 500px;font-weight: bold;'>               
        //      </div>       
        //      </div>        
        //      <div style='display: flex;flex-flow: row wrap;padding-bottom: 6px;'>
        //        <div style='width:850px;'>
        //        <div style='margin-left: 10px;'>(우) <span style='font-weight: bold;'>13202</span></div>
        //        <div style='margin-left: 10px;'>경기도 성남시 중원구 사기막골로 <span style='font-weight: bold;'>145</span></div>          
        //        <div style='font-weight: bold;margin-left: 10px;'>TEL) 031-748-0304</div>          
        //        <div style='font-weight: bold;margin-left: 10px;'>FAX) 031-748-1057</div>
        //        <div style='margin-left: 20px;width:850px;height: 25px;'></div>
        //        <div style='font-weight: bold;margin-left: 10px;'>공급처</div>";

        //                    htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:850px;height: 36px;'>" + Vendor.VendorName + "</div>";
        //                    htmlPageOne = htmlPageOne + "<div style='font-weight: bold;margin-left: 10px;width:850px;height: 25px;'>Prod Order # <span style='font-weight: 100;'>" + item.RefVKNo + "</span></div>";
        //                    htmlPageOne = htmlPageOne + @"</div>  
        //        <div style='width: 200px;font-weight: bold;text-align: right;padding-right: 10px;border-right: solid;border-width: thin;'>   
        //        <div style='font-weight: bold;'>발주일</div>
        //        <div style='font-weight: bold;'>발주번호</div>          
        //        <div style='font-weight: bold;'>납품기일</div>
        //        <div style='font-weight: bold;'>담당자</div>
        //        <div style='font-weight: bold;'>대금결제방법</div>
        //        <div style='font-weight: bold;'>PACKING LIST</div>
        //        <div style='font-weight: bold;'>납품장소</div>
        //        <div style='font-weight: bold;'>비고</div>    
        //      </div> 
        //      <div style='width: 500px;padding-left: 10px;'>";
        //                    htmlPageOne = htmlPageOne + "<div style='height: 35px;white-space: nowrap;'>" + item.PostingDateReportTiengHanConvert + "</div>";
        //                    htmlPageOne = htmlPageOne + "<div style='height: 29px;white-space: nowrap;'>" + item.MatPOCD + " </div>";
        //                    htmlPageOne = htmlPageOne + "<div style='height: 29px;white-space: nowrap;'>" + item.ReqETDReportTiengHanConvert + "</div>";
        //                    if (!string.IsNullOrEmpty(item.AttentionTo) && item.AttentionTo.Length > 26)
        //                    {
        //                        item.AttentionTo = item.AttentionTo.Substring(0, 26);
        //                    }
        //                    htmlPageOne = htmlPageOne + "<div style='height: 29px;white-space: nowrap;'>" + item.AttentionTo + "</div>";
        //                    if (!string.IsNullOrEmpty(item.ExtPayMethod) && item.ExtPayMethod.Length > 26)
        //                    {
        //                        item.ExtPayMethod = item.ExtPayMethod.Substring(0, 26);
        //                    }
        //                    htmlPageOne = htmlPageOne + "<div style='height: 29px;white-space: nowrap;'>" + item.ExtPayMethod + "</div>";
        //                    if (!string.IsNullOrEmpty(item.ExtPLNo) && item.ExtPLNo.Length > 26)
        //                    {
        //                        item.ExtPLNo = item.ExtPLNo.Substring(0, 26);
        //                    }
        //                    htmlPageOne = htmlPageOne + "<div style='height: 31px;white-space: nowrap;'>" + item.ExtPLNo + "</div>";
        //                    if (!string.IsNullOrEmpty(item.ExtDeliverTo) && item.ExtDeliverTo.Length > 26)
        //                    {
        //                        item.ExtDeliverTo = item.ExtDeliverTo.Substring(0, 26);
        //                    }
        //                    htmlPageOne = htmlPageOne + "<div style='height: 33px;white-space: nowrap;'>" + item.ExtDeliverTo + " </div>";
        //                    if (!string.IsNullOrEmpty(item.Range) && item.Range.Length > 26)
        //                    {
        //                        item.Range = item.Range.Substring(0, 26);
        //                    }
        //                    htmlPageOne = htmlPageOne + "<div style='height: 29px;white-space: nowrap;'>" + item.Range + "  </div>";
        //                    htmlPageOne = htmlPageOne + @"</div>       
        //                </div>            
        //      <div style='border-style: solid;padding: 5px 5px 5px 5px;margin-bottom: 5px;border-width: thin;'>
        //       <div>1. 본 주문서를 수령한 거래선은 수출품에 사용될 자재인 만큼 신뢰를 바탕으로 최선을 다해 양질의 제품 생산과 납품기일을 엄수하여 주십시오.</div>  
        //       <div>2. 납품시 납품물량에 불량, 부족량 등 미비한 점이 있을 경우 내지 납품불이행 및 납기 지연시 대체교환과 손해배상을 하여야 합니다.</div>
        //       <div>3. 기타 본 주문서에 의문사항이 있을 경우 당사에 즉시 문의 확인하여 주시기 바랍니다.</div>      
        //       <div >";
        //                    htmlPageOne = htmlPageOne + item.Notice + "</div>";
        //                    htmlPageOne = htmlPageOne + @"</div>    
        //      <table style='border-collapse: collapse;width: 100%;font-size: 26px;'>";

        //                    if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
        //                    {

        //                        htmlPageOne = htmlPageOne + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
        //                      <th style='width: 12%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재번호</th>
        //                      <th style='width: 73%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재명 // 생산자 자재번호[색상]</th>
        //                      <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>수량</th>   
        //                      <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
        //                    </tr>";
        //                    }
        //                    if (item.PRINTTYPE == "2")
        //                    {
        //                        htmlPageOne = htmlPageOne + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
        //                      <th style='width: 17%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재번호/CM#(Coach)</th>
        //                      <th style='width: 68%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재명 // 생산자 자재번호[색상]</th>
        //                      <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>수량</th>   
        //                      <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
        //                    </tr>";
        //                    }


        //                    string detailpageone = "";

        //                    List<TMatPOItem_Custom> ListChildrentPageOne = itemchildrent.Where(x => x.RowGRVendor <= 21 && x.Vendor == Vendor.Vendor).OrderBy(x => x.RowGRVendor).ToList();

        //                    decimal pdfpagesize = Math.Ceiling((decimal)(itemchildrent.Where(x => x.Vendor == Vendor.Vendor).Count() - 21) / 27);

        //                    foreach (TMatPOItem_Custom ob in ListChildrentPageOne)
        //                    {
        //                        if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
        //                        {
        //                            detailpageone = detailpageone + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatCD + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
        //                        }
        //                        if (item.PRINTTYPE == "2")
        //                        {
        //                            detailpageone = detailpageone + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 10px;'> " + ob.MatCD + " <br> " + ob.CustItemNo1 + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
        //                        }
        //                    }
        //                    htmlPageOne = htmlPageOne + detailpageone + "</table></div><div style='width: 1650px;margin: 0px 30px 0px 30px;height: 6px;background-color: black;'></div><div style='font-weight: bold;text-align: center; margin-bottom: 5px;font-size: 25px;'>" + pagesizeCurrent.ToString() + " / " + (pagesizeAllVendor).ToString() + "</div></body></html>";


        //                    if (doc == null)
        //                    {
        //                        doc = converter.ConvertHtmlString(htmlPageOne, baseUrl);
        //                    }
        //                    else if (doc != null)
        //                    {
        //                        SelectPdf.PdfDocument docVendor = converter.ConvertHtmlString(htmlPageOne, baseUrl);
        //                        doc.Append(docVendor);
        //                    }



        //                    if (pdfpagesize > 0)
        //                    {
        //                        for (int i = 1; i <= pdfpagesize; i++)
        //                        {
        //                            pagesizeCurrent += 1;
        //                            HtmlToPdf convertertwo = new HtmlToPdf();

        //                            string htmlPageTwo = @"
        //                            <html>

        //                            <style>
        //                              tr td {height:70px;}
        //                            </style>
        //                             <body>
        //                                <div id='print_section' style='width: 1650px;height: 2350px; padding: 0px 30px 0px 30px;font-family: sans-serif; font-size: 26px;'>
        //                                  <br>
        //                                  <br>
        //                                  <br>
        //                                  <br>  
        //                                  <table style='border-collapse: collapse;width: 100%;font-size: 26px;'>";

        //                            if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
        //                            {
        //                                htmlPageTwo = htmlPageTwo + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
        //                              <th style='width: 12%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재번호</th>
        //                              <th style='width: 73%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재명 // 생산자 자재번호[색상]</th>
        //                              <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>수량</th>   
        //                              <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
        //                            </tr>
        //                            ";
        //                            }
        //                            if (item.PRINTTYPE == "2")
        //                            {
        //                                htmlPageTwo = htmlPageTwo + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
        //                              <th style='width: 17%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재번호/CM#(Coach)</th>
        //                              <th style='width: 68%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재명 // 생산자 자재번호[색상]</th>
        //                              <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>수량</th>   
        //                              <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
        //                            </tr>";
        //                            }
        //                            List<TMatPOItem_Custom> ListChildrentPagetwo = itemchildrent.Where(x => x.Vendor == Vendor.Vendor && x.RowGRVendor > 21 && x.RowGRVendor <= ((i * 27) + 21)).OrderBy(x => x.RowGRVendor).Skip(((i - 1) * 27)).Take(27).ToList();
        //                            string detailpagetwo = "";

        //                            foreach (TMatPOItem_Custom ob in ListChildrentPagetwo)
        //                            {
        //                                if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
        //                                {
        //                                    detailpagetwo = detailpagetwo + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatCD + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
        //                                }
        //                                if (item.PRINTTYPE == "2")
        //                                {
        //                                    detailpagetwo = detailpagetwo + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 10px;'> " + ob.MatCD + " <br> " + ob.CustItemNo1 + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
        //                                }


        //                            }
        //                            htmlPageTwo = htmlPageTwo + detailpagetwo + "</table></div><div style='width: 1650px;margin: 0px 30px 0px 30px;height: 6px;background-color: black;'></div><div style='font-weight: bold;text-align: center; margin-bottom: 5px;font-size: 25px;'>" + pagesizeCurrent.ToString() + " / " + (pagesizeAllVendor).ToString() + " </ div></body></html>";

        //                            convertertwo.Options.PdfPageSize = pageSize;
        //                            convertertwo.Options.WebPageWidth = WebPageWidth;

        //                            SelectPdf.PdfDocument doctwo = convertertwo.ConvertHtmlString(htmlPageTwo, baseUrl);
        //                            doc.Append(doctwo);


        //                        }
        //                    }

        //                }
        //                return doc.Save();
        //            }
        //            catch (Exception ex)
        //            {
        //                throw new NotImplementedException(ex.Message.ToString());
        //            }
        //        }
        public Byte[] ERP_TMatPO_IssueSelectedPO(TMatPO_Custom item,string database,string Userlogin, string company)
        {
            try
            {
                using (var connection = new SqlConnection(database))
                {
                    connection.Open();


                    if (string.IsNullOrEmpty(item.PostingDateReportTiengHanConvert))
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@MatPOCD", item.MatPOCD);
                        queryParameters.Add("@UpdatedBy", Userlogin);
                        TMatPO_Custom data = connection.QueryFirstOrDefault<TMatPO_Custom>("UPDATE dbo.TMatPO SET PostingDate=convert(varchar, getdate(), 23),UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE MatPOCD=@MatPOCD;SELECT CONVERT(NVARCHAR(10),YEAR(getdate()))+N'년 ' +CONVERT(NVARCHAR(10),MONTH(getdate()))+N'월 '+  +CONVERT(NVARCHAR(10),DAY(getdate()))+ N'일' PostingDateReportTiengHanConvert,REPLACE(REPLACE(CONVERT(varchar, getdate(), 107),', ',' '),' ',', ') PostingDateReportConvert "
                            , queryParameters, commandTimeout: 1500
                            , commandType: CommandType.Text);

                        item.PostingDateReportTiengHanConvert = data.PostingDateReportTiengHanConvert;
                        item.PostingDateReportConvert = data.PostingDateReportConvert;
                    }



                    var Parameterschildrent = new DynamicParameters();
                    Parameterschildrent.Add("@MatPOCD", item.MatPOCD);
                    Parameterschildrent.Add("@Type", 0);
                    Parameterschildrent.Add("@PageNumber", 1);
                    Parameterschildrent.Add("@PageSize", 100);
                    var datachildrent = connection.Query<TMatPOItem_Custom>("ERP_TMatPOItem_GetByMatPOCD"
                        , Parameterschildrent, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);

                    if (item.PGrpCD != "MKR")
                    {
                        return this.ERP_TMatPO_IssueReturnPDFNotExist_MKR(item, datachildrent.ToList(), company);
                    }
                    return this.ERP_TMatPO_IssueReturnPDF_MKR(item, datachildrent.ToList());



                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }


            //try
            //{
            //    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            //    {
            //        connection.Open();


            //        if (string.IsNullOrEmpty(item.PostingDateReportTiengHanConvert))
            //        {
            //            var queryParameters = new DynamicParameters();
            //            queryParameters.Add("@MatPOCD", item.MatPOCD);
            //            queryParameters.Add("@UpdatedBy", GetUserlogin());
            //            TMatPO_Custom data = connection.QueryFirstOrDefault<TMatPO_Custom>("UPDATE dbo.TMatPO SET PostingDate=convert(varchar, getdate(), 23),UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE MatPOCD=@MatPOCD;SELECT CONVERT(NVARCHAR(10),YEAR(getdate()))+N'년 ' +CONVERT(NVARCHAR(10),MONTH(getdate()))+N'월 '+  +CONVERT(NVARCHAR(10),DAY(getdate()))+ N'일' PostingDateReportTiengHanConvert,REPLACE(REPLACE(CONVERT(varchar, getdate(), 107),', ',' '),' ',', ') PostingDateReportConvert "
            //                , queryParameters, commandTimeout: 1500
            //                , commandType: CommandType.Text);

            //            item.PostingDateReportTiengHanConvert = data.PostingDateReportTiengHanConvert;
            //            item.PostingDateReportConvert = data.PostingDateReportConvert;
            //        }



            //        var Parameterschildrent = new DynamicParameters();
            //        Parameterschildrent.Add("@MatPOCD", item.MatPOCD);
            //        Parameterschildrent.Add("@Type", 0);
            //        Parameterschildrent.Add("@PageNumber", 1);
            //        Parameterschildrent.Add("@PageSize", 100);
            //        var datachildrent = connection.Query<TMatPOItem_Custom>("ERP_TMatPOItem_GetByMatPOCD"
            //            , Parameterschildrent, commandTimeout: 1500
            //            , commandType: CommandType.StoredProcedure);

            //        if (item.PGrpCD!="MKR")
            //        {
            //            return this.ERP_TMatPO_IssueReturnPDFNotExist_MKR(item, datachildrent.ToList());
            //        }
            //        return this.ERP_TMatPO_IssueReturnPDF_MKR(item, datachildrent.ToList());



            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw new NotImplementedException(ex.Message.ToString());
            //}
        }
        public async Task<TMatPO_Custom> ERP_TMatPOItem_GeneratePOWithSelected(dynamic param, string database, string key
                , PARKERPEntities Context, string Userlogin, string valued)
            {

                //đối MKR thì 1 Vendor 1 MPO còn lại thì 1 shipper 1 vendor
                List<TPRItem_Custom> itemlist = new List<TPRItem_Custom>();
                using (var connection = new SqlConnection(database))
                {

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRItemID", param.PRItemID.Value);
                    queryParameters.Add("@POStatus", param.POStatus.Value);
                    queryParameters.Add("@PRID", param.PRID.Value);
                    queryParameters.Add("@VerityVendor", param.VerityVendor.Value);

                    queryParameters.Add("@SortColumn", "");
                    queryParameters.Add("@SortOrder", "");
                    queryParameters.Add("@MatCDList", param.MatCDList.Value);
                    queryParameters.Add("@VendorCD", param.VendorCD.Value);
                    queryParameters.Add("@Shipper", param.Shipper.Value);
                    queryParameters.Add("@PGrpCD", param.PGrpCD.Value);

                    queryParameters.Add("@Type", param.Type.Value);
                    queryParameters.Add("@PageNumber", param.PageNumber.Value);
                    queryParameters.Add("@PageSize", param.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<TPRItem_Custom>("ERP_TPRItem_GetMaterials"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);


                    if (string.IsNullOrEmpty(valued))
                    {
                        itemlist = data.ToList();
                    }
                    if (!string.IsNullOrEmpty(valued))
                    {
                        List<string> result = valued.Trim().Split(' ').ToList();

                        itemlist = (from c in data
                                    join ct in result
                                    on c.PRItemID equals int.Parse(ct) into g
                                    from ct in g.DefaultIfEmpty()
                                        //where !string.IsNullOrEmpty(ct) 
                                    select new TPRItem_Custom()
                                    {
                                        TotalRows = c.TotalRows,
                                        RowID = c.RowID,
                                        expand = c.expand,
                                        Sel = ct == null ? false : true,
                                        PO = c.PO,
                                        AltMat_SmallInt = c.AltMat_SmallInt,
                                        AltMat_Bit = c.AltMat_Bit,
                                        AltMat = c.AltMat,
                                        NG = c.NG,
                                        MatCD = c.MatCD,
                                        MatDesc = c.MatDesc,
                                        Color = c.Color,
                                        MfgVendor = c.MfgVendor,
                                        Shipper = c.Shipper,
                                        PGrp = c.PGrp,
                                        TBPQtyFloat = c.TBPQtyFloat,
                                        TBPQty = c.TBPQty,
                                        CustItemNo1 = c.CustItemNo1,
                                        GR = c.GR,
                                        MultiSrc = c.MultiSrc,
                                        PLotSize = c.PLotSize,
                                        PLeadtime = c.PLeadtime,
                                        MOQ = c.MOQ,
                                        MOQEnabled = c.MOQEnabled,
                                        LossRate = c.LossRate,
                                        LossQty = c.LossQty,
                                        AddQty = c.AddQty,
                                        LeftOverQty = c.LeftOverQty,
                                        StdQty = c.StdQty,
                                        ActQty = c.ActQty,
                                        PUnit = c.PUnit,
                                        POQty = c.POQty,
                                        POCnt = c.POCnt,
                                        YUnit = c.YUnit,
                                        YQty = c.YQty,
                                        Remark = c.Remark,
                                        MHID = c.MHID,
                                        MH1 = c.MH1,
                                        RefVKNo = c.RefVKNo,
                                        PRItemID = c.PRItemID,
                                        PRID = c.PRID,
                                        NotReady = c.NotReady,
                                        SumAltMat = c.SumAltMat,
                                        MOQNum = c.MOQNum,
                                        Applied = c.Applied
                                    }).ToList();

                    }

                    itemlist = itemlist.Where(x => x.Sel && x.TBPQtyFloat > 0).ToList();

                    if (!itemlist.Any())
                    {
                        throw new NotImplementedException("There is no transaction performed or </BR> no record with transaction </BR> will be performed.");
                    }


                    DateTime today = DateTime.Now;
                    TMatPO parentTemp = new TMatPO();
                    var MCM_GroupShipper = itemlist.Where(x => x.PGrp == "MCM" && !string.IsNullOrEmpty(x.Shipper)).GroupBy(g => g.Shipper).Select(g => new { Shipper = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();
                    var MFR_GroupShipper = itemlist.Where(x => x.PGrp == "MFR" && !string.IsNullOrEmpty(x.Shipper)).GroupBy(g => g.Shipper).Select(g => new { Shipper = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();
                    var MVN_GroupShipper = itemlist.Where(x => x.PGrp == "MVN" && !string.IsNullOrEmpty(x.Shipper)).GroupBy(g => g.Shipper).Select(g => new { Shipper = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();
                    var MBD_GroupShipper = itemlist.Where(x => x.PGrp == "MBD" && !string.IsNullOrEmpty(x.Shipper)).GroupBy(g => g.Shipper).Select(g => new { Shipper = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();
                    var MKH_GroupShipper = itemlist.Where(x => x.PGrp == "MKH" && !string.IsNullOrEmpty(x.Shipper)).GroupBy(g => g.Shipper).Select(g => new { Shipper = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();
                    var MKR_GroupMfgVendor = itemlist.Where(x => x.PGrp == "MKR" && !string.IsNullOrEmpty(x.MfgVendor)).GroupBy(g => g.MfgVendor).Select(g => new { MfgVendor = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();

                    using (var transaction = Context.Database.BeginTransaction())
                    {
                        try
                        {
                            foreach (dynamic ob in MCM_GroupShipper)//tách theo Shipper MCM
                            {
                                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@Shipper";
                                var queryParametersInforPO = new DynamicParameters();
                                queryParametersInforPO.Add("@Shipper", ob.Shipper);
                                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
                                    , queryParametersInforPO, commandTimeout: 1500
                                    , commandType: CommandType.Text);

                                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD(database);

                                parent.RefVKNo = ob.RefVKNo;
                                parent.Shipper = ob.Shipper;

                                if (tvendor.POByMfgVendor)
                                {
                                    parent.MfgVendor = ob.Shipper;
                                }

                                parent.PRID = ob.PRID;
                                parent.PGrpCD = "MCM";
                                parent.ReqETD = DateTime.Parse("1900-01-01");
                                parent.Currency = tvendor.CurrCD;
                                parent.IncotermsCD = tvendor.IncotermsCD;
                                parent.Range = "";
                                parent.AttentionTo = tvendor.TPGrp_ContactTo;
                                parent.Manual = false;
                                parent.Closed = false;
                                parent.Canceled = false;
                                parent.CreatedBy = int.Parse(Userlogin);
                                parent.CreatedOn = today;
                                Context.TMatPOes.Add(parent);
                                parentTemp = parent;

                                var childrentList = itemlist.Where(x => x.PGrp == "MCM" && x.Shipper == ob.Shipper).ToList();

                                foreach (TPRItem_Custom item in childrentList)
                                {

                                    TMatPOItem childrent = new TMatPOItem();
                                    childrent.MatPOCD = parent.MatPOCD;
                                    childrent.Vendor = item.MfgVendor;
                                    childrent.MatCD = item.MatCD;
                                    // childrent.VendorItemNo = item.VendorItemNo;
                                    childrent.YQty = item.YQty;
                                    childrent.PUnit = item.PUnit;
                                    childrent.POQty = item.TBPQtyFloat;
                                    childrent.CreatedBy = int.Parse(Userlogin);
                                    childrent.CreatedOn = today;
                                    childrent.PIDiffCfm = false;
                                    childrent.Shipper = item.Shipper;
                                    childrent.BenefitQty = item.ActQty.Value;
                                    childrent.ConsolQty = item.ActQty.Value;
                                    childrent.PaidOn = false;
                                    childrent.GROn = false;
                                    Context.TMatPOItems.Add(childrent);
                                    await Context.SaveChangesAsync();


                                }
                            }
                            foreach (dynamic ob in MFR_GroupShipper)//tách theo Shipper MFR
                            {
                                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@Shipper";
                                var queryParametersInforPO = new DynamicParameters();
                                queryParametersInforPO.Add("@Shipper", ob.Shipper);
                                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
                                    , queryParametersInforPO, commandTimeout: 1500
                                    , commandType: CommandType.Text);

                                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD(database);

                                parent.RefVKNo = ob.RefVKNo;
                                parent.Shipper = ob.Shipper;

                                if (tvendor.POByMfgVendor)
                                {
                                    parent.MfgVendor = ob.Shipper;
                                }

                                parent.PRID = ob.PRID;
                                parent.PGrpCD = "MFR";
                                parent.ReqETD = DateTime.Parse("1900-01-01");
                                parent.Currency = tvendor.CurrCD;
                                parent.IncotermsCD = tvendor.IncotermsCD;
                                parent.Range = "";
                                parent.AttentionTo = tvendor.TPGrp_ContactTo;
                                parent.Manual = false;
                                parent.Closed = false;
                                parent.Canceled = false;
                                parent.CreatedBy = int.Parse(Userlogin);
                                parent.CreatedOn = today;
                                Context.TMatPOes.Add(parent);
                                parentTemp = parent;

                                var childrentList = itemlist.Where(x => x.PGrp == "MFR" && x.Shipper == ob.Shipper).ToList();

                                foreach (TPRItem_Custom item in childrentList)
                                {

                                    TMatPOItem childrent = new TMatPOItem();
                                    childrent.MatPOCD = parent.MatPOCD;
                                    childrent.Vendor = item.MfgVendor;
                                    childrent.MatCD = item.MatCD;
                                    // childrent.VendorItemNo = item.VendorItemNo;
                                    childrent.YQty = item.YQty;
                                    childrent.PUnit = item.PUnit;
                                    childrent.POQty = item.TBPQtyFloat;
                                    childrent.CreatedBy = int.Parse(Userlogin);
                                    childrent.CreatedOn = today;
                                    childrent.PIDiffCfm = false;
                                    childrent.Shipper = item.Shipper;
                                    childrent.BenefitQty = item.ActQty.Value;
                                    childrent.ConsolQty = item.ActQty.Value;
                                    childrent.PaidOn = false;
                                    childrent.GROn = false;
                                    Context.TMatPOItems.Add(childrent);
                                    await Context.SaveChangesAsync();


                                }
                            }
                            foreach (dynamic ob in MVN_GroupShipper)//tách theo Shipper MVN
                            {
                                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@Shipper";
                                var queryParametersInforPO = new DynamicParameters();
                                queryParametersInforPO.Add("@Shipper", ob.Shipper);
                                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
                                    , queryParametersInforPO, commandTimeout: 1500
                                    , commandType: CommandType.Text);

                                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD(database);

                                parent.RefVKNo = ob.RefVKNo;
                                parent.Shipper = ob.Shipper;

                                if (tvendor.POByMfgVendor)
                                {
                                    parent.MfgVendor = ob.Shipper;
                                }

                                parent.PRID = ob.PRID;
                                parent.PGrpCD = "MVN";
                                parent.ReqETD = DateTime.Parse("1900-01-01");
                                parent.Currency = tvendor.CurrCD;
                                parent.IncotermsCD = tvendor.IncotermsCD;
                                parent.Range = "";
                                parent.AttentionTo = tvendor.TPGrp_ContactTo;
                                parent.Manual = false;
                                parent.Closed = false;
                                parent.Canceled = false;
                                parent.CreatedBy = int.Parse(Userlogin);
                                parent.CreatedOn = today;
                                Context.TMatPOes.Add(parent);
                                parentTemp = parent;

                                var childrentList = itemlist.Where(x => x.PGrp == "MVN" && x.Shipper == ob.Shipper).ToList();

                                foreach (TPRItem_Custom item in childrentList)
                                {

                                    TMatPOItem childrent = new TMatPOItem();
                                    childrent.MatPOCD = parent.MatPOCD;
                                    childrent.Vendor = item.MfgVendor;
                                    childrent.MatCD = item.MatCD;
                                    // childrent.VendorItemNo = item.VendorItemNo;
                                    childrent.YQty = item.YQty;
                                    childrent.PUnit = item.PUnit;
                                    childrent.POQty = item.TBPQtyFloat;
                                    childrent.CreatedBy = int.Parse(Userlogin);
                                    childrent.CreatedOn = today;
                                    childrent.PIDiffCfm = false;
                                    childrent.Shipper = item.Shipper;
                                    childrent.BenefitQty = item.ActQty.Value;
                                    childrent.ConsolQty = item.ActQty.Value;
                                    childrent.PaidOn = false;
                                    childrent.GROn = false;
                                    Context.TMatPOItems.Add(childrent);
                                    await Context.SaveChangesAsync();


                                }
                            }
                            foreach (dynamic ob in MBD_GroupShipper)//tách theo Shipper MBD
                            {
                                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@Shipper";
                                var queryParametersInforPO = new DynamicParameters();
                                queryParametersInforPO.Add("@Shipper", ob.Shipper);
                                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
                                    , queryParametersInforPO, commandTimeout: 1500
                                    , commandType: CommandType.Text);

                                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD(database);

                                parent.RefVKNo = ob.RefVKNo;
                                parent.Shipper = ob.Shipper;

                                if (tvendor.POByMfgVendor)
                                {
                                    parent.MfgVendor = ob.Shipper;
                                }

                                parent.PRID = ob.PRID;
                                parent.PGrpCD = "MBD";
                                parent.ReqETD = DateTime.Parse("1900-01-01");
                                parent.Currency = tvendor.CurrCD;
                                parent.IncotermsCD = tvendor.IncotermsCD;
                                parent.Range = "";
                                parent.AttentionTo = tvendor.TPGrp_ContactTo;
                                parent.Manual = false;
                                parent.Closed = false;
                                parent.Canceled = false;
                                parent.CreatedBy = int.Parse(Userlogin);
                                parent.CreatedOn = today;
                                Context.TMatPOes.Add(parent);
                                parentTemp = parent;

                                var childrentList = itemlist.Where(x => x.PGrp == "MBD" && x.Shipper == ob.Shipper).ToList();

                                foreach (TPRItem_Custom item in childrentList)
                                {

                                    TMatPOItem childrent = new TMatPOItem();
                                    childrent.MatPOCD = parent.MatPOCD;
                                    childrent.Vendor = item.MfgVendor;
                                    childrent.MatCD = item.MatCD;
                                    // childrent.VendorItemNo = item.VendorItemNo;
                                    childrent.YQty = item.YQty;
                                    childrent.PUnit = item.PUnit;
                                    childrent.POQty = item.TBPQtyFloat;
                                    childrent.CreatedBy = int.Parse(Userlogin);
                                    childrent.CreatedOn = today;
                                    childrent.PIDiffCfm = false;
                                    childrent.Shipper = item.Shipper;
                                    childrent.BenefitQty = item.ActQty.Value;
                                    childrent.ConsolQty = item.ActQty.Value;
                                    childrent.PaidOn = false;
                                    childrent.GROn = false;
                                    Context.TMatPOItems.Add(childrent);
                                    await Context.SaveChangesAsync();


                                }
                            }
                            foreach (dynamic ob in MKH_GroupShipper)//tách theo Shipper MKH
                            {
                                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@Shipper";
                                var queryParametersInforPO = new DynamicParameters();
                                queryParametersInforPO.Add("@Shipper", ob.Shipper);
                                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
                                    , queryParametersInforPO, commandTimeout: 1500
                                    , commandType: CommandType.Text);

                                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD(database);

                                parent.RefVKNo = ob.RefVKNo;
                                parent.Shipper = ob.Shipper;

                                if (tvendor.POByMfgVendor)
                                {
                                    parent.MfgVendor = ob.Shipper;
                                }

                                parent.PRID = ob.PRID;
                                parent.PGrpCD = "MKH";
                                parent.ReqETD = DateTime.Parse("1900-01-01");
                                parent.Currency = tvendor.CurrCD;
                                parent.IncotermsCD = tvendor.IncotermsCD;
                                parent.Range = "";
                                parent.AttentionTo = tvendor.TPGrp_ContactTo;
                                parent.Manual = false;
                                parent.Closed = false;
                                parent.Canceled = false;
                                parent.CreatedBy = int.Parse(Userlogin);
                                parent.CreatedOn = today;
                                Context.TMatPOes.Add(parent);
                                parentTemp = parent;

                                var childrentList = itemlist.Where(x => x.PGrp == "MKH" && x.Shipper == ob.Shipper).ToList();

                                foreach (TPRItem_Custom item in childrentList)
                                {

                                    TMatPOItem childrent = new TMatPOItem();
                                    childrent.MatPOCD = parent.MatPOCD;
                                    childrent.Vendor = item.MfgVendor;
                                    childrent.MatCD = item.MatCD;
                                    // childrent.VendorItemNo = item.VendorItemNo;
                                    childrent.YQty = item.YQty;
                                    childrent.PUnit = item.PUnit;
                                    childrent.POQty = item.TBPQtyFloat;
                                    childrent.CreatedBy = int.Parse(Userlogin);
                                    childrent.CreatedOn = today;
                                    childrent.PIDiffCfm = false;
                                    childrent.Shipper = item.Shipper;
                                    childrent.BenefitQty = item.ActQty.Value;
                                    childrent.ConsolQty = item.ActQty.Value;
                                    childrent.PaidOn = false;
                                    childrent.GROn = false;
                                    Context.TMatPOItems.Add(childrent);
                                    await Context.SaveChangesAsync();


                                }
                            }

                            foreach (dynamic ob in MKR_GroupMfgVendor)//tách theo vendor MKR
                            {
                                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@MfgVendor";
                                var queryParametersInforPO = new DynamicParameters();
                                queryParametersInforPO.Add("@MfgVendor", ob.MfgVendor);
                                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
                                    , queryParametersInforPO, commandTimeout: 1500
                                    , commandType: CommandType.Text);

                                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD(database);

                                parent.RefVKNo = ob.RefVKNo;
                                parent.Shipper = tvendor.Shipper;
                                parent.MfgVendor = ob.MfgVendor;
                                //if (tvendor.POByMfgVendor)
                                //{
                                //    parent.MfgVendor = ob.Shipper;
                                //}

                                parent.PRID = ob.PRID;
                                parent.PGrpCD = "MKR";// tvendor.PGrpCD;
                                parent.ReqETD = DateTime.Parse("1900-01-01");
                                parent.Currency = tvendor.CurrCD;
                                parent.IncotermsCD = tvendor.IncotermsCD;
                                parent.Range = "";
                                parent.AttentionTo = tvendor.TPGrp_ContactTo;
                                parent.Manual = false;
                                parent.Closed = false;
                                parent.Canceled = false;
                                parent.CreatedBy = int.Parse(Userlogin);
                                parent.CreatedOn = today;
                                Context.TMatPOes.Add(parent);
                                parentTemp = parent;

                                var childrentList = itemlist.Where(x => x.PGrp == "MKR" && x.MfgVendor == ob.MfgVendor).ToList();

                                foreach (TPRItem_Custom item in childrentList)
                                {

                                    TMatPOItem childrent = new TMatPOItem();
                                    childrent.MatPOCD = parent.MatPOCD;
                                    childrent.Vendor = item.MfgVendor;
                                    childrent.MatCD = item.MatCD;
                                    // childrent.VendorItemNo = item.VendorItemNo;
                                    childrent.YQty = item.YQty;
                                    childrent.PUnit = item.PUnit;
                                    childrent.POQty = item.TBPQtyFloat;
                                    childrent.CreatedBy = int.Parse(Userlogin);
                                    childrent.CreatedOn = today;
                                    childrent.PIDiffCfm = false;
                                    childrent.Shipper = item.Shipper;
                                    childrent.BenefitQty = item.ActQty.Value;
                                    childrent.ConsolQty = item.ActQty.Value;
                                    childrent.PaidOn = false;
                                    childrent.GROn = false;
                                    Context.TMatPOItems.Add(childrent);
                                    await Context.SaveChangesAsync();

                                }
                            }
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new NotImplementedException(ex.Message.ToString());
                        }
                    }
                    string PRID_Custom = "R" + parentTemp.PRID.Value.ToString("0000000");
                    return await Task.FromResult(new TMatPO_Custom { PRID_Custom = PRID_Custom, CreatedOn = parentTemp.CreatedOn, PRID = parentTemp.PRID });

                }



            ////đối MKR thì 1 Vendor 1 MPO còn lại thì 1 shipper 1 vendor
            //List<TPRItem_Custom> itemlist = new List<TPRItem_Custom>();
            //using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            //{

            //    var queryParameters = new DynamicParameters();
            //    queryParameters.Add("@PRItemID", param.PRItemID.Value);
            //    queryParameters.Add("@POStatus", param.POStatus.Value);
            //    queryParameters.Add("@PRID", param.PRID.Value);
            //    queryParameters.Add("@VerityVendor", param.VerityVendor.Value);

            //    queryParameters.Add("@SortColumn", "");
            //    queryParameters.Add("@SortOrder", "");
            //    queryParameters.Add("@MatCDList", param.MatCDList.Value);
            //    queryParameters.Add("@VendorCD", param.VendorCD.Value);
            //    queryParameters.Add("@Shipper", param.Shipper.Value);
            //    queryParameters.Add("@PGrpCD", param.PGrpCD.Value);

            //    queryParameters.Add("@Type", param.Type.Value);
            //    queryParameters.Add("@PageNumber", param.PageNumber.Value);
            //    queryParameters.Add("@PageSize", param.PageSize.Value);
            //    connection.Open();
            //    var data = await connection.QueryAsync<TPRItem_Custom>("ERP_TPRItem_GetMaterials"
            //        , queryParameters, commandTimeout: 1500
            //        , commandType: CommandType.StoredProcedure);

            //    IDatabase db = ConnectRedis();
            //    string key = GetDB() + "_" + GetUserlogin() + "_TPRItem_Radis_Sel" + param.PRID.Value;
            //    string valued = await db.StringGetAsync(key);
            //    if (string.IsNullOrEmpty(valued))
            //    {
            //        itemlist = data.ToList();
            //    }
            //    if (!string.IsNullOrEmpty(valued))
            //    {
            //        List<string> result = valued.Trim().Split(' ').ToList();

            //        itemlist = (from c in data
            //                     join ct in result
            //                     on c.PRItemID equals int.Parse(ct) into g
            //                     from ct in g.DefaultIfEmpty()
            //                         //where !string.IsNullOrEmpty(ct) 
            //                     select new TPRItem_Custom()
            //                     {
            //                         TotalRows = c.TotalRows,
            //                         RowID = c.RowID,
            //                         expand = c.expand,
            //                         Sel = ct == null ? false : true,
            //                         PO = c.PO,
            //                         AltMat_SmallInt = c.AltMat_SmallInt,
            //                         AltMat_Bit = c.AltMat_Bit,
            //                         AltMat = c.AltMat,
            //                         NG = c.NG,
            //                         MatCD = c.MatCD,
            //                         MatDesc = c.MatDesc,
            //                         Color = c.Color,
            //                         MfgVendor = c.MfgVendor,
            //                         Shipper = c.Shipper,
            //                         PGrp = c.PGrp,
            //                         TBPQtyFloat=c.TBPQtyFloat,
            //                         TBPQty = c.TBPQty,
            //                         CustItemNo1 = c.CustItemNo1,
            //                         GR = c.GR,
            //                         MultiSrc = c.MultiSrc,
            //                         PLotSize = c.PLotSize,
            //                         PLeadtime = c.PLeadtime,
            //                         MOQ = c.MOQ,
            //                         MOQEnabled = c.MOQEnabled,
            //                         LossRate = c.LossRate,
            //                         LossQty = c.LossQty,
            //                         AddQty = c.AddQty,
            //                         LeftOverQty = c.LeftOverQty,
            //                         StdQty = c.StdQty,
            //                         ActQty = c.ActQty,
            //                         PUnit = c.PUnit,
            //                         POQty = c.POQty,
            //                         POCnt = c.POCnt,
            //                         YUnit = c.YUnit,
            //                         YQty = c.YQty,
            //                         Remark = c.Remark,
            //                         MHID = c.MHID,
            //                         MH1 = c.MH1,
            //                         RefVKNo = c.RefVKNo,
            //                         PRItemID = c.PRItemID,
            //                         PRID = c.PRID,
            //                         NotReady = c.NotReady,
            //                         SumAltMat = c.SumAltMat,
            //                         MOQNum = c.MOQNum,
            //                         Applied = c.Applied
            //                     }).ToList();

            //    }

            //    itemlist = itemlist.Where(x => x.Sel && x.TBPQtyFloat > 0).ToList();

            //    if (!itemlist.Any())
            //    {
            //        throw new NotImplementedException("There is no transaction performed or </BR> no record with transaction </BR> will be performed.");
            //    }


            //    DateTime today= DateTime.Now;
            //    TMatPO parentTemp = new TMatPO();
            //    var MCM_GroupShipper = itemlist.Where(x => x.PGrp == "MCM" && !string.IsNullOrEmpty(x.Shipper)).GroupBy(g => g.Shipper).Select(g => new { Shipper = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();
            //    var MFR_GroupShipper = itemlist.Where(x => x.PGrp == "MFR" && !string.IsNullOrEmpty(x.Shipper)).GroupBy(g => g.Shipper).Select(g => new { Shipper = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();
            //    var MVN_GroupShipper = itemlist.Where(x=>x.PGrp == "MVN" && !string.IsNullOrEmpty(x.Shipper)).GroupBy(g => g.Shipper).Select(g => new { Shipper = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();
            //    var MBD_GroupShipper = itemlist.Where(x => x.PGrp == "MBD" && !string.IsNullOrEmpty(x.Shipper)).GroupBy(g => g.Shipper).Select(g => new { Shipper = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();
            //    var MKH_GroupShipper = itemlist.Where(x => x.PGrp == "MKH" && !string.IsNullOrEmpty(x.Shipper)).GroupBy(g => g.Shipper).Select(g => new { Shipper = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();
            //    var MKR_GroupMfgVendor = itemlist.Where(x => x.PGrp == "MKR" && !string.IsNullOrEmpty(x.MfgVendor)).GroupBy(g => g.MfgVendor).Select(g => new { MfgVendor = g.Key, RefVKNo = g.ToList().Max(d => d.RefVKNo), PRID = g.ToList().Max(d => d.PRID) }).ToList();

            //    using (var transaction = this.Context.Database.BeginTransaction())
            //    {
            //        try
            //        {
            //            foreach (dynamic ob in MCM_GroupShipper)//tách theo Shipper MCM
            //            {
            //                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@Shipper";
            //                var queryParametersInforPO = new DynamicParameters();
            //                queryParametersInforPO.Add("@Shipper", ob.Shipper);
            //                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
            //                    , queryParametersInforPO, commandTimeout: 1500
            //                    , commandType: CommandType.Text);

            //                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD();

            //                parent.RefVKNo = ob.RefVKNo;
            //                parent.Shipper = ob.Shipper;

            //                if (tvendor.POByMfgVendor)
            //                {
            //                    parent.MfgVendor = ob.Shipper;
            //                }

            //                parent.PRID = ob.PRID;
            //                parent.PGrpCD = "MCM";
            //                parent.ReqETD = DateTime.Parse("1900-01-01");
            //                parent.Currency = tvendor.CurrCD;
            //                parent.IncotermsCD = tvendor.IncotermsCD;
            //                parent.Range = "";
            //                parent.AttentionTo = tvendor.TPGrp_ContactTo;
            //                parent.Manual = false;
            //                parent.Closed = false;
            //                parent.Canceled = false;
            //                parent.CreatedBy = int.Parse(GetUserlogin());
            //                parent.CreatedOn = today;
            //                this.Context.TMatPOes.Add(parent);
            //                parentTemp = parent;

            //                var childrentList = itemlist.Where(x => x.PGrp == "MCM" && x.Shipper == ob.Shipper).ToList();

            //                foreach (TPRItem_Custom item in childrentList)
            //                {

            //                    TMatPOItem childrent = new TMatPOItem();
            //                    childrent.MatPOCD = parent.MatPOCD;
            //                    childrent.Vendor = item.MfgVendor;
            //                    childrent.MatCD = item.MatCD;
            //                    // childrent.VendorItemNo = item.VendorItemNo;
            //                    childrent.YQty = item.YQty;
            //                    childrent.PUnit = item.PUnit;
            //                    childrent.POQty = item.TBPQtyFloat;
            //                    childrent.CreatedBy = int.Parse(GetUserlogin());
            //                    childrent.CreatedOn = today;
            //                    childrent.PIDiffCfm = false;
            //                    childrent.Shipper = item.Shipper;
            //                    childrent.BenefitQty = item.ActQty.Value;
            //                    childrent.ConsolQty = item.ActQty.Value;
            //                    childrent.PaidOn = false;
            //                    childrent.GROn = false;
            //                    this.Context.TMatPOItems.Add(childrent);
            //                    await this.Context.SaveChangesAsync();


            //                }
            //            }
            //            foreach (dynamic ob in MFR_GroupShipper)//tách theo Shipper MFR
            //            {
            //                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@Shipper";
            //                var queryParametersInforPO = new DynamicParameters();
            //                queryParametersInforPO.Add("@Shipper", ob.Shipper);
            //                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
            //                    , queryParametersInforPO, commandTimeout: 1500
            //                    , commandType: CommandType.Text);

            //                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD();

            //                parent.RefVKNo = ob.RefVKNo;
            //                parent.Shipper = ob.Shipper;

            //                if (tvendor.POByMfgVendor)
            //                {
            //                    parent.MfgVendor = ob.Shipper;
            //                }

            //                parent.PRID = ob.PRID;
            //                parent.PGrpCD = "MFR";
            //                parent.ReqETD = DateTime.Parse("1900-01-01");
            //                parent.Currency = tvendor.CurrCD;
            //                parent.IncotermsCD = tvendor.IncotermsCD;
            //                parent.Range = "";
            //                parent.AttentionTo = tvendor.TPGrp_ContactTo;
            //                parent.Manual = false;
            //                parent.Closed = false;
            //                parent.Canceled = false;
            //                parent.CreatedBy = int.Parse(GetUserlogin());
            //                parent.CreatedOn = today;
            //                this.Context.TMatPOes.Add(parent);
            //                parentTemp = parent;

            //                var childrentList = itemlist.Where(x => x.PGrp == "MFR" && x.Shipper == ob.Shipper).ToList();

            //                foreach (TPRItem_Custom item in childrentList)
            //                {

            //                    TMatPOItem childrent = new TMatPOItem();
            //                    childrent.MatPOCD = parent.MatPOCD;
            //                    childrent.Vendor = item.MfgVendor;
            //                    childrent.MatCD = item.MatCD;
            //                    // childrent.VendorItemNo = item.VendorItemNo;
            //                    childrent.YQty = item.YQty;
            //                    childrent.PUnit = item.PUnit;
            //                    childrent.POQty = item.TBPQtyFloat;
            //                    childrent.CreatedBy = int.Parse(GetUserlogin());
            //                    childrent.CreatedOn = today;
            //                    childrent.PIDiffCfm = false;
            //                    childrent.Shipper = item.Shipper;
            //                    childrent.BenefitQty = item.ActQty.Value;
            //                    childrent.ConsolQty = item.ActQty.Value;
            //                    childrent.PaidOn = false;
            //                    childrent.GROn = false;
            //                    this.Context.TMatPOItems.Add(childrent);
            //                    await this.Context.SaveChangesAsync();


            //                }
            //            }
            //            foreach (dynamic ob in MVN_GroupShipper)//tách theo Shipper MVN
            //            {
            //                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@Shipper";
            //                var queryParametersInforPO = new DynamicParameters();
            //                queryParametersInforPO.Add("@Shipper", ob.Shipper);
            //                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
            //                    , queryParametersInforPO, commandTimeout: 1500
            //                    , commandType: CommandType.Text);

            //                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD();

            //                parent.RefVKNo = ob.RefVKNo;
            //                parent.Shipper = ob.Shipper;

            //                if (tvendor.POByMfgVendor)
            //                {
            //                    parent.MfgVendor = ob.Shipper;
            //                }

            //                parent.PRID = ob.PRID;
            //                parent.PGrpCD = "MVN";
            //                parent.ReqETD = DateTime.Parse("1900-01-01");
            //                parent.Currency = tvendor.CurrCD;
            //                parent.IncotermsCD = tvendor.IncotermsCD;
            //                parent.Range = "";
            //                parent.AttentionTo = tvendor.TPGrp_ContactTo;
            //                parent.Manual = false;
            //                parent.Closed = false;
            //                parent.Canceled = false;
            //                parent.CreatedBy = int.Parse(GetUserlogin());
            //                parent.CreatedOn = today;
            //                this.Context.TMatPOes.Add(parent);
            //                parentTemp = parent;

            //                var childrentList = itemlist.Where(x => x.PGrp == "MVN" && x.Shipper == ob.Shipper).ToList();

            //                foreach (TPRItem_Custom item in childrentList)
            //                {

            //                    TMatPOItem childrent = new TMatPOItem();
            //                    childrent.MatPOCD = parent.MatPOCD;
            //                    childrent.Vendor = item.MfgVendor;
            //                    childrent.MatCD = item.MatCD;
            //                    // childrent.VendorItemNo = item.VendorItemNo;
            //                    childrent.YQty = item.YQty;
            //                    childrent.PUnit = item.PUnit;
            //                    childrent.POQty = item.TBPQtyFloat;
            //                    childrent.CreatedBy = int.Parse(GetUserlogin());
            //                    childrent.CreatedOn = today;
            //                    childrent.PIDiffCfm = false;
            //                    childrent.Shipper = item.Shipper;
            //                    childrent.BenefitQty = item.ActQty.Value;
            //                    childrent.ConsolQty = item.ActQty.Value;
            //                    childrent.PaidOn = false;
            //                    childrent.GROn = false;
            //                    this.Context.TMatPOItems.Add(childrent);
            //                    await this.Context.SaveChangesAsync();


            //                }
            //            }
            //            foreach (dynamic ob in MBD_GroupShipper)//tách theo Shipper MBD
            //            {
            //                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@Shipper";
            //                var queryParametersInforPO = new DynamicParameters();
            //                queryParametersInforPO.Add("@Shipper", ob.Shipper);
            //                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
            //                    , queryParametersInforPO, commandTimeout: 1500
            //                    , commandType: CommandType.Text);

            //                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD();

            //                parent.RefVKNo = ob.RefVKNo;
            //                parent.Shipper = ob.Shipper;

            //                if (tvendor.POByMfgVendor)
            //                {
            //                    parent.MfgVendor = ob.Shipper;
            //                }

            //                parent.PRID = ob.PRID;
            //                parent.PGrpCD = "MBD";
            //                parent.ReqETD = DateTime.Parse("1900-01-01");
            //                parent.Currency = tvendor.CurrCD;
            //                parent.IncotermsCD = tvendor.IncotermsCD;
            //                parent.Range = "";
            //                parent.AttentionTo = tvendor.TPGrp_ContactTo;
            //                parent.Manual = false;
            //                parent.Closed = false;
            //                parent.Canceled = false;
            //                parent.CreatedBy = int.Parse(GetUserlogin());
            //                parent.CreatedOn = today;
            //                this.Context.TMatPOes.Add(parent);
            //                parentTemp = parent;

            //                var childrentList = itemlist.Where(x => x.PGrp == "MBD" && x.Shipper == ob.Shipper).ToList();

            //                foreach (TPRItem_Custom item in childrentList)
            //                {

            //                    TMatPOItem childrent = new TMatPOItem();
            //                    childrent.MatPOCD = parent.MatPOCD;
            //                    childrent.Vendor = item.MfgVendor;
            //                    childrent.MatCD = item.MatCD;
            //                    // childrent.VendorItemNo = item.VendorItemNo;
            //                    childrent.YQty = item.YQty;
            //                    childrent.PUnit = item.PUnit;
            //                    childrent.POQty = item.TBPQtyFloat;
            //                    childrent.CreatedBy = int.Parse(GetUserlogin());
            //                    childrent.CreatedOn = today;
            //                    childrent.PIDiffCfm = false;
            //                    childrent.Shipper = item.Shipper;
            //                    childrent.BenefitQty = item.ActQty.Value;
            //                    childrent.ConsolQty = item.ActQty.Value;
            //                    childrent.PaidOn = false;
            //                    childrent.GROn = false;
            //                    this.Context.TMatPOItems.Add(childrent);
            //                    await this.Context.SaveChangesAsync();


            //                }
            //            }
            //            foreach (dynamic ob in MKH_GroupShipper)//tách theo Shipper MKH
            //            {
            //                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@Shipper";
            //                var queryParametersInforPO = new DynamicParameters();
            //                queryParametersInforPO.Add("@Shipper", ob.Shipper);
            //                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
            //                    , queryParametersInforPO, commandTimeout: 1500
            //                    , commandType: CommandType.Text);

            //                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD();

            //                parent.RefVKNo = ob.RefVKNo;
            //                parent.Shipper = ob.Shipper;

            //                if (tvendor.POByMfgVendor)
            //                {
            //                    parent.MfgVendor = ob.Shipper;
            //                }

            //                parent.PRID = ob.PRID;
            //                parent.PGrpCD = "MKH";
            //                parent.ReqETD = DateTime.Parse("1900-01-01");
            //                parent.Currency = tvendor.CurrCD;
            //                parent.IncotermsCD = tvendor.IncotermsCD;
            //                parent.Range = "";
            //                parent.AttentionTo = tvendor.TPGrp_ContactTo;
            //                parent.Manual = false;
            //                parent.Closed = false;
            //                parent.Canceled = false;
            //                parent.CreatedBy = int.Parse(GetUserlogin());
            //                parent.CreatedOn = today;
            //                this.Context.TMatPOes.Add(parent);
            //                parentTemp = parent;

            //                var childrentList = itemlist.Where(x => x.PGrp == "MKH" && x.Shipper == ob.Shipper).ToList();

            //                foreach (TPRItem_Custom item in childrentList)
            //                {

            //                    TMatPOItem childrent = new TMatPOItem();
            //                    childrent.MatPOCD = parent.MatPOCD;
            //                    childrent.Vendor = item.MfgVendor;
            //                    childrent.MatCD = item.MatCD;
            //                    // childrent.VendorItemNo = item.VendorItemNo;
            //                    childrent.YQty = item.YQty;
            //                    childrent.PUnit = item.PUnit;
            //                    childrent.POQty = item.TBPQtyFloat;
            //                    childrent.CreatedBy = int.Parse(GetUserlogin());
            //                    childrent.CreatedOn = today;
            //                    childrent.PIDiffCfm = false;
            //                    childrent.Shipper = item.Shipper;
            //                    childrent.BenefitQty = item.ActQty.Value;
            //                    childrent.ConsolQty = item.ActQty.Value;
            //                    childrent.PaidOn = false;
            //                    childrent.GROn = false;
            //                    this.Context.TMatPOItems.Add(childrent);
            //                    await this.Context.SaveChangesAsync();


            //                }
            //            }

            //            foreach (dynamic ob in MKR_GroupMfgVendor)//tách theo vendor MKR
            //            {
            //                string Query = "SELECT TOP(1)a.*,b.ContactTo TPGrp_ContactTo,b.POByShipper,b.POByMfgVendor FROM dbo.TVendor a (NOLOCK) LEFT JOIN dbo.TPGrp b (NOLOCK) ON b.PGrpCD = a.PGrpCD WHERE a.VendorCD=@MfgVendor";
            //                var queryParametersInforPO = new DynamicParameters();
            //                queryParametersInforPO.Add("@MfgVendor", ob.MfgVendor);
            //                TVendor_Custom tvendor = await connection.QueryFirstOrDefaultAsync<TVendor_Custom>(Query
            //                    , queryParametersInforPO, commandTimeout: 1500
            //                    , commandType: CommandType.Text);

            //                TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD();

            //                parent.RefVKNo = ob.RefVKNo;
            //                parent.Shipper = tvendor.Shipper;
            //                parent.MfgVendor = ob.MfgVendor;
            //                //if (tvendor.POByMfgVendor)
            //                //{
            //                //    parent.MfgVendor = ob.Shipper;
            //                //}

            //                parent.PRID = ob.PRID;
            //                parent.PGrpCD = "MKR";// tvendor.PGrpCD;
            //                parent.ReqETD = DateTime.Parse("1900-01-01");
            //                parent.Currency = tvendor.CurrCD;
            //                parent.IncotermsCD = tvendor.IncotermsCD;
            //                parent.Range = "";
            //                parent.AttentionTo = tvendor.TPGrp_ContactTo;
            //                parent.Manual = false;
            //                parent.Closed = false;
            //                parent.Canceled = false;
            //                parent.CreatedBy = int.Parse(GetUserlogin());
            //                parent.CreatedOn = today;
            //                this.Context.TMatPOes.Add(parent);
            //                parentTemp = parent;

            //                var childrentList = itemlist.Where(x => x.PGrp == "MKR" && x.MfgVendor == ob.MfgVendor).ToList();

            //                foreach (TPRItem_Custom item in childrentList)
            //                {

            //                    TMatPOItem childrent = new TMatPOItem();
            //                    childrent.MatPOCD = parent.MatPOCD;
            //                    childrent.Vendor = item.MfgVendor;
            //                    childrent.MatCD = item.MatCD;
            //                    // childrent.VendorItemNo = item.VendorItemNo;
            //                    childrent.YQty = item.YQty;
            //                    childrent.PUnit = item.PUnit;
            //                    childrent.POQty = item.TBPQtyFloat;
            //                    childrent.CreatedBy = int.Parse(GetUserlogin());
            //                    childrent.CreatedOn = today;
            //                    childrent.PIDiffCfm = false;
            //                    childrent.Shipper = item.Shipper;
            //                    childrent.BenefitQty = item.ActQty.Value;
            //                    childrent.ConsolQty = item.ActQty.Value;
            //                    childrent.PaidOn = false;
            //                    childrent.GROn = false;
            //                    this.Context.TMatPOItems.Add(childrent);
            //                    await this.Context.SaveChangesAsync();


            //                }
            //            }
            //            transaction.Commit();
            //        }
            //        catch (Exception ex)
            //        {
            //            transaction.Rollback();
            //            throw new NotImplementedException(ex.Message.ToString());
            //        }
            //    }
            //    string PRID_Custom = "R"+ parentTemp.PRID.Value.ToString("0000000");
            //    return await Task.FromResult(new TMatPO_Custom { PRID_Custom= PRID_Custom,CreatedOn= parentTemp.CreatedOn, PRID= parentTemp.PRID });

            //}
        }
        public async Task<TMatPO> ERP_TMatPO_GenerateMatPOCD(string database)
            {
                try
                {
                    using (var connection = new SqlConnection(database))
                    {
                        var queryParameters = new DynamicParameters();
                        connection.Open();
                        TMatPO data = await connection.QueryFirstOrDefaultAsync<TMatPO>("ERP_TMatPO_GenerateMatPOCD", queryParameters, commandTimeout: 1500
                            , commandType: CommandType.StoredProcedure);
                        return data;
                    }
                }
                catch (Exception ex)
                {
                    throw new NotImplementedException(ex.Message.ToString());
                }
            }
        


//        public Byte[] ERP_TMatPO_IssueReturnPDFNotExist_MKR(TMatPO_Custom item, List<TMatPOItem_Custom> itemchildrent)
//        {
//            try
//            {
//                //đối với những trường hợp khác MKR Khi generate MPO thì 1 MPO là 1 shipper nhưng khi in thì 1 trang là 1 vendor
//                var GroupVendor = itemchildrent.GroupBy(g => g.Vendor).Select(g => new { Vendor = g.Key }).ToList();

//                string baseUrl = "";

//                string pdf_page_size = "A4";
//                PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
//                    pdf_page_size, true);
//                int WebPageWidth = 950;
//                HtmlToPdf converter = new HtmlToPdf();
//                converter.Options.PdfPageSize = pageSize;
//                converter.Options.WebPageWidth = WebPageWidth;
//                SelectPdf.PdfDocument doc = null;
//                decimal pagesizeAllVendor = 0;
//                decimal pagesizeCurrent = 0;
//                foreach (var Vendor in GroupVendor)
//                {
//                    pagesizeAllVendor = pagesizeAllVendor + 1 + Math.Ceiling((decimal)(itemchildrent.Where(x => x.Vendor == Vendor.Vendor).ToList().Count() - 21) / 27);
//                }

//                foreach (var Vendor in GroupVendor)
//                {
//                    pagesizeCurrent += 1;
//                    string htmlPageOne = @"<html>
//    <style>
//      tr td {height:70px;}
//    </style>
//     <body>


//    <div id='print_section' style='width: 1650px;height: 2350px; padding: 0px 30px 0px 30px;font-family: sans-serif; font-size: 26px;'>
//      <br>
//      <br>
//      <br>
//      <br>
//      <div style='display: flex;flex-flow: row wrap; font-size: 38px;'>
//        <div style='width:950px;'>";
//                    string company = GetDB();
//                    if (company == "PCV")
//                    {
//                        if (item.PGrpCD != "MVN")
//                        {
//                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo1 + "</div>";
//                        }
//                        if (item.PGrpCD == "MVN")
//                        {
//                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo2 + "</div>";
//                        }
//                    }
//                    if (company == "CYV")
//                    {
//                        if (item.PGrpCD != "MVN")
//                        {
//                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo1 + "</div>";
//                        }
//                        if (item.PGrpCD == "MVN")
//                        {
//                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo2 + "</div>";
//                        }
//                    }
//                    if (company == "PHC")
//                    {
//                        if (item.PGrpCD != "MKH")
//                        {
//                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo1 + "</div>";
//                        }
//                        if (item.PGrpCD == "MKH")
//                        {
//                            htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo2 + "</div>";
//                        }
//                    }
//                    if (company == "PBC")
//                    {
//                        htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo1 + "</div>";
//                    }
//                    if (company == "PAH")
//                    {
//                        htmlPageOne = htmlPageOne + "<div style='font-weight: 900;'>" + item.ShipTo1 + "</div>";
//                    }

//                    htmlPageOne = htmlPageOne + @"</div>  
//      <div style='width: 230px;font-weight: bold;'>     
//      </div> 
//      <div style='width: 370px;font-weight: bold;'>
//        <div style='font-weight: 900;'>PURCHASE ORDER</div>               
//      </div>       
//      </div> 
    
//      <div style='display: flex;flex-flow: row wrap;'>
//        <div style='width:950px; margin-top: -9px;'>
//        <div>";

//                    if (((company == "PCV" || company == "CYV") && item.PGrpCD != "MVN")
//                        || (company == "PHC" && item.PGrpCD != "MKH")
//                        || (company == "PBC" && item.PGrpCD != "MBD")
//                        || (company == "PAH")
//                                    )
//                    {
//                        htmlPageOne = htmlPageOne + "145, Sagimakgol-ro,Jungwon-gu, Seongnam-si, Gyeonggi-do, Korea";
//                    }

//                    htmlPageOne = htmlPageOne + @"</div> </div>        
//      </div> 
//      <div style='display: flex;flex-flow: row wrap;padding-bottom: 6px; font-size: 28px;'>
//        <div style='width:950px;'>
//        <div style='font-weight: bold;margin-left: 10px;'>VENDOR (SELLER)</div>
//       </div>  
//       <div style='width: 230px;font-weight: bold;'>     
//        <div style='font-weight: bold;margin-left: 10px;'>PO ISSUED</div>
//      </div> 
//      <div style='width: 370px;'>";
//                    htmlPageOne = htmlPageOne + "<div>" + item.PostingDateReportConvert + " </div> ";
//                    htmlPageOne = htmlPageOne + @" </div>       
//      </div> 
//      <div style='display: flex;flex-flow: row wrap;padding-bottom: 6px; font-size: 28px;'>
//        <div style='width:950px;'>";

//                    htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;'>" + item.VendorNamePrintMPO + "</div>";
//                    htmlPageOne = htmlPageOne + @"</div>  
//       <div style='width: 230px;font-weight: bold;'>     
//        <div style='font-weight: bold;margin-left: 10px;'>PO NUMBER</div>
//      </div> 
//      <div style='width: 370px;'>";
//                    htmlPageOne = htmlPageOne + "<div>" + item.MatPOCD + " </div>";
//                    htmlPageOne = htmlPageOne + @"</div>
//      </div>
//                      <div style = 'display: flex;flex-flow: row wrap; font-size: 28px;'>
//                         <div style = 'width:950px;'>
//                          <div style = 'font-weight: bold;margin-left: 10px;'> SHIP TO </div>   
//                              </div>
//                              <div style = 'width: 230px;font-weight: bold;'>
//                                <div style = 'font-weight: bold;margin-left: 10px;margin-bottom:6px;'> CURRENCY </div>
//                               </div>
//                               <div style = 'width: 370px;'>";
//                    htmlPageOne = htmlPageOne + "<div>" + item.Currency + " </div>";
//                    htmlPageOne = htmlPageOne + @"</div>
//                                </div>
//                                <div style = 'display: flex;flex-flow: row wrap;padding-bottom: 6px;'>
//                                   <div style = 'width:950px;'>";

//                    htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo3 + "</div>";
//                    htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>"
//                            + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>"
//                            + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";

//                    //if (company == "PCV")
//                    //    {
//                    //        if (item.PGrpCD != "MVN")
//                    //        {
//                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo1 + "</div>";
//                    //        }
//                    //        if (item.PGrpCD == "MVN")
//                    //        {
//                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo2 + "</div>";
//                    //        }
//                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";
//                    //    }
//                    //    if (company == "CYV")
//                    //    {
//                    //        if (item.PGrpCD != "MVN")
//                    //        {
//                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo1 + "</div>";
//                    //        }
//                    //        if (item.PGrpCD == "MVN")
//                    //        {
//                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo2 + "</div>";
//                    //        }
//                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";
//                    //    }
//                    //    if (company == "PHC")
//                    //    {
//                    //        if (item.PGrpCD != "MKH")
//                    //        {
//                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo1 + "</div>";
//                    //        }
//                    //        if (item.PGrpCD == "MKH")
//                    //        {
//                    //            htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo2 + "</div>";
//                    //        }
//                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";
//                    //    }
//                    //    if (company == "PBC")
//                    //    {
//                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo1 + "</div>";
//                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";
//                    //    }
//                    //    if (company == "PAH")
//                    //    {
//                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.ShipTo1 + "</div>";
//                    //        htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address1 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 25px;'>" + item.Address2 + "</div>" + "<div style='margin-left: 20px;width:950px;height: 36px;'>" + item.Phone + " " + item.Fax + "</div>";
//                    //    }

//                    htmlPageOne = htmlPageOne + "<div style='font-weight: bold;margin-left: 10px;width:950px;height: 33px; font-size: 28px;'>Prod Order # <span style='font-weight: 100;'>" + item.RefVKNo + "</span></div>";

//                    htmlPageOne = htmlPageOne + @"</div>  
//                                  <div style='width: 230px;font-weight: bold; font-size: 28px;'>     
//                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>ORIG. ETD</div>
//                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>ATTENTION TO</div>
//                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>RANGE</div>
//                                    <div style='font-weight: bold;margin-left: 10px;padding-bottom: 6px;'>VENDOR</div>
//                                  </div> 
//                                  <div style='width: 470px; font-size: 28px;'>";
//                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:33px;white-space: nowrap;'>" + item.ReqETDReportConvert + " </div>";
//                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:34px;white-space: nowrap;'>" + item.AttentionTo + "</div>";
//                    if (!string.IsNullOrEmpty(item.Range) && item.Range.Length > 26)
//                    {
//                        item.Range = item.Range.Substring(0, 26);
//                    }
//                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:34px;white-space: nowrap;'>" + item.Range + " </div>";
//                    string vendor = Vendor.Vendor;
//                    if (!string.IsNullOrEmpty(vendor) && vendor.Length > 26)
//                    {
//                        vendor = vendor.Substring(0, 26);
//                    }
//                    htmlPageOne = htmlPageOne + "<div style='padding-bottom: 6px;height:34px;white-space: nowrap;'>" + vendor + " </div>";

//                    htmlPageOne = htmlPageOne + @"</div>       


//                </div> 

//                                  <div style='border-style: solid;padding: 5px 5px 5px 5px;margin-bottom: 5px;border-width: thin;";
//                    if (company == "PHC")
//                    {
//                        htmlPageOne = htmlPageOne + "font-size:24px;'>";
//                    }
//                    if (company != "PHC")
//                    {
//                        htmlPageOne = htmlPageOne + "'>";
//                    }
//                    htmlPageOne = htmlPageOne + @"<div>The Supplier will deliver goods in time-mentioned as above. ";
//                    if (company == "PCV" || company == "PBC" || company == "PAH")
//                    {
//                        htmlPageOne = htmlPageOne + "Park Company Ltd";
//                    }
//                    if (company == "PHC")
//                    {
//                        htmlPageOne = htmlPageOne + "PARK HANDBAG (CAMBODIA) Co., Ltd";
//                    }
//                    if (company == "CYV")
//                    {
//                        htmlPageOne = htmlPageOne + "CY Vina Co., Ltd";
//                    }

//                    htmlPageOne = htmlPageOne + @" reserves the right to examine goods after delivery.</div>  
//                                  <div>The Supplier shall reimburse ";
//                    if (company == "PCV" || company == "PBC" || company == "PAH")
//                    {
//                        htmlPageOne = htmlPageOne + "Park Company Ltd";
//                    }
//                    if (company == "PHC")
//                    {
//                        htmlPageOne = htmlPageOne + "PARK HANDBAG (CAMBODIA) Co., Ltd";
//                    }
//                    if (company == "CYV")
//                    {
//                        htmlPageOne = htmlPageOne + "CY Vina Co., Ltd";
//                    }

//                    htmlPageOne = htmlPageOne + @" immediately cost and expense involved in such delay or inferior quality.</div>
//                                  <div>Original Invoice, Packing List, Bill of Lading for this MPO should be sent to consignee as 'ship to' address and attn above timely.";
//                    if (!string.IsNullOrEmpty(item.Notice))
//                    {
//                        htmlPageOne = htmlPageOne + " </br> " + item.Notice;
//                    }
//                    htmlPageOne = htmlPageOne + @"</div>      
//                                  </div>    
//                                  <table style='border-collapse: collapse;width: 100%;font-size: 26px;'>";
//                    if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
//                    {
//                        htmlPageOne = htmlPageOne + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px; font-size: 28px;'>
//                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
//                                  <th style='width: 12%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 35px;'>Code</th>
//                                  <th style='width: 70%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
//                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
//                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
//                                </tr>    
//                            ";
//                    }
//                    if (item.PRINTTYPE == "2")
//                    {
//                        htmlPageOne = htmlPageOne + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
//                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
//                                  <th style='width: 17%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 10px;'>Code/CM#(Coach)</th>
//                                  <th style='width: 65%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
//                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
//                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
//                                </tr>    
//                            ";
//                    }


//                    string detailpageone = "";
//                    List<TMatPOItem_Custom> ListChildrentPageOne = itemchildrent.Where(x => x.RowGRVendor <= 21 && x.Vendor == Vendor.Vendor).OrderBy(x => x.RowGRVendor).ToList();

//                    decimal pdfpagesize = Math.Ceiling((decimal)(itemchildrent.Where(x => x.Vendor == Vendor.Vendor).Count() - 21) / 27);

//                    foreach (TMatPOItem_Custom ob in ListChildrentPageOne)
//                    {
//                        if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
//                        {
//                            detailpageone = detailpageone + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: center;vertical-align: top;'> " + ob.MatCD + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
//                        }
//                        if (item.PRINTTYPE == "2")
//                        {
//                            detailpageone = detailpageone + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 10px;'> " + ob.MatCD + " <br> " + ob.CustItemNo1 + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
//                        }
//                    }
//                    htmlPageOne = htmlPageOne + detailpageone + "</table></div><div style='width: 1650px;margin: 0px 30px 0px 30px;height: 6px;background-color: black;'></div><div style='font-weight: bold;text-align: center; margin-bottom: 5px;font-size: 25px;'>" + pagesizeCurrent.ToString() + " / " + (pagesizeAllVendor).ToString() + "</div></body></html>";


//                    if (doc == null)
//                    {
//                        doc = converter.ConvertHtmlString(htmlPageOne, baseUrl);
//                    }
//                    else if (doc != null)
//                    {
//                        SelectPdf.PdfDocument docVendor = converter.ConvertHtmlString(htmlPageOne, baseUrl);
//                        doc.Append(docVendor);
//                    }


//                    if (pdfpagesize > 0)
//                    {
//                        for (int i = 1; i <= pdfpagesize; i++)
//                        {
//                            pagesizeCurrent += 1;
//                            HtmlToPdf convertertwo = new HtmlToPdf();

//                            string htmlPageTwo = @"
//                            <html>

//                            <style>
//                              tr td {height:70px;}
//                            </style>
//                             <body>
//                                <div id='print_section' style='width: 1650px;height: 2350px; padding: 0px 30px 0px 30px;font-family: sans-serif; font-size: 26px;'>
//                                  <br>
//                                  <br>
//                                  <br>
//                                  <br>  
//                                  <table style='border-collapse: collapse;width: 100%;font-size: 25px;'>";

//                            if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
//                            {
//                                htmlPageTwo = htmlPageTwo + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;font-size: 28px;'>
//                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
//                                  <th style='width: 12%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 35px;'>Code</th>
//                                  <th style='width: 70%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
//                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
//                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
//                                </tr>    
//                            ";
//                            }
//                            if (item.PRINTTYPE == "2")
//                            {
//                                htmlPageTwo = htmlPageTwo + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
//                                  <th style='width: 3%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Ord</th>
//                                  <th style='width: 17%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding-left: 10px;'>Code/CM#(Coach)</th>
//                                  <th style='width: 65%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>Material Description // Vendor's Item No[Color]</th>
//                                  <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>Quantity</th>   
//                                  <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
//                                </tr>    
//                            ";
//                            }
//                            List<TMatPOItem_Custom> ListChildrentPagetwo = itemchildrent.Where(x => x.Vendor == Vendor.Vendor && x.RowGRVendor > 21 && x.RowGRVendor <= ((i * 27) + 21)).OrderBy(x => x.RowGRVendor).Skip(((i - 1) * 27)).Take(27).ToList();
//                            string detailpagetwo = "";

//                            foreach (TMatPOItem_Custom ob in ListChildrentPagetwo)
//                            {
//                                if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
//                                {
//                                    detailpagetwo = detailpagetwo + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: center;vertical-align: top;'> " + ob.MatCD + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
//                                }
//                                if (item.PRINTTYPE == "2")
//                                {
//                                    detailpagetwo = detailpagetwo + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'>" + ob.RowID.ToString() + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 10px;'> " + ob.MatCD + " <br> " + ob.CustItemNo1 + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
//                                }


//                            }
//                            htmlPageTwo = htmlPageTwo + detailpagetwo + "</table></div><div style='width: 1650px;margin: 0px 30px 0px 30px;height: 6px;background-color: black;'></div><div style='font-weight: bold;text-align: center; margin-bottom: 5px;font-size: 25px;'>" + pagesizeCurrent.ToString() + " / " + (pagesizeAllVendor).ToString() + " </div></body></html>";

//                            convertertwo.Options.PdfPageSize = pageSize;
//                            convertertwo.Options.WebPageWidth = WebPageWidth;

//                            SelectPdf.PdfDocument doctwo = convertertwo.ConvertHtmlString(htmlPageTwo, baseUrl);
//                            doc.Append(doctwo);


//                        }
//                    }




//                }
//                return doc.Save();
//            }
//            catch (Exception ex)
//            {
//                throw new NotImplementedException(ex.Message.ToString());
//            }
//        }
//        public Byte[] ERP_TMatPO_IssueReturnPDF_MKR(TMatPO_Custom item, List<TMatPOItem_Custom> itemchildrent)
//        {
//            try
//            {
//                //đối với những trường hợp MKR Khi generate MPO thì 1 MPO là 1 vendor nhưng khi in thì 1 trang là 1 vendor
//                var GroupVendor = itemchildrent.GroupBy(g => g.Vendor).Select(g => new { Vendor = g.Key, VendorName = g.ToList().Max(d => d.VendorName) }).ToList();

//                string baseUrl = "";

//                string pdf_page_size = "A4";
//                PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
//                    pdf_page_size, true);
//                int WebPageWidth = 950;
//                HtmlToPdf converter = new HtmlToPdf();
//                converter.Options.PdfPageSize = pageSize;
//                converter.Options.WebPageWidth = WebPageWidth;
//                SelectPdf.PdfDocument doc = null;
//                decimal pagesizeAllVendor = 0;
//                decimal pagesizeCurrent = 0;
//                foreach (var Vendor in GroupVendor)
//                {
//                    pagesizeAllVendor = pagesizeAllVendor + 1 + Math.Ceiling((decimal)(itemchildrent.Where(x => x.Vendor == Vendor.Vendor).ToList().Count() - 21) / 27);
//                }
//                foreach (var Vendor in GroupVendor)
//                {
//                    pagesizeCurrent += 1;
//                    string htmlPageOne = @"<html>
//<style>
//  tr td {height:70px;}
//</style>
// <body>
//    <div id='print_section' style='width: 1650px;height: 2350px; padding: 0px 30px 0px 30px;font-family: sans-serif; font-size: 26px;'>
//      <br>
//      <br>
//      <br>
//      <br>
//      <div style='display: flex;flex-flow: row wrap; font-size: 38px;'>
//        <div style='width:850px;'>
//        <div style='font-weight: bold;'>주식회사 한국파크</div>
//       </div>  
//       <div style='width: 200px;font-weight: bold;'>     
//      </div> 
//      <div style='width: 500px;font-weight: bold;'>               
//      </div>       
//      </div>        
//      <div style='display: flex;flex-flow: row wrap;padding-bottom: 6px;'>
//        <div style='width:850px;'>
//        <div style='margin-left: 10px;'>(우) <span style='font-weight: bold;'>13202</span></div>
//        <div style='margin-left: 10px;'>경기도 성남시 중원구 사기막골로 <span style='font-weight: bold;'>145</span></div>          
//        <div style='font-weight: bold;margin-left: 10px;'>TEL) 031-748-0304</div>          
//        <div style='font-weight: bold;margin-left: 10px;'>FAX) 031-748-1057</div>
//        <div style='margin-left: 20px;width:850px;height: 25px;'></div>
//        <div style='font-weight: bold;margin-left: 10px;'>공급처</div>";

//                    htmlPageOne = htmlPageOne + "<div style='margin-left: 20px;width:850px;height: 36px;'>" + Vendor.VendorName + "</div>";
//                    htmlPageOne = htmlPageOne + "<div style='font-weight: bold;margin-left: 10px;width:850px;height: 25px;'>Prod Order # <span style='font-weight: 100;'>" + item.RefVKNo + "</span></div>";
//                    htmlPageOne = htmlPageOne + @"</div>  
//        <div style='width: 200px;font-weight: bold;text-align: right;padding-right: 10px;border-right: solid;border-width: thin;'>   
//        <div style='font-weight: bold;'>발주일</div>
//        <div style='font-weight: bold;'>발주번호</div>          
//        <div style='font-weight: bold;'>납품기일</div>
//        <div style='font-weight: bold;'>담당자</div>
//        <div style='font-weight: bold;'>대금결제방법</div>
//        <div style='font-weight: bold;'>PACKING LIST</div>
//        <div style='font-weight: bold;'>납품장소</div>
//        <div style='font-weight: bold;'>비고</div>    
//      </div> 
//      <div style='width: 500px;padding-left: 10px;'>";
//                    htmlPageOne = htmlPageOne + "<div style='height: 35px;white-space: nowrap;'>" + item.PostingDateReportTiengHanConvert + "</div>";
//                    htmlPageOne = htmlPageOne + "<div style='height: 29px;white-space: nowrap;'>" + item.MatPOCD + " </div>";
//                    htmlPageOne = htmlPageOne + "<div style='height: 29px;white-space: nowrap;'>" + item.ReqETDReportTiengHanConvert + "</div>";
//                    if (!string.IsNullOrEmpty(item.AttentionTo) && item.AttentionTo.Length > 26)
//                    {
//                        item.AttentionTo = item.AttentionTo.Substring(0, 26);
//                    }
//                    htmlPageOne = htmlPageOne + "<div style='height: 29px;white-space: nowrap;'>" + item.AttentionTo + "</div>";
//                    if (!string.IsNullOrEmpty(item.ExtPayMethod) && item.ExtPayMethod.Length > 26)
//                    {
//                        item.ExtPayMethod = item.ExtPayMethod.Substring(0, 26);
//                    }
//                    htmlPageOne = htmlPageOne + "<div style='height: 29px;white-space: nowrap;'>" + item.ExtPayMethod + "</div>";
//                    if (!string.IsNullOrEmpty(item.ExtPLNo) && item.ExtPLNo.Length > 26)
//                    {
//                        item.ExtPLNo = item.ExtPLNo.Substring(0, 26);
//                    }
//                    htmlPageOne = htmlPageOne + "<div style='height: 31px;white-space: nowrap;'>" + item.ExtPLNo + "</div>";
//                    if (!string.IsNullOrEmpty(item.ExtDeliverTo) && item.ExtDeliverTo.Length > 26)
//                    {
//                        item.ExtDeliverTo = item.ExtDeliverTo.Substring(0, 26);
//                    }
//                    htmlPageOne = htmlPageOne + "<div style='height: 33px;white-space: nowrap;'>" + item.ExtDeliverTo + " </div>";
//                    if (!string.IsNullOrEmpty(item.Range) && item.Range.Length > 26)
//                    {
//                        item.Range = item.Range.Substring(0, 26);
//                    }
//                    htmlPageOne = htmlPageOne + "<div style='height: 29px;white-space: nowrap;'>" + item.Range + "  </div>";
//                    htmlPageOne = htmlPageOne + @"</div>       
//                </div>            
//      <div style='border-style: solid;padding: 5px 5px 5px 5px;margin-bottom: 5px;border-width: thin;'>
//       <div>1. 본 주문서를 수령한 거래선은 수출품에 사용될 자재인 만큼 신뢰를 바탕으로 최선을 다해 양질의 제품 생산과 납품기일을 엄수하여 주십시오.</div>  
//       <div>2. 납품시 납품물량에 불량, 부족량 등 미비한 점이 있을 경우 내지 납품불이행 및 납기 지연시 대체교환과 손해배상을 하여야 합니다.</div>
//       <div>3. 기타 본 주문서에 의문사항이 있을 경우 당사에 즉시 문의 확인하여 주시기 바랍니다.</div>      
//       <div >";
//                    htmlPageOne = htmlPageOne + item.Notice + "</div>";
//                    htmlPageOne = htmlPageOne + @"</div>    
//      <table style='border-collapse: collapse;width: 100%;font-size: 26px;'>";

//                    if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
//                    {

//                        htmlPageOne = htmlPageOne + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
//                      <th style='width: 12%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재번호</th>
//                      <th style='width: 73%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재명 // 생산자 자재번호[색상]</th>
//                      <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>수량</th>   
//                      <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
//                    </tr>";
//                    }
//                    if (item.PRINTTYPE == "2")
//                    {
//                        htmlPageOne = htmlPageOne + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
//                      <th style='width: 17%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재번호/CM#(Coach)</th>
//                      <th style='width: 68%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재명 // 생산자 자재번호[색상]</th>
//                      <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>수량</th>   
//                      <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
//                    </tr>";
//                    }


//                    string detailpageone = "";

//                    List<TMatPOItem_Custom> ListChildrentPageOne = itemchildrent.Where(x => x.RowGRVendor <= 21 && x.Vendor == Vendor.Vendor).OrderBy(x => x.RowGRVendor).ToList();

//                    decimal pdfpagesize = Math.Ceiling((decimal)(itemchildrent.Where(x => x.Vendor == Vendor.Vendor).Count() - 21) / 27);

//                    foreach (TMatPOItem_Custom ob in ListChildrentPageOne)
//                    {
//                        if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
//                        {
//                            detailpageone = detailpageone + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatCD + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
//                        }
//                        if (item.PRINTTYPE == "2")
//                        {
//                            detailpageone = detailpageone + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 10px;'> " + ob.MatCD + " <br> " + ob.CustItemNo1 + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
//                        }
//                    }
//                    htmlPageOne = htmlPageOne + detailpageone + "</table></div><div style='width: 1650px;margin: 0px 30px 0px 30px;height: 6px;background-color: black;'></div><div style='font-weight: bold;text-align: center; margin-bottom: 5px;font-size: 25px;'>" + pagesizeCurrent.ToString() + " / " + (pagesizeAllVendor).ToString() + "</div></body></html>";


//                    if (doc == null)
//                    {
//                        doc = converter.ConvertHtmlString(htmlPageOne, baseUrl);
//                    }
//                    else if (doc != null)
//                    {
//                        SelectPdf.PdfDocument docVendor = converter.ConvertHtmlString(htmlPageOne, baseUrl);
//                        doc.Append(docVendor);
//                    }



//                    if (pdfpagesize > 0)
//                    {
//                        for (int i = 1; i <= pdfpagesize; i++)
//                        {
//                            pagesizeCurrent += 1;
//                            HtmlToPdf convertertwo = new HtmlToPdf();

//                            string htmlPageTwo = @"
//                            <html>

//                            <style>
//                              tr td {height:70px;}
//                            </style>
//                             <body>
//                                <div id='print_section' style='width: 1650px;height: 2350px; padding: 0px 30px 0px 30px;font-family: sans-serif; font-size: 26px;'>
//                                  <br>
//                                  <br>
//                                  <br>
//                                  <br>  
//                                  <table style='border-collapse: collapse;width: 100%;font-size: 26px;'>";

//                            if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
//                            {
//                                htmlPageTwo = htmlPageTwo + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
//                              <th style='width: 12%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재번호</th>
//                              <th style='width: 73%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재명 // 생산자 자재번호[색상]</th>
//                              <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>수량</th>   
//                              <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
//                            </tr>
//                            ";
//                            }
//                            if (item.PRINTTYPE == "2")
//                            {
//                                htmlPageTwo = htmlPageTwo + @"<tr style='border-bottom: 6px solid #343a40;text-align: left;padding: 8px;'>
//                              <th style='width: 17%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재번호/CM#(Coach)</th>
//                              <th style='width: 68%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;'>자재명 // 생산자 자재번호[색상]</th>
//                              <th style='width: 10%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'>수량</th>   
//                              <th style='width: 5%;border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;'></th> 
//                            </tr>";
//                            }
//                            List<TMatPOItem_Custom> ListChildrentPagetwo = itemchildrent.Where(x => x.Vendor == Vendor.Vendor && x.RowGRVendor > 21 && x.RowGRVendor <= ((i * 27) + 21)).OrderBy(x => x.RowGRVendor).Skip(((i - 1) * 27)).Take(27).ToList();
//                            string detailpagetwo = "";

//                            foreach (TMatPOItem_Custom ob in ListChildrentPagetwo)
//                            {
//                                if (item.PRINTTYPE == "1" || item.PRINTTYPE == "3")
//                                {
//                                    detailpagetwo = detailpagetwo + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatCD + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
//                                }
//                                if (item.PRINTTYPE == "2")
//                                {
//                                    detailpagetwo = detailpagetwo + "<tr style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 10px;'> " + ob.MatCD + " <br> " + ob.CustItemNo1 + "</td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;'> " + ob.MatDescColor + " </td><td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: right;vertical-align: top;'> " + string.Format("{0:#,###}", ob.POQty) + " </td>   <td style='border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd;text-align: left;vertical-align: top;padding-left: 3px;' > " + ob.PUnit + " </td>   </tr>  ";
//                                }


//                            }
//                            htmlPageTwo = htmlPageTwo + detailpagetwo + "</table></div><div style='width: 1650px;margin: 0px 30px 0px 30px;height: 6px;background-color: black;'></div><div style='font-weight: bold;text-align: center; margin-bottom: 5px;font-size: 25px;'>" + pagesizeCurrent.ToString() + " / " + (pagesizeAllVendor).ToString() + " </ div></body></html>";

//                            convertertwo.Options.PdfPageSize = pageSize;
//                            convertertwo.Options.WebPageWidth = WebPageWidth;

//                            SelectPdf.PdfDocument doctwo = convertertwo.ConvertHtmlString(htmlPageTwo, baseUrl);
//                            doc.Append(doctwo);


//                        }
//                    }

//                }
//                return doc.Save();
//            }
//            catch (Exception ex)
//            {
//                throw new NotImplementedException(ex.Message.ToString());
//            }
//        }
    }
}

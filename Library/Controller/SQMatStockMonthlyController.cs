﻿using Dapper;
using HR.API.Models.ERP;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;


namespace Library.Controller
{
    public class SQMatStockMonthlyController:BaseController
    {
        public async Task<IList<SQMatStockMonthlyModel>> ERP_Report_SQMatStockMonthly(string database,string Outsourcing, string datemonth, int PageNumber, int PageSize)
        {
            //try
            //{
            //    DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
            //    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            //    {
            //        var queryParameters = new DynamicParameters();
            //        queryParameters.Add("@Month", date.Month);
            //        queryParameters.Add("@Year", date.Year);
            //        queryParameters.Add("@Outsourcing", Outsourcing);
            //        queryParameters.Add("@PageSize", PageSize);
            //        queryParameters.Add("@PageNumber", PageNumber);
            //        queryParameters.Add("@Type", 0);
            //        connection.Open();
            //        var data = await connection.QueryAsync<SQMatStockMonthlyModel>("ERP_Report_SQMatStockMonthly_List"
            //            , queryParameters, commandTimeout: 1500
            //            , commandType: CommandType.StoredProcedure);
            //        return data.ToList();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw new NotImplementedException(ex.Message.ToString());
            //}
            try
            {
                DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                using (var connection = new SqlConnection(database))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@Outsourcing", Outsourcing);
                    queryParameters.Add("@PageSize", PageSize);
                    queryParameters.Add("@PageNumber", PageNumber);
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<SQMatStockMonthlyModel>(
                        @"	DECLARE @NgayDauThang DATETIME,@NgayCuoiThang DATETIME
	SET @NgayDauThang= CONVERT(DATETIME,CAST(@Year AS varchar(4)) + '-' + CAST(@Month AS varchar(4)) + '-1')

	SET @NgayCuoiThang=DATEADD(DAY,-1,DATEADD(month,1,@NgayDauThang))


	SELECT PInfoID,MatCD,PUnit,MfgVendor,CASE WHEN Incoterms='CIF' THEN  ISNULL(CIFPrice,0)/NULLIF(PriceLotSize,0)
		WHEN Incoterms='EXW' THEN ISNULL(EXWPrice,0)/NULLIF(PriceLotSize,0)
		WHEN Incoterms='FOB' THEN ISNULL(FOBPrice,0)/NULLIF(PriceLotSize,0)
		WHEN Incoterms='DAP' THEN ISNULL(CIFPrice,0)/NULLIF(PriceLotSize,0)
	ELSE ISNULL(FOBcPrice,0)/NULLIF(PriceLotSize,0) END CIFPrice
	,CASE WHEN Incoterms='CIF' THEN CIFCurr
		WHEN Incoterms='EXW' THEN EXWCurr
		WHEN Incoterms='FOB' THEN FOBCurr
		WHEN Incoterms='DAP' THEN CIFCurr
	ELSE FOBCurr END CIFCurr
	,CIFCurr CIFCurrTemp
	,EXWCurr EXWCurrTemp
	,FOBCurr FOBCurrTemp

	,ValidFrom
	,CASE WHEN Incoterms='CIF' THEN CIFPrice
		  WHEN Incoterms='EXW' THEN EXWPrice
	      WHEN Incoterms='FOB' THEN FOBPrice
		  WHEN Incoterms='DAP' THEN CIFPrice
		ELSE FOBPrice END PriceIncoterms
	,PriceLotSize
	INTO #TPInfo
	FROM dbo.TPInfo (NOLOCK)
	WHERE ValidFrom <= @NgayCuoiThang
	AND Discontinue=0
	OPTION (RECOMPILE)	

	--WHERE CIFPrice IS NOT NULL --Discontinue=0

	SELECT MAX(PInfoID) PInfoID,MatCD
	INTO #TPInfoGRID
	FROM #TPInfo
	GROUP BY MatCD


	SELECT MatCD,PUnit
	INTO #TPInfoGRPUnit
	FROM #TPInfo
	GROUP BY MatCD,PUnit
	ORDER BY MatCD

	SELECT MatCD
	INTO #TPInfoGRMutiPunit
	FROM #TPInfoGRPUnit
	GROUP BY MatCD
	HAVING COUNT(MatCD) > 1

	DROP TABLE #TPInfoGRPUnit

	SELECT a.MatCD
			,a.PUnit
			,a.MfgVendor
	--		,CASE WHEN a.CIFCurr IS NULL THEN 0
	--ELSE  a.CIFPrice END CIFPrice
			,a.CIFPrice
			,a.CIFCurr
			,a.ValidFrom
			,a.PriceLotSize
			,a.PriceIncoterms
			,b.ExRate
			,CASE WHEN c.MatCD IS NOT NULL AND a.PUNIT='SET' THEN 'PC'
				WHEN c.MatCD IS NOT NULL AND a.PUNIT='YRD' THEN 'M'
				WHEN c.MatCD IS NOT NULL AND a.PUNIT='SQM' THEN 'SQF'
				ELSE a.PUNIT END PUnitChuan
	INTO #TPInfoLaster
	FROM #TPInfo a
	LEFT JOIN dbo.TTExRateDaily b (NOLOCK) ON a.ValidFrom=b.BDate  AND a.CIFCurr=b.BCurrCD
	LEFT JOIN #TPInfoGRMutiPunit c ON a.MatCD=c.MatCD
	WHERE a.PInfoID IN (SELECT PInfoID FROM #TPInfoGRID)
	OPTION (RECOMPILE)	
	

	DROP TABLE #TPInfo,#TPInfoGRID











	SELECT c.PRID,a.BrandCD,a.ProdID
	INTO #PRIDBrandCD
	FROM TProd a (NOLOCK) 
	INNER JOIN  TProdInstItem b (NOLOCK) ON a.ProdID=b.ProdID
	INNER JOIN TPRProdInst c (NOLOCK) ON  b.ProdInstCD=c.ProdInstCD

	SELECT MAX(ProdID) ProdID,PRID,BrandCD 
	INTO #PRIDBrandCDGR
	FROM #PRIDBrandCD
	GROUP BY PRID,BrandCD

	SELECT ProdID,PRID,BrandCD 
	INTO #PRIDBrandCDGRFinal
	FROM #PRIDBrandCD
	WHERE ProdID IN (SELECT ProdID FROM #PRIDBrandCDGR)
	OPTION (RECOMPILE)	


	SELECT a.MatCD,MAX(b.BrandCD) BrandCD--,b.ProdID
	INTO #BrandCD
	FROM (
		SELECT MAX(a.PRID) PRID,a.MatCD
		FROM (
			SELECT PRID,MatCD,PRItemID 
			FROM dbo.TPRItem (NOLOCK) 
		) a
		GROUP BY a.MatCD
	) a
	INNER JOIN #PRIDBrandCDGRFinal b ON a.PRID=b.PRID
	GROUP BY a.MatCD--,b.ProdID--,b.BrandCD

	--SELECT MatCD,MAX(ProdID) ProdID
	--INTO #BrandCDGR
	--FROM #BrandCD
	--GROUP BY MatCD



	--SELECT a.MatCD,a.BrandCD 
	--INTO #BrandFinal
	--FROM #BrandCD a
	--INNER JOIN #BrandCDGR B ON a.MatCD=b.MatCD AND a.ProdID=b.ProdID

	DROP TABLE #PRIDBrandCDGR,#PRIDBrandCDGRFinal--,#BrandCD,#BrandCDGR


		SELECT  a.IVID
				,a.MatCD
				,a.PRICE
				,a.CURR
				,a.PUNIT
				,a.VCD
				,a.IVLISTNO
				,ISNULL(b.IDATE,ISNULL(a.WDATE,ISNULL(a.UDATE,a.CreatedOn))) IDATE
				,a.OUTCD
	    INTO #GR 
		FROM STin_List a (NOLOCK)
		LEFT JOIN STin b (NOLOCK) ON a.IVID=b.IVID
		WHERE ISNULL(b.IDATE,ISNULL(a.WDATE,ISNULL(a.UDATE,a.CreatedOn))) <= @NgayCuoiThang
	    OPTION (RECOMPILE)	

		--WHERE 
		--AND a.QTY > 0  AND a.OUTCD IS NULL-- AND a.MATCD='M0000220'


		SELECT MAX(IVLISTNO) IVLISTNO
			   ,MatCD 
		INTO #VendorGR
		FROM #GR
		GROUP BY MatCD

		SELECT  a.MatCD
			   ,b.VCD
		INTO #Vendor
		FROM #VendorGR a
		INNER JOIN #GR b ON a.IVLISTNO=b.IVLISTNO

		DROP TABLE #VendorGR

		--Giá lớn hơn 0
		SELECT MAX(IVLISTNO) IVLISTNO
			   ,MatCD 
		INTO #GRLatest
		FROM #GR
		WHERE PRICE > 0 AND CURR IS NOT NULL AND OUTCD IS NULL
		GROUP BY MatCD
	    OPTION (RECOMPILE)	



		SELECT  a.MatCD
			   ,b.PRICE
			   ,b.PUNIT
			   ,b.CURR
			   ,c.ExRate
			   ,b.IDATE
		INTO #GRPriceLatest
		FROM #GRLatest a
		INNER JOIN #GR b ON a.IVLISTNO=b.IVLISTNO
		LEFT JOIN dbo.TTExRateDaily c (NOLOCK) ON c.BDate=b.IDATE AND c.BCurrCD=b.CURR

		--Giá 0

		SELECT MAX(a.IVLISTNO) IVLISTNO
			   ,a.MatCD 
		INTO #GRLatest_Price0
		FROM #GR a
		WHERE a.PRICE =0  AND a.CURR IS NOT NULL AND a.OUTCD IS NULL
		AND NOT EXISTS (SELECT * FROM #GRPriceLatest b WHERE a.MATCD=b.MATCD)
		GROUP BY MatCD
	    OPTION (RECOMPILE)	


		INSERT #GRPriceLatest
		(
		    MATCD,
		    PRICE,
		    PUNIT,
		    CURR,
		    ExRate,
			IDATE
		)
		SELECT  a.MatCD
			   ,b.PRICE
			   ,b.PUNIT
			   ,b.CURR
			   ,c.ExRate
			   ,b.IDATE
		FROM #GRLatest_Price0 a
		INNER JOIN #GR b ON a.IVLISTNO=b.IVLISTNO
		LEFT JOIN dbo.TTExRateDaily c (NOLOCK) ON c.BDate=b.IDATE AND c.BCurrCD=b.CURR


 
 	DROP TABLE #GR,#GRLatest,#GRLatest_Price0





	SELECT c.MatCD
			,SUM(CASE WHEN e.RSign='-' THEN -(c.OQTY)
			ELSE c.OQTY END) QTYGI
			,c.PUNIT
	INTO #GINotConvert
	FROM STOut_List c (NOLOCK)
	INNER JOIN (
	    SELECT OUTCD
		FROM STOut (NOLOCK) 
		WHERE ODATE BETWEEN @NgayDauThang AND @NgayCuoiThang

	) g ON g.OUTCD=c.OUTCD
	INNER JOIN STReason e (NOLOCK) ON c.RCD=e.RCD
	WHERE c.MATCD IS NOT NULL
	GROUP BY c.MatCD,c.PUNIT
	OPTION (RECOMPILE)	

	--ORDER BY c.MatCD

		SELECT   a.MatCD
				,a.QTYGI
				,a.PUNIT
				,l.PUnitChuan
	INTO #GIPUnitChuan
	FROM #GINotConvert a
	LEFT JOIN #TPInfoLaster l ON l.MatCD = a.MATCD


	SELECT MatCD
		,PUnitChuan PUNIT
		,SUM(QTYGI*dbo.ConvQtyGR(ISNULL(PUNIT,''),ISNULL(PUnitChuan,0))) QTYGI
	INTO #GI
	FROM #GIPUnitChuan 
	GROUP BY MatCD,PUnitChuan


	DROP TABLE #GINotConvert,#GIPUnitChuan

	--DROP TABLE #STOut



	SELECT IVID 
	INTO #STIn_IVID
	FROM dbo.STIn (NOLOCK)
	WHERE IDATE BETWEEN @NgayDauThang AND @NgayCuoiThang
	OPTION (RECOMPILE)	


	SELECT  c.MatCD
			,c.QTY
			,c.PUNIT

			,l.PUnitChuan

			,c.IV
	INTO #GRSTIn_List
	FROM dbo.STIn_List c (NOLOCK)
	INNER JOIN #STIn_IVID g ON g.IVID=c.IVID
	LEFT JOIN #TPInfoLaster l ON c.MATCD=l.MatCD
	WHERE  c.MATCD IS NOT NULL
	AND c.OUTCD IS NULL
	OPTION (RECOMPILE)	
	

	DROP TABLE #STIn_IVID

	INSERT #GRSTIn_List
	(
	    MATCD,
	    QTY,
	    PUNIT,
		PUnitChuan
	)
	SELECT  c.MatCD
		,c.QTY
		,c.PUNIT
		,l.PUnitChuan
	FROM dbo.STIn_List c (NOLOCK)
	INNER JOIN dbo.STInBad h (NOLOCK) ON c.OUTCD=h.OUTCD AND h.ODATE BETWEEN @NgayDauThang AND @NgayCuoiThang
	LEFT JOIN #TPInfoLaster l ON c.MATCD=l.MatCD
	WHERE c.MATCD IS NOT NULL AND c.OUTCD IS NOT NULL 
	--AND c.IV NOT IN (SELECT IV FROM #GRSTIn_List)
	OPTION (RECOMPILE)	


	SELECT MatCD,SUM(QTY*dbo.ConvQtyGR(ISNULL(PUNIT,''),ISNULL(PUnitChuan,0))) QTYGR,PUnitChuan PUNIT 
	INTO #GRConverPUNITfINAL
	FROM #GRSTIn_List 
	GROUP BY MatCD,PUnitChuan

	DROP TABLE #GRSTIn_List


	SELECT BYear,BCurrCD,TCurrCD,ExRate 
	INTO #TExRate
	FROM dbo.TExRate  (NOLOCK)

	SELECT MAX(BYear) BYear,BCurrCD
	INTO #TExRateGR
	FROM #TExRate
	GROUP BY BCurrCD


	SELECT a.BYear,a.BCurrCD,a.TCurrCD,a.ExRate
	INTO #TExRateGRTPInfor
	FROM #TExRate a
	INNER JOIN #TExRateGR b ON a.BYear=b.BYear AND a.BCurrCD=b.BCurrCD
	ORDER BY a.BYear
	
	DROP TABLE #TExRateGR

	SELECT MatCD,Qty,CreatedOn,StartYear,StartMonth,Unit,CloseYear,CloseMonth
	INTO #STBasicStock
	FROM dbo.STBasicStock (NOLOCK) 
	WHERE (StartYear=@Year AND StartMonth=@Month) 
	OR (StartYear=YEAR(DATEDIFF(MONTH,-1,@NgayCuoiThang)) 
	AND StartMonth=MONTH(DATEDIFF(MONTH,-1,@NgayCuoiThang)))
	OR (CloseYear=@Year AND CloseMonth=@Month)
	OPTION (RECOMPILE)	



	SELECT  a.MatCD
			,CASE WHEN a.Spec1 IS NULL AND a.Spec2 IS NOT NULL THEN a.MatName+ ' '+ a.Spec2
			WHEN a.Spec1 IS NOT NULL AND a.Spec2 IS NULL  THEN a.MatName+ ' '+ a.Spec1
			WHEN a.Spec1 IS NOT NULL AND a.Spec2 IS NOT NULL  THEN a.MatName+ ' '+ a.Spec1 + ' '+ a.Spec2
			ELSE a.MatName END MatDesc
			,a.Color
			,b.MH1
			,a.OutSourcing
			,CASE WHEN p.VCD IS NULL THEN c.MfgVendor 
			      ELSE p.VCD END Vendor
			,CASE WHEN m.MatCD IS NOT NULL THEN 1
			ELSE 0 END MultiUnit
			,c.PUnit PUnitLast
			,d.BrandCD

			,c.PriceLotSize PriceLotSizePInfo
			,c.PriceIncoterms PriceIncotermsPInfo
			,c.CIFCurr

			,e.PRICE*c.PriceLotSize PriceGR
			,e.CURR CURRGR



			,c.CIFPrice LastPricePInfo
			,e.PRICE LastPriceGR
			,e.PUNIT PUNITLastPriceGR
			,e.ExRate
			,e.IDATE
	        ,c.ExRate ExRateTPInfor
			,c.ValidFrom
			,f.Qty QtyBS
			,f.Unit PUnitBasicStock
			,h.QTYGR
			,h.PUNIT PUNITGR
			,g.QTYGI
			,g.PUNIT PUNITGI
			,(ISNULL(f.Qty,0) + ISNULL(h.QTYGR,0)) - ISNULL(g.QTYGI,0)  QtyEndingS
			,q.Qty QtyInspect
			,q.Unit PUnitInspect
			,q.CreatedOn DateInspect
	INTO #SQMatStockMonthly
	FROM  dbo.TMat a (NOLOCK) 
	LEFT JOIN  dbo.TMH b (NOLOCK) ON a.MHID=b.MHID
	LEFT JOIN #TPInfoLaster c ON a.MatCD=c.MatCD
	LEFT JOIN #BrandCD d ON a.MatCD=d.MatCD
	LEFT JOIN #GRPriceLatest e ON e.MATCD=a.MatCD
	LEFT JOIN #Vendor p ON a.MatCD=p.MATCD
	LEFT JOIN #STBasicStock f ON a.MatCD=f.MATCD AND f.StartYear=@Year AND f.StartMonth=@Month AND f.Qty<>0
	LEFT JOIN #GI g ON a.MatCD=g.MATCD
	LEFT JOIN #GRConverPUNITfINAL h ON a.MatCD=h.MATCD
	LEFT JOIN #TPInfoGRMutiPunit m ON a.MatCD=m.MatCD
	--LEFT JOIN #TExRate n (NOLOCK) ON n.BYear=e.YEAR AND e.CURR=n.BCurrCD
	--LEFT JOIN #TExRateGRTPInfor o (NOLOCK) ON  c.CIFCurr=o.BCurrCD
	--LEFT JOIN #STBasicStock q ON q.MatCD=a.MatCD AND q.StartYear=YEAR(DATEDIFF(MONTH,-1,@NgayCuoiThang)) 
	--AND q.StartMonth=MONTH(DATEDIFF(MONTH,-1,@NgayCuoiThang))
	LEFT JOIN #STBasicStock q ON q.MatCD=a.MatCD AND q.CloseYear=@Year AND q.CloseMonth=@Month AND q.Qty<>0 

	WHERE a.Discontinue=0 AND (@Outsourcing='ALL' OR (ISNULL(a.OutSourcing,0)=0 AND @Outsourcing='false') OR (ISNULL(a.OutSourcing,0)=1 AND @Outsourcing='true'))
	OPTION (RECOMPILE)	
	
	-- AND a.MatCD='M0000159'

	DROP TABLE #TPInfoGRMutiPunit,#TPInfoLaster,#GRPriceLatest,#GI,#GRConverPUNITfINAL,#TExRate,#TExRateGRTPInfor,#Vendor,#BrandCD,#STBasicStock

	SELECT   a.MatCD
			,a.MatDesc
			,a.Color
			,a.MH1
			,a.OutSourcing
			,a.Vendor
			,a.MultiUnit
			,a.PUnitLast
			,a.PUNITLastPriceGR
			,CASE   WHEN a.MultiUnit=1 AND a.PUnitLast='SET' THEN 'PC'
					WHEN a.MultiUnit=1 AND a.PUnitLast='YRD' THEN 'M'
					WHEN a.MultiUnit=1 AND a.PUnitLast='SQM' THEN 'SQF'
			ELSE a.PUnitLast END PUnitGRChuan
			,a.BrandCD

			--,a.PriceLotSizePInfo
			--,a.PriceIncotermsPInfo
			,a.LastPricePInfo
			,a.LastPriceGR


			,a.PriceLotSizePInfo
			,a.PriceIncotermsPInfo
			,a.CIFCurr
			,a.PriceGR
			,a.CURRGR


			,a.ExRateTPInfor
			,a.ValidFrom
			,a.ExRate
			,a.IDATE
			,a.QtyBS
			,a.PUnitBasicStock
			,a.QTYGR
			,a.PUNITGR
			,a.QTYGI
			,a.PUNITGI
			,a.QtyEndingS
			,a.QtyInspect
			,a.PUnitInspect
			,a.DateInspect			 
	INTO #SQMatStockMonthlyUnitChuan
	FROM #SQMatStockMonthly a

	DROP TABLE #SQMatStockMonthly

	SELECT   a.MatCD
			,a.MatDesc 
			,a.Color
			,a.MH1
			,a.OutSourcing
			,a.Vendor
			,a.MultiUnit
			,a.PUnitLast
			,ISNULL(a.PUnitGRChuan,a.PUnitGR) PUnitGRChuan
			,a.BrandCD

			,a.PriceLotSizePInfo
			,a.PriceIncotermsPInfo*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUnitLast,0)) PriceIncotermsPInfo
			,(a.LastPricePInfo*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUnitLast,0)))*a.ExRateTPInfor LastPricePInfo
			,a.CIFCurr
			,a.PriceGR*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)) PriceGR
			,a.CURRGR			
			

			--,CASE WHEN a.PriceIncotermsPInfo = 0 OR a.PriceIncotermsPInfo*a.ExRateTPInfor = 0  THEN NULL
			--		      WHEN a.CIFCurr=a.CURRGR  THEN ((a.PriceGR*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)))/a.PriceIncotermsPInfo)*100
			--		ELSE ((a.PriceGR*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0))*a.ExRate)/a.PriceIncotermsPInfo*a.ExRateTPInfor)*100 END Comp
			
			--,CASE WHEN a.PriceLotSizePInfo = 0 OR a.PriceGR = 0  THEN NULL
			--		WHEN a.CIFCurr=a.CURRGR  THEN (((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))
			--		- a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast,a.PUnitGRChuan) )
			--		/(a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan)))*100

			--ELSE (( (a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))*a.ExRate
			--- (a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast,a.PUnitGRChuan))*a.ExRateTPInfor)
			--/(a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))*a.ExRate)*100 END Comp

			,CASE WHEN a.PriceLotSizePInfo = 0 OR a.PriceGR = 0  THEN NULL
					WHEN a.CIFCurr=a.CURRGR  THEN ((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))		
			-(a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast,a.PUnitGRChuan)))
			/((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan)))*100 

			ELSE ((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))*a.ExRate 			
			-(a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast,a.PUnitGRChuan))*a.ExRateTPInfor)
			/((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR,a.PUnitGRChuan))*a.ExRate)*100  END Comp
						
			
			,f.Payment



			--,a.PriceIncotermsPInfo
			--,a.CIFCurr
			
			
			
			
			
			,a.ValidFrom
			,a.LastPriceGR LastPriceGRtt
			,a.PUNITLastPriceGR
			,(a.LastPriceGR*dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)))*a.ExRate LastPriceGR
			,a.IDATE
			,a.ExRate
			--,ISNULL(a.QtyBS,0) QtyBS
			,ISNULL(a.QtyBS,0)*dbo.ConvQtyGR(ISNULL(a.PUnitBasicStock,''),ISNULL(a.PUnitGRChuan,0)) QtyBS
			,a.QTYGR*dbo.ConvQtyGR(ISNULL(a.PUnitGR,''),ISNULL(a.PUnitGRChuan,0)) QTYGR
			--,a.PUnitGR
			,a.QTYGI*dbo.ConvQtyGR(ISNULL(a.PUNITGI,''),ISNULL(a.PUnitGRChuan,0)) QTYGI
			,a.PUNITGI
			--,ISNULL(a.QtyInspect,0) QtyInspect
			,ISNULL(a.QtyInspect,0)*dbo.ConvQtyGR(ISNULL(a.PUnitInspect,''),ISNULL(a.PUnitGRChuan,0)) QtyInspect
			,a.DateInspect	
    INTO #SQMatStockMonthlyFinal
	FROM #SQMatStockMonthlyUnitChuan a
	LEFT JOIN STCustomsMat f (NOLOCK) ON a.MatCD=f.MatCD
	--WHERE a.MatCD='M0046983'
	ORDER BY a.MatCD
	OPTION (RECOMPILE)	




	DROP TABLE #SQMatStockMonthlyUnitChuan

	SELECT MatCD
	       ,SUM(QtyUnCut+QtySubStore) QtyEU
	INTO #tb_TTWIPMat
	FROM dbo.TTWIPMat (NOLOCK)
	WHERE @Year=Year AND @Month=Month
	GROUP BY MatCD
	OPTION (RECOMPILE)	


	SELECT      IDENTITY( int ) AS RowID
				,a.MatCD
				,a.MatDesc 
				,a.Color
				,a.MH1
			    ,a.OutSourcing
				,a.Vendor
				--,a.MultiUnit
				--,a.PUnitLast
				,a.PUnitGRChuan PUnit
				,a.BrandCD

				--,a.PriceLotSizePInfo
			 --   ,a.PriceIncotermsPInfo
			,a.LastPricePInfo

			,a.PriceLotSizePInfo
			,a.PriceIncotermsPInfo
			,a.CIFCurr
			,a.PriceGR
			,a.CURRGR			
			,a.Comp
			,a.Payment



				,a.ValidFrom
				,a.LastPriceGR
				,a.IDATE
				--,ExRate
				,CASE WHEN a.LastPriceGR IS NULL THEN a.LastPricePInfo
				 ELSE a.LastPriceGR END UnitPrice
				,a.QtyBS
				,ISNULL(a.QTYGR,0)  QTYGR
				,ISNULL(a.QTYGI,0) QTYGI
				,ROUND((ISNULL(a.QtyBS,0) + ISNULL(a.QTYGR,0)) - ISNULL(a.QTYGI,0),5)  QtyEndingS 
				,a.QtyInspect
				,a.DateInspect	

				,b.QtyEU
    INTO #SQMatStockMonthlyRowID
	FROM #SQMatStockMonthlyFinal a
	LEFT JOIN #tb_TTWIPMat b ON a.MatCD=b.MatCD


	SELECT IDENTITY( int ) AS RowID
			,MatCD
			,MatDesc 
			,Color
			,MH1
			,OutSourcing
			,Vendor
			,PUnit
			,BrandCD

			--,PriceLotSizePInfo
			--,PriceIncotermsPInfo
			,LastPricePInfo
			,LastPriceGR

			,PriceLotSizePInfo
			,PriceIncotermsPInfo
			,CIFCurr
			,PriceGR
			,CURRGR			
			,ROUND(Comp,0) Comp
			,Payment


			,UnitPrice
			,QtyBS
			,QTYGR
			,QTYGI
			,QtyEndingS 
			,QtyBS*UnitPrice AmtBS
			,QTYGR*UnitPrice AmtGR
			,QTYGI*UnitPrice AmtGI
			,QtyEndingS*UnitPrice AmtES
			,QtyInspect
			,ROUND(QtyInspect - QtyEndingS,2) QtyDiff
			,DateInspect

			,QtyEU
			,(QtyEU+QtyEndingS) QtyESSum
			,QtyEU*UnitPrice AmtEU
			,(QtyEU+QtyEndingS)*UnitPrice AmtESSum
    INTO #SQMatStock
	FROM #SQMatStockMonthlyRowID
	ORDER BY MatCD

	DECLARE @SumAllQtyBS FLOAT,@SumAllQTYGR FLOAT,@SumAllQTYGI FLOAT,@SumAllQtyEndingS FLOAT,@SumAllAmtBS FLOAT,@SumAllAmtGR FLOAT
	,@SumAllAmtGI FLOAT,@SumAllAmtES FLOAT,@SumAllQtyInspect FLOAT,@SumAllQtyDiff FLOAT,@SumAllQtyEU FLOAT,@SumAllQtyESSum FLOAT
	,@SumAllAmtEU FLOAT,@SumAllAmtESSum FLOAT


	SELECT   @SumAllQtyBS=SUM(QtyBS)
			,@SumAllQTYGR=SUM(QTYGR) 
			,@SumAllQTYGI=SUM(QTYGI) 
			,@SumAllQtyEndingS=SUM(QtyEndingS) 
			,@SumAllAmtBS=SUM(AmtBS) 
			,@SumAllAmtGR=SUM(AmtGR) 
			,@SumAllAmtGI=SUM(AmtGI) 
			,@SumAllAmtES=SUM(AmtES) 
			,@SumAllQtyInspect=SUM(QtyInspect) 
			,@SumAllQtyDiff=SUM(QtyDiff) 
			,@SumAllQtyEU=SUM(QtyEU) 
			--,@SumAllQtyESSum=SUM(QtyESSum) 
			,@SumAllAmtEU=SUM(AmtEU) 
			,@SumAllAmtESSum=SUM(AmtESSum)  
	FROM #SQMatStock

	DROP TABLE #SQMatStockMonthlyFinal

	IF @Type=0
	BEGIN
				DECLARE @TotalRows INT
				SELECT @TotalRows=COUNT(RowID) FROM #SQMatStock


				SELECT TOP 15  @TotalRows TotalRows
						,RowID
						,MatCD
						,MatDesc 
						,Color
						,MH1
			            ,OutSourcing
						,Vendor
						,PUnit
						,BrandCD

						,LastPricePInfo
						,LastPriceGR

						,PriceLotSizePInfo
						,PriceIncotermsPInfo
						,CIFCurr
						,PriceGR
						,CURRGR			
						,Comp
						,Payment

						,UnitPrice
						,QtyBS
						,QTYGR
						,QTYGI
						,QtyEndingS 
						,AmtBS
						,AmtGR
						,AmtGI
						,AmtES
						,QtyInspect
						,QtyDiff
						,REPLACE(convert(varchar, DateInspect, 106),' ','/') DateInspectConvert
						,QtyEU
						,QtyESSum
						,AmtEU
						,AmtESSum
						,@SumAllQtyBS      SumAllQtyBS
						,@SumAllQTYGR	   SumAllQTYGR
						,@SumAllQTYGI	   SumAllQTYGI

						,@SumAllQtyEndingS SumAllQtyEndingS
						,@SumAllQtyEU	   SumAllQtyEU
						,@SumAllQtyEndingS + @SumAllQtyEU  SumAllQtyESSum


						,@SumAllAmtBS	   SumAllAmtBS
						,@SumAllAmtGR	   SumAllAmtGR
						,@SumAllAmtGI	   SumAllAmtGI
						,@SumAllAmtES	   SumAllAmtES
						,@SumAllAmtEU	   SumAllAmtEU
						,@SumAllAmtESSum   SumAllAmtESSum


						,@SumAllQtyInspect SumAllQtyInspect
						,@SumAllQtyDiff	   SumAllQtyDiff
				FROM #SQMatStock
				WHERE RowID > ((@PageNumber-1)*@PageSize)
				ORDER BY MatCD
	            OPTION (RECOMPILE)	




	END

	IF @Type=1
	BEGIN

				SELECT  
						MatCD
						,MatDesc
						,Color	
						,MH1
						,PUnit

			            ,OutSourcing
						,Vendor
						,BrandCD

						,PriceLotSizePInfo 'PriceLot'
						,PriceIncotermsPInfo 'PricePInfo'
						,CIFCurr 'Curr'

						,PriceGR
						,CURRGR	'Curr_Double0'
						,Comp 'Diff(%)_Percent0'
						,Payment
						,UnitPrice


						--,PriceLotSizePInfo
						--,PriceIncotermsPInfo
						--,LastPricePInfo
						--,LastPriceGR
						,QtyBS
						,QTYGR
						,QTYGI
						,QtyEndingS 'QtyES'
						,QtyEU
						,QtyESSum
						,AmtBS
						,AmtGR
						,AmtGI
						,AmtES
						,AmtEU
						,AmtESSum
						,QtyInspect
						,QtyDiff
						,REPLACE(convert(varchar, DateInspect, 106),' ','/') DateInspect

						,'Total' Total_Total_16
						,@SumAllQtyBS      Total_SumAllQtyBS_Format2_17
						,@SumAllQTYGR	   Total_SumAllQTYGR_Format2_18
						,@SumAllQTYGI	   Total_SumAllQTYGI_Format2_19
						,@SumAllQtyEndingS Total_SumAllQtyEndingS_Format2_20
						,@SumAllQtyEU	   Total_SumAllQtyEU_Format2_21
						,@SumAllQtyEndingS + @SumAllQtyEU  Total_SumAllQtyESSum_Format2_22
						,@SumAllAmtBS	   Total_SumAllAmtBS_Format2_23
						,@SumAllAmtGR	   Total_SumAllAmtGR_Format2_24
						,@SumAllAmtGI	   Total_SumAllAmtGI_Format2_25
						,@SumAllAmtES	   Total_SumAllAmtES_Format2_26
						,@SumAllAmtEU	   Total_SumAllAmtEU_Format2_27
						,@SumAllAmtESSum   Total_SumAllAmtESSum_Format2_28
						,@SumAllQtyInspect Total_SumAllQtyInspect_Format2_29
						,@SumAllQtyDiff	   Total_SumAllQtyDiff_Format2_30
				FROM #SQMatStock
				ORDER BY MatCD


	END

	IF @Type=2
	BEGIN




				SELECT  
						c.MatCD
						,SUM(c.QTY) QTYGR
						,c.PUNIT PUNITGR
						,h.PUnit PUnitChuan
				INTO #GRNowMatCD
				FROM dbo.STIn_List c (NOLOCK)
				INNER JOIN dbo.STIn g (NOLOCK) ON g.IVID=c.IVID
				INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
				WHERE YEAR(g.IDATE) = YEAR(GETDATE()) AND MONTH(g.IDATE) = MONTH(GETDATE())
				AND DAY(g.IDATE) = DAY(GETDATE()) 
				AND c.MATCD IS NOT NULL
				GROUP BY c.MatCD,c.PUNIT,h.PUnit
				ORDER BY c.MatCD
	            OPTION (RECOMPILE)	


				SELECT MATCD
				      ,SUM(ISNULL(QTYGR,0)*dbo.ConvQtyGR(ISNULL(PUNITGR,0),ISNULL(PUnitChuan,0))) QTYGRNow
                INTO #GRNow
				FROM #GRNowMatCD
				GROUP BY MATCD

				DROP TABLE #GRNowMatCD




				SELECT c.MatCD
						,SUM(CASE WHEN e.RSign='-' THEN -(c.OQTY)
						ELSE c.OQTY END) QTYGI
						,c.PUNIT PUNITGI
						,h.PUnit PUnitChuan
				INTO #GINowMatCD
				FROM STOut_List c (NOLOCK)
				INNER JOIN dbo.STOut g (NOLOCK) ON g.OUTCD=c.OUTCD
				INNER JOIN STReason e (NOLOCK) ON c.RCD=e.RCD
				INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
				WHERE YEAR(g.ODATE) = YEAR(GETDATE()) AND MONTH(g.ODATE) = MONTH(GETDATE())
				AND DAY(g.ODATE) = DAY(GETDATE())
				AND c.MATCD IS NOT NULL
				GROUP BY c.MatCD,c.PUNIT,h.PUnit
				ORDER BY c.MatCD
	            OPTION (RECOMPILE)	




				SELECT MATCD
					  ,SUM(ISNULL(QTYGI,0)*dbo.ConvQtyGR(ISNULL(PUNITGI,0),ISNULL(PUnitChuan,0))) QTYGINow
				INTO #GINow
				FROM #GINowMatCD
				GROUP BY MATCD


				DROP TABLE #GINowMatCD



				SELECT 
						MH1
					   ,COUNT(a.MH1) Count
					   ,SUM(a.QtyBS*a.UnitPrice) AmtBS
					   ,SUM(a.QTYGR*UnitPrice) AmtGR
					   ,SUM(a.QTYGI*a.UnitPrice) AmtGI
					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice) AmtES
					   ,SUM(a.QtyEU*a.UnitPrice) AmtEU
					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice + a.QtyEU*a.UnitPrice) AmtESSum

					   --,SUM(b.QTYGRNow*a.UnitPrice) AmtGRNow
					   --,SUM(c.QTYGINow*a.UnitPrice) AmtGINow
					   ,1 RowID
                INTO #SQMatStockMonthlyGR
				FROM #SQMatStockMonthlyRowID a
				LEFT JOIN #GRNow b ON a.MatCD=b.MATCD
				LEFT JOIN #GINow c ON a.MatCD=c.MATCD
				GROUP BY a.MH1

				DROP TABLE #GRNow,#GINow

						DECLARE  @ToTal_CountSum FLOAT
								,@ToTal_AmtBSSum FLOAT
								,@ToTal_AmtGRSum FLOAT
								,@ToTal_AmtGISum FLOAT
								,@ToTal_AmtESSum FLOAT
								,@ToTal_AmtEUSum FLOAT
								,@ToTal_AmtESSumSum FLOAT



				SELECT  @ToTal_CountSum=SUM(Count) 
					   ,@ToTal_AmtBSSum=SUM(AmtBS) 
					   ,@ToTal_AmtGRSum=SUM(AmtGR) 
					   ,@ToTal_AmtGISum=SUM(AmtGI) 
					   ,@ToTal_AmtESSum=SUM(AmtES) 
					   ,@ToTal_AmtEUSum=SUM(AmtEU) 
					   ,@ToTal_AmtESSumSum=SUM(AmtESSum)
				FROM #SQMatStockMonthlyGR




				SELECT 	MH1
						,Count
						,ROUND(AmtBS,2) AmtBS
						,ROUND(AmtGR,2) AmtGR
						,ROUND(AmtGI,2) AmtGI
						,ROUND(AmtES,2) AmtES
						,ROUND(ISNULL(AmtEU,0),2) AmtEU
						,ROUND(ISNULL(AmtESSum,0),2) AmtESSum
						,@ToTal_CountSum    ToTal_CountSum
						,@ToTal_AmtBSSum	   ToTal_AmtBSSum
						,@ToTal_AmtGRSum	   ToTal_AmtGRSum
						,@ToTal_AmtGISum	   ToTal_AmtGISum
						,@ToTal_AmtESSum	   ToTal_AmtESSum
						,@ToTal_AmtEUSum	   ToTal_AmtEUSum
						,@ToTal_AmtESSumSum ToTal_AmtESSumSum

				FROM #SQMatStockMonthlyGR
				ORDER BY RowID

				DROP TABLE #SQMatStockMonthlyGR


	END
	IF @Type=3
	BEGIN




				SELECT  
						c.MatCD
						,SUM(c.QTY) QTYGR
						,c.PUNIT PUNITGR
						,h.PUnit PUnitChuan
				INTO #GRNowMatCD_Excel
				FROM dbo.STIn_List c (NOLOCK)
				INNER JOIN dbo.STIn g (NOLOCK) ON g.IVID=c.IVID
				INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
				WHERE YEAR(g.IDATE) = YEAR(GETDATE()) AND MONTH(g.IDATE) = MONTH(GETDATE())
				AND DAY(g.IDATE) = DAY(GETDATE()) 
				AND c.MATCD IS NOT NULL
				GROUP BY c.MatCD,c.PUNIT,h.PUnit
				ORDER BY c.MatCD
	            OPTION (RECOMPILE)	


				SELECT MATCD
				      ,SUM(ISNULL(QTYGR,0)*dbo.ConvQtyGR(ISNULL(PUNITGR,0),ISNULL(PUnitChuan,0))) QTYGRNow
                INTO #GRNow_Excel
				FROM #GRNowMatCD_Excel
				GROUP BY MATCD

				DROP TABLE #GRNowMatCD_Excel




				SELECT c.MatCD
						,SUM(CASE WHEN e.RSign='-' THEN -(c.OQTY)
						ELSE c.OQTY END) QTYGI
						,c.PUNIT PUNITGI
						,h.PUnit PUnitChuan
				INTO #GINowMatCD_Excel
				FROM STOut_List c (NOLOCK)
				INNER JOIN dbo.STOut g (NOLOCK) ON g.OUTCD=c.OUTCD
				INNER JOIN STReason e (NOLOCK) ON c.RCD=e.RCD
				INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
				WHERE YEAR(g.ODATE) = YEAR(GETDATE()) AND MONTH(g.ODATE) = MONTH(GETDATE())
				AND DAY(g.ODATE) = DAY(GETDATE())
				AND c.MATCD IS NOT NULL
				GROUP BY c.MatCD,c.PUNIT,h.PUnit
				ORDER BY c.MatCD
	            OPTION (RECOMPILE)	




				SELECT MATCD
					  ,SUM(ISNULL(QTYGI,0)*dbo.ConvQtyGR(ISNULL(PUNITGI,0),ISNULL(PUnitChuan,0))) QTYGINow
				INTO #GINow_Excel
				FROM #GINowMatCD_Excel
				GROUP BY MATCD


				DROP TABLE #GINowMatCD_Excel



				SELECT 
						MH1
					   ,COUNT(a.MH1) Count
					   ,SUM(a.QtyBS*a.UnitPrice) AmtBS
					   ,SUM(a.QTYGR*UnitPrice) AmtGR
					   ,SUM(a.QTYGI*a.UnitPrice) AmtGI
					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice) AmtES
					   ,SUM(a.QtyEU*a.UnitPrice) AmtEU
					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice + a.QtyEU*a.UnitPrice) AmtESSum
					   ,1 RowID
                INTO #SQMatStockMonthlyGR_Excel
				FROM #SQMatStockMonthlyRowID a
				LEFT JOIN #GRNow_Excel b ON a.MatCD=b.MATCD
				LEFT JOIN #GINow_Excel c ON a.MatCD=c.MATCD
				GROUP BY a.MH1

				DROP TABLE #GRNow_Excel,#GINow_Excel


						DECLARE  @ToTal_CountSum_Excel FLOAT
								,@ToTal_AmtBSSum_Excel FLOAT
								,@ToTal_AmtGRSum_Excel FLOAT
								,@ToTal_AmtGISum_Excel FLOAT
								,@ToTal_AmtESSum_Excel FLOAT
								,@ToTal_AmtEUSum_Excel FLOAT
								,@ToTal_AmtESSumSum_Excel FLOAT



				SELECT  @ToTal_CountSum_Excel=SUM(Count) 
					   ,@ToTal_AmtBSSum_Excel=SUM(AmtBS) 
					   ,@ToTal_AmtGRSum_Excel=SUM(AmtGR) 
					   ,@ToTal_AmtGISum_Excel=SUM(AmtGI) 
					   ,@ToTal_AmtESSum_Excel=SUM(AmtES) 
					   ,@ToTal_AmtEUSum_Excel=SUM(AmtEU) 
					   ,@ToTal_AmtESSumSum_Excel=SUM(AmtESSum)
				FROM #SQMatStockMonthlyGR_Excel





				SELECT 	MH1 'Categories'
						,Count Count_Format2
						,ROUND(AmtBS,2) 'AmtBS_Format2'
						,ROUND(AmtGR,2) 'AmtGR_Format2'
						,ROUND(AmtGI,2) 'AmtGI_Format2'
						,ROUND(AmtES,2) 'AmtES_Format2' 
						,ROUND(AmtEU,2) 'AmtEU_Format2' 
						,ROUND(AmtESSum,2) 'AmtESSum_Format2' 
						,'ToTal'    Total_ToTal_1 
						,@ToTal_CountSum_Excel    Total_CountSum_Format2_2 
						,@ToTal_AmtBSSum_Excel	Total_AmtBSSum_Format2_3
						,@ToTal_AmtGRSum_Excel	Total_AmtGRSum_Format2_4 
						,@ToTal_AmtGISum_Excel	Total_AmtGISum_Format2_5 
						,@ToTal_AmtESSum_Excel	Total_AmtESSum_Format2_6 
						,@ToTal_AmtEUSum_Excel	Total_AmtEUSum_Format2_7 
						,@ToTal_AmtESSumSum_Excel	Total_AmtESSum_Format2_8

				FROM #SQMatStockMonthlyGR_Excel
				ORDER BY RowID

				DROP TABLE #SQMatStockMonthlyGR_Excel


	END
	DROP TABLE #SQMatStockMonthlyRowID
"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public Byte[] ERP_Report_SQMatStockMonthlyExcel(string database,string Outsourcing, string datemonth)
        {
            //try
            //{
            //    DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);

            //    DataTable table = new DataTable();
            //    SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            //    SqlCommand cmd = new SqlCommand("ERP_Report_SQMatStockMonthly_List", con);
            //    cmd.CommandTimeout = 1500;
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    cmd.Parameters.Add("@Month", SqlDbType.Int).Value = date.Month;
            //    cmd.Parameters.Add("@Year", SqlDbType.Int).Value = date.Year;
            //    cmd.Parameters.Add("@Outsourcing", SqlDbType.NVarChar).Value = Outsourcing;
            //    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            //    using (var da = new SqlDataAdapter(cmd))
            //    {
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        da.Fill(table);
            //    }


            //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            //        string filename = "MatStockMonthly";
            //        Byte[] bytes = ExportDataTableToExcelFromStore(table, filename, true, false);
            //        response.Content = new ByteArrayContent(bytes);
            //        response.Content.Headers.ContentLength = bytes.LongLength;
            //        response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            //        response.Content.Headers.ContentDisposition.FileName = filename;
            //        response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
            //        return response;

            //}
            //catch (Exception ex)
            //{
            //    throw new NotImplementedException(ex.Message.ToString());
            //}
            try
            {
                DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);

                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(database);
                SqlCommand cmd = new SqlCommand("ERP_Report_SQMatStockMonthly_List", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = date.Month;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = date.Year;
                cmd.Parameters.Add("@Outsourcing", SqlDbType.NVarChar).Value = Outsourcing;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                string filename = "MatStockMonthly";

                return ExportDataTableToExcelFromStore(table, filename, true, false);

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        //        USE[PARKERP]
        //        GO
        ///****** Object:  StoredProcedure [dbo].[ERP_Report_SQMatStockMonthly_List]    Script Date: 2024/03/12 4:23:00 PM ******/
        //SET ANSI_NULLS ON
        //GO
        //SET QUOTED_IDENTIFIER ON
        //GO



        //-- =============================================
        //-- Author:		<Author,,Name>
        //-- Create date: <Create Date,,>
        //-- Description:	<Description,,>
        //-- =============================================
        //ALTER PROCEDURE[dbo].[ERP_Report_SQMatStockMonthly_List]
        //        @Month INT = 8,
        //   @Year INT=2022,
        //	@Outsourcing NVARCHAR(10),
        //	@Type INT = 0,
        //    @PageNumber INT=0,
        //	@PageSize INT = 0
        //AS
        //BEGIN
        //	--DECLARE @Month INT
        //	--SET @Month = 10
        //    --DECLARE @Year INT
        //	--SET @Year = 2023
        //    --DECLARE @Outsourcing NVARCHAR(10)
        //	--SET @Outsourcing = 'ALL'
        //    --M0060824
        //    SET NOCOUNT ON;
        //	--có gọi từ ben ERP_STOut_List_AdjustStock

        //    DECLARE @NgayDauThang DATETIME, @NgayCuoiThang DATETIME
        //     SET @NgayDauThang= CONVERT(DATETIME, CAST(@Year AS varchar(4)) + '-' + CAST(@Month AS varchar(4)) + '-1')

        //	SET @NgayCuoiThang = DATEADD(DAY, -1, DATEADD(month, 1, @NgayDauThang))



        //    SELECT PInfoID, MatCD, PUnit, MfgVendor, CASE WHEN Incoterms = 'CIF' THEN ISNULL(CIFPrice,0)/NULLIF(PriceLotSize,0)

        //        WHEN Incoterms = 'EXW' THEN ISNULL(EXWPrice,0)/NULLIF(PriceLotSize,0)

        //        WHEN Incoterms = 'FOB' THEN ISNULL(FOBPrice,0)/NULLIF(PriceLotSize,0)

        //        WHEN Incoterms = 'DAP' THEN ISNULL(CIFPrice,0)/NULLIF(PriceLotSize,0)

        //    ELSE ISNULL(FOBcPrice,0)/NULLIF(PriceLotSize,0) END CIFPrice
        //    , CASE WHEN Incoterms = 'CIF' THEN CIFCurr

        //        WHEN Incoterms = 'EXW' THEN EXWCurr

        //        WHEN Incoterms = 'FOB' THEN FOBCurr

        //        WHEN Incoterms = 'DAP' THEN CIFCurr

        //    ELSE FOBCurr END CIFCurr
        //    , CIFCurr CIFCurrTemp
        //	,EXWCurr EXWCurrTemp
        //    , FOBCurr FOBCurrTemp

        //	,ValidFrom
        //	,CASE WHEN Incoterms='CIF' THEN CIFPrice

        //          WHEN Incoterms = 'EXW' THEN EXWPrice

        //          WHEN Incoterms = 'FOB' THEN FOBPrice

        //          WHEN Incoterms = 'DAP' THEN CIFPrice

        //        ELSE FOBPrice END PriceIncoterms
        //    , PriceLotSize

        //    INTO #TPInfo
        //	FROM dbo.TPInfo(NOLOCK)
        //   WHERE ValidFrom <= @NgayCuoiThang
        //   AND Discontinue=0
        //	OPTION(RECOMPILE)

        //	--WHERE CIFPrice IS NOT NULL --Discontinue=0

        //	SELECT MAX(PInfoID) PInfoID,MatCD
        //    INTO #TPInfoGRID
        //	FROM #TPInfo
        //	GROUP BY MatCD



        //    SELECT MatCD, PUnit

        //    INTO #TPInfoGRPUnit
        //	FROM #TPInfo
        //	GROUP BY MatCD,PUnit
        //    ORDER BY MatCD


        //    SELECT MatCD

        //    INTO #TPInfoGRMutiPunit
        //	FROM #TPInfoGRPUnit
        //	GROUP BY MatCD
        //    HAVING COUNT(MatCD) > 1

        //	DROP TABLE #TPInfoGRPUnit


        //    SELECT a.MatCD

        //            , a.PUnit

        //            , a.MfgVendor
        //	--		, CASE WHEN a.CIFCurr IS NULL THEN 0
        //	--ELSE a.CIFPrice END CIFPrice

        //            , a.CIFPrice

        //            , a.CIFCurr

        //            , a.ValidFrom

        //            , a.PriceLotSize

        //            , a.PriceIncoterms

        //            , b.ExRate

        //            , CASE WHEN c.MatCD IS NOT NULL AND a.PUNIT= 'SET' THEN 'PC'

        //                WHEN c.MatCD IS NOT NULL AND a.PUNIT= 'YRD' THEN 'M'

        //                WHEN c.MatCD IS NOT NULL AND a.PUNIT= 'SQM' THEN 'SQF'

        //                ELSE a.PUNIT END PUnitChuan
        //    INTO #TPInfoLaster
        //	FROM #TPInfo a
        //	LEFT JOIN dbo.TTExRateDaily b (NOLOCK) ON a.ValidFrom= b.BDate  AND a.CIFCurr= b.BCurrCD

        //    LEFT JOIN #TPInfoGRMutiPunit c ON a.MatCD=c.MatCD
        //	WHERE a.PInfoID IN (SELECT PInfoID FROM #TPInfoGRID)
        //	OPTION (RECOMPILE)


        //    DROP TABLE #TPInfo,#TPInfoGRID











        //	SELECT c.PRID, a.BrandCD, a.ProdID
        //    INTO #PRIDBrandCD
        //	FROM TProd a (NOLOCK)
        //    INNER JOIN TProdInstItem b (NOLOCK) ON a.ProdID= b.ProdID

        //    INNER JOIN TPRProdInst c (NOLOCK) ON  b.ProdInstCD= c.ProdInstCD


        //    SELECT MAX(ProdID) ProdID, PRID, BrandCD
        //    INTO #PRIDBrandCDGR
        //	FROM #PRIDBrandCD
        //	GROUP BY PRID, BrandCD

        //    SELECT ProdID, PRID, BrandCD
        //    INTO #PRIDBrandCDGRFinal
        //	FROM #PRIDBrandCD
        //	WHERE ProdID IN (SELECT ProdID FROM #PRIDBrandCDGR)
        //	OPTION (RECOMPILE)


        //    SELECT a.MatCD, MAX(b.BrandCD) BrandCD--, b.ProdID
        //    INTO #BrandCD
        //	FROM (
        //        SELECT MAX(a.PRID) PRID, a.MatCD
        //        FROM (
        //            SELECT PRID, MatCD, PRItemID
        //            FROM dbo.TPRItem (NOLOCK)

        //        ) a
        //        GROUP BY a.MatCD

        //    ) a
        //    INNER JOIN #PRIDBrandCDGRFinal b ON a.PRID=b.PRID
        //	GROUP BY a.MatCD--, b.ProdID--, b.BrandCD

        //	--SELECT MatCD, MAX(ProdID) ProdID
        //	--INTO #BrandCDGR
        //	--FROM #BrandCD
        //	--GROUP BY MatCD



        //	--SELECT a.MatCD, a.BrandCD
        //	--INTO #BrandFinal
        //	--FROM #BrandCD a
        //	--INNER JOIN #BrandCDGR B ON a.MatCD=b.MatCD AND a.ProdID=b.ProdID


        //    DROP TABLE #PRIDBrandCDGR,#PRIDBrandCDGRFinal--,#BrandCD,#BrandCDGR



        //        SELECT a.IVID

        //                , a.MatCD

        //                , a.PRICE

        //                , a.CURR

        //                , a.PUNIT

        //                , a.VCD

        //                , a.IVLISTNO

        //                , ISNULL(b.IDATE, ISNULL(a.WDATE, ISNULL(a.UDATE, a.CreatedOn))) IDATE
        //				,a.OUTCD
        //        INTO #GR 
        //		FROM STin_List a(NOLOCK)

        //        LEFT JOIN STin b(NOLOCK) ON a.IVID=b.IVID
        //       WHERE ISNULL(b.IDATE, ISNULL(a.WDATE, ISNULL(a.UDATE, a.CreatedOn))) <= @NgayCuoiThang
        //          OPTION(RECOMPILE)

        //		--WHERE 
        //		--AND a.QTY > 0  AND a.OUTCD IS NULL-- AND a.MATCD= 'M0000220'



        //        SELECT MAX(IVLISTNO) IVLISTNO
        //               , MatCD
        //        INTO #VendorGR
        //		FROM #GR
        //		GROUP BY MatCD


        //        SELECT a.MatCD

        //               , b.VCD
        //        INTO #Vendor
        //		FROM #VendorGR a
        //		INNER JOIN #GR b ON a.IVLISTNO=b.IVLISTNO

        //		DROP TABLE #VendorGR

        //		--Giá lớn hơn 0

        //        SELECT MAX(IVLISTNO) IVLISTNO
        //               , MatCD
        //        INTO #GRLatest
        //		FROM #GR
        //		WHERE PRICE > 0 AND CURR IS NOT NULL AND OUTCD IS NULL
        //        GROUP BY MatCD

        //        OPTION (RECOMPILE)



        //        SELECT  a.MatCD

        //               , b.PRICE

        //               , b.PUNIT

        //               , b.CURR

        //               , c.ExRate

        //               , b.IDATE
        //        INTO #GRPriceLatest
        //		FROM #GRLatest a
        //		INNER JOIN #GR b ON a.IVLISTNO=b.IVLISTNO
        //		LEFT JOIN dbo.TTExRateDaily c (NOLOCK) ON c.BDate= b.IDATE AND c.BCurrCD= b.CURR

        //        --Giá 0


        //        SELECT MAX(a.IVLISTNO) IVLISTNO
        //               , a.MatCD
        //        INTO #GRLatest_Price0
        //		FROM #GR a
        //		WHERE a.PRICE = 0  AND a.CURR IS NOT NULL AND a.OUTCD IS NULL
        //        AND NOT EXISTS (SELECT* FROM #GRPriceLatest b WHERE a.MATCD=b.MATCD)
        //		GROUP BY MatCD

        //        OPTION (RECOMPILE)


        //        INSERT #GRPriceLatest
        //		(
        //            MATCD,
        //            PRICE,
        //            PUNIT,
        //            CURR,
        //            ExRate,
        //            IDATE
        //        )
        //        SELECT  a.MatCD

        //               , b.PRICE

        //               , b.PUNIT

        //               , b.CURR

        //               , c.ExRate

        //               , b.IDATE
        //        FROM #GRLatest_Price0 a
        //		INNER JOIN #GR b ON a.IVLISTNO=b.IVLISTNO
        //		LEFT JOIN dbo.TTExRateDaily c (NOLOCK) ON c.BDate= b.IDATE AND c.BCurrCD= b.CURR




        //     DROP TABLE #GR,#GRLatest,#GRLatest_Price0






        //    SELECT c.MatCD

        //            , SUM(CASE WHEN e.RSign= '-' THEN -(c.OQTY)
        //            ELSE c.OQTY END) QTYGI
        //			,c.PUNIT
        //    INTO #GINotConvert
        //	FROM STOut_List c(NOLOCK)

        //    INNER JOIN(
        //        SELECT OUTCD

        //        FROM STOut (NOLOCK)
        //        WHERE ODATE BETWEEN @NgayDauThang AND @NgayCuoiThang


        //    ) g ON g.OUTCD=c.OUTCD
        //    INNER JOIN STReason e(NOLOCK) ON c.RCD=e.RCD
        //   WHERE c.MATCD IS NOT NULL

        //   GROUP BY c.MatCD, c.PUNIT
        //   OPTION (RECOMPILE)

        //	--ORDER BY c.MatCD

        //       SELECT   a.MatCD

        //               , a.QTYGI

        //               , a.PUNIT

        //               , l.PUnitChuan
        //   INTO #GIPUnitChuan
        //	FROM #GINotConvert a
        //	LEFT JOIN #TPInfoLaster l ON l.MatCD = a.MATCD


        //	SELECT MatCD

        //       , PUnitChuan PUNIT
        //       , SUM(QTYGI* dbo.ConvQtyGR(ISNULL(PUNIT,''),ISNULL(PUnitChuan,0))) QTYGI
        //     INTO #GI
        //	FROM #GIPUnitChuan 
        //	GROUP BY MatCD, PUnitChuan



        //    DROP TABLE #GINotConvert,#GIPUnitChuan

        //	--DROP TABLE #STOut




        //    SELECT IVID

        //    INTO #STIn_IVID
        //	FROM dbo.STIn(NOLOCK)
        //   WHERE IDATE BETWEEN @NgayDauThang AND @NgayCuoiThang
        //   OPTION(RECOMPILE)



        //    SELECT c.MatCD

        //            , c.QTY

        //            , c.PUNIT


        //            , l.PUnitChuan


        //            , c.IV
        //    INTO #GRSTIn_List
        //	FROM dbo.STIn_List c (NOLOCK)
        //    INNER JOIN #STIn_IVID g ON g.IVID=c.IVID
        //	LEFT JOIN #TPInfoLaster l ON c.MATCD=l.MatCD
        //	WHERE  c.MATCD IS NOT NULL

        //    AND c.OUTCD IS NULL
        //    OPTION (RECOMPILE)


        //    DROP TABLE #STIn_IVID

        //	INSERT #GRSTIn_List
        //	(
        //        MATCD,
        //        QTY,
        //        PUNIT,
        //        PUnitChuan
        //    )
        //    SELECT  c.MatCD

        //        , c.QTY

        //        , c.PUNIT

        //        , l.PUnitChuan
        //    FROM dbo.STIn_List c (NOLOCK)
        //    INNER JOIN dbo.STInBad h (NOLOCK) ON c.OUTCD= h.OUTCD AND h.ODATE BETWEEN @NgayDauThang AND @NgayCuoiThang
        //    LEFT JOIN #TPInfoLaster l ON c.MATCD=l.MatCD
        //	WHERE c.MATCD IS NOT NULL AND c.OUTCD IS NOT NULL 
        //	--AND c.IV NOT IN (SELECT IV FROM #GRSTIn_List)
        //	OPTION (RECOMPILE)


        //    SELECT MatCD, SUM(QTY* dbo.ConvQtyGR(ISNULL(PUNIT,''),ISNULL(PUnitChuan,0))) QTYGR,PUnitChuan PUNIT

        //    INTO #GRConverPUNITfINAL
        //	FROM #GRSTIn_List 
        //	GROUP BY MatCD,PUnitChuan

        //    DROP TABLE #GRSTIn_List


        //	SELECT BYear,BCurrCD,TCurrCD,ExRate
        //    INTO #TExRate
        //	FROM dbo.TExRate(NOLOCK)


        //    SELECT MAX(BYear) BYear,BCurrCD
        //    INTO #TExRateGR
        //	FROM #TExRate
        //	GROUP BY BCurrCD



        //    SELECT a.BYear, a.BCurrCD, a.TCurrCD, a.ExRate
        //    INTO #TExRateGRTPInfor
        //	FROM #TExRate a
        //	INNER JOIN #TExRateGR b ON a.BYear=b.BYear AND a.BCurrCD=b.BCurrCD
        //	ORDER BY a.BYear

        //    DROP TABLE #TExRateGR

        //	SELECT MatCD, Qty, CreatedOn, StartYear, StartMonth, Unit, CloseYear, CloseMonth
        //    INTO #STBasicStock
        //	FROM dbo.STBasicStock (NOLOCK)
        //    WHERE (StartYear= @Year AND StartMonth = @Month)

        //    OR(StartYear= YEAR(DATEDIFF(MONTH, -1, @NgayCuoiThang))

        //    AND StartMonth = MONTH(DATEDIFF(MONTH, -1, @NgayCuoiThang)))

        //    OR(CloseYear= @Year AND CloseMonth = @Month)

        //    OPTION(RECOMPILE)




        //    SELECT a.MatCD

        //            , CASE WHEN a.Spec1 IS NULL AND a.Spec2 IS NOT NULL THEN a.MatName+ ' '+ a.Spec2
        //            WHEN a.Spec1 IS NOT NULL AND a.Spec2 IS NULL THEN a.MatName+ ' '+ a.Spec1
        //            WHEN a.Spec1 IS NOT NULL AND a.Spec2 IS NOT NULL  THEN a.MatName+ ' '+ a.Spec1 + ' '+ a.Spec2
        //            ELSE a.MatName END MatDesc

        //            , a.Color

        //            , b.MH1

        //            , a.OutSourcing

        //            , CASE WHEN p.VCD IS NULL THEN c.MfgVendor
        //                  ELSE p.VCD END Vendor

        //            , CASE WHEN m.MatCD IS NOT NULL THEN 1

        //            ELSE 0 END MultiUnit
        //            , c.PUnit PUnitLast
        //            , d.BrandCD


        //            , c.PriceLotSize PriceLotSizePInfo
        //            , c.PriceIncoterms PriceIncotermsPInfo
        //            , c.CIFCurr


        //            , e.PRICE* c.PriceLotSize PriceGR
        //            , e.CURR CURRGR



        //            , c.CIFPrice LastPricePInfo
        //            , e.PRICE LastPriceGR
        //            , e.PUNIT PUNITLastPriceGR
        //            , e.ExRate

        //            , e.IDATE

        //            , c.ExRate ExRateTPInfor
        //            , c.ValidFrom

        //            , f.Qty QtyBS
        //            , f.Unit PUnitBasicStock
        //            , h.QTYGR

        //            , h.PUNIT PUNITGR
        //            , g.QTYGI

        //            , g.PUNIT PUNITGI
        //            , (ISNULL(f.Qty,0) + ISNULL(h.QTYGR,0)) - ISNULL(g.QTYGI,0)  QtyEndingS
        //			,q.Qty QtyInspect
        //            , q.Unit PUnitInspect
        //             , q.CreatedOn DateInspect

        //    INTO #SQMatStockMonthly
        //	FROM  dbo.TMat a(NOLOCK)

        //    LEFT JOIN  dbo.TMH b(NOLOCK) ON a.MHID=b.MHID
        //   LEFT JOIN #TPInfoLaster c ON a.MatCD=c.MatCD
        //	LEFT JOIN #BrandCD d ON a.MatCD=d.MatCD
        //	LEFT JOIN #GRPriceLatest e ON e.MATCD=a.MatCD
        //	LEFT JOIN #Vendor p ON a.MatCD=p.MATCD
        //	LEFT JOIN #STBasicStock f ON a.MatCD=f.MATCD AND f.StartYear=@Year AND f.StartMonth=@Month AND f.Qty<>0
        //	LEFT JOIN #GI g ON a.MatCD=g.MATCD
        //	LEFT JOIN #GRConverPUNITfINAL h ON a.MatCD=h.MATCD
        //	LEFT JOIN #TPInfoGRMutiPunit m ON a.MatCD=m.MatCD
        //	--LEFT JOIN #TExRate n (NOLOCK) ON n.BYear=e.YEAR AND e.CURR=n.BCurrCD
        //	--LEFT JOIN #TExRateGRTPInfor o (NOLOCK) ON  c.CIFCurr=o.BCurrCD
        //	--LEFT JOIN #STBasicStock q ON q.MatCD=a.MatCD AND q.StartYear=YEAR(DATEDIFF(MONTH,-1,@NgayCuoiThang)) 
        //	--AND q.StartMonth= MONTH(DATEDIFF(MONTH, -1, @NgayCuoiThang))

        //   LEFT JOIN #STBasicStock q ON q.MatCD=a.MatCD AND q.CloseYear=@Year AND q.CloseMonth=@Month AND q.Qty<>0 


        //   WHERE a.Discontinue= 0 AND (@Outsourcing= 'ALL' OR (ISNULL(a.OutSourcing,0)=0 AND @Outsourcing = 'false') OR(ISNULL(a.OutSourcing,0)=1 AND @Outsourcing = 'true'))

        //    OPTION(RECOMPILE)

        //	-- AND a.MatCD='M0000159'

        //	DROP TABLE #TPInfoGRMutiPunit,#TPInfoLaster,#GRPriceLatest,#GI,#GRConverPUNITfINAL,#TExRate,#TExRateGRTPInfor,#Vendor,#BrandCD,#STBasicStock


        //    SELECT a.MatCD

        //            , a.MatDesc

        //            , a.Color

        //            , a.MH1

        //            , a.OutSourcing

        //            , a.Vendor

        //            , a.MultiUnit

        //            , a.PUnitLast

        //            , a.PUNITLastPriceGR

        //            , CASE WHEN a.MultiUnit= 1 AND a.PUnitLast= 'SET' THEN 'PC'

        //                    WHEN a.MultiUnit= 1 AND a.PUnitLast= 'YRD' THEN 'M'

        //                    WHEN a.MultiUnit= 1 AND a.PUnitLast= 'SQM' THEN 'SQF'

        //            ELSE a.PUnitLast END PUnitGRChuan

        //            , a.BrandCD

        //			--, a.PriceLotSizePInfo
        //			--, a.PriceIncotermsPInfo

        //            , a.LastPricePInfo

        //            , a.LastPriceGR



        //            , a.PriceLotSizePInfo

        //            , a.PriceIncotermsPInfo

        //            , a.CIFCurr

        //            , a.PriceGR

        //            , a.CURRGR



        //            , a.ExRateTPInfor

        //            , a.ValidFrom

        //            , a.ExRate

        //            , a.IDATE

        //            , a.QtyBS

        //            , a.PUnitBasicStock

        //            , a.QTYGR

        //            , a.PUNITGR

        //            , a.QTYGI

        //            , a.PUNITGI

        //            , a.QtyEndingS

        //            , a.QtyInspect

        //            , a.PUnitInspect

        //            , a.DateInspect
        //    INTO #SQMatStockMonthlyUnitChuan
        //	FROM #SQMatStockMonthly a

        //	DROP TABLE #SQMatStockMonthly

        //	SELECT   a.MatCD

        //            , a.MatDesc

        //            , a.Color

        //            , a.MH1

        //            , a.OutSourcing

        //            , a.Vendor

        //            , a.MultiUnit

        //            , a.PUnitLast

        //            , ISNULL(a.PUnitGRChuan, a.PUnitGR) PUnitGRChuan
        //            , a.BrandCD


        //            , a.PriceLotSizePInfo

        //            , a.PriceIncotermsPInfo* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUnitLast,0)) PriceIncotermsPInfo
        //			,(a.LastPricePInfo* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUnitLast,0)))*a.ExRateTPInfor LastPricePInfo
        //             , a.CIFCurr
        //			,a.PriceGR* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)) PriceGR
        //			,a.CURRGR			


        //			--,CASE WHEN a.PriceIncotermsPInfo = 0 OR a.PriceIncotermsPInfo*a.ExRateTPInfor = 0  THEN NULL
        //			--		      WHEN a.CIFCurr= a.CURRGR  THEN ((a.PriceGR* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)))/a.PriceIncotermsPInfo)*100
        //			--		ELSE((a.PriceGR* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0))*a.ExRate)/a.PriceIncotermsPInfo* a.ExRateTPInfor)*100 END Comp

        //			--, CASE WHEN a.PriceLotSizePInfo = 0 OR a.PriceGR = 0  THEN NULL
        //			--		WHEN a.CIFCurr= a.CURRGR  THEN (((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))
        //			--		- a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast, a.PUnitGRChuan) )
        //			--		/(a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan)))*100

        //			--ELSE(((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))*a.ExRate
        //			--- (a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast, a.PUnitGRChuan))*a.ExRateTPInfor)
        //			--/(a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))*a.ExRate)*100 END Comp

        //             , CASE WHEN a.PriceLotSizePInfo = 0 OR a.PriceGR = 0  THEN NULL

        //                      WHEN a.CIFCurr= a.CURRGR  THEN ((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))
        //			-(a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast, a.PUnitGRChuan)))
        //			/((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan)))*100 

        //			ELSE((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))*a.ExRate 			
        //			-(a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast, a.PUnitGRChuan))*a.ExRateTPInfor)
        //			/((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))*a.ExRate)*100  END Comp


        //             , f.Payment



        //			--,a.PriceIncotermsPInfo
        //			--,a.CIFCurr





        //			,a.ValidFrom
        //			,a.LastPriceGR LastPriceGRtt
        //            , a.PUNITLastPriceGR
        //			,(a.LastPriceGR* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)))*a.ExRate LastPriceGR
        //             , a.IDATE
        //			,a.ExRate
        //			--,ISNULL(a.QtyBS,0) QtyBS
        //			,ISNULL(a.QtyBS,0)*dbo.ConvQtyGR(ISNULL(a.PUnitBasicStock,''),ISNULL(a.PUnitGRChuan,0)) QtyBS
        //			,a.QTYGR* dbo.ConvQtyGR(ISNULL(a.PUnitGR,''),ISNULL(a.PUnitGRChuan,0)) QTYGR
        //			--,a.PUnitGR
        //			,a.QTYGI* dbo.ConvQtyGR(ISNULL(a.PUNITGI,''),ISNULL(a.PUnitGRChuan,0)) QTYGI
        //			,a.PUNITGI
        //			--,ISNULL(a.QtyInspect,0) QtyInspect
        //			,ISNULL(a.QtyInspect,0)*dbo.ConvQtyGR(ISNULL(a.PUnitInspect,''),ISNULL(a.PUnitGRChuan,0)) QtyInspect
        //			,a.DateInspect
        //    INTO #SQMatStockMonthlyFinal
        //	FROM #SQMatStockMonthlyUnitChuan a
        //	LEFT JOIN STCustomsMat f(NOLOCK) ON a.MatCD=f.MatCD
        //	--WHERE a.MatCD= 'M0046983'

        //   ORDER BY a.MatCD
        //   OPTION (RECOMPILE)




        //   DROP TABLE #SQMatStockMonthlyUnitChuan

        //	SELECT MatCD

        //          , SUM(QtyUnCut+QtySubStore) QtyEU
        //    INTO #tb_TTWIPMat
        //	FROM dbo.TTWIPMat(NOLOCK)

        //    WHERE @Year = Year AND @Month = Month

        //    GROUP BY MatCD
        //    OPTION(RECOMPILE)



        //    SELECT IDENTITY(int ) AS RowID
        //          , a.MatCD
        //				,a.MatDesc 
        //				,a.Color
        //				,a.MH1
        //			    ,a.OutSourcing
        //				,a.Vendor
        //				--,a.MultiUnit
        //				--,a.PUnitLast
        //				,a.PUnitGRChuan PUnit
        //                , a.BrandCD

        //				--,a.PriceLotSizePInfo
        //			 --   ,a.PriceIncotermsPInfo
        //			,a.LastPricePInfo

        //			,a.PriceLotSizePInfo
        //			,a.PriceIncotermsPInfo
        //			,a.CIFCurr
        //			,a.PriceGR
        //			,a.CURRGR			
        //			,a.Comp
        //			,a.Payment



        //				,a.ValidFrom
        //				,a.LastPriceGR
        //				,a.IDATE
        //				--,ExRate
        //				,CASE WHEN a.LastPriceGR IS NULL THEN a.LastPricePInfo
        //                 ELSE a.LastPriceGR END UnitPrice
        //				,a.QtyBS
        //				,ISNULL(a.QTYGR,0)  QTYGR
        //				,ISNULL(a.QTYGI,0) QTYGI
        //				,ROUND((ISNULL(a.QtyBS,0) + ISNULL(a.QTYGR,0)) - ISNULL(a.QTYGI,0),5)  QtyEndingS 
        //				,a.QtyInspect
        //				,a.DateInspect	

        //				,b.QtyEU
        //    INTO #SQMatStockMonthlyRowID
        //	FROM #SQMatStockMonthlyFinal a
        //	LEFT JOIN #tb_TTWIPMat b ON a.MatCD=b.MatCD


        //	SELECT IDENTITY(int ) AS RowID
        //           , MatCD
        //           , MatDesc
        //           , Color
        //           , MH1
        //           , OutSourcing
        //           , Vendor
        //           , PUnit
        //           , BrandCD

        //			--, PriceLotSizePInfo
        //			--, PriceIncotermsPInfo
        //           , LastPricePInfo
        //           , LastPriceGR

        //           , PriceLotSizePInfo
        //           , PriceIncotermsPInfo
        //           , CIFCurr
        //           , PriceGR
        //           , CURRGR
        //           , ROUND(Comp, 0) Comp
        //			,Payment


        //			,UnitPrice
        //			,QtyBS
        //			,QTYGR
        //			,QTYGI
        //			,QtyEndingS 
        //			,QtyBS* UnitPrice AmtBS
        //			,QTYGR* UnitPrice AmtGR
        //			,QTYGI* UnitPrice AmtGI
        //			,QtyEndingS* UnitPrice AmtES
        //			,QtyInspect
        //			,ROUND(QtyInspect - QtyEndingS,2) QtyDiff
        //			,DateInspect

        //			,QtyEU
        //			,(QtyEU+QtyEndingS) QtyESSum
        //			,QtyEU* UnitPrice AmtEU
        //			,(QtyEU+QtyEndingS)*UnitPrice AmtESSum
        //    INTO #SQMatStock
        //	FROM #SQMatStockMonthlyRowID
        //	ORDER BY MatCD

        //    DECLARE @SumAllQtyBS FLOAT, @SumAllQTYGR FLOAT,@SumAllQTYGI FLOAT, @SumAllQtyEndingS FLOAT,@SumAllAmtBS FLOAT, @SumAllAmtGR FLOAT
        //	,@SumAllAmtGI FLOAT, @SumAllAmtES FLOAT,@SumAllQtyInspect FLOAT, @SumAllQtyDiff FLOAT,@SumAllQtyEU FLOAT, @SumAllQtyESSum FLOAT
        //	,@SumAllAmtEU FLOAT, @SumAllAmtESSum FLOAT


        //     SELECT   @SumAllQtyBS=SUM(QtyBS)
        //			,@SumAllQTYGR=SUM(QTYGR)
        //			,@SumAllQTYGI=SUM(QTYGI)
        //			,@SumAllQtyEndingS=SUM(QtyEndingS)
        //			,@SumAllAmtBS=SUM(AmtBS)
        //			,@SumAllAmtGR=SUM(AmtGR)
        //			,@SumAllAmtGI=SUM(AmtGI)
        //			,@SumAllAmtES=SUM(AmtES)
        //			,@SumAllQtyInspect=SUM(QtyInspect)
        //			,@SumAllQtyDiff=SUM(QtyDiff)
        //			,@SumAllQtyEU=SUM(QtyEU)
        //			--,@SumAllQtyESSum=SUM(QtyESSum)
        //			,@SumAllAmtEU=SUM(AmtEU)
        //			,@SumAllAmtESSum=SUM(AmtESSum)

        //    FROM #SQMatStock

        //	DROP TABLE #SQMatStockMonthlyFinal

        //	IF @Type=0
        //	BEGIN
        //                DECLARE @TotalRows INT

        //                SELECT @TotalRows = COUNT(RowID) FROM #SQMatStock


        //				SELECT TOP 15  @TotalRows TotalRows
        //                        , RowID
        //                        , MatCD
        //                        , MatDesc
        //                        , Color
        //                        , MH1
        //                        , OutSourcing
        //                        , Vendor
        //                        , PUnit
        //                        , BrandCD

        //                        , LastPricePInfo
        //                        , LastPriceGR

        //                        , PriceLotSizePInfo
        //                        , PriceIncotermsPInfo
        //                        , CIFCurr
        //                        , PriceGR
        //                        , CURRGR
        //                        , Comp
        //                        , Payment

        //                        , UnitPrice
        //                        , QtyBS
        //                        , QTYGR
        //                        , QTYGI
        //                        , QtyEndingS
        //                        , AmtBS
        //                        , AmtGR
        //                        , AmtGI
        //                        , AmtES
        //                        , QtyInspect
        //                        , QtyDiff
        //                        , REPLACE(convert(varchar, DateInspect, 106), ' ', '/') DateInspectConvert
        //						,QtyEU
        //						,QtyESSum
        //						,AmtEU
        //						,AmtESSum
        //						,@SumAllQtyBS SumAllQtyBS
        //                        , @SumAllQTYGR      SumAllQTYGR
        //						,@SumAllQTYGI SumAllQTYGI

        //                        , @SumAllQtyEndingS SumAllQtyEndingS
        //						,@SumAllQtyEU SumAllQtyEU
        //                        , @SumAllQtyEndingS + @SumAllQtyEU SumAllQtyESSum


        //                         , @SumAllAmtBS      SumAllAmtBS
        //						,@SumAllAmtGR SumAllAmtGR
        //                        , @SumAllAmtGI      SumAllAmtGI
        //						,@SumAllAmtES SumAllAmtES
        //                        , @SumAllAmtEU      SumAllAmtEU
        //						,@SumAllAmtESSum SumAllAmtESSum


        //                        , @SumAllQtyInspect SumAllQtyInspect
        //						,@SumAllQtyDiff SumAllQtyDiff

        //                FROM #SQMatStock
        //				WHERE RowID > ((@PageNumber-1)*@PageSize)
        //				ORDER BY MatCD
        //                OPTION(RECOMPILE)





        //    END

        //    IF @Type=1
        //	BEGIN

        //                SELECT

        //                        MatCD
        //						,MatDesc
        //						,Color	
        //						,MH1
        //						,PUnit

        //			            ,OutSourcing
        //						,Vendor
        //						,BrandCD

        //						,PriceLotSizePInfo 'PriceLot'
        //						,PriceIncotermsPInfo 'PricePInfo'
        //						,CIFCurr 'Curr'

        //						,PriceGR
        //						,CURRGR	'Curr_Double0'
        //						,Comp 'Diff(%)_Percent0'
        //						,Payment
        //						,UnitPrice


        //						--,PriceLotSizePInfo
        //						--,PriceIncotermsPInfo
        //						--,LastPricePInfo
        //						--,LastPriceGR
        //						,QtyBS
        //						,QTYGR
        //						,QTYGI
        //						,QtyEndingS 'QtyES'
        //						,QtyEU
        //						,QtyESSum
        //						,AmtBS
        //						,AmtGR
        //						,AmtGI
        //						,AmtES
        //						,AmtEU
        //						,AmtESSum
        //						,QtyInspect
        //						,QtyDiff
        //						,REPLACE(convert(varchar, DateInspect, 106),' ','/') DateInspect

        //						,'Total' Total_Total_16
        //						,@SumAllQtyBS Total_SumAllQtyBS_Format2_17
        //                        , @SumAllQTYGR      Total_SumAllQTYGR_Format2_18
        //						,@SumAllQTYGI Total_SumAllQTYGI_Format2_19
        //                        , @SumAllQtyEndingS Total_SumAllQtyEndingS_Format2_20
        //						,@SumAllQtyEU Total_SumAllQtyEU_Format2_21
        //                        , @SumAllQtyEndingS + @SumAllQtyEU Total_SumAllQtyESSum_Format2_22
        //                         , @SumAllAmtBS      Total_SumAllAmtBS_Format2_23
        //						,@SumAllAmtGR Total_SumAllAmtGR_Format2_24
        //                        , @SumAllAmtGI      Total_SumAllAmtGI_Format2_25
        //						,@SumAllAmtES Total_SumAllAmtES_Format2_26
        //                        , @SumAllAmtEU      Total_SumAllAmtEU_Format2_27
        //						,@SumAllAmtESSum Total_SumAllAmtESSum_Format2_28
        //                        , @SumAllQtyInspect Total_SumAllQtyInspect_Format2_29
        //						,@SumAllQtyDiff Total_SumAllQtyDiff_Format2_30

        //                FROM #SQMatStock
        //				ORDER BY MatCD



        //    END

        //    IF @Type=2
        //	BEGIN




        //                SELECT

        //                        c.MatCD
        //						,SUM(c.QTY) QTYGR
        //						,c.PUNIT PUNITGR
        //                        , h.PUnit PUnitChuan

        //                INTO #GRNowMatCD
        //				FROM dbo.STIn_List c(NOLOCK)

        //                INNER JOIN dbo.STIn g(NOLOCK) ON g.IVID=c.IVID
        //               INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
        //				WHERE YEAR(g.IDATE) = YEAR(GETDATE()) AND MONTH(g.IDATE) = MONTH(GETDATE())

        //               AND DAY(g.IDATE) = DAY(GETDATE())

        //               AND c.MATCD IS NOT NULL

        //               GROUP BY c.MatCD, c.PUNIT, h.PUnit
        //               ORDER BY c.MatCD
        //               OPTION (RECOMPILE)


        //               SELECT MATCD

        //                     , SUM(ISNULL(QTYGR,0)*dbo.ConvQtyGR(ISNULL(PUNITGR,0),ISNULL(PUnitChuan,0))) QTYGRNow
        //                INTO #GRNow
        //				FROM #GRNowMatCD
        //				GROUP BY MATCD


        //                DROP TABLE #GRNowMatCD





        //                SELECT c.MatCD

        //                        , SUM(CASE WHEN e.RSign= '-' THEN -(c.OQTY)
        //                        ELSE c.OQTY END) QTYGI
        //						,c.PUNIT PUNITGI
        //                        , h.PUnit PUnitChuan

        //                INTO #GINowMatCD
        //				FROM STOut_List c(NOLOCK)

        //                INNER JOIN dbo.STOut g(NOLOCK) ON g.OUTCD=c.OUTCD
        //               INNER JOIN STReason e (NOLOCK) ON c.RCD= e.RCD

        //               INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
        //				WHERE YEAR(g.ODATE) = YEAR(GETDATE()) AND MONTH(g.ODATE) = MONTH(GETDATE())

        //               AND DAY(g.ODATE) = DAY(GETDATE())

        //               AND c.MATCD IS NOT NULL

        //               GROUP BY c.MatCD, c.PUNIT, h.PUnit
        //               ORDER BY c.MatCD
        //               OPTION (RECOMPILE)




        //               SELECT MATCD

        //                     , SUM(ISNULL(QTYGI,0)*dbo.ConvQtyGR(ISNULL(PUNITGI,0),ISNULL(PUnitChuan,0))) QTYGINow
        //                INTO #GINow
        //				FROM #GINowMatCD
        //				GROUP BY MATCD



        //                DROP TABLE #GINowMatCD




        //                SELECT
        //                        MH1
        //                       ,COUNT(a.MH1) Count
        //					   ,SUM(a.QtyBS* a.UnitPrice) AmtBS
        //					   ,SUM(a.QTYGR* UnitPrice) AmtGR
        //					   ,SUM(a.QTYGI* a.UnitPrice) AmtGI
        //					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice) AmtES
        //					   ,SUM(a.QtyEU* a.UnitPrice) AmtEU
        //					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice + a.QtyEU* a.UnitPrice) AmtESSum

        //					   --,SUM(b.QTYGRNow* a.UnitPrice) AmtGRNow
        //					   --,SUM(c.QTYGINow* a.UnitPrice) AmtGINow
        //					   ,1 RowID
        //                INTO #SQMatStockMonthlyGR
        //				FROM #SQMatStockMonthlyRowID a
        //				LEFT JOIN #GRNow b ON a.MatCD=b.MATCD
        //				LEFT JOIN #GINow c ON a.MatCD=c.MATCD
        //				GROUP BY a.MH1


        //                DROP TABLE #GRNow,#GINow


        //                        DECLARE @ToTal_CountSum FLOAT

        //                                , @ToTal_AmtBSSum FLOAT
        //                                , @ToTal_AmtGRSum FLOAT
        //                                , @ToTal_AmtGISum FLOAT
        //                                , @ToTal_AmtESSum FLOAT
        //                                , @ToTal_AmtEUSum FLOAT
        //                                , @ToTal_AmtESSumSum FLOAT




        //                SELECT @ToTal_CountSum = SUM(Count)
        //                       , @ToTal_AmtBSSum= SUM(AmtBS)
        //                       , @ToTal_AmtGRSum= SUM(AmtGR)
        //                       , @ToTal_AmtGISum= SUM(AmtGI)
        //                       , @ToTal_AmtESSum= SUM(AmtES)
        //                       , @ToTal_AmtEUSum= SUM(AmtEU)
        //                       , @ToTal_AmtESSumSum= SUM(AmtESSum)

        //                FROM #SQMatStockMonthlyGR




        //				SELECT  MH1

        //                        , Count

        //                        , ROUND(AmtBS,2) AmtBS
        //						,ROUND(AmtGR,2) AmtGR
        //						,ROUND(AmtGI,2) AmtGI
        //						,ROUND(AmtES,2) AmtES
        //						,ROUND(ISNULL(AmtEU,0),2) AmtEU
        //						,ROUND(ISNULL(AmtESSum,0),2) AmtESSum
        //						,@ToTal_CountSum ToTal_CountSum
        //                        , @ToTal_AmtBSSum       ToTal_AmtBSSum
        //						,@ToTal_AmtGRSum ToTal_AmtGRSum
        //                        , @ToTal_AmtGISum       ToTal_AmtGISum
        //						,@ToTal_AmtESSum ToTal_AmtESSum
        //                        , @ToTal_AmtEUSum       ToTal_AmtEUSum
        //						,@ToTal_AmtESSumSum ToTal_AmtESSumSum


        //                FROM #SQMatStockMonthlyGR
        //				ORDER BY RowID


        //                DROP TABLE #SQMatStockMonthlyGR



        //    END
        //    IF @Type=3
        //	BEGIN




        //                SELECT

        //                        c.MatCD
        //						,SUM(c.QTY) QTYGR
        //						,c.PUNIT PUNITGR
        //                        , h.PUnit PUnitChuan

        //                INTO #GRNowMatCD_Excel
        //				FROM dbo.STIn_List c(NOLOCK)

        //                INNER JOIN dbo.STIn g(NOLOCK) ON g.IVID=c.IVID
        //               INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
        //				WHERE YEAR(g.IDATE) = YEAR(GETDATE()) AND MONTH(g.IDATE) = MONTH(GETDATE())

        //               AND DAY(g.IDATE) = DAY(GETDATE())

        //               AND c.MATCD IS NOT NULL

        //               GROUP BY c.MatCD, c.PUNIT, h.PUnit
        //               ORDER BY c.MatCD
        //               OPTION (RECOMPILE)


        //               SELECT MATCD

        //                     , SUM(ISNULL(QTYGR,0)*dbo.ConvQtyGR(ISNULL(PUNITGR,0),ISNULL(PUnitChuan,0))) QTYGRNow
        //                INTO #GRNow_Excel
        //				FROM #GRNowMatCD_Excel
        //				GROUP BY MATCD


        //                DROP TABLE #GRNowMatCD_Excel





        //                SELECT c.MatCD

        //                        , SUM(CASE WHEN e.RSign= '-' THEN -(c.OQTY)
        //                        ELSE c.OQTY END) QTYGI
        //						,c.PUNIT PUNITGI
        //                        , h.PUnit PUnitChuan

        //                INTO #GINowMatCD_Excel
        //				FROM STOut_List c(NOLOCK)

        //                INNER JOIN dbo.STOut g(NOLOCK) ON g.OUTCD=c.OUTCD
        //               INNER JOIN STReason e (NOLOCK) ON c.RCD= e.RCD

        //               INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
        //				WHERE YEAR(g.ODATE) = YEAR(GETDATE()) AND MONTH(g.ODATE) = MONTH(GETDATE())

        //               AND DAY(g.ODATE) = DAY(GETDATE())

        //               AND c.MATCD IS NOT NULL

        //               GROUP BY c.MatCD, c.PUNIT, h.PUnit
        //               ORDER BY c.MatCD
        //               OPTION (RECOMPILE)




        //               SELECT MATCD

        //                     , SUM(ISNULL(QTYGI,0)*dbo.ConvQtyGR(ISNULL(PUNITGI,0),ISNULL(PUnitChuan,0))) QTYGINow
        //                INTO #GINow_Excel
        //				FROM #GINowMatCD_Excel
        //				GROUP BY MATCD



        //                DROP TABLE #GINowMatCD_Excel




        //                SELECT
        //                        MH1
        //                       ,COUNT(a.MH1) Count
        //					   ,SUM(a.QtyBS* a.UnitPrice) AmtBS
        //					   ,SUM(a.QTYGR* UnitPrice) AmtGR
        //					   ,SUM(a.QTYGI* a.UnitPrice) AmtGI
        //					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice) AmtES
        //					   ,SUM(a.QtyEU* a.UnitPrice) AmtEU
        //					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice + a.QtyEU* a.UnitPrice) AmtESSum
        //					   ,1 RowID
        //                INTO #SQMatStockMonthlyGR_Excel
        //				FROM #SQMatStockMonthlyRowID a
        //				LEFT JOIN #GRNow_Excel b ON a.MatCD=b.MATCD
        //				LEFT JOIN #GINow_Excel c ON a.MatCD=c.MATCD
        //				GROUP BY a.MH1


        //                DROP TABLE #GRNow_Excel,#GINow_Excel



        //                        DECLARE @ToTal_CountSum_Excel FLOAT

        //                                , @ToTal_AmtBSSum_Excel FLOAT
        //                                , @ToTal_AmtGRSum_Excel FLOAT
        //                                , @ToTal_AmtGISum_Excel FLOAT
        //                                , @ToTal_AmtESSum_Excel FLOAT
        //                                , @ToTal_AmtEUSum_Excel FLOAT
        //                                , @ToTal_AmtESSumSum_Excel FLOAT




        //                SELECT @ToTal_CountSum_Excel = SUM(Count)
        //                       , @ToTal_AmtBSSum_Excel= SUM(AmtBS)
        //                       , @ToTal_AmtGRSum_Excel= SUM(AmtGR)
        //                       , @ToTal_AmtGISum_Excel= SUM(AmtGI)
        //                       , @ToTal_AmtESSum_Excel= SUM(AmtES)
        //                       , @ToTal_AmtEUSum_Excel= SUM(AmtEU)
        //                       , @ToTal_AmtESSumSum_Excel= SUM(AmtESSum)

        //                FROM #SQMatStockMonthlyGR_Excel





        //				SELECT  MH1 'Categories'
        //						, Count Count_Format2
        //                        , ROUND(AmtBS,2) 'AmtBS_Format2'
        //						,ROUND(AmtGR,2) 'AmtGR_Format2'
        //						,ROUND(AmtGI,2) 'AmtGI_Format2'
        //						,ROUND(AmtES,2) 'AmtES_Format2' 
        //						,ROUND(AmtEU,2) 'AmtEU_Format2' 
        //						,ROUND(AmtESSum,2) 'AmtESSum_Format2' 
        //						,'ToTal'    Total_ToTal_1 
        //						,@ToTal_CountSum_Excel Total_CountSum_Format2_2
        //                        , @ToTal_AmtBSSum_Excel  Total_AmtBSSum_Format2_3
        //						,@ToTal_AmtGRSum_Excel Total_AmtGRSum_Format2_4
        //                        , @ToTal_AmtGISum_Excel  Total_AmtGISum_Format2_5 
        //						,@ToTal_AmtESSum_Excel Total_AmtESSum_Format2_6
        //                        , @ToTal_AmtEUSum_Excel  Total_AmtEUSum_Format2_7 
        //						,@ToTal_AmtESSumSum_Excel Total_AmtESSum_Format2_8


        //                FROM #SQMatStockMonthlyGR_Excel
        //				ORDER BY RowID


        //                DROP TABLE #SQMatStockMonthlyGR_Excel



        //    END
        //    DROP TABLE #SQMatStockMonthlyRowID




        //END






























        //[HttpGet]
        //[Route("ERP_Report_SQMatStockMonthlyV2")]
        //public async Task<IList<SQMatStockMonthlyModel>> ERP_Report_SQMatStockMonthlyV2(string Outsourcing, string datemonth, int PageNumber, int PageSize)
        //{
        //    try
        //    {
        //        List<SQMatStockMonthlyModel> result = new List<SQMatStockMonthlyModel>();
        //        DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
        //        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
        //        {

        //            var queryParameters = new DynamicParameters();
        //            queryParameters.Add("@Month", date.Month);
        //            queryParameters.Add("@Year", date.Year);
        //            queryParameters.Add("@Outsourcing", Outsourcing);
        //            queryParameters.Add("@PageSize", PageSize);
        //            queryParameters.Add("@PageNumber", PageNumber);
        //            queryParameters.Add("@Type", 0);
        //            connection.Open();
        //            var data = await connection.QueryAsync<SQMatStockMonthlyModel>("ERP_Report_SQMatStockMonthly_List_V2"
        //                , queryParameters, commandTimeout: 1500
        //                , commandType: CommandType.StoredProcedure);
        //            return data.ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new NotImplementedException(ex.Message.ToString());
        //    }
        //}
        //[HttpGet]
        //[Route("ERP_Report_SQMatStockMonthlyV2Excel")]
        //public HttpResponseMessage ERP_Report_SQMatStockMonthlyV2Excel(string Outsourcing, string datemonth)
        //{
        //    try
        //    {
        //        DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
        //        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
        //        {

        //            DataTable table = new DataTable();
        //            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
        //            SqlCommand cmd = new SqlCommand("ERP_Report_SQMatStockMonthly_List_V2", con);
        //            cmd.CommandTimeout = 1500;
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.Add("@Month", SqlDbType.Int).Value = date.Month;
        //            cmd.Parameters.Add("@Year", SqlDbType.Int).Value = date.Year;
        //            cmd.Parameters.Add("@Outsourcing", SqlDbType.NVarChar).Value = Outsourcing;
        //            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;


        //            using (var da = new SqlDataAdapter(cmd))
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                da.Fill(table);
        //            }

        //            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
        //            string filename = "MatStockMonthly";
        //            Byte[] bytes = ExportDataTableToExcelFromStore(table, filename, true, false);
        //            response.Content = new ByteArrayContent(bytes);
        //            response.Content.Headers.ContentLength = bytes.LongLength;
        //            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        //            response.Content.Headers.ContentDisposition.FileName = filename;
        //            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
        //            return response;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new NotImplementedException(ex.Message.ToString());
        //    }
        //}







        public async Task<IList<SQMatStockMonthlyGroupModel>> ERP_Report_SQMatStockMonthlyGR(string database,string outsourcing, string datemonth)
        {
            try
            {
                List<SQMatStockMonthlyGroupModel> result = new List<SQMatStockMonthlyGroupModel>();
                DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                using (var connection = new SqlConnection(database))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@Outsourcing", outsourcing);
                    queryParameters.Add("@Type", 2);
                    connection.Open();
                    var data = await connection.QueryAsync<SQMatStockMonthlyGroupModel>("ERP_Report_SQMatStockMonthly_List"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
            //try
            //{
            //    List<SQMatStockMonthlyGroupModel> result = new List<SQMatStockMonthlyGroupModel>();
            //    DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
            //    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            //    {
            //        var queryParameters = new DynamicParameters();
            //        queryParameters.Add("@Month", date.Month);
            //        queryParameters.Add("@Year", date.Year);
            //        queryParameters.Add("@Outsourcing", outsourcing);
            //        queryParameters.Add("@Type", 2);
            //        connection.Open();
            //        var data = await connection.QueryAsync<SQMatStockMonthlyGroupModel>("ERP_Report_SQMatStockMonthly_List"
            //            , queryParameters, commandTimeout: 1500
            //            , commandType: CommandType.StoredProcedure);
            //        result = data.ToList();
            //    }
            //    return result;
            //}
            //catch (Exception ex)
            //{
            //    throw new NotImplementedException(ex.Message.ToString());
            //}
        }
        public Byte[] ERP_Report_SQMatStockMonthlyGR_Excel(string database,string outsourcing, string datemonth)
        {
            try
            {
                DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(database);
                SqlCommand cmd = new SqlCommand("ERP_Report_SQMatStockMonthly_List", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = date.Month;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = date.Year;
                cmd.Parameters.Add("@Outsourcing", SqlDbType.NVarChar).Value = outsourcing;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 3;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                string filename = "MatStockMonthly" + date.ToString("yyyy/HH") + ".xlsx";
                return ExportDataTableToExcelFromStore(table, "Categories Group", true, false);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }















//        USE[PARKERP]
//        GO
///****** Object:  StoredProcedure [dbo].[ERP_Report_SQMatStockMonthly_List_V2]    Script Date: 2024/03/13 8:11:45 AM ******/
//SET ANSI_NULLS ON
//GO
//SET QUOTED_IDENTIFIER ON
//GO



//-- =============================================
//-- Author:		<Author,,Name>
//-- Create date: <Create Date,,>
//-- Description:	<Description,,>
//-- =============================================
//ALTER PROCEDURE[dbo].[ERP_Report_SQMatStockMonthly_List_V2]
//        @Month INT = 8,
//   @Year INT=2022,
//	@Outsourcing NVARCHAR(10),
//	@Type INT = 0,
//    @PageNumber INT=0,
//	@PageSize INT = 0
//AS
//BEGIN

//--declare @Month INT
//--SET @Month = 3
//--declare @Year INT
//--SET @Year = 2023
//--declare @Outsourcing NVARCHAR(10)
//--SET @Outsourcing = 'false'
//--declare @Type INT
//--SET @Type = 1
//--declare @PageNumber INT
//--SET @PageNumber = 1
//--declare @PageSize INT
//--SET @PageSize = 15
//    --M0060824
//    SET NOCOUNT ON;

//        DECLARE @NgayDauThang DATETIME,@NgayCuoiThang DATETIME

//    SET @NgayDauThang = CONVERT(DATETIME, CAST(@Year AS varchar(4)) + '-' + CAST(@Month AS varchar(4)) + '-1')


//    SET @NgayCuoiThang = DATEADD(DAY, -1, DATEADD(month, 1, @NgayDauThang))



//    SELECT PInfoID, MatCD, PUnit, MfgVendor, CASE WHEN Incoterms = 'CIF' THEN ISNULL(CIFPrice,0)/NULLIF(PriceLotSize,0)

//        WHEN Incoterms = 'EXW' THEN ISNULL(EXWPrice,0)/NULLIF(PriceLotSize,0)

//        WHEN Incoterms = 'FOB' THEN ISNULL(FOBPrice,0)/NULLIF(PriceLotSize,0)

//        WHEN Incoterms = 'DAP' THEN ISNULL(CIFPrice,0)/NULLIF(PriceLotSize,0)

//    ELSE ISNULL(FOBcPrice,0)/NULLIF(PriceLotSize,0) END CIFPrice
//    , CASE WHEN Incoterms = 'CIF' THEN CIFCurr

//        WHEN Incoterms = 'EXW' THEN EXWCurr

//        WHEN Incoterms = 'FOB' THEN FOBCurr

//        WHEN Incoterms = 'DAP' THEN CIFCurr

//    ELSE FOBCurr END CIFCurr
//    , CIFCurr CIFCurrTemp
//	,EXWCurr EXWCurrTemp
//    , FOBCurr FOBCurrTemp

//	,ValidFrom
//	,CASE WHEN Incoterms='CIF' THEN CIFPrice

//          WHEN Incoterms = 'EXW' THEN EXWPrice

//          WHEN Incoterms = 'FOB' THEN FOBPrice

//          WHEN Incoterms = 'DAP' THEN CIFPrice

//        ELSE FOBPrice END PriceIncoterms
//    , PriceLotSize

//    INTO #TPInfo
//	FROM dbo.TPInfo(NOLOCK)
//   WHERE ValidFrom <= @NgayCuoiThang
//   AND Discontinue=0
//	--WHERE CIFPrice IS NOT NULL --Discontinue=0

//	SELECT MAX(PInfoID) PInfoID,MatCD
//    INTO #TPInfoGRID
//	FROM #TPInfo
//	GROUP BY MatCD



//    SELECT MatCD, PUnit

//    INTO #TPInfoGRPUnit
//	FROM #TPInfo
//	GROUP BY MatCD,PUnit
//    ORDER BY MatCD


//    SELECT MatCD

//    INTO #TPInfoGRMutiPunit
//	FROM #TPInfoGRPUnit
//	GROUP BY MatCD
//    HAVING COUNT(MatCD) > 1

//	DROP TABLE #TPInfoGRPUnit


//    SELECT a.MatCD

//            , a.PUnit

//            , a.MfgVendor
//	--		, CASE WHEN a.CIFCurr IS NULL THEN 0
//	--ELSE a.CIFPrice END CIFPrice

//            , a.CIFPrice

//            , a.CIFCurr

//            , a.ValidFrom

//            , a.PriceLotSize

//            , a.PriceIncoterms

//            , b.ExRate

//            , CASE WHEN c.MatCD IS NOT NULL AND a.PUNIT= 'SET' THEN 'PC'

//                WHEN c.MatCD IS NOT NULL AND a.PUNIT= 'YRD' THEN 'M'

//                WHEN c.MatCD IS NOT NULL AND a.PUNIT= 'SQM' THEN 'SQF'

//                ELSE a.PUNIT END PUnitChuan
//    INTO #TPInfoLaster
//	FROM #TPInfo a
//	LEFT JOIN dbo.TTExRateDaily b (NOLOCK) ON a.ValidFrom= b.BDate  AND a.CIFCurr= b.BCurrCD

//    LEFT JOIN #TPInfoGRMutiPunit c ON a.MatCD=c.MatCD
//	WHERE a.PInfoID IN (SELECT PInfoID FROM #TPInfoGRID)
	


//    DROP TABLE #TPInfo,#TPInfoGRID





//    SELECT PRID, MatCD, PRItemID
//    INTO #TPRItem
//	FROM dbo.TPRItem (NOLOCK)


//    SELECT MAX(PRID) PRID, MatCD
//    INTO #TPRItemGR
//	FROM #TPRItem 
//	GROUP BY MatCD




//    SELECT c.PRID, a.BrandCD, a.ProdID
//    INTO #PRIDBrandCD
//	FROM TProd a (NOLOCK)
//    INNER JOIN TProdInstItem b (NOLOCK) ON a.ProdID= b.ProdID

//    INNER JOIN TPRProdInst c (NOLOCK) ON  b.ProdInstCD= c.ProdInstCD


//    SELECT MAX(ProdID) ProdID, PRID, BrandCD
//    INTO #PRIDBrandCDGR
//	FROM #PRIDBrandCD
//	GROUP BY PRID, BrandCD

//    SELECT ProdID, PRID, BrandCD
//    INTO #PRIDBrandCDGRFinal
//	FROM #PRIDBrandCD
//	WHERE ProdID IN (SELECT ProdID FROM #PRIDBrandCDGR)



//    SELECT a.MatCD, MAX(b.BrandCD) BrandCD--, b.ProdID
//    INTO #BrandCD
//	FROM #TPRItemGR a
//	INNER JOIN #PRIDBrandCDGRFinal b ON a.PRID=b.PRID
//	GROUP BY a.MatCD--, b.ProdID--, b.BrandCD

//	--SELECT MatCD, MAX(ProdID) ProdID
//	--INTO #BrandCDGR
//	--FROM #BrandCD
//	--GROUP BY MatCD



//	--SELECT a.MatCD, a.BrandCD
//	--INTO #BrandFinal
//	--FROM #BrandCD a
//	--INNER JOIN #BrandCDGR B ON a.MatCD=b.MatCD AND a.ProdID=b.ProdID


//    DROP TABLE #TPRItem,#TPRItemGR,#PRIDBrandCDGR,#PRIDBrandCDGRFinal--,#BrandCD,#BrandCDGR



//        SELECT a.IVID

//                , a.MatCD

//                , a.PRICE

//                , a.CURR

//                , a.PUNIT

//                , a.VCD

//                , a.IVLISTNO

//                , ISNULL(b.IDATE, ISNULL(a.WDATE, ISNULL(a.UDATE, a.CreatedOn))) IDATE
//				,a.OUTCD
//        INTO #GR 
//		FROM STin_List a(NOLOCK)

//        LEFT JOIN STin b(NOLOCK) ON a.IVID=b.IVID
//       WHERE ISNULL(b.IDATE, ISNULL(a.WDATE, ISNULL(a.UDATE, a.CreatedOn))) <= @NgayCuoiThang
//		--WHERE 
//		--AND a.QTY > 0  AND a.OUTCD IS NULL-- AND a.MATCD= 'M0000220'



//        SELECT MAX(IVLISTNO) IVLISTNO
//               , MatCD
//        INTO #VendorGR
//		FROM #GR
//		GROUP BY MatCD


//        SELECT a.MatCD

//               , b.VCD
//        INTO #Vendor
//		FROM #VendorGR a
//		INNER JOIN #GR b ON a.IVLISTNO=b.IVLISTNO

//		DROP TABLE #VendorGR


//		SELECT MAX(IVLISTNO) IVLISTNO
//               , MatCD
//        INTO #GRLatest
//		FROM #GR
//		WHERE PRICE > 0 AND CURR IS NOT NULL AND OUTCD IS NULL
//        GROUP BY MatCD




//        SELECT a.MatCD

//               , b.PRICE

//               , b.PUNIT

//               , b.CURR

//               , c.ExRate

//               , b.IDATE
//        INTO #GRPriceLatest
//		FROM #GRLatest a
//		INNER JOIN #GR b ON a.IVLISTNO=b.IVLISTNO
//		LEFT JOIN dbo.TTExRateDaily c (NOLOCK) ON c.BDate= b.IDATE AND c.BCurrCD= b.CURR



//    DROP TABLE #GR,#GRLatest
 



//    SELECT OUTCD

//    INTO #STOut
//	FROM STOut (NOLOCK)
//    WHERE ODATE BETWEEN @NgayDauThang AND @NgayCuoiThang


//    SELECT c.MatCD

//            , SUM(CASE WHEN e.RSign= '-' THEN -(c.OQTY)
//            ELSE c.OQTY END) QTYGI
//			,c.PUNIT
//    INTO #GINotConvert
//	FROM STOut_List c(NOLOCK)

//    INNER JOIN #STOut g ON g.OUTCD=c.OUTCD
//	INNER JOIN STReason e(NOLOCK) ON c.RCD=e.RCD
//   WHERE c.MATCD IS NOT NULL

//   GROUP BY c.MatCD, c.PUNIT
//	--ORDER BY c.MatCD

//       SELECT   a.MatCD

//               , a.QTYGI

//               , a.PUNIT

//               , l.PUnitChuan
//   INTO #GIPUnitChuan
//	FROM #GINotConvert a
//	LEFT JOIN #TPInfoLaster l ON l.MatCD = a.MATCD




//	SELECT MatCD

//       , PUnitChuan PUNIT
//       , SUM(QTYGI* dbo.ConvQtyGR(ISNULL(PUNIT,''),ISNULL(PUnitChuan,0))) QTYGI
//     INTO #GI_NotFiFo
//	FROM #GIPUnitChuan 
//	GROUP BY MatCD, PUnitChuan





//    DROP TABLE #STOut,#GINotConvert,#GIPUnitChuan

//	--DROP TABLE #STOut




//    SELECT IVID, ISNULL(IDATE, ISNULL(WDATE, ISNULL(UDATE, CreatedOn))) IDATE
//        INTO #STIn_IVID
//	FROM dbo.STIn(NOLOCK)

//    WHERE IDATE BETWEEN @NgayDauThang AND @NgayCuoiThang



//    SELECT c.MatCD

//            , c.QTY

//            , c.PUNIT


//            , l.PUnitChuan

//            , c.IVLISTNO

//            , c.PRICE* m.ExRate PRICE

//    INTO #GRSTIn_List
//	FROM dbo.STIn_List c (NOLOCK)
//    INNER JOIN #STIn_IVID g ON g.IVID=c.IVID
//	LEFT JOIN #TPInfoLaster l ON c.MATCD=l.MatCD
//	LEFT JOIN dbo.TTExRateDaily m (NOLOCK) ON m.BDate= g.IDATE AND c.CURR= m.BCurrCD

//    WHERE c.MATCD IS NOT NULL

//    AND c.OUTCD IS NULL

//    DROP TABLE #STIn_IVID

//	INSERT #GRSTIn_List
//	(
//        MATCD,
//        QTY,
//        PUNIT,
//        PUnitChuan,
//        IVLISTNO,
//        PRICE
//    )
//    SELECT  c.MatCD

//        , c.QTY

//        , c.PUNIT

//        , l.PUnitChuan

//        , c.IVLISTNO

//        , c.PRICE* m.ExRate PRICE

//    FROM dbo.STIn_List c (NOLOCK)
//    INNER JOIN dbo.STIn d (NOLOCK) ON c.IVID= d.IVID

//    INNER JOIN dbo.STInBad h (NOLOCK) ON c.OUTCD= h.OUTCD AND h.ODATE BETWEEN @NgayDauThang AND @NgayCuoiThang
//    LEFT JOIN #TPInfoLaster l ON c.MATCD=l.MatCD
//	LEFT JOIN dbo.TTExRateDaily m (NOLOCK) ON m.BDate= ISNULL(d.IDATE, ISNULL(d.WDATE, ISNULL(d.UDATE, d.CreatedOn))) AND c.CURR= m.BCurrCD

//    WHERE c.MATCD IS NOT NULL AND c.OUTCD IS NOT NULL 
//	--AND c.IV NOT IN (SELECT IV FROM #GRSTIn_List)



//    SELECT MatCD, SUM(QTY* dbo.ConvQtyGR(ISNULL(PUNIT,''),ISNULL(PUnitChuan,0))) QTYGR
//	,SUM(PRICE/dbo.ConvQtyGR(ISNULL(PUNIT,''),ISNULL(PUnitChuan,0)) * QTY* dbo.ConvQtyGR(ISNULL(PUNIT,''),ISNULL(PUnitChuan,0))) AmtGR
//	,PUnitChuan
//    INTO #GRConverPUNITfINAL
//	FROM #GRSTIn_List 
//	GROUP BY MatCD, PUnitChuan


//    SELECT MatCD, SUM(QTY * dbo.ConvQtyGR(ISNULL(PUNIT, ''), ISNULL(PUnitChuan, 0))) QTYGR,PUnitChuan PUNIT, IVLISTNO
//          , SUM(PRICE / dbo.ConvQtyGR(ISNULL(PUNIT, ''), ISNULL(PUnitChuan, 0))) PRICE
//                INTO #GR_fifo
//	FROM #GRSTIn_List
//	WHERE QTY>0
//	GROUP BY MATCD,PUnitChuan,IVLISTNO

//    DROP TABLE #GRSTIn_List



//	--		SELECT a.MatCD, SUM(a.QTY* dbo.ConvQtyGR(ISNULL(a.PUNIT,''),ISNULL(a.PUnitChuan,0))) QTYGR,a.PUnitChuan PUNIT, a.IVLISTNO
//	--,SUM(a.PRICE/dbo.ConvQtyGR(ISNULL(a.PUNIT,''),ISNULL(a.PUnitChuan,0))) PRICE
//	----INTO #GR_fifo
//	--FROM #GRSTIn_List a
//	--INNER JOIN dbo.TMat b(NOLOCK) ON a.MATCD=b.MatCD
//	--WHERE a.QTY>0 AND b.Discontinue= 0 AND (ISNULL(b.OutSourcing,0)=0 )
//	--GROUP BY a.MATCD,a.PUnitChuan,a.IVLISTNO



//	--IDENTITY(int ) AS RowID


//    SELECT a.MatCD, b.IVLISTNO, b.QTYGR, a.QtyGI, b.PRICE
//    INTO #GI_IVLISTNO
//	FROM #GI_NotFiFo a	 
//	INNER JOIN dbo.TTBasicStockFiFo b (NOLOCK) ON a.MATCD= b.MatCD

//    UNION ALL

//    SELECT a.MatCD, b.IVLISTNO, b.QTYGR, a.QtyGI, b.PRICE
//    FROM #GI_NotFiFo a
//	INNER JOIN #GR_fifo b ON b.MatCD = a.MATCD	 


//    DROP TABLE #GI_NotFiFo,#GR_fifo


//    SELECT IDENTITY(int ) AS RowID, MatCD, IVLISTNO, QTYGR, QtyGI, PRICE
//    INTO #GI_IVLISTNO_OrderBy
//	FROM #GI_IVLISTNO
//	ORDER BY MatCD, IVLISTNO

//    DROP TABLE #GI_IVLISTNO


//	SELECT IDENTITY(int ) AS RowID, a.MatCD, a.IVLISTNO

//    , a.QtyGI QtyGISumMatCD
//    , a.QtyGI - SUM(b.QTYGR) QtyGICheckQty
//	--, CASE WHEN (a.QtyGI - SUM(b.QTYGR))<=0 THEN 0
//	--ELSE(a.QtyGI - SUM(b.QTYGR)) END QtyGI
//   , CASE WHEN(a.QtyGI - SUM(b.QTYGR))<=0 THEN(a.QtyGI - SUM(b.QTYGR)) +a.QTYGR
//  ELSE a.QTYGR END QTYGI
//	,a.QTYGR
//	,a.PRICE
//    INTO #GIFiFo
//	FROM #GI_IVLISTNO_OrderBy a
//	INNER JOIN #GI_IVLISTNO_OrderBy b ON b.MATCD = a.MATCD AND b.RowID<=a.RowID
//	GROUP BY a.MatCD, a.IVLISTNO, a.QtyGI, a.PRICE, a.QTYGR
//	--HAVING (a.QtyGI - SUM(b.QTYGR)) >=0
//	ORDER BY a.MatCD,a.IVLISTNO

//    DROP TABLE #GI_IVLISTNO_OrderBy

//	SELECT MatCD,MIN(IVLISTNO) IVLISTNO,MIN(RowID) RowID
//    INTO #QtyGICheckQty
//	FROM #GIFiFo WHERE QtyGICheckQty < 0
//	GROUP BY MatCD




//    SELECT MatCD
//			--, IVLISTNO
//			--, QtyGISumMatCD
//			--, QtyGICheckQty
//			--, QTYGI
//			--, QTYGR
//			--, PRICE
//            , SUM(QTYGI) QTYGI
//			,SUM(QTYGI* PRICE) AmtGI
//     INTO #GI
//	FROM #GIFiFo 
//	WHERE QtyGICheckQty > 0 OR RowID IN(SELECT RowID FROM #QtyGICheckQty)
//	GROUP BY MatCD

//	--	SELECT a.MatCD
//	--		, a.IVLISTNO
//	--	    , a.QTYGI QTYGI
//	--		, a.PRICE
//	--		, a.QTYGI* a.PRICE AmtGI
//	----INTO #GI
//	--FROM #GIFiFo a
//	--INNER JOIN dbo.TMat b (NOLOCK) ON a.MATCD= b.MatCD
//    --WHERE (a.QtyGICheckQty > 0 OR a.RowID IN (SELECT RowID FROM #QtyGICheckQty))
//	--AND b.Discontinue= 0 AND (ISNULL(b.OutSourcing,0)=0 )

//	SELECT BYear, BCurrCD, TCurrCD, ExRate

//    INTO #TExRate
//	FROM dbo.TExRate(NOLOCK)

//  SELECT MAX(BYear) BYear,BCurrCD
//  INTO #TExRateGR
//	FROM #TExRate
//	GROUP BY BCurrCD



//    SELECT a.BYear, a.BCurrCD, a.TCurrCD, a.ExRate
//    INTO #TExRateGRTPInfor
//	FROM #TExRate a
//	INNER JOIN #TExRateGR b ON a.BYear=b.BYear AND a.BCurrCD=b.BCurrCD
//	ORDER BY a.BYear

//    DROP TABLE #TExRateGR

//	SELECT a.MatCD, a.Qty, SUM(b.QTYGR* b.PRICE) AmtBasicStock,a.CreatedOn,a.StartYear,a.StartMonth,a.Unit,a.CloseYear,a.CloseMonth
//       INTO #STBasicStock
//	FROM dbo.STBasicStock a (NOLOCK)
//    LEFT JOIN dbo.TTBasicStockFiFo b (NOLOCK) ON a.MatCD= b.MatCD AND b.Year= YEAR(DATEADD(MONTH, -1, @NgayCuoiThang))

//    AND b.Month= MONTH(DATEADD(MONTH, -1, @NgayCuoiThang))

//    WHERE (a.StartYear= @Year AND a.StartMonth= @Month)

//    OR(a.StartYear= YEAR(DATEDIFF(MONTH, -1, @NgayCuoiThang))

//    AND a.StartMonth= MONTH(DATEDIFF(MONTH, -1, @NgayCuoiThang)))

//    OR(a.CloseYear= @Year AND a.CloseMonth= @Month)


//    GROUP BY a.MatCD,a.Qty,a.CreatedOn,a.StartYear,a.StartMonth,a.Unit,a.CloseYear,a.CloseMonth


//    SELECT  a.MatCD
//			,CASE WHEN a.Spec1 IS NULL AND a.Spec2 IS NOT NULL THEN a.MatName+ ' '+ a.Spec2
//            WHEN a.Spec1 IS NOT NULL AND a.Spec2 IS NULL THEN a.MatName+ ' '+ a.Spec1
//            WHEN a.Spec1 IS NOT NULL AND a.Spec2 IS NOT NULL  THEN a.MatName+ ' '+ a.Spec1 + ' '+ a.Spec2
//            ELSE a.MatName END MatDesc

//            , a.Color

//            , b.MH1

//            , a.OutSourcing

//            , CASE WHEN p.VCD IS NULL THEN c.MfgVendor
//                  ELSE p.VCD END Vendor

//            , CASE WHEN m.MatCD IS NOT NULL THEN 1

//            ELSE 0 END MultiUnit
//            , c.PUnit PUnitLast
//            , d.BrandCD


//            , c.PriceLotSize PriceLotSizePInfo
//            , c.PriceIncoterms PriceIncotermsPInfo
//            , c.CIFCurr


//            , e.PRICE* c.PriceLotSize PriceGR
//            , e.CURR CURRGR



//            , c.CIFPrice LastPricePInfo
//            , e.PRICE LastPriceGR
//            , e.PUNIT PUNITLastPriceGR
//            , e.ExRate

//            , e.IDATE

//            , c.ExRate ExRateTPInfor
//            , c.ValidFrom

//            , f.Qty QtyBS
//            , f.AmtBasicStock

//            , f.Unit PUnitBasicStock
//            , h.QTYGR

//            , h.AmtGR

//            , h.PUnitChuan PUNITGR
//            , g.QTYGI

//            , g.AmtGI
//			--, g.PUNIT PUNITGI
//            , (ISNULL(f.Qty,0) + ISNULL(h.QTYGR,0)) - ISNULL(g.QTYGI,0)  QtyEndingS
//			,q.Qty QtyInspect
//            , q.Unit PUnitInspect
//             , q.CreatedOn DateInspect

//    INTO #SQMatStockMonthly
//	FROM  dbo.TMat a(NOLOCK)

//    LEFT JOIN  dbo.TMH b(NOLOCK) ON a.MHID=b.MHID
//   LEFT JOIN #TPInfoLaster c ON a.MatCD=c.MatCD
//	LEFT JOIN #BrandCD d ON a.MatCD=d.MatCD
//	LEFT JOIN #GRPriceLatest e ON e.MATCD=a.MatCD
//	LEFT JOIN #Vendor p ON a.MatCD=p.MATCD
//	LEFT JOIN #STBasicStock f ON a.MatCD=f.MATCD AND f.StartYear=@Year AND f.StartMonth=@Month AND f.Qty<>0
//	LEFT JOIN #GI g ON a.MatCD=g.MATCD
//	LEFT JOIN #GRConverPUNITfINAL h ON a.MatCD=h.MATCD
//	LEFT JOIN #TPInfoGRMutiPunit m ON a.MatCD=m.MatCD
//	--LEFT JOIN #TExRate n (NOLOCK) ON n.BYear=e.YEAR AND e.CURR=n.BCurrCD
//	--LEFT JOIN #TExRateGRTPInfor o (NOLOCK) ON  c.CIFCurr=o.BCurrCD
//	--LEFT JOIN #STBasicStock q ON q.MatCD=a.MatCD AND q.StartYear=YEAR(DATEDIFF(MONTH,-1,@NgayCuoiThang)) 
//	--AND q.StartMonth= MONTH(DATEDIFF(MONTH, -1, @NgayCuoiThang))

//   LEFT JOIN #STBasicStock q ON q.MatCD=a.MatCD AND q.CloseYear=@Year AND q.CloseMonth=@Month AND q.Qty<>0 


//   WHERE a.Discontinue= 0 AND (@Outsourcing= 'ALL' OR (ISNULL(a.OutSourcing,0)=0 AND @Outsourcing = 'false') OR(ISNULL(a.OutSourcing,0)=1 AND @Outsourcing = 'true'))
//	-- AND a.MatCD='M0000159'

//	DROP TABLE #TPInfoGRMutiPunit,#TPInfoLaster,#GRPriceLatest,#GI,#GRConverPUNITfINAL,#TExRate,#TExRateGRTPInfor,#Vendor,#BrandCD,#STBasicStock


//    SELECT a.MatCD

//            , a.MatDesc

//            , a.Color

//            , a.MH1

//            , a.OutSourcing

//            , a.Vendor

//            , a.MultiUnit

//            , a.PUnitLast

//            , a.PUNITLastPriceGR

//            , CASE WHEN a.MultiUnit= 1 AND a.PUnitLast= 'SET' THEN 'PC'

//                    WHEN a.MultiUnit= 1 AND a.PUnitLast= 'YRD' THEN 'M'

//                    WHEN a.MultiUnit= 1 AND a.PUnitLast= 'SQM' THEN 'SQF'

//            ELSE a.PUnitLast END PUnitGRChuan

//            , a.BrandCD

//			--, a.PriceLotSizePInfo
//			--, a.PriceIncotermsPInfo

//            , a.LastPricePInfo

//            , a.LastPriceGR



//            , a.PriceLotSizePInfo

//            , a.PriceIncotermsPInfo

//            , a.CIFCurr

//            , a.PriceGR

//            , a.CURRGR



//            , a.ExRateTPInfor

//            , a.ValidFrom

//            , a.ExRate

//            , a.IDATE

//            , a.QtyBS

//            , a.AmtBasicStock

//            , a.PUnitBasicStock

//            , a.QTYGR

//            , a.AmtGR

//            , a.PUNITGR

//            , a.QTYGI

//            , a.AmtGI
//			--, a.PUNITGI

//            , a.QtyEndingS

//            , a.QtyInspect

//            , a.PUnitInspect

//            , a.DateInspect
//    INTO #SQMatStockMonthlyUnitChuan
//	FROM #SQMatStockMonthly a

//	DROP TABLE #SQMatStockMonthly

//	SELECT   a.MatCD

//            , a.MatDesc

//            , a.Color

//            , a.MH1

//            , a.OutSourcing

//            , a.Vendor

//            , a.MultiUnit

//            , a.PUnitLast

//            , ISNULL(a.PUnitGRChuan, a.PUnitGR) PUnitGRChuan
//            , a.BrandCD


//            , a.PriceLotSizePInfo

//            , a.PriceIncotermsPInfo* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUnitLast,0)) PriceIncotermsPInfo
//			,(a.LastPricePInfo* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUnitLast,0)))*a.ExRateTPInfor LastPricePInfo
//             , a.CIFCurr
//			,a.PriceGR* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)) PriceGR
//			,a.CURRGR			
			

//			--,CASE WHEN a.PriceIncotermsPInfo = 0 OR a.PriceIncotermsPInfo*a.ExRateTPInfor = 0  THEN NULL
//			--		      WHEN a.CIFCurr= a.CURRGR  THEN ((a.PriceGR* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)))/a.PriceIncotermsPInfo)*100
//			--		ELSE((a.PriceGR* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0))*a.ExRate)/a.PriceIncotermsPInfo* a.ExRateTPInfor)*100 END Comp
			
//			--, CASE WHEN a.PriceLotSizePInfo = 0 OR a.PriceGR = 0  THEN NULL
//			--		WHEN a.CIFCurr= a.CURRGR  THEN (((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))
//			--		- a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast, a.PUnitGRChuan) )
//			--		/(a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan)))*100

//			--ELSE(((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))*a.ExRate
//			--- (a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast, a.PUnitGRChuan))*a.ExRateTPInfor)
//			--/(a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))*a.ExRate)*100 END Comp

//             , CASE WHEN a.PriceLotSizePInfo = 0 OR a.PriceGR = 0  THEN NULL
  
//                      WHEN a.CIFCurr= a.CURRGR  THEN ((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))
//			-(a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast, a.PUnitGRChuan)))
//			/((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan)))*100 

//			ELSE((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))*a.ExRate 			
//			-(a.PriceIncotermsPInfo/dbo.ConvQtyGR(a.PUnitLast, a.PUnitGRChuan))*a.ExRateTPInfor)
//			/((a.PriceGR/dbo.ConvQtyGR(a.PUNITLastPriceGR, a.PUnitGRChuan))*a.ExRate)*100  END Comp


//             , f.Payment



//			--,a.PriceIncotermsPInfo
//			--,a.CIFCurr
			
			
			
			
			
//			,a.ValidFrom
//			,a.LastPriceGR LastPriceGRtt
//            , a.PUNITLastPriceGR
//			,(a.LastPriceGR* dbo.ConvQtyGR(ISNULL(a.PUnitGRChuan,''),ISNULL(a.PUNITLastPriceGR,0)))*a.ExRate LastPriceGR
//             , a.IDATE
//			,a.ExRate
//			--,ISNULL(a.QtyBS,0) QtyBS
//			,ISNULL(a.QtyBS,0)*dbo.ConvQtyGR(ISNULL(a.PUnitBasicStock,''),ISNULL(a.PUnitGRChuan,0)) QtyBS
//			,a.AmtBasicStock
//			,a.QTYGR* dbo.ConvQtyGR(ISNULL(a.PUnitGR,''),ISNULL(a.PUnitGRChuan,0)) QTYGR
//			,a.AmtGR
//			--,a.PUnitGR
//			--,a.QTYGI* dbo.ConvQtyGR(ISNULL(a.PUNITGI,''),ISNULL(a.PUnitGRChuan,0)) QTYGI
//			,a.QTYGI
//			,a.AmtGI
//			--,a.PUNITGI
//			--,ISNULL(a.QtyInspect,0) QtyInspect
//			,ISNULL(a.QtyInspect,0)*dbo.ConvQtyGR(ISNULL(a.PUnitInspect,''),ISNULL(a.PUnitGRChuan,0)) QtyInspect
//			,a.DateInspect
//    INTO #SQMatStockMonthlyFinal
//	FROM #SQMatStockMonthlyUnitChuan a
//	LEFT JOIN STCustomsMat f(NOLOCK) ON a.MatCD=f.MatCD
//	--WHERE a.MatCD= 'M0046983'

//   ORDER BY a.MatCD



//   DROP TABLE #SQMatStockMonthlyUnitChuan

//	SELECT MatCD

//          , SUM(QtyUnCut+QtySubStore) QtyEU
//    INTO #tb_TTWIPMat
//	FROM dbo.TTWIPMat(NOLOCK)

//    WHERE @Year = Year AND @Month = Month

//    GROUP BY MatCD


//    SELECT      IDENTITY(int ) AS RowID
//               , a.MatCD
//				,a.MatDesc 
//				,a.Color
//				,a.MH1
//			    ,a.OutSourcing
//				,a.Vendor
//				--,a.MultiUnit
//				--,a.PUnitLast
//				,a.PUnitGRChuan PUnit
//                , a.BrandCD

//				--,a.PriceLotSizePInfo
//			 --   ,a.PriceIncotermsPInfo
//			,a.LastPricePInfo

//			,a.PriceLotSizePInfo
//			,a.PriceIncotermsPInfo
//			,a.CIFCurr
//			,a.PriceGR
//			,a.CURRGR			
//			,a.Comp
//			,a.Payment



//				,a.ValidFrom
//				,a.LastPriceGR
//				,a.IDATE
//				--,ExRate
//				,CASE WHEN a.LastPriceGR IS NULL THEN a.LastPricePInfo
//                 ELSE a.LastPriceGR END UnitPrice
//				,a.QtyBS
//				,a.AmtBasicStock
//				,ISNULL(a.QTYGR,0)  QTYGR
//				,a.AmtGR
//				,ISNULL(a.QTYGI,0) QTYGI
//				,a.AmtGI
//				,ROUND((ISNULL(a.QtyBS,0) + ISNULL(a.QTYGR,0)) - ISNULL(a.QTYGI,0),5)  QtyEndingS 
//				,a.QtyInspect
//				,a.DateInspect	

//				,b.QtyEU
//    INTO #SQMatStockMonthlyRowID
//	FROM #SQMatStockMonthlyFinal a
//	LEFT JOIN #tb_TTWIPMat b ON a.MatCD=b.MatCD


//	SELECT IDENTITY(int ) AS RowID
//           , MatCD
//           , MatDesc
//           , Color
//           , MH1
//           , OutSourcing
//           , Vendor
//           , PUnit
//           , BrandCD

//			--, PriceLotSizePInfo
//			--, PriceIncotermsPInfo
//           , LastPricePInfo
//           , LastPriceGR

//           , PriceLotSizePInfo
//           , PriceIncotermsPInfo
//           , CIFCurr
//           , PriceGR
//           , CURRGR
//           , ROUND(Comp, 0) Comp
//			,Payment


//			,UnitPrice
//			,QtyBS
//			,QTYGR
//			,QTYGI
//			,QtyEndingS 
//			--,QtyBS* UnitPrice AmtBS
//			,AmtBasicStock AmtBS
//			--, QTYGR*UnitPrice AmtGR
//             , AmtGR
//			--, QTYGI*UnitPrice AmtGI
//             , AmtGI
//			--, QtyEndingS*UnitPrice AmtES
//             , ROUND((ISNULL(AmtBasicStock, 0) + ISNULL(AmtGR, 0)) - ISNULL(AmtGI, 0), 5)  AmtES 

//			,QtyInspect
//			,QtyInspect - QtyEndingS QtyDiff
//            , DateInspect

//            , QtyEU
//            ,(QtyEU + QtyEndingS) QtyESSum
//			,QtyEU* UnitPrice AmtEU
//			,(QtyEU+QtyEndingS)*UnitPrice AmtESSum
//    INTO #SQMatStock
//	FROM #SQMatStockMonthlyRowID
//	WHERE RowID > ((@PageNumber-1)*@PageSize)
//	ORDER BY MatCD

//    DECLARE @SumAllQtyBS FLOAT, @SumAllQTYGR FLOAT,@SumAllQTYGI FLOAT, @SumAllQtyEndingS FLOAT,@SumAllAmtBS FLOAT, @SumAllAmtGR FLOAT
//	,@SumAllAmtGI FLOAT, @SumAllAmtES FLOAT,@SumAllQtyInspect FLOAT, @SumAllQtyDiff FLOAT,@SumAllQtyEU FLOAT, @SumAllQtyESSum FLOAT
//	,@SumAllAmtEU FLOAT, @SumAllAmtESSum FLOAT


//     SELECT   @SumAllQtyBS=SUM(QtyBS)
//			,@SumAllQTYGR=SUM(QTYGR)
//			,@SumAllQTYGI=SUM(QTYGI)
//			,@SumAllQtyEndingS=SUM(QtyEndingS)
//			,@SumAllAmtBS=SUM(AmtBS)
//			,@SumAllAmtGR=SUM(AmtGR)
//			,@SumAllAmtGI=SUM(AmtGI)
//			,@SumAllAmtES=SUM(AmtES)
//			,@SumAllQtyInspect=SUM(QtyInspect)
//			,@SumAllQtyDiff=SUM(QtyDiff)
//			,@SumAllQtyEU=SUM(QtyEU)
//			--,@SumAllQtyESSum=SUM(QtyESSum)
//			,@SumAllAmtEU=SUM(AmtEU)
//			,@SumAllAmtESSum=SUM(AmtESSum)

//    FROM #SQMatStock

//	DROP TABLE #SQMatStockMonthlyFinal

//	IF @Type=0
//	BEGIN
//                DECLARE @TotalRows INT

//                SELECT @TotalRows = COUNT(RowID) FROM #SQMatStock


//				SELECT TOP 15  @TotalRows TotalRows
//                        , RowID
//                        , MatCD
//                        , MatDesc
//                        , Color
//                        , MH1
//                        , OutSourcing
//                        , Vendor
//                        , PUnit
//                        , BrandCD

//                        , LastPricePInfo
//                        , LastPriceGR

//                        , PriceLotSizePInfo
//                        , PriceIncotermsPInfo
//                        , CIFCurr
//                        , PriceGR
//                        , CURRGR
//                        , Comp
//                        , Payment

//                        , UnitPrice
//                        , QtyBS
//                        , QTYGR
//                        , QTYGI
//                        , QtyEndingS
//                        , AmtBS
//                        , AmtGR
//                        , AmtGI
//                        , AmtES
//                        , QtyInspect
//                        , QtyDiff
//                        , REPLACE(convert(varchar, DateInspect, 106), ' ', '/') DateInspectConvert
//						,QtyEU
//						,QtyESSum
//						,AmtEU
//						,AmtESSum
//						,@SumAllQtyBS SumAllQtyBS
//                        , @SumAllQTYGR      SumAllQTYGR
//						,@SumAllQTYGI SumAllQTYGI

//                        , @SumAllQtyEndingS SumAllQtyEndingS
//						,@SumAllQtyEU SumAllQtyEU
//                        , @SumAllQtyEndingS + @SumAllQtyEU SumAllQtyESSum


//                         , @SumAllAmtBS      SumAllAmtBS
//						,@SumAllAmtGR SumAllAmtGR
//                        , @SumAllAmtGI      SumAllAmtGI
//						,@SumAllAmtES SumAllAmtES
//                        , @SumAllAmtEU      SumAllAmtEU
//						,@SumAllAmtESSum SumAllAmtESSum


//                        , @SumAllQtyInspect SumAllQtyInspect
//						,@SumAllQtyDiff SumAllQtyDiff

//                FROM #SQMatStock
//				WHERE RowID > ((@PageNumber-1)*@PageSize)
//				ORDER BY MatCD



//    END


//    IF @Type = 1

//    BEGIN

//                SELECT

//                        MatCD
//						,MatDesc
//						,Color	
//						,MH1
//						,PUnit

//			            ,OutSourcing
//						,Vendor
//						,BrandCD

//						,PriceLotSizePInfo 'PriceLot'
//						,PriceIncotermsPInfo 'PricePInfo'
//						,CIFCurr 'Curr'

//						,PriceGR
//						,CURRGR	'Curr_Double0'
//						,Comp 'Diff(%)_Percent0'
//						,Payment
//						,UnitPrice


//						--,PriceLotSizePInfo
//						--,PriceIncotermsPInfo
//						--,LastPricePInfo
//						--,LastPriceGR
//						,QtyBS
//						,QTYGR
//						,QTYGI
//						,QtyEndingS 'QtyES'
//						,QtyEU
//						,QtyESSum
//						,AmtBS
//						,AmtGR
//						,AmtGI
//						,AmtES
//						,AmtEU
//						,AmtESSum
//						,QtyInspect
//						,QtyDiff
//						,REPLACE(convert(varchar, DateInspect, 106),' ','/') DateInspect

//						,'Total' Total_Total_16
//						,@SumAllQtyBS Total_SumAllQtyBS_Format2_17
//                        , @SumAllQTYGR      Total_SumAllQTYGR_Format2_18
//						,@SumAllQTYGI Total_SumAllQTYGI_Format2_19
//                        , @SumAllQtyEndingS Total_SumAllQtyEndingS_Format2_20
//						,@SumAllQtyEU Total_SumAllQtyEU_Format2_21
//                        , @SumAllQtyEndingS + @SumAllQtyEU Total_SumAllQtyESSum_Format2_22
//                         , @SumAllAmtBS      Total_SumAllAmtBS_Format2_23
//						,@SumAllAmtGR Total_SumAllAmtGR_Format2_24
//                        , @SumAllAmtGI      Total_SumAllAmtGI_Format2_25
//						,@SumAllAmtES Total_SumAllAmtES_Format2_26
//                        , @SumAllAmtEU      Total_SumAllAmtEU_Format2_27
//						,@SumAllAmtESSum Total_SumAllAmtESSum_Format2_28
//                        , @SumAllQtyInspect Total_SumAllQtyInspect_Format2_29
//						,@SumAllQtyDiff Total_SumAllQtyDiff_Format2_30

//                FROM #SQMatStock
//				ORDER BY MatCD



//    END

//    IF @Type=2
//	BEGIN




//                SELECT

//                        c.MatCD
//						,SUM(c.QTY) QTYGR
//						,c.PUNIT PUNITGR
//                        , h.PUnit PUnitChuan

//                INTO #GRNowMatCD
//				FROM dbo.STIn_List c(NOLOCK)

//                INNER JOIN dbo.STIn g(NOLOCK) ON g.IVID=c.IVID
//               INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
//				WHERE YEAR(g.IDATE) = YEAR(GETDATE()) AND MONTH(g.IDATE) = MONTH(GETDATE())

//               AND DAY(g.IDATE) = DAY(GETDATE())

//               AND c.MATCD IS NOT NULL

//               GROUP BY c.MatCD, c.PUNIT, h.PUnit
//               ORDER BY c.MatCD

//               SELECT MATCD

//                     , SUM(ISNULL(QTYGR,0)*dbo.ConvQtyGR(ISNULL(PUNITGR,0),ISNULL(PUnitChuan,0))) QTYGRNow
//                INTO #GRNow
//				FROM #GRNowMatCD
//				GROUP BY MATCD


//                DROP TABLE #GRNowMatCD





//                SELECT c.MatCD

//                        , SUM(CASE WHEN e.RSign= '-' THEN -(c.OQTY)
//                        ELSE c.OQTY END) QTYGI
//						,c.PUNIT PUNITGI
//                        , h.PUnit PUnitChuan

//                INTO #GINowMatCD
//				FROM STOut_List c(NOLOCK)

//                INNER JOIN dbo.STOut g(NOLOCK) ON g.OUTCD=c.OUTCD
//               INNER JOIN STReason e (NOLOCK) ON c.RCD= e.RCD

//               INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
//				WHERE YEAR(g.ODATE) = YEAR(GETDATE()) AND MONTH(g.ODATE) = MONTH(GETDATE())

//               AND DAY(g.ODATE) = DAY(GETDATE())

//               AND c.MATCD IS NOT NULL

//               GROUP BY c.MatCD, c.PUNIT, h.PUnit
//               ORDER BY c.MatCD




//               SELECT MATCD

//                     , SUM(ISNULL(QTYGI,0)*dbo.ConvQtyGR(ISNULL(PUNITGI,0),ISNULL(PUnitChuan,0))) QTYGINow
//                INTO #GINow
//				FROM #GINowMatCD
//				GROUP BY MATCD



//                DROP TABLE #GINowMatCD




//                SELECT
//                        MH1
//                       ,COUNT(a.MH1) Count
//					   ,SUM(a.QtyBS* a.UnitPrice) AmtBS
//					   ,SUM(a.QTYGR* UnitPrice) AmtGR
//					   ,SUM(a.QTYGI* a.UnitPrice) AmtGI
//					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice) AmtES
//					   ,SUM(a.QtyEU* a.UnitPrice) AmtEU
//					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice + a.QtyEU* a.UnitPrice) AmtESSum

//					   --,SUM(b.QTYGRNow* a.UnitPrice) AmtGRNow
//					   --,SUM(c.QTYGINow* a.UnitPrice) AmtGINow
//					   ,1 RowID
//                INTO #SQMatStockMonthlyGR
//				FROM #SQMatStockMonthlyRowID a
//				LEFT JOIN #GRNow b ON a.MatCD=b.MATCD
//				LEFT JOIN #GINow c ON a.MatCD=c.MATCD
//				GROUP BY a.MH1


//                DROP TABLE #GRNow,#GINow


//                        DECLARE @ToTal_CountSum FLOAT

//                                , @ToTal_AmtBSSum FLOAT
//                                , @ToTal_AmtGRSum FLOAT
//                                , @ToTal_AmtGISum FLOAT
//                                , @ToTal_AmtESSum FLOAT
//                                , @ToTal_AmtEUSum FLOAT
//                                , @ToTal_AmtESSumSum FLOAT




//                SELECT @ToTal_CountSum = SUM(Count)
//                       , @ToTal_AmtBSSum= SUM(AmtBS)
//                       , @ToTal_AmtGRSum= SUM(AmtGR)
//                       , @ToTal_AmtGISum= SUM(AmtGI)
//                       , @ToTal_AmtESSum= SUM(AmtES)
//                       , @ToTal_AmtEUSum= SUM(AmtEU)
//                       , @ToTal_AmtESSumSum= SUM(AmtESSum)

//                FROM #SQMatStockMonthlyGR




//				SELECT  MH1

//                        , Count

//                        , ROUND(AmtBS,2) AmtBS
//						,ROUND(AmtGR,2) AmtGR
//						,ROUND(AmtGI,2) AmtGI
//						,ROUND(AmtES,2) AmtES
//						,ROUND(ISNULL(AmtEU,0),2) AmtEU
//						,ROUND(ISNULL(AmtESSum,0),2) AmtESSum
//						,@ToTal_CountSum ToTal_CountSum
//                        , @ToTal_AmtBSSum       ToTal_AmtBSSum
//						,@ToTal_AmtGRSum ToTal_AmtGRSum
//                        , @ToTal_AmtGISum       ToTal_AmtGISum
//						,@ToTal_AmtESSum ToTal_AmtESSum
//                        , @ToTal_AmtEUSum       ToTal_AmtEUSum
//						,@ToTal_AmtESSumSum ToTal_AmtESSumSum


//                FROM #SQMatStockMonthlyGR
//				ORDER BY RowID


//                DROP TABLE #SQMatStockMonthlyGR



//    END
//    IF @Type=3
//	BEGIN




//                SELECT

//                        c.MatCD
//						,SUM(c.QTY) QTYGR
//						,c.PUNIT PUNITGR
//                        , h.PUnit PUnitChuan

//                INTO #GRNowMatCD_Excel
//				FROM dbo.STIn_List c(NOLOCK)

//                INNER JOIN dbo.STIn g(NOLOCK) ON g.IVID=c.IVID
//               INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
//				WHERE YEAR(g.IDATE) = YEAR(GETDATE()) AND MONTH(g.IDATE) = MONTH(GETDATE())

//               AND DAY(g.IDATE) = DAY(GETDATE())

//               AND c.MATCD IS NOT NULL

//               GROUP BY c.MatCD, c.PUNIT, h.PUnit
//               ORDER BY c.MatCD

//               SELECT MATCD

//                     , SUM(ISNULL(QTYGR,0)*dbo.ConvQtyGR(ISNULL(PUNITGR,0),ISNULL(PUnitChuan,0))) QTYGRNow
//                INTO #GRNow_Excel
//				FROM #GRNowMatCD_Excel
//				GROUP BY MATCD


//                DROP TABLE #GRNowMatCD_Excel





//                SELECT c.MatCD

//                        , SUM(CASE WHEN e.RSign= '-' THEN -(c.OQTY)
//                        ELSE c.OQTY END) QTYGI
//						,c.PUNIT PUNITGI
//                        , h.PUnit PUnitChuan

//                INTO #GINowMatCD_Excel
//				FROM STOut_List c(NOLOCK)

//                INNER JOIN dbo.STOut g(NOLOCK) ON g.OUTCD=c.OUTCD
//               INNER JOIN STReason e (NOLOCK) ON c.RCD= e.RCD

//               INNER JOIN #SQMatStockMonthlyRowID h ON c.MATCD=h.MatCD
//				WHERE YEAR(g.ODATE) = YEAR(GETDATE()) AND MONTH(g.ODATE) = MONTH(GETDATE())

//               AND DAY(g.ODATE) = DAY(GETDATE())

//               AND c.MATCD IS NOT NULL

//               GROUP BY c.MatCD, c.PUNIT, h.PUnit
//               ORDER BY c.MatCD




//               SELECT MATCD

//                     , SUM(ISNULL(QTYGI,0)*dbo.ConvQtyGR(ISNULL(PUNITGI,0),ISNULL(PUnitChuan,0))) QTYGINow
//                INTO #GINow_Excel
//				FROM #GINowMatCD_Excel
//				GROUP BY MATCD



//                DROP TABLE #GINowMatCD_Excel




//                SELECT
//                        MH1
//                       ,COUNT(a.MH1) Count
//					   ,SUM(a.QtyBS* a.UnitPrice) AmtBS
//					   ,SUM(a.QTYGR* UnitPrice) AmtGR
//					   ,SUM(a.QTYGI* a.UnitPrice) AmtGI
//					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice) AmtES
//					   ,SUM(a.QtyEU* a.UnitPrice) AmtEU
//					   ,SUM(((a.QtyBS + a.QTYGR) - a.QTYGI)*a.UnitPrice + a.QtyEU* a.UnitPrice) AmtESSum
//					   ,1 RowID
//                INTO #SQMatStockMonthlyGR_Excel
//				FROM #SQMatStockMonthlyRowID a
//				LEFT JOIN #GRNow_Excel b ON a.MatCD=b.MATCD
//				LEFT JOIN #GINow_Excel c ON a.MatCD=c.MATCD
//				GROUP BY a.MH1


//                DROP TABLE #GRNow_Excel,#GINow_Excel



//                        DECLARE @ToTal_CountSum_Excel FLOAT

//                                , @ToTal_AmtBSSum_Excel FLOAT
//                                , @ToTal_AmtGRSum_Excel FLOAT
//                                , @ToTal_AmtGISum_Excel FLOAT
//                                , @ToTal_AmtESSum_Excel FLOAT
//                                , @ToTal_AmtEUSum_Excel FLOAT
//                                , @ToTal_AmtESSumSum_Excel FLOAT




//                SELECT @ToTal_CountSum_Excel = SUM(Count)
//                       , @ToTal_AmtBSSum_Excel= SUM(AmtBS)
//                       , @ToTal_AmtGRSum_Excel= SUM(AmtGR)
//                       , @ToTal_AmtGISum_Excel= SUM(AmtGI)
//                       , @ToTal_AmtESSum_Excel= SUM(AmtES)
//                       , @ToTal_AmtEUSum_Excel= SUM(AmtEU)
//                       , @ToTal_AmtESSumSum_Excel= SUM(AmtESSum)

//                FROM #SQMatStockMonthlyGR_Excel





//				SELECT  MH1 'Categories'
//						, Count Count_Format2
//                        , ROUND(AmtBS,2) 'AmtBS_Format2'
//						,ROUND(AmtGR,2) 'AmtGR_Format2'
//						,ROUND(AmtGI,2) 'AmtGI_Format2'
//						,ROUND(AmtES,2) 'AmtES_Format2' 
//						,ROUND(AmtEU,2) 'AmtEU_Format2' 
//						,ROUND(AmtESSum,2) 'AmtESSum_Format2' 
//						,'ToTal'    Total_ToTal_1 
//						,@ToTal_CountSum_Excel Total_CountSum_Format2_2
//                        , @ToTal_AmtBSSum_Excel  Total_AmtBSSum_Format2_3
//						,@ToTal_AmtGRSum_Excel Total_AmtGRSum_Format2_4
//                        , @ToTal_AmtGISum_Excel  Total_AmtGISum_Format2_5 
//						,@ToTal_AmtESSum_Excel Total_AmtESSum_Format2_6
//                        , @ToTal_AmtEUSum_Excel  Total_AmtEUSum_Format2_7 
//						,@ToTal_AmtESSumSum_Excel Total_AmtESSum_Format2_8


//                FROM #SQMatStockMonthlyGR_Excel
//				ORDER BY RowID


//                DROP TABLE #SQMatStockMonthlyGR_Excel



//    END
//    DROP TABLE #SQMatStockMonthlyRowID




//END




    }
}

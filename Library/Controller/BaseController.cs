﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Controller
{
    public class BaseController
    {
        static readonly string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ", "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ" };
        public byte[] ExportDataTableToExcelFromStore(DataTable dt, string Worksheets, Boolean isOneTotal = false, Boolean isTwoTotal = false
        , string formname = "")
        {
            try
            {
                ExcelPackage excel = new ExcelPackage();
                var workSheet = excel.Workbook.Worksheets.Add(Worksheets);
                if (dt.Rows.Count == 0)
                {
                    return excel.GetAsByteArray();
                }
                if (isTwoTotal)//Có 2 Total
                {
                    int start = 1;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {

                        if (dt.Columns[i].ColumnName.ToString().Contains("Total_"))
                        {
                            string ColumnName = dt.Columns[i].ColumnName.ToString();
                            string[] ColumnNamearray = ColumnName.Split('_');
                            int cot = int.Parse(ColumnNamearray[ColumnNamearray.Length - 1]);
                            workSheet.Cells[1, 1, 1, cot].Style.Font.Bold = true;

                            if (dt.Columns[i].ColumnName.ToString().Contains("_Percent0"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[0][i].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[1, cot].Value = value / 100;
                                }

                                workSheet.Cells[1, cot].Style.Numberformat.Format = "#0%";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Percent2"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[0][i].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[1, cot].Value = value / 100;
                                }

                                workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00%";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Percent3"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[0][i].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[1, cot].Value = value / 100;
                                }

                                workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000%";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Format2"))
                            {
                                workSheet.Cells[1, cot].Value = dt.Rows[0][i];
                                workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Format4"))
                            {
                                workSheet.Cells[1, cot].Value = dt.Rows[0][i];
                                workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0000";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Format12"))
                            {
                                workSheet.Cells[1, cot].Value = dt.Rows[0][i];
                                if (dt.Rows[0][i] == null)
                                {
                                    continue;
                                }
                                string number = Convert.ToString(dt.Rows[0][i]);
                                if (number.Contains("."))
                                {
                                    while (number.Substring(number.Length - 1) == "0")
                                    {
                                        number = number.Substring(0, number.Length - 1);
                                    }
                                }
                                int degit = number.ToString().Substring(number.IndexOf(".") + 1).Length;
                                if (degit == 0)
                                {
                                    continue;
                                }
                                else if (degit == 1)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0";
                                }
                                else if (degit == 2)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00";
                                }
                                else if (degit == 3)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000";
                                }
                                else if (degit == 4)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0000";
                                }
                                else if (degit == 5)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00000";
                                }
                                else if (degit == 6)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000000";
                                }
                                else if (degit == 7)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0000000";
                                }
                                else if (degit == 8)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00000000";
                                }
                                else if (degit == 9)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000000000";
                                }
                                else if (degit == 10)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0000000000";
                                }
                                else if (degit == 11)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00000000000";
                                }
                                else if (degit == 12)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000000000000";
                                }
                                continue;
                            }
                            workSheet.Cells[1, cot].Value = dt.Rows[0][i];
                        }
                    }




                    for (int i = 0; i < dt.Columns.Count; i++)
                    {

                        if (dt.Columns[i].ColumnName.ToString().Contains("Total2_"))
                        {
                            string ColumnName = dt.Columns[i].ColumnName.ToString();
                            string[] ColumnNamearray = ColumnName.Split('_');
                            int cot = int.Parse(ColumnNamearray[ColumnNamearray.Length - 1]);
                            workSheet.Cells[2, 1, 2, cot].Style.Font.Bold = true;

                            if (dt.Columns[i].ColumnName.ToString().Contains("_Percent0"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[0][i].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[2, cot].Value = value / 100;
                                }

                                workSheet.Cells[2, cot].Style.Numberformat.Format = "#0%";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Percent2"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[0][i].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[2, cot].Value = value / 100;
                                }

                                workSheet.Cells[2, cot].Style.Numberformat.Format = "#0.00%";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Format2"))
                            {
                                workSheet.Cells[2, cot].Value = dt.Rows[0][i];
                                workSheet.Cells[2, cot].Style.Numberformat.Format = "#0.00";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Format4"))
                            {
                                workSheet.Cells[2, cot].Value = dt.Rows[0][i];
                                workSheet.Cells[2, cot].Style.Numberformat.Format = "#0.0000";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Format12"))
                            {
                                workSheet.Cells[1, cot].Value = dt.Rows[0][i];
                                if (dt.Rows[0][i] == null)
                                {
                                    continue;
                                }
                                string number = Convert.ToString(dt.Rows[0][i]);
                                if (number.Contains("."))
                                {
                                    while (number.Substring(number.Length - 1) == "0")
                                    {
                                        number = number.Substring(0, number.Length - 1);
                                    }
                                }
                                int degit = number.ToString().Substring(number.IndexOf(".") + 1).Length;
                                if (degit == 0)
                                {
                                    continue;
                                }
                                else if (degit == 1)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0";
                                }
                                else if (degit == 2)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00";
                                }
                                else if (degit == 3)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000";
                                }
                                else if (degit == 4)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0000";
                                }
                                else if (degit == 5)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00000";
                                }
                                else if (degit == 6)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000000";
                                }
                                else if (degit == 7)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0000000";
                                }
                                else if (degit == 8)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00000000";
                                }
                                else if (degit == 9)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000000000";
                                }
                                else if (degit == 10)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0000000000";
                                }
                                else if (degit == 11)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00000000000";
                                }
                                else if (degit == 12)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000000000000";
                                }
                                continue;
                            }
                            workSheet.Cells[2, cot].Value = dt.Rows[0][i];
                        }
                    }

                    start = 1;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (dt.Columns[i].ColumnName.ToString().Contains("Total_") || dt.Columns[i].ColumnName.ToString().Contains("Total2_"))
                        {
                            continue;
                        }
                        workSheet.Cells[3, start, 3, start].Style.Font.Bold = true;
                        workSheet.Cells[3, start].Value = dt.Columns[i].ColumnName.Replace("_Format2", "").Replace("_Format4", "").Replace("_Format12", "").Replace("_Percent0", "").Replace("_Percent2", "")
                        .Replace("_Double0", "").Replace("_Double1", "").Replace("_Double2", "").Replace("_Double3", "").Replace("_Double4", "");
                        workSheet.Cells["A3:" + Columns[start - 1] + "3"].AutoFilter = true;
                        start++;
                    }
                    start = 4;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        for (int k = 0; k < dt.Columns.Count; k++)
                        {
                            if (dt.Columns[k].ColumnName.ToString().Contains("Total_") || dt.Columns[k].ColumnName.ToString().Contains("Total2_"))
                            {
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Percent0") && !dt.Columns[k].ColumnName.ToString().Contains("Total_") && !dt.Columns[k].ColumnName.ToString().Contains("Total2_"))
                            {

                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[i][k].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[start + i, k + 1].Value = value / 100;
                                }
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0%";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Percent2") && !dt.Columns[k].ColumnName.ToString().Contains("Total_") && !dt.Columns[k].ColumnName.ToString().Contains("Total2_"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[i][k].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[start + i, k + 1].Value = value / 100;
                                }
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00%";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Format2") && !dt.Columns[k].ColumnName.ToString().Contains("Total_") && !dt.Columns[k].ColumnName.ToString().Contains("Total2_"))
                            {
                                workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Format4") && !dt.Columns[k].ColumnName.ToString().Contains("Total_") && !dt.Columns[k].ColumnName.ToString().Contains("Total2_"))
                            {
                                workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Format12"))
                            {
                                workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                                if (dt.Rows[i][k] == null)
                                {
                                    continue;
                                }
                                string number = Convert.ToString(dt.Rows[i][k]);
                                if (number.Contains("."))
                                {
                                    while (number.Substring(number.Length - 1) == "0")
                                    {
                                        number = number.Substring(0, number.Length - 1);
                                    }
                                }
                                int degit = number.ToString().Substring(number.IndexOf(".") + 1).Length;
                                if (degit == 0)
                                {
                                    continue;
                                }
                                else if (degit == 1)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0";
                                }
                                else if (degit == 2)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00";
                                }
                                else if (degit == 3)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000";
                                }
                                else if (degit == 4)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000";
                                }
                                else if (degit == 5)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00000";
                                }
                                else if (degit == 6)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000000";
                                }
                                else if (degit == 7)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000000";
                                }
                                else if (degit == 8)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00000000";
                                }
                                else if (degit == 9)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000000000";
                                }
                                else if (degit == 10)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000000000";
                                }
                                else if (degit == 11)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00000000000";
                                }
                                else if (degit == 12)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000000000000";
                                }
                                continue;
                            }
                            workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                        }
                    }
                }
                if (isOneTotal)//Có Total
                {
                    int start = 1;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {

                        if (dt.Columns[i].ColumnName.ToString().Contains("Total_"))
                        {
                            string ColumnName = dt.Columns[i].ColumnName.ToString();
                            string[] ColumnNamearray = ColumnName.Split('_');
                            int cot = int.Parse(ColumnNamearray[ColumnNamearray.Length - 1]);
                            workSheet.Cells[1, 1, 1, cot].Style.Font.Bold = true;

                            if (dt.Columns[i].ColumnName.ToString().Contains("_Percent0"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[0][i].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[1, cot].Value = value / 100;
                                }

                                workSheet.Cells[1, cot].Style.Numberformat.Format = "#0%";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Percent2"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[0][i].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[1, cot].Value = value / 100;
                                }

                                workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00%";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Percent3"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[0][i].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[1, cot].Value = value / 100;
                                }

                                workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000%";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Format2"))
                            {
                                workSheet.Cells[1, cot].Value = dt.Rows[0][i];
                                workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Format4"))
                            {
                                workSheet.Cells[1, cot].Value = dt.Rows[0][i];
                                workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0000";
                                continue;
                            }
                            if (dt.Columns[i].ColumnName.ToString().Contains("_Format12"))
                            {
                                workSheet.Cells[1, cot].Value = dt.Rows[0][i];
                                if (dt.Rows[0][i] == null)
                                {
                                    continue;
                                }
                                string number = Convert.ToString(dt.Rows[0][i]);
                                if (number.Contains("."))
                                {
                                    while (number.Substring(number.Length - 1) == "0")
                                    {
                                        number = number.Substring(0, number.Length - 1);
                                    }
                                }
                                int degit = number.ToString().Substring(number.IndexOf(".") + 1).Length;
                                if (degit == 0)
                                {
                                    continue;
                                }
                                else if (degit == 1)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0";
                                }
                                else if (degit == 2)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00";
                                }
                                else if (degit == 3)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000";
                                }
                                else if (degit == 4)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0000";
                                }
                                else if (degit == 5)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00000";
                                }
                                else if (degit == 6)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000000";
                                }
                                else if (degit == 7)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0000000";
                                }
                                else if (degit == 8)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00000000";
                                }
                                else if (degit == 9)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000000000";
                                }
                                else if (degit == 10)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.0000000000";
                                }
                                else if (degit == 11)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.00000000000";
                                }
                                else if (degit == 12)
                                {
                                    workSheet.Cells[1, cot].Style.Numberformat.Format = "#0.000000000000";
                                }
                                continue;
                            }

                            workSheet.Cells[1, cot].Value = dt.Rows[0][i];
                        }
                    }

                    start = 1;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (dt.Columns[i].ColumnName.ToString().Contains("Total_"))
                        {
                            continue;
                        }
                        workSheet.Cells[2, start, 2, start].Style.Font.Bold = true;
                        workSheet.Cells[2, start].Value = dt.Columns[i].ColumnName.Replace("_Format2", "").Replace("_Format4", "").Replace("_Format12", "").Replace("_Percent0", "").Replace("_Percent2", "").Replace("_Percent3", "")
                        .Replace("_Double0", "").Replace("_Double1", "").Replace("_Double2", "").Replace("_Double3", "").Replace("_Double4", "");
                        workSheet.Cells["A2:" + Columns[start - 1] + "2"].AutoFilter = true;
                        start++;
                    }
                    start = 3;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        for (int k = 0; k < dt.Columns.Count; k++)
                        {
                            if (dt.Columns[k].ColumnName.ToString().Contains("Total_"))
                            {
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Percent0") && !dt.Columns[k].ColumnName.ToString().Contains("Total_"))
                            {

                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[i][k].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[start + i, k + 1].Value = value / 100;
                                }
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0%";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Percent2") && !dt.Columns[k].ColumnName.ToString().Contains("Total_"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[i][k].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[start + i, k + 1].Value = value / 100;
                                }
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00%";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Percent3") && !dt.Columns[k].ColumnName.ToString().Contains("Total_"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[i][k].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[start + i, k + 1].Value = value / 100;
                                }
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000%";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Format2") && !dt.Columns[k].ColumnName.ToString().Contains("Total_"))
                            {
                                workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Format4") && !dt.Columns[k].ColumnName.ToString().Contains("Total_"))
                            {
                                workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Format12"))
                            {
                                workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                                if (dt.Rows[i][k] == null)
                                {
                                    continue;
                                }
                                string number = Convert.ToString(dt.Rows[i][k]);
                                if (number.Contains("."))
                                {
                                    while (number.Substring(number.Length - 1) == "0")
                                    {
                                        number = number.Substring(0, number.Length - 1);
                                    }
                                }
                                int degit = number.ToString().Substring(number.IndexOf(".") + 1).Length;
                                if (degit == 0)
                                {
                                    continue;
                                }
                                else if (degit == 1)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0";
                                }
                                else if (degit == 2)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00";
                                }
                                else if (degit == 3)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000";
                                }
                                else if (degit == 4)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000";
                                }
                                else if (degit == 5)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00000";
                                }
                                else if (degit == 6)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000000";
                                }
                                else if (degit == 7)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000000";
                                }
                                else if (degit == 8)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00000000";
                                }
                                else if (degit == 9)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000000000";
                                }
                                else if (degit == 10)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000000000";
                                }
                                else if (degit == 11)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00000000000";
                                }
                                else if (degit == 12)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000000000000";
                                }
                                continue;
                            }
                            workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                        }
                    }
                }
                if (!isOneTotal && !isTwoTotal)//Không có Total
                {
                    int start = 1;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        workSheet.Cells[1, start, 1, start].Style.Font.Bold = true;
                        workSheet.Cells[1, start].Value = dt.Columns[i].ColumnName.Replace("_Format2", "").Replace("_Format4", "").Replace("_Format12", "").Replace("_Percent0", "").Replace("_Percent2", "").Replace("_Percent3", "")
                        .Replace("_Double0", "").Replace("_Double1", "").Replace("_Double2", "").Replace("_Double3", "").Replace("_Double4", "");
                        workSheet.Cells["A1:" + Columns[start - 1] + "1"].AutoFilter = true;
                        start++;
                    }
                    start = 2;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        for (int k = 0; k < dt.Columns.Count; k++)
                        {
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Percent0"))
                            {
                                //workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                                //workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00%";
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[i][k].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[start + i, k + 1].Value = value / 100;
                                }
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0%";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Percent2"))
                            {
                                //workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                                //workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00%";
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[i][k].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[start + i, k + 1].Value = value / 100;
                                }
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00%";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Percent3"))
                            {
                                Double value;
                                bool successfullyParsed = double.TryParse(dt.Rows[i][k].ToString(), out value);
                                if (successfullyParsed)
                                {
                                    workSheet.Cells[start + i, k + 1].Value = value / 100;
                                }
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000%";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Format2"))
                            {
                                workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Format4"))
                            {
                                workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                                workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000";
                                continue;
                            }
                            if (dt.Columns[k].ColumnName.ToString().Contains("_Format12"))
                            {
                                workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                                if (dt.Rows[i][k] == null)
                                {
                                    continue;
                                }
                                string number = Convert.ToString(dt.Rows[i][k]);
                                if (number.Contains("."))
                                {
                                    while (number.Substring(number.Length - 1) == "0")
                                    {
                                        number = number.Substring(0, number.Length - 1);
                                    }
                                }
                                int degit = number.ToString().Substring(number.IndexOf(".") + 1).Length;
                                if (degit == 0)
                                {
                                    continue;
                                }
                                else if (degit == 1)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0";
                                }
                                else if (degit == 2)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00";
                                }
                                else if (degit == 3)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000";
                                }
                                else if (degit == 4)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000";
                                }
                                else if (degit == 5)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00000";
                                }
                                else if (degit == 6)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000000";
                                }
                                else if (degit == 7)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000000";
                                }
                                else if (degit == 8)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00000000";
                                }
                                else if (degit == 9)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000000000";
                                }
                                else if (degit == 10)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000000000";
                                }
                                else if (degit == 11)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00000000000";
                                }
                                else if (degit == 12)
                                {
                                    workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000000000000";
                                }
                                continue;
                            }
                            workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                        }
                    }
                }
                return excel.GetAsByteArray();

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

﻿using Dapper;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TempModel.Development;
using StackExchange.Redis;
using Newtonsoft.Json;
using System.Web;

namespace Library
{
    public class TDMat_Repository
    {
        public async Task<TDMat> UpdateTDMat(TDMat_Custom item,string database,string Userlogin)
        {
            if (item.ValidFrom > DateTime.Today)
            {
                throw new NotImplementedException("ValidFrom larger than the current date");
            }
            using (PARKERPEntities context = new PARKERPEntities())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        using (var connection = new SqlConnection(database))
                        {
                            var queryParameters = new DynamicParameters();
                            connection.Open();
                            queryParameters.Add("@MatName", item.MatName);
                            queryParameters.Add("@Spec1", item.Spec1);
                            queryParameters.Add("@Spec2", item.Spec2);
                            queryParameters.Add("@Color", item.Color);
                            queryParameters.Add("@DMatCD", item.DMatCD);
                            var data = await connection.QueryAsync<TDMat_Custom>("ERP_Material_TDMat_CheckDouble", queryParameters, commandTimeout: 1500
                                , commandType: CommandType.StoredProcedure);
                            if (data.Any())
                            {
                                throw new NotImplementedException("Exist MatName, Spec1, Spec2, Color in DMatCD " + data.FirstOrDefault().DMatCD + ", MatCD " + data.FirstOrDefault().MatCD + "");
                            }
                        }



                        TDMat tdmat = new TDMat();
                        tdmat.NA = item.NA;
                        tdmat.FirstMatGrpID = item.MatGrpID;
                        tdmat.DMatCD = item.DMatCD;
                        //tdmat.MatCD = item.MatCD;
                        tdmat.MatTypeCD = item.MatTypeCD;
                        tdmat.MatCat = item.MatCat;
                        tdmat.MHID = item.MHID;
                        tdmat.MatName = item.MatName;
                        tdmat.Spec1 = item.Spec1;
                        tdmat.Spec2 = item.Spec2;
                        tdmat.Color = item.Color;
                        tdmat.MUnit = item.MUnit;
                        tdmat.YUnit = item.YUnit;
                        tdmat.LossRate = item.LossRate;
                        tdmat.LossQty = item.LossQty;
                        tdmat.Costing = item.Costing;
                        tdmat.PO = item.PO;
                        tdmat.MultiSrc = item.MultiSrc;
                        tdmat.RapMua = item.RapMua;
                        tdmat.GR = item.GR;
                        tdmat.AltMat = item.AltMat;
                        tdmat.OutSourcing = item.OutSourcing;
                        tdmat.HSCode = item.HSCode;
                        tdmat.Tag = item.Tag;
                        tdmat.Class = item.Class;
                        tdmat.UnderDev = item.UnderDev;
                        tdmat.ETAFromVendor = item.ETAFromVendor;
                        tdmat.ETDToCust = item.ETDToCust;
                        tdmat.Couier = item.Couier;
                        tdmat.TrackingNo = item.TrackingNo;
                        tdmat.SpecStatus = item.SpecStatus;
                        tdmat.SpecApprovedOn = item.SpecApprovedOn;
                        tdmat.QualityStatus = item.QualityStatus;
                        tdmat.QualityApprovedOn = item.QualityApprovedOn;
                        tdmat.TestReportNo = item.TestReportNo;
                        tdmat.DevRemark = item.DevRemark;










                        tdmat.MatShapeID = item.MatShapeID;
                        tdmat.MatStdW = item.MatStdW;
                        tdmat.MatStdH = item.MatStdH;
                        tdmat.MatStdUnit = item.MatStdUnit;
                        tdmat.MfgVendor = item.MfgVendor;
                        tdmat.Shipper = item.Shipper;
                        tdmat.ShipFrom = item.ShipFrom;
                        tdmat.PUnit = item.PUnit;
                        tdmat.PGrpCD = item.PGrpCD;
                        tdmat.VendorItemNo = item.VendorItemNo;
                        tdmat.CustItemNo1 = item.CustItemNo1;//CM#(Coach)
                        tdmat.CustItemNo2 = item.CustItemNo2;//Spare Par#(Samsonite)
                        tdmat.PLeadtime = item.PLeadtime;
                        tdmat.MOQ = item.MOQ;
                        tdmat.Incoterms = item.Incoterms;
                        tdmat.PLotSize = item.PLotSize;//Purchase Lot Size
                        tdmat.FreightRate = item.FreightRate;
                        tdmat.InsuranceRate = item.InsuranceRate;
                        tdmat.CommissionRate = item.CommissionRate;
                        tdmat.OtherCostRate = item.OtherCostRate;
                        tdmat.PriceLotSize = item.PriceLotSize;
                        tdmat.EXWPrice = item.EXWPrice; //ExWPrite
                        tdmat.EXWCurr = item.EXWCurr; //EXWCurr
                        tdmat.FOBPrice = item.FOBPrice;
                        tdmat.FOBCurr = item.FOBCurr;
                        tdmat.FOBcPrice = item.FOBcPrice;
                        tdmat.CIFPrice = item.CIFPrice;
                        tdmat.CIFCurr = item.CIFCurr;
                        tdmat.BuyerPrice = item.BuyerPrice;
                        tdmat.BuyerCurr = item.BuyerCurr;
                        tdmat.MinPackingQty = item.MinPackingQty;
                        tdmat.CBM = item.CBM;
                        tdmat.Weight = item.Weight;
                        tdmat.ValidFrom = item.ValidFrom;
                        tdmat.Remark = item.Remark;
                        //var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
                        //string createdbystring = claims?.FirstOrDefault()?.Subject?.Name;

                        //TDMat ob = context.TDMats.FirstOrDefault(x=>x.DMatCD==tdmat.DMatCD);
                        //if (ob!=null)
                        //{
                        //    tdmat.CreatedBy = ob.CreatedBy;
                        //    tdmat.CreatedOn = ob.CreatedOn;
                        //}


                        tdmat.CreatedBy = item.CreatedBy;
                        tdmat.CreatedOn = item.CreatedOn;
                        tdmat.UpdatedBy = int.Parse(Userlogin);
                        tdmat.UpdatedOn = DateTime.Now;
                        context.Entry(tdmat).State = System.Data.Entity.EntityState.Modified;
                        await context.SaveChangesAsync();

                        TMatGrpItem tmatgr = new TMatGrpItem();
                        tmatgr.MatGrpItemID = item.MatGrpItemID;
                        tmatgr.MatGrpID = item.MatGrpID;
                        tmatgr.DMatCD = item.DMatCD;
                        tmatgr.SrcMatGrpID = null;
                        tmatgr.Flag = false;
                        context.Entry(tmatgr).State = System.Data.Entity.EntityState.Modified;
                        await context.SaveChangesAsync();

                        transaction.Commit();
                        return tdmat;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new NotImplementedException(ex.Message.ToString());
                    }
                }
            }
        }
        public async Task<TDMat> InsertTDMat(TDMat_Custom item,string database)
        {
            try
            {
                if (string.IsNullOrEmpty(item.DMatCD))
                {
                    throw new NotImplementedException("DMatCD not null");
                }
                if (item.ValidFrom > DateTime.Today)
                {
                    throw new NotImplementedException("ValidFrom larger than the current date");
                }

                using (PARKERPEntities context = new PARKERPEntities())
                {
                    using (var transaction = context.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        try
                        {

                            using (var connection = new SqlConnection(database))
                            {
                                var queryParameters = new DynamicParameters();
                                connection.Open();
                                queryParameters.Add("@MatName", item.MatName);
                                queryParameters.Add("@Spec1", item.Spec1);
                                queryParameters.Add("@Spec2", item.Spec2);
                                queryParameters.Add("@Color", item.Color);
                                queryParameters.Add("@DMatCD", item.DMatCD);
                                var data = await connection.QueryAsync<TDMat_Custom>("ERP_Material_TDMat_CheckDouble", queryParameters, commandTimeout: 1500
                                    , commandType: CommandType.StoredProcedure);
                                if (data.Any())
                                {
                                    throw new NotImplementedException("Exist MatName, Spec1, Spec2, Color in DMatCD " + data.FirstOrDefault().DMatCD + ", MatCD " + data.FirstOrDefault().MatCD + "");
                                }
                            }



                            TDMat tdmat = new TDMat();
                            tdmat.NA = item.NA;
                            tdmat.FirstMatGrpID = item.MatGrpID;


                            //tdmat.OrigMatCD = item.MatCD;
                            //tdmat.MatCD = item.MatCD;
                            tdmat.MatTypeCD = item.MatTypeCD;
                            tdmat.MatCat = item.MatCat;
                            tdmat.MHID = item.MHID;
                            tdmat.MatName = item.MatName;
                            tdmat.Spec1 = item.Spec1;
                            tdmat.Spec2 = item.Spec2;
                            tdmat.Color = item.Color;
                            tdmat.MUnit = item.MUnit;
                            tdmat.YUnit = item.YUnit;
                            tdmat.LossRate = item.LossRate;
                            tdmat.LossQty = item.LossQty;
                            tdmat.Costing = item.Costing;
                            tdmat.PO = item.PO;
                            tdmat.MultiSrc = item.MultiSrc;
                            tdmat.RapMua = item.RapMua;
                            tdmat.GR = item.GR;
                            tdmat.AltMat = item.AltMat;
                            tdmat.OutSourcing = item.OutSourcing;
                            tdmat.HSCode = item.HSCode;
                            tdmat.Tag = item.Tag;
                            tdmat.Class = item.Class;
                            tdmat.UnderDev = item.UnderDev;


                            tdmat.ETAFromVendor = item.ETAFromVendor;
                            tdmat.ETDToCust = item.ETDToCust;
                            tdmat.Couier = item.Couier;
                            tdmat.TrackingNo = item.TrackingNo;
                            tdmat.SpecStatus = item.SpecStatus;
                            tdmat.SpecApprovedOn = item.SpecApprovedOn;
                            tdmat.QualityStatus = item.QualityStatus;
                            tdmat.QualityApprovedOn = item.QualityApprovedOn;
                            tdmat.TestReportNo = item.TestReportNo;
                            tdmat.DevRemark = item.DevRemark;


                            tdmat.MatShapeID = item.MatShapeID;
                            tdmat.MatStdW = item.MatStdW;
                            tdmat.MatStdH = item.MatStdH;
                            tdmat.MatStdUnit = item.MatStdUnit;
                            tdmat.MfgVendor = item.MfgVendor;
                            tdmat.Shipper = item.Shipper;
                            tdmat.ShipFrom = item.ShipFrom;
                            tdmat.PUnit = item.PUnit;
                            tdmat.PGrpCD = item.PGrpCD;
                            tdmat.VendorItemNo = item.VendorItemNo;
                            tdmat.CustItemNo1 = item.CustItemNo1;//CM#(Coach)
                            tdmat.CustItemNo2 = item.CustItemNo2;//Spare Par#(Samsonite)
                            tdmat.PLeadtime = item.PLeadtime;
                            tdmat.MOQ = item.MOQ;
                            tdmat.Incoterms = item.Incoterms;
                            tdmat.PLotSize = item.PLotSize;//Purchase Lot Size
                            tdmat.FreightRate = item.FreightRate;
                            tdmat.InsuranceRate = item.InsuranceRate;
                            tdmat.CommissionRate = item.CommissionRate;
                            tdmat.OtherCostRate = item.OtherCostRate;
                            tdmat.PriceLotSize = item.PriceLotSize;
                            tdmat.EXWPrice = item.EXWPrice; //ExWPrite
                            tdmat.EXWCurr = item.EXWCurr; //EXWCurr
                            tdmat.FOBPrice = item.FOBPrice;
                            tdmat.FOBCurr = item.FOBCurr;
                            tdmat.FOBcPrice = item.FOBcPrice;
                            tdmat.CIFPrice = item.CIFPrice;
                            tdmat.CIFCurr = item.CIFCurr;
                            tdmat.BuyerPrice = item.BuyerPrice;
                            tdmat.BuyerCurr = item.BuyerCurr;
                            tdmat.MinPackingQty = item.MinPackingQty;
                            tdmat.CBM = item.CBM;
                            tdmat.Weight = item.Weight;
                            tdmat.ValidFrom = item.ValidFrom;
                            tdmat.Remark = item.Remark;
                            var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
                            string createdbystring = claims?.FirstOrDefault()?.Subject?.Name;
                            tdmat.CreatedBy = int.Parse(createdbystring.ToString());
                            tdmat.CreatedOn = DateTime.Now;
                            tdmat.OrigMatCD = item.OrigMatCD;



                            //tdmat.DMatCD = item.DMatCD;

                            TDMat_Custom DMatCD_Last = await ERP_TDMat_GenerateDMatCD(database);
                            tdmat.DMatCD = DMatCD_Last.DMatCD;

                            context.TDMats.Add(tdmat);
                            await context.SaveChangesAsync();



                            TMatGrpItem tmatgr = new TMatGrpItem();
                            tmatgr.MatGrpID = item.MatGrpID;
                            tmatgr.DMatCD = DMatCD_Last.DMatCD;
                            tmatgr.SrcMatGrpID = null;
                            tmatgr.Flag = false;
                            context.TMatGrpItems.Add(tmatgr);
                            await context.SaveChangesAsync();

                            transaction.Commit();
                            return tdmat;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new NotImplementedException(ex.Message.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<TDMat_Custom> ERP_TDMat_GenerateDMatCD(string database)
        {
            try
            {
                //IDatabase db = ConnectRedis();
                //string DMatCD_Redis = await db.StringGetAsync("DMatCDGenerate");
                using (var connection = new SqlConnection(database))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<TDMat_Custom>("ERP_TDMat_GenerateDMatCD", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    //if (!string.IsNullOrEmpty(DMatCD_Redis))
                    //{
                    //    TDMat_Custom ob = new TDMat_Custom();
                    //    DMatCD_Redis = DMatCD_Redis.Replace("D", "");
                    //    int number = int.Parse(DMatCD_Redis) +1;
                    //    DMatCD_Redis =String.Format("{0:D7}", number);
                    //    DMatCD_Redis = "D" + DMatCD_Redis;
                    //    ob.DMatCD = DMatCD_Redis;
                    //    ob.CreatedOn = DateTime.Now;
                    //    await db.StringSetAsync("DMatCDGenerate", DMatCD_Redis);
                    //    return ob;
                    //}
                    //await db.StringSetAsync("DMatCDGenerate", data.FirstOrDefault().DMatCD);
                    TDMat_Custom tdmat = data.FirstOrDefault();
                    tdmat.CreatedOn = DateTime.Now;
                    return tdmat;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<PressModel>> ERP_FDMat_AddPressMaterial_Insert(List<PressModel> dt, string database
            , string Userlogin)
        {
            try
            {
                foreach (PressModel ob in dt)
                {
                    using (var connection = new SqlConnection(database))
                    {

                        var queryParameters = new DynamicParameters();
                        connection.Open();
                        string query = "SELECT a.* FROM dbo.TDMat a (NOLOCK) INNER JOIN dbo.TMatGrpItem b (NOLOCK) ON a.DMatCD=b.DMatCD AND b.MatGrpID=" + ob.MatGrpID_Old + " WHERE a.MatCD='" + ob.MatCD + "' ";
                        var data = await connection.QueryAsync<TDMat>(query, queryParameters, commandTimeout: 1500
                            , commandType: CommandType.Text);

                        connection.Close();


                        TDMat item = data.FirstOrDefault();
                        TDMat_Custom tdmat = new TDMat_Custom();
                        tdmat.NA = true;
                        tdmat.FirstMatGrpID = ob.MatGrpID;

                        TDMat_Custom DMatCD = await ERP_TDMat_GenerateDMatCD(database);
                        tdmat.DMatCD = DMatCD.DMatCD;

                        tdmat.OrigMatCD = item.MatCD;

                        tdmat.MatTypeCD = item.MatTypeCD;
                        tdmat.MatCat = item.MatCat;
                        tdmat.MHID = item.MHID;
                        tdmat.MatName = item.MatName;
                        tdmat.Spec1 = item.Spec1;
                        tdmat.Spec2 = ob.MatDesc;//
                        tdmat.Color = item.Color;
                        //Lưu ý
                        tdmat.MUnit = "PC";
                        tdmat.YUnit = "PC";

                        tdmat.LossRate = item.LossRate;
                        tdmat.LossQty = item.LossQty;
                        tdmat.Costing = item.Costing;
                        tdmat.PO = item.PO;
                        tdmat.MultiSrc = item.MultiSrc;
                        tdmat.GR = item.GR;
                        tdmat.AltMat = item.AltMat;
                        tdmat.OutSourcing = item.OutSourcing;
                        tdmat.HSCode = item.HSCode;
                        tdmat.Tag = item.Tag;
                        tdmat.Class = item.Class;
                        tdmat.UnderDev = item.UnderDev;


                        tdmat.ETAFromVendor = item.ETAFromVendor;
                        tdmat.ETDToCust = item.ETDToCust;
                        tdmat.Couier = item.Couier;
                        tdmat.TrackingNo = item.TrackingNo;
                        tdmat.SpecStatus = item.SpecStatus;
                        tdmat.SpecApprovedOn = item.SpecApprovedOn;
                        tdmat.QualityStatus = item.QualityStatus;
                        tdmat.QualityApprovedOn = item.QualityApprovedOn;
                        tdmat.TestReportNo = item.TestReportNo;
                        tdmat.DevRemark = item.DevRemark;


                        tdmat.MatStdH = item.MatStdH;
                        //Lưu ý

                        tdmat.MatStdUnit = "PC";

                        //

                        tdmat.MfgVendor = item.MfgVendor;
                        tdmat.Shipper = item.Shipper;
                        tdmat.ShipFrom = item.ShipFrom;
                        //Lưu ý

                        tdmat.PUnit = "PC";

                        //
                        tdmat.PGrpCD = item.PGrpCD;
                        tdmat.VendorItemNo = item.VendorItemNo;
                        tdmat.CustItemNo1 = item.CustItemNo1;//CM#(Coach)
                        tdmat.CustItemNo2 = item.CustItemNo2;//Spare Par#(Samsonite)
                        tdmat.PLeadtime = item.PLeadtime;
                        tdmat.MOQ = item.MOQ;
                        tdmat.Incoterms = ob.Incoterms;//
                        tdmat.PLotSize = item.PLotSize;
                        tdmat.FreightRate = item.FreightRate;
                        tdmat.InsuranceRate = item.InsuranceRate;
                        tdmat.CommissionRate = ob.CommissionRate;
                        tdmat.OtherCostRate = item.OtherCostRate;
                        tdmat.PriceLotSize = item.PriceLotSize;
                        tdmat.EXWPrice = ob.EXWPrice;
                        tdmat.EXWCurr = ob.EXWCurr;
                        tdmat.FOBPrice = ob.FOBPrice;
                        tdmat.FOBCurr = ob.FOBCurr;
                        tdmat.FOBcPrice = ob.FOBcPrice;
                        tdmat.CIFPrice = ob.CIFPrice;
                        tdmat.CIFCurr = ob.CIFCurr;
                        tdmat.BuyerPrice = ob.BuyerPrice;
                        tdmat.BuyerCurr = ob.BuyerCurr;
                        tdmat.MinPackingQty = item.MinPackingQty;
                        tdmat.CBM = item.CBM;
                        tdmat.Weight = item.Weight;
                        tdmat.ValidFrom = DateTime.Today;//item.ValidFrom;
                        tdmat.Remark = item.Remark;
                        var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
                        string createdbystring = claims?.FirstOrDefault()?.Subject?.Name;
                        tdmat.CreatedBy = int.Parse(createdbystring.ToString());
                        tdmat.CreatedOn = DateTime.Now;
                        tdmat.MatGrpID = ob.MatGrpID;


                        tdmat.RapMua = false;
                        tdmat.MatStdW = null;
                        tdmat.MatShapeID = 4;


                        TDMat kq = await InsertTDMat(tdmat,database);

                        await ERP_FDMat_AddPressMaterial_DeleteRedisByMatGrpID(ob, Userlogin);
                        ob.DMatCD = kq.DMatCD;
                    }


                }
                return await Task.FromResult(dt);

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_FDMat_AddPressMaterial_DeleteRedisByMatGrpID(PressModel item,string Userlogin)
        {



            IDatabase db = ConnectRedis();
            try
            {
                string valued = await db.StringGetAsync("FDMat_AddPressMaterial_" + Userlogin);
                if (!string.IsNullOrEmpty(valued))
                {
                    List<PressModel> InsChkTempByUser = JsonConvert.DeserializeObject<List<PressModel>>(valued);
                    InsChkTempByUser = InsChkTempByUser.Where(x => x.MatGrpID != item.MatGrpID).ToList();
                    string value = JsonConvert.SerializeObject(InsChkTempByUser);
                    await db.StringSetAsync("FDMat_AddPressMaterial_" + Userlogin, value);
                }
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public IDatabase ConnectRedis()
        {
            //var host = Dns.GetHostEntry(Dns.GetHostName());
            //foreach (var ip in host.AddressList)
            //{
            //    if (ip.AddressFamily == AddressFamily.InterNetwork)
            //    {
            //        string a= ip.ToString();
            //    }
            //}
            bool isLocal = HttpContext.Current.Request.IsLocal;
            ConnectionMultiplexer redis = null;
            if (isLocal)
            {
                redis = ConnectionMultiplexer.Connect("localhost");
                return redis.GetDatabase();
            }
            redis = ConnectionMultiplexer.Connect("localhost,abortConnect=false,connectTimeout=30000,responseTimeout=30000");
            return redis.GetDatabase();
        }
    }
}

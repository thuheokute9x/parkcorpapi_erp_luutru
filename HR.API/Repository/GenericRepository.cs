﻿
using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository
{
    public class GenericRepository<T> : IGenericRepository<T>, IDisposable where T : class
    {

        private IDbSet<T> _entities;
        private string _errorMessage = string.Empty;
        private bool _isDisposed;

        public GenericRepository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : this(unitOfWork.Context)
        {
        }

        public GenericRepository(PARKERPEntities context)
        {
            _isDisposed = false;
            Context = context;
        }

        public PARKERPEntities Context { get; set; }

        public virtual IQueryable<T> Table
        {
            get { return Entities; }
        }

        protected virtual IDbSet<T> Entities
        {
            get { return _entities ?? (_entities = Context.Set<T>()); }
        }

        public void Dispose()
        {
            if (Context != null)
                Context.Dispose();
            _isDisposed = true;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return Entities.ToList();
        }

        public virtual T GetById(object id)
        {
            return Entities.Find(id);
        }

        public virtual void Insert(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");
                Entities.Add(entity);

                if (Context == null || _isDisposed)
                    Context = new PARKERPEntities();

                //Context.SaveChanges(); commented out call to SaveChanges as Context save changes will be 6+
                //called with Unit of work
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                throw new Exception(_errorMessage, dbEx);
            }
        }
        public async Task<bool> Insert_SaveChange(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");
                Entities.Add(entity);

                if (Context == null || _isDisposed)
                    Context = new PARKERPEntities();

                var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
                string createdbystring = claims?.FirstOrDefault()?.Subject?.Name;
                var CreatedBy = entity.GetType().GetProperty("CreatedBy", BindingFlags.Public | BindingFlags.Instance);
                if (CreatedBy != null && CreatedBy.CanWrite && !string.IsNullOrEmpty(createdbystring))
                {
                    CreatedBy.SetValue(entity, int.Parse(createdbystring), null);
                }
                var CreatedOn = entity.GetType().GetProperty("CreatedOn", BindingFlags.Public | BindingFlags.Instance);
                if (CreatedOn != null && CreatedOn.CanWrite)
                {
                    CreatedOn.SetValue(entity, DateTime.Now, null);
                }
                await Context.SaveChangesAsync();
                return await Task.FromResult(true);
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                throw new Exception(_errorMessage, dbEx);
            }

            catch (Exception e)
            {
                throw new NotImplementedException(e.InnerException.InnerException.Message.ToString());
            }
        }

        public async Task<List<T>> BulkInsert(IEnumerable<T> entities)
        {
            try
            {
                if (entities == null)
                {
                    throw new ArgumentNullException("entities");
                }
                Context.Configuration.AutoDetectChangesEnabled = false;
                var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
                string createdbystring = claims?.FirstOrDefault()?.Subject?.Name;
                foreach (T item in entities)
                {
                    var CreatedBy = item.GetType().GetProperty("CreatedBy", BindingFlags.Public | BindingFlags.Instance);
                    if (CreatedBy != null && CreatedBy.CanWrite && !string.IsNullOrEmpty(createdbystring))
                    {
                        CreatedBy.SetValue(item, int.Parse(createdbystring), null);
                    }
                    var CreatedOn = item.GetType().GetProperty("CreatedOn", BindingFlags.Public | BindingFlags.Instance);
                    if (CreatedOn != null && CreatedOn.CanWrite)
                    {
                        CreatedOn.SetValue(item, DateTime.Now, null);
                    }
                }


                Context.Set<T>().AddRange(entities);
                await Context.SaveChangesAsync();
                return entities.ToList();
            }
            catch (DbEntityValidationException dbEx)
            {

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName,
                                             validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(_errorMessage, dbEx);
            }
            catch (Exception e)
            {
                throw new NotImplementedException(e.ToString());
            }

        }
        public List<T> BulkInsertNotSaveChange(PARKERPEntities Context, IEnumerable<T> entities)
        {
            try
            {
                if (entities == null)
                {
                    throw new ArgumentNullException("entities");
                }
                Context.Configuration.AutoDetectChangesEnabled = false;
                foreach (T item in entities)
                {
                    var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
                    string createdbystring = claims?.FirstOrDefault()?.Subject?.Name;
                    var CreatedBy = item.GetType().GetProperty("CreatedBy", BindingFlags.Public | BindingFlags.Instance);
                    if (CreatedBy != null && CreatedBy.CanWrite && !string.IsNullOrEmpty(createdbystring))
                    {
                        CreatedBy.SetValue(item, int.Parse(createdbystring), null);
                    }
                    var CreatedOn = item.GetType().GetProperty("CreatedOn", BindingFlags.Public | BindingFlags.Instance);
                    if (CreatedOn != null && CreatedOn.CanWrite)
                    {
                        CreatedOn.SetValue(item, DateTime.Now, null);
                    }
                }


                Context.Set<T>().AddRange(entities);
                return entities.ToList();
            }
            catch (DbEntityValidationException dbEx)
            {

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName,
                                             validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(_errorMessage, dbEx);
            }
            catch (Exception e)
            {
                throw new NotImplementedException(e.ToString());
            }

        }
        public async Task<List<T>> BulkDelete(IEnumerable<T> entities)
        {
            try
            {
                if (entities == null)
                {
                    throw new ArgumentNullException("entities");
                }
                Context.Configuration.AutoDetectChangesEnabled = false;
                Context.Set<T>().RemoveRange(entities);
                await Context.SaveChangesAsync();
                return entities.ToList();
            }
            catch (DbEntityValidationException dbEx)
            {

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName,
                                             validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(_errorMessage, dbEx);
            }
            catch (Exception e)
            {
                throw new NotImplementedException(e.ToString());
            }

        }
        public List<T> BulkDeleteNotSaveChange(IEnumerable<T> entities)
        {
            try
            {
                if (entities == null)
                {
                    throw new ArgumentNullException("entities");
                }
                Context.Set<T>().RemoveRange(entities);
                //Context.Entry(entities).State = System.Data.Entity.EntityState.Deleted;
                return entities.ToList();
            }
            catch (DbEntityValidationException dbEx)
            {

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName,
                                             validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(_errorMessage, dbEx);
            }
            catch (Exception e)
            {
                throw new NotImplementedException(e.ToString());
            }

        }
        public async Task<bool> Update(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                if (Context == null || _isDisposed)
                    Context = new PARKERPEntities();

                SetEntryModified(entity);
                var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
                string updatedbystring = claims?.FirstOrDefault()?.Subject?.Name;
                var UpdatedBy = entity.GetType().GetProperty("UpdatedBy", BindingFlags.Public | BindingFlags.Instance);
                if (UpdatedBy != null && UpdatedBy.CanWrite && !string.IsNullOrEmpty(updatedbystring))
                {
                    UpdatedBy.SetValue(entity, int.Parse(updatedbystring), null);
                }
                var UpdatedOn = entity.GetType().GetProperty("UpdatedOn", BindingFlags.Public | BindingFlags.Instance);
                if (UpdatedOn != null && UpdatedOn.CanWrite)
                {
                    UpdatedOn.SetValue(entity, DateTime.Now, null);
                }
                return await Task.FromResult(true);

                //Context.SaveChanges(); commented out call to SaveChanges as Context save changes will be called with Unit of work
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        _errorMessage += Environment.NewLine + string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);

                throw new Exception(_errorMessage, dbEx);
            }
            catch (Exception e)
            {
                throw new NotImplementedException(e.InnerException.InnerException.Message.ToString());
            }
        }
        public async Task<bool> Update_SaveChange(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                if (Context == null || _isDisposed)
                    Context = new PARKERPEntities();

                SetEntryModified(entity);
                var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
                string updatedbystring = claims?.FirstOrDefault()?.Subject?.Name;
                var UpdatedBy = entity.GetType().GetProperty("UpdatedBy", BindingFlags.Public | BindingFlags.Instance);
                if (UpdatedBy != null && UpdatedBy.CanWrite && !string.IsNullOrEmpty(updatedbystring))
                {
                    UpdatedBy.SetValue(entity, int.Parse(updatedbystring), null);
                }
                var UpdatedOn = entity.GetType().GetProperty("UpdatedOn", BindingFlags.Public | BindingFlags.Instance);
                if (UpdatedOn != null && UpdatedOn.CanWrite)
                {
                    UpdatedOn.SetValue(entity, DateTime.Now, null);
                }


                await Context.SaveChangesAsync();
                return await Task.FromResult(true);
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        _errorMessage += Environment.NewLine + string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);

                throw new Exception(_errorMessage, dbEx);
            }
            catch (Exception e)
            {
                throw new NotImplementedException(e.InnerException.InnerException.Message.ToString());
            }
        }

        public async Task<bool> Delete(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                if (Context == null || _isDisposed)
                    Context = new PARKERPEntities();

                //Entities.Remove(entity);
                Context.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                return await Task.FromResult(true);
                //Context.SaveChanges(); commented out call to SaveChanges as Context save changes will be called with Unit of work
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        _errorMessage += Environment.NewLine + string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                throw new Exception(_errorMessage, dbEx);
            }
            catch (Exception e)
            {
                throw new NotImplementedException(e.InnerException.InnerException.Message.ToString());
            }
        }
        public async Task<bool> Delete_SaveChange(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                if (Context == null || _isDisposed)
                    Context = new PARKERPEntities();

                //Entities.Remove(entity);
                Context.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                await Context.SaveChangesAsync();
                return await Task.FromResult(true);
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        _errorMessage += Environment.NewLine + string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                throw new Exception(_errorMessage, dbEx);
            }
            catch (Exception e)
            {
                throw new NotImplementedException(e.InnerException.InnerException.Message.ToString());
            }
        }

        public virtual void SetEntryModified(T entity)
        {
            Context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            var CreatedBy = entity.GetType().GetProperty("CreatedBy", BindingFlags.Public | BindingFlags.Instance);
            if (CreatedBy != null)
            {
                Context.Entry(entity).Property("CreatedBy").IsModified = false;
            }
            var CreatedOn = entity.GetType().GetProperty("CreatedOn", BindingFlags.Public | BindingFlags.Instance);
            if (CreatedOn != null)
            {
                Context.Entry(entity).Property("CreatedOn").IsModified = false;
            }

        }

        public async Task<T> Insert_ReturnOBJ(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");
                Entities.Add(entity);

                if (Context == null || _isDisposed)
                    Context = new PARKERPEntities();

                var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
                string createdbystring = claims?.FirstOrDefault()?.Subject?.Name;
                var CreatedBy = entity.GetType().GetProperty("CreatedBy", BindingFlags.Public | BindingFlags.Instance);
                if (CreatedBy != null && CreatedBy.CanWrite && !string.IsNullOrEmpty(createdbystring))
                {
                    CreatedBy.SetValue(entity, int.Parse(createdbystring), null);
                }
                var CreatedOn = entity.GetType().GetProperty("CreatedOn", BindingFlags.Public | BindingFlags.Instance);
                if (CreatedOn != null && CreatedOn.CanWrite)
                {
                    CreatedOn.SetValue(entity, DateTime.Now, null);
                }
                await Context.SaveChangesAsync();
                return entity;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                throw new Exception(_errorMessage, dbEx);
            }
            catch (Exception e)
            {
                throw new NotImplementedException(e.InnerException.InnerException.Message.ToString());
            }
        }
        public async Task<bool> UpdateFieldsSave(T entity, params Expression<Func<T, object>>[] includeProperties)
        {
            using (var context = new PARKERPEntities())
            {
                var dbEntry = context.Entry(entity);
                foreach (var includeProperty in includeProperties)
                {
                    dbEntry.Property(includeProperty).IsModified = true;
                }
                await context.SaveChangesAsync();
                return await Task.FromResult(true);
            }
        }
        public string GetUserlogin()
        {
            var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
            string userlogin = claims?.FirstOrDefault()?.Subject?.Name;
            if (string.IsNullOrEmpty(userlogin))
            {
                throw new NotImplementedException("Pleace Login again");
            }
            return userlogin;
        }
        public string GetDB()
        {
            var claimslist = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
            var claims = claimslist?.ToList();
            string Company = "";
            if (claims.Count > 1)
            {
                Company = claims[1].Value;
            }
            if (string.IsNullOrEmpty(Company))
            {
                throw new NotImplementedException("Pleace Login again");
            }
            return Company;
        }
        public string WIP_GetProdInstCDWithCompany()
        {
            var claimslist = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
            var claims = claimslist?.ToList();
            string Company = "";
            if (claims.Count > 1)
            {
                Company = claims[1].Value;
            }
            if (string.IsNullOrEmpty(Company))
            {
                throw new NotImplementedException("Pleace Login again");
            }
            if (Company == "PCV")
            {
                return "VK";
            }
            if (Company == "CYV")
            {
                return "CK";
            }
            if (Company == "PBC")
            {
                return "BG";
            }
            if (Company == "PHC")
            {
                return "KK";
            }
            if (Company == "PAH")
            {
                return "AK";
            }
            return "";
        }
        public IDatabase ConnectRedis()
        {
            //var host = Dns.GetHostEntry(Dns.GetHostName());
            //foreach (var ip in host.AddressList)
            //{
            //    if (ip.AddressFamily == AddressFamily.InterNetwork)
            //    {
            //        string a= ip.ToString();
            //    }
            //}
            bool isLocal = HttpContext.Current.Request.IsLocal;
            ConnectionMultiplexer redis = null;
            if (isLocal)
            {
                redis = ConnectionMultiplexer.Connect("localhost");
                return redis.GetDatabase();
            }
            redis = ConnectionMultiplexer.Connect("localhost,abortConnect=false,connectTimeout=30000,responseTimeout=30000");
            return redis.GetDatabase();
        }
    }
}
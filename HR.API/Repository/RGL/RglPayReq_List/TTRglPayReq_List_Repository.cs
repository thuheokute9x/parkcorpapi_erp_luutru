﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.RglPayReq_List
{
    public class TTRglPayReq_List_Repository : GenericRepository<TTRglPayReq_List>, ITTRglPayReq_List_Repository
    {
        public TTRglPayReq_List_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TTRglPayReq_List_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<bool> ERP_TTRglPayReq_List_BulkInsert(string RglPayReqID)
        {
            string userlogin = GetUserlogin();
            IDatabase db = ConnectRedis();
            string valued = await db.StringGetAsync("TTRglPayReq_List_InsChkTempByUser_" + RglPayReqID + "_" + userlogin);
            if (!string.IsNullOrEmpty(valued))
            {
                List<TTRglPayReq_List_Custom> InsChkTempByUser = JsonConvert.DeserializeObject<List<TTRglPayReq_List_Custom>>(valued);
                var listInsert = (from a in InsChkTempByUser
                               select new TTRglPayReq_List { RglPayReqID = a.RglPayReqID, PayReqID = a.PayReqID }).ToList();
                await this.BulkInsert(listInsert);
                await db.KeyDeleteAsync("TTRglPayReq_List_InsChkTempByUser_" + RglPayReqID + "_" + userlogin);
                return await Task.FromResult(true);

            }
            if (string.IsNullOrEmpty(valued))
            {
                throw new NotImplementedException("Please choose PayReqID");
            }
            return await Task.FromResult(true);
        }
        public async Task<bool> ERP_TTRglPayReq_List_Insert_InsChkTempByUser(TTRglPayReq_List_Custom item)
        {
            string userlogin = GetUserlogin();
            IDatabase db = ConnectRedis();
            try
            {

                string valued = await db.StringGetAsync("TTRglPayReq_List_InsChkTempByUser_"+ item.RglPayReqID+ "_" + userlogin);
                if (!string.IsNullOrEmpty(valued))
                {
                    if (item.Checked)//Nếu Checked = true thì insert
                    {
                        List<TTRglPayReq_List_Custom> InsChkTempByUser = JsonConvert.DeserializeObject<List<TTRglPayReq_List_Custom>>(valued);
                        InsChkTempByUser.Add(item);
                        string value = JsonConvert.SerializeObject(InsChkTempByUser);
                        await db.StringSetAsync("TTRglPayReq_List_InsChkTempByUser_" + item.RglPayReqID + "_" + userlogin, value);
                    }
                    if (!item.Checked)//Nếu Checked = false thì xóa cái cũ đã inser của PayReqID đó đi trong redis
                    {
                        List<TTRglPayReq_List_Custom> InsChkTempByUser = JsonConvert.DeserializeObject<List<TTRglPayReq_List_Custom>>(valued);
                        string value = JsonConvert.SerializeObject(InsChkTempByUser.Where(x=>x.PayReqID != item.PayReqID));
                        await db.StringSetAsync("TTRglPayReq_List_InsChkTempByUser_" + item.RglPayReqID + "_" + userlogin, value);
                    }
                }
                if (string.IsNullOrEmpty(valued))
                {
                    List<TTRglPayReq_List_Custom> InsChkTempByUser = new List<TTRglPayReq_List_Custom>();
                    InsChkTempByUser.Add(item);
                    string value = JsonConvert.SerializeObject(InsChkTempByUser);
                    await db.StringSetAsync("TTRglPayReq_List_InsChkTempByUser_" + item.RglPayReqID + "_" + userlogin, value);
                }
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                await db.KeyDeleteAsync("TTRglPayReq_List_InsChkTempByUser_" + item.RglPayReqID + "_" + userlogin);
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<IList<TTRglPayReq_List_Custom>> ERP_TTRglPayReq_List_Get_InsChkTempByUser(string RglPayReqID)
        {
            try
            {
                string userlogin = GetUserlogin();
                IDatabase db = ConnectRedis();
                string valued = await db.StringGetAsync("TTRglPayReq_List_InsChkTempByUser_" + RglPayReqID + "_" + userlogin);
                if (!string.IsNullOrEmpty(valued))
                {
                    return JsonConvert.DeserializeObject<List<TTRglPayReq_List_Custom>>(valued);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }

        public async Task<IList<STPayReq_Custom>> ERP_TTRglPayReq_List_ByRglPayReqID(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RglPayReqID", RglPayReqID);
                    connection.Open();
                    var data = await connection.QueryAsync<STPayReq_Custom>("ERP_TTRglPayReq_List_ByRglPayReqID", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
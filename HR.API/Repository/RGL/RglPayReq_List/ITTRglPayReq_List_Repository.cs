﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.RglPayReq_List
{
    interface ITTRglPayReq_List_Repository : IGenericRepository<TTRglPayReq_List>
    {
        Task<Boolean> ERP_TTRglPayReq_List_BulkInsert(string RglPayReqID);
        Task<IList<STPayReq_Custom>> ERP_TTRglPayReq_List_ByRglPayReqID(string RglPayReqID);
        Task<Boolean> ERP_TTRglPayReq_List_Insert_InsChkTempByUser(TTRglPayReq_List_Custom item);
        Task<IList<TTRglPayReq_List_Custom>> ERP_TTRglPayReq_List_Get_InsChkTempByUser(string RglPayReqID);
        

    }
}

﻿using HR.API.IRepository;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.RglPayReq
{
    interface ITTRglPayReqRepository : IGenericRepository<TTRglPayReq>
    {
        Task<IList<TTRglPayReq_Custom>> ERP_TTRglPayReq_GetPageSize(TTRglPayReq_Custom item);
        Task<IList<TTRglPayReq>> ERP_TTRglPayReq_GetRglPayReqID(string RglPayReqID);
        Task<IList<RglPayReq_DocGrp>> ERP_TTRglPayReq_DocGrp(string RglPayReqID);
        Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp(string RglPayReqID);
        Task<IList<Rgl_Report_WithPayDocNo>> ERP_TTRglPayReq_Report_WithPayDocNo(string RglPayReqID);
        Task<byte[]> ERP_TTRglPayReq_Report_WithPayDocNo_Excel(string RglPayReqID);
        Task<IList<ERP_TTRglPayReq_Report_WithPayReqID>> ERP_TTRglPayReq_Report_WithPayReqID(ERP_TTRglPayReq_Report_WithPayReqID RglPayReqID);
        DataTable ERP_TTRglPayReq_Report_WithPayReqID_Excel(ERP_TTRglPayReq_Report_WithPayReqID RglPayReqID);
        Task<IList<RglPayReq_DocGrp>> ERP_TTRglPayReq_DocGrp_GetVendor(string RglPayReqID);
        Task<IList<RglPayReq_DocGrp>> ERP_TTRglPayReq_DocGrp_GetPayReqID(RglPayReq_DocGrp item);
        Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp_GetVendorCD(string RglPayReqID);
        Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp_PayDocCD(string RglPayReqID, string VendorCD);
        Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp_GetPayDocNo(string RglPayReqID, string VendorCD, string PayDocCD);
        Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_PayReq_GetPayDocNo(string RglPayReqID);
        Task<IList<TTRglPayReq_List>> ERP_TTRglPayReq_PayReq_PayReqID(TTRglPayReq_List_Custom item);
        Task<bool> ERP_TTRglPayReq_List_UpdateFilePath(TTRglPayReq_List_Custom item);
        Task<bool> ERP_TTRglPayReq_List_DeleteFilePath(TTRglPayReq_List_Custom PayReq);
        Task<Result> ERP_TTRglPayReq_CheckPreviewRglPayReq(string RglPayReqID);
        Task<IList<TVendor_Custom>> ERP_TTRglPayReq_List_GetVendorBanInfor(string RglPayReqID);
        Task<Boolean> ERP_TTRglPayReq_UpdateConfirmedOn(TTRglPayReq_Custom item);

    }
}

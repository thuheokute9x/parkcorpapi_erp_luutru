﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.RglPayReq
{
    public class TTRglPayReqRepository : GenericRepository<TTRglPayReq>, ITTRglPayReqRepository
    {
        public TTRglPayReqRepository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TTRglPayReqRepository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TTRglPayReq>> ERP_TTRglPayReq_GetRglPayReqID(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                  string query = "SELECT TOP (15) RglPayReqID  FROM dbo.TTRglPayReq (NOLOCK) WHERE RglPayReqID LIKE N'%"+ RglPayReqID + "%' OR '"+ RglPayReqID + "'='' ORDER BY RglPayReqID DESC";
                    var data = await connection.QueryAsync<TTRglPayReq>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<TTRglPayReq_Custom>> ERP_TTRglPayReq_GetPageSize(TTRglPayReq_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RglPayReqID", item.RglPayReqID);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    connection.Open();
                    var data = await connection.QueryAsync<TTRglPayReq_Custom>("ERP_TTRglPayReq_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<RglPayReq_DocGrp>> ERP_TTRglPayReq_DocGrp(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RglPayReqID", RglPayReqID);
                    connection.Open();
                    var data = await connection.QueryAsync<RglPayReq_DocGrp>("ERP_TTRglPayReq_DocGrp", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RglPayReqID", RglPayReqID);
                    connection.Open();
                    var data = await connection.QueryAsync<RglPayReq_VndGrp>("ERP_TTRglPayReq_VndGrp", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<Rgl_Report_WithPayDocNo>> ERP_TTRglPayReq_Report_WithPayDocNo(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RglPayReqID", RglPayReqID);
                    connection.Open();
                    var data = await connection.QueryAsync<Rgl_Report_WithPayDocNo>("ERP_TTRglPayReq_Report_WithPayDocNo", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<byte[]> ERP_TTRglPayReq_Report_WithPayDocNo_Excel(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RglPayReqID", RglPayReqID);
                    connection.Open();
                    var data = await connection.QueryAsync<Rgl_Report_WithPayDocNo>("ERP_TTRglPayReq_Report_WithPayDocNo", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);


                    ExcelPackage excel = new ExcelPackage();
                    var workSheet = excel.Workbook.Worksheets.Add("RglPayReqVendor_" + RglPayReqID);
                    if (data.Any())
                    {
                        Rgl_Report_WithPayDocNo itemfirst = data.FirstOrDefault();
                        

                        workSheet.Cells[1, 3].Value = "";
                        workSheet.Cells[1, 3, 1, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        workSheet.Cells[1, 3, 1, 10].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#bacfe4"));
                        workSheet.Cells[1, 4].Value = "USD";
                        workSheet.Cells[1, 5].Value = itemfirst.Total;
                        workSheet.Cells[1, 6].Value = itemfirst.SumAmtMat;
                        workSheet.Cells[1, 7].Value = itemfirst.SumSurcharge;
                        workSheet.Cells[1, 8].Value = itemfirst.SumDN;
                        workSheet.Cells[1, 9].Value = itemfirst.SumCN;
                        workSheet.Cells[1, 10].Value = itemfirst.SumOT;

                        workSheet.Cells[2, 3].Value = "Total((1)+(2)-(3)+(4)+(5)) :";
                        workSheet.Cells[2, 3, 2, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        workSheet.Cells[2, 3, 2, 10].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#bacfe4"));
                        workSheet.Cells[2, 4].Value = "etc";
                        workSheet.Cells[2, 5].Value = itemfirst.TotalNotUSD;
                        workSheet.Cells[2, 6].Value = itemfirst.SumAmtMatNotUSD;
                        workSheet.Cells[2, 7].Value = itemfirst.SumSurchargeNotUSD;
                        workSheet.Cells[2, 8].Value = itemfirst.SumDNNotUSD;
                        workSheet.Cells[2, 9].Value = itemfirst.SumCNNotUSD;
                        workSheet.Cells[2, 10].Value = itemfirst.SumOTNotUSD;

                        workSheet.Cells[3, 1, 3, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        workSheet.Cells[3, 1, 3, 10].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#40a9ff"));
                        workSheet.Cells[3, 1, 3, 10].Style.Font.Bold = true;
                        workSheet.Cells[3, 1].Value = "VendorCD";
                        workSheet.Cells[3, 2].Value = "PayDocCD";
                        workSheet.Cells[3, 3].Value = "PayDocNo";
                        workSheet.Cells[3, 4].Value = "Curr";
                        workSheet.Cells[3, 5].Value = "Sum";
                        workSheet.Cells[3, 6].Value = "(1)AmtMat";
                        workSheet.Cells[3, 7].Value = "(2)Surcharge";
                        workSheet.Cells[3, 8].Value = "(3)DN";
                        workSheet.Cells[3, 9].Value = "(4)CN";
                        workSheet.Cells[3, 10].Value = "(5)OT";


                        int recordIndex = 4;
                        foreach (Rgl_Report_WithPayDocNo detail in data.ToList())
                        {
                            if (detail.STTWithVendor != 1)
                            {
                                workSheet.Cells[recordIndex, 2].Value = detail.PayDocCD;
                                workSheet.Cells[recordIndex, 3].Value = detail.PayDocNo;
                                workSheet.Cells[recordIndex, 4].Value = detail.Curr;
                                workSheet.Cells[recordIndex, 5].Value = detail.AmtMat;
                                workSheet.Cells[recordIndex, 6].Value = detail.Amt;
                                workSheet.Cells[recordIndex, 7].Value = detail.AmtSurcharge;
                            }
                            if (detail.STTWithVendor == 1)
                            {
                                workSheet.Cells[recordIndex, 1].Value = detail.VendorCD;
                                workSheet.Cells[recordIndex, 1, recordIndex + detail.CountVendor - 1, 1].Merge = true;
                                workSheet.Cells[recordIndex, 2].Value = detail.PayDocCD;
                                workSheet.Cells[recordIndex, 3].Value = detail.PayDocNo;
                                workSheet.Cells[recordIndex, 4].Value = detail.Curr;
                                workSheet.Cells[recordIndex, 5].Value = detail.AmtMat;
                                workSheet.Cells[recordIndex, 6].Value = detail.Amt;
                                workSheet.Cells[recordIndex, 7].Value = detail.AmtSurcharge;
                                workSheet.Cells[recordIndex, 8].Value = detail.DN;
                                workSheet.Cells[recordIndex, 8, recordIndex + detail.CountVendor - 1, 8].Merge = true;
                                workSheet.Cells[recordIndex, 9].Value = detail.CN;
                                workSheet.Cells[recordIndex, 9, recordIndex + detail.CountVendor - 1, 9].Merge = true;
                                workSheet.Cells[recordIndex, 10].Value = detail.OT;
                                workSheet.Cells[recordIndex, 10, recordIndex + detail.CountVendor - 1, 10].Merge = true;
                            }
                            if (detail.CountVendor == detail.STTWithVendor)
                            {
                                recordIndex++;
                                workSheet.Cells[recordIndex, 1].Value = "SubTotal";
                                workSheet.Cells[recordIndex, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                workSheet.Cells[recordIndex, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                                workSheet.Cells[recordIndex, 2, recordIndex, 4].Merge = true;
                                workSheet.Cells[recordIndex, 2, recordIndex, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                workSheet.Cells[recordIndex, 2, recordIndex, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                                workSheet.Cells[recordIndex, 5].Value = detail.SumAmtMat_Vendor;
                                workSheet.Cells[recordIndex, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                workSheet.Cells[recordIndex, 5].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                                workSheet.Cells[recordIndex, 6].Value = detail.SumAmt_Vendor;
                                workSheet.Cells[recordIndex, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                workSheet.Cells[recordIndex, 6].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                                workSheet.Cells[recordIndex, 7].Value = detail.SumAmtSurcharge_Vendor;
                                workSheet.Cells[recordIndex, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                workSheet.Cells[recordIndex, 7].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                                workSheet.Cells[recordIndex, 8].Value = detail.DN;
                                workSheet.Cells[recordIndex, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                workSheet.Cells[recordIndex, 8].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                                workSheet.Cells[recordIndex, 9].Value = detail.CN;
                                workSheet.Cells[recordIndex, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                workSheet.Cells[recordIndex, 9].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                                workSheet.Cells[recordIndex, 10].Value = detail.OT;
                                workSheet.Cells[recordIndex, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                workSheet.Cells[recordIndex, 10].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                            }
                            recordIndex++;
                        }
                    }
                    return excel.GetAsByteArray();

                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<ERP_TTRglPayReq_Report_WithPayReqID>> ERP_TTRglPayReq_Report_WithPayReqID(ERP_TTRglPayReq_Report_WithPayReqID item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RefVKNo", item.RefVKNo);
                    queryParameters.Add("@RglPayReqID", item.RglPayReqID);
                    queryParameters.Add("@PayReqID", item.PayReqID);
                    queryParameters.Add("@Vendor", item.Vendor);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<ERP_TTRglPayReq_Report_WithPayReqID>("ERP_TTRglPayReq_Report_WithPayReqID", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TTRglPayReq_Report_WithPayReqID_Excel(ERP_TTRglPayReq_Report_WithPayReqID item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TTRglPayReq_Report_WithPayReqID", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefVKNo", SqlDbType.NVarChar).Value = item.RefVKNo;
            cmd.Parameters.Add("@RglPayReqID", SqlDbType.NVarChar).Value = item.RglPayReqID;
            cmd.Parameters.Add("@PayReqID", SqlDbType.NVarChar).Value = item.PayReqID;
            cmd.Parameters.Add("@Vendor", SqlDbType.NVarChar).Value = item.Vendor;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;


        }
        //public async Task<byte[]> ERP_TTRglPayReq_Report_WithPayReqID_Excel(ERP_TTRglPayReq_Report_WithPayReqID item)
        //{
        //    try
        //    {
        //        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
        //        {
        //            var queryParameters = new DynamicParameters();
        //            queryParameters.Add("@RefVKNo", item.RefVKNo);
        //            queryParameters.Add("@RglPayReqID", item.RglPayReqID);
        //            queryParameters.Add("@PayReqID", item.PayReqID);
        //            queryParameters.Add("@Vendor", item.Vendor);
        //            queryParameters.Add("@Type", item.Type);
        //            queryParameters.Add("@PageNumber", item.PageNumber);
        //            queryParameters.Add("@PageSize", item.PageSize);
        //            connection.Open();
        //            var data = await connection.QueryAsync<ERP_TTRglPayReq_Report_WithPayReqID>("ERP_TTRglPayReq_Report_WithPayReqID", queryParameters, commandTimeout: 1500
        //                , commandType: CommandType.StoredProcedure);


        //            ExcelPackage excel = new ExcelPackage();
        //            var workSheet = excel.Workbook.Worksheets.Add("RglPayReqVendor");
        //            if (data.Any())
        //            {
        //                workSheet.Cells[1, 1, 1, 25].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                workSheet.Cells[1, 1, 1, 25].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#40a9ff"));
        //                workSheet.Cells[1, 1, 1, 25].Style.Font.Bold = true;
        //                workSheet.Cells[1, 1].Value = "RglPayReqID";
        //                workSheet.Cells[1, 2].Value = "RglDate";
        //                //workSheet.Cells[1, 3].Value = "ConfirmedDate";
        //                workSheet.Cells[1, 3].Value = "PayReqID";
        //                workSheet.Cells[1, 4].Value = "PayReqDateConvert";
        //                workSheet.Cells[1, 5].Value = "PayDueDateConvert";
        //                workSheet.Cells[1, 6].Value = "PayType";
        //                workSheet.Cells[1, 7].Value = "PayDocCD";
        //                workSheet.Cells[1, 8].Value = "PayDocNo";
        //                workSheet.Cells[1, 9].Value = "ProdOrder";
        //                workSheet.Cells[1, 10].Value = "MatPOCD";
        //                workSheet.Cells[1, 11].Value = "Vendor";
        //                workSheet.Cells[1, 12].Value = "Shipper";
        //                workSheet.Cells[1, 13].Value = "MatCD";
        //                workSheet.Cells[1, 14].Value = "MatDesc";
        //                workSheet.Cells[1, 15].Value = "Color";
        //                workSheet.Cells[1, 16].Value = "MatPOItemID";
        //                workSheet.Cells[1, 17].Value = "Grade";
        //                workSheet.Cells[1, 18].Value = "PUnit";
        //                workSheet.Cells[1, 19].Value = "Qty";
        //                workSheet.Cells[1, 20].Value = "Price";
        //                workSheet.Cells[1, 21].Value = "Amount";
        //                workSheet.Cells[1, 22].Value = "Curr";
        //                workSheet.Cells[1, 23].Value = "ETDConvert";
        //                workSheet.Cells[1, 24].Value = "ETAConvert";
        //                workSheet.Cells[1, 25].Value = "Remark";

        //                int recordIndex = 2;
        //                foreach (ERP_TTRglPayReq_Report_WithPayReqID detail in data.ToList())
        //                {

        //                    workSheet.Cells[recordIndex, 1].Value = detail.RglPayReqID;
        //                    workSheet.Cells[recordIndex, 2].Value = detail.RglDateConvert;
        //                    //workSheet.Cells[recordIndex, 3].Value = detail.ConfirmedOnConvert;
        //                    workSheet.Cells[recordIndex, 3].Value = detail.PayReqID;
        //                    workSheet.Cells[recordIndex, 4].Value = detail.PayReqDateConvert;
        //                    workSheet.Cells[recordIndex, 5].Value = detail.PayDueDateConvert;
        //                    workSheet.Cells[recordIndex, 6].Value = detail.PayType;
        //                    workSheet.Cells[recordIndex, 7].Value = detail.PayDocCD;
        //                    workSheet.Cells[recordIndex, 8].Value = detail.PayDocNo;
        //                    workSheet.Cells[recordIndex, 9].Value = detail.RefVKNo;
        //                    workSheet.Cells[recordIndex, 10].Value = detail.MatPOCD;
        //                    workSheet.Cells[recordIndex, 11].Value = detail.Vendor;
        //                    workSheet.Cells[recordIndex, 12].Value = detail.Shipper;
        //                    workSheet.Cells[recordIndex, 13].Value = detail.MatCD;
        //                    workSheet.Cells[recordIndex, 14].Value = detail.MatDesc;
        //                    workSheet.Cells[recordIndex, 15].Value = detail.Color;
        //                    workSheet.Cells[recordIndex, 16].Value = detail.MatPOItemID;
        //                    workSheet.Cells[recordIndex, 17].Value = detail.Grade;
        //                    workSheet.Cells[recordIndex, 18].Value = detail.PUnit;
        //                    workSheet.Cells[recordIndex, 19].Value = detail.Qty;
        //                    workSheet.Cells[recordIndex, 20].Value = detail.Price;
        //                    workSheet.Cells[recordIndex, 21].Value = detail.Amountstring;
        //                    workSheet.Cells[recordIndex, 22].Value = detail.Curr;
        //                    workSheet.Cells[recordIndex, 23].Value = detail.ETDConvert;
        //                    workSheet.Cells[recordIndex, 24].Value = detail.ETAConvert;
        //                    workSheet.Cells[recordIndex, 25].Value = detail.Remark;
        //                    recordIndex++;              
        //                }
        //            }
        //            return excel.GetAsByteArray();

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new NotImplementedException(ex.Message.ToString());
        //    }
        //}

        public async Task<IList<RglPayReq_DocGrp>> ERP_TTRglPayReq_DocGrp_GetVendor(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    string Query = "SELECT b.VendorCD FROM dbo.TTRglPayReq_List a (NOLOCK) " +
                        "INNER JOIN STPayReq b (NOLOCK) ON a.PayReqID=b.PayReqID " +
                        "INNER JOIN dbo.STPayReq_List c (NOLOCK) ON a.PayReqID = c.PayReqID "+
                        "WHERE a.RglPayReqID='" + RglPayReqID + "' " +
                        "GROUP BY b.VendorCD";
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<RglPayReq_DocGrp>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<RglPayReq_DocGrp>> ERP_TTRglPayReq_DocGrp_GetPayReqID(RglPayReq_DocGrp item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RglPayReqID", item.RglPayReqID);
                    queryParameters.Add("@VendorCD", item.VendorCD);
                    connection.Open();
                    var data = await connection.QueryAsync<RglPayReq_DocGrp>("ERP_TTRglPayReq_DocGrp_GetPayReqID", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp_GetVendorCD(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT VendorCD FROM dbo.TTRglPayReqDCN (NOLOCK) WHERE RglPayReqID='" + RglPayReqID + "' GROUP BY VendorCD";
                    connection.Open();
                    var data = await connection.QueryAsync<RglPayReq_VndGrp>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp_PayDocCD(string RglPayReqID,string VendorCD)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT b.PayDocCD FROM dbo.TTRglPayReqDCN a (NOLOCK) " +
                        "INNER JOIN dbo.TTDCN b (NOLOCK) ON a.DCNID=b.DCNID " +
                        "WHERE a.RglPayReqID='"+ RglPayReqID + "' AND b.VendorCD='"+ VendorCD + "' " +
                        "GROUP BY b.PayDocCD";
                    connection.Open();
                    var data = await connection.QueryAsync<RglPayReq_VndGrp>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp_GetPayDocNo(string RglPayReqID, string VendorCD,string PayDocCD)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT b.PayDocNo FROM dbo.TTRglPayReqDCN a (NOLOCK) " +
                        "INNER JOIN dbo.TTDCN b (NOLOCK) ON a.DCNID=b.DCNID " +
                        "WHERE a.RglPayReqID='"+ RglPayReqID + "' AND b.VendorCD='"+ VendorCD + "' AND b.PayDocCD='" + PayDocCD + "' " +
                        "GROUP BY b.PayDocNo";
                    connection.Open();
                    var data = await connection.QueryAsync<RglPayReq_VndGrp>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_PayReq_GetPayDocNo(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT b.VendorCD FROM dbo.TTRglPayReq_List a (NOLOCK) INNER JOIN dbo.STPayReq b (NOLOCK) ON a.PayReqID=b.PayReqID WHERE a.RglPayReqID='"+ RglPayReqID + "' GROUP BY b.VendorCD";
                    connection.Open();
                    var data = await connection.QueryAsync<RglPayReq_VndGrp>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TTRglPayReq_List>> ERP_TTRglPayReq_PayReq_PayReqID(TTRglPayReq_List_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT a.PayReqID FROM dbo.TTRglPayReq_List a (NOLOCK) INNER JOIN dbo.STPayReq b (NOLOCK) ON a.PayReqID=b.PayReqID WHERE a.RglPayReqID='" + item.RglPayReqID + "' " +
                        "AND (b.VendorCD='"+ item.VendorCD + "' or '" + item.VendorCD + "'='')";
                    connection.Open();
                    var data = await connection.QueryAsync<TTRglPayReq_List>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_TTRglPayReq_List_DeleteFilePath(TTRglPayReq_List_Custom PayReq)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE dbo.TTRglPayReq_List SET FilePath=NULL WHERE PayReqID='" + PayReq.PayReqID + "' AND RglPayReqID='" + PayReq.RglPayReqID + "'";
                    connection.Open();
                    var data = await connection.QueryAsync<STPayReq>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_TTRglPayReq_List_UpdateFilePath(TTRglPayReq_List_Custom PayReq)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "SELECT PayReqID FROM dbo.TTRglPayReq_List (NOLOCK) WHERE PayReqID='" + PayReq.PayReqID + "' AND RglPayReqID='" + PayReq.RglPayReqID + "' AND ISNULL(FilePath,'')<>'' ";
                    connection.Open();
                    var data = await connection.QueryAsync<STPayReq>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    if (data.ToList().Any())
                    {
                        throw new NotImplementedException("FilePath is exist");
                    }
                    queryParameters.Add("@RglPayReqID", PayReq.RglPayReqID);
                    queryParameters.Add("@PayReqID", PayReq.PayReqID);
                    queryParameters.Add("@FilePath", PayReq.FilePath);
                    queryParameters.Add("UpdatedBy", GetUserlogin());
                    //string query2 = "SELECT PayReqID FROM dbo.TTRglPayReq_List  (NOLOCK) " +
                    //    " WHERE RglPayReqID='" + PayReq.RglPayReqID + "' AND FilePath='" + PayReq.FilePath + "'";
                    var data2 = await connection.QueryAsync<TTRglPayReq_List>("ERP_TTRglPayReq_List_CheckDoubleFilePath"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    if (data2.Count() >0 )
                    {
                        if (data2.FirstOrDefault().RglPayReqListID > 1)
                        {
                            throw new NotImplementedException("FilePath is exist in RglPayReqID " + PayReq.RglPayReqID + "");
                        }

                    }
                    //string update = "UPDATE dbo.TTRglPayReq_List SET FilePath=N'" + PayReq.FilePath + "',UpdatedBy='" + GetUserlogin() + "',UpdatedOn=GETDATE() " +
                    //    "WHERE PayReqID='" + PayReq.PayReqID + "' AND RglPayReqID='" + PayReq.RglPayReqID + "'";
                    //var dt = await connection.QueryAsync<STPayReq>(update
                    //    , queryParameters, commandTimeout: 1500
                    //    , commandType: CommandType.Text);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Result> ERP_TTRglPayReq_CheckPreviewRglPayReq(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RglPayReqID", RglPayReqID);
                    connection.Open();
                    var data = await connection.QueryAsync<Result>("ERP_TTRglPayReq_CheckPreviewRglPayReq", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    if (data.Count()>0)
                    {
                        throw new NotImplementedException(data.FirstOrDefault().ErrorMessage);
                    }
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TVendor_Custom>> ERP_TTRglPayReq_List_GetVendorBanInfor(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RglPayReqID", RglPayReqID);
                    connection.Open();
                    var data = await connection.QueryAsync<TVendor_Custom>("ERP_TTRglPayReq_List_GetVendorBanInfor", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<Boolean> ERP_TTRglPayReq_UpdateConfirmedOn(TTRglPayReq_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "";

                    if (item.IsconfirmCEO)//CEO
                    {
                        query = "UPDATE dbo.TTRglPayReq SET ConfirmedCEOOn=GETDATE(),UpdatedOn=GETDATE(),UpdatedBy=" + GetUserlogin() + " WHERE RglPayReqID='" + item.RglPayReqID + "'";
                    }
                    if (!item.IsconfirmCEO)//Manager
                    {
                        await ERP_TTRglPayReq_CheckPreviewRglPayReq(item.RglPayReqID);
                        query = "UPDATE dbo.TTRglPayReq SET ConfirmedManagerOn=GETDATE(),UpdatedOn=GETDATE(),UpdatedBy=" + GetUserlogin() + " WHERE RglPayReqID='" + item.RglPayReqID + "'";
                    }
                    connection.Open();
                    var data = await connection.QueryAsync<TTRglPayReq>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return await Task.FromResult(true);

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
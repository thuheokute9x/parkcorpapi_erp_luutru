﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.RGL.DN
{
    public class TTDCN_Repository : GenericRepository<TTDCN>, ITTDCN_Repository
    {
        public TTDCN_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TTDCN_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TTDCN>> ERP_TTDCN_GetDCNNo(string PayDocNo)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT TOP (15) PayDocNo  FROM dbo.TTDCN (NOLOCK) WHERE PayDocNo LIKE N'%" + PayDocNo + "%' OR '" + PayDocNo + "'=''";
                    var data = await connection.QueryAsync<TTDCN>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<TTDCN_Custom>> ERP_TTDCN_GetPageSize(TTDCN_Custom item)
        {
            try
            {

                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@VendorCD", item.VendorCD);
                    queryParameters.Add("@PayDocNo", item.PayDocNo);
                    if (item.ClosedOnString!="None")
                    {
                        if (item.ClosedOnString == "False")
                        {
                            queryParameters.Add("@ClosedOn", false);
                        }
                        if (item.ClosedOnString == "True")
                        {
                            queryParameters.Add("@ClosedOn", true);
                        }
                    }
                    else
                    {
                        queryParameters.Add("@ClosedOn", 3);
                    }
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    connection.Open();
                    var data = await connection.QueryAsync<TTDCN_Custom>("ERP_TTDCN_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.RGL.DN
{
    interface ITTDCN_Repository : IGenericRepository<TTDCN>
    {
        Task<IList<TTDCN_Custom>> ERP_TTDCN_GetPageSize(TTDCN_Custom item);
        Task<IList<TTDCN>> ERP_TTDCN_GetDCNNo(string DNNo);
    }
}

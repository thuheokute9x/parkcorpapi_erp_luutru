﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.RGL.RglPayReqDN
{
    public class TTRglPayReqDN_Repository : GenericRepository<TTRglPayReqDCN>, ITTRglPayReqDN_Repository
    {
        public TTRglPayReqDN_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TTRglPayReqDN_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TTRglPayReqDCN_Custom>> ERP_TTRglPayReqDCN_ByDCNID(int DCNID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT RglDCNID,RglPayReqID,VendorCD,DCNID,DCNAmount,CreatedBy" +
                    ",CreatedOn,UpdatedBy,UpdatedOn  FROM dbo.TTRglPayReqDCN (NOLOCK) WHERE DCNID='" + DCNID + "'";
                    var data = await connection.QueryAsync<TTRglPayReqDCN_Custom>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<TTRglPayReqDCN_Custom>> ERP_TTRglPayReqDCN_ByRglPayReqIDVendor(TTRglPayReqDCN_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RglPayReqID", item.RglPayReqID);
                    queryParameters.Add("@VendorCD", item.VendorCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TTRglPayReqDCN_Custom>("ERP_TTRglPayReqDCN_ByRglPayReqIDVendor", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TTRglPayReqDCN_Custom>> ERP_TTRglPayReqDCN_CalculatorBalance(TTRglPayReqDCN item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RglPayReqID", item.RglPayReqID);
                    queryParameters.Add("@VendorCD", item.VendorCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TTRglPayReqDCN_Custom>("ERP_TTRglPayReqDCN_CalculatorBalance", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<Boolean> ERP_TTRglPayReqDCN_InsertList(List<TTRglPayReqDCN> item,List<TTDCN> tTDCNList)
        {
            try
            {
                var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
                string createdbystring = claims?.FirstOrDefault()?.Subject?.Name;
                int user = 0;
                if (!string.IsNullOrEmpty(createdbystring))
                {
                    user = int.Parse(createdbystring);
                }
                //using (PARKERPEntities context = new PARKERPEntities())
                //{
                //    using (var transaction = context.Database.BeginTransaction())
                //    {
                //        try
                //        {
                //            foreach (TTDCN ob in tTDCNList)
                //            {
                //                TTDCN a = new TTDCN();
                //                a.DCNID = ob.DCNID;
                //                a.ClosedOn = ob.ClosedOn;
                //                a.UpdatedBy = user;
                //                a.UpdatedOn = DateTime.Now;
                //                Context.Entry(a).State = System.Data.Entity.EntityState.Modified;
                //                Context.Entry(a).Property(x => x.CreatedBy).IsModified = false;
                //                Context.Entry(a).Property(x => x.CreatedOn).IsModified = false;
                //                Context.Entry(a).Property(x => x.CompanyCD).IsModified = false;
                //                Context.Entry(a).Property(x => x.PayDocCD).IsModified = false;
                //                Context.Entry(a).Property(x => x.PayDocNo).IsModified = false;
                //                Context.Entry(a).Property(x => x.VendorCD).IsModified = false;
                //                Context.Entry(a).Property(x => x.Amount).IsModified = false;
                //                await context.SaveChangesAsync();
                //            }


                //            await this.BulkInsert(item);

                //            transaction.Commit();
                //        }
                //        catch (Exception ex)
                //        {                         
                //            transaction.Rollback();
                //            throw new NotImplementedException(ex.Message.ToString());
                //        }
                //    }
                //}
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    connection.Open();
                    foreach (TTDCN a in tTDCNList)
                    {
                        var queryParameters = new DynamicParameters();
                        var data = await connection.QueryAsync<TTRglPayReqDCN_Custom>(
                            "UPDATE TTDCN SET UpdatedBy=" + user + ",UpdatedOn=GETDATE(),ClosedOn=1 WHERE DCNID=" + a.DCNID + ""
                            , queryParameters, commandTimeout: 1500
                            , commandType: CommandType.Text);
                    }


                }
                await this.BulkInsert(item);
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
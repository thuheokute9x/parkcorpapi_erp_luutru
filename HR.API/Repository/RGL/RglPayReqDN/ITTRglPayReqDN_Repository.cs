﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.RGL.RglPayReqDN
{
    interface ITTRglPayReqDN_Repository : IGenericRepository<TTRglPayReqDCN>
    {
        Task<IList<TTRglPayReqDCN_Custom>> ERP_TTRglPayReqDCN_ByDCNID(int item); 
        Task<IList<TTRglPayReqDCN_Custom>> ERP_TTRglPayReqDCN_ByRglPayReqIDVendor(TTRglPayReqDCN_Custom item);
        Task<IList<TTRglPayReqDCN_Custom>> ERP_TTRglPayReqDCN_CalculatorBalance(TTRglPayReqDCN item);
        Task<Boolean> ERP_TTRglPayReqDCN_InsertList(List<TTRglPayReqDCN> item,List<TTDCN> tTDCNList);
    }
}

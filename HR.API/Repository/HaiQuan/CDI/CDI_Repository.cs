﻿using Dapper;

using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.HaiQuan.CDI
{
    public class CDI_Repository : GenericRepository<TTCDI>, ICDI_Repository
    {
        public CDI_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public CDI_Repository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<IList<TTCDI>> ERP_TTCDI_GetCDICDLike(TTCDI item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@CDICD", item.CDICD);
                    connection.Open();
                    var data = await connection.QueryAsync<TTCDI>("SELECT TOP 15 CDICD FROM dbo.TTCDI (NOLOCK) WHERE CDICD LIKE N'%'+@CDICD+'%' OR  @CDICD=''"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<TTCDI>> ERP_TTCDI_GetCDNoLike(TTCDI item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@CDINo", item.CDINo);
                    connection.Open();
                    var data = await connection.QueryAsync<TTCDI>("SELECT TOP 15 CDINo FROM dbo.TTCDI (NOLOCK) WHERE ISNULL(CDINo,'')<>'' AND (CDINo LIKE N'%'+@CDINo+'%' OR  @CDINo='')"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TTCDI_GetPageSize(TTCDI_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@CDICD", item.CDICD);
                    queryParameters.Add("@CDINo", item.CDINo);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TTCDI_GetPageSize"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TTCDIIV_GetByCDICD(TTCDIIV item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@CDICD", item.CDICD);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("SELECT * FROM dbo.TTCDIIV (NOLOCK) WHERE CDICD=@CDICD ORDER BY CreatedOn DESC"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TTCDIIVM_InsertFromPO(TMatPOItem_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RefVKNo", item.RefVKNo);
                    queryParameters.Add("@PGrpCD", item.PGrpCD);
                    queryParameters.Add("@MatPOCD", item.MatPOCD);
                    queryParameters.Add("@Vendor", item.Vendor);
                    queryParameters.Add("@Shipper", item.Shipper);
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@MatDesc", item.MatDesc);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);

                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TTCDIIVM_InsertFromPO"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TTCDIIVM_InsertFromGR(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RefVKNo", item.RefVKNo.Value);
                    queryParameters.Add("@PGrpCD", item.PGrpCD.Value);
                    queryParameters.Add("@IV", item.IV.Value);
                    queryParameters.Add("@Vendor", item.Vendor.Value);


                    queryParameters.Add("@Shipper", item.Shipper.Value);
                    queryParameters.Add("@MatCD", item.MatCD.Value);
                    queryParameters.Add("@MatDesc", item.MatDesc.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);

                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TTCDIIVM_InsertFromGR"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TTCDIIVM_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@CDICD", item.CDICD.Value);
                    queryParameters.Add("@CDIIVID", item.CDIIVID.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TTCDIIVM_GetPageSize"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public DataTable ERP_TTCDIIVM_GetPageSizeExcel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TTCDIIVM_GetPageSize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CDICD", SqlDbType.NVarChar).Value = item.CDICD.Value;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
                
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TTCDCustomsCD>> ERP_TTCDCustomsCD_GetByCustomsCDLike(TTCDCustomsCD item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@CustomsCD", item.CustomsCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TTCDCustomsCD>("SELECT TOP 15 CustomsCD,DescCustomsCD FROM dbo.TTCDCustomsCD (NOLOCK) WHERE CustomsCD LIKE N'%'+@CustomsCD+'%' OR  @CustomsCD=''"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<TTCDIIV>> ERP_TTCDIIV_GetByIVNoLike(TTCDIIV item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@IVNo", item.IVNo);
                    connection.Open();
                    var data = await connection.QueryAsync<TTCDIIV>("SELECT DISTINCT TOP 15 IVNo FROM dbo.TTCDIIV (NOLOCK) WHERE IVNo LIKE N'%'+@IVNo+'%' OR  @IVNo=''"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<TTCDCustomsCD> ERP_TTCDCustomsCD_GetByCustomsCD(TTCDCustomsCD item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@CustomsCD", item.CustomsCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TTCDCustomsCD>("SELECT * FROM dbo.TTCDCustomsCD (NOLOCK) WHERE CustomsCD=@CustomsCD"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TTCDIIVM_Report(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    if (item.MonthCDI.Value != "")
                    {
                        queryParameters.Add("@MonthCDI", item.MonthCDI.Value);
                    }
                    queryParameters.Add("@CDINo", item.CDINo.Value);
                    queryParameters.Add("@IVNo", item.IVNo.Value);
                    queryParameters.Add("@VendorCD", item.VendorCD.Value);
                    queryParameters.Add("@CustomsCD", item.CustomsCD.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TTCDIIVM_Report"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public DataTable ERP_TTCDIIVM_Report_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TTCDIIVM_Report", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                if (item.MonthCDI.Value != "")
                {
                    cmd.Parameters.Add("@MonthCDI", SqlDbType.DateTime).Value = item.MonthCDI.Value;
                }
                cmd.Parameters.Add("@CDINo", SqlDbType.NVarChar).Value = item.CDINo.Value;
                cmd.Parameters.Add("@IVNo", SqlDbType.NVarChar).Value = item.IVNo.Value;
                cmd.Parameters.Add("@VendorCD", SqlDbType.NVarChar).Value = item.VendorCD.Value;
                cmd.Parameters.Add("@CustomsCD", SqlDbType.NVarChar).Value = item.CustomsCD.Value;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

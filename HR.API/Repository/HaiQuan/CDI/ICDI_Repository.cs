﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.HaiQuan.CDI
{
    interface ICDI_Repository : IGenericRepository<TTCDI>
    {
        Task<IList<TTCDI>> ERP_TTCDI_GetCDICDLike(TTCDI item);
        Task<IList<TTCDI>> ERP_TTCDI_GetCDNoLike(TTCDI item);
        Task<IList<Object>> ERP_TTCDI_GetPageSize(TTCDI_Custom item);
        Task<IList<Object>> ERP_TTCDIIV_GetByCDICD(TTCDIIV item);
        Task<IList<Object>> ERP_TTCDIIVM_InsertFromPO(TMatPOItem_Custom item);
        Task<IList<Object>> ERP_TTCDIIVM_InsertFromGR(dynamic item);
        Task<IList<Object>> ERP_TTCDIIVM_GetPageSize(dynamic item);
        DataTable ERP_TTCDIIVM_GetPageSizeExcel(dynamic item);
        Task<IList<TTCDCustomsCD>> ERP_TTCDCustomsCD_GetByCustomsCDLike(TTCDCustomsCD item);
        Task<TTCDCustomsCD> ERP_TTCDCustomsCD_GetByCustomsCD(TTCDCustomsCD item);
        Task<IList<TTCDIIV>> ERP_TTCDIIV_GetByIVNoLike(TTCDIIV item);
        Task<IList<Object>> ERP_TTCDIIVM_Report(dynamic item);
        DataTable ERP_TTCDIIVM_Report_Excel(dynamic item);
    }
}

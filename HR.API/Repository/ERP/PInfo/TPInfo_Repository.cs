﻿using AutoMapper.Configuration;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.IRepository;
using HR.API.IUnitOfWork;
using System.Data;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models;

namespace HR.API.Repository.ERP.PInfo
{
    public class TPInfo_Repository : GenericRepository<TPInfo>, ITPInfo_Repository
    {
        public TPInfo_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TPInfo_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TPInfo_Custom>> ERP_TPInfo_GetPageSize(TPInfo_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@BasicDate", item.BasicDate);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@IncDiscontinued", item.IncDiscontinued);
                    queryParameters.Add("@MfgVendorDiscontinued", item.MfgVendorDiscontinued);
                    queryParameters.Add("@ShipperDiscontinued", item.ShipperDiscontinued);

                    queryParameters.Add("@FirstMatGrpID", item.FirstMatGrpID==0?"": item.FirstMatGrpID.ToString());
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@MfgVendor", item.MfgVendor);
                    queryParameters.Add("@Shipper", item.Shipper);
                    queryParameters.Add("@MHID", item.MHID == 0 ? "" : item.MHID.ToString());
                    queryParameters.Add("@CustItemNo1", item.CustItemNo1);
                    queryParameters.Add("@MatDesc", item.MatDesc);

                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TPInfo_Custom>("ERP_TPInfo_GetPageSize"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TPInfo_Custom>> ERP_TPInfo_GetByMatCD_MfgVendor_Shipper(TPInfo_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@MfgVendor", item.MfgVendor);
                    queryParameters.Add("@Shipper", item.Shipper);
                    connection.Open();
                    var data = await connection.QueryAsync<TPInfo_Custom>("ERP_TPInfo_GetByMatCD_MfgVendor_Shipper"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<bool> ERP_TPInfo_Update(TPInfo item)
        {
            try
            {
                if (item.ValidFrom > DateTime.Today)
                {
                    throw new NotImplementedException("ValidFrom larger than the current date");
                }
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PInfoID", item.PInfoID);
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@MfgVendor", item.MfgVendor);
                    queryParameters.Add("@Shipper", item.Shipper);
                    queryParameters.Add("@ValidFrom", item.ValidFrom);
                    connection.Open();
                    var data = await connection.QueryAsync<Result>("ERP_TPInfo_CheckUpdate"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    if (data.ToList().Count > 0)
                    {
                        var dt = data.FirstOrDefault().ErrorMessage;
                        throw new NotImplementedException(dt);
                    }
                }
                await this.Update_SaveChange(item);
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<TPInfo> ERP_TPInfo_Insert(TPInfo item)
        {
            try
            {
                if (item.ValidFrom > DateTime.Today)
                {
                    throw new NotImplementedException("ValidFrom larger than the current date");
                }
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@MfgVendor", item.MfgVendor);
                    queryParameters.Add("@Shipper", item.Shipper);
                    queryParameters.Add("@ValidFrom", item.ValidFrom);
                    connection.Open();
                    var data = await connection.QueryAsync<Result>("ERP_TPInfo_CheckInsert"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    if (data.ToList().Count > 0)
                    {
                        var dt = data.FirstOrDefault().ErrorMessage;
                        throw new NotImplementedException(dt);
                    }
                }
                return await this.Insert_ReturnOBJ(item);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<Result> ERP_TPInfo_CheckUpdate_Discontinue(TPInfo item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    queryParameters.Add("@PInfoID", item.PInfoID);
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@MfgVendor", item.MfgVendor);
                    queryParameters.Add("@Shipper", item.Shipper);
                    var data = await connection.QueryAsync<Result>("ERP_TPInfo_CheckUpdate_Discontinue"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TPInfo_GetPUnitByMatCD(TPInfo item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@MfgVendor", item.MfgVendor);
                    var data = await connection.QueryAsync<Object>("SELECT PUnit FROM dbo.TPInfo (NOLOCK) WHERE MatCD=@MatCD AND MfgVendor=@MfgVendor AND Discontinue=0  GROUP BY PUnit"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
﻿using HR.API.IRepository;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.PInfo
{
    interface ITPInfo_Repository : IGenericRepository<TPInfo> 
    {
        Task<IList<TPInfo_Custom>> ERP_TPInfo_GetPageSize(TPInfo_Custom item);
        Task<IList<TPInfo_Custom>> ERP_TPInfo_GetByMatCD_MfgVendor_Shipper(TPInfo_Custom item);
        Task<Boolean> ERP_TPInfo_Update(TPInfo item);
        Task<TPInfo> ERP_TPInfo_Insert(TPInfo item);
        Task<Result> ERP_TPInfo_CheckUpdate_Discontinue(TPInfo item);
        Task<IList<Object>> ERP_TPInfo_GetPUnitByMatCD(TPInfo item);
    }
}

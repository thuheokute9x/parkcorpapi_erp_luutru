﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.BOM
{
    interface ITBOM_Repository : IGenericRepository<TBOM>
    {
        Task<IList<Object>> ERP_TBOM_GetVersionByProdID(TBOM item);
        Task<IList<Object>> ERP_TBOM_GetPageSize(dynamic item);
        DataTable ERP_TBOM_GetPageSize_Excel(dynamic item);
        Task<IList<Object>> ERP_TBOMItem_GetPageSize(dynamic item);
        DataTable ERP_TBOMItem_GetPageSize_Excel(dynamic item);
        Task<IList<Object>> ERP_TBOM_SearchingProductionByMatCD_GetPageSize(dynamic item);
        DataTable ERP_TBOM_SearchingProductionByMatCD_GetPageSize_Excel(dynamic item);
    }
}

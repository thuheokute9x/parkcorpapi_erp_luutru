﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.BOM
{
    public class TBOM_Repository : GenericRepository<TBOM>, ITBOM_Repository
    {
        public TBOM_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TBOM_Repository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<Object>> ERP_TBOM_GetVersionByProdID(TBOM item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    //string query = "SELECT DISTINCT a.Version FROM dbo.TProdInstItem a (NOLOCK) INNER JOIN dbo.TProd b (NOLOCK) ON b.ProdID = a.ProdID WHERE b.ProdID=@ProdID AND a.Version<>@Version ORDER BY a.Version ASC";
                    string query = "SELECT DISTINCT CONVERT(NVARCHAR,Version) Version,Version VersionINT,BOMID  FROM dbo.TBOM (NOLOCK) WHERE ProdID=@ProdID  ORDER BY VersionINT ASC";
                    queryParameters.Add("@ProdID", item.ProdID);
                    queryParameters.Add("@Version", item.Version);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TBOM_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@BOMID", item.BOMID.Value);
                    queryParameters.Add("@StyleDesc", item.StyleDesc.Value);
                    queryParameters.Add("@ProdCode", item.ProdCode.Value);
                    queryParameters.Add("@SKUCode", item.SKUCode.Value);
                    queryParameters.Add("@Color", item.Color.Value);
                    queryParameters.Add("@MatGrpCode", item.MatGrpCode.Value);
                    queryParameters.Add("@Version", item.Version.Value);
                    queryParameters.Add("@PatternMaker", item.PatternMaker.Value);
                    queryParameters.Add("@BOMMaker", item.BOMMaker.Value);

                    queryParameters.Add("@MaterialStaff", item.MaterialStaff.Value);
                    queryParameters.Add("@SampleMaker", item.SampleMaker.Value);
                    queryParameters.Add("@PatternFileName", item.PatternFileName.Value);
                    if (item.PatternCreatedOnFrom.Value!= "null")
                    {
                        queryParameters.Add("@PatternCreatedOnFrom", item.PatternCreatedOnFrom.Value);

                    }
                    if (item.PatternCreatedOnTo.Value != "null")
                    {
                        queryParameters.Add("@PatternCreatedOnTo", item.PatternCreatedOnTo.Value);
                    }
                    queryParameters.Add("@RefVKNo", item.RefVKNo.Value);
                    queryParameters.Add("@BOMRemark", item.BOMRemark.Value);


                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TBOM_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TBOM_GetPageSize_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TBOM_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;



            cmd.Parameters.Add("@StyleDesc", SqlDbType.NVarChar).Value = item.StyleDesc.Value;
            cmd.Parameters.Add("@ProdCode", SqlDbType.NVarChar).Value = item.ProdCode.Value;
            cmd.Parameters.Add("@SKUCode", SqlDbType.NVarChar).Value = item.SKUCode.Value;
            cmd.Parameters.Add("@MatGrpCode", SqlDbType.NVarChar).Value = item.MatGrpCode.Value;
            cmd.Parameters.Add("@Version", SqlDbType.Int).Value = item.Version.Value;
            cmd.Parameters.Add("@PatternMaker", SqlDbType.NVarChar).Value = item.PatternMaker.Value;
            cmd.Parameters.Add("@BOMMaker", SqlDbType.NVarChar).Value = item.BOMMaker.Value;
            cmd.Parameters.Add("@MaterialStaff", SqlDbType.NVarChar).Value = item.MaterialStaff.Value;
            cmd.Parameters.Add("@SampleMaker", SqlDbType.NVarChar).Value = item.SampleMaker.Value;


            if (item.PatternCreatedOnFrom.Value != "null")
            {
                cmd.Parameters.Add("@PatternCreatedOnFrom", SqlDbType.DateTime).Value = item.PatternCreatedOnFrom.Value;
            }
            if (item.PatternCreatedOnTo.Value != "null")
            {
                cmd.Parameters.Add("@PatternCreatedOnTo", SqlDbType.DateTime).Value = item.PatternCreatedOnTo.Value;
            }        
            cmd.Parameters.Add("@RefVKNo", SqlDbType.NVarChar).Value = item.RefVKNo.Value;
            cmd.Parameters.Add("@BOMRemark", SqlDbType.NVarChar).Value = item.BOMRemark.Value;



            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }
        public async Task<IList<Object>> ERP_TBOMItem_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@BOMID", item.BOMID.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TBOMItem_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TBOMItem_GetPageSize_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TBOMItem_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@BOMID", SqlDbType.NVarChar).Value = item.BOMID.Value;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }
        public async Task<IList<Object>> ERP_TBOM_SearchingProductionByMatCD_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCD", item.MatCD.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TBOM_SearchingProductionByMatCD_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TBOM_SearchingProductionByMatCD_GetPageSize_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TBOM_SearchingProductionByMatCD_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD.Value;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }
    }
}
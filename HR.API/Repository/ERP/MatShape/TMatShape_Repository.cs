﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.MatShape
{
    public class TMatShape_Repository : GenericRepository<TMatShape>, ITMatShape_Repository
    {
        public TMatShape_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TMatShape_Repository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<TMatShape>> ERP_TMatShape_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT MatShapeID,MatShape,Dimension,ShapeDesc,EnablePattern,EnableRndPress" +
                    ",Seq,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn FROM TMatShape (NOLOCK)";
                    connection.Open();
                    var data = await connection.QueryAsync<TMatShape>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
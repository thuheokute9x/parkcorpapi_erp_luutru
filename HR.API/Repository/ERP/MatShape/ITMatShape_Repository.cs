﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.MatShape
{
    interface ITMatShape_Repository : IGenericRepository<TMatShape>
    {
        Task<IList<TMatShape>> ERP_TMatShape_GetAll();
    }
}

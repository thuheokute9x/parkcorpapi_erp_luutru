﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.In_ItemLeather
{
    interface ISTIn_ItemLeather_Repository : IGenericRepository<STIn_ItemLeather>
    {
        Task<IList<Object>> ERP_Report_STIn_ItemLeather_SFLeatherGI(dynamic item);
        DataTable ERP_Report_STIn_ItemLeather_SFLeatherGI_Excel(dynamic item);
        Task<IList<Object>> ERP_Report_STIn_ItemLeather_SFLeather(dynamic item);
        DataTable ERP_Report_STIn_ItemLeather_SFLeather_Excel(dynamic item);
        Task<IList<Object>> ERP_Report_STIn_ItemLeather_SFLeatherGR(dynamic item);
        DataTable ERP_Report_STIn_ItemLeather_SFLeatherGR_Excel(dynamic item);
        Task<IList<Object>> ERP_Report_STIn_ItemLeather_GetByIVListNo(dynamic item);
    }
}

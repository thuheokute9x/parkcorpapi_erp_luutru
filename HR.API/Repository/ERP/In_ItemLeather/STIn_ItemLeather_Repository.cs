﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.In_ItemLeather
{
    public class STIn_ItemLeather_Repository : GenericRepository<STIn_ItemLeather>, ISTIn_ItemLeather_Repository
    {
        public STIn_ItemLeather_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
: base(unitOfWork)
        {
        }
        public STIn_ItemLeather_Repository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<IList<Object>> ERP_Report_STIn_ItemLeather_SFLeatherGI(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();

                    if (item.From.Value != "null" && item.To.Value != "null")
                    {
                        DateTime fromdate;
                        fromdate = DateTime.ParseExact(item.From.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        DateTime todate;
                        todate = DateTime.ParseExact(item.To.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                        todate = new DateTime(todate.Year, todate.Month, todate.Day);
                        if (fromdate > todate)
                        {
                            throw new NotImplementedException("Please choose To bigger From");
                        }
                        queryParameters.Add("@From", fromdate);
                        queryParameters.Add("@To", todate);
                    }

                    queryParameters.Add("@ProdOrder", item.ProdOrder.Value);
                    queryParameters.Add("@OUTCD", item.OUTCD.Value);
                    queryParameters.Add("@PLANTCD", item.PLANTCD.Value);
                    queryParameters.Add("@MatCD", item.MatCD.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_Report_STIn_ItemLeather_SFLeatherGI"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_Report_STIn_ItemLeather_SFLeatherGI_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_Report_STIn_ItemLeather_SFLeatherGI", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;

                if (item.From.Value != "null" && item.To.Value != "null")
                {
                    DateTime fromdate;
                    fromdate = DateTime.ParseExact(item.From.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    DateTime todate;
                    todate = DateTime.ParseExact(item.To.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                    todate = new DateTime(todate.Year, todate.Month, todate.Day);
                    if (fromdate > todate)
                    {
                        throw new NotImplementedException("Please choose To bigger From");
                    }
                    cmd.Parameters.Add("@From", SqlDbType.DateTime).Value = fromdate;
                    cmd.Parameters.Add("@To", SqlDbType.DateTime).Value = todate;
                }




                cmd.Parameters.Add("@ProdOrder", SqlDbType.NVarChar).Value = item.ProdOrder.Value;
                cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD.Value;
                cmd.Parameters.Add("@OUTCD", SqlDbType.NVarChar).Value = item.OUTCD.Value;
                cmd.Parameters.Add("@PLANTCD", SqlDbType.NVarChar).Value = item.PLANTCD.Value;
                cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD.Value;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_Report_STIn_ItemLeather_SFLeather(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@LeatherType", item.LeatherType.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_Report_STIn_ItemLeather_SFLeather"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_Report_STIn_ItemLeather_SFLeather_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_Report_STIn_ItemLeather_SFLeather", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@LeatherType", SqlDbType.NVarChar).Value = item.LeatherType.Value;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_Report_STIn_ItemLeather_SFLeatherGR(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    if (item.From.Value != "null" && item.To.Value != "null")
                    {
                        DateTime fromdate;
                        fromdate = DateTime.ParseExact(item.From.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        DateTime todate;
                        todate = DateTime.ParseExact(item.To.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                        todate = new DateTime(todate.Year, todate.Month, todate.Day);
                        if (fromdate > todate)
                        {
                            throw new NotImplementedException("Please choose To bigger From");
                        }
                        queryParameters.Add("@From", fromdate);
                        queryParameters.Add("@To", todate);
                    }

                    queryParameters.Add("@ProdOrder", item.ProdOrder.Value);
                    queryParameters.Add("@IV", item.IV.Value);
                    queryParameters.Add("@VendorCD", item.VendorCD.Value);
                    queryParameters.Add("@MatCD", item.MatCD.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_Report_STIn_ItemLeather_SFLeatherGR"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_Report_STIn_ItemLeather_SFLeatherGR_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_Report_STIn_ItemLeather_SFLeatherGR", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                if (item.From.Value != "null" && item.To.Value != "null")
                {
                    DateTime fromdate;
                    fromdate = DateTime.ParseExact(item.From.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    DateTime todate;
                    todate = DateTime.ParseExact(item.To.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                    todate = new DateTime(todate.Year, todate.Month, todate.Day);
                    if (fromdate > todate)
                    {
                        throw new NotImplementedException("Please choose To bigger From");
                    }
                    cmd.Parameters.Add("@From", SqlDbType.DateTime).Value = fromdate;
                    cmd.Parameters.Add("@To", SqlDbType.DateTime).Value = todate;
                }




                cmd.Parameters.Add("@ProdOrder", SqlDbType.NVarChar).Value = item.ProdOrder.Value;
                cmd.Parameters.Add("@IV", SqlDbType.NVarChar).Value = item.IV.Value;
                cmd.Parameters.Add("@VendorCD", SqlDbType.NVarChar).Value = item.VendorCD.Value;
                cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD.Value;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_Report_STIn_ItemLeather_GetByIVListNo(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@IVLISTNO", item.IVLISTNO.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("SELECT a.* FROM dbo.STIn_ItemLeather (NOLOCK) a WHERE a.IVListNoGR=@IVLISTNO AND a.OutListNoRGI Is Null"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.MH
{
    public class TMH_Repository : GenericRepository<TMH>, ITMH_Repository
    {
        public TMH_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TMH_Repository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<IList<TMH>> ERP_TMH_ByMHCode(string MHCode)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT MHID,MHCode,MH1Code,MH1,MH1Desc,MH2Code,MH2,MH2Desc,UOM,Remark,Seq,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn " +
                        "FROM dbo.TMH (NOLOCK) " +
                        "WHERE MHCode='' OR ''='' ORDER BY MH1 ";
                    connection.Open();
                    var data = await connection.QueryAsync<TMH>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

    }
}
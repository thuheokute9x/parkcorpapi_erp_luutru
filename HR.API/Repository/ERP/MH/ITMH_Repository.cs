﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.MH
{
    interface ITMH_Repository : IGenericRepository<TMH>
    {
        Task<IList<TMH>> ERP_TMH_ByMHCode(string MHCode);
        
    }
}

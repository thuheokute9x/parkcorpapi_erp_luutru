﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models.ERP.TempModel.Development;
using HR.API.Repository;
using Jil;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Mat
{
    public class TMatRepository : GenericRepository<TMat>, ITMatRepository
    {
        public TMatRepository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TMatRepository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<Data> ERP_TMat_Transfer(TDMat_Custom item)
        {
            Library.TMatRepository dt = new Library.TMatRepository();
            return await dt.ERP_TMat_Transfer(item, Settings.GetConnectionString(GetDB()),GetUserlogin());
        }
        public async Task<IList<TMat>> ERP_TMat_GetMatName(string MatName)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "";
                    if (!string.IsNullOrEmpty(MatName))
                    {
                        query = "SELECT TOP 15  MatName " +
                            "FROM dbo.TMat (NOLOCK) WHERE MatName LIKE N'%" + MatName + "%'";
                    }
                    if (string.IsNullOrEmpty(MatName))
                    {
                        query = "SELECT TOP 15 MatName FROM dbo.TMat (NOLOCK)";
                    }
                    var data = await connection.QueryAsync<TMat>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TMat_Custom>> ERP_TMat_GetwithMatDescLike(TMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatDesc", string.IsNullOrEmpty(item.MatDesc) ? "" : item.MatDesc.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<TMat_Custom>("ERP_TMat_GetwithMatDescLike"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TMat_Basic>> ERP_TMat_GetwithMatCDLike(TMat_Custom item)//Nhớ sửa bên load all select Rediscache
        {
            try
            {
                IDatabase db = ConnectRedis();
                string key = GetDB()+ "TMatSelect_GetAll";
                string valued = await db.StringGetAsync(key);
                item.MatCD = item.MatCD.Replace('m','M');
                if (!string.IsNullOrEmpty(valued))
                {
                    if (string.IsNullOrEmpty(item.MatCD))
                    {
                        List<TMat_Basic> dt = JSON.Deserialize<List<TMat_Basic>>(valued).Take(15).ToList();
                        return dt;
                    }
                    if (!string.IsNullOrEmpty(item.MatCD))
                    {
                        List<TMat_Basic> ListTMat = JSON.Deserialize<List<TMat_Basic>>(valued).Where(x => x.MatCD.Contains(item.MatCD)).Take(15).ToList();
                        return ListTMat;
                    }
                }
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCD", item.MatCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TMat_Basic>("ERP_TMat_GetwithMatCDLike"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                IDatabase db = ConnectRedis();
                using (var connection = new SqlConnection(Settings.GetConnectionString(Settings.GetConnectionString("Company"))))
                {

                    string Query = "SELECT MatCD,CASE WHEN Spec1 IS NULL AND Spec2 IS NOT NULL THEN MatName+ ' '+ Spec2 WHEN Spec1 IS NOT NULL AND Spec2 IS NULL  THEN MatName+ ' '+ Spec1 WHEN Spec1 IS NOT NULL AND Spec2 IS NOT NULL  THEN MatName+ ' '+ Spec1 + ' '+ Spec2 ELSE MatName END MatDesc  FROM dbo.TMat (NOLOCK)";
                    var List = new DynamicParameters();
                    connection.Open();
                    var data = connection.Query<TMat_Basic>(Query, List, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    string value = JsonConvert.SerializeObject(data);
                    db.StringSet(Settings.GetConnectionString("Company") + "TMatSelect_GetAll", value);

                }                
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<TMat_Custom> ERP_TMat_GetItemByMatCD(TMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@MatGrpID", item.MatGrpID);
                    queryParameters.Add("@MfgVendor", item.MfgVendor);
                    connection.Open();
                    var data = await connection.QueryAsync<TMat_Custom>("ERP_TMat_GetItemByMatCD"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TMat_Custom>> ERP_TMat_GetRapMua(TMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatDesc", string.IsNullOrEmpty(item.MatDesc) ? "" : item.MatDesc.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<TMat_Custom>("SELECT c.MatCD,CASE WHEN c.Spec1 IS NULL AND c.Spec2 IS NOT NULL THEN c.MatName+ ' '+ c.Spec2 " +
                        "WHEN c.Spec1 IS NOT NULL AND c.Spec2 IS NULL  THEN c.MatName+ ' '+ c.Spec1 " +
                        "WHEN c.Spec1 IS NOT NULL AND c.Spec2 IS NOT NULL  THEN c.MatName+ ' '+ c.Spec1 + ' '+ c.Spec2 " +
                        "ELSE c.MatName END MatDesc,c.Color,c.MUnit " +
                        "FROM dbo.TMatGrpItem a (NOLOCK) " +
                        "INNER JOIN dbo.TDMat b (NOLOCK) ON a.DMatCD=b.DMatCD " +
                        "INNER JOIN dbo.TMat c (NOLOCK) ON b.MatCD=c.MatCD AND c.Discontinue=0 " +
                        "WHERE c.RapMua=1 AND a.MatGrpID="+ item.MatGrpID + " ORDER BY c.MatCD"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TMat_Custom>> ERP_TMat_GetPageSize(TMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCDList", item.MatCDList);
                    queryParameters.Add("@FirstMatGrpID", item.FirstMatGrpID);
                    queryParameters.Add("@MatName", item.MatName);
                    queryParameters.Add("@OrigMatCD", item.OrigMatCD);
                    queryParameters.Add("@Shipper", item.Shipper);
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@Spec1", item.Spec1);
                    queryParameters.Add("@Costing", item.CostingInt);
                    queryParameters.Add("@GR", item.GRInt);
                    queryParameters.Add("@MfgVendor", item.MfgVendor);
                    queryParameters.Add("@MHID", item.MHID);
                    queryParameters.Add("@Spec2", item.Spec2);
                    queryParameters.Add("@RapMua", item.RapMuaInt);
                    queryParameters.Add("@PO", item.POInt);
                    queryParameters.Add("@PlanID", item.PlanID);
                    queryParameters.Add("@MatDesc", item.MatDesc);
                    queryParameters.Add("@Color", item.Color);
                    queryParameters.Add("@Discontinue", item.DiscontinueInt);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TMat_Custom>("ERP_TMat_GetPageSize"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<TMat> ERP_TMat_Update_FromFMat(TMat item)
        {
            try
            {
                int lenghtcheck = 0;
                lenghtcheck = item.MatName.Length;
                if (item.Spec1 != null)
                {
                    lenghtcheck = lenghtcheck + item.Spec1.Length +1;
                }
                if (item.Spec2 != null)
                {
                    lenghtcheck = lenghtcheck + item.Spec2.Length +1;
                }
                if (lenghtcheck > 255)
                {
                    throw new NotImplementedException("MatDesc(MatName+Spec1+Spec2) exceeds 255 characters. Please check and update again.");
                }
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@FirstMatGrpID", item.FirstMatGrpID);
                    queryParameters.Add("@MatTypeCD", item.MatTypeCD);
                    queryParameters.Add("@MHID", item.MHID);
                    queryParameters.Add("@MatName", item.MatName);
                    queryParameters.Add("@Spec1", item.Spec1);
                    queryParameters.Add("@Spec2", item.Spec2);
                    queryParameters.Add("@Color", item.Color);
                    queryParameters.Add("@MUnit", item.MUnit);
                    queryParameters.Add("@LossRate", item.LossRate);
                    queryParameters.Add("@Costing", item.Costing);
                    queryParameters.Add("@PO", item.PO);
                    queryParameters.Add("@OutSourcing", item.OutSourcing);
                    queryParameters.Add("@RapMua", item.RapMua);
                    queryParameters.Add("@GR", item.GR);
                    queryParameters.Add("@OrigMatCD", item.OrigMatCD);
                    queryParameters.Add("@HSCode", item.HSCode);
                    queryParameters.Add("@Class", item.Class);
                    queryParameters.Add("@Discontinue", item.Discontinue);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<TMat>("ERP_TMat_Update_FromFMat"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return item;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TMat_GetPageSize_Excel(TMat_Custom item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TMat_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;



            cmd.Parameters.Add("@FirstMatGrpID", SqlDbType.Int).Value = item.FirstMatGrpID;
            cmd.Parameters.Add("@MatName", SqlDbType.NVarChar).Value = item.MatName;
            cmd.Parameters.Add("@OrigMatCD", SqlDbType.NVarChar).Value = item.OrigMatCD;
            cmd.Parameters.Add("@Shipper", SqlDbType.NVarChar).Value = item.Shipper;
            cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD;
            cmd.Parameters.Add("@Spec1", SqlDbType.NVarChar).Value = item.Spec1;
            cmd.Parameters.Add("@Costing", SqlDbType.Int).Value = item.CostingInt;
            cmd.Parameters.Add("@GR", SqlDbType.Int).Value = item.GRInt;
            cmd.Parameters.Add("@MfgVendor", SqlDbType.NVarChar).Value = item.MfgVendor;
            cmd.Parameters.Add("@MHID", SqlDbType.Int).Value = item.MHID;
            cmd.Parameters.Add("@Spec2", SqlDbType.NVarChar).Value = item.Spec2;
            cmd.Parameters.Add("@RapMua", SqlDbType.Int).Value = item.RapMuaInt;
            cmd.Parameters.Add("@PO", SqlDbType.Int).Value = item.POInt;
            cmd.Parameters.Add("@PlanID", SqlDbType.Int).Value = item.PlanID;
            cmd.Parameters.Add("@MatDesc", SqlDbType.NVarChar).Value = item.MatDesc;
            cmd.Parameters.Add("@Color", SqlDbType.NVarChar).Value = item.Color;
            cmd.Parameters.Add("@Discontinue", SqlDbType.Int).Value = item.DiscontinueInt;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;


        }
        public async Task<int> ERP_TMat_CheckChangeYUnit(TMat item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParametersMatPO = new DynamicParameters();
                    connection.Open();
                    var dataMatPO = await connection.QueryAsync<TMatPOItem>("SELECT MatCD FROM dbo.TMatPOItem (NOLOCK) WHERE MatCD='"+item.MatCD+"'"
                        , queryParametersMatPO, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    if(dataMatPO.ToList().Any())
                    {
                        return 1;//Có MPO
                    }

                    var queryParametesConsItem = new DynamicParameters();
                    var data = await connection.QueryAsync<TConsItem>("SELECT MatCD FROM dbo.TConsItem (NOLOCK) WHERE MatCD='" + item.MatCD + "'"
                        , queryParametesConsItem, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    if (data.ToList().Any())
                    {
                        return 2;//Có ConsItem
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Boolean> ERP_TMat_UpdateYUnit(TMat item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParametersMatPO = new DynamicParameters();
                    connection.Open();
                    var dataMatPO = await connection.QueryAsync<TMat>("UPDATE TMat SET YUnit='" + item.YUnit+"',UpdatedBy="+ GetUserlogin()+",UpdatedOn=GETDATE() WHERE MatCD='"+item.MatCD+"'"
                        , queryParametersMatPO, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<STMatClass>> ERP_STMatClass_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParametersMatPO = new DynamicParameters();
                    connection.Open();
                    var dt = await connection.QueryAsync<STMatClass>("SELECT MatClassID,MatClassDesc FROM STMatClass (NOLOCK)"
                        , queryParametersMatPO, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return dt.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<TMat> ERP_TMat_GetMatDescByMatCD(TMat item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "SELECT MatCD,CASE WHEN Spec1 IS NULL AND Spec2 IS NOT NULL THEN MatName+ ' '+ Spec2 WHEN Spec1 IS NOT NULL AND Spec2 IS NULL  THEN MatName+ ' '+ Spec1 WHEN Spec1 IS NOT NULL AND Spec2 IS NOT NULL  THEN MatName+ ' '+ Spec1 + ' '+ Spec2 ELSE MatName END MatDesc FROM dbo.TMat (NOLOCK) WHERE MatCD=@MatCD";
                    queryParameters.Add("@MatCD", item.MatCD);
                    connection.Open();
                    var dt = await connection.QueryAsync<TMat>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return dt.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TMat_SearchingMaterial_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatDesc", item.MatDesc.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TMat_SearchingMaterial_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TMat_SearchingMaterial_GetPageSize_Excel(dynamic item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TMat_SearchingMaterial_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@MatDesc", SqlDbType.NVarChar).Value = item.MatDesc.Value;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;


        }
    }
}
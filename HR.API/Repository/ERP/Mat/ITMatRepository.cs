﻿using HR.API.IRepository;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models.ERP.TempModel.Development;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Mat
{
    interface ITMatRepository : IGenericRepository<TMat>
    {
        Task<IList<TMat_Custom>> ERP_TMat_GetwithMatDescLike(TMat_Custom item);
        Task<IList<TMat_Basic>> ERP_TMat_GetwithMatCDLike(TMat_Custom item);
        Task<IList<TMat>> ERP_TMat_GetMatName(string MatName);
        Task<TMat_Custom> ERP_TMat_GetItemByMatCD(TMat_Custom item);
        Task<Data> ERP_TMat_Transfer(TDMat_Custom item);
        Task<IList<TMat_Custom>> ERP_TMat_GetRapMua(TMat_Custom item);
        Task<IList<TMat_Custom>> ERP_TMat_GetPageSize(TMat_Custom item);
        Task<TMat> ERP_TMat_Update_FromFMat(TMat item);
        DataTable ERP_TMat_GetPageSize_Excel(TMat_Custom item);
        Task<int> ERP_TMat_CheckChangeYUnit(TMat item);
        Task<Boolean> ERP_TMat_UpdateYUnit(TMat item);
        Task<IList<STMatClass>> ERP_STMatClass_GetAll();
        Task<TMat> ERP_TMat_GetMatDescByMatCD(TMat item);
        Task<IList<Object>> ERP_TMat_SearchingMaterial_GetPageSize(dynamic item);
        DataTable ERP_TMat_SearchingMaterial_GetPageSize_Excel(dynamic item);
    }
}
﻿using Dapper;

using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.PayDoc
{
    public class STPayDoc_ListRepository : GenericRepository<STPayDoc_List>, ISTPayDoc_ListRepository
    {
        public STPayDoc_ListRepository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public STPayDoc_ListRepository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<IList<STPayDoc_List_Custom>> ERP_STPayDoc_List_Get(STPayDoc_List_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT a.PayDocID,b.PayDocListID,b.PayDocID,b.PayDocCD" +
                        ",b.FilePath,b.Merge,c.PayDocDesc,c.PayDocSN FROM dbo.STPayDoc a (NOLOCK) " +
                        "INNER JOIN dbo.STPayDoc_List b (NOLOCK) ON a.PayDocID=b.PayDocID " +
                        "LEFT JOIN dbo.STPayDocCD c (NOLOCK) ON b.PayDocCD=c.PayDocCD " +
                        "WHERE a.RglPayReqID='" + item.RglPayReqID + "' AND a.PayDocNo='" + item.PayDocNo + "'  AND   b.PayDocCD NOT IN ('DN','CN','OT') " +
                        "ORDER BY c.PayDocSN";

                    var data = await connection.QueryAsync<STPayDoc_List_Custom>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }

        public async Task<Boolean> ERP_STPayDoc_List_InsertList(List<STPayDoc_List> item)
        {
            await this.BulkInsert(item);
            return await Task.FromResult(true);
        }

        public async Task<Boolean> ERP_STPayDoc_Delete(STPayDoc_List STPayDoc)
        {
            try
            {
                await this.Delete(STPayDoc);
                await this.Context.SaveChangesAsync();
                return await Task.FromResult(true);

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<int> ERP_STPayDoc_List_IsExistPayDocCD(STPayDoc_List_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    //string querycheck = "SELECT a.PayDocCD,a.PayDocNo FROM dbo.STPayDoc a (NOLOCK) " +
                    //    "INNER JOIN dbo.STPayDoc_List b (NOLOCK) ON a.PayDocID=b.PayDocID AND b.FilePath='"+ item.FilePath + "' " +
                    //    "WHERE a.RglPayReqID='"+ item.RglPayReqID + "'";
                    if (!string.IsNullOrEmpty(item.FilePath))
                    {
                        var queryParameterscheck = new DynamicParameters();
                        queryParameterscheck.Add("@RglPayReqID", item.RglPayReqID);
                        queryParameterscheck.Add("@FilePath", item.FilePath);
                        connection.Open();
                        var datacheck = await connection.QueryAsync<STPayDoc_List>("ERP_STPayDoc_List_CheckFilePathExist"
                            , queryParameterscheck, commandTimeout: 1500
                            , commandType: CommandType.StoredProcedure);
                        if (datacheck.ToList().Any())
                        {
                            return await Task.FromResult(3);//This FilePath is exist in
                        }
                    }


                    string query = "SELECT b.PayDocListID FROM dbo.STPayDoc a (NOLOCK) " +
                        "INNER JOIN dbo.STPayDoc_List b (NOLOCK) " +
                        "ON a.PayDocID=b.PayDocID AND b.PayDocCD='" + item.PayDocCD + "' AND a.RglPayReqID='" + item.RglPayReqID + "' AND a.PayDocNo='" + item.PayDocNo + "'";
                    var queryParameters = new DynamicParameters();
                    var data = await connection.QueryAsync<STPayDoc_List>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    if (data.Any())
                    {
                        return await Task.FromResult(2);//PayDocCD is exist
                    }
                    else
                        return await Task.FromResult(1);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<bool> ERP_STPayDoc_List_CheckPatchExist(STPayDoc_List_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameterscheck = new DynamicParameters();
                    queryParameterscheck.Add("@RglPayReqID", item.RglPayReqID);
                    queryParameterscheck.Add("@FilePath", item.FilePath);
                    connection.Open();
                    var datacheck = await connection.QueryAsync<STPayDoc_List>("ERP_STPayDoc_List_CheckFilePathExist"
                        , queryParameterscheck, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    if (datacheck.ToList().Any())
                    {
                        return await Task.FromResult(false);
                    }
                    else
                        return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<bool> ERP_STPayDoc_List_UpdateMerge(STPayDoc_List_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    int value= item.Merge.Value?1 : 0;
                    string query = "UPDATE dbo.STPayDoc_List SET Merge="+ value + " WHERE PayDocListID=" + item.PayDocListID +"";
                    var data = await connection.QueryAsync<STPayDoc_List_Custom>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<STPayDoc_List_Custom>> ERP_STPayDoc_List_GetByRglPayReqID(string RglPayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT a.PayDocID,b.PayDocListID,b.PayDocID,b.PayDocCD" +
                        ",b.FilePath,b.Merge,c.PayDocDesc,c.PayDocSN FROM dbo.STPayDoc a (NOLOCK) " +
                        "INNER JOIN dbo.STPayDoc_List b (NOLOCK) ON a.PayDocID=b.PayDocID " +
                        "LEFT JOIN dbo.STPayDocCD c (NOLOCK) ON b.PayDocCD=c.PayDocCD " +
                        "WHERE a.RglPayReqID='" + RglPayReqID + "' AND b.Merge=1" +
                        "ORDER BY a.PayDocVendorCD,a.PayDocNo";

                    var data = await connection.QueryAsync<STPayDoc_List_Custom>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
﻿using HR.API.IRepository;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.PayDoc
{
    interface ISTPayDoc_ListRepository : IGenericRepository<STPayDoc_List>
    {
        Task<IList<STPayDoc_List_Custom>> ERP_STPayDoc_List_Get(STPayDoc_List_Custom item);
        Task<Boolean> ERP_STPayDoc_List_InsertList(List<STPayDoc_List> STPayDoc_List);
        Task<Boolean> ERP_STPayDoc_Delete(STPayDoc_List STPayDoc_List);
        Task<int> ERP_STPayDoc_List_IsExistPayDocCD(STPayDoc_List_Custom item);
        Task<Boolean> ERP_STPayDoc_List_CheckPatchExist(STPayDoc_List_Custom item);
        Task<Boolean> ERP_STPayDoc_List_UpdateMerge(STPayDoc_List_Custom item);
        Task<IList<STPayDoc_List_Custom>> ERP_STPayDoc_List_GetByRglPayReqID(string RglPayReqID);
        

    }
}

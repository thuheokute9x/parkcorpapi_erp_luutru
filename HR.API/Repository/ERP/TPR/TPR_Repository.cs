﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using Jil;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.TTPR
{
    public class TPR_Repository : GenericRepository<TPR>, ITPR_Repository
    {
        public TPR_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TPR_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TPRStatu>> ERP_TPRStatus_GetTPRStatus()
        {
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                var queryParameters = new DynamicParameters();
                connection.Open();
                var data = await connection.QueryAsync<TPRStatu>("SELECT StatusID,PRStatus,StatusDesc FROM dbo.TPRStatus (NOLOCK)"
                    , queryParameters, commandTimeout: 1500
                    , commandType: CommandType.Text);
                return data.ToList();
            }
        }

        public async Task<IList<TPR>> ERP_TPR_GetWithRefVKNo(string RefVKNo)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("RefVKNo", string.IsNullOrEmpty(RefVKNo) || RefVKNo == "All" ? "" : RefVKNo.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<TPR>("ERP_TPR_GetWithRefVKNo"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TPR_GetRefVKNoLike(TPR item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(item.RefVKNo) || item.RefVKNo == "All" ? "" : item.RefVKNo.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("SELECT TOP(15) 'R' + RIGHT('0000000' + CAST(PRID AS VARCHAR(7)), 7) PRID_Custom,PRID,RefVKNo FROM dbo.TPR (NOLOCK) WHERE @RefVKNo='' OR 'R' + RIGHT('0000000' + CAST(PRID AS VARCHAR(7)), 7) LIKE '%'+ @RefVKNo+'%' ORDER BY CreatedOn DESC"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TPR>> ERP_TPR_GetWithRefVKNo_ExcludeSP(TPR item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RefVKNo", item.RefVKNo);
                    connection.Open();
                    var data = await connection.QueryAsync<TPR>("ERP_TPR_GetWithRefVKNo_ExcludeSP"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TPR_Custom>> ERP_TPR_GetQtyTBP_OverpPageSize(TPR_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRID", item.PRID);
                    queryParameters.Add("@StandBy", item.StandBy);
                    queryParameters.Add("@InProcess", item.InProcess);
                    queryParameters.Add("@Pending", item.Pending);
                    queryParameters.Add("@Completed", item.Completed);
                    queryParameters.Add("@Canceled", item.Canceled);
                    queryParameters.Add("@CloseOn", item.CloseOn);
                    queryParameters.Add("@RefVKNo", item.RefVKNo);
                    queryParameters.Add("@Remark", string.IsNullOrEmpty(item.Remark)? item.Remark: item.Remark.Trim());
                    if (item.CreatedOn_From.HasValue)
                    {
                        queryParameters.Add("@CreatedOn_From", item.CreatedOn_From);
                        queryParameters.Add("@CreatedOn_To", item.CreatedOn_To);
                    }
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TPR_Custom>("ERP_TPR_GetQtyTBP_OverpPageSize"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TPR_GetQtyTBP_OverpPageSize_Excel(TPR_Custom item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TPR_GetQtyTBP_OverpPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@StandBy", SqlDbType.Bit).Value = item.StandBy;
            cmd.Parameters.Add("@InProcess", SqlDbType.Bit).Value = item.InProcess;
            cmd.Parameters.Add("@Pending", SqlDbType.Bit).Value = item.Pending;
            cmd.Parameters.Add("@Completed", SqlDbType.Bit).Value = item.Completed;
            cmd.Parameters.Add("@Canceled", SqlDbType.Bit).Value = item.Canceled;
            cmd.Parameters.Add("@CloseOn", SqlDbType.Bit).Value = item.CloseOn;
            cmd.Parameters.Add("@RefVKNo", SqlDbType.NVarChar).Value = item.RefVKNo;
            cmd.Parameters.Add("@Remark", SqlDbType.NVarChar).Value = item.Remark;
            if (item.CreatedOn_From.HasValue)
            {
                cmd.Parameters.Add("@CreatedOn_From", SqlDbType.DateTime).Value = item.CreatedOn_From;
                cmd.Parameters.Add("@CreatedOn_To", SqlDbType.DateTime).Value = item.CreatedOn_To;
            }
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;


        }
        public async Task<Result> ERP_TPR_UpdateRefVKNo(TPRProdInst item)
        {
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                Result dt = new Result();
                var queryParametersCheck = new DynamicParameters();
                connection.Open();
                queryParametersCheck.Add("@ProdInstCD", item.ProdInstCD);
                var dataCheck = await connection.QueryAsync<TPRProdInst>("SELECT ProdInstCD FROM dbo.TPRProdInst (NOLOCK) WHERE ProdInstCD=@ProdInstCD"
                    , queryParametersCheck, commandTimeout: 1500
                    , commandType: CommandType.Text);
                if (dataCheck.FirstOrDefault()!=null)
                {
                    dt.ErrorCode = 1;
                    dt.ErrorMessage = "0 data is(are) added successfully.";
                    return dt;
                }


                var queryParameters = new DynamicParameters();
                queryParameters.Add("@ProdInstCD", item.ProdInstCD);
                queryParameters.Add("@UpdatedBy", GetUserlogin());
                queryParameters.Add("@ProdInstCD", item.ProdInstCD);
                queryParameters.Add("@PRID", item.PRID);
                var data = await connection.QueryAsync<TPR>("UPDATE dbo.TPR SET RefVKNo=@ProdInstCD,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE PRID=@PRID"
                    , queryParameters, commandTimeout: 1500
                    , commandType: CommandType.Text);

                dt.ErrorCode = 0;
                dt.ErrorMessage = "1 data is(are) added successfully.";
                return dt;
            }
        }
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_GetMaterials(dynamic item)//Có gọi từ hàm hàm khác lưu ý bên ERP_TMatPOItem_GeneratePOWithSelected,ERP_TPRItem_Radis_Sel_UserLogin_PRID sửa bên client
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRItemID", item.PRItemID.Value);
                    queryParameters.Add("@POStatus", item.POStatus.Value);
                    queryParameters.Add("@PRID", item.PRID.Value);
                    queryParameters.Add("@VerityVendor", item.VerityVendor.Value);

                    IDatabase db = ConnectRedis();
                    string key = GetDB() + "_" + GetUserlogin() + "_TPRItem_Radis_Sel" + item.PRID.Value;
                    string valued = await db.StringGetAsync(key);
                    queryParameters.Add("@PRItemIDSel", string.IsNullOrEmpty(valued)?"": valued);

                    queryParameters.Add("@SortColumn", item.SortColumn.Value);
                    queryParameters.Add("@SortOrder", item.SortOrder.Value);

                    queryParameters.Add("@MatCDList", item.MatCDList.Value);
                    queryParameters.Add("@VendorCD", item.VendorCD.Value);
                    queryParameters.Add("@Shipper", item.Shipper.Value);
                    queryParameters.Add("@PGrpCD", item.PGrpCD.Value);

                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<TPRItem_Custom>("ERP_TPRItem_GetMaterials"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);

                    //IDatabase db = ConnectRedis();
                    //string key = this.GetDB() + "_" + GetUserlogin() + "_TPRItem_Radis_Sel" + item.PRID.Value;
                    //string valued = await db.StringGetAsync(key);
                    //if (!string.IsNullOrEmpty(valued))
                    //{
                    //    List<string> result = valued.Trim().Split(' ').ToList();

                    //    var query = (from c in data
                    //                 join ct in result
                    //                 on c.PRItemID equals int.Parse(ct) into g
                    //                 from ct in g.DefaultIfEmpty()
                    //                 //where !string.IsNullOrEmpty(ct) 
                    //                 select new TPRItem_Custom()
                    //                 {
                    //                    TotalRows = c.TotalRows,
                    //                    RowID = c.RowID,
                    //                    expand = c.expand,
                    //                    Sel = ct == null ? false:true,
                    //                    PO = c.PO,
                    //                    AltMat_SmallInt = c.AltMat_SmallInt,
                    //                    AltMat_Bit = c.AltMat_Bit,
                    //                    AltMat = c.AltMat,
                    //                    NG = c.NG,
                    //                    MatCD = c.MatCD,
                    //                    MatDesc = c.MatDesc,
                    //                    Color = c.Color,
                    //                    MfgVendor = c.MfgVendor,
                    //                    Shipper = c.Shipper,
                    //                    PGrp = c.PGrp,
                    //                    TBPQtyFloat = c.TBPQtyFloat,
                    //                    TBPQty = c.TBPQty,
                    //                    CustItemNo1 = c.CustItemNo1,
                    //                    GR = c.GR,
                    //                    MultiSrc = c.MultiSrc,
                    //                    PLotSize = c.PLotSize,
                    //                    PLeadtime = c.PLeadtime,
                    //                    MOQ = c.MOQ,
                    //                    MOQEnabled = c.MOQEnabled,
                    //                    LossRate = c.LossRate,
                    //                    LossQty = c.LossQty,
                    //                    AddQty = c.AddQty,
                    //                    LeftOverQty = c.LeftOverQty,
                    //                    StdQty = c.StdQty,
                    //                    ActQty = c.ActQty,
                    //                    PUnit = c.PUnit,
                    //                    POQty = c.POQty,
                    //                    POCnt = c.POCnt,
                    //                    YUnit = c.YUnit,
                    //                    YQty = c.YQty,
                    //                    Remark = c.Remark,
                    //                    MHID = c.MHID,
                    //                    MH1 = c.MH1,
                    //                    RefVKNo = c.RefVKNo,
                    //                    PRItemID = c.PRItemID,
                    //                    PRID = c.PRID,
                    //                    NotReady = c.NotReady,
                    //                    SumAltMat = c.SumAltMat,
                    //                    MOQNum = c.MOQNum,
                    //                    Applied = c.Applied,
                    //                    TBPNum = c.TBPNum,
                    //                    OverNum = c.OverNum,
                    //                 }).ToList();
                    //    return query;
                    //}

                        return data.ToList();
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Boolean> ERP_TPRItem_Radis_Sel_UserLogin_PRID(dynamic item)
        {
            try
            {
                IDatabase db = ConnectRedis();
                string key = GetDB()+"_"+ GetUserlogin() + "_TPRItem_Radis_Sel" + item.PRID.Value;
                string valued = await db.StringGetAsync(key);


                if (item.SelAll.Value=="TRUE")
                {
   


                    item.PRItemID = 0;
                    item.SortColumn = "";
                    item.SortOrder = "";
                    //item.MatCDList = "";
                    //item.VendorCD = "";
                    //item.Shipper = "";
                    item.Type = 2;
                    item.PageNumber = 1;
                    item.PageSize = 1000000;

                    List<TPRItem_Custom> dt =await this.ERP_TPRItem_GetMaterials(item);
                    if (string.IsNullOrEmpty(valued))
                    {
                        string ListPRItemID = String.Join(" ", dt.Select(x => x.PRItemID).ToArray());
                        if (ListPRItemID.Length > 0)
                        {
                            db.StringSet(key, " " + ListPRItemID);
                        }
                    }
                    if (!string.IsNullOrEmpty(valued))
                    {
                        foreach (TPRItem_Custom ob in dt)
                        {
                            valued = valued.Replace(" " + ob.PRItemID.ToString(), "");
                            valued = valued.Replace("  ", "");
                            valued = valued + ' ' + ob.PRItemID;
                        }
                        db.StringSet(GetDB() + "_" + GetUserlogin() + "_TPRItem_Radis_Sel" + item.PRID.Value, valued);

                    }


                    return await Task.FromResult(true);
                }
                if (item.SelAll.Value == "FALSE")
                {
                    if (string.IsNullOrEmpty(valued))
                    {
                        return await Task.FromResult(true);
                    }
                    //db.KeyDelete(key);
                    item.PRItemID = 0;
                    item.SortColumn = "";
                    item.SortOrder = "";
                    //item.MatCDList = "";
                    //item.VendorCD = "";
                    //item.Shipper = "";
                    item.Type = 2;
                    item.PageNumber = 1;
                    item.PageSize = 1000000;

                    List<TPRItem_Custom> dt = await this.ERP_TPRItem_GetMaterials(item);
                    if (!string.IsNullOrEmpty(valued))
                    {
                        foreach (TPRItem_Custom ob in dt)
                        {
                            valued = valued.Replace(" " + ob.PRItemID.ToString(), "");
                            valued = valued.Replace("  ", "");
                        }
                        db.StringSet(GetDB() + "_" + GetUserlogin() + "_TPRItem_Radis_Sel" + item.PRID.Value, valued);

                    }

                    return await Task.FromResult(true);
                }




                if (!string.IsNullOrEmpty(valued))
                {
                    if (item.Sel.Value)
                    {
                        valued=valued.Replace(" " + item.PRItemID.Value.ToString(), "");
                        valued = valued.Replace("  ", "");
                        valued = valued + ' ' + item.PRItemID.Value;
                        db.StringSet(GetDB() + "_" + GetUserlogin() + "_TPRItem_Radis_Sel" + item.PRID.Value, valued);
                    }
                    if (!item.Sel.Value)
                    {
                        valued=valued.Replace(" "+ item.PRItemID.Value.ToString(), "");
                        valued = valued.Replace("  ", "");
                        db.StringSet(GetDB() + "_" + GetUserlogin() + "_TPRItem_Radis_Sel" + item.PRID.Value, valued);
                    }

                    return await Task.FromResult(true);
                }
                if (string.IsNullOrEmpty(valued))
                {
                    db.StringSet(GetDB() + "_" + GetUserlogin() + "_TPRItem_Radis_Sel" + item.PRID.Value," "+ item.PRItemID.Value);
                    return await Task.FromResult(true);
                }
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
   public DataTable ERP_TPRItem_GetMaterials_Excel(dynamic item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TPRItem_GetMaterials", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            IDatabase db = ConnectRedis();
            string key = GetDB() + "_" + GetUserlogin() + "_TPRItem_Radis_Sel" + item.PRID.Value;
            string valued = db.StringGet(key);
            cmd.Parameters.Add("@PRItemIDSel", SqlDbType.VarChar).Value = string.IsNullOrEmpty(valued)?"": valued;

            cmd.Parameters.Add("@PRItemID", SqlDbType.Int).Value = item.PRItemID.Value;
            cmd.Parameters.Add("@POStatus", SqlDbType.Int).Value = item.POStatus.Value;
            cmd.Parameters.Add("@PRID", SqlDbType.Int).Value = item.PRID.Value;
            cmd.Parameters.Add("@VerityVendor", SqlDbType.Bit).Value = item.VerityVendor.Value;
            
            cmd.Parameters.Add("@SortColumn", SqlDbType.NVarChar).Value = item.SortColumn.Value;
            cmd.Parameters.Add("@SortOrder", SqlDbType.NVarChar).Value = item.SortOrder.Value;
            cmd.Parameters.Add("@MatCDList", SqlDbType.NVarChar).Value = item.MatCDList.Value;
            cmd.Parameters.Add("@VendorCD", SqlDbType.NVarChar).Value = item.VendorCD.Value;
            cmd.Parameters.Add("@Shipper", SqlDbType.NVarChar).Value = item.Shipper.Value;
            cmd.Parameters.Add("@PGrpCD", SqlDbType.NVarChar).Value = item.PGrpCD.Value;

            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            //if (!string.IsNullOrEmpty(valued))
            //{
            //    //List<string> result = valued.Trim().Split(' ').ToList();



            //    foreach (DataRow dr in table.Rows) // search whole table
            //    {
            //        if (valued.IndexOf(dr["PRItemID"].ToString()) > -1)
            //        {
            //            dr["Sel"] = true;
            //        }
            //    }
            //    table.Columns.Remove("PRItemID");
            //    return table;
            //}
            //table.Columns.Remove("PRItemID");
            return table;


        }    
        public async Task<IList<Object>> ERP_TMatPOItem_GetMaterialsByMatPOItemID(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RefVKNo", item.RefVKNo.Value);
                    queryParameters.Add("@MatCD", item.MatCD.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TMatPOItem_GetMaterialsByMatPOItemID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_PO_AltMat_MfgVendor_Shipper_PGrp_PLotSize_PLeadtime_MOQ_MOQEnabled_StdQty_PUnit_Remark(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    connection.Open();
                    item.IsUpdateMOQ = false;
                    if (item.IschangeVendor.Value)
                    {

                        var valuewithMfgVendor = new DynamicParameters();
                        string query = @"SELECT a.Shipper,a.PGrpCD,a.Shipper,a.PGrpCD,a.PLotSize,a.PLeadtime,a.MOQ,a.PUnit 
                        FROM dbo.TPInfo a (NOLOCK) INNER JOIN dbo.TVendor b (NOLOCK) 
                        ON a.MfgVendor=b.VendorCD WHERE a.MatCD=@MatCD AND a.Discontinue=0 
                        AND b.VendorCD=@VendorCD ORDER BY a.PInfoID DESC";
                        valuewithMfgVendor.Add("@VendorCD", item.MfgVendor.Value);
                        valuewithMfgVendor.Add("@MatCD", item.MatCD.Value);


                        TPInfo_Custom ob = await connection.QueryFirstOrDefaultAsync<TPInfo_Custom>(query
                            , valuewithMfgVendor, commandTimeout: 1500
                            , commandType: CommandType.Text);
                        if (ob!=null)
                        {
                            item.Shipper = ob.Shipper;
                            item.PGrp.Value = ob.PGrpCD;
                            item.PLotSize.Value = ob.PLotSize;
                            item.PLeadtime.Value = ob.PLeadtime;
                            item.MOQ.Value = ob.MOQ;
                            item.IsUpdateMOQ = true;
                            if (ob.MOQ>0)
                            {
                                item.MOQEnabled.Value = true;
                            }
                            if (ob.MOQ <= 0)
                            {
                                item.MOQEnabled.Value = false;
                            }
                            item.PUnit.Value = ob.PUnit;
                        }

                    }

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRItemID", item.PRItemID.Value);
                    queryParameters.Add("@PO", item.PO.Value);
                    queryParameters.Add("@AltMat", item.AltMat.Value);
                    queryParameters.Add("@MfgVendor", item.MfgVendor.Value);
                    queryParameters.Add("@Shipper", item.Shipper.Value);
                    queryParameters.Add("@PGrp", item.PGrp.Value);
                    queryParameters.Add("@PLotSize", item.PLotSize.Value);
                    queryParameters.Add("@PLeadtime", item.PLeadtime.Value);
                    queryParameters.Add("@MOQ", item.MOQ.Value);
                    queryParameters.Add("@IsUpdateMOQ", item.IsUpdateMOQ.Value);
                    queryParameters.Add("@MOQEnabled", item.MOQEnabled.Value);
                    queryParameters.Add("@AddQty", item.AddQty.Value);
//                    queryParameters.Add("@LeftOverQty", item.LeftOverQty.Value);
                    queryParameters.Add("@PUnit", item.PUnit.Value);
                    queryParameters.Add("@YUnit", item.YUnit.Value);
                    queryParameters.Add("@Remark", item.Remark.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());

                    
                    var updateTPRItem = await connection.QueryAsync<Object>(@"UPDATE dbo.TPRItem SET PO=@PO,AltMat=@AltMat,MfgVendor=@MfgVendor
,Shipper=@Shipper,PGrp=@PGrp,PLotSize=@PLotSize,PLeadtime=@PLeadtime
,MOQ= CASE WHEN @IsUpdateMOQ=1 THEN @MOQ
ELSE MOQ END
,MOQEnabled=@MOQEnabled
,Remark=@Remark,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE(),StdQty=YQty*dbo.ConvQtyGR(@YUnit,@PUnit)
,PUnit=@PUnit
WHERE PRItemID=@PRItemID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    dynamic param= new System.Dynamic.ExpandoObject();

                    //item.PRItemID = item.PRItemID.Value;
                    item.POStatus = 0;
                    //item.PRID = item.PRID.Value;
                    item.PageNumber = 1;
                    item.PageSize = 1;
                    item.Type = 0;
                    item.VerityVendor = false;
                    item.SortColumn = "";
                    item.SortOrder = "";
                    item.MatCDList = "";
                    item.VendorCD = "";
                    item.Shipper = "";
                    item.PGrpCD = "";
                    return await this.ERP_TPRItem_GetMaterials(item);
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_LeftOverQty(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    connection.Open();

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRItemID", item.PRItemID.Value);
                    queryParameters.Add("@LeftOverQty", item.LeftOverQty.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());


                    var updateTPRItem = await connection.QueryAsync<Object>(@"UPDATE dbo.TPRItem SET LeftOverQty=@LeftOverQty
,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE()
WHERE PRItemID=@PRItemID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    dynamic param = new System.Dynamic.ExpandoObject();

                    //item.PRItemID = item.PRItemID.Value;
                    item.POStatus = 0;
                    //item.PRID = item.PRID.Value;
                    item.PageNumber = 1;
                    item.PageSize = 1;
                    item.Type = 0;
                    item.VerityVendor = false;
                    item.SortColumn = "";
                    item.SortOrder = "";
                    item.MatCDList = "";
                    item.VendorCD = "";
                    item.Shipper = "";
                    item.PGrpCD = "";
                    return await this.ERP_TPRItem_GetMaterials(item);
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_AddQty(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    connection.Open();

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRItemID", item.PRItemID.Value);
                    queryParameters.Add("@AddQty", item.AddQty.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());


                    var updateTPRItem = await connection.QueryAsync<Object>(@"UPDATE dbo.TPRItem SET AddQty=@AddQty
,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE()
WHERE PRItemID=@PRItemID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    dynamic param = new System.Dynamic.ExpandoObject();

                    //item.PRItemID = item.PRItemID.Value;
                    item.POStatus = 0;
                    //item.PRID = item.PRID.Value;
                    item.PageNumber = 1;
                    item.PageSize = 1;
                    item.Type = 0;
                    item.VerityVendor = false;
                    item.SortColumn = "";
                    item.SortOrder = "";
                    item.MatCDList = "";
                    item.VendorCD = "";
                    item.Shipper = "";
                    item.PGrpCD = "";
                    return await this.ERP_TPRItem_GetMaterials(item);
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_LossQty(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    connection.Open();

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRItemID", item.PRItemID.Value);
                    queryParameters.Add("@LossQty", item.LossQty.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());


                    var updateTPRItem = await connection.QueryAsync<Object>(@"UPDATE dbo.TPRItem SET LossQty=@LossQty
,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE()
WHERE PRItemID=@PRItemID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    dynamic param = new System.Dynamic.ExpandoObject();

                    //item.PRItemID = item.PRItemID.Value;
                    item.POStatus = 0;
                    //item.PRID = item.PRID.Value;
                    item.PageNumber = 1;
                    item.PageSize = 1;
                    item.Type = 0;
                    item.VerityVendor = false;
                    item.SortColumn = "";
                    item.SortOrder = "";
                    item.MatCDList = "";
                    item.VendorCD = "";
                    item.Shipper = "";
                    item.PGrpCD = "";
                    return await this.ERP_TPRItem_GetMaterials(item);
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_LossRate(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    connection.Open();

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRItemID", item.PRItemID.Value);
                    queryParameters.Add("@LossRate", item.LossRate.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());


                    var updateTPRItem = await connection.QueryAsync<Object>(@"UPDATE dbo.TPRItem SET LossRate=@LossRate
,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE()
WHERE PRItemID=@PRItemID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    dynamic param = new System.Dynamic.ExpandoObject();

                    //item.PRItemID = item.PRItemID.Value;
                    item.POStatus = 0;
                    //item.PRID = item.PRID.Value;
                    item.PageNumber = 1;
                    item.PageSize = 1;
                    item.Type = 0;
                    item.VerityVendor = false;
                    item.SortColumn = "";
                    item.SortOrder = "";
                    item.MatCDList = "";
                    item.VendorCD = "";
                    item.Shipper = "";
                    item.PGrpCD = "";
                    return await this.ERP_TPRItem_GetMaterials(item);
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_MOQ(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    connection.Open();

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRItemID", item.PRItemID.Value);
                    queryParameters.Add("@MOQ", item.MOQ.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());


                    var updateTPRItem = await connection.QueryAsync<Object>(@"UPDATE dbo.TPRItem SET MOQ=@MOQ
,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE()
WHERE PRItemID=@PRItemID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    dynamic param = new System.Dynamic.ExpandoObject();

                    //item.PRItemID = item.PRItemID.Value;
                    item.POStatus = 0;
                    //item.PRID = item.PRID.Value;
                    item.PageNumber = 1;
                    item.PageSize = 1;
                    item.Type = 0;
                    item.VerityVendor = false;
                    item.SortColumn = "";
                    item.SortOrder = "";
                    item.MatCDList = "";
                    item.VendorCD = "";
                    item.Shipper = "";
                    item.PGrpCD = "";
                    return await this.ERP_TPRItem_GetMaterials(item);
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Boolean> ERP_TPRItem_RefreshPOInfo(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRID", item.PRID.Value);
                    queryParameters.Add("@CreatedBy", GetUserlogin());//nhớ sửa bên ERP_TMatPO_Update
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TPRItem_RefreshPOInfo"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Result> ERP_TPRItem_VerityVendor(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRID", item.PRID.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Result>("ERP_TPRItem_VerityVendor"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Boolean> ERP_TPRProdInst_Delete(TPRProdInst item)
        {
            using (PARKERPEntities context = new PARKERPEntities())
            {
                using (var transaction = context.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        TPRProdInst itemdelete = context.TPRProdInsts.FirstOrDefault(x => x.PRProdInstID == item.PRProdInstID);
                        context.TPRProdInsts.Remove(itemdelete);
                        TPR itemUpdate = context.TPRs.FirstOrDefault(x => x.RefVKNo == item.ProdInstCD);

                        //List<TPRItem> itemchildrentUpdate = context.TPRItems.Where(x => x.PRID == item.PRID).ToList();
                        //foreach (TPRItem ob in itemchildrentUpdate)
                        //{
                        //    context.TPRItems.Remove(ob);
                        //}
                        itemUpdate.RefVKNo = null;
                        await context.SaveChangesAsync();
                        transaction.Commit();
                        return await Task.FromResult(true);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new NotImplementedException(ex.Message.ToString());
                    }
                }
            }
        }
        public async Task<Boolean> ERP_TPR_CheckExistRefVKNo(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RefVKNo", item.RefVKNo.Value);
                    connection.Open();
                    TPR data = await connection.QueryFirstOrDefaultAsync<TPR>("SELECT RefVKNo FROM dbo.TPR (NOLOCK) WHERE RefVKNo=@RefVKNo"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    if (data!=null)
                    {
                        return await Task.FromResult(true);

                    }
                    return await Task.FromResult(false);

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TPRItem_GetVendorWithPRID(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@VendorCD", item.VendorCD.Value);
                    queryParameters.Add("@PRID", item.PRID.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TPRItem_GetVendorWithPRID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TPRItem_GetShipperWithPRID(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Shipper", item.Shipper.Value);
                    queryParameters.Add("@PRID", item.PRID.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TPRItem_GetShipperWithPRID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TPRItem_GetPGrpWithPRID(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRID", item.PRID.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TPRItem_GetPGrpWithPRID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TPR_UpdateRefVKNo_RefreshVKNo(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRID", item.PRID.Value);
                    queryParameters.Add("@UpdatedBy", this.GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(@"UPDATE dbo.TPR SET RefVKNo=
		                    LTRIM(RTRIM(STUFF((SELECT ', ' + CAST(a.ProdInstCD AS varchar(7))
	                    FROM dbo.TProdInst a (NOLOCK) 
	                    INNER JOIN dbo.TPRProdInst b (NOLOCK) ON a.ProdInstCD = b.ProdInstCD
	                    WHERE b.PRID=@PRID
							                    FOR XML PATH('')
		                    ),1,1,'')))
		                    ,UpdatedBy=@UpdatedBy
		                    ,UpdatedOn=GETDATE()
                        WHERE TPR.PRID=@PRID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
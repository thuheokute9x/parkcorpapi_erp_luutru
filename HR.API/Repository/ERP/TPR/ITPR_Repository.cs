﻿using HR.API.IRepository;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.TTPR
{
    interface ITPR_Repository : IGenericRepository<TPR>
    {
        Task<IList<TPR>> ERP_TPR_GetWithRefVKNo(string RefVKNo);
        Task<IList<TPR>> ERP_TPR_GetWithRefVKNo_ExcludeSP(TPR item);
        Task<IList<TPRStatu>> ERP_TPRStatus_GetTPRStatus();
        Task<IList<TPR_Custom>> ERP_TPR_GetQtyTBP_OverpPageSize(TPR_Custom item);
        DataTable ERP_TPR_GetQtyTBP_OverpPageSize_Excel(TPR_Custom item);
        Task<Result> ERP_TPR_UpdateRefVKNo(TPRProdInst item);
        Task<IList<TPRItem_Custom>> ERP_TPRItem_GetMaterials(dynamic item);
        Task<Boolean> ERP_TPRItem_Radis_Sel_UserLogin_PRID(dynamic item);
        DataTable ERP_TPRItem_GetMaterials_Excel(dynamic item);
        Task<IList<Object>> ERP_TMatPOItem_GetMaterialsByMatPOItemID(dynamic item);
        Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_PO_AltMat_MfgVendor_Shipper_PGrp_PLotSize_PLeadtime_MOQ_MOQEnabled_StdQty_PUnit_Remark(dynamic item);
        Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_LeftOverQty(dynamic item);
        Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_AddQty(dynamic item);
        Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_LossQty(dynamic item);
        Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_LossRate(dynamic item);
        Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_MOQ(dynamic item);
        Task<Boolean> ERP_TPRItem_RefreshPOInfo(dynamic item);
        Task<IList<Object>> ERP_TPR_GetRefVKNoLike(TPR item);
        Task<Result> ERP_TPRItem_VerityVendor(dynamic item);
        Task<Boolean> ERP_TPRProdInst_Delete(TPRProdInst item);
        Task<Boolean> ERP_TPR_CheckExistRefVKNo(dynamic item);
        Task<IList<Object>> ERP_TPRItem_GetVendorWithPRID(dynamic item);
        Task<IList<Object>> ERP_TPRItem_GetShipperWithPRID(dynamic item);
        Task<IList<Object>> ERP_TPRItem_GetPGrpWithPRID(dynamic item);
        Task<IList<Object>> ERP_TPR_UpdateRefVKNo_RefreshVKNo(dynamic item);
    }
}

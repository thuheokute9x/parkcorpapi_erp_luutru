﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.PayReqExt
{
    public class STPayReqExt_Repository : GenericRepository<STPayReqExt>, ISTPayReqExt_Repository
    {
        public STPayReqExt_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public STPayReqExt_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<STPayReqExt>> ERP_STPayReqExt_ByPayReqID(string PayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT a.PayDocCD,a.PayDocNo,b.SurchargeSName SurchargeSCD,a.PayExtDesc,a.Price,a.Curr,a.Remark " +
                        "FROM STPayReqExt a(NOLOCK) " +
                        "INNER JOIN STSurchargeSCat b(NOLOCK) ON a.SurchargeSCD = b.SurchargeSCD WHERE a.PayReqID = '"+ PayReqID + "'";
                    var data = await connection.QueryAsync<STPayReqExt>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
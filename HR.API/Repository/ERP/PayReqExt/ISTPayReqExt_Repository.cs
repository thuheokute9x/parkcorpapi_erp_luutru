﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.PayReqExt
{
    interface ISTPayReqExt_Repository : IGenericRepository<STPayReqExt>
    {
        Task<IList<STPayReqExt>> ERP_STPayReqExt_ByPayReqID(string PayReqID);
    }
}

﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Curr
{
    interface ITCurr_Repository : IGenericRepository<TCurr>
    {
        Task<IList<TCurr>> ERP_TCurr_GetAll();
    }
}

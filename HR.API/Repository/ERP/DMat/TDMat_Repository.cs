﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models.ERP.TempModel.Development;
using HR.API.Repository;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.DMat
{
    public class TDMat_Repository : GenericRepository<TDMat>, ITDMat_Repository
    {
        public TDMat_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TDMat_Repository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<TDMat_Custom> ERP_TDMat_GetItemByDMatCD(TDMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@DMatCD", item.DMatCD);
                    queryParameters.Add("@MatGrpID", item.MatGrpID);
                    connection.Open();
                    var data = await connection.QueryAsync<TDMat_Custom>("ERP_TDMat_GetItemByDMatCD", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TDMat>> ERP_TDMat_ByDMatCD(string DMatCD)
        {
            try
            {  
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT TOP(15)DMatCD FROM dbo.TDMat (NOLOCK) WHERE DMatCD LIKE N'%"+ DMatCD + "%' OR '"+DMatCD+"'=''";
                    var data = await connection.QueryAsync<TDMat>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TDMat_Custom>> ERP_Material_TDMat(TDMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatGrpID", item.MatGrpID);
                    if (item.NANull!=null)
                    {
                        queryParameters.Add("@NANull", item.NANull);
                    }

                    queryParameters.Add("@Flag", item.Flag);
                    queryParameters.Add("@MatDesc", item.MatDesc);
                    queryParameters.Add("@Costing", item.Costing);
                    queryParameters.Add("@DMatCD", item.DMatCD);
                    queryParameters.Add("@DMatCDList", item.DMatCDList);
                    queryParameters.Add("@RapMua", item.RapMua);
                    queryParameters.Add("@CustItemNo1", item.CustItemNo1);
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@PO", item.PO);
                    queryParameters.Add("@MatCat", item.MatCat);
                    queryParameters.Add("@GR", item.GR);
                    queryParameters.Add("@MfgVendor", item.MfgVendor);
                    queryParameters.Add("@MHID", item.MHID);
                    queryParameters.Add("@Color", item.Color);
                    queryParameters.Add("@Shipper", item.Shipper);
                    queryParameters.Add("@MultiSrc", item.MultiSrcInt);
                    queryParameters.Add("@OutSourcing",item.OutSourcingInt);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TDMat_Custom>("ERP_Material_TDMat", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_Material_TDMat_Excel(TDMat_Custom item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_Material_TDMat", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@MatGrpID", SqlDbType.Int).Value = item.MatGrpID;
            if (item.NANull!=null)
            {
                cmd.Parameters.Add("@NANull", SqlDbType.NVarChar).Value = item.NANull;
            }
            cmd.Parameters.Add("@Flag", SqlDbType.Bit).Value = item.Flag;
            cmd.Parameters.Add("@MatDesc", SqlDbType.NVarChar).Value = item.MatDesc;
            cmd.Parameters.Add("@Costing", SqlDbType.Bit).Value = item.Costing;
            cmd.Parameters.Add("@DMatCD", SqlDbType.NVarChar).Value = item.DMatCD;
            cmd.Parameters.Add("@RapMua", SqlDbType.Bit).Value = item.RapMua;
            cmd.Parameters.Add("@CustItemNo1", SqlDbType.NVarChar).Value = item.CustItemNo1;
            cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD;
            cmd.Parameters.Add("@PO", SqlDbType.Bit).Value = item.PO;
            cmd.Parameters.Add("@MatCat", SqlDbType.NVarChar).Value = item.MatCat;
            cmd.Parameters.Add("@GR", SqlDbType.Bit).Value = item.GR;
            cmd.Parameters.Add("@MfgVendor", SqlDbType.NVarChar).Value = item.MfgVendor;
            cmd.Parameters.Add("@MHID", SqlDbType.Int).Value = item.MHID;
            cmd.Parameters.Add("@Color", SqlDbType.NVarChar).Value = item.Color;
            cmd.Parameters.Add("@Shipper", SqlDbType.NVarChar).Value = item.Shipper;
            cmd.Parameters.Add("@MultiSrc", SqlDbType.Int).Value = item.MultiSrcInt;
            cmd.Parameters.Add("@OutSourcing", SqlDbType.Int).Value = item.OutSourcingInt;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;


        }
        public DataTable ERP_Material_TDMat_TMat_Excel(TDMat_Custom item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_Material_TDMat_TMat", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@MatGrpID", SqlDbType.Int).Value = item.MatGrpID;
            if (item.NANull != null)
            {
                cmd.Parameters.Add("@NANull", SqlDbType.NVarChar).Value = item.NANull;

            }
            cmd.Parameters.Add("@Flag", SqlDbType.Bit).Value = item.Flag;
            cmd.Parameters.Add("@MatDesc", SqlDbType.NVarChar).Value = item.MatDesc;
            cmd.Parameters.Add("@Costing", SqlDbType.Bit).Value = item.Costing;
            cmd.Parameters.Add("@DMatCDList", SqlDbType.NVarChar).Value = item.DMatCDList;
            cmd.Parameters.Add("@DMatCD", SqlDbType.NVarChar).Value = item.DMatCD;
            cmd.Parameters.Add("@RapMua", SqlDbType.Bit).Value = item.RapMua;
            cmd.Parameters.Add("@CustItemNo1", SqlDbType.NVarChar).Value = item.CustItemNo1;
            cmd.Parameters.Add("@MatCDList", SqlDbType.NVarChar).Value = item.DMatCDList;
            cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD;
            cmd.Parameters.Add("@PO", SqlDbType.Bit).Value = item.PO;
            cmd.Parameters.Add("@MatCat", SqlDbType.NVarChar).Value = item.MatCat;
            cmd.Parameters.Add("@GR", SqlDbType.Bit).Value = item.GR;
            cmd.Parameters.Add("@MfgVendor", SqlDbType.NVarChar).Value = item.MfgVendor;
            cmd.Parameters.Add("@MHID", SqlDbType.Int).Value = item.MHID;
            cmd.Parameters.Add("@Color", SqlDbType.NVarChar).Value = item.Color;
            cmd.Parameters.Add("@Shipper", SqlDbType.NVarChar).Value = item.Shipper;
            cmd.Parameters.Add("@MultiSrc", SqlDbType.Int).Value = item.MultiSrcInt;
            cmd.Parameters.Add("@OutSourcing", SqlDbType.Int).Value = item.OutSourcingInt;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;


        }
        public async Task<IList<TDMat_Custom>> ERP_Material_TDMat_TMat(TDMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatGrpID", item.MatGrpID);
                    if (item.NANull != null)
                    {
                        queryParameters.Add("@NANull", item.NANull);
                    }
                    queryParameters.Add("@Flag", item.Flag);
                    queryParameters.Add("@MatDesc", item.MatDesc);
                    queryParameters.Add("@Costing", item.Costing);
                    queryParameters.Add("@DMatCDList", item.DMatCDList);
                    queryParameters.Add("@DMatCD", item.DMatCD);
                    queryParameters.Add("@RapMua", item.RapMua);
                    queryParameters.Add("@CustItemNo1", item.CustItemNo1);
                    queryParameters.Add("@MatCDList", item.MatCDList);
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@PO", item.PO);
                    queryParameters.Add("@MatCat", item.MatCat);
                    queryParameters.Add("@GR", item.GR);
                    queryParameters.Add("@MfgVendor", item.MfgVendor);
                    queryParameters.Add("@MHID", item.MHID);
                    queryParameters.Add("@Color", item.Color);
                    queryParameters.Add("@Shipper", item.Shipper);
                    queryParameters.Add("@MultiSrc", item.MultiSrcInt);
                    queryParameters.Add("@OutSourcing", item.OutSourcingInt);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TDMat_Custom>("ERP_Material_TDMat_TMat", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TDMat_Custom>> ERP_Material_TDMat_TMat_ImportProdTransferredMaterial(TDMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatGrpID", item.MatGrpID);
                    if (item.NANull != null)
                    {
                        queryParameters.Add("@NANull", item.NANull);
                    }
                    queryParameters.Add("@Flag", item.Flag);
                    queryParameters.Add("@MatDesc", item.MatDesc);
                    queryParameters.Add("@Costing", item.Costing);
                    queryParameters.Add("@UnderDev", item.UnderDev);
                    queryParameters.Add("@VendorItemNo", item.VendorItemNo);
                    queryParameters.Add("@DMatCD", item.DMatCD);
                    queryParameters.Add("@MatName", item.MatName);
                    queryParameters.Add("@RapMua", item.RapMua);
                    queryParameters.Add("@Tag", item.Tag);
                    queryParameters.Add("@CustItemNo1", item.CustItemNo1);
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@Spec1", item.Spec1);
                    queryParameters.Add("@PO", item.PO);
                    queryParameters.Add("@DevRemark", item.DevRemark);
                    queryParameters.Add("@CustItemNo2", item.CustItemNo2);
                    queryParameters.Add("@MatCat", item.MatCat);
                    queryParameters.Add("@Spec2", item.Spec2);
                    queryParameters.Add("@GR", item.GR);
                    queryParameters.Add("@MfgVendor", item.MfgVendor);
                    queryParameters.Add("@MHID", item.MHID);
                    queryParameters.Add("@Color", item.Color);
                    queryParameters.Add("@Shipper", item.Shipper);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TDMat_Custom>("ERP_Material_TDMat_TMat_ImportProdTransferredMaterial", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<TDMat_Custom> ERP_TDMat_GenerateDMatCD()
        {
            Library.TDMat_Repository dt = new Library.TDMat_Repository();
            return await dt.ERP_TDMat_GenerateDMatCD(Settings.GetConnectionString(GetDB()));
        }
        public async Task<TDMat> InsertTDMat(TDMat_Custom item)
        {
            Library.TDMat_Repository dt = new Library.TDMat_Repository();
            return await dt.InsertTDMat(item, Settings.GetConnectionString(GetDB()));
        }
        public async Task<TDMat> UpdateTDMat(TDMat_Custom item)
        {
            Library.TDMat_Repository dt = new Library.TDMat_Repository();
            return await dt.UpdateTDMat(item, Settings.GetConnectionString(GetDB()),GetUserlogin());
        }

        public async Task<bool> ERP_TDMat_Update_NA(TDMat item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    int NA = 0;
                    if (item.NA)
                    {
                        NA = 1;
                    }
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "Update TDMat set NA="+ NA + ",UpdatedBy="+ GetUserlogin()+ ",UpdatedOn=GETDATE() WHERE DMatCD='"+ item.DMatCD + "'";
                    var data = await connection.QueryAsync<TDMat>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    connection.Close();
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_TMatGrpItem_Update_Flag(TMatGrpItem item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    queryParameters.Add("@Flag", item.Flag);
                    queryParameters.Add("@MatGrpItemID", item.MatGrpItemID);
                    string query = "UPDATE TMatGrpItem SET Flag=@Flag WHERE MatGrpItemID=@MatGrpItemID";
                    var data = await connection.QueryAsync<TMatGrpItem>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    connection.Close();
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_TDMat_Update_Matcat_Price_Spec_Color(TDMat item)
        {
            try
            {

                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    {
                        var queryParameters = new DynamicParameters();
                        connection.Open();
                        queryParameters.Add("@DMatCD", item.DMatCD);
                        queryParameters.Add("@MatCat", item.MatCat);
                        queryParameters.Add("@Spec1", item.Spec1);
                        queryParameters.Add("@Spec2", item.Spec2);
                        queryParameters.Add("@Color", item.Color);
                        queryParameters.Add("@EXWPrice", item.EXWPrice);
                        queryParameters.Add("@FOBPrice", item.FOBPrice);
                        queryParameters.Add("@FOBcPrice", item.FOBcPrice);
                        queryParameters.Add("@CIFPrice", item.CIFPrice);
                        queryParameters.Add("@BuyerPrice", item.BuyerPrice);
                        queryParameters.Add("@UpdatedBy", GetUserlogin());
                        var data = await connection.QueryAsync<TDMat>("ERP_TDMat_Update_Matcat_Price_Spec_Color", queryParameters, commandTimeout: 1500
                            , commandType: CommandType.StoredProcedure);
                        connection.Close();
                    }
                
                return await Task.FromResult(true);

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_TDMat_Update_NAList(List<TDMat> oblist)
        {
            try
            {
                foreach (TDMat item in oblist)
                {
                    await this.ERP_TDMat_Update_NA(item);
                }
                return await Task.FromResult(true);
                
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<bool> UpdateTDMat_MatCat_LossRate_Remark(TDMat_Custom item)
        {
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                using (var transaction = this.Context.Database.BeginTransaction())
                {
                    try
                    {

                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@DMatCD", item.DMatCD);
                        queryParameters.Add("@MatGrpID", item.MatGrpID);
                        connection.Open();
                        var data = await connection.QueryAsync<TDMat>("ERP_TDMat_GetItemByDMatCD"
                            , queryParameters, commandTimeout: 1500
                            , commandType: CommandType.StoredProcedure);

                        if (data.FirstOrDefault() != null)
                        {
                            TDMat ob = new TDMat();
                            ob = data.FirstOrDefault();
                            ob.MatCat = item.MatCat;
                            ob.LossRate = item.LossRate;

                            ob.LossQty = item.LossQty;



    
                            ob.PO = item.PO;
                            ob.GR = item.GR;
                            ob.MultiSrc = item.MultiSrc;
                            ob.AltMat = item.AltMat;
                            ob.OutSourcing = item.OutSourcing;
                            ob.Tag = item.Tag;
                            ob.Remark = item.Remark;

                            ob.UnderDev = item.UnderDev;
                            ob.ETAFromVendor = item.ETAFromVendor;
                            ob.ETDToCust = item.ETDToCust;
                            ob.Couier = item.Couier;
                            ob.TrackingNo = item.TrackingNo;
                            ob.SpecStatus = item.SpecStatus;
                            ob.SpecApprovedOn = item.SpecApprovedOn;
                            ob.QualityStatus = item.QualityStatus;
                            ob.QualityApprovedOn = item.QualityApprovedOn;
                            ob.TestReportNo = item.TestReportNo;
                            ob.DevRemark = item.DevRemark;

                            ob.MfgVendor = item.MfgVendor;
                            TVendor Vendor = Context.TVendors.FirstOrDefault(x => x.VendorCD == item.MfgVendor);
                            ob.Shipper = Vendor.Shipper;

                            TMat a = Context.TMats.FirstOrDefault(x => x.MatCD == item.MatCD);

                            //a.LossRate = item.LossRate.Value;
                            //a.LossQty = item.LossQty;
                            //a.PO = item.PO;
                            //a.GR = item.GR;
                            //a.MultiSrc = item.MultiSrc;
                            //a.AltMat = item.AltMat;
                            //a.OutSourcing = item.OutSourcing;

                            a.Discontinue = item.Discontinue;
                            var dbEntry = Context.Entry<TMat>(a);
                            this.Context.Entry(a).State = System.Data.Entity.EntityState.Modified;

                          
                            //await this.Context.SaveChangesAsync();
                            //await Update(ob);
                            await this.Update_SaveChange(ob);
                            transaction.Commit();
                            return await Task.FromResult(true);
                        }
                        else
                        {
                            throw new NotImplementedException("Not find DMatCD " + item.DMatCD);
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new NotImplementedException(ex.Message.ToString());
                    }
                  }
              }        
        }
        public async Task<IList<TDMatStatu>> ERP_TDMatStatus_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {

                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT DMatStatusID,StatusID,DMatStatus,StatusDesc,Seq FROM dbo.TDMatStatus (NOLOCK)";
                    var data = await connection.QueryAsync<TDMatStatu>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<bool> ERP_Material_TDMat_Delete(TDMat item)
        {
            //return await this.Delete_SaveChange(item);
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                using (var transaction = this.Context.Database.BeginTransaction())
                {
                    try
                    {                  
                            this.Context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                            List<TMatGrpItem> tmatGrpItemsList = Context.TMatGrpItems.Where(x => x.DMatCD == item.DMatCD).ToList();
                            foreach (TMatGrpItem tmatGrpItems in tmatGrpItemsList)
                            {
                               this.Context.Entry(tmatGrpItems).State = System.Data.Entity.EntityState.Deleted;
                            }
                            Context.SaveChanges();
                            transaction.Commit();
                            return await Task.FromResult(true);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new NotImplementedException(ex.Message.ToString());
                    }
                }
            }
        }
        public async Task<bool> ERP_TDMat_ImportOtherDevMaterial(List<TDMat_Custom> itemlist)
        {
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                using (var transaction = this.Context.Database.BeginTransaction())
                {
                    try
                    {

                        //var queryParameters = new DynamicParameters();
                        //connection.Open();
                        //string query = "SELECT a.MatGrpID,a.DMatCD,a.SrcMatGrpID,b.MatGrpDesc FROM dbo.TMatGrpItem a (NOLOCK) " +
                        //    "INNER JOIN dbo.TMatGrp b (NOLOCK) ON a.MatGrpID=b.MatGrpID WHERE a.MatGrpID=" + itemlist.FirstOrDefault().MatGrpID + "";
                        //var data = await connection.QueryAsync<TDMat_Custom>(query
                        //    , queryParameters, commandTimeout: 1500
                        //    , commandType: CommandType.Text);

                        List<TMatGrpItem> listinsert = new List<TMatGrpItem>();
                        foreach (TDMat_Custom ob in itemlist)
                        {
                            //var checkdata = data.ToList().Where(x => x.DMatCD == ob.DMatCD).FirstOrDefault();
                            //if (checkdata!=null)
                            //{
                            //    throw new NotImplementedException("Exist DMatCD "+ checkdata.DMatCD + " in MatGrpDesc "+ checkdata.MatGrpDesc + "");
                            //}
                            TMatGrpItem item = new TMatGrpItem { DMatCD=ob.DMatCD,MatGrpID=ob.MatGrpID,SrcMatGrpID=ob.SrcMatGrpID,Flag=false};
                            listinsert.Add(item);
                        }
                        foreach (TMatGrpItem a in listinsert)
                        {
                            this.Context.Entry(a).State = System.Data.Entity.EntityState.Added;
                        }
                        await this.Context.SaveChangesAsync();
                        transaction.Commit();
                        return await Task.FromResult(true);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new NotImplementedException(ex.Message.ToString());
                    }
                }
            }
        }
        public async Task<bool> ERP_TDMat_CheckImportOtherDevMaterial(TDMat_Custom ob)
        {
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                using (var transaction = this.Context.Database.BeginTransaction())
                {
                    try
                    {

                        var queryParameters = new DynamicParameters();
                        connection.Open();
                        string query = "SELECT a.MatGrpID,a.DMatCD,a.SrcMatGrpID,b.MatGrpDesc FROM dbo.TMatGrpItem a (NOLOCK) " +
                            "INNER JOIN dbo.TMatGrp b (NOLOCK) ON a.MatGrpID=b.MatGrpID WHERE a.MatGrpID=" + ob.MatGrpID + "";
                        var data = await connection.QueryAsync<TDMat_Custom>(query
                            , queryParameters, commandTimeout: 1500
                            , commandType: CommandType.Text);

                        List<TMatGrpItem> listinsert = new List<TMatGrpItem>();
                        var checkdata = data.ToList().Where(x => x.DMatCD == ob.DMatCD).FirstOrDefault();
                        if (checkdata != null)
                        {
                            throw new NotImplementedException("Exist DMatCD " + checkdata.DMatCD + " in MatGrpDesc </br> " + checkdata.MatGrpDesc + "");
                        }
                        return await Task.FromResult(true);
                    }
                    catch (Exception ex)
                    {
                        throw new NotImplementedException(ex.Message.ToString());
                    }
                }
            }
        }
        public async Task<bool> ERP_TDMat_ImportProdTransferredMaterial(List<TDMat_Custom> itemlist)
        {
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                using (var transaction = this.Context.Database.BeginTransaction())
                {
                    try
                    {

                        //var queryParameters = new DynamicParameters();
                        //connection.Open();
                        //string query = "SELECT a.MatGrpID,a.DMatCD,a.SrcMatGrpID,b.MatGrpDesc,c.MatCD FROM dbo.TMatGrpItem a (NOLOCK) " +
                        //    "INNER JOIN dbo.TMatGrp b (NOLOCK) ON a.MatGrpID=b.MatGrpID " +
                        //    "INNER JOIN dbo.TDMat c (NOLOCK) ON a.DMatCD=c.DMatCD WHERE a.MatGrpID=" + itemlist.FirstOrDefault().MatGrpID + "";
                        //var data = await connection.QueryAsync<TDMat_Custom>(query
                        //    , queryParameters, commandTimeout: 1500
                        //    , commandType: CommandType.Text);

                        List<TMatGrpItem> listinsert = new List<TMatGrpItem>();
                        foreach (TDMat_Custom ob in itemlist)
                        {
                            //var checkdata = data.ToList().Where(x => x.DMatCD == ob.DMatCD).FirstOrDefault();
                            //if (checkdata != null)
                            //{
                            //    throw new NotImplementedException("Exist MatCD " + checkdata.MatCD + " in MatGrpDesc " + checkdata.MatGrpDesc + "");
                            //}
                            if (string.IsNullOrEmpty(ob.DMatCD))
                            {
                                throw new NotImplementedException("Not find DMatCD in MatCD " + ob.MatCD + "");
                            }
                            TMatGrpItem item = new TMatGrpItem { DMatCD = ob.DMatCD, MatGrpID = ob.MatGrpID, SrcMatGrpID = ob.SrcMatGrpID, Flag = false };
                            listinsert.Add(item);
                        }
                        foreach (TMatGrpItem a in listinsert)
                        {
                            this.Context.Entry(a).State = System.Data.Entity.EntityState.Added;
                        }
                        await this.Context.SaveChangesAsync();
                        transaction.Commit();
                        return await Task.FromResult(true);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new NotImplementedException(ex.Message.ToString());
                    }
                }
            }
        }
        public async Task<Result> ERP_TDMat_CheckImportProdTransferredMaterial(TDMat_Custom ob)
        {
            Result kq = new Result();
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                using (var transaction = this.Context.Database.BeginTransaction())
                {
                    try
                    {
                        var queryParameters = new DynamicParameters();
                        connection.Open();
                        string query = "SELECT a.MatGrpID,a.DMatCD,a.SrcMatGrpID,b.MatGrpDesc,c.MatCD FROM dbo.TMatGrpItem a (NOLOCK) " +
                            "INNER JOIN dbo.TMatGrp b (NOLOCK) ON a.MatGrpID=b.MatGrpID " +
                            "INNER JOIN dbo.TDMat c (NOLOCK) ON a.DMatCD=c.DMatCD WHERE c.MatCD='"+ ob.MatCD+ "' AND a.MatGrpID=" + ob.MatGrpID + "";
                        var data = await connection.QueryAsync<TDMat_Custom>(query
                            , queryParameters, commandTimeout: 1500
                            , commandType: CommandType.Text);
                        connection.Close();
                        if (data.ToList().Any())
                        {
                            var item = data.FirstOrDefault();
                            throw new NotImplementedException("Exist MatCD " + item.MatCD + " in MatGrpDesc </br>" + item.MatGrpDesc + "");
                        }


                        connection.Open();
                        string queryGetDMatCD = "SELECT MAX(DMatCD) DMatCD FROM dbo.TDMat a (NOLOCK) INNER JOIN dbo.TPInfo b (NOLOCK) ON a.MatCD=b.MatCD AND a.MfgVendor=b.MfgVendor AND a.Shipper=b.Shipper AND b.Discontinue=0 WHERE a.MatCD='"+ob.MatCD+"' ";
                        var DMatCD = await connection.QueryAsync<TDMat_Custom>(queryGetDMatCD
                            , queryParameters, commandTimeout: 1500
                            , commandType: CommandType.Text);
                        connection.Close();
                        if (!DMatCD.ToList().Any())
                        {

                            throw new NotImplementedException("Not Find DMatCD in MatCD " + ob.MatCD + ", because material NG");
                        }
                        if (string.IsNullOrEmpty(DMatCD.FirstOrDefault().DMatCD))
                        {
                            throw new NotImplementedException("Not Find DMatCD in MatCD " + ob.MatCD + ", because material NG");
                        }

                        kq.ErrorCode = 1;
                        kq.ErrorMessage = DMatCD.FirstOrDefault().DMatCD;
                        return kq;
                    }
                    catch (Exception ex)
                    {
                        throw new NotImplementedException(ex.Message.ToString());
                    }
                }
            }
        }
        public async Task<bool> ERP_FDMat_AddPressMaterial_InsertRedis(List<PressModel> item)
        {
            string userlogin = GetUserlogin();
            IDatabase db = ConnectRedis();
            try
            {
                string valued = await db.StringGetAsync("FDMat_AddPressMaterial_" + userlogin);
                if (!string.IsNullOrEmpty(valued))
                {
                    List<PressModel> InsChkTempByUser = JsonConvert.DeserializeObject<List<PressModel>>(valued);

                    var results = item.GroupBy(ob => ob.MatGrpID,
                                  (key, group) => new { MatGrpID = key })
                                  .ToList();
                    foreach (var ob in results)
                    {
                        InsChkTempByUser = InsChkTempByUser.Where(x => x.MatGrpID != ob.MatGrpID).ToList();
                    }

                    foreach (PressModel ob in item)
                    {
                        InsChkTempByUser.Add(ob);
                    }
                    string value = JsonConvert.SerializeObject(InsChkTempByUser);
                    await db.StringSetAsync("FDMat_AddPressMaterial_" + userlogin, value);
                }
                if (string.IsNullOrEmpty(valued))
                {
                    string value = JsonConvert.SerializeObject(item);
                    await db.StringSetAsync("FDMat_AddPressMaterial_" + userlogin, value);
                }
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                await db.KeyDeleteAsync("FDMat_AddPressMaterial_" + userlogin);
                throw new NotImplementedException(ex.ToString());
            }
        }

        public async Task<List<PressModel>> ERP_FDMat_AddPressMaterial_GetRedis(PressModel item)
        {
            string userlogin = GetUserlogin();
            IDatabase db = ConnectRedis();
            List<PressModel> InsChkTempByUser = new List<PressModel>();
            try
            {
                string valued = await db.StringGetAsync("FDMat_AddPressMaterial_" + userlogin);
                if (!string.IsNullOrEmpty(valued))
                {
                    InsChkTempByUser = JsonConvert.DeserializeObject<List<PressModel>>(valued);
                    InsChkTempByUser=InsChkTempByUser.Where(x=>x.MatGrpID==item.MatGrpID).ToList();
                }
                return InsChkTempByUser;
            }
            catch (Exception ex)
            {
                await db.KeyDeleteAsync("FDMat_AddPressMaterial_" + userlogin);
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<bool> ERP_FDMat_AddPressMaterial_DeleteRedisByUserlogin()
        {
            string userlogin = GetUserlogin();
            IDatabase db = ConnectRedis();
            List<PressModel> InsChkTempByUser = new List<PressModel>();
            try
            {
                await db.KeyDeleteAsync("FDMat_AddPressMaterial_" + userlogin);
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<bool> ERP_FDMat_AddPressMaterial_DeleteRedisByMatGrpID(PressModel item)
        {
            Library.TDMat_Repository dt = new Library.TDMat_Repository();
            return await dt.ERP_FDMat_AddPressMaterial_DeleteRedisByMatGrpID(item, this.GetUserlogin());
        }
        public async Task<IList<PressModel>> ERP_FDMat_AddPressMaterial_Insert(List<PressModel> dt)
        {
            Library.TDMat_Repository data = new Library.TDMat_Repository();
            return await data.ERP_FDMat_AddPressMaterial_Insert(dt, Settings.GetConnectionString(GetDB()), this.GetUserlogin());
        }
        public async Task<IList<Object>> ERP_TDMat_SearchingMaterial_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatDesc", item.MatDesc.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TDMat_SearchingMaterial_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TDMat_SearchingMaterial_GetPageSize_Excel(dynamic item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TDMat_SearchingMaterial_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@MatDesc", SqlDbType.NVarChar).Value = item.MatDesc.Value;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;


        }
    }
}
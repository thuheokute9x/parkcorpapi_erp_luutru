﻿using HR.API.IRepository;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models.ERP.TempModel.Development;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.DMat
{
    interface ITDMat_Repository : IGenericRepository<TDMat>
    {
        Task<IList<TDMat>> ERP_TDMat_ByDMatCD(string DMatCD);
        Task<IList<TDMat_Custom>> ERP_Material_TDMat(TDMat_Custom DMatCD);
        Task<IList<TDMat_Custom>> ERP_Material_TDMat_TMat(TDMat_Custom DMatCD);
        Task<TDMat_Custom> ERP_TDMat_GenerateDMatCD();
        Task<TDMat> InsertTDMat(TDMat_Custom item);
        Task<TDMat> UpdateTDMat(TDMat_Custom item);
        Task<bool> ERP_TDMat_Update_NA(TDMat item);
        Task<bool> ERP_TMatGrpItem_Update_Flag(TMatGrpItem item); 
        Task<TDMat_Custom> ERP_TDMat_GetItemByDMatCD(TDMat_Custom item);
        Task<IList<TDMatStatu>> ERP_TDMatStatus_GetAll();
        Task<bool> UpdateTDMat_MatCat_LossRate_Remark(TDMat_Custom item);
        DataTable ERP_Material_TDMat_Excel(TDMat_Custom item);
        DataTable ERP_Material_TDMat_TMat_Excel(TDMat_Custom item);
        Task<bool> ERP_Material_TDMat_Delete(TDMat item);
        Task<bool> ERP_TDMat_ImportOtherDevMaterial(List<TDMat_Custom> itemlist);
        Task<bool> ERP_TDMat_ImportProdTransferredMaterial(List<TDMat_Custom> itemlist);
        Task<IList<TDMat_Custom>> ERP_Material_TDMat_TMat_ImportProdTransferredMaterial(TDMat_Custom item);
        Task<bool> ERP_FDMat_AddPressMaterial_InsertRedis(List<PressModel> item);
        Task<List<PressModel>> ERP_FDMat_AddPressMaterial_GetRedis(PressModel item);
        Task<bool> ERP_FDMat_AddPressMaterial_DeleteRedisByUserlogin();
        Task<bool> ERP_FDMat_AddPressMaterial_DeleteRedisByMatGrpID(PressModel item);
        Task<IList<PressModel>> ERP_FDMat_AddPressMaterial_Insert(List<PressModel> dt);
        Task<bool> ERP_TDMat_CheckImportOtherDevMaterial(TDMat_Custom ob);
        Task<Result> ERP_TDMat_CheckImportProdTransferredMaterial(TDMat_Custom ob);
        Task<bool> ERP_TDMat_Update_NAList(List<TDMat> oblist);
        Task<bool> ERP_TDMat_Update_Matcat_Price_Spec_Color(TDMat itemlist);
        Task<IList<Object>> ERP_TDMat_SearchingMaterial_GetPageSize(dynamic item);
        DataTable ERP_TDMat_SearchingMaterial_GetPageSize_Excel(dynamic item);

    }
}

﻿using HR.API.IRepository;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.ConsItem
{
    interface ITConsItem_Repository : IGenericRepository<TConsItem>
    {
        Task<IList<TConsItem_Custom>> ERP_TConsItem_ConsWithPriceTPinfo_GR(TConsItem_Custom item);
        DataTable ERP_TConsItem_ConsWithPriceTPinfo_GR_Excel(TConsItem_Custom item);
        Task<IList<TConsItem_Custom>> ERP_TConsItem_ProdPriceMatCostCons_Pagesize(TConsItem_Custom item);
        DataTable ERP_TConsItem_ProdPriceMatCostCons_Pagesize_Excel(TConsItem_Custom item);
        Task<IList<TConsItem_Custom>> ERP_TPRItem_Amount_Report(TConsItem_Custom item);
        DataTable ERP_TPRItem_Amount_Report_Excel(TConsItem_Custom item);
        Task<Result> ERP_TConsItem_GenerateTPRItem(dynamic item);
        Task<Result> ERP_TConsItem_PreCheck(dynamic item);
        Task<List<Object>> ERP_TConsItem_PreviewCons(dynamic item);
        DataTable ERP_TConsItem_PreviewCons_Excel(dynamic item);
        Task<List<Object>> ERP_TConsItem_PreviewCons_GetHeader(dynamic item);
        Task<List<Object>> ERP_TConsItem_GetPageSize(dynamic item);
        DataTable ERP_TConsItem_GetPageSize_Excel(dynamic item);

        Task<List<Object>> ERP_TConsItem_Lectra_GetPageSize(dynamic item);
        DataTable ERP_TConsItem_Lectra_GetPageSize_Excel(dynamic item);
        Task<List<Object>> ERP_TConsLog_PageSize(dynamic item);
        DataTable ERP_TConsLog_PageSize_Excel(dynamic item);
    }
}

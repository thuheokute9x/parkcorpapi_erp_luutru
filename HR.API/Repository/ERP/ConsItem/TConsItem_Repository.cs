﻿using Dapper;

using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.ConsItem
{
    public class TConsItem_Repository : GenericRepository<TConsItem>, ITConsItem_Repository
    {
        public TConsItem_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TConsItem_Repository(PARKERPEntities context)
        : base(context)
        {
        }


        public async Task<IList<TConsItem_Custom>> ERP_TConsItem_ConsWithPriceTPinfo_GR(TConsItem_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdOrder);
                    queryParameters.Add("@ProdCode", item.ProdCode);
                    queryParameters.Add("@Outsourcing", item.Outsourcingstring);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TConsItem_Custom>("ERP_TConsItem_ConsWithPriceTPinfo_GR", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TConsItem_ConsWithPriceTPinfo_GR_Excel(TConsItem_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TConsItem_ConsWithPriceTPinfo_GR", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.ProdOrder;
                cmd.Parameters.Add("@ProdCode", SqlDbType.NVarChar).Value = item.ProdCode;
                cmd.Parameters.Add("@Outsourcing", SqlDbType.NVarChar).Value = item.Outsourcingstring;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TConsItem_Custom>> ERP_TConsItem_ProdPriceMatCostCons_Pagesize(TConsItem_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdOrder);
                    queryParameters.Add("@ProdCode", item.ProdCode);
                    queryParameters.Add("@BrandCD", item.BrandCD);
                    queryParameters.Add("@StyleCode", item.StyleCode);
                    queryParameters.Add("@ColorCode", item.ColorCode);
                    queryParameters.Add("@SKUCode", item.SKUCode);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TConsItem_Custom>("ERP_TConsItem_ProdPriceMatCostCons_Pagesize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TConsItem_ProdPriceMatCostCons_Pagesize_Excel(TConsItem_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TConsItem_ProdPriceMatCostCons_Pagesize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.ProdOrder;
                cmd.Parameters.Add("@ProdCode", SqlDbType.NVarChar).Value = item.ProdCode;
                cmd.Parameters.Add("@BrandCD", SqlDbType.NVarChar).Value = item.BrandCD;
                cmd.Parameters.Add("@StyleCode", SqlDbType.NVarChar).Value = item.StyleCode;
                cmd.Parameters.Add("@ColorCode", SqlDbType.NVarChar).Value = item.ColorCode;
                cmd.Parameters.Add("@SKUCode", SqlDbType.NVarChar).Value = item.SKUCode;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TConsItem_Custom>> ERP_TPRItem_Amount_Report(TConsItem_Custom item)
        {
            try
            {
                int fromyear= int.Parse(item.ProdOrderFrom.Substring(item.ProdOrderFrom.Length - 2, 2));
                int fromto = int.Parse(item.ProdOrderTo.Substring(item.ProdOrderTo.Length - 2, 2));
                if (fromyear> fromto)
                {
                    throw new NotImplementedException("ProdOrder Year From bigger ProdOrder Year To");
                }
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdOrderFrom", item.ProdOrderFrom);
                    queryParameters.Add("@ProdOrderTo", item.ProdOrderTo);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TConsItem_Custom>("ERP_TPRItem_Amount_Report", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TPRItem_Amount_Report_Excel(TConsItem_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TPRItem_Amount_Report", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProdOrderFrom", SqlDbType.NVarChar).Value = item.ProdOrderFrom;
                cmd.Parameters.Add("@ProdOrderTo", SqlDbType.NVarChar).Value = item.ProdOrderTo;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Result> ERP_TConsItem_GenerateTPRItem(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD.Value);
                    queryParameters.Add("@PRID", item.PRID.Value);
                    queryParameters.Add("@CreatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Result>("ERP_TConsItem_GenerateTPRItem", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Result> ERP_TConsItem_PreCheck(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Result>("ERP_TConsItem_PreCheck", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<List<Object>> ERP_TConsItem_PreviewCons(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD.Value);
                    queryParameters.Add("@PRID", item.PRID.Value);
                    queryParameters.Add("@POStatus", item.POStatus.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TConsItem_PreviewCons", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<List<Object>> ERP_TConsItem_PreviewCons_GetHeader(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TConsItem_PreviewCons_GetHeader", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TConsItem_PreviewCons_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TConsItem_PreviewCons", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.ProdInstCD.Value;
                cmd.Parameters.Add("@PRID", SqlDbType.Int).Value = item.PRID.Value;
                cmd.Parameters.Add("@POStatus", SqlDbType.Int).Value = item.POStatus.Value;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<List<Object>> ERP_TConsItem_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatGrpCode", item.MatGrpCode.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TConsItem_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TConsItem_GetPageSize_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TConsItem_GetPageSize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MatGrpCode", SqlDbType.NVarChar).Value = item.MatGrpCode.Value;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<List<Object>> ERP_TConsItem_Lectra_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Completed", item.Completed.Value);
                    queryParameters.Add("@Confirmationrequired", item.Confirmationrequired.Value);
                    queryParameters.Add("@NewBlank", item.NewBlank.Value);

                    queryParameters.Add("@MatGrpCode", item.MatGrpCode.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TConsItem_Lectra_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TConsItem_Lectra_GetPageSize_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TConsItem_Lectra_GetPageSize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MatGrpCode", SqlDbType.NVarChar).Value = item.MatGrpCode.Value;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<List<Object>> ERP_TConsLog_PageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TConsLog_PageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TConsLog_PageSize_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TConsLog_PageSize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
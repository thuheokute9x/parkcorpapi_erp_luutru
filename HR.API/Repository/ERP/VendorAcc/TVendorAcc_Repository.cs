﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.VendorAcc
{
    public class TVendorAcc_Repository : GenericRepository<TVendorAcc>, ITVendorAcc_Repository
    {
        public TVendorAcc_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TVendorAcc_Repository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<TVendorAcc>> ERP_TVendorAcc_GetByVendor(string VendorCD)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT VendorAccID,VendorCD,BankName,BankAddr,SwiftCode,AccountName,AccountNo,Remark, " +
                        "Discontinue,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn FROM dbo.TVendorAcc (NOLOCK) WHERE VendorCD='"+ VendorCD + "'";
                    var data = await connection.QueryAsync<TVendorAcc>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
    }
}
﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.VendorAcc
{
    interface ITVendorAcc_Repository  : IGenericRepository<TVendorAcc>
    {
        Task<IList<TVendorAcc>> ERP_TVendorAcc_GetByVendor(string VendorCD);
    }
}

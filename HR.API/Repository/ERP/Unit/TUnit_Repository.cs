﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Unit
{
    public class TUnit_Repository : GenericRepository<TUnit>, ITUnit_Repository
    {
        public TUnit_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TUnit_Repository(PARKERPEntities context)
        : base(context)
        {
        }
        public async Task<IList<TUnit>> ERP_TUnit_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT UnitCD,UnitDesc,ConvUnit,ConvFactor,ConvUnitGoods,ConvFactorGoods" +
                    ",ConvUnitCustoms,ConvFactorCustoms,UnitCat,BuiltInFlag FROM dbo.TUnit (NOLOCK) ORDER BY UnitCD";
                    connection.Open();
                    var data = await connection.QueryAsync<TUnit>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TUnit>> ERP_TUnit_ByConvUnit(TUnit item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT UnitCD,UnitDesc,ConvUnit,ConvFactor,ConvUnitGoods,ConvFactorGoods " +
                        "FROM dbo.TUnit (NOLOCK) WHERE UnitCD='" + item.ConvUnit + "'";
                    connection.Open();
                    var data = await connection.QueryAsync<TUnit>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    if (data.FirstOrDefault()!=null)
                    {
                        string Query2 = "SELECT UnitCD,UnitDesc,ConvUnit,ConvFactor,ConvUnitGoods,ConvFactorGoods " +
                            "FROM dbo.TUnit (NOLOCK) WHERE ConvUnit='" + data.FirstOrDefault().ConvUnit + "'";
                        var data2 = await connection.QueryAsync<TUnit>(Query2, queryParameters, commandTimeout: 1500
                            , commandType: CommandType.Text);

                        if (data2.Any())
                        {
                            IList<TUnit> ob = data2.ToList();
                            //ob.Add(data.FirstOrDefault());
                            return ob;
                        }
                    }

                  
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<object>> ERP_TTCDUnit_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT CDUnit UnitCCD,UnitDest FROM dbo.TTCDUnit (NOLOCK)";
                    connection.Open();
                    var data = await connection.QueryAsync<object>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<TTCDIIVM> ERP_TUnit_GetConvertCDUnit(TTCDIIVM item)
        {
            try
            {
                //if ()
                //{
                //    return item;
                //}
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    if (item.Unit== item.UnitCCD)
                    {
                       item.QtyCCD = item.Qty;
                       return item;
                    }
                    string Query = "SELECT ROUND(ISNULL(CONVERT(FLOAT,ConvFactorCustoms),1)*@Qty,2) QtyCCD,0 CDIIVMID FROM dbo.TUnit (NOLOCK) WHERE UnitCD=@UnitCD AND ConvUnitCustoms=@ConvUnitCustoms";
                    queryParameters.Add("@Qty", item.Qty);
                    queryParameters.Add("@UnitCD", item.Unit);
                    queryParameters.Add("@ConvUnitCustoms", item.UnitCCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TTCDIIVM>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    TTCDIIVM ob = data.FirstOrDefault();
                    if (ob==null)
                    {
                        TTCDIIVM dt = new TTCDIIVM();
                        dt.CDIIVMID = 0;
                        dt.QtyCCD = null;
                        return dt;
                    }

                    
                    return ob;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
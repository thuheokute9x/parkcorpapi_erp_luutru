﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Unit
{
    interface ITUnit_Repository : IGenericRepository<TUnit>
    {
        Task<IList<TUnit>> ERP_TUnit_GetAll();
        Task<IList<TUnit>> ERP_TUnit_ByConvUnit(TUnit item);
        Task<IList<object>> ERP_TTCDUnit_GetAll();
        Task<TTCDIIVM> ERP_TUnit_GetConvertCDUnit(TTCDIIVM item);
    }
}

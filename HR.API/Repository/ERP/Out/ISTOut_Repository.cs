﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Out
{
    interface ISTOut_Repository
    {
        Task<IList<STOut_List_Custom>> ERP_STOut_List_ByOUTCD(STOut_List STIn);
        Task<IList<STOut>> ERP_STOut_GetOUTCD(STOut STIn);
        Task<IList<STVKGrp>> ERP_STVKGrp_GetVKGRPCD(STVKGrp item);
        Task<IList<STOut_List_Custom>> ERP_STOut_List_GetGIPageSize(STOut_List_Custom item);
        DataTable ERP_STOut_List_GetGIPageSize_Excel(STOut_List_Custom item);
        Task<bool> ERP_STOut_List_AdjustStock(dynamic item);
        Task<Boolean> ERP_STOut_CheckAdjustStock(dynamic item);
    }
}

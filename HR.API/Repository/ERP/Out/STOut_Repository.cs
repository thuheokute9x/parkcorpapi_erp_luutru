﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Out
{
    public class STOut_Repository : GenericRepository<STOut>, ISTOut_Repository
    {
        public STOut_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public STOut_Repository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<STOut_List_Custom>> ERP_STOut_List_ByOUTCD(STOut_List STIn)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "SELECT OUTCD,VKGRPCD,FactCD,PRID,TPRID,PLANTCD,DMATCD,MATCD,PUNIT, " +
                    "OQTY,RCD,TYPE,EMPNO,REVID,CANCLE,ResvQTY,REMARK FROM dbo.STOut_List (NOLOCK) WHERE OUTCD='"+ STIn.OUTCD + "'";
                    connection.Open();
                    var data = await connection.QueryAsync<STOut_List_Custom>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<STOut>> ERP_STOut_GetOUTCD(STOut STIn)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "SELECT TOP(15) OUTCD FROM dbo.STOut (NOLOCK) WHERE OUTCD LIKE '%" + STIn.OUTCD + "%' OR " +
                        "'" + STIn.OUTCD + "'='' ORDER BY ODATE DESC";
                    connection.Open();
                    var data = await connection.QueryAsync<STOut>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<STVKGrp>> ERP_STVKGrp_GetVKGRPCD(STVKGrp item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@VKGRPCD", item.VKGRPCD);
                    string query = "SELECT TOP 15 VKGRPCD FROM dbo.STVKGrp (NOLOCK) WHERE VKGRPCD LIKE N'%'+ @VKGRPCD +'%' OR @VKGRPCD=''";
                    connection.Open();
                    var data = await connection.QueryAsync<STVKGrp>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<STOut_List_Custom>> ERP_STOut_List_GetGIPageSize(STOut_List_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@FromDate", item.FromDate);
                    queryParameters.Add("@ToDate", item.ToDate);
                    queryParameters.Add("@OUTCD", item.OUTCD);
                    queryParameters.Add("@ProdOrder", item.ProdOrder);
                    queryParameters.Add("@VKGRPCD", item.VKGRPCD);
                    queryParameters.Add("@MATCD", item.MATCD);
                    queryParameters.Add("@VendorCD", item.VendorCD);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@Type", item.Type);
                    connection.Open();
                    var data = await connection.QueryAsync<STOut_List_Custom>("ERP_STOut_List_GetGIPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_STOut_List_GetGIPageSize_Excel(STOut_List_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_STOut_List_GetGIPageSize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = item.FromDate;
                cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = item.ToDate;
                cmd.Parameters.Add("@OUTCD", SqlDbType.NVarChar).Value = item.OUTCD;
                cmd.Parameters.Add("@ProdOrder", SqlDbType.NVarChar).Value = item.ProdOrder;
                cmd.Parameters.Add("@VKGRPCD", SqlDbType.NVarChar).Value = item.VKGRPCD;
                cmd.Parameters.Add("@MATCD", SqlDbType.NVarChar).Value = item.MATCD;
                cmd.Parameters.Add("@VendorCD", SqlDbType.NVarChar).Value = item.VendorCD;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type;

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_STOut_List_AdjustStock(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParametersCheckSTBasicStock = new DynamicParameters();
                    queryParametersCheckSTBasicStock.Add("@CloseYear", int.Parse(item.Year.Value));
                    queryParametersCheckSTBasicStock.Add("@CloseMonth", int.Parse(item.Month.Value));
                    connection.Open();
                    STBasicStock dataCheckSTBasicStock = await connection.QueryFirstOrDefaultAsync<STBasicStock>("SELECT TOP 1 * FROM dbo.STBasicStock (NOLOCK) WHERE CloseYear=@CloseYear AND CloseMonth=@CloseMonth"
                        , queryParametersCheckSTBasicStock, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    if (dataCheckSTBasicStock==null)
                    {
                        throw new NotImplementedException("Please Transfer Data Closing Year " + item.Year.Value + " and Closing Month " + item.Month.Value + "");
                    }
                    
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Month", item.Month.Value);
                    queryParameters.Add("@Year", item.Year.Value);
                    queryParameters.Add("@CreatedBy", int.Parse(GetUserlogin()));
                    var data = await connection.QueryAsync<STIn_List_Custom>("ERP_STOut_List_AdjustStock"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_STOut_CheckAdjustStock(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParametersCheckSTBasicStock = new DynamicParameters();
                    queryParametersCheckSTBasicStock.Add("@Year", item.Year.Value);
                    queryParametersCheckSTBasicStock.Add("@Month", item.Month.Value);
                    connection.Open();
                    STOut dataCheckSTOut = await connection.QueryFirstOrDefaultAsync<STOut>("ERP_STOut_CheckAdjustStock"
                        , queryParametersCheckSTBasicStock, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    if (dataCheckSTOut!=null)
                    {
                        return await Task.FromResult(true);
                    }
                    return await Task.FromResult(false);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
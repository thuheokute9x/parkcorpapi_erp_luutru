﻿using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Repository.ERP.ExRateDaily
{
    public class TTExRateDaily_Repository : GenericRepository<TTExRateDaily>, ITTExRateDaily_Repository
    {
        public TTExRateDaily_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TTExRateDaily_Repository(PARKERPEntities context)
            : base(context)
        {
        }
    }
}
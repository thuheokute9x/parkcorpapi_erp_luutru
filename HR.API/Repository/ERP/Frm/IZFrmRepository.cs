﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Frm
{
    interface IZFrmRepository : IGenericRepository<ZFrm>
    {
        Task<IList<ZFrm>> ERP_ZFrm_GetWithType(ZFrm item);
        Task<IList<ZFrm_Custom>> ERP_ZFrm_GetPageSize(ZFrm_Custom item);
        Task<IList<ZFrm_Custom>> ERP_ZFrm_GetWithB();

    }
}

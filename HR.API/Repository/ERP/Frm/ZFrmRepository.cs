﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Frm
{
    public class ZFrmRepository : GenericRepository<ZFrm>, IZFrmRepository
    {
        public ZFrmRepository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public ZFrmRepository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<ZFrm_Custom>> ERP_ZFrm_GetPageSize(ZFrm_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@FormName", item.FormName);
                    //queryParameters.Add("@TypeForm", item.Type);
                    queryParameters.Add("@Type", item.TypeForm);
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    connection.Open();
                    var data = await connection.QueryAsync<ZFrm_Custom>("ERP_ZFrm_Get", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<ZFrm_Custom>> ERP_ZFrm_GetWithB()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = @"SELECT FormName,FCap1,FCap2,FCap3,B,TopForm,SetPerm,DefaultView,Seq,  
                        hParent FROM dbo.ZFrm (NOLOCK) WHERE B=1 ORDER BY Seq";
                    var data = await connection.QueryAsync<ZFrm_Custom>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<ZFrm>> ERP_ZFrm_GetWithType(ZFrm item)
        {

            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "";
                    //if (item.Type.Contains("Form"))
                    //{
                    //    query = "SELECT  FormName FROM dbo.ZFrm (NOLOCK) WHERE Type='Form' AND ViewAll=0";
                    //}
                    //if (item.Type.Contains("Button"))
                    //{
                    //    query = "SELECT  FormName FROM dbo.ZFrm (NOLOCK) WHERE Type='Button' AND ParentFormName='"+ item.ParentFormName + "'";
                    //}
                    //if (string.IsNullOrEmpty(item.Type))//Get All
                    //{
                    //    query = "SELECT FormName,FCap1,FCap2,FCap3,B,TopForm,SetPerm,DefaultView,Seq,hParent,ViewAll,Type,ParentFormName " +
                    //            "FROM dbo.ZFrm (NOLOCK)";
                    //}
                    var data = await connection.QueryAsync<ZFrm>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
            
        }
    }
}
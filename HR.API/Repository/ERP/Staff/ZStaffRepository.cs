﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.PayDoc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Staff
{
    public class ZStaffRepository : GenericRepository<ZStaff>, IZStaffRepository
    {
        public ZStaffRepository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public ZStaffRepository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<bool> ERP_ZStaff_UpdatePassWord(ZStaff_Custom item)
        {
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                string Query = "SELECT StaffID,EmpID,ID,PWD,IsAdmin,LangID,Discontinue,CreatedBy,CreatedOn," +
                    "UpdatedBy,UpdatedOn FROM dbo.ZStaff (NOLOCK) WHERE EmpID=" + item.EmpID + " AND Discontinue=0";
                var queryParameters = new DynamicParameters();
                connection.Open();
                var data = await connection.QueryAsync<ZStaff>(Query
                    , queryParameters, commandTimeout: 1500
                    , commandType: CommandType.Text);
                if (data.ToList()==null || data.Count()==0)
                {
                    throw new NotImplementedException("Not Find EmpID " + item.EmpID.ToString() + "");
                }

                if (data.FirstOrDefault().PWD != item.PassWordOld)
                {
                    throw new NotImplementedException("Incorrect password.Please type again.");
                }

                //string QueryUpdate = "UPDATE ZStaff SET PWD="+ item.PWD + ",UpdatedBy="+ item.UpdatedBy + ",UpdatedOn=GETDATE() WHERE StaffID=" + data.FirstOrDefault().StaffID + "";
                //var queryUpdate = new DynamicParameters();
                //connection.Open();
                //var Update = await connection.QueryAsync<ZStaff>(QueryUpdate
                //    , queryUpdate, commandTimeout: 1500
                //    , commandType: CommandType.Text);

                ZStaff ob = new ZStaff();
                ob = data.FirstOrDefault();
                ob.PWD = item.PWD;
                ob.UpdatedBy = item.UpdatedBy;
                ob.UpdatedOn = DateTime.Now;
                return await this.Update_SaveChange(ob);
            }
        }
        public async Task<IList<ZStaff_Custom>> ERP_ZStaff_GetByID_EmpName_EmpNo(ZStaff_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@EmpID", item.EmpID);
                    queryParameters.Add("@StaffID", item.StaffID);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<ZStaff_Custom>("ERP_ZStaff_GetByID_EmpName_EmpNo"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<TXLAutho_Custom>> ERP_TXLAutho_GetCostSheet(TXLAutho_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@StaffID", item.StaffID);
                    queryParameters.Add("@EmpID", item.EmpID);
                    string query = "SELECT a.XLAuthoID,b.EmpName, b.EmpNo, b.Dept, a.StaffID, a.EmpID, a.Authority FROM dbo.TXLAutho a (NOLOCK) INNER JOIN dbo.TEmp b (NOLOCK) ON a.EmpID = b.EmpID WHERE  (a.StaffID=@StaffID OR @StaffID=0) AND (a.EmpID=@EmpID OR @EmpID=0) ORDER BY a.XLAuthoID";
                    connection.Open();
                    var data = await connection.QueryAsync<TXLAutho_Custom>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
    }
}
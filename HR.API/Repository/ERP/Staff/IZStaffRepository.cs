﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Staff
{
    interface IZStaffRepository : IGenericRepository<ZStaff>
    {
        Task<Boolean> ERP_ZStaff_UpdatePassWord(ZStaff_Custom item);
        Task<IList<ZStaff_Custom>> ERP_ZStaff_GetByID_EmpName_EmpNo(ZStaff_Custom item);
        Task<IList<TXLAutho_Custom>> ERP_TXLAutho_GetCostSheet(TXLAutho_Custom item);
    }
}

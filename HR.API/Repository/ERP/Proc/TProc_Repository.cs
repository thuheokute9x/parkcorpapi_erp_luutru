﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Proc
{
    public class TProc_Repository : GenericRepository<TProc>, ITProc_Repository
    {
        public TProc_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TProc_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TProc>> ERP_TProc_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT ProcCD,ProcCatCD,ProcDesc,Seq,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn FROM dbo.TProc (NOLOCK)";
                    connection.Open();
                    var data = await connection.QueryAsync<TProc>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

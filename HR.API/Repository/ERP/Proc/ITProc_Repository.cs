﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Proc
{
    interface ITProc_Repository : IGenericRepository<TProc>
    {
        Task<IList<TProc>> ERP_TProc_GetAll();
    }
}

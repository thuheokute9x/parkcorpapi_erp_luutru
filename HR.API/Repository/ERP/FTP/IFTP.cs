﻿using HR.API.Models.ERP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.FTP
{
    public interface IFTP
    {
        Task<IList<FTPIVModel>> ERP_TVendor_GetwithVendorCDLike(string Gender);
    }
}

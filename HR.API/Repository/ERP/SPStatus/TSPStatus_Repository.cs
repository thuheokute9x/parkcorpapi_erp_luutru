﻿using Dapper;
using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.SPStatus
{
    public class TSPStatus_Repository : GenericRepository<TSPStatu>, ITSPStatus_Repository
    {
        public TSPStatus_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TSPStatus_Repository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<TSPStatu_Custom>> ERP_TSPStatu_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    //queryParameters.Add("@PayDocNo", string.IsNullOrEmpty(TSPStatu.PayDocNo) ? "" : TSPStatu.PayDocNo);
                    string query = "SELECT  a.SPStatusID,a.StatusID,b.Status,a.SPStatus,a.SPStatusDesc,a.Seq,a.CreatedBy,a.CreatedOn,a.UpdatedBy, " +
                        "a.UpdatedOn FROM dbo.TSPStatus a (NOLOCK) INNER JOIN dbo.ZStatus b (NOLOCK) ON a.StatusID=b.StatusID ORDER BY SPStatusID";
                    connection.Open();
                    var data = await connection.QueryAsync<TSPStatu_Custom>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
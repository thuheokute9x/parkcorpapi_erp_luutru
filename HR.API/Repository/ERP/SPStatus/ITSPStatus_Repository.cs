﻿using Dapper;
using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.SPStatus
{
    interface ITSPStatus_Repository : IGenericRepository<TSPStatu>
    {
        Task<IList<TSPStatu_Custom>> ERP_TSPStatu_GetAll();
    }
}

﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Country
{
    interface ITCountry_Repository : IGenericRepository<TCountry>
    {
        Task<IList<TCountry>> ERP_TCountry_GetAll();
    }
}

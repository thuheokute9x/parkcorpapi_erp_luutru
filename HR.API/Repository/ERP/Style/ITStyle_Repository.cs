﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Style
{
    interface ITStyle_Repository : IGenericRepository<TStyle>
    {
        Task<IList<TStyle_Custom>> ERP_TStyle_GetPageSize(TStyle_Custom Gender);
        DataTable ERP_TStyle_GetPageSize_Excel(TStyle_Custom item);
        Task<IList<TStyle>> ERP_TStyle_GetStyleDesc(TStyle item);
    }
}

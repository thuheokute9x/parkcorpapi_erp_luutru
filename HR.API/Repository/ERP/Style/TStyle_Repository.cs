﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Style
{
    public class TStyle_Repository : GenericRepository<TStyle>, ITStyle_Repository
    {
        public TStyle_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TStyle_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TStyle_Custom>> ERP_TStyle_GetPageSize(TStyle_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@StyleDesc", item.StyleDesc);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@Type", item.Type);
                    connection.Open();
                    var data = await connection.QueryAsync<TStyle_Custom>("ERP_TStyle_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TStyle_GetPageSize_Excel(TStyle_Custom item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TStyle_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@StyleDesc", SqlDbType.NVarChar).Value = item.StyleDesc;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;


            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;
        }

        public async Task<IList<TStyle>> ERP_TStyle_GetStyleDesc(TStyle item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT TOP 15 StyleDesc FROM dbo.TStyle (NOLOCK) WHERE StyleDesc LIKE  N'%" + item.StyleDesc + "%'  AND StyleDesc IS NOT NULL " +
                        "GROUP BY StyleDesc";
                    connection.Open();
                    var data = await connection.QueryAsync<TStyle>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
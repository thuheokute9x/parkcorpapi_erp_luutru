﻿using Dapper;

using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using SelectPdf;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;




namespace HR.API.Repository.ERP.MatPOItem
{
    public class TMatPOItem_Repository : GenericRepository<TMatPOItem>, ITMatPOItem_Repository
    {
        public TMatPOItem_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TMatPOItem_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TMatPOItem>> ERP_TMatPOItem_GetMatPOCDLike(TMatPOItem_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatPOCD", item.MatPOCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TMatPOItem>("SELECT TOP 15 MatPOCD FROM dbo.TMatPO (NOLOCK) WHERE MatPOCD LIKE N'%'+@MatPOCD+'%' OR  @MatPOCD=''"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPOItem_GetMatPOCDLikeWithVK(TMatPOItem_Custom item)//
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatPOCD", item.MatPOCD);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(@"SELECT TOP 15 a.MatPOCD,b.RefVKNo  
                    FROM dbo.TMatPO (NOLOCK) a
                    LEFT JOIN dbo.TPR (NOLOCK) b ON b.PRID = a.PRID
                    WHERE a.MatPOCD LIKE N'%'+@MatPOCD+'%' OR  @MatPOCD=''"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }

        public async Task<IList<Object>> ERP_TMatPO_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Issued", item.Issued.Value);
                    queryParameters.Add("@Canceled", item.Canceled.Value);
                    queryParameters.Add("@ManualPO", item.ManualPO.Value);
                    queryParameters.Add("@AutoPO", item.AutoPO.Value);
                    queryParameters.Add("@Closed", item.Closed.Value);
                    queryParameters.Add("@MatPOCD", item.MatPOCD.Value);
                    queryParameters.Add("@PGrpCD", item.PGrpCD.Value);
                    queryParameters.Add("@IncotermsCD", item.IncotermsCD.Value);
                    queryParameters.Add("@ProdInstCD", item.ProdOrder.Value);

                    if (item.ReqETDFrom.Value!="null")
                    {
                        queryParameters.Add("@ReqETDFrom", item.ReqETDFrom.Value);
                        queryParameters.Add("@ReqETDTo", item.ReqETDTo.Value);
                    }

                    queryParameters.Add("@Remark", item.Remark.Value);
                    queryParameters.Add("@PRID", item.PRID.Value);

                    queryParameters.Add("@Range", item.Range.Value);
                    if (item.PostingDateFrom.Value!= "null")
                    {
                        queryParameters.Add("@PostingDateFrom", item.PostingDateFrom.Value);
                        queryParameters.Add("@PostingDateTo", item.PostingDateTo.Value);
                    }

                    queryParameters.Add("@Shipper", item.Shipper.Value);
                    queryParameters.Add("@Currency", item.CurrCD.Value);
                    if (item.CreatedOnFrom.Value!="null")
                    {
                        queryParameters.Add("@CreatedOnFrom", item.CreatedOnFrom.Value);
                        queryParameters.Add("@CreatedOnTo", item.CreatedOnTo.Value);
                    }

                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    queryParameters.Add("@CompanyCD", GetDB());
                    connection.Open();


                    var data = await connection.QueryAsync<Object>("ERP_TMatPO_GetPageSize"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
               
                    return data.ToList();
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TMatPO_GetPageSize_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TMatPO_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;



            cmd.Parameters.Add("@Issued", SqlDbType.Bit).Value = item.Issued.Value;
            cmd.Parameters.Add("@Canceled", SqlDbType.Bit).Value = item.Canceled.Value;
            cmd.Parameters.Add("@ManualPO", SqlDbType.Bit).Value = item.ManualPO.Value;
            cmd.Parameters.Add("@AutoPO", SqlDbType.Bit).Value = item.AutoPO.Value;
            cmd.Parameters.Add("@Closed", SqlDbType.Bit).Value = item.Closed.Value;
            cmd.Parameters.Add("@MatPOCD", SqlDbType.NVarChar).Value = item.MatPOCD.Value;
            cmd.Parameters.Add("@PGrpCD", SqlDbType.NVarChar).Value = item.PGrpCD.Value;
            cmd.Parameters.Add("@IncotermsCD", SqlDbType.NVarChar).Value = item.IncotermsCD.Value;
            cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.ProdOrder.Value;

            if (item.ReqETDFrom.Value != "null")
            {
                cmd.Parameters.Add("@ReqETDFrom", SqlDbType.DateTime).Value = item.ReqETDFrom.Value;
                cmd.Parameters.Add("@ReqETDTo", SqlDbType.DateTime).Value = item.ReqETDTo.Value;
            }

            cmd.Parameters.Add("@Remark", SqlDbType.NVarChar).Value = item.Remark.Value;
            cmd.Parameters.Add("@PRID", SqlDbType.Int).Value = item.PRID.Value;
            cmd.Parameters.Add("@Range", SqlDbType.NVarChar).Value = item.Range.Value;

            if (item.PostingDateFrom.Value != "null")
            {
                cmd.Parameters.Add("@PostingDateFrom", SqlDbType.DateTime).Value = item.PostingDateFrom.Value;
                cmd.Parameters.Add("@PostingDateTo", SqlDbType.DateTime).Value = item.PostingDateTo.Value;
            }

            cmd.Parameters.Add("@Shipper", SqlDbType.NVarChar).Value = item.Shipper.Value;
            cmd.Parameters.Add("@Currency", SqlDbType.NVarChar).Value = item.CurrCD.Value;
            if (item.CreatedOnFrom.Value != "null")
            {
                cmd.Parameters.Add("@CreatedOnFrom", SqlDbType.DateTime).Value = item.CreatedOnFrom.Value;
                cmd.Parameters.Add("@CreatedOnTo", SqlDbType.DateTime).Value = item.CreatedOnTo.Value;
            }

            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            cmd.Parameters.Add("@CompanyCD", SqlDbType.NVarChar).Value = GetDB();
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }

        public async Task<IList<Object>> ERP_TMatPO_ExtPayMethod_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("SELECT 1 IsUpdate,PaymentMethod PaymentMethodTemp,PaymentMethod FROM TMatPO_ExtPayMethod (NOLOCK)"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    IList<Object> kq= data.ToList();

                    kq.Add(new { PaymentMethodTemp="", PaymentMethod = "", IsUpdate = 0 });
                    return kq;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPO_ExtPayMethod_Insert(TMatPO_ExtPayMethod item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    context.TMatPO_ExtPayMethod.Add(item);
                    await context.SaveChangesAsync();
                    return await this.ERP_TMatPO_ExtPayMethod_GetAll();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPO_ExtPayMethod_Update(dynamic item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    string PaymentMethodTemp = item.PaymentMethodTemp.Value;
                    TMatPO_ExtPayMethod entity= context.TMatPO_ExtPayMethod.FirstOrDefault(x=>x.PaymentMethod== PaymentMethodTemp);
                    if (entity!=null)
                    {
                        context.TMatPO_ExtPayMethod.Remove(entity);
                        await context.SaveChangesAsync();
                        entity.PaymentMethod= item.PaymentMethod.Value;
                        context.TMatPO_ExtPayMethod.Add(entity);
                        await context.SaveChangesAsync();

                    }
                    return await this.ERP_TMatPO_ExtPayMethod_GetAll();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPO_ExtPayMethod_Delete(dynamic item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    string PaymentMethodTemp = item.PaymentMethodTemp.Value;
                    TMatPO_ExtPayMethod entity = context.TMatPO_ExtPayMethod.FirstOrDefault(x => x.PaymentMethod == PaymentMethodTemp);
                    if (entity != null)
                    {
                        context.TMatPO_ExtPayMethod.Remove(entity);
                        await context.SaveChangesAsync();
                    }
                    return await this.ERP_TMatPO_ExtPayMethod_GetAll();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("SELECT 1 IsUpdate,DeliverTo DeliverToTemp,DeliverTo FROM dbo.TMatPO_ExtDeliverTo (NOLOCK)"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    IList<Object> kq = data.ToList();
                    kq.Add(new { DeliverToTemp = "", DeliverTo = "", IsUpdate = 0 });
                    return kq;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_Insert(TMatPO_ExtDeliverTo item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    context.TMatPO_ExtDeliverTo.Add(item);
                    await context.SaveChangesAsync();
                    return await this.ERP_TMatPO_ExtDeliverTo_GetAll();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_Update(dynamic item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    string DeliverToTemp = item.DeliverToTemp.Value;
                    TMatPO_ExtDeliverTo entity = context.TMatPO_ExtDeliverTo.FirstOrDefault(x => x.DeliverTo == DeliverToTemp);
                    if (entity != null)
                    {
                        context.TMatPO_ExtDeliverTo.Remove(entity);
                        await context.SaveChangesAsync();
                        entity.DeliverTo = item.DeliverTo.Value;
                        context.TMatPO_ExtDeliverTo.Add(entity);
                        await context.SaveChangesAsync();

                    }
                    return await this.ERP_TMatPO_ExtPayMethod_GetAll();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_Delete(dynamic item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    string DeliverToTemp = item.DeliverToTemp.Value;
                    TMatPO_ExtDeliverTo entity = context.TMatPO_ExtDeliverTo.FirstOrDefault(x => x.DeliverTo == DeliverToTemp);
                    if (entity != null)
                    {
                        context.TMatPO_ExtDeliverTo.Remove(entity);
                        await context.SaveChangesAsync();
                    }
                    return await this.ERP_TMatPO_ExtDeliverTo_GetAll();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }




        public async Task<IList<Object>> ERP_TMatPO_AttentionTo_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("SELECT 1 IsUpdate,AttentionTo AttentionToTemp,AttentionTo FROM dbo.TMatPO_AttentionTo (NOLOCK)"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    IList<Object> kq = data.ToList();
                    kq.Add(new { AttentionToTemp = "", AttentionTo = "", IsUpdate = 0 });
                    return kq;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPO_AttentionTo_Insert(TMatPO_AttentionTo item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    context.TMatPO_AttentionTo.Add(item);
                    await context.SaveChangesAsync();
                    return await this.ERP_TMatPO_AttentionTo_GetAll();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPO_AttentionTo_Update(dynamic item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    string AttentionToTemp = item.AttentionToTemp.Value;
                    string AttentionTo = item.AttentionTo.Value;
                    TMatPO_AttentionTo entity = context.TMatPO_AttentionTo.FirstOrDefault(x => x.AttentionTo == AttentionToTemp);
                    if (entity != null)
                    {
                        context.TMatPO_AttentionTo.Remove(entity);
                        await context.SaveChangesAsync();
                        entity.AttentionTo = item.DeliverTo.Value;
                        context.TMatPO_AttentionTo.Add(entity);
                        await context.SaveChangesAsync();

                    }
                    return await this.ERP_TMatPO_AttentionTo_GetAll();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPO_AttentionTo_Delete(dynamic item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    string AttentionToTemp = item.AttentionToTemp.Value;
                    TMatPO_AttentionTo entity = context.TMatPO_AttentionTo.FirstOrDefault(x => x.AttentionTo == AttentionToTemp);
                    if (entity != null)
                    {
                        context.TMatPO_AttentionTo.Remove(entity);
                        await context.SaveChangesAsync();
                    }
                    return await this.ERP_TMatPO_AttentionTo_GetAll();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }





        public async Task<bool> ERP_TMatPOItem_InsertList(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCDList", item.MatCDList.Value);
                    queryParameters.Add("@Shipper", item.Shipper.Value);
                    queryParameters.Add("@MatPOCD", item.MatPOCD.Value);
                    queryParameters.Add("@CreatedBy", GetUserlogin());
                    connection.Open();

                    var data = await connection.QueryAsync<Object>("ERP_TMatPOItem_InsertList"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);

                    return await Task.FromResult(true);
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<Object>> ERP_TMatPOItem_GetByMatPOCD(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCDList", item.MatCDList.Value);
                    queryParameters.Add("@MatPOCD", item.MatPOCD.Value);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();

                    var data = await connection.QueryAsync<Object>("ERP_TMatPOItem_GetByMatPOCD"//Sử dụng 2 chổ
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);

                    return data.ToList();
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TMatPOItem_GetByMatPOCD_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TMatPOItem_GetByMatPOCD", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@MatPOCD", SqlDbType.NVarChar).Value = item.MatPOCD.Value;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }
        public async Task<IList<Object>> ERP_TMatPOItem_UpdatePOQty(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatPOItemID", item.MatPOItemID.Value);
                    queryParameters.Add("@POQty", item.POQty.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("UPDATE TMatPOItem SET POQty=ROUND(@POQty,0),BenefitQty=ROUND(@POQty,0),ConsolQty=ROUND(@POQty,0),UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE MatPOItemID=@MatPOItemID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    item.MatCDList = "";
                    return await this.ERP_TMatPOItem_GetByMatPOCD(item);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Boolean> ERP_TMatPOItem_UpdatePUnit(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatPOItemID", item.MatPOItemID.Value);
                    queryParameters.Add("@PUnit", item.PUnit.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("UPDATE TMatPOItem SET PUnit=@PUnit,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE MatPOItemID=@MatPOItemID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Boolean> ERP_TMatPOItem_UpdateRemark(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatPOItemID", item.MatPOItemID.Value);
                    queryParameters.Add("@Remark", item.Remark.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("UPDATE TMatPOItem SET Remark=@Remark,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE MatPOItemID=@MatPOItemID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<TMatPO> ERP_TMatPO_GenerateMatPOCD()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    TMatPO data = await connection.QueryFirstOrDefaultAsync<TMatPO>("ERP_TMatPO_GenerateMatPOCD", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Boolean> ERP_TMatPO_UpdateRemark(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatPOItemID", item.MatPOItemID.Value);
                    queryParameters.Add("@Remark", item.Remark.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("UPDATE TMatPOItem SET Remark=@Remark,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE MatPOItemID=@MatPOItemID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public Byte[] ERP_TMatPO_IssueSelectedPO(TMatPO_Custom item)
        {
            Library.TMatPOItem_Repository dt = new Library.TMatPOItem_Repository();
            return dt.ERP_TMatPO_IssueSelectedPO(item, Settings.GetConnectionString(GetDB()), GetUserlogin(), GetDB());
        }
        public async Task<TMatPO_Custom> ERP_TMatPOItem_GeneratePOWithSelected(dynamic param)
        {
            IDatabase db = ConnectRedis();
            string key = GetDB() + "_" + GetUserlogin() + "_TPRItem_Radis_Sel" + param.PRID.Value;
            string valued = await db.StringGetAsync(key);
            Library.TMatPOItem_Repository dt = new Library.TMatPOItem_Repository();
            return await dt.ERP_TMatPOItem_GeneratePOWithSelected(param, Settings.GetConnectionString(GetDB()),
                GetDB() + "_" + GetUserlogin() + "_TPRItem_Radis_Sel" + param.PRID.Value
                , Context, GetUserlogin(), valued);

        }
        public async Task<bool> ERP_TMatPO_Insert(TMatPO item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    TMatPO parent = await this.ERP_TMatPO_GenerateMatPOCD();//lấy lại MatPOCD mới nhất cho k trùng key
                    item.MatPOCD = parent.MatPOCD;
                    item.Manual = true;
                    context.TMatPOes.Add(item);
                    await context.SaveChangesAsync();
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<bool> ERP_TMatPO_Update(TMatPO item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    TMatPO ob=context.TMatPOes.FirstOrDefault(x=>x.MatPOCD== item.MatPOCD);

                    if (ob!=null)
                    {
                        if (item.PostingDate==null&& item.ReqETD < DateTime.Today)
                        {
                            throw new NotImplementedException("ReqETD is(are) not available.");
                        }
                        ob.PrevMatPOCD = item.PrevMatPOCD;
                        ob.RefVKNo = item.RefVKNo;
                        ob.MatPOStatusID = item.MatPOStatusID;
                        ob.StatusID = item.StatusID;
                        //ob.PostingDate = item.PostingDate;
                        ob.PRID = item.PRID;
                        ob.Shipper = item.Shipper;
                        ob.MfgVendor = item.MfgVendor;
                        ob.PGrpCD = item.PGrpCD;
                        ob.ReqETD = item.ReqETD;
                        ob.RangeID = item.RangeID;
                        ob.Range = item.Range;
                        ob.Currency = item.Currency;
                        ob.IncotermsCD = item.IncotermsCD;
                        ob.AttentionTo = item.AttentionTo;
                        ob.ExtPayMethod = item.ExtPayMethod;
                        ob.ExtPLNo = item.ExtPLNo;
                        ob.ExtDeliverTo = item.ExtDeliverTo;
                        ob.Remark = item.Remark;
                        ob.Notice = item.Notice;
                        //ob.Manual = item.Manual;
                        ob.ReasonWhyManual = item.ReasonWhyManual;
                        ob.Closed = item.Closed;
                        ob.Canceled = item.Canceled;
                        ob.CanceledOn = item.CanceledOn;
                        ob.UpdatedBy = int.Parse(GetUserlogin());
                        ob.UpdatedOn = DateTime.Now;
                        ob.PINo = item.PINo;
                        ob.PIDate = item.PIDate;
                        ob.PIETD = item.PIETD;
                        ob.PayMthCD = item.PayMthCD;
                        ob.PayCondID = item.PayCondID;
                        ob.PayTerm = item.PayTerm;
                        ob.PayDocCD = item.PayDocCD;
                        ob.AccountNo = item.AccountNo;
                        ob.PIFileID = item.PIFileID;
                        ob.PayCfm = item.PayCfm;
                        ob.PayCfmDate = item.PayCfmDate;
                        ob.PICreatedBy = item.PICreatedBy;
                        ob.PICreatedOn = item.PICreatedOn;
                        ob.PIUpdatedBy = item.PIUpdatedBy;
                        ob.PIUpdatedOn = item.PIUpdatedOn;
                    }
                    await context.SaveChangesAsync();
                    if (!ob.Manual)
                    {
                        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                        {
                            var queryParameters = new DynamicParameters();
                            queryParameters.Add("@PRID", ob.PRID);
                            queryParameters.Add("@CreatedBy", GetUserlogin());
                            connection.Open();
                            var data = await connection.QueryAsync<Object>("ERP_TPRItem_RefreshPOInfo"
                                , queryParameters, commandTimeout: 1500
                                , commandType: CommandType.StoredProcedure);
                        }
                    }

                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<Object>> ERP_TMatPOItem_GetManualTrue(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PRID", item.PRID.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();

                    var data = await connection.QueryAsync<Object>("ERP_TMatPOItem_GetManualTrue"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TMatPOItem_GetManualTrue_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TMatPOItem_GetManualTrue", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@PRID", SqlDbType.NVarChar).Value = item.PRID.Value;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }       
            return table;

        }
        public async Task<IList<Object>> ERP_TMatPOItem_GetManualTrueByMatCD(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCD", item.MatCD.Value);
                    queryParameters.Add("@PRID", item.PRID.Value);

                    connection.Open();

                    var data = await connection.QueryAsync<Object>("ERP_TMatPOItem_GetManualTrueByMatCD"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_TMatPOItem_Delete(TMatPOItem item)
        {
            try
            {
                return await this.Delete_SaveChange(item);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<Result> ERP_TMatPO_CheckTMatPOItemExist(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatPOCD", item.MatPOCD.Value);
                    connection.Open();

                    TMatPOItem data = await connection.QueryFirstOrDefaultAsync<TMatPOItem>("SELECT COUNT(MatCD) MatPOItemID FROM dbo.TMatPOItem (NOLOCK) WHERE MatPOCD=@MatPOCD  HAVING COUNT(MatCD) > 0"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    Result dt = new Result();
                    if (data==null)
                    {
                        dt.ErrorCode = 1;//có lỗi
                    }
                    if (data != null)
                    {
                        dt.ErrorCode = 0;
                    }
                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_Report__TMatPOItem_SQMatPOItem(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdOrder", item.ProdOrder.Value);
                    queryParameters.Add("@PGrpCD", item.PGrpCD.Value);
                    queryParameters.Add("@VenDorCD", item.VenDorCD.Value);
                    queryParameters.Add("@Shipper", item.Shipper.Value);
                    queryParameters.Add("@MatPOCD", item.MatPOCD.Value);
                    queryParameters.Add("@MatCD", item.MatCD.Value);
                    queryParameters.Add("@MatDesc", item.MatDesc.Value);
                    queryParameters.Add("@Issued", item.Issued.Value);
                    queryParameters.Add("@Manual", item.Manual.Value);
                    queryParameters.Add("@Cancelled", item.Cancelled.Value);

                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();


                    var data = await connection.QueryAsync<Object>("ERP_Report__TMatPOItem_SQMatPOItem"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);

                    return data.ToList();
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_Report__TMatPOItem_SQMatPOItem_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_Report__TMatPOItem_SQMatPOItem", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ProdOrder", SqlDbType.NVarChar).Value = item.ProdOrder.Value;
            cmd.Parameters.Add("@PGrpCD", SqlDbType.NVarChar).Value = item.PGrpCD.Value;
            cmd.Parameters.Add("@VenDorCD", SqlDbType.NVarChar).Value = item.VenDorCD.Value;
            cmd.Parameters.Add("@Shipper", SqlDbType.NVarChar).Value = item.Shipper.Value;
            cmd.Parameters.Add("@MatPOCD", SqlDbType.NVarChar).Value = item.MatPOCD.Value;
            cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD.Value;
            cmd.Parameters.Add("@MatDesc", SqlDbType.NVarChar).Value = item.MatDesc.Value;
            cmd.Parameters.Add("@Issued", SqlDbType.NVarChar).Value = item.Issued.Value;
            cmd.Parameters.Add("@Manual", SqlDbType.NVarChar).Value = item.Manual.Value;
            cmd.Parameters.Add("@Cancelled", SqlDbType.NVarChar).Value = item.Cancelled.Value;

            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }
        public async Task<IList<Object>> ERP_Report_TMatPOItem_MatPOItemBalanceList(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdOrder", item.ProdOrder.Value);
                    queryParameters.Add("@VenDorCD", item.VenDorCD.Value);

                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();


                    var data = await connection.QueryAsync<Object>("ERP_Report_TMatPOItem_MatPOItemBalanceList"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);

                    return data.ToList();
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_Report_TMatPOItem_MatPOItemBalanceList_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_Report_TMatPOItem_MatPOItemBalanceList", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ProdOrder", SqlDbType.NVarChar).Value = item.ProdOrder.Value;
            cmd.Parameters.Add("@VenDorCD", SqlDbType.NVarChar).Value = item.VenDorCD.Value;

            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }
        public async Task<bool> ERP_TMatPO_UpdateRangeReqETDAttentionTo(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    if (item.ReqETD.Value != "null")
                    {
                        DateTime fromdate;
                        fromdate = DateTime.ParseExact(item.ReqETD.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                        queryParameters.Add("@ReqETD", fromdate);
                        if (fromdate < DateTime.Today)
                        {
                            throw new NotImplementedException("ReqETD is(are) not available.");
                        }
                    }
                    if (item.ReqETD.Value == "null")
                    {
                        DateTime fromdate;
                        fromdate = DateTime.ParseExact("1900-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                        queryParameters.Add("@ReqETD", fromdate);
                    }
                    queryParameters.Add("@MatPOCD", item.MatPOCD.Value);
                    queryParameters.Add("@Range", item.Range.Value);
                    queryParameters.Add("@AttentionTo", item.AttentionTo.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(@"UPDATE dbo.TMatPO SET 
ReqETD=@ReqETD,Range=@Range,AttentionTo=@AttentionTo,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE MatPOCD=@MatPOCD"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
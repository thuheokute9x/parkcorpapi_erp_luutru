﻿using HR.API.IRepository;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.MatPOItem
{
    interface ITMatPOItem_Repository : IGenericRepository<TMatPOItem>
    {
        Task<IList<TMatPOItem>> ERP_TMatPOItem_GetMatPOCDLike(TMatPOItem_Custom item);
        Task<IList<Object>> ERP_TMatPO_GetPageSize(dynamic item);
        DataTable ERP_TMatPO_GetPageSize_Excel(dynamic item);
        Task<IList<Object>> ERP_TMatPO_ExtPayMethod_GetAll();
        Task<IList<Object>> ERP_TMatPO_ExtPayMethod_Insert(TMatPO_ExtPayMethod item);
        Task<IList<Object>> ERP_TMatPO_ExtPayMethod_Update(dynamic item);
        Task<IList<Object>> ERP_TMatPO_ExtPayMethod_Delete(dynamic item);
        Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_GetAll();
        Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_Insert(TMatPO_ExtDeliverTo item);
        Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_Update(dynamic item);
        Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_Delete(dynamic item);
        Task<IList<Object>> ERP_TMatPO_AttentionTo_GetAll();
        Task<IList<Object>> ERP_TMatPO_AttentionTo_Insert(TMatPO_AttentionTo item);
        Task<IList<Object>> ERP_TMatPO_AttentionTo_Update(dynamic item);
        Task<IList<Object>> ERP_TMatPO_AttentionTo_Delete(dynamic item);
        Task<IList<Object>> ERP_TMatPOItem_GetByMatPOCD(dynamic item);
        DataTable ERP_TMatPOItem_GetByMatPOCD_Excel(dynamic item);
        Task<bool> ERP_TMatPOItem_InsertList(dynamic item);
        Task<TMatPO> ERP_TMatPO_GenerateMatPOCD();
        Task<IList<Object>> ERP_TMatPOItem_UpdatePOQty(dynamic item);
        Task<Boolean> ERP_TMatPOItem_UpdatePUnit(dynamic item);
        Task<Boolean> ERP_TMatPOItem_UpdateRemark(dynamic item);
        Byte[] ERP_TMatPO_IssueSelectedPO(TMatPO_Custom item);
        Task<TMatPO_Custom> ERP_TMatPOItem_GeneratePOWithSelected(dynamic item);
        Task<Boolean> ERP_TMatPO_Insert(TMatPO item);
        Task<Boolean> ERP_TMatPO_Update(TMatPO item);
        Task<IList<Object>> ERP_TMatPOItem_GetManualTrue(dynamic item);
        DataTable ERP_TMatPOItem_GetManualTrue_Excel(dynamic item);
        Task<IList<Object>> ERP_TMatPOItem_GetManualTrueByMatCD(dynamic item);
        Task<Boolean> ERP_TMatPOItem_Delete(TMatPOItem item);
        Task<Result> ERP_TMatPO_CheckTMatPOItemExist(dynamic item);
        Task<IList<Object>> ERP_Report__TMatPOItem_SQMatPOItem(dynamic item);
        DataTable ERP_Report__TMatPOItem_SQMatPOItem_Excel(dynamic item);
        Task<IList<Object>> ERP_Report_TMatPOItem_MatPOItemBalanceList(dynamic item);
        DataTable ERP_Report_TMatPOItem_MatPOItemBalanceList_Excel(dynamic item);
        Task<IList<Object>> ERP_TMatPOItem_GetMatPOCDLikeWithVK(TMatPOItem_Custom item);
        Task<bool> ERP_TMatPO_UpdateRangeReqETDAttentionTo(dynamic item);
    }
}

﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.VKGrp
{
    interface ISTVKGrp_Repository : IGenericRepository<STVKGrp>
    {
        Task<IList<Object>> ERP_STVKGrp_GetPageSize(dynamic item);
        DataTable ERP_STVKGrp_GetPageSize_Excel(dynamic item);
        Task<IList<Object>> ERP_STVKGrp_Item_GetPageSize(dynamic item);
        DataTable ERP_STVKGrp_Item_GetPageSize_Excel(dynamic item);
        Task<IList<Object>> ERP_STVKGrp_Cons_GetPageSize(dynamic item);
        DataTable ERP_STVKGrp_Cons_GetPageSize_Excel(dynamic item);
        Task<IList<Object>> ERP_SQVKGrp_Srch_List_PageSize(dynamic item);
        Task<Boolean> ERP_SQVKGrp_Srch_List_InserRadis(List<TProd_Custom> item);
        Task<IList<TProd_Custom>> ERP_STVKGrp_Item_FromRediscache_GetPageSize(dynamic item);
    }
}

﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.VKGrp
{
    public class STVKGrp_Repository : GenericRepository<STVKGrp>, ISTVKGrp_Repository
    {
        public STVKGrp_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public STVKGrp_Repository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<IList<Object>> ERP_STVKGrp_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();

                    queryParameters.Add("@VKGRPCD", item.VKGRPCD.Value);
                    queryParameters.Add("@RefVKNo", item.RefVKNo.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_STVKGrp_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_STVKGrp_GetPageSize_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_STVKGrp_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }
        public async Task<IList<Object>> ERP_STVKGrp_Item_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@VKGrpID", item.VKGrpID.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_STVKGrp_Item_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_STVKGrp_Item_GetPageSize_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_STVKGrp_Item_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@VKGrpID", SqlDbType.Int).Value = item.VKGrpID.Value;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }
        public async Task<IList<Object>> ERP_STVKGrp_Cons_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@VKGrpID", item.VKGrpID.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_STVKGrp_Cons_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_STVKGrp_Cons_GetPageSize_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_STVKGrp_Cons_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@VKGrpID", SqlDbType.Int).Value = item.VKGrpID.Value;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }
        public async Task<IList<Object>> ERP_SQVKGrp_Srch_List_PageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@VKGRPCD", item.VKGRPCD.Value);
                    queryParameters.Add("@RefVKNo", item.RefVKNo.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_SQVKGrp_Srch_List_PageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_SQVKGrp_Srch_List_PageSize_Excel(dynamic item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_SQVKGrp_Srch_List_PageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@VKGRPCD", SqlDbType.NVarChar).Value = item.VKGRPCD.Value;
            cmd.Parameters.Add("@RefVKNo", SqlDbType.NVarChar).Value = item.RefVKNo.Value;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }
        public async Task<Boolean> ERP_SQVKGrp_Srch_List_InserRadis(List<TProd_Custom> item)
        {
            try
            {
                IDatabase db = ConnectRedis();
                string key = GetDB() + "_" + GetUserlogin() + "_SQVKGrp_Srch_List";
                string valued = db.StringGet(key);
                if (string.IsNullOrEmpty(valued))
                {
                    List<TProd_Custom> dt = new List<TProd_Custom>();
                    item.ForEach(ob =>
                    {
                        dt.Add(ob);
                    });
                    string json = JsonConvert.SerializeObject(dt);
                    db.StringSet(GetDB() + "_" + GetUserlogin() + "_SQVKGrp_Srch_List", json);
                }
                if (!string.IsNullOrEmpty(valued))
                {
                    List<TProd_Custom> dt = JsonConvert.DeserializeObject<List<TProd_Custom>>(valued);
                    item.ForEach(ob =>
                    {
                        dt.Add(ob);
                    });
                    string json = JsonConvert.SerializeObject(dt);
                    db.StringSet(GetDB() + "_" + GetUserlogin() + "_SQVKGrp_Srch_List", json);
                }
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Boolean> ERP_SQVKGrp_Srch_List_(List<TProd_Custom> item)
        {
            try
            {
                IDatabase db = ConnectRedis();
                string key = GetDB() + "_" + GetUserlogin() + "_SQVKGrp_Srch_List";
                string valued = db.StringGet(key);
                if (string.IsNullOrEmpty(valued))
                {
                    List<TProd_Custom> dt = new List<TProd_Custom>();
                    item.ForEach(ob =>
                    {
                        dt.Add(ob);
                    });
                    string json = JsonConvert.SerializeObject(dt);
                    db.StringSet(GetDB() + "_" + GetUserlogin() + "_SQVKGrp_Srch_List", json);
                }
                if (!string.IsNullOrEmpty(valued))
                {
                    List<TProd_Custom> dt = JsonConvert.DeserializeObject<List<TProd_Custom>>(valued);
                    item.ForEach(ob =>
                    {
                        dt.Add(ob);
                    });
                    string json = JsonConvert.SerializeObject(dt);
                    db.StringSet(GetDB() + "_" + GetUserlogin() + "_SQVKGrp_Srch_List", json);
                }
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TProd_Custom>> ERP_STVKGrp_Item_FromRediscache_GetPageSize(dynamic item)
        {
            try
            {
                IDatabase db = ConnectRedis();
                List<TProd_Custom> dt=null;

                string key = GetDB() + "_" + GetUserlogin() + "_SQVKGrp_Srch_List";
                string valued =await db.StringGetAsync(key);
                if (!string.IsNullOrEmpty(valued))
                {
                    string VKGRPCD = item.VKGRPCD;
                    List<TProd_Custom> ob = JsonConvert.DeserializeObject<List<TProd_Custom>>(valued);
                    dt=ob.Where(x => x.VKGRPCD == VKGRPCD).ToList();
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
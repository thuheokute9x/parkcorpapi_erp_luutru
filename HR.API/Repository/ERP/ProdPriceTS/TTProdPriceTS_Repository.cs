﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.ProdPriceTS
{
    //public class TTProdPriceTS_Repository : GenericRepository<TTProdPriceT>, ITTProdPriceTS_Repository
    //{
    //    public TTProdPriceTS_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
    //    : base(unitOfWork)
    //    {
    //    }
    //    public TTProdPriceTS_Repository(PARKERPEntities context)
    //    : base(context)
    //    {
    //    }
    //    public async Task<IList<Object>> ERP_TTProdPriceTS_GetByProdInstCD(dynamic item)
    //    {
    //        try
    //        {
    //            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
    //            {
    //                var queryParameters = new DynamicParameters();
    //                queryParameters.Add("@FACTORY", this.GetDB());
    //                queryParameters.Add("@Type", 0);
    //                queryParameters.Add("@ProdInstCD", item.ProdInstCD.Value);
    //                queryParameters.Add("@PageNumber", item.PageNumber.Value);
    //                queryParameters.Add("@PageSize", item.PageSize.Value);

    //                connection.Open();
    //                var data = await connection.QueryAsync<Object>("ERP_TTProdPriceTS_GetByProdInstCD", queryParameters, commandTimeout: 1500
    //                    , commandType: CommandType.StoredProcedure);
    //                return data.ToList();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw new NotImplementedException(ex.Message.ToString());
    //        }
    //    }
    //    public DataTable ERP_TTProdPriceTS_GetByProdInstCD_Excel(dynamic item)
    //    {
    //        try
    //        {
    //            DataTable table = new DataTable();
    //            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
    //            SqlCommand cmd = new SqlCommand("ERP_TTProdPriceTS_GetByProdInstCD", con);
    //            cmd.CommandTimeout = 1500;
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            cmd.Parameters.Add("@FACTORY", SqlDbType.NVarChar).Value = this.GetDB();
    //            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
    //            cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.ProdInstCD.Value;


    //            using (var da = new SqlDataAdapter(cmd))
    //            {
    //                cmd.CommandType = CommandType.StoredProcedure;
    //                da.Fill(table);
    //            }
    //            return table;


    //        }
    //        catch (Exception ex)
    //        {
    //            throw new NotImplementedException(ex.Message.ToString());
    //        }
    //    }
    //    public async Task<bool> ERP_TTProdPriceTS_DeleteInsertList(List<TTProdPriceT> item)
    //    {
    //        try
    //        {
    //            using (PARKERPEntities context = new PARKERPEntities())
    //            {
    //                using (var transaction = context.Database.BeginTransaction())
    //                {
    //                    try
    //                    {
    //                        List <TTProdPriceT> deleteList = (from a in context.TTProdPriceTS.ToList()
    //                                      join b in item
    //                                      on a.SalseInfoID equals b.SalseInfoID
    //                                      select a).ToList();

    //                        //List<TTProdPriceT> deleteList=context.TTProdPriceTS.Where(x => item.All(y=> x.SalseInfoID==y.SalseInfoID)).Select(new TTProdPriceT {SalseInfoID= });
    //                        context.TTProdPriceTS.RemoveRange(deleteList);
    //                        this.BulkInsertNotSaveChange(context, item);
    //                        await context.SaveChangesAsync();
    //                        transaction.Commit();
    //                        return await Task.FromResult(true);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        transaction.Rollback();
    //                        throw new NotImplementedException(ex.Message.ToString());
    //                    }
    //                }
    //            }

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new NotImplementedException(ex.Message.ToString());
    //        }
    //    }
    //    public async Task<IList<Object>> ERP_TTProdPriceTS_GetReport(dynamic item)
    //    {
    //        try
    //        {
    //            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
    //            {
    //                var queryParameters = new DynamicParameters();
    //                if (item.PlanYearDate.Value!="null")
    //                {
    //                    queryParameters.Add("@PlanYear", item.PlanYearDate.Value);
    //                }
    //                if (item.PlanMonthDate.Value!="null")
    //                {
    //                    queryParameters.Add("@PlanMonth", item.PlanMonthDate.Value);
    //                }
    //                queryParameters.Add("@CustGrpCD", item.CustGrpCD.Value);
    //                queryParameters.Add("@ProdCode", item.ProdCode.Value);
    //                queryParameters.Add("@SKUCode", item.SKUCode.Value);
    //                queryParameters.Add("@FACTORY", this.GetDB());
    //                queryParameters.Add("@Type", 0);
    //                queryParameters.Add("@ProdInstCD", item.ProdInstCD.Value);
    //                queryParameters.Add("@PageNumber", item.PageNumber.Value);
    //                queryParameters.Add("@PageSize", item.PageSize.Value);
    //                connection.Open();
    //                var data = await connection.QueryAsync<Object>("ERP_TTProdPriceTS_GetReport", queryParameters, commandTimeout: 1500
    //                    , commandType: CommandType.StoredProcedure);
    //                return data.ToList();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw new NotImplementedException(ex.Message.ToString());
    //        }
    //    }
    //    public DataTable ERP_TTProdPriceTS_GetReport_Excel(dynamic item)
    //    {
    //        try
    //        {
    //            DataTable table = new DataTable();
    //            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
    //            SqlCommand cmd = new SqlCommand("ERP_TTProdPriceTS_GetReport", con);
    //            cmd.CommandTimeout = 1500;
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            if (item.PlanYearDate.Value != "null")
    //            {
    //                cmd.Parameters.Add("@PlanYear", SqlDbType.DateTime).Value = item.PlanYear.Value;
    //            }
    //            if (item.PlanMonthDate.Value != "null")
    //            {
    //                cmd.Parameters.Add("@PlanMonth", SqlDbType.DateTime).Value = item.PlanMonth.Value;
    //            }
    //            cmd.Parameters.Add("@CustGrpCD", SqlDbType.NVarChar).Value = item.CustGrpCD.Value;
    //            cmd.Parameters.Add("@ProdCode", SqlDbType.NVarChar).Value = item.ProdCode.Value;
    //            cmd.Parameters.Add("@SKUCode", SqlDbType.NVarChar).Value = item.SKUCode.Value;
    //            cmd.Parameters.Add("@FACTORY", SqlDbType.NVarChar).Value =  this.GetDB();
    //            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
    //            cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.ProdInstCD.Value;
    //            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
    //            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;

    //            using (var da = new SqlDataAdapter(cmd))
    //            {
    //                cmd.CommandType = CommandType.StoredProcedure;
    //                da.Fill(table);
    //            }
    //            return table;


    //        }
    //        catch (Exception ex)
    //        {
    //            throw new NotImplementedException(ex.Message.ToString());
    //        }
    //    }
    //    public async Task<IList<TTSMG>> ERP_TTSMG_GetByCustGrpCD(dynamic item)
    //    {
    //        try
    //        {
    //            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
    //            {
    //                var queryParameters = new DynamicParameters();
    //                //queryParameters.Add("@FACTORY", this.GetDB());
    //                connection.Open();
    //                var data = await connection.QueryAsync<TTSMG>("SELECT * FROM dbo.TTSMG (NOLOCK)", queryParameters, commandTimeout: 1500
    //                    , commandType: CommandType.Text);
    //                return data.ToList();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw new NotImplementedException(ex.Message.ToString());
    //        }
    //    }
    //    public async Task<bool> ERP_TTProdPriceTS_UpdateSMG(dynamic item)
    //    {
    //        try
    //        {
    //            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
    //            {
    //                string query = "UPDATE dbo.TTProdPriceTS SET SMG=@SMG,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE SalseInfoID=@SalseInfoID";
    //                var queryParameters = new DynamicParameters();
    //                queryParameters.Add("@SMG", item.SMG.Value);
    //                queryParameters.Add("@UpdatedBy", GetUserlogin());
    //                queryParameters.Add("@SalseInfoID", item.SalseInfoID.Value);
    //                connection.Open();
    //                var data = await connection.QueryAsync<TTSMG>(query, queryParameters, commandTimeout: 1500
    //                    , commandType: CommandType.Text);
    //                return await Task.FromResult(true);
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw new NotImplementedException(ex.Message.ToString());
    //        }
    //    }
    //}
}
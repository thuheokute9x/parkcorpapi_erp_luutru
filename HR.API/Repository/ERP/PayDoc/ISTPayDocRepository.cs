﻿using HR.API.IRepository;
using HR.API.Models;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.PayDoc
{
    interface ISTPayDocRepository : IGenericRepository<STPayDoc>
    {
        Task<IList<STPayDoc_Custom>> ERP_STPayDoc_Get(STPayDoc_Custom STPayDoc);
        Task<bool> ERP_STPayDoc_CheckInserFromGR(STPayDoc_Custom STPayDoc);
        Task<bool> ERP_STPayDoc_CheckInserFromPayDoc(STPayDoc_Custom STPayDoc);
        Task<List<STPayDoc>> ERP_STPayDoc_InsertList(List<STPayDoc> sTPayDoclist);
    }
}

﻿using Dapper;

using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.PayDoc
{
    public class STPayDocRepository : GenericRepository<STPayDoc>, ISTPayDocRepository
    {
        public STPayDocRepository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public STPayDocRepository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<bool> ERP_STPayDoc_CheckInserFromGR(STPayDoc_Custom STPayDoc)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "";
                    if (!string.IsNullOrEmpty(STPayDoc.PayDocVendorCD))
                    {
                        query = "SELECT PayDocID FROM dbo.STPayDoc (NOLOCK) " +
                        "WHERE PayDocCD='IV' AND PayDocNo=N'" + STPayDoc.PayDocNo + "' AND PayDocVendorCD=N'" + STPayDoc.PayDocVendorCD + "'";
                    }
                    if (string.IsNullOrEmpty(STPayDoc.PayDocVendorCD))
                    {
                        query = "SELECT PayDocID FROM dbo.STPayDoc (NOLOCK) " +
                        "WHERE PayDocCD='IV' AND PayDocNo=N'" + STPayDoc.PayDocNo + "' AND PayDocVendorCD IS NULL";
                    }
                    var data = await connection.QueryAsync<STPayDoc_Custom>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    if (data.ToList().Count() > 0)
                    {
                        return await Task.FromResult(true);
                    }
                    else
                        return await Task.FromResult(false);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_STPayDoc_CheckInserFromPayDoc(STPayDoc_Custom STPayDoc)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    //string query = "SELECT PayDocID FROM dbo.STPayDoc(NOLOCK)" +
                    //    " WHERE (PayDocCD='IV' AND IVDocNo='" + STPayDoc.IVDocNo + "' AND PayDocVendorCD='" + STPayDoc.PayDocVendorCD + "')" +
                    //    " OR (PayDocCD='PI' AND PIDocNo='" + STPayDoc.PIDocNo + "' AND PayDocVendorCD='" + STPayDoc.PayDocVendorCD + "')";

                    string query = "SELECT PayDocID FROM dbo.STPayDoc(NOLOCK)" +
                    " WHERE (PayDocCD='IV' AND PayDocNo='" + STPayDoc.PayDocNo + "')" +
                    " OR (PayDocCD='PI' AND PayDocNo='" + STPayDoc.PayDocNo + "')";
                    var data = await connection.QueryAsync<STPayDoc_Custom>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    if (data.ToList().Count() > 0)
                    {
                        return await Task.FromResult(true);
                    }
                    else
                        return await Task.FromResult(false);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }

        public async Task<IList<STPayDoc_Custom>> ERP_STPayDoc_Get(STPayDoc_Custom PayDoc)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PayDocVendorCD", string.IsNullOrEmpty(PayDoc.PayDocVendorCD) || PayDoc.PayDocVendorCD == "All" ? "" : PayDoc.PayDocVendorCD.Trim());
                    queryParameters.Add("@PayDocCD", string.IsNullOrEmpty(PayDoc.PayDocCD) ? "" : PayDoc.PayDocCD.Trim());
                    queryParameters.Add("@PayDocNo", string.IsNullOrEmpty(PayDoc.PayDocNo) ? "" : PayDoc.PayDocNo);
                    //queryParameters.Add("@IVDocNo", string.IsNullOrEmpty(PayDoc.IVDocNo) ? "" : PayDoc.IVDocNo);
                    queryParameters.Add("@PayType", string.IsNullOrEmpty(PayDoc.PayType) ? "" : PayDoc.PayType.Trim());
                    if (PayDoc.HasPayDoc == "True")
                    {
                        queryParameters.Add("@HasDoc", 1, DbType.Boolean);
                    }
                    if (PayDoc.HasPayDoc == "False")
                    {
                        queryParameters.Add("@HasDoc", 0, DbType.Boolean);
                    }
                    if (PayDoc.HasPayDoc == "None")
                    {
                        queryParameters.Add("@HasDoc", null, DbType.Boolean);
                    }
                    queryParameters.Add("@PayDocIDList", PayDoc.PayDocIDList);
                    queryParameters.Add("@PageNumber", PayDoc.PageNumber);
                    queryParameters.Add("@PageSize", PayDoc.PageSize);
                    queryParameters.Add("@Type", PayDoc.Type);
                    connection.Open();
                    var data = await connection.QueryAsync<STPayDoc_Custom>("ERP_STPayDoc_Get", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<List<STPayDoc>> ERP_STPayDoc_InsertList(List<STPayDoc> sTPayDoclist)
        {
            try
            {
                foreach (STPayDoc item in sTPayDoclist)
                {
                    item.CreatedOn = DateTime.Now;
                }
                return await this.BulkInsert(sTPayDoclist);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
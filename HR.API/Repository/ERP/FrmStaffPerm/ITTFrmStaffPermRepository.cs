﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.FrmStaffPerm
{
    interface ITTFrmStaffPermRepository : IGenericRepository<TTFrmStaffPerm>
    {
        Task<IList<TTFrmStaffPerm_Custom>> ERP_TTFrmStaffPerm_InsertList(List<TTFrmStaffPerm> itemlist);
        Task<IList<TTFrmStaffPerm_Custom>> ERP_TTFrmStaffPerm_Get(TTFrmStaffPerm_Custom item);
        Task<bool> ERP_TTFrmStaffPerm_Delete(TTFrmStaffPerm item);
        Task<bool> ERP_TTFrmStaffPerm_Update(TTFrmStaffPerm item);
        Task<IList<TTMenuWebERP_Custom>> ERT_TTMenuWebERP_GetHierachy(TTMenuWebERP item);
        Task<IList<TTFrmStaffPerm_Custom>> ERT_TTMenuWebERP_GetHierachyByStaffID(TTMenuWebERP_Custom item);
        Task<Boolean> ERP_TTFrmStaffPerm_CopyPer(TTFrmStaffPerm_Custom item);
        Task<Boolean> ERT_TTFrmStaffPerm_DeleteByStaffIDMenuID(TTFrmStaffPerm_Custom item);
    }
}

﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.FrmStaffPerm
{
    public class TTFrmStaffPermRepository : GenericRepository<TTFrmStaffPerm>, ITTFrmStaffPermRepository
    {
        public TTFrmStaffPermRepository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TTFrmStaffPermRepository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<TTFrmStaffPerm_Custom>> ERP_TTFrmStaffPerm_Get(TTFrmStaffPerm_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters(); 
                    queryParameters.Add("@StaffID", item.StaffID);
                    queryParameters.Add("@EmpName", item.EmpName);
                    queryParameters.Add("@EmpID", item.EmpID);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TTFrmStaffPerm_Custom>("ERP_TTFrmStaffPerm_Get"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<TTFrmStaffPerm_Custom>> ERP_TTFrmStaffPerm_InsertList(List<TTFrmStaffPerm> TTFrmStaffPermList)
        {
            try
            {
                List <TTFrmStaffPerm_Custom> CheckExist = new List<TTFrmStaffPerm_Custom>();

                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();

                    foreach (TTFrmStaffPerm item in TTFrmStaffPermList)
                    {
                        string query = "DELETE dbo.TTFrmStaffPerm OUTPUT b.FormNameDesc,d.EmpName FROM TTFrmStaffPerm a INNER JOIN dbo.TTMenuWebERP b(NOLOCK) on a.FormName = b.FormName INNER JOIN dbo.ZStaff c(NOLOCK) on c.StaffID = " + item.StaffID + " INNER JOIN dbo.TEmp d(NOLOCK) on c.EmpID = d.EmpID WHERE a.StaffID = " + item.StaffID+" AND a.FormName = '"+item.FormName+"'";
                        var data = await connection.QueryAsync<TTFrmStaffPerm_Custom>(query, queryParameters, commandTimeout: 1500
                            , commandType: CommandType.Text);
                        if (data.ToList().Count>0)
                        {
                            CheckExist.Add(data.FirstOrDefault());
                        }
                        item.CreatedOn = DateTime.Now;
                    }
                     await this.BulkInsert(TTFrmStaffPermList);
                    return CheckExist;

                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }

        public async Task<bool> ERP_TTFrmStaffPerm_Delete(TTFrmStaffPerm item)
        {
            try
            {
                await this.Delete(item);
                await this.Context.SaveChangesAsync();
                return await Task.FromResult(true);

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_TTFrmStaffPerm_Update(TTFrmStaffPerm item)
        {
            try
            {
                await this.Update_SaveChange(item);
                return await Task.FromResult(true);

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<TTMenuWebERP_Custom>> ERT_TTMenuWebERP_GetHierachy(TTMenuWebERP item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ParentID", item.ParentID);
                    connection.Open();
                    var data = await connection.QueryAsync<TTMenuWebERP_Custom>("ERT_TTMenuWebERP_GetHierachy"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TTFrmStaffPerm_Custom>> ERT_TTMenuWebERP_GetHierachyByStaffID(TTMenuWebERP_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@StaffID", item.StaffID);
                    queryParameters.Add("@ParentID", item.ParentID);
                    queryParameters.Add("@Level", item.Level);
                    connection.Open();
                    var data = await connection.QueryAsync<TTFrmStaffPerm_Custom>("ERT_TTMenuWebERP_GetHierachyByStaffID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public static string DataTableToJson(List<TTMenuWebERP> dt)
        {
            string jsonstring = "";
            if (dt.Count > 0)
            {
                jsonstring = "[";
                int i = 1;
                string[] listarr = { "", "}]", "}]}]", "}]}]}]", "}]}]}]}]", "}]}]}]}]}]" };
                foreach (TTMenuWebERP dr in dt)
                {
                    int currentLevel = dr.Level.Value;
                    if (dt.Count == 1)
                    {
                        i = 0;
                    }
                    int nextItemLevel = int.Parse(dt[i].Level.ToString());
                    string disableCheckbox = dr.IsCheck.Value ? "false" : "true";
                    jsonstring = jsonstring + "{\"key\":\"" + dr.FormName.ToString() +
                                                "\", \"title\":\"" + dr.FormNameDesc.ToString() +
                                                "\", \"disableCheckbox\":" + disableCheckbox;

                    if (currentLevel >= nextItemLevel) //sinh thêm thẻ children
                    {
                        jsonstring = jsonstring + ", \"isLeaf\":" + "true";
                    }
                    jsonstring = jsonstring+ ", \"value\":\"" + dr.FormName.ToString();

                    jsonstring = jsonstring + "\"";

                    if (currentLevel < nextItemLevel) //sinh thêm thẻ children
                    {
                        jsonstring = jsonstring + ",\"children\":[";
                    }
                    else // đóng thẻ
                    {
                        if (currentLevel == nextItemLevel)
                        {
                            jsonstring = jsonstring + "},";
                        }
                        else
                        {
                            int giatri = currentLevel - nextItemLevel;
                            jsonstring = jsonstring + listarr[giatri] + "},";
                        }
                    }
                    if (i < dt.Count - 1)
                    {
                        i++;
                    }
                }
                jsonstring = jsonstring.Substring(0, jsonstring.Length - 1);
                jsonstring = jsonstring + "]";
                jsonstring = jsonstring + listarr[int.Parse(dt[dt.Count - 1].Level.ToString())];
            }
            return jsonstring;
        }
        public async Task<Boolean> ERP_TTFrmStaffPerm_CopyPer(TTFrmStaffPerm_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@StaffID", item.StaffID);
                    queryParameters.Add("@StaffIDCopy", item.StaffIDCopy); 
                    queryParameters.Add("@CreatedBy", GetUserlogin());
                    queryParameters.Add("@Type", item.Type);
                    connection.Open();
                    var data = await connection.QueryAsync<TTFrmStaffPerm_Custom>("ERP_TTFrmStaffPerm_CopyPer"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return await Task.FromResult(true);

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<bool> ERT_TTFrmStaffPerm_DeleteByStaffIDMenuID(TTFrmStaffPerm_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@StaffID", item.StaffID);
                    queryParameters.Add("@FormName", item.FormName);
                    connection.Open();
                    var data = await connection.QueryAsync<TTFrmStaffPerm_Custom>("ERT_TTFrmStaffPerm_DeleteByStaffIDMenuID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return await Task.FromResult(true);

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
    }
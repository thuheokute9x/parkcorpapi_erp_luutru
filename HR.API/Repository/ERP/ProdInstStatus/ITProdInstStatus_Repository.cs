﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.ProdInstStatus
{
    interface ITProdInstStatus_Repository : IGenericRepository<TProdInstStatu>
    {
        Task<IList<TProdInstStatu>> ERP_TProdInstStatus_GetAll();

    }
}

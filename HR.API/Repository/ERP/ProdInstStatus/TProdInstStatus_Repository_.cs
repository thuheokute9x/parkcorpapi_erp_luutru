﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.ProdInstStatus
{
    public class TProdInstStatus_Repository : GenericRepository<TProdInstStatu>, ITProdInstStatus_Repository
    {
        public TProdInstStatus_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TProdInstStatus_Repository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<IList<TProdInstStatu>> ERP_TProdInstStatus_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<TProdInstStatu>("SELECT ProdInstStatusID,StatusID, ProdInstStatus, StatusDesc,Seq,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn FROM TProdInstStatus (NOLOCK)"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
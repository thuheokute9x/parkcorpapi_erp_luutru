﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ProcCat
{
    interface ITProcCat_Repository : IGenericRepository<TProcCat>
    {
        Task<IList<TProcCat_Custom>> ERP_TProcCat_GetAll();
        Task<IList<TProcCat>> ERP_TProcCat_GetProcCatCD();
    }
}
﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ProcCat
{
    public class TProcCat_Repository : GenericRepository<TProcCat>, ITProcCat_Repository
    {
        public TProcCat_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TProcCat_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TProcCat_Custom>> ERP_TProcCat_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT ProcCatCD,Seq,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn FROM dbo.TProcCat (NOLOCK) " +
                        "ORDER BY CreatedOn DESC";
                    connection.Open();
                    var data = await connection.QueryAsync<TProcCat_Custom>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<TProcCat>> ERP_TProcCat_GetProcCatCD()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT ProcCatCD FROM dbo.TProcCat (NOLOCK)";
                    connection.Open();
                    var data = await connection.QueryAsync<TProcCat>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

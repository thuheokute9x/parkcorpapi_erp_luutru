﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.PGrp
{
    interface ITPGrp_Repository : IGenericRepository<TPGrp>
    {
        Task<IList<TPGrp>> ERP_TPGrp_GetAll();
    }
}

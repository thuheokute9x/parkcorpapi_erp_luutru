﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.PGrp
{
    public class TPGrp_Repository : GenericRepository<TPGrp>, ITPGrp_Repository
    {
        public TPGrp_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TPGrp_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TPGrp>> ERP_TPGrp_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT PGrpCD,PGrpDesc,ContactCode,ContactTo,Tel,EMail" +
                    ",POByShipper,POByMfgVendor,POSheet,Discontinue,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn FROM dbo.TPGrp (NOLOCK) ORDER BY PGrpCD";
                    connection.Open();
                    var data = await connection.QueryAsync<TPGrp>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
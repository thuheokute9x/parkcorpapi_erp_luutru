﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Incoterms
{
    public class TIncoterms_Repository : GenericRepository<TIncoterm>, ITIncoterms_Repository
    {
        public TIncoterms_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TIncoterms_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TIncoterm>> ERP_TIncoterms_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT IncotermsCD FROM dbo.TIncoterms (NOLOCK)";
                    var data = await connection.QueryAsync<TIncoterm>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
    }
}
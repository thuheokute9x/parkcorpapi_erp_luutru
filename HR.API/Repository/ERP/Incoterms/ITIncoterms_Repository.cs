﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Incoterms
{
    interface ITIncoterms_Repository : IGenericRepository<TIncoterm>
    {
        Task<IList<TIncoterm>> ERP_TIncoterms_GetAll();
    }
}

﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.MatGrp
{
    interface ITMatGrp_Repository : IGenericRepository<TMatGrp>
    {
        Task<IList<TMatGrp_Custom>> ERP_TMatGrp_GetPageSize(TMatGrp_Custom item);
        DataTable ERP_TMatGrp_GetPageSize_Excel(TMatGrp_Custom item);
        Task<IList<TMatGrp_Custom>> ERP_TMatGrp_ByMatGrpCode(TMatGrp_Custom item);
        Task<IList<TMatGrp_Custom>> ERP_TMatGrp_Count_DMatMat(TMatGrp item);
        Task<TMatGrp_Custom> ERP_TMatGrp_ByMatGrpID(int MatGrpID);
        Task<IList<TMatGrp_Custom>> ERP_TMatGrp_Tranfer(TMatGrp item);

    }
}

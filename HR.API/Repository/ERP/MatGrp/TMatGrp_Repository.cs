﻿using Dapper;
using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.MatGrp
{
    public class TMatGrp_Repository : GenericRepository<TMatGrp>, ITMatGrp_Repository
    {
        public TMatGrp_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TMatGrp_Repository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<TMatGrp_Custom>> ERP_TMatGrp_GetPageSize(TMatGrp_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatGrpCode", item.MatGrpCode);
                    queryParameters.Add("@CustGrpCD", item.CustGrpCD);
                    queryParameters.Add("@MatGrpDesc", item.MatGrpDesc);
                    queryParameters.Add("@BrandCD", item.BrandCD);
                    queryParameters.Add("@Remark", item.Remark);
                    queryParameters.Add("@RangeID", item.RangeID);
                    queryParameters.Add("@SeasonID", item.SeasonID);
                    queryParameters.Add("@Year", item.Year);
                    queryParameters.Add("@IsCM", item.IsCM);
                    queryParameters.Add("@Discontinue", item.Discontinue);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TMatGrp_Custom>("ERP_TMatGrp_GetPageSize"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public DataTable ERP_TMatGrp_GetPageSize_Excel(TMatGrp_Custom item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TMatGrp_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@MatGrpCode", SqlDbType.NVarChar).Value = item.MatGrpCode;
            cmd.Parameters.Add("@CustGrpCD", SqlDbType.NVarChar).Value = item.CustGrpCD;
            cmd.Parameters.Add("@MatGrpDesc", SqlDbType.NVarChar).Value = item.MatGrpDesc;
            cmd.Parameters.Add("@BrandCD", SqlDbType.NVarChar).Value = item.BrandCD;
            cmd.Parameters.Add("@Remark", SqlDbType.NVarChar).Value = item.Remark;
            cmd.Parameters.Add("@RangeID", SqlDbType.Int).Value = item.RangeID;
            cmd.Parameters.Add("@SeasonID", SqlDbType.Int).Value = item.SeasonID;
            cmd.Parameters.Add("@Year", SqlDbType.Int).Value = item.Year;
            cmd.Parameters.Add("@IsCM", SqlDbType.Bit).Value = item.IsCM;
            cmd.Parameters.Add("@Discontinue", SqlDbType.Bit).Value = item.Discontinue;

            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }
        public async Task<IList<TMatGrp_Custom>> ERP_TMatGrp_ByMatGrpCode(TMatGrp_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT TOP(15) MatGrpCode,MatGrpDesc FROM dbo.TMatGrp (NOLOCK) WHERE MatGrpCode LIKE N'%"+ item.MatGrpCode+ "%' OR '"+ item.MatGrpCode + "'='' ";
                    var data = await connection.QueryAsync<TMatGrp_Custom>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<TMatGrp_Custom> ERP_TMatGrp_ByMatGrpID(int MatGrpID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT MatGrpID,MatGrpCode,MatGrpDesc FROM dbo.TMatGrp (NOLOCK) WHERE MatGrpID="+ MatGrpID + "";
                    var data = await connection.QueryAsync<TMatGrp_Custom>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<IList<TMatGrp_Custom>> ERP_TMatGrp_Select(TMatGrp_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT TOP(15) MatGrpCode,MatGrpDesc FROM dbo.TMatGrp (NOLOCK) WHERE MatGrpCode LIKE N'%" + item.MatGrpCode + "%' OR '" + item.MatGrpCode + "'='' ";
                    var data = await connection.QueryAsync<TMatGrp_Custom>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<IList<TMatGrp_Custom>> ERP_TMatGrp_Count_DMatMat(TMatGrp item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    if (item.MatGrpID==0)
                    {
                        queryParameters.Add("@MatGrpCode", item.MatGrpCode);
                    }
                    if (item.MatGrpID != 0)
                    {
                        queryParameters.Add("@MatGrpID", item.MatGrpID);
                    }
                    connection.Open();
                    var data = await connection.QueryAsync<TMatGrp_Custom>("ERP_TMatGrp_Count_DMatMat"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TMatGrp_Custom>> ERP_TMatGrp_Tranfer(TMatGrp item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatGrpCode", item.MatGrpCode);
                    connection.Open();
                    var data = await connection.QueryAsync<TMatGrp_Custom>("ERP_TMatGrp_Tranfer"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
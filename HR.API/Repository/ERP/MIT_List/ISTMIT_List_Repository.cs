﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.MIT_List
{
    interface ISTMIT_List_Repository
    {
        Task<IList<Object>> ERP_STMIT_List_SFMIT_GetPageSize(dynamic item);
        DataTable ERP_STMIT_List_SFMIT_GetPageSize_Excel(dynamic item);
        Task<IList<Object>> ERP_STMIT_List_GetByMatPOItemID(STMIT_List item);
        Task<IList<STMIStatu>> ERP_STMIStatus_GetAll(dynamic item);
        Task<IList<Object>> ERP_STMIT_List_InsertReturnExpand(STMIT_List item);
        Task<IList<Object>> ERP_STMIT_List_UpdateReturnExpand(STMIT_List item);
    }
}

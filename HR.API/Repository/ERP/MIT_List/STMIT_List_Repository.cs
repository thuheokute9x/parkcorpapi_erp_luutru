﻿using Dapper;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.MIT_List
{
    public class STMIT_List_Repository : GenericRepository<STMIT_List>, ISTMIT_List_Repository
    {
        public STMIT_List_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public async Task<IList<Object>> ERP_STMIT_List_SFMIT_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RefVKNo", item.RefVKNo.Value);
                    queryParameters.Add("@PGrpCD", item.PGrpCD.Value);
                    queryParameters.Add("@MatPOCD", item.MatPOCD.Value);
                    queryParameters.Add("@MatCD", item.MatCD.Value);
                    queryParameters.Add("@VenDorCD", item.VenDorCD.Value);
                    queryParameters.Add("@Shipper", item.Shipper.Value);
                    queryParameters.Add("@BalanceChk", item.BalanceChk.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_STMIT_List_SFMIT_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_STMIT_List_SFMIT_GetPageSize_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_STMIT_List_SFMIT_GetPageSize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@RefVKNo", SqlDbType.NVarChar).Value = item.RefVKNo.Value;
                cmd.Parameters.Add("@PGrpCD", SqlDbType.NVarChar).Value = item.PGrpCD.Value;
                cmd.Parameters.Add("@MatPOCD", SqlDbType.NVarChar).Value = item.MatPOCD.Value;
                cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD.Value;
                cmd.Parameters.Add("@VenDorCD", SqlDbType.NVarChar).Value = item.VenDorCD.Value;
                cmd.Parameters.Add("@Shipper", SqlDbType.NVarChar).Value = item.Shipper.Value;
                cmd.Parameters.Add("@BalanceChk", SqlDbType.Bit).Value = item.BalanceChk.Value;

                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_STMIT_List_GetByMatPOItemID(STMIT_List item)//có gọi từ ERP_STMIT_List_InsertReturnExpand ERP_STMIT_List_UpdateReturnExpand
        {
            try
            {
                
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatPOItemID", item.MatPOItemID);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(@"ERP_STMIT_List_GetByMatPOItemID", queryParameters, commandTimeout: 1500
                         ,commandType: CommandType.StoredProcedure);
                    
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<STMIStatu>> ERP_STMIStatus_GetAll(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<STMIStatu>(@"SELECT * FROM dbo.STMIStatus (NOLOCK)", queryParameters, commandTimeout: 1500
                         , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_STMIT_List_InsertReturnExpand(STMIT_List item)
        {
            try
            {
                await this.Insert_SaveChange(item);

                return await this.ERP_STMIT_List_GetByMatPOItemID(item);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_STMIT_List_UpdateReturnExpand(STMIT_List item)
        {
            try
            {
                await this.Update_SaveChange(item);
                return await this.ERP_STMIT_List_GetByMatPOItemID(item);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
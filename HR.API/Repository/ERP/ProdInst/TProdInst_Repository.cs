﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.ProdInst
{
    public class TProdInst_Repository : GenericRepository<TProdInst>, ITProdInst_Repository
    {
        public TProdInst_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TProdInst_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TProdInst_Custom>> ERP_TProdInst_GetClosedOnFGGR(TProdInst_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD);
                    queryParameters.Add("@ProdInstCDType", WIP_GetProdInstCDWithCompany());
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@ClosedOnFGGR", item.ClosedOnFGGRBit);
                    connection.Open();
                    var data = await connection.QueryAsync<TProdInst_Custom>("ERP_TProdInst_GetClosedOnFGGR"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TProdInst_Custom>> ERP_TProdInst_GetWorkingFGGR(TProdInst_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@FromDate", item.FromDate);
                    queryParameters.Add("@ToDate", item.ToDate);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@ClosedOnFGGR", item.ClosedOnFGGRBit);
                    queryParameters.Add("@ProdInstCDType", WIP_GetProdInstCDWithCompany());
                    connection.Open();
                    var data = await connection.QueryAsync<TProdInst_Custom>("ERP_TProdInst_GetWorkingFGGR"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TProdInst_GetWorkingFGGR_Excel(TProdInst_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TProdInst_GetWorkingFGGR", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = item.FromDate;
                cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = item.ToDate;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@PageNumber", SqlDbType.NVarChar).Value = item.PageNumber;
                cmd.Parameters.Add("@PageSize", SqlDbType.NVarChar).Value = item.PageSize;
                cmd.Parameters.Add("@ClosedOnFGGR", SqlDbType.Bit).Value = item.ClosedOnFGGRBit;
                cmd.Parameters.Add("@ProdInstCDType", SqlDbType.NVarChar).Value = WIP_GetProdInstCDWithCompany();

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TProdInst_GetClosedOnFGGR_Excel(TProdInst_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TProdInst_GetClosedOnFGGR", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ProdInstCDType", SqlDbType.NVarChar).Value = WIP_GetProdInstCDWithCompany();
                cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.ProdInstCD;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@PageNumber", SqlDbType.NVarChar).Value = item.PageNumber;
                cmd.Parameters.Add("@PageSize", SqlDbType.NVarChar).Value = item.PageSize;
                cmd.Parameters.Add("@ClosedOnFGGR", SqlDbType.Bit).Value = item.ClosedOnFGGRBit;


                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<bool> ERP_TProdInst_UpdateClosedOnFGGR(TProdInst_Custom ob)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string Query = "UPDATE TProdInst SET ClosedOnFGGR='" + ob.ClosedOnFGGR.Value.ToString("yyyy-MM-dd") + "',UpdatedBy='" + GetUserlogin() + "',UpdatedOn=GETDATE() WHERE ProdInstCD='" + ob.ProdInstCD + "'";
                    var data = await connection.QueryAsync<TProdInst_Custom>(Query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return await Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TProdInst_Custom>> ERP_TProdInst_GetVKwithYear(TProdInst_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TProdInst_Custom>("ERP_TProdInst_GetVKwithYear"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TProdInstCat>> ERP_TProdInstCat_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<TProdInstCat>("SELECT ProdInstCatCD,ProdDesc FROM TProdInstCat (NOLOCK)"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TProdInstStatu>> ERP_TProdInstStatus_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<TProdInstStatu>("SELECT TProdInstStatus.ProdInstStatusID, TProdInstStatus.ProdInstStatus, TProdInstStatus.StatusDesc FROM TProdInstStatus (NOLOCK)"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TCutWay>> ERP_TCutWay_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<TCutWay>("SELECT CutWayCD,CutWayCap FROM dbo.TCutWay (NOLOCK) WHERE Discontinue=0"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TProdInst_Custom>> ERP_TProdInst_GetProductionInstructionPagesize(TProdInst_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@NA", item.NA);
                    queryParameters.Add("@StandBy", item.StandBy);
                    queryParameters.Add("@InProcess", item.InProcess);
                    queryParameters.Add("@Pending", item.Pending);
                    queryParameters.Add("@Completed", item.Completed);
                    queryParameters.Add("@Canceled", item.Canceled);
                    queryParameters.Add("@Discontinue", item.Discontinue);
                    queryParameters.Add("@Close", item.Close);
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD);
                    if (item.IDate_From.HasValue)
                    {
                        queryParameters.Add("@IDate_From", item.IDate_From);
                        queryParameters.Add("@IDate_To", item.IDate_To);
                    }
                    if (item.EDate_From.HasValue)
                    {
                        queryParameters.Add("@EDate_From", item.EDate_From);
                        queryParameters.Add("@EDate_To", item.EDate_To);
                    }

                    queryParameters.Add("@ProdInstCatCD", item.ProdInstCatCD);
                    queryParameters.Add("@InstStatusID", item.InstStatusID);
                    queryParameters.Add("@CutWayCD", item.CutWayCD);
                    if (item.PlanYearDate.HasValue)
                    {
                        queryParameters.Add("@PlanYear", item.PlanYearDate.Value.Year);
                    }
                    if (item.PlanMonthDate.HasValue)
                    {
                        queryParameters.Add("@PlanMonth", item.PlanMonthDate.Value.Month);
                    }
                    queryParameters.Add("@Note", item.Note);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    var data = await connection.QueryAsync<TProdInst_Custom>("ERP_TProdInst_GetProductionInstructionPagesize"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TProdInst_GetProductionInstructionPagesize_Excel(TProdInst_Custom item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TProdInst_GetProductionInstructionPagesize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@NA", SqlDbType.Bit).Value = item.NA;
            cmd.Parameters.Add("@StandBy", SqlDbType.Bit).Value = item.StandBy;
            cmd.Parameters.Add("@InProcess", SqlDbType.Bit).Value = item.InProcess;
            cmd.Parameters.Add("@Pending", SqlDbType.Bit).Value = item.Pending;
            cmd.Parameters.Add("@Completed", SqlDbType.Bit).Value = item.Completed;
            cmd.Parameters.Add("@Canceled", SqlDbType.Bit).Value = item.Canceled;
            cmd.Parameters.Add("@Discontinue", SqlDbType.Bit).Value = item.Discontinue;
            cmd.Parameters.Add("@Close", SqlDbType.Bit).Value = item.Close;
            cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.ProdInstCD;
            if (item.IDate_From.HasValue)
            {
                cmd.Parameters.Add("@IDate_From", SqlDbType.DateTime).Value = item.IDate_From;
                cmd.Parameters.Add("@IDate_To", SqlDbType.DateTime).Value = item.IDate_To;
            }
            if (item.EDate_From.HasValue)
            {
                cmd.Parameters.Add("@EDate_From", SqlDbType.DateTime).Value = item.EDate_From;
                cmd.Parameters.Add("@EDate_To", SqlDbType.DateTime).Value = item.EDate_To;
            }

            cmd.Parameters.Add("@ProdInstCatCD", SqlDbType.NVarChar).Value = item.ProdInstCatCD;
            cmd.Parameters.Add("@InstStatusID", SqlDbType.Int).Value = item.InstStatusID;
            cmd.Parameters.Add("@CutWayCD", SqlDbType.NVarChar).Value = item.CutWayCD;
            if (item.PlanYearDate.HasValue)
            {
                cmd.Parameters.Add("@PlanYear", SqlDbType.Int).Value = item.PlanYearDate;
            }
            if (item.PlanMonthDate.HasValue)
            {
                cmd.Parameters.Add("@PlanMonth", SqlDbType.Int).Value = item.PlanMonthDate;
            }
            cmd.Parameters.Add("@Note", SqlDbType.NVarChar).Value = item.Note;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;


        }
        public async Task<TProdInst_Custom> ERP_TProdInst_GenerateProdInstCD(TProdInst_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCatCD", item.ProdInstCatCD);
                    var data = await connection.QueryAsync<TProdInst_Custom>("ERP_TProdInst_GenerateProdInstCD"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TProdInst_TPRProdInst_GetByProdInstCD(TProdInst_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TProdInst_TPRProdInst_GetByProdInstCD"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

    }

}
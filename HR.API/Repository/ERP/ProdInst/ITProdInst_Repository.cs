﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.ProdInst
{
    interface ITProdInst_Repository : IGenericRepository<TProdInst>
    {
        Task<IList<TProdInst_Custom>> ERP_TProdInst_GetClosedOnFGGR(TProdInst_Custom item);
        DataTable ERP_TProdInst_GetClosedOnFGGR_Excel(TProdInst_Custom item);
        Task<IList<TProdInst_Custom>> ERP_TProdInst_GetWorkingFGGR(TProdInst_Custom item);
        DataTable ERP_TProdInst_GetWorkingFGGR_Excel(TProdInst_Custom item);
        Task<Boolean> ERP_TProdInst_UpdateClosedOnFGGR(TProdInst_Custom item);
        Task<IList<TProdInst_Custom>> ERP_TProdInst_GetVKwithYear(TProdInst_Custom item);
        Task<IList<TProdInstCat>> ERP_TProdInstCat_GetAll();
        Task<IList<TCutWay>> ERP_TCutWay_GetAll();
        Task<IList<TProdInst_Custom>> ERP_TProdInst_GetProductionInstructionPagesize(TProdInst_Custom item);
        DataTable ERP_TProdInst_GetProductionInstructionPagesize_Excel(TProdInst_Custom item);
        Task<TProdInst_Custom> ERP_TProdInst_GenerateProdInstCD(TProdInst_Custom item);
        Task<IList<Object>> ERP_TProdInst_TPRProdInst_GetByProdInstCD(TProdInst_Custom item);
    }
}

﻿using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.IRepository
{
    public interface ITVendorRepository : IGenericRepository<TVendor>
    {
        Task<IList<TVendor>> ERP_TVendor_GetwithVendorCDLike(string VendorCD);
        Task<IList<TVendor>> ERP_TVendor_GetwithVendorCDLike_All(TVendor item);

        Task<IList<TVendor>> ERP_TVendor_GetSHIPPER(string SHIPPER);
        Task<IList<TVendor>> ERP_TVendor_GetAll();
        Task<IList<TVendor_Custom>> ERP_TVendor_GetPageSize(TVendor_Custom item);
        Task<IList<TVendor_Custom>> ERP_TVendor_FromTPInfo(TVendor_Custom item);
        //Task<IList<TVendor_Custom>> ERP_TVendor_FromTPInfo_NotExistDis(TVendor_Custom item);
         Task<TVendor> ERP_TVendor_GetVendorCD(TVendor VendorCD);
        Task<bool> ERP_TVendor_Update_FilePath(TVendor_Custom item);
        Task<bool> ERP_TVendor_Update(TVendor item);
        Task<bool> ERP_TVendor_Insert(TVendor item);
        Task<Result> ERP_TVendor_CheckTel_AccountNo(TVendor item);
        Task<List<Object>> ERP_TVendor_TPInfo_GetVendorCD_PGrpCD(TVendor_Custom item);
        Task<List<TPayDoc>> ERP_TPayDoc_GetAll();
        Task<List<STCompany>> ERP_STCompany_GetByPayReqTo();
        DataTable ERP_TVendor_GetPageSize_Excel(TVendor_Custom item);
    }
}

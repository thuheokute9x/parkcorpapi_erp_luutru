﻿using AutoMapper.Configuration;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.IRepository;
using HR.API.IUnitOfWork;
using System.Data;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models;


namespace HR.API.Repository.Vendor
{
    public class TVendorRepository : GenericRepository<TVendor>, ITVendorRepository
    {
        public TVendorRepository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TVendorRepository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<int> AddAsync(TVendor entity)
        {
            //entity.AddedOn = DateTime.Now;
            var sql = "Insert into Products (Name,Description,Barcode,Rate,AddedOn) VALUES (@Name,@Description,@Barcode,@Rate,@AddedOn)";
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, entity);
                return result;
            }
        }
        public async Task<TVendor> ERP_TVendor_GetVendorCD(TVendor item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "SELECT VendorCD,VendorName,AltName,Addr,Tel,Fax,ContactTo,EMail,DealingItem,HSCode,IsShipper" +
                        ",IsConsole,IsBagMaker,IsMatMaker,BankName,BankAddr,SwiftCode,AccountName,AccountNo,ShipFrom,POTo,CurrCD" +
                        ",PGrpCD,IncotermsCD,Shipper,TaxRate,FreightRate,CommissionRate,PayMthCD,PayCondID,PayTerm,PayDocCD " +
                        "FROM dbo.TVendor (NOLOCK) WHERE VendorCD='"+ item.VendorCD + "' AND Discontinue=0 ";
                    connection.Open();
                    var data = await connection.QueryAsync<TVendor>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return  data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<TVendor> ERP_TVendor_(TVendor item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "SELECT VendorCD,VendorName,AltName,Addr,Tel,Fax,ContactTo,EMail,DealingItem,HSCode,IsShipper" +
                        ",IsConsole,IsBagMaker,IsMatMaker,BankName,BankAddr,SwiftCode,AccountName,AccountNo,ShipFrom,POTo,CurrCD" +
                        ",PGrpCD,IncotermsCD,Shipper,TaxRate,FreightRate,CommissionRate,PayMthCD,PayCondID,PayTerm,PayDocCD " +
                        "FROM dbo.TVendor (NOLOCK) WHERE VendorCD='" + item.VendorCD + "' AND Discontinue=0 ";
                    connection.Open();
                    var data = await connection.QueryAsync<TVendor>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<List<Object>> ERP_TVendor_TPInfo_GetVendorCD_PGrpCD(TVendor_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@VendorCD", item.VendorCD);
                    queryParameters.Add("@PInfoID", item.PInfoID);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TVendor_TPInfo_GetVendorCD_PGrpCD"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<TVendor>> ERP_TVendor_GetwithVendorCDLike(string VendorCD)
        {
            try
            {
                List<TVendor> result = new List<TVendor>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@VendorCD", string.IsNullOrEmpty(VendorCD) || VendorCD == "All" ? "" : VendorCD.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<TVendor>("ERP_TVendor_GetwithVendorCDLike"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<TVendor>> ERP_TVendor_GetwithVendorCDLike_All(TVendor item)
        {
            try
            {
                List<TVendor> result = new List<TVendor>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@VendorCD", string.IsNullOrEmpty(item.VendorCD) || item.VendorCD == "All" ? "" : item.VendorCD.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<TVendor>("ERP_TVendor_GetwithVendorCDLike_All"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<TVendor>> ERP_TVendor_GetSHIPPER(string SHIPPER)
        {
            try
            {
                List<TVendor> result = new List<TVendor>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@SHIPPER", string.IsNullOrEmpty(SHIPPER) || SHIPPER == "All" ? "" : SHIPPER.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<TVendor>("ERP_TVendor_GetSHIPPER"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<TVendor_Custom>> ERP_TVendor_GetPageSize(TVendor_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@VendorCD", string.IsNullOrEmpty(item.VendorCD) ? "" : item.VendorCD);
                    queryParameters.Add("@VendorName", string.IsNullOrEmpty(item.VendorName) ? "" : item.VendorName);
                    queryParameters.Add("@PGrpCD", item.PGrpCD);
                    queryParameters.Add("@PayReqTo", item.PayReqTo);
                    queryParameters.Add("@PayDocCD", item.PayDocCD);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TVendor_Custom>("ERP_TVendor_GetPageSize"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                   return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public DataTable ERP_TVendor_GetPageSize_Excel(TVendor_Custom item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TVendor_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@VendorCD", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.VendorCD) ? "" : item.VendorCD;
            cmd.Parameters.Add("@VendorName", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.VendorName) ? "" : item.VendorName;
            cmd.Parameters.Add("@PGrpCD", SqlDbType.NVarChar).Value = item.PGrpCD;
            cmd.Parameters.Add("@PayReqTo", SqlDbType.NVarChar).Value = item.PayReqTo;
            cmd.Parameters.Add("@PayDocCD", SqlDbType.NVarChar).Value = item.PayDocCD;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;


            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;


        }
        public async Task<IList<TVendor_Custom>> ERP_TVendor_FromTPInfo(TVendor_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCD", item.MatCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TVendor_Custom>("ERP_TVendor_FromTPInfo"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        //public async Task<IList<TVendor_Custom>> ERP_TVendor_FromTPInfo_NotExistDis(TVendor_Custom item)
        //{
        //    try
        //    {
        //        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
        //        {
        //            var queryParameters = new DynamicParameters();
        //            queryParameters.Add("@MatCD", item.MatCD);
        //            connection.Open();
        //            var data = await connection.QueryAsync<TVendor_Custom>(@"	SELECT b.VendorCD,b.VendorName,b.AltName
	       //         FROM dbo.TPInfo a (NOLOCK)
	       //         INNER JOIN dbo.TVendor b (NOLOCK) ON a.MfgVendor=b.VendorCD
	       //         WHERE a.MatCD=@MatCD AND a.Discontinue=0 AND b.Discontinue=0
	       //         GROUP BY b.VendorCD,b.VendorName,b.AltName"
        //                , queryParameters, commandTimeout: 1500
        //                , commandType: CommandType.Text);
        //            return data.ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new NotImplementedException(ex.Message.ToString());
        //    }

        //}
        public async Task<bool> ERP_TVendor_Update_FilePath(TVendor_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    queryParameters.Add("@FilePath", item.FilePath);
                    queryParameters.Add("@VendorCD", item.VendorCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TVendor>("ERP_TVendor_Update_FilePath"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                }
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<bool> ERP_TVendor_Insert(TVendor item)
        {
            try
            {
                //using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                //{
                //    if (!string.IsNullOrEmpty(item.Tel))
                //    {
                //        var queryParameters = new DynamicParameters();
                //        queryParameters.Add("@Tel", item.Tel);
                //        connection.Open();
                //        var data = await connection.QueryAsync<TVendor>("SELECT VendorCD FROM dbo.TVendor (NOLOCK) WHERE Tel=@Tel "
                //            , queryParameters, commandTimeout: 1500
                //            , commandType: CommandType.Text);
                //        connection.Close();
                //        if (data.ToList().Any())
                //        {
                //            throw new NotImplementedException("Tel exists in VendorCD </br> " + data.FirstOrDefault().VendorCD);
                //        }
                //    }
                //    if (!string.IsNullOrEmpty(item.AccountNo))
                //    {
                //        var queryParameters = new DynamicParameters();
                //        queryParameters.Add("@AccountNo", item.AccountNo);
                //        connection.Open();
                //        var data = await connection.QueryAsync<TVendor>("SELECT VendorCD FROM dbo.TVendor (NOLOCK) WHERE AccountNo=@AccountNo"
                //            , queryParameters, commandTimeout: 1500
                //            , commandType: CommandType.Text);
                //        connection.Close();
                //        if (data.ToList().Any())
                //        {
                //            throw new NotImplementedException("AccountNo exists in VendorCD </br> " + data.FirstOrDefault().VendorCD);
                //        }
                //    }

                //}
                await this.Insert_SaveChange(item);
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<bool> ERP_TVendor_Update(TVendor item)
        {
            try
            {
                //using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                //{
                //    if (!string.IsNullOrEmpty(item.Tel))
                //    {
                //        var queryParameters = new DynamicParameters();
                //        queryParameters.Add("@Tel", item.Tel);
                //        queryParameters.Add("@VendorCD", item.VendorCD);
                //        connection.Open();
                //        var data = await connection.QueryAsync<TVendor>("SELECT VendorCD FROM dbo.TVendor (NOLOCK) WHERE Tel=@Tel AND VendorCD<>@VendorCD  "
                //            , queryParameters, commandTimeout: 1500
                //            , commandType: CommandType.Text);
                //        connection.Close();
                //        if (data.ToList().Any())
                //        {
                //            throw new NotImplementedException("Tel exists in VendorCD </br> " + data.FirstOrDefault().VendorCD);
                //        }
                //    }
                //    if (!string.IsNullOrEmpty(item.AccountNo))
                //    {
                //        var queryParameters = new DynamicParameters();
                //        queryParameters.Add("@AccountNo", item.AccountNo);
                //        queryParameters.Add("@VendorCD", item.VendorCD);
                //        connection.Open();
                //        var data = await connection.QueryAsync<TVendor>("SELECT VendorCD FROM dbo.TVendor (NOLOCK) WHERE AccountNo=@AccountNo AND VendorCD<>@VendorCD   "
                //            , queryParameters, commandTimeout: 1500
                //            , commandType: CommandType.Text);
                //        connection.Close();
                //        if (data.ToList().Any())
                //        {
                //            throw new NotImplementedException("AccountNo exists in VendorCD </br> " + data.FirstOrDefault().VendorCD);
                //        }
                //    }

                //}
                await this.Update_SaveChange(item);
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<Result> ERP_TVendor_CheckTel_AccountNo(TVendor item)
        {
            try
            {
                Result dt = new Result();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    if (!string.IsNullOrEmpty(item.AccountNo))
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@AccountNo", item.AccountNo);
                        queryParameters.Add("@Tel", item.Tel);
                        queryParameters.Add("@VendorCD", item.VendorCD);
                        connection.Open();
                        var data = await connection.QueryAsync<TVendor>("SELECT VendorCD FROM dbo.TVendor (NOLOCK) WHERE (AccountNo=@AccountNo AND Tel=@Tel) AND VendorCD<>@VendorCD   "
                            , queryParameters, commandTimeout: 1500
                            , commandType: CommandType.Text);
                        connection.Close();
                        if (data.ToList().Any())
                        {
                            dt.ErrorCode = 1;
                            dt.ErrorMessage = "Information (Tel & AccountNo) already exits on  Vendor '" + data.FirstOrDefault().VendorCD +"'. </br> Select Yes to continue or No to cancel";
                            return dt;
                        }
                    }
                    //if (!string.IsNullOrEmpty(item.Tel))
                    //{
                    //    var queryParameters = new DynamicParameters();
                    //    queryParameters.Add("@Tel", item.Tel);
                    //    queryParameters.Add("@VendorCD", item.VendorCD);
                    //    connection.Open();
                    //    var data = await connection.QueryAsync<TVendor>("SELECT VendorCD FROM dbo.TVendor (NOLOCK) WHERE Tel=@Tel AND VendorCD<>@VendorCD  "
                    //        , queryParameters, commandTimeout: 1500
                    //        , commandType: CommandType.Text);
                    //    connection.Close();
                    //    if (data.ToList().Any())
                    //    {
                    //        dt.ErrorCode = 1;
                    //        dt.ErrorMessage="Tel exists in VendorCD " + data.FirstOrDefault().VendorCD;
                    //        return dt;
                    //    }
                    //}

                }
                dt.ErrorCode = 0;
                return dt;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<IList<TVendor>> ERP_TVendor_GetAll()
        {
            List<TVendor> result = new List<TVendor>();
            return await Task.FromResult(result);
        }
        public async Task<List<TPayDoc>> ERP_TPayDoc_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    string query = "SELECT a.PayDocCD, a.VendorPayDoc FROM dbo.TPayDoc a (NOLOCK) WHERE a.VendorPayDoc=1";
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<TPayDoc>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        public async Task<List<STCompany>> ERP_STCompany_GetByPayReqTo()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<STCompany>("SELECT CompanyCD,CompanyDesc FROM dbo.STCompany (NOLOCK) WHERE PayReqTo=1"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
    }
}
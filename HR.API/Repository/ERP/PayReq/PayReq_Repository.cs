﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.PayReq
{
    public class PayReq_Repository : GenericRepository<STPayReq>, IPayReq_Repository
    {
        public PayReq_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public PayReq_Repository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<STPayReq_Custom>> ERP_STPayReq_Rgl_GetPageSize(STPayReq_Custom PayReq)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@VendorCD", PayReq.VendorCD);
                    queryParameters.Add("@PayReqID", PayReq.PayReqID); 
                    queryParameters.Add("@RglPayReqID", PayReq.RglPayReqID);
                    queryParameters.Add("@PageNumber", PayReq.PageNumber);
                    queryParameters.Add("@PageSize", PayReq.PageSize);
                    queryParameters.Add("@Type", PayReq.Type);
                    connection.Open();
                    var data = await connection.QueryAsync<STPayReq_Custom>("ERP_STPayReq_Rgl_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);


                    string userlogin = GetUserlogin();
                    IDatabase db = ConnectRedis();
                    string valued = await db.StringGetAsync("TTRglPayReq_List_InsChkTempByUser_" + PayReq.RglPayReqID + "_" + userlogin);
                    if (!string.IsNullOrEmpty(valued))
                    {
                        List<TTRglPayReq_List_Custom> InsChkTempByUser = JsonConvert.DeserializeObject<List<TTRglPayReq_List_Custom>>(valued);
                        //var listInsert = (from a in InsChkTempByUser
                        //                  select new TTRglPayReq_List { RglPayReqID = a.RglPayReqID, PayReqID = a.PayReqID }).ToList();

                         var result = (from a in data.ToList()
                                      join b in InsChkTempByUser
                                      on a.PayReqID equals b.PayReqID into g
                                      from b in g.DefaultIfEmpty()
                                      select new STPayReq_Custom
                                      {
                                          RowID=a.RowID,
                                          TotalRows=a.TotalRows,
                                          PayReqID=a.PayReqID,
                                          HasSchg=a.HasSchg,
                                          VendorCD=a.VendorCD,
                                          VendorName=a.VendorName,
                                          EmpNo=a.EmpNo,
                                          EmpName=a.EmpName,
                                          PayReqDateConvert=a.PayReqDateConvert,
                                          PayDueDateConvert=a.PayDueDateConvert,
                                          PayReqTo=a.PayReqTo,
                                          PaidOn=a.PaidOn,
                                          PayReqAllow=a.PayReqAllow,
                                          Remark=a.Remark,
                                          Checked = b==null?false:true
                                    }).OrderBy(x=>x.RowID).ToList();
                        return result;

                    }
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<STPayReq>> ERP_STPayReq_GetPayReqID(string PayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<STPayReq>("SELECT TOP (15) PayReqID FROM dbo.STPayReq (NOLOCK) WHERE PayReqID LIKE '%"+PayReqID.ToString()+"%'"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }

    }
}
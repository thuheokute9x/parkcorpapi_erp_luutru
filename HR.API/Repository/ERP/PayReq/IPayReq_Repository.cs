﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.PayReq
{
    interface IPayReq_Repository : IGenericRepository<STPayReq>
    {
        Task<IList<STPayReq>> ERP_STPayReq_GetPayReqID(string PayReqID);
        Task<IList<STPayReq_Custom>> ERP_STPayReq_Rgl_GetPageSize(STPayReq_Custom PayReqID);

    }
}

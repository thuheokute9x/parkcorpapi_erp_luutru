﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Emp
{
    interface ITEmpRepository : IGenericRepository<TEmp>
    {
        Task<IList<TEmp_Custom>> ERP_TEmp_GetWithStaffID();
        Task<IList<TEmp>> ERP_TEmp_GetAll();
        Task<IList<TEmp_Custom>> ERP_TEmp_GetByEmpName_EmpID(TEmp_Custom item);
        Task<Boolean> ERP_TEmp_Insert(TEmp item);
        Task<Boolean> ERP_TEmp_Update(TEmp item);
        Task<IList<TEmp>> ERP_TEmp_GetEmpID_EmpName_EmpNo();
    }
}

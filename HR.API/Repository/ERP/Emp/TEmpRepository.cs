﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Emp
{
    public class TEmpRepository : GenericRepository<TEmp>, ITEmpRepository
    {
        public TEmpRepository(IUnitOfWork<PARKERPEntities> unitOfWork)
: base(unitOfWork)
        {
        }
        public TEmpRepository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<TEmp>> ERP_TEmp_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT EmpID,EmpName,EmpNo,Dept,EMail,SysID,IsAdmin,Discontinue,CreatedBy" +
                    ",CreatedOn,UpdatedBy,UpdatedOn FROM dbo.TEmp (NOLOCK) WHERE Discontinue=0 ORDER BY EmpNo";
                    var data = await connection.QueryAsync<TEmp>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TEmp>> ERP_TEmp_GetEmpID_EmpName_EmpNo()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT EmpID,EmpName,EmpNo" +
                    " FROM dbo.TEmp (NOLOCK) ORDER BY EmpID";
                    var data = await connection.QueryAsync<TEmp>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<TEmp_Custom>> ERP_TEmp_GetByEmpName_EmpID(TEmp_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    queryParameters.Add("@EmpID", item.EmpID);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    var data = await connection.QueryAsync<TEmp_Custom>("ERP_TEmp_GetByEmpName_EmpID", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<TEmp_Custom>> ERP_TEmp_GetWithStaffID()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT CONVERT(VARCHAR, b.ID) + ' - ' + a.EmpName ID_EmpName,a.EmpName,a.EmpID,b.StaffID FROM dbo.TEmp a (NOLOCK) " +
                        "INNER JOIN ZStaff b (NOLOCK) ON a.EmpID=b.EmpID WHERE b.Discontinue=0 AND a.Discontinue=0";
                    var data = await connection.QueryAsync<TEmp_Custom>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Boolean> ERP_TEmp_Insert(TEmp item)
        {
            try
            {
                await this.Insert_SaveChange(item);
                return await Task.FromResult(true);

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Boolean> ERP_TEmp_Update(TEmp item)
        {
            try
            {
                await this.Update_SaveChange(item);
                return await Task.FromResult(true);

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
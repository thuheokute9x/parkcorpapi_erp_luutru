﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Brand
{
    interface ITBrand_Repository : IGenericRepository<TBrand>
    {
        Task<IList<TBrand_Custom>> ERP_TBrand_GetPageSize(TBrand_Custom item);
        DataTable ERP_TBrand_GetPageSize_Excel(TBrand_Custom item);
        Task<IList<TBrand_Custom>> ERP_TBrand_GetAll(TBrand_Custom item);
        Task<IList<TBrand_Custom>> ERP_TBrand_ByCustGrpCD(TBrand item);
        Task<IList<TBrand_Custom>> ERP_TBrand_ByBrandCD(TBrand item);


    }
}

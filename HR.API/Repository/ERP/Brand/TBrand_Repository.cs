﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Brand
{
    public class TBrand_Repository : GenericRepository<TBrand>, ITBrand_Repository
    {
        public TBrand_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TBrand_Repository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<TBrand_Custom>> ERP_TBrand_GetAll(TBrand_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT BrandCD,CustGrpCD,BrandDesc,DirectedSourcing,Discontinue,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn FROM dbo.TBrand (NOLOCK) WHERE Discontinue=0";
                    connection.Open();
                    var data = await connection.QueryAsync<TBrand_Custom>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TBrand_Custom>> ERP_TBrand_ByCustGrpCD(TBrand item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT BrandCD + ' of ' + CustGrpCD BrandCDCustGrpCD,BrandCD,CustGrpCD " +
                        "FROM dbo.TBrand (NOLOCK) " +
                        "WHERE Discontinue=0 AND CustGrpCD='"+ item.CustGrpCD + "'";
                    connection.Open();
                    var data = await connection.QueryAsync<TBrand_Custom>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TBrand_Custom>> ERP_TBrand_ByBrandCD(TBrand item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();

                    string Query = "SELECT BrandCD + ' of ' + CustGrpCD BrandCDCustGrpCD,BrandCD,CustGrpCD " +
                        "FROM dbo.TBrand (NOLOCK) " +
                        "WHERE Discontinue=0 AND CustGrpCD=@CustGrpCD";
                    queryParameters.Add("@CustGrpCD", item.CustGrpCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TBrand_Custom>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TBrand_Custom>> ERP_TBrand_GetPageSize(TBrand_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@BrandCD", item.BrandCD);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TBrand_Custom>("ERP_TBrand_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }






        public DataTable ERP_TBrand_GetPageSize_Excel(TBrand_Custom item)
        {

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TBrand_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@BrandCD", SqlDbType.NVarChar).Value = item.BrandCD;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

        }

    }
}
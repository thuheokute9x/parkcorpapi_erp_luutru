﻿using Dapper;

using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Range
{
    public class TRange_Repository : GenericRepository<TRange>, ITRange_Repository
    {
        public TRange_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TRange_Repository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<IList<TRange_Custom>> ERP_TRange_GetPageSize(TRange_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@BrandCD", item.BrandCD);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@Type", item.Type);
                    connection.Open();
                    var data = await connection.QueryAsync<TRange_Custom>("ERP_TRange_GetPageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TRange_GetPageSize_Excel(TRange_Custom item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TRange_GetPageSize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@BrandCD", SqlDbType.NVarChar).Value = item.BrandCD;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;


            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;
        }
        public async Task<IList<TRange>> ERP_TRange_ByRangeDesc(TRange item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT TOP(15) RangeDesc,RangeID " +
                        "FROM dbo.TRange (NOLOCK) " +
                        "WHERE RangeDesc LIKE N'%" + item.RangeDesc + "%' OR '" + item.RangeDesc + "'=''";
                    var data = await connection.QueryAsync<TRange>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TRange>> ERP_TRange_ByRangeID(TRange item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RangeID", item.RangeID);
                    connection.Open();
                    string query = "SELECT RangeDesc,RangeID FROM dbo.TRange (NOLOCK) WHERE RangeID=@RangeID";
                    var data = await connection.QueryAsync<TRange>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<IList<TRange_Custom>> ERP_TRange_ByBrandCD(TRange item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT RangeID,RangeDesc+' of '+BrandCD RangeDescBrandCD,RangeDesc,BrandCD FROM dbo.TRange (NOLOCK) WHERE BrandCD='"+ item.BrandCD + "'";
                    var data = await connection.QueryAsync<TRange_Custom>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
    }
}
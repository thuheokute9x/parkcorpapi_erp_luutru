﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Range
{
    interface ITRange_Repository : IGenericRepository<TRange>
    {
        Task<IList<TRange_Custom>> ERP_TRange_GetPageSize(TRange_Custom item);
        DataTable ERP_TRange_GetPageSize_Excel(TRange_Custom item);
        Task<IList<TRange>> ERP_TRange_ByRangeDesc(TRange item);
        Task<IList<TRange_Custom>> ERP_TRange_ByBrandCD(TRange item);
        Task<IList<TRange>> ERP_TRange_ByRangeID(TRange item);
    }
}

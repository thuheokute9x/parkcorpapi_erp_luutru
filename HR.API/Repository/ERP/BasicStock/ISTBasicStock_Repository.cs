﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.BasicStock
{
    interface ISTBasicStock_Repository : IGenericRepository<STBasicStock>
    {
        Task<Boolean> ERP_STBasicStock_InsertList(List<STBasicStock> item);
        Task<IList<Object>> ERP_STBasicStock_GetByStartYearStartMonth(dynamic item);
        DataTable ERP_STBasicStock_GetByStartYearStartMonth_Excel(dynamic item);
    }
}

﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.BasicStock
{
    public class STBasicStock_Repository : GenericRepository<STBasicStock>, ISTBasicStock_Repository
    {
        public STBasicStock_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public STBasicStock_Repository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<Boolean> ERP_STBasicStock_InsertList(List<STBasicStock> list)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            STBasicStock item = list.FirstOrDefault();
                            List<STBasicStock> deleteList =context.STBasicStocks.Where(x=>x.CloseYear== item.CloseYear
                            && x.CloseMonth== item.CloseMonth && x.StartYear==item.StartYear && x.StartMonth == item.StartMonth
                            ).ToList();
                            context.STBasicStocks.RemoveRange(deleteList);
                            this.BulkInsertNotSaveChange(context, list);
                            await context.SaveChangesAsync();
                            transaction.Commit();
                            return await Task.FromResult(true);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new NotImplementedException(ex.Message.ToString());
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_STBasicStock_GetByStartYearStartMonth(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ClosingYear", item.ClosingYear.Value);
                    queryParameters.Add("@ClosingMonth", item.ClosingMonth.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(@"SELECT ROW_NUMBER() OVER (ORDER BY MatCD)   RowID
,MatCD,Unit,Qty,Remark FROM dbo.STBasicStock (NOLOCK) 
WHERE CloseYear=@ClosingYear AND CloseMonth=@ClosingMonth ORDER BY MatCD"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_STBasicStock_GetByStartYearStartMonth_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand(@"SELECT MatCD,Unit,Qty,Remark FROM dbo.STBasicStock (NOLOCK) 
                WHERE CloseYear=@ClosingYear AND CloseMonth=@ClosingMonth ORDER BY MatCD", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ClosingYear", SqlDbType.Int).Value = item.ClosingYear.Value;
                cmd.Parameters.Add("@ClosingMonth", SqlDbType.Int).Value = item.ClosingMonth.Value;

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

    }
}
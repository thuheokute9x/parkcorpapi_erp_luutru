﻿using HR.API.IRepository;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.Prod
{
    interface ITProd_Repository : IGenericRepository<TProd>
    {
        Task<IList<TProd>> ERP_TProd_GetwithProdCodeLike(TProd item);
        Task<IList<TProd>> ERP_TProd_GetwithSKUCodeLike(TProd item);
        Task<IList<TProd>> ERP_TProd_GetColorCodeLike(TProd item);
        Task<IList<TProd>> ERP_TProd_GetStyleCodeLike(TProd item);
        Task<IList<TProd>> ERP_TProd_GetwithProdDescLike(TProd item);
        Task<IList<TProd>> ERP_TProd_GetwithProdColorLike(TProd item);
        Task<IList<Object>> ERP_TProd_GetPagesize(dynamic item);
        Task<IList<Object>> ERP_TStyleGrp_GetAll();
        Task<Result> ERP_TProd_Insert(TProd item);
        Task<object> ERP_TProd_Update(TProd item); 
         Task<TProd> ERP_TProd_GenerateProdCode();
        DataTable ERP_TProd_GetPagesize_Excel(dynamic item);
        Task<IList<Object>> ERP_TProd_Getwith_SKUCode_ProdCode_Like(TProd item);
        Task<bool> ERP_TProd_CheckTStyleMulti(TProd item);
        Task<Object> ERP_TProd_TStyle_UpdateProdDesc(TProd item);
        Task<Object> ERP_TProd_Update_TStyle_InsertUpdate(TProd item);
    }
}

﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using Jil;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.Prod
{
    public class TProd_Repository : GenericRepository<TProd>, ITProd_Repository
    {
        public TProd_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TProd_Repository(PARKERPEntities context)
        : base(context)
        {
        }
        public async Task<IList<TProd>> ERP_TProd_GetwithProdCodeLike(TProd item)//Nhớ sửa bên load all select Rediscache
        {
            try
            {
                IDatabase db = ConnectRedis();
                string key = GetDB() + "TProdSelect_GetAll";
                string valued = await db.StringGetAsync(key);
                item.ProdCode = item.ProdCode.Replace('p', 'P');
                if (!string.IsNullOrEmpty(valued))
                {
                    List<TProd> ListTProd = JSON.Deserialize<List<TProd>>(valued);
                    ListTProd = ListTProd.Where(x => x.ProdCode.Contains(item.ProdCode) || string.IsNullOrEmpty(item.ProdCode)).Take(15).ToList();
                    return ListTProd;
                }
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    string Query = "SELECT TOP(15)  ProdCode,ProdDesc FROM dbo.TProd (NOLOCK) WHERE(@ProdCode = '' OR(ProdCode LIKE '%' + @ProdCode + '%'))";
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdCode", item.ProdCode);
                    connection.Open();
                    var data = await connection.QueryAsync<TProd>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                IDatabase db = ConnectRedis();
                using (var connection = new SqlConnection(Settings.GetConnectionString(Settings.GetConnectionString("Company"))))
                    {

                        string Query = "SELECT ProdCode,ProdDesc FROM dbo.TProd (NOLOCK)";
                        var List = new DynamicParameters();
                        connection.Open();
                        var data = connection.Query<TProd>(Query, List, commandTimeout: 1500
                            , commandType: CommandType.Text);
                        string value = JsonConvert.SerializeObject(data);
                        db.StringSet(Settings.GetConnectionString("Company") + "TProdSelect_GetAll", value);

                    }                 
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        //public async Task<IList<TProd>> ERP_TProd_GetwithProdCodeLike(TProd item)//Nhớ sửa bên load all select
        //{
        //    try
        //    {
        //        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
        //        {
        //            string Query = "SELECT TOP(15)  ProdCode,ProdDesc FROM dbo.TProd (NOLOCK) WHERE(@ProdCode = '' OR(ProdCode LIKE '%' + @ProdCode + '%'))";
        //            var queryParameters = new DynamicParameters();
        //            queryParameters.Add("@ProdCode", item.ProdCode);
        //            connection.Open();
        //            var data = await connection.QueryAsync<TProd>(Query, queryParameters, commandTimeout: 1500
        //                , commandType: CommandType.Text);
        //            return data.ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new NotImplementedException(ex.Message.ToString());
        //    }
        //}
        public async Task<IList<TProd>> ERP_TProd_GetwithProdDescLike(TProd item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    string Query = "SELECT TOP(15)  ProdCode,ProdDesc FROM dbo.TProd (NOLOCK) WHERE(@ProdDesc = '' OR(ProdDesc LIKE '%' + @ProdDesc + '%'))";
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdDesc", item.ProdDesc);
                    connection.Open();
                    var data = await connection.QueryAsync<TProd>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TProd_Getwith_SKUCode_ProdCode_Like(TProd item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    string Query = "SELECT TOP(15) ISNULL(a.SKUCode,'') + ' (' + a.ProdCode + ')' SKUProdCode,a.ProdID,a.ProdCode,a.SKUCode,b.RangeDesc,a.ProdDesc,a.Color FROM dbo.TProd a (NOLOCK) LEFT JOIN dbo.TRange b (NOLOCK) ON b.RangeID = a.RangeID WHERE a.SKUCode LIKE N'%'+@SKUCode+'%' OR a.ProdCode LIKE N'%'+@SKUCode+'%' OR @SKUCode='' ";
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@SKUCode", item.SKUCode);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TProd>> ERP_TProd_GetwithProdColorLike(TProd item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    string Query = "SELECT TOP(15)  ProdCode,Color FROM dbo.TProd (NOLOCK) WHERE(@Color = '' OR(Color LIKE '%' + @Color + '%'))";
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Color", item.Color);
                    connection.Open();
                    var data = await connection.QueryAsync<TProd>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TProd>> ERP_TProd_GetwithSKUCodeLike(TProd item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@SKUCode", item.SKUCode);
                    connection.Open();
                    var data = await connection.QueryAsync<TProd>("ERP_TProd_GetwithSKUCodeLike", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TProd>> ERP_TProd_GetColorCodeLike(TProd item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ColorCode", item.ColorCode);
                    connection.Open();
                    var data = await connection.QueryAsync<TProd>("ERP_TProd_GetColorCodeLike", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TProd>> ERP_TProd_GetStyleCodeLike(TProd item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@StyleCode", item.StyleCode);
                    connection.Open();
                    var data = await connection.QueryAsync<TProd>("ERP_TProd_GetStyleCodeLike", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TProd_GetPagesize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ViewBOMVersion", item.ViewBOMVersion.Value);
                    queryParameters.Add("@CustGrpCD", item.CustGrpCD.Value);
                    queryParameters.Add("@BrandCD", item.BrandCD.Value);
                    queryParameters.Add("@RangeID", item.RangeID.Value);
                    queryParameters.Add("@SeasonID", item.SeasonID.Value);
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD.Value);

                    queryParameters.Add("@ProdCode", item.ProdCode.Value);
                    queryParameters.Add("@ProdDesc", item.ProdDesc.Value);
                    queryParameters.Add("@ShortDesc", item.ShortDesc.Value);
                    queryParameters.Add("@Color", item.Color.Value);
                    queryParameters.Add("@SKUCode", item.SKUCode.Value);

                    queryParameters.Add("@Discontinue", item.DiscontinueInt.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);                                          
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TProd_GetPagesize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TStyleGrp_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("SELECT StyleGrpID,StyleGrpDesc FROM dbo.TStyleGrp (NOLOCK)", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Result> ERP_TProd_Insert(TProd item)
        {
            try
            {
                Result dt = new Result();
                bool isExistStyleID = false;
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        try
                        {


                            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                            {
                                int StyleID = item.StyleID;
                                var queryParameters = new DynamicParameters();
                                connection.Open();
                                //AND Discontinue = 0
                                string query = "SELECT StyleID FROM dbo.TProd (NOLOCK) WHERE ProdDesc=@ProdDesc AND CustGrpCD=@CustGrpCD AND BrandCD=@BrandCD AND RangeID=@RangeID";
                                queryParameters.Add("@ProdDesc", item.ProdDesc);
                                queryParameters.Add("@CustGrpCD", item.CustGrpCD);
                                queryParameters.Add("@BrandCD", item.BrandCD);
                                queryParameters.Add("@RangeID", item.RangeID);

                                TProd tProd = await connection.QueryFirstOrDefaultAsync<TProd>(query, queryParameters, commandTimeout: 1500
                                    , commandType: CommandType.Text);
                                if (tProd!=null)
                                {
                                    StyleID = item.StyleID= tProd.StyleID;
                                    isExistStyleID = true;
                                }

                                if (tProd == null)
                                {
                                    TStyle tStyle = new TStyle();
                                    tStyle.StyleDesc = item.ProdDesc;
                                    tStyle.CreatedBy = int.Parse(GetUserlogin());
                                    tStyle.CreatedOn = DateTime.Now;
                                    tStyle.StyleDesc = item.ProdDesc;
                                    context.TStyles.Add(tStyle);
                                    await context.SaveChangesAsync();
                                    StyleID = tStyle.StyleID;
                                }
                                item.StyleID = StyleID;
                                item.CreatedBy = int.Parse(GetUserlogin());
                                item.CreatedOn = DateTime.Now;
                                context.TProds.Add(item);
                                await context.SaveChangesAsync();
                                transaction.Commit();



                                IDatabase db = ConnectRedis();
                                string Query = "SELECT ProdCode,ProdDesc FROM dbo.TProd (NOLOCK)";
                                var queryParametersRedist = new DynamicParameters();
                                var kq = connection.Query<TProd>(Query, queryParametersRedist, commandTimeout: 1500
                                    , commandType: CommandType.Text);
                                string value = JsonConvert.SerializeObject(kq);
                                await db.StringSetAsync(GetDB() +"TProdSelect_GetAll", value);

                                if (isExistStyleID)
                                {
                                    return  new Result { ErrorCode=1};//Có StyleID rồi
                                }
                                return new Result { ErrorCode = 0 };//Chưa có StyleID
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new NotImplementedException(ex.Message.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<object> ERP_TProd_Update(TProd item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                            {

                                TProd tProd = context.TProds.FirstOrDefault(x => x.ProdID == item.ProdID);
                                TStyle tStyle = new TStyle();
                                bool isChangeStyleID = false;

                                bool isExistStyleID = false;



                                //if (!isChangeStyleID)//trường hợp StyleID dùng cho 1 Pcode duy nhất thì cho update ProdDesc của Pcode và TStyle 
                                //{
                                //Nếu cập nhật ProdDesc mà đã có TStyle của ProdDesc rồi thì gán TStyle lại                                
                                TProd checkExistStyleID = context.TProds.FirstOrDefault(x => x.ProdID != item.ProdID
                                    && x.CustGrpCD == item.CustGrpCD && x.BrandCD == item.BrandCD
                                    && x.RangeID == item.RangeID && x.ProdDesc == item.ProdDesc);
                                    if (checkExistStyleID != null)//
                                    {
                                        tProd.StyleID = checkExistStyleID.StyleID;
                                        item.StyleID = checkExistStyleID.StyleID;
                                        isExistStyleID = true;
                                    }
                                    if (checkExistStyleID == null)//update ProdDesc của Pcode và TStyle
                                    {

                                        if (item.CustGrpCD != tProd.CustGrpCD || item.BrandCD != tProd.BrandCD
                                            || item.RangeID != tProd.RangeID)//trường hợp thay đổi CustGrpCD,BrandCD,RangeID
                                        {
                                            tStyle.StyleDesc = item.ProdDesc;
                                            tStyle.CreatedBy = int.Parse(GetUserlogin());
                                            tStyle.CreatedOn = DateTime.Now;
                                            context.TStyles.Add(tStyle);
                                            await context.SaveChangesAsync();
                                            isChangeStyleID = true;
                                        }
                                        if (!isChangeStyleID)
                                        {
                                            int? StyleID = item.StyleID;
                                            dynamic ob = new { StyleID = StyleID };
                                            //if (!(await this.ERP_TProd_CheckTStyleMulti(item)))
                                            //{
                                            TStyle tStyleUpdate = context.TStyles.FirstOrDefault(x => x.StyleID == item.StyleID);
                                            tStyleUpdate.StyleDesc = item.ProdDesc;
                                            tStyleUpdate.UpdatedBy = int.Parse(GetUserlogin());
                                            tStyleUpdate.UpdatedOn = DateTime.Now;
                                            context.Entry(tStyleUpdate).State = System.Data.Entity.EntityState.Modified;
                                            await context.SaveChangesAsync();
                                            isExistStyleID = false;
                                            //}
                                        }

                                    }
                                    tProd.ProdDesc = item.ProdDesc;


                                //}

                                tProd.ProdCode = item.ProdCode;

                                tProd.ShortDesc = item.ShortDesc;
                                tProd.Color = item.Color;
                                tProd.CustGrpCD = item.CustGrpCD;
                                tProd.BrandCD = item.BrandCD;
                                tProd.RangeID = item.RangeID;
                                tProd.Variation = item.Variation;
                                tProd.Class = item.Class;
                                tProd.StyleGrpID = item.StyleGrpID;
                                tProd.SeasonID = item.SeasonID;
                                tProd.MatTypeCD = item.MatTypeCD;

                                //if (item.StyleID == 0)//
                                //{
                                //    tProd.StyleID = tStyle.StyleID;
                                //}
                                if (isChangeStyleID)//trường hợp thay đổi CustGrpCD,BrandCD,RangeID thì cập nhật lại StyleID mới
                                {
                                    tProd.StyleID = tStyle.StyleID;
                                }

                                tProd.IntroDate = item.IntroDate;
                                tProd.UOM = item.UOM;
                                tProd.DevCode = item.DevCode;
                                tProd.SKUCode = item.SKUCode;
                                tProd.RangeCode = item.RangeCode;
                                tProd.StyleCode = item.StyleCode;
                                tProd.ColorCode = item.ColorCode;
                                tProd.ProdDimW = item.ProdDimW;
                                tProd.ProdDimH = item.ProdDimH;
                                tProd.ProdDimD = item.ProdDimD;
                                tProd.ProdDimUnit = item.ProdDimUnit;
                                tProd.BoxDimW = item.BoxDimW;
                                tProd.BoxDimH = item.BoxDimH;
                                tProd.BoxDimD = item.BoxDimD;
                                tProd.BoxDimUnit = item.BoxDimUnit;
                                tProd.PCPerCarton = item.PCPerCarton;
                                tProd.Print = item.Print;
                                tProd.CustCode1 = item.CustCode1;
                                tProd.CustCode2 = item.CustCode2;
                                tProd.CustCode3 = item.CustCode3;
                                tProd.NetWeight = item.NetWeight;
                                tProd.GrossWeight = item.GrossWeight;
                                tProd.CBM = item.CBM;
                                tProd.HSCode = item.HSCode;
                                tProd.Discontinue = item.Discontinue;


                                tProd.UpdatedBy = int.Parse(GetUserlogin());
                                tProd.UpdatedOn = DateTime.Now;
                                context.Entry(tProd).State = System.Data.Entity.EntityState.Modified;
                                await context.SaveChangesAsync();
                                transaction.Commit();


                                item.StyleID = tProd.StyleID;
                                dynamic kq = new ExpandoObject();
                                kq.data = item;
                                kq.checkExistStyleID = isExistStyleID;
                                return kq;
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new NotImplementedException(ex.Message.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        //public async Task<object> ERP_TProd_Update(TProd item)
        //{
        //    try
        //    {
        //        using (PARKERPEntities context = new PARKERPEntities())
        //        {
        //            using (var transaction = context.Database.BeginTransaction())
        //            {
        //                try
        //                {

        //                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
        //                    {
        //                        //TStyle tStyle = new TStyle();
        //                        //if (item.StyleID == 0)//trường hợp thay đổi CustGrpCD,BrandCD,RangeID
        //                        //{
        //                        //    tStyle.StyleDesc = item.ProdDesc;
        //                        //    tStyle.CreatedBy = int.Parse(GetUserlogin());
        //                        //    tStyle.CreatedOn = DateTime.Now;
        //                        //    context.TStyles.Add(tStyle);
        //                        //    await context.SaveChangesAsync();
        //                        //}

        //                        TProd tProd = context.TProds.FirstOrDefault(x => x.ProdID == item.ProdID);
        //                        TStyle tStyle = new TStyle();
        //                        bool isChangeStyleID = false;

        //                        bool isExistStyleID = false;

        //                        if (item.CustGrpCD != tProd.CustGrpCD || item.BrandCD != tProd.BrandCD 
        //                            || item.RangeID != tProd.RangeID)//trường hợp thay đổi CustGrpCD,BrandCD,RangeID
        //                        {
        //                            tStyle.StyleDesc = item.ProdDesc;
        //                            tStyle.CreatedBy = int.Parse(GetUserlogin());
        //                            tStyle.CreatedOn = DateTime.Now;
        //                            context.TStyles.Add(tStyle);
        //                            await context.SaveChangesAsync();
        //                            isChangeStyleID = true;
        //                        }

        //                        if (!isChangeStyleID)//trường hợp StyleID dùng cho 1 Pcode duy nhất thì cho update ProdDesc của Pcode và TStyle 
        //                        {
        //                            //Nếu cập nhật ProdDesc mà đã có TStyle của ProdDesc rồi thì gán TStyle lại                                
        //                            TProd checkExistStyleID = context.TProds.FirstOrDefault(x => x.ProdID != item.ProdID 
        //                            && x.CustGrpCD == item.CustGrpCD && x.BrandCD == item.BrandCD 
        //                            && x.RangeID == item.RangeID && x.ProdDesc==item.ProdDesc);
        //                            if (checkExistStyleID != null)//
        //                            {
        //                                tProd.StyleID = checkExistStyleID.StyleID;
        //                                item.StyleID = checkExistStyleID.StyleID;
        //                                isExistStyleID = true;
        //                            }
        //                            if (checkExistStyleID == null)//update ProdDesc của Pcode và TStyle
        //                            {
        //                                int? StyleID = item.StyleID;
        //                                dynamic ob = new { StyleID = StyleID };
        //                                if (!(await this.ERP_TProd_CheckTStyleMulti(item)))
        //                                {
        //                                    TStyle tStyleUpdate = context.TStyles.FirstOrDefault(x => x.StyleID == item.StyleID);
        //                                    tStyleUpdate.StyleDesc = item.ProdDesc;
        //                                    tStyleUpdate.UpdatedBy = int.Parse(GetUserlogin());
        //                                    tStyleUpdate.UpdatedOn = DateTime.Now;
        //                                    context.Entry(tStyleUpdate).State = System.Data.Entity.EntityState.Modified;
        //                                    await context.SaveChangesAsync();
        //                                    isExistStyleID = false;
        //                                }
        //                            }
        //                            tProd.ProdDesc = item.ProdDesc;


        //                        }

        //                        tProd.ProdCode=item.ProdCode;

        //                        tProd.ShortDesc = item.ShortDesc;
        //                        tProd.Color = item.Color;
        //                        tProd.CustGrpCD = item.CustGrpCD;
        //                        tProd.BrandCD = item.BrandCD;
        //                        tProd.RangeID = item.RangeID;
        //                        tProd.Variation = item.Variation;
        //                        tProd.Class = item.Class;
        //                        tProd.StyleGrpID = item.StyleGrpID;
        //                        tProd.SeasonID = item.SeasonID;
        //                        tProd.MatTypeCD = item.MatTypeCD;

        //                        //if (item.StyleID == 0)//
        //                        //{
        //                        //    tProd.StyleID = tStyle.StyleID;
        //                        //}
        //                        if (isChangeStyleID)//trường hợp thay đổi CustGrpCD,BrandCD,RangeID thì cập nhật lại StyleID mới
        //                        {
        //                            tProd.StyleID = tStyle.StyleID;
        //                        }

        //                        tProd.IntroDate = item.IntroDate;
        //                        tProd.UOM = item.UOM;
        //                        tProd.DevCode = item.DevCode;
        //                        tProd.SKUCode = item.SKUCode;
        //                        tProd.RangeCode = item.RangeCode;
        //                        tProd.StyleCode = item.StyleCode;
        //                        tProd.ColorCode = item.ColorCode;
        //                        tProd.ProdDimW = item.ProdDimW;
        //                        tProd.ProdDimH = item.ProdDimH;
        //                        tProd.ProdDimD = item.ProdDimD;
        //                        tProd.ProdDimUnit = item.ProdDimUnit;
        //                        tProd.BoxDimW = item.BoxDimW;
        //                        tProd.BoxDimH = item.BoxDimH;
        //                        tProd.BoxDimD = item.BoxDimD;
        //                        tProd.BoxDimUnit = item.BoxDimUnit;
        //                        tProd.PCPerCarton = item.PCPerCarton;
        //                        tProd.Print = item.Print;
        //                        tProd.CustCode1 = item.CustCode1;
        //                        tProd.CustCode2 = item.CustCode2;
        //                        tProd.CustCode3 = item.CustCode3;
        //                        tProd.NetWeight = item.NetWeight;
        //                        tProd.GrossWeight = item.GrossWeight;
        //                        tProd.CBM = item.CBM;
        //                        tProd.HSCode = item.HSCode;
        //                        tProd.Discontinue = item.Discontinue;


        //                        tProd.UpdatedBy = int.Parse(GetUserlogin());
        //                        tProd.UpdatedOn = DateTime.Now;
        //                        context.Entry(tProd).State = System.Data.Entity.EntityState.Modified;
        //                        await context.SaveChangesAsync();
        //                        transaction.Commit();


        //                        item.StyleID = tProd.StyleID;
        //                        dynamic kq = new ExpandoObject();
        //                        kq.data = item;
        //                        kq.checkExistStyleID = isExistStyleID;
        //                        return kq;
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    transaction.Rollback();
        //                    throw new NotImplementedException(ex.Message.ToString());
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new NotImplementedException(ex.Message.ToString());
        //    }
        //    //try
        //    //{
        //    //    //context.Entry(tmatgr).State = System.Data.Entity.EntityState.Modified;
        //    //    await this.Update_SaveChange(item);
        //    //    return await Task.FromResult(true);
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    throw new NotImplementedException(ex.Message.ToString());
        //    //}
        //}
        public async Task<TProd> ERP_TProd_GenerateProdCode()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    var data = await connection.QueryAsync<TProd>("ERP_TProd_GenerateProdCode"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TProd_GetPagesize_Excel(dynamic item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TProd_GetPagesize", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@ViewBOMVersion", SqlDbType.Bit).Value = item.ViewBOMVersion.Value;
            cmd.Parameters.Add("@CustGrpCD", SqlDbType.NVarChar).Value = item.CustGrpCD.Value;
            cmd.Parameters.Add("@BrandCD", SqlDbType.NVarChar).Value = item.BrandCD.Value;
            cmd.Parameters.Add("@RangeID", SqlDbType.Int).Value = item.RangeID.Value;
            cmd.Parameters.Add("@SeasonID", SqlDbType.Int).Value = item.SeasonID.Value;

            cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.ProdInstCD.Value;
            cmd.Parameters.Add("@ProdCode", SqlDbType.NVarChar).Value = item.ProdCode.Value;
            cmd.Parameters.Add("@ProdDesc", SqlDbType.NVarChar).Value = item.ProdDesc.Value;
            cmd.Parameters.Add("@ShortDesc", SqlDbType.NVarChar).Value = item.ShortDesc.Value;
            cmd.Parameters.Add("@Color", SqlDbType.NVarChar).Value = item.Color.Value;
            cmd.Parameters.Add("@SKUCode", SqlDbType.NVarChar).Value = item.SKUCode.Value;
            cmd.Parameters.Add("@Discontinue", SqlDbType.Int).Value = item.DiscontinueInt.Value;

            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;


        }
        public async Task<bool> ERP_TProd_CheckTStyleMulti(TProd item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    string Query = "SELECT COUNT(StyleID) StyleID FROM dbo.TProd (NOLOCK) WHERE StyleID=@StyleID GROUP BY StyleID";
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@StyleID", item.StyleID);
                    connection.Open();
                    var data = await connection.QueryAsync<TProd>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    int count = data.FirstOrDefault().StyleID;
                    if (count>1)
                    {
                        return await Task.FromResult(true);
                    }
                    return await Task.FromResult(false);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TProd_TStyle_UpdateProdDesc(TProd item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    using (PARKERPEntities context = new PARKERPEntities())
                    {
                        int StyleIDOld = item.StyleID;
                        TProd checkExistStyleID = context.TProds.FirstOrDefault(x => x.ProdID != item.ProdID
                        && x.CustGrpCD == item.CustGrpCD && x.BrandCD == item.BrandCD
                        && x.RangeID == item.RangeID && x.ProdDesc == item.ProdDesc);
                        if (checkExistStyleID != null)//
                        {
                            item.StyleID = checkExistStyleID.StyleID;
                        }


                        string Query = "UPDATE dbo.TProd SET StyleID=@StyleID,ProdDesc=@ProdDesc,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE StyleID=@StyleIDOld;" +
                            "UPDATE dbo.TStyle SET StyleDesc=@ProdDesc,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE StyleID=@StyleIDOld";
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@ProdDesc", item.ProdDesc);
                        queryParameters.Add("@StyleID", item.StyleID);
                        queryParameters.Add("@StyleIDOld", StyleIDOld);
                        queryParameters.Add("@UpdatedBy", int.Parse(GetUserlogin()));
                        connection.Open();
                        var data = await connection.QueryAsync<TProd>(Query, queryParameters, commandTimeout: 1500
                            , commandType: CommandType.Text);
                        return item;

                    }
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TProd_Update_TStyle_InsertUpdate(TProd item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                            {
                                //TStyle tStyle = new TStyle();
                                //tStyle.StyleDesc = item.ProdDesc;
                                //tStyle.CreatedBy = int.Parse(GetUserlogin());
                                //tStyle.CreatedOn = DateTime.Now;
                                //context.TStyles.Add(tStyle);

                                //await context.SaveChangesAsync();

                                TProd tProd = context.TProds.FirstOrDefault(x => x.ProdID == item.ProdID);

                                //Nếu cập nhật ProdDesc mà đã có TStyle của ProdDesc rồi thì gán TStyle lại                                
                                TProd checkExistStyleID = context.TProds.FirstOrDefault(x => x.ProdID != item.ProdID
                                && x.CustGrpCD == item.CustGrpCD && x.BrandCD == item.BrandCD
                                && x.RangeID == item.RangeID && x.ProdDesc == item.ProdDesc);
                                if (checkExistStyleID != null)//
                                {
                                    tProd.StyleID = checkExistStyleID.StyleID;
                                    item.StyleID = checkExistStyleID.StyleID;
                                }
                                TStyle tStyle = new TStyle();
                                if (checkExistStyleID == null)//
                                {
                                    tStyle.StyleDesc = item.ProdDesc;
                                    tStyle.CreatedBy = int.Parse(GetUserlogin());
                                    tStyle.CreatedOn = DateTime.Now;
                                    context.TStyles.Add(tStyle);

                                    await context.SaveChangesAsync();
                                    tProd.StyleID = tStyle.StyleID;

                                }


                                tProd.ProdDesc = item.ProdDesc;
                                tProd.UpdatedBy = int.Parse(GetUserlogin());
                                tProd.UpdatedOn = DateTime.Now;
                                context.Entry(tProd).State = System.Data.Entity.EntityState.Modified;
                                await context.SaveChangesAsync();
                                transaction.Commit();

                                item.StyleID = tProd.StyleID;

                                dynamic ob = new ExpandoObject();
                                ob.data = item;
                                ob.checkExistStyleID = checkExistStyleID==null?false:true;

                                return ob;
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new NotImplementedException(ex.Message.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
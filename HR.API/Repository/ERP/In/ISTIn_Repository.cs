﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.In
{
    interface ISTIn_Repository : IGenericRepository<STIn>
    {
        Task<IList<STIn>> ERP_STIn_GetwithIV(STIn STIn);
        Task<IList<STIn_List_Custom>> ERP_STIn_List_RGL_GetByVenDorSHIPPERIV(STIn_List_Custom GRDoc);
        Task<IList<STIn_List_Custom>> ERP_STin_List_GetPriceLast(STIn_List_Custom item);
        DataTable ERP_STin_List_GetPriceLastExcel(STIn_List_Custom item);
        Task<IList<Object>> ERP_STIn_GetPageSize(dynamic STIn);
        DataTable ERP_STIn_GetPageSize_Excel(dynamic item);
        Task<IList<Object>> ERP_STIn_List_GetPageSize(dynamic STIn);
        DataTable ERP_STIn_List_GetPageSize_Excel(dynamic item);
        Task<IList<Object>> ERP_STPlant_GetPLANTDESCLike(dynamic item);
        Task<string> ERP_STin_GenerateIV(STIn item);
        Task<bool> ERP_STin_Insert(STIn item);
    }
}

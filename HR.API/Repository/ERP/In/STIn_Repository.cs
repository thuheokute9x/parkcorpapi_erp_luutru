﻿using Dapper;
using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.PayDoc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.In
{
    public class STIn_Repository : GenericRepository<STIn>, ISTIn_Repository
    {
        public STIn_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public STIn_Repository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<STIn_List_Custom>> ERP_STIn_List_RGL_GetByVenDorSHIPPERIV(STIn_List_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@SHIPPER", item.SHIPPER);
                    queryParameters.Add("@VCD", item.VCD);
                    queryParameters.Add("@IV", item.IV);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<STIn_List_Custom>("ERP_STIn_List_RGL_GetByVenDorIV"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }

        public async Task<IList<STIn>> ERP_STIn_GetwithIV(STIn STIn)
        {
            try
            {
                List<STIn> result = new List<STIn>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@IV", string.IsNullOrEmpty(STIn.IV) ? "" : STIn.IV);
                    connection.Open();
                    var data = await connection.QueryAsync<STIn>("ERP_STIn_GetwithIV"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<IList<STIn_List_Custom>> ERP_STin_List_GetPriceLast(STIn_List_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCD", item.MATCD);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<STIn_List_Custom>("ERP_STin_List_GetPriceLast"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_STin_List_GetPriceLastExcel(STIn_List_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_STin_List_GetPriceLast", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MATCD;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_STIn_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@IV", item.IV.Value);
                    queryParameters.Add("@SHIPPER", item.SHIPPER.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_STIn_GetPageSize"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_STIn_GetPageSize_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_STIn_GetPageSize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@IV", SqlDbType.NVarChar).Value = item.IV.Value;
                cmd.Parameters.Add("@SHIPPER", SqlDbType.NVarChar).Value = item.SHIPPER.Value;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_STIn_List_GetPageSize(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@IVID", item.IVID.Value);
                    queryParameters.Add("@Type", item.Type.Value);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_STIn_List_GetPageSize"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_STIn_List_GetPageSize_Excel(dynamic item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_STIn_List_GetPageSize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@IVID", SqlDbType.NVarChar).Value = item.IVID.Value;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_STPlant_GetPLANTDESCLike(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PLANTDESC", item.PLANTDESC.Value);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("SELECT TOP 15 a.PLANTDESC, a.PLANTCD FROM dbo.STPlant (NOLOCK) a WHERE a.PLANTDESC LIKE '%'+@PLANTDESC+'%' OR @PLANTDESC=''  ORDER BY a.PLANTCD"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<string> ERP_STin_GenerateIV(STIn item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    STIn_List_Custom data = await connection.QueryFirstOrDefaultAsync<STIn_List_Custom>("SELECT '-'+CONVERT(NVARCHAR(50),COUNT(IVID)) IV FROM dbo.STIn (NOLOCK)"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return item.IV + data.IV;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_STin_Insert(STIn item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    STIn_List_Custom checkexist = await connection.QueryFirstOrDefaultAsync<STIn_List_Custom>("SELECT IV FROM dbo.STIn (NOLOCK) WHERE IV=@IV AND SHIPPER=@SHIPPER"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    if (checkexist!=null)
                    {
                        throw new NotImplementedException("your IV(" + item.IV + ") occurs in ERP. Please check your IV again.");
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
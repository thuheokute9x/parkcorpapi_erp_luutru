﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.ProdInstItem
{
    interface ITProdInstItem_Repository : IGenericRepository<TProdInstItem>
    {
        Task<IList<TProdInstItem_Custom>> ERP_TProdInstItem_ByProdInstCD(TProdInstItem item);
        DataTable ERP_TProdInstItem_GetByProdInstCD_Excel(dynamic item);
        Task<byte[]> ERP_TProdInstItem_ByProdInstCD_Excel(TProdInstItem dt);
        Task<IList<Object>> ERP_TProdInstItem_GetByProdInstCD(dynamic item);
        Task<IList<Object>> ERP_TProdInstItem_DeleteZeroQty_ReturnData(TProdInstItem_Custom item);
        Task<bool> ERP_TProdInstItem_TSalseInfo_InsertList(List<TProdInstItem> item);
        Task<Object> ERP_TProdInstItem_TSalseInfo_Insert(dynamic item);
        Task<bool> ERP_TProdInstItem_Update(TProdInstItem item);
        Task<Object> ERP_TProdInstItem_UpdateProdCode(TProdInstItem item);
        Task<Object> ERP_TSalseInfo_UpdateBuyerPONo(TSalseInfo item);
        Task<Object> ERP_TSalseInfo_UpdatePrice(dynamic item);
        Task<Object> ERP_TSalseInfo_UpdateQty(dynamic item);
        Task<Object> ERP_TProdInstItem_UpdateVersion(TProdInstItem item);
        Task<IList<Object>> ERP_TCust_GetAll();
        Task<Object> ERP_TSalseInfo_UpdateSoldTo(dynamic item);
        Task<Object> ERP_TSalseInfo_UpdateShipTo(dynamic item);
        Task<Object> ERP_TSalseInfo_UpdatePCPerCarton(dynamic item);
        Task<Object> ERP_TSalseInfo_UpdateRemark(dynamic item);
        Task<Object> ERP_TSalseInfo_UpdateSKUCodeBuyer(dynamic item);
        Task<Object> ERP_TSalseInfo_UpdateReqETD(dynamic item);
        DataTable ERP_TCust_GetCustIDCustDesc_Excel(ExcelPackage excel);
        string ERP_TSalseInfo_UpdateList(List<TSalseInfo> itemlist, string ViewAndUpdatePrice);
        Task<bool> ERP_TProdInstItem_Import(List<TProdInstItem_Custom> listitem);

    }
}

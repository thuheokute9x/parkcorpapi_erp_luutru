﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.ProdInstItem
{
    public class TProdInstItem_Repository : GenericRepository<TProdInstItem>, ITProdInstItem_Repository
    {
        public TProdInstItem_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TProdInstItem_Repository(PARKERPEntities context)
        : base(context)
        {
        }
        public async Task<IList<TProdInstItem_Custom>> ERP_TProdInstItem_ByProdInstCD(TProdInstItem item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TProdInstItem_Custom>("ERP_TProdInstItem_ByProdInstCD", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<byte[]> ERP_TProdInstItem_ByProdInstCD_Excel(TProdInstItem dt)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", dt.ProdInstCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TProdInstItem_Custom>("ERP_TProdInstItem_ByProdInstCD", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    //return data.ToList();


                    ExcelPackage excel = new ExcelPackage();
                    var workSheet = excel.Workbook.Worksheets.Add("ProdPrice");
                    workSheet.Cells[1, 1, 1, 4].Style.Font.Bold = true;
                    workSheet.Cells[1, 1].Value = "ProdOrder";
                    workSheet.Cells[1, 2].Value = "ProdCode";
                    workSheet.Cells[1, 3].Value = "FOB";
                    workSheet.Cells[1, 4].Value = "MatCost";

           
                    int recordIndex = 2;
                    foreach (TProdInstItem_Custom item in data.ToList())
                    {
                        workSheet.Cells[recordIndex, 1].Value = item.ProdInstCD;
                        workSheet.Cells[recordIndex, 2].Value = item.ProdCode;
                        workSheet.Cells[recordIndex, 3].Value = 0;
                        workSheet.Cells[recordIndex, 4].Value = 0;

                        recordIndex++;
                    }
                    return excel.GetAsByteArray();

                }

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TProdInstItem_GetByProdInstCD(dynamic item)//có gọi từ chổ khác
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdCodeList", item.ProdCodeList.Value);
                    queryParameters.Add("@SKUCodeList", item.SKUCodeList.Value);
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD.Value);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber.Value);
                    queryParameters.Add("@PageSize", item.PageSize.Value);
                    queryParameters.Add("@IsPrice", item.IsPrice.Value);

                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TProdInstItem_GetByProdInstCD", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TProdInstItem_GetByProdInstCD_Excel(dynamic item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("ERP_TProdInstItem_GetByProdInstCD", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.ProdInstCD.Value;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type.Value;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize.Value;
            cmd.Parameters.Add("@IsPrice", SqlDbType.Bit).Value = item.IsPrice.Value;
            cmd.Parameters.Add("@Company", SqlDbType.NVarChar).Value = GetDB();


            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;


        }
        public DataTable ERP_TCust_GetCustIDCustDesc_Excel(ExcelPackage excel)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("SELECT CustID,CustDesc FROM dbo.TCust (NOLOCK) WHERE Discontinue=0", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }


            var workSheet2 = excel.Workbook.Worksheets.Add("SoldTo_ShipTo");
            int start2 = 1;
            for (int i = 0; i < table.Columns.Count; i++)
            {
                workSheet2.Cells[1, start2, 1, start2].Style.Font.Bold = true;
                workSheet2.Cells[1, start2].Value = table.Columns[i].ColumnName.Replace("_Format2", "").Replace("_Format4", "").Replace("_Percent0", "").Replace("_Percent2", "").Replace("_Percent3", "")
                .Replace("_Double0", "").Replace("_Double1", "").Replace("_Double2", "").Replace("_Double3", "").Replace("_Double4", "");
                //workSheet2.Cells["A1:" + Columns[start2 - 1] + "1"].AutoFilter = true;
                start2++;
            }
            start2 = 2;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                for (int k = 0; k < table.Columns.Count; k++)
                {
                    if (table.Columns[k].ColumnName.ToString().Contains("_Percent0"))
                    {
                        Double value;
                        bool successfullyParsed = double.TryParse(table.Rows[i][k].ToString(), out value);
                        if (successfullyParsed)
                        {
                            workSheet2.Cells[start2 + i, k + 1].Value = value / 100;
                        }
                        workSheet2.Cells[start2 + i, k + 1].Style.Numberformat.Format = "#0%";
                        continue;
                    }
                    if (table.Columns[k].ColumnName.ToString().Contains("_Percent2"))
                    {
                        Double value;
                        bool successfullyParsed = double.TryParse(table.Rows[i][k].ToString(), out value);
                        if (successfullyParsed)
                        {
                            workSheet2.Cells[start2 + i, k + 1].Value = value / 100;
                        }
                        workSheet2.Cells[start2 + i, k + 1].Style.Numberformat.Format = "#0.00%";
                        continue;
                    }
                    if (table.Columns[k].ColumnName.ToString().Contains("_Percent3"))
                    {
                        Double value;
                        bool successfullyParsed = double.TryParse(table.Rows[i][k].ToString(), out value);
                        if (successfullyParsed)
                        {
                            workSheet2.Cells[start2 + i, k + 1].Value = value / 100;
                        }
                        workSheet2.Cells[start2 + i, k + 1].Style.Numberformat.Format = "#0.000%";
                        continue;
                    }
                    if (table.Columns[k].ColumnName.ToString().Contains("_Format2"))
                    {
                        workSheet2.Cells[start2 + i, k + 1].Value = table.Rows[i][k];
                        workSheet2.Cells[start2 + i, k + 1].Style.Numberformat.Format = "#0.00";
                        continue;
                    }
                    if (table.Columns[k].ColumnName.ToString().Contains("_Format4"))
                    {
                        workSheet2.Cells[start2 + i, k + 1].Value = table.Rows[i][k];
                        workSheet2.Cells[start2 + i, k + 1].Style.Numberformat.Format = "#0.0000";
                        continue;
                    }

                    workSheet2.Cells[start2 + i, k + 1].Value = table.Rows[i][k];
                }
            }

            return table;


        }
        public async Task<IList<Object>> ERP_TProdInstItem_DeleteZeroQty_ReturnData(TProdInstItem_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD);//có gọi ERP_TProdInstItem_GetByProdInstCD
                    queryParameters.Add("@SalseInfoID", item.SalseInfoID);//có gọi ERP_TProdInstItem_GetByProdInstCD 
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("ERP_TProdInstItem_DeleteZeroQty_ReturnData", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_TProdInstItem_TSalseInfo_InsertList(List<TProdInstItem> item)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                            {

                                foreach (TProdInstItem ob in item)
                                {
                                    ob.CreatedBy = int.Parse(GetUserlogin());
                                    ob.CreatedOn = DateTime.Now;
                                    context.TProdInstItems.Add(ob);
                                    await context.SaveChangesAsync();
                                    TSalseInfo tsalseInfo = new TSalseInfo();
                                    tsalseInfo.ProdInstItemID = ob.ProdInstItemID;
                                    tsalseInfo.Qty = 0;
                                    tsalseInfo.SoldTo = 0;
                                    tsalseInfo.ShipTo = 0;
                                    tsalseInfo.ReqETD = DateTime.Today;
                                    tsalseInfo.CreatedBy = int.Parse(GetUserlogin());
                                    tsalseInfo.CreatedOn = DateTime.Now;
                                    context.TSalseInfoes.Add(tsalseInfo);
                                    await context.SaveChangesAsync();
                                }

                                transaction.Commit();
                                return await Task.FromResult(true);
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new NotImplementedException(ex.Message.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TProdInstItem_TSalseInfo_Insert(dynamic ob)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                            {
                                //TProdInstItem item = new TProdInstItem();



                                //item.ProdInstCD = ob.ProdInstCD.Value;
                                //item.ProdID = int.Parse(ob.ProdID.Value.ToString());
                                //item.RangeID = ob.RangeID.Value==null? null : int.Parse(ob.RangeID.Value.ToString());
                                //item.Version = ob.Version.Value==null? null : short.Parse(ob.Version.Value.ToString());
                                //item.BOMID = ob.BOMID.Value==null? null : int.Parse(ob.BOMID.Value.ToString());
                                //item.ConsID = ob.ConsID.Value==null? null : int.Parse(ob.ConsID.Value.ToString());
                                //item.CreatedBy= int.Parse(GetUserlogin());
                                //item.CreatedOn = DateTime.Now;



                                //    context.TProdInstItems.Add(item);
                                    //await context.SaveChangesAsync();
                                    TSalseInfo tsalseInfo = new TSalseInfo();
                                    tsalseInfo.ProdInstItemID = int.Parse(ob.ProdInstItemID.Value.ToString());
                                    tsalseInfo.Qty = 0;
                                    tsalseInfo.SoldTo = 0;
                                    tsalseInfo.ShipTo = 0;
                                    tsalseInfo.ReqETD = DateTime.Today;
                                    tsalseInfo.CreatedBy = int.Parse(GetUserlogin());
                                    tsalseInfo.CreatedOn = DateTime.Now;
                                    context.TSalseInfoes.Add(tsalseInfo);
                                    await context.SaveChangesAsync();
                                
                                transaction.Commit();

                                ob.ProdInstCD = ob.ProdInstCD.Value;
                                ob.Type = 0;
                                ob.PageNumber = 1;
                                ob.PageSize = 100;
                                ob.ProdCodeList = "";
                                ob.SKUCodeList = "";
                                ob.IsPrice = ob.IsPrice.Value;
                                return await ERP_TProdInstItem_GetByProdInstCD(ob);
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new NotImplementedException(ex.Message.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<bool> ERP_TProdInstItem_Update(TProdInstItem item)
        {
            try
            {
                await this.Update_SaveChange(item);
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TProdInstItem_UpdateProdCode(TProdInstItem item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE TProdInstItem SET ProdID=@ProdID,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE ProdInstItemID=@ProdInstItemID";
                    queryParameters.Add("@ProdID", item.ProdID);
                    queryParameters.Add("@ProdInstItemID", item.ProdInstItemID);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    var queryParametersgetProd = new DynamicParameters();
                    string querygetProd = "SELECT a.ProdDesc,b.RangeDesc,a.Color,REPLACE(convert(varchar, GETDATE(), 106),' ','/') UpdatedOn FROM dbo.TProd a (NOLOCK) LEFT JOIN dbo.TRange b (NOLOCK) ON a.RangeID=b.RangeID WHERE a.ProdID=@ProdID";
                    queryParametersgetProd.Add("@ProdID", item.ProdID);

                    var ob = await connection.QueryFirstAsync<Object>(querygetProd, queryParametersgetProd, commandTimeout: 1500
                        , commandType: CommandType.Text);
       
                    return ob;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TProdInstItem_UpdateVersion(TProdInstItem item)
        {
            try
            {

                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {


                    var queryParametersTBOM = new DynamicParameters();
                    string queryTBOM = "SELECT BOMID FROM dbo.TBOM (NOLOCK) WHERE ProdID=@ProdID AND Version=@Version";
                    queryParametersTBOM.Add("@Version", item.Version);
                    queryParametersTBOM.Add("@ProdID", item.@ProdID);
                    connection.Open();
                    var TBOM = await connection.QueryAsync<TBOM>(queryTBOM, queryParametersTBOM, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    TBOM tbom = TBOM.FirstOrDefault();

                    int? BOMID = null;
                    int? ConsID = null;

                    if (tbom != null)
                    {
                        BOMID = tbom.BOMID;
                        var queryParametersTCons = new DynamicParameters();
                        string queryTCons = "SELECT ConsID FROM dbo.TCons (NOLOCK) WHERE ProdID=@ProdID AND BOMID=@BOMID";
                        queryParametersTBOM.Add("@BOMID", BOMID);
                        queryParametersTBOM.Add("@ProdID", item.@ProdID);
                        var TCons = await connection.QueryAsync<TCon>(queryTCons, queryParametersTBOM, commandTimeout: 1500
                            , commandType: CommandType.Text);
                        TCon tCon = TCons.FirstOrDefault();
                        if (tCon != null)
                        {
                            ConsID = tCon.ConsID;
                        }
                    }

                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE TProdInstItem SET Version=@Version,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE(),BOMID=@BOMID,ConsID=@ConsID WHERE ProdInstItemID=@ProdInstItemID";
                    queryParameters.Add("@Version", item.Version);
                    queryParameters.Add("@ProdInstItemID", item.ProdInstItemID);
                    queryParameters.Add("@BOMID", BOMID);
                    queryParameters.Add("@ConsID", ConsID);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);



                    var queryParametersUpdatedOn = new DynamicParameters();
                    string queryQtySum = "SELECT REPLACE(convert(varchar, GETDATE(), 106),' ','/') UpdatedOn";
                    var kq = await connection.QueryFirstAsync<Object>(queryQtySum, queryParametersUpdatedOn, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    return kq;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TSalseInfo_UpdateBuyerPONo(TSalseInfo item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE TSalseInfo SET BuyerPONo=@BuyerPONo,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE SalseInfoID=@SalseInfoID;SELECT REPLACE(convert(varchar, GETDATE(), 106),' ','/') UpdatedOn";
                    queryParameters.Add("@SalseInfoID", item.SalseInfoID);
                    queryParameters.Add("@BuyerPONo", item.BuyerPONo);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryFirstAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TSalseInfo_UpdatePrice(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE TSalseInfo SET Price=@Price,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE SalseInfoID=@SalseInfoID";
                    queryParameters.Add("@SalseInfoID", item.SalseInfoID.Value);
                    queryParameters.Add("@Price", item.Price.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    
                    var queryParametersUpdatedOn = new DynamicParameters();
                    string queryQtySum = "SELECT REPLACE(convert(varchar, GETDATE(), 106),' ','/') UpdatedOn";
                    var kq = await connection.QueryFirstAsync<Object>(queryQtySum, queryParametersUpdatedOn, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    return kq;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TSalseInfo_UpdateQty(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE TSalseInfo SET Qty=@Qty,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE SalseInfoID=@SalseInfoID";
                    queryParameters.Add("@SalseInfoID", item.SalseInfoID.Value);
                    queryParameters.Add("@Qty", item.Qty.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);



                    var queryParametersQtySum = new DynamicParameters();
                    string queryQtySum = "SELECT QtySum=SUM(b.Qty),REPLACE(convert(varchar, GETDATE(), 106),' ','/') UpdatedOn FROM dbo.TProdInstItem a (NOLOCK) INNER JOIN dbo.TSalseInfo b (NOLOCK) ON b.ProdInstItemID = a.ProdInstItemID WHERE a.ProdInstCD=@ProdInstCD";
                    queryParametersQtySum.Add("@ProdInstCD", item.ProdInstCD.Value);
                    var kq = await connection.QueryFirstAsync<Object>(queryQtySum, queryParametersQtySum, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    return kq;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TSalseInfo_UpdateSoldTo(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE TSalseInfo SET SoldTo=@SoldTo,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE SalseInfoID=@SalseInfoID";
                    queryParameters.Add("@SalseInfoID", item.SalseInfoID.Value);
                    queryParameters.Add("@SoldTo", item.SoldTo.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);



                    var queryParameters_TCust = new DynamicParameters();
                    string queryTCust = "SELECT CustDesc,REPLACE(convert(varchar, GETDATE(), 106),' ','/') UpdatedOn FROM dbo.TCust (NOLOCK) WHERE CustID=@SoldTo";
                    queryParameters_TCust.Add("@SoldTo", item.SoldTo.Value);
                    var CustDesc = await connection.QueryFirstAsync<Object>(queryTCust, queryParameters_TCust, commandTimeout: 1500
                        , commandType: CommandType.Text);


                    return CustDesc;

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TSalseInfo_UpdateShipTo(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE TSalseInfo SET ShipTo=@ShipTo,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE SalseInfoID=@SalseInfoID";
                    queryParameters.Add("@SalseInfoID", item.SalseInfoID.Value);
                    queryParameters.Add("@ShipTo", item.ShipTo.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);



                    var queryParameters_TCust = new DynamicParameters();
                    string queryTCust = "SELECT CustDesc,REPLACE(convert(varchar, GETDATE(), 106),' ','/') UpdatedOn FROM dbo.TCust (NOLOCK) WHERE CustID=@ShipTo";
                    queryParameters_TCust.Add("@ShipTo", item.ShipTo.Value);
                    var CustDesc = await connection.QueryFirstAsync<Object>(queryTCust, queryParameters_TCust, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    return CustDesc;

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TSalseInfo_UpdatePCPerCarton(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE TSalseInfo SET PCPerCarton=@PCPerCarton,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE SalseInfoID=@SalseInfoID";
                    queryParameters.Add("@SalseInfoID", item.SalseInfoID.Value);
                    queryParameters.Add("@PCPerCarton", item.PCPerCarton.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);


                    var queryParametersUpdatedOn = new DynamicParameters();
                    string queryQtySum = "SELECT REPLACE(convert(varchar, GETDATE(), 106),' ','/') UpdatedOn";
                    var kq = await connection.QueryFirstAsync<Object>(queryQtySum, queryParametersUpdatedOn, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    return kq;




                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TSalseInfo_UpdateRemark(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE TSalseInfo SET Remark=@Remark,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE SalseInfoID=@SalseInfoID";
                    queryParameters.Add("@SalseInfoID", item.SalseInfoID.Value);
                    queryParameters.Add("@Remark", item.Remark.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    var queryParametersUpdatedOn = new DynamicParameters();
                    string queryQtySum = "SELECT REPLACE(convert(varchar, GETDATE(), 106),' ','/') UpdatedOn";
                    var kq = await connection.QueryFirstAsync<Object>(queryQtySum, queryParametersUpdatedOn, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    return kq;

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TSalseInfo_UpdateSKUCodeBuyer(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE TSalseInfo SET SKUCodeBuyer=@SKUCodeBuyer,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE SalseInfoID=@SalseInfoID";
                    queryParameters.Add("@SalseInfoID", item.SalseInfoID.Value);
                    queryParameters.Add("@SKUCodeBuyer", item.SKUCodeBuyer.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    var queryParametersUpdatedOn = new DynamicParameters();
                    string queryQtySum = "SELECT REPLACE(convert(varchar, GETDATE(), 106),' ','/') UpdatedOn";
                    var kq = await connection.QueryFirstAsync<Object>(queryQtySum, queryParametersUpdatedOn, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    return kq;

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<Object> ERP_TSalseInfo_UpdateReqETD(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "UPDATE TSalseInfo SET ReqETD=@ReqETD,UpdatedBy=@UpdatedBy,UpdatedOn=GETDATE() WHERE SalseInfoID=@SalseInfoID";
                    queryParameters.Add("@SalseInfoID", item.SalseInfoID.Value);
                    queryParameters.Add("@ReqETD", item.ReqETD.Value);
                    queryParameters.Add("@UpdatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);



                    var queryParametersUpdatedOn = new DynamicParameters();
                    string queryQtySum = "SELECT REPLACE(convert(varchar, GETDATE(), 106),' ','/') UpdatedOn";
                    var kq = await connection.QueryFirstAsync<Object>(queryQtySum, queryParametersUpdatedOn, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    return kq;

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TCust_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string query = "SELECT CONVERT(NVARCHAR,CustID) CustID,CustDesc FROM dbo.TCust (NOLOCK)";
                    connection.Open();
                    var data = await connection.QueryAsync<Object>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);

                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public string ERP_TSalseInfo_UpdateList(List<TSalseInfo> itemlist,string ViewAndUpdatePrice)
        {
            using (PARKERPEntities context = new PARKERPEntities())
            {
                using (var transaction = context.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        foreach (TSalseInfo item in itemlist)
                        {
                            TSalseInfo itemupdate=context.TSalseInfoes.FirstOrDefault(x=>x.SalseInfoID== item.SalseInfoID);
                            if (itemupdate==null)
                            {
                                throw new NotImplementedException("Not find SalseInfoID " + item.SalseInfoID.ToString());
                            }
                            itemupdate.BuyerPONo = item.BuyerPONo;
                            itemupdate.Price = ViewAndUpdatePrice=="true"?item.Price: itemupdate.Price;
                            itemupdate.Qty = item.Qty;
                            itemupdate.SoldTo = item.SoldTo;
                            itemupdate.ShipTo = item.ShipTo;
                            itemupdate.ReqETD = item.ReqETD;
                            itemupdate.PCPerCarton = item.PCPerCarton;
                            itemupdate.Remark = item.Remark;
                            itemupdate.SKUCodeBuyer = item.SKUCodeBuyer;
                            itemupdate.UpdatedBy = int.Parse(GetUserlogin());
                            itemupdate.SKUCodeBuyer = item.SKUCodeBuyer;

                        }
                        context.SaveChanges();

                        transaction.Commit();
                        return "";
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new NotImplementedException(ex.Message.ToString());
                    }
                }
            }
        }
        public async Task<bool> ERP_TProdInstItem_Import(List<TProdInstItem_Custom> listitem)
        {
            try
            {
                using (PARKERPEntities context = new PARKERPEntities())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                            {


                                var ListTProd = (from c in listitem//check trùng ProdCode và Prod Order
                                                  group c by new
                                                  {
                                                      c.ProdCode
                                                  } into gcs
                                                  //where gcs.Count() > 1
                                                  select new TProd()
                                                  {
                                                      ProdCode = gcs.Key.ProdCode,
                                                  }).ToList();

                                foreach (TProd ob in ListTProd)
                                {


                                    List<TProdInstItem_Custom> listTProdInstItemInsert = listitem.Where(x=>x.ProdCode==ob.ProdCode).ToList();
                                    TProdInstItem_Custom TProdInstItemInsert = listitem.FirstOrDefault(x => x.ProdCode == ob.ProdCode);
                                    TProdInstItem tProdInstItem = new TProdInstItem();

                                    tProdInstItem.ProdInstCD = TProdInstItemInsert.ProdInstCD;
                                    tProdInstItem.ProdID = TProdInstItemInsert.ProdID;
                                    tProdInstItem.RangeID = TProdInstItemInsert.RangeID;
                                    tProdInstItem.CreatedBy = int.Parse(GetUserlogin());
                                    tProdInstItem.CreatedOn =DateTime.Now;
                                    context.TProdInstItems.Add(tProdInstItem);
                                    await context.SaveChangesAsync();

                                    foreach (TProdInstItem_Custom childrent in listTProdInstItemInsert)
                                    {

                                        TSalseInfo salseInfo=new TSalseInfo();
                                        salseInfo.ProdInstItemID = tProdInstItem.ProdInstItemID;
                                        salseInfo.BuyerPONo = childrent.BuyerPONo;
                                        salseInfo.Qty = childrent.Qty;
                                        salseInfo.SoldTo = childrent.SoldTo;
                                        salseInfo.ShipTo = childrent.ShipTo;
                                        salseInfo.ReqETD = childrent.ReqETD;
                                        salseInfo.SKUCodeBuyer = childrent.SKUCodeBuyer;
                                        salseInfo.Remark = childrent.Remark;
                                        salseInfo.CreatedBy = int.Parse(GetUserlogin());
                                        salseInfo.CreatedOn = DateTime.Now;
                                        context.TSalseInfoes.Add(salseInfo);
                                        await context.SaveChangesAsync();
                                    }


                                }

                                transaction.Commit();
                                return await Task.FromResult(true);
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new NotImplementedException(ex.Message.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
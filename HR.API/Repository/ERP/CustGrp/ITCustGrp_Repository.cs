﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.ERP.CustGrp
{
    interface ITCustGrp_Repository : IGenericRepository<TCustGrp>
    {
        Task<IList<Object>> ERP_TCustGrp_GetAll();
        Task<IList<Object>> ERP_TCust_GetByCustGrpCD(TCust item);
        Task<IList<Object>> ERP_TCust_Insert(TCust item);
        Task<IList<Object>> ERP_TCust_Update(TCust_Custom item);
        Task<IList<Object>> ERP_TCustGrp_GetAllWithAddItemNull();
    }
}

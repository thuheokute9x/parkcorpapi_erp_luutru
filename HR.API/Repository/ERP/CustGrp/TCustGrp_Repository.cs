﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.ERP.CustGrp
{
    public class TCustGrp_Repository : GenericRepository<TCustGrp>, ITCustGrp_Repository
    {
        public TCustGrp_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TCustGrp_Repository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<IList<Object>> ERP_TCustGrp_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT CONVERT(BIT,0) expand,* FROM dbo.TCustGrp (NOLOCK) ORDER BY CustGrpCD";
                    var data = await connection.QueryAsync<Object>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }

        public async Task<IList<Object>> ERP_TCustGrp_GetAllWithAddItemNull()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "ERP_TCustGrp_GetAllWithAddItemNull";
                    var data = await connection.QueryAsync<Object>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TCust_GetByCustGrpCD(TCust item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    queryParameters.Add("@CustGrpCD", item.CustGrpCD);
                    string query = "ERP_TCust_GetByCustGrpCD";
                    var data = await connection.QueryAsync<Object>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TCust_Insert(TCust item)
        {
            try
            {
                this.Context.Entry(item).State = System.Data.Entity.EntityState.Added;
                await this.Context.SaveChangesAsync();
                return await this.ERP_TCust_GetByCustGrpCD(item);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<IList<Object>> ERP_TCust_Update(TCust_Custom item)
        {
            try
            {
                TCust ob=this.Context.TCusts.FirstOrDefault(x=>x.CustID==item.CustID);
                if (ob!=null)
                {
                    ob.CustGrpCD = item.CustGrpCD;
                    ob.CustDesc = item.CustDesc;
                    ob.Discontinue = item.Discontinue;
                    ob.UpdatedBy = int.Parse(GetUserlogin());
                    ob.UpdatedOn = DateTime.Now;
                }
                await this.Context.SaveChangesAsync();
                item.CustGrpCD = item.CustGrpCD_OLD;//goi cai cu truoc do moi chinh xac
                return await this.ERP_TCust_GetByCustGrpCD(item);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
    }
}
﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.PayReq_List
{
    interface ISTPayReq_List_Repository : IGenericRepository<STPayReq_List>
    {
        Task<IList<STPayReq_List>> ERP_STPayReq_List_GetPayDocNo(STPayReq_List STPayReq_List);//Đặt sai tên ERP_STPayDoc_GetPayDocNo
        Task<IList<RglPayReq_DocGrp>> ERP_STPayReq_List_ByPayReqID(string PayReqID);
        Task<STPayReq_List> ERP_STPayReq_List_ByPayDocNo(STPayReq_List STPayReq_List);
    }
}

﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.PayReq_List
{
    public class STPayReq_List_Repository : GenericRepository<STPayReq_List>, ISTPayReq_List_Repository
    {
        public STPayReq_List_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public STPayReq_List_Repository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<STPayReq_List> ERP_STPayReq_List_ByPayDocNo(STPayReq_List STPayReq_List)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    string query = "SELECT PayReqListNo,PBGRDocID,PAGRDocID,PayReqID,PayType,PayDocCD,PayDocNo" +
                    ",IVListNo,MatPoItemID,MatCD,Grade,PUnit,Qty,Price,Amount,Curr,ETD,ETA,PriceEx,CurrEx,ExRate" +
                    ",Remark FROM dbo.STPayReq_List (NOLOCK) WHERE PayDocNo='"+ STPayReq_List.PayDocNo + "'";
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<STPayReq_List>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<STPayReq_List>> ERP_STPayReq_List_GetPayDocNo(STPayReq_List STPayReq_List)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PayDocNo", string.IsNullOrEmpty(STPayReq_List.PayDocNo) ? "" : STPayReq_List.PayDocNo);
                    connection.Open();
                    var data = await connection.QueryAsync<STPayReq_List>("ERP_STPayReq_List_GetPayDocNo"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<RglPayReq_DocGrp>> ERP_STPayReq_List_ByPayReqID(string PayReqID)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PayReqID", PayReqID);
                    connection.Open();
                    var data = await connection.QueryAsync<RglPayReq_DocGrp>("ERP_STPayReq_List_ByPayReqID"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
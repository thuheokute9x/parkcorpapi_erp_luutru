﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.Season
{
    interface ITSeason_Repository : IGenericRepository<TSeason>
    {
        Task<IList<TSeason>> ERP_TSeason_GetAll();      
        Task<IList<TSeason>> ERP_TSeason_BySeasonDesc(TSeason item);
    }
}

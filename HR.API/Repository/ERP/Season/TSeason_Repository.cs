﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.Season
{
    public class TSeason_Repository : GenericRepository<TSeason>, ITSeason_Repository
    {
        public TSeason_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
        : base(unitOfWork)
        {
        }
        public TSeason_Repository(PARKERPEntities context)
        : base(context)
        {
        }

        public async Task<IList<TSeason>> ERP_TSeason_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    string Query = "SELECT SeasonID,SeasonDesc,Discontinue,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn FROM dbo.TSeason (NOLOCK)";
                    connection.Open();
                    var data = await connection.QueryAsync<TSeason>(Query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TSeason>> ERP_TSeason_BySeasonDesc(TSeason item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT SeasonID,SeasonDesc FROM dbo.TSeason (NOLOCK) WHERE SeasonDesc LIKE N'%" + item.SeasonDesc + "%' OR '" + item.SeasonDesc + "'='' ORDER BY SeasonDesc ";
                    var data = await connection.QueryAsync<TSeason>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
    }
}
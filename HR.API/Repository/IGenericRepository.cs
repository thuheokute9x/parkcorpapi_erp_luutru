﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.IRepository
{
    public interface IGenericRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(object id);
        void Insert(T obj);
        Task<bool> Insert_SaveChange(T obj);
        Task<T> Insert_ReturnOBJ(T obj);
        Task<bool> Update(T obj);
        Task<bool> Update_SaveChange(T obj);
        Task<bool> Delete(T obj); 
        Task<bool> Delete_SaveChange(T obj);

    }
}

﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.WIB.MatLoss;
using HR.API.Models.WIB.ProdPrice;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.WIP
{
    interface ITTMatLoss_Repository : IGenericRepository<TTMatLoss>
    {
        Task<Boolean> WIP_TTMatLoss_Insert(TTMatLoss item);
        Task<IList<TTMatLoss_Custom>> WIP_TTMatLoss_PageSize(TTMatLoss_Custom item);
        DataTable WIP_TTMatLoss_PageSize_Excel(TTMatLoss_Custom item);
    }
}

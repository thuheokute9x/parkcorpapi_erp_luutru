﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.WIB.ProdPrice;
using HR.API.Models.WIB.WIPMat;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.WIP
{
    public class TTWIPMat_Repository : GenericRepository<TTWIPMat>, ITTWIPMat_Repository
    {
        public TTWIPMat_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TTWIPMat_Repository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_PageSize(TTWIPMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    if (item.Date!=null)
                    {
                        queryParameters.Add("@Date", item.Date,DbType.DateTime);
                    }
                    queryParameters.Add("@RefVKNo", item.ProdInstCD);
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TTWIPMat_Custom>("WIP_TTWIPMat_PageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable WIP_TTWIPMat_PageSize_Excel(TTWIPMat_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("WIP_TTWIPMat_PageSize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                if (item.Date != null)
                {
                    cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value= item.Date;
                }
                cmd.Parameters.Add("@RefVKNo", SqlDbType.NVarChar).Value = item.ProdInstCD;
                cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_ByRefVKNo(TTWIPMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RefVKNo", item.ProdInstCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TTWIPMat_Custom>("WIP_TTWIPMat_ByRefVKNo", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_ByMatCD(TTWIPMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCD", item.MatCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TTWIPMat_Custom>("WIP_TTWIPMat_ByMatCD", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_Get_UnCutSubStorePageSize(TTWIPMat_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@MatCD", item.MatCD);
                    if (item.Date != null)
                    {
                        queryParameters.Add("@Date", item.Date);
                    }

                    queryParameters.Add("@RefVKNo", item.ProdInstCD);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TTWIPMat_Custom>("WIP_TTWIPMat_Get_UnCutSubStorePageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable WIP_TTWIPMat_Get_UnCutSubStorePageSize_Excel(TTWIPMat_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("WIP_TTWIPMat_Get_UnCutSubStorePageSize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                if (item.Date != null)
                {
                    cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = item.Date;
                }
                cmd.Parameters.Add("@RefVKNo", SqlDbType.NVarChar).Value = item.ProdInstCD;
                cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
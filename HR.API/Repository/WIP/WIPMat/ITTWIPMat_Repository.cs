﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.WIB.WIPMat;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.WIP
{
    interface ITTWIPMat_Repository : IGenericRepository<TTWIPMat>
    {
        Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_ByRefVKNo(TTWIPMat_Custom item);
        Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_ByMatCD(TTWIPMat_Custom item);
        Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_PageSize(TTWIPMat_Custom item);
        DataTable WIP_TTWIPMat_PageSize_Excel(TTWIPMat_Custom item);
        Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_Get_UnCutSubStorePageSize(TTWIPMat_Custom item);
        DataTable WIP_TTWIPMat_Get_UnCutSubStorePageSize_Excel(TTWIPMat_Custom item);
    }
}

﻿using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.WIB.ProdPrice;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.WIP
{
    interface ITTProdPrice_Repository : IGenericRepository<TTProdPrice>
    {
        Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_Pagesize(TTProdPrice_Custom item);
        Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_ByProdInstCD(TTProdPrice_Custom item);
        Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_ByBrandCD();
        Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_ProdCode(TTProdPrice_Custom item);
        Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_SKUCode(TTProdPrice_Custom item);
        DataTable ERP_TTProdPrice_Pagesize_Excel(TTProdPrice_Custom item);
    }
}

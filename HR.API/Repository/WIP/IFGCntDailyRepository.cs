﻿using HR.API.Controllers;
using HR.API.IRepository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Repository.WIP
{
    interface IFGCntDailyRepository:IGenericRepository<TTFGCntDaily>
    {
        Task<IList<Object>> WIP_Get_Insert_Del_TTFGCntDaily(TTFGCntDaily_Custom item);
        Task<bool> ERP_WIP_TTFGCntDaily_Update(TTFGCntDaily item);
        Task<IList<TTFGCntDaily>> WIP_TTFGCntDaily_GetProdOrder(TTFGCntDaily item); 
        Task<IList<TTFGCntDaily>> WIP_TTFGCntDaily_GetProdCode(TTFGCntDaily item);
        DataTable WIP_TTFGCntDaily_GetExcel(TTFGCntDaily_Custom item);
        DataTable WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup_Excel(TTFGCntDaily_Custom item);
        DataTable WIP_TTFGCntDaily_GetConsExcel(WIB_FG_GR_ConsModel item);       
        Task<IList<WIB_FG_GR_ConsModel>> WIP_TTFGCntDaily_GetFG_GR_Cons(WIB_FG_GR_ConsModel item);
        Task<IList<TTFGCntDaily_Custom>> WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup(TTFGCntDaily_Custom item);
        Task<IList<TTFGCntDaily_Custom>> ERP_WIP_TTFGCntDaily_Delete(TTFGCntDaily_Custom item);

    }
}

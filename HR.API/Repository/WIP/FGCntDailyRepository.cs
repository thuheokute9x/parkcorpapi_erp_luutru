﻿using Dapper;

using HR.API.Controllers;
using HR.API.IRepository;
using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models.WIB.StoreViewModel.GI;
using HR.API.Repository;
using HR.API.Repository.PayDoc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.WIP
{
    public class FGCntDailyRepository : GenericRepository<TTFGCntDaily>, IFGCntDailyRepository
    {
        public FGCntDailyRepository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public FGCntDailyRepository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<IList<TTFGCntDaily_Custom>> ERP_WIP_TTFGCntDaily_Delete(TTFGCntDaily_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    //queryParameters.Add("@FGDate", item.FGDate);
                    //queryParameters.Add("@ProdOrder", item.ProdOrder);
                    //queryParameters.Add("@ProdCode", item.ProdCode);
                    //queryParameters.Add("@CreatedBy", item.UpdatedBy);
                    queryParameters.Add("@FGCntDailyID", item.FGCntDailyID);
                    queryParameters.Add("@Type", 6);
                    connection.Open();
                    var data = await connection.QueryAsync<TTFGCntDaily_Custom>("WIP_Get_Insert_Del_TTFGCntDaily", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<IList<Object>> WIP_Get_Insert_Del_TTFGCntDaily(TTFGCntDaily_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    if (item.FromDate != "null" && item.ToDate != "null")
                    {
                        DateTime fromdate;
                        fromdate = DateTime.ParseExact(item.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        DateTime todate;
                        todate = DateTime.ParseExact(item.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                        todate = new DateTime(todate.Year, todate.Month, todate.Day);
                        if (fromdate > todate)
                        {
                            throw new NotImplementedException("Please choose Todate bigger Fromdate");
                        }
                        queryParameters.Add("@FromDate", fromdate);
                        queryParameters.Add("@ToDate", todate);
                    }
                    queryParameters.Add("@ProdOrder", item.ProdOrder);
                    queryParameters.Add("@Payment", item.Payment);
                    queryParameters.Add("@ProdCode", item.ProdCode);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<Object>("WIP_Get_Insert_Del_TTFGCntDaily", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<bool> ERP_WIP_TTFGCntDaily_Update(TTFGCntDaily item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    //if (item.FGDate.Value.Year != DateTime.Now.Year)
                    //{
                    //    throw new NotImplementedException("Pleace choose year  " + DateTime.Now.Year.ToString() + "");
                    //}
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string Query = "SELECT a.ProdInstCD,b.ProdCode FROM TProdInstItem a (NOLOCK) " +
                    "INNER JOIN dbo.TProd b(NOLOCK) ON a.ProdID = b.ProdID AND b.Discontinue = 0 " +
                    "WHERE a.ProdInstCD='" + item.ProdOrder + "' AND b.ProdCode='" + item.ProdCode + "' ";
                    var data = connection.Query<ProdInstItemViewModel>(Query
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.Text)
                        .ToList();
                    if (data.Count == 0)
                    {
                        throw new NotImplementedException("Not find ProdCode " + item.ProdCode + " of " + item.ProdOrder + " in Parkerp");
                    }
                }

                item.FGDate = item.FGDate.Value.Date;
                item.UpdatedOn = DateTime.Now;
                await this.Update(item);
                await this.Context.SaveChangesAsync();
                return await Task.FromResult(true);

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<TTFGCntDaily>> WIP_TTFGCntDaily_GetProdOrder(TTFGCntDaily item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string Query = "";
                    if (!string.IsNullOrEmpty(item.ProdOrder))
                    {
                        Query = "SELECT TOP(15) ProdOrder FROM TTFGCntDaily (NOLOCK) WHERE ProdOrder LIKE N'%" + item.ProdOrder + "%' GROUP BY ProdOrder";
                    }
                    if (string.IsNullOrEmpty(item.ProdOrder))
                    {
                        Query = "SELECT TOP(15) ProdOrder FROM TTFGCntDaily (NOLOCK) GROUP BY ProdOrder";
                    }
                    var data = await connection.QueryAsync<TTFGCntDaily>(Query, queryParameters, commandTimeout: 1500, commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public async Task<IList<TTFGCntDaily>> WIP_TTFGCntDaily_GetProdCode(TTFGCntDaily item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string Query = "";
                    if (!string.IsNullOrEmpty(item.ProdCode))
                    {
                        Query = "SELECT TOP(15) ProdCode FROM TTFGCntDaily (NOLOCK) Where ProdCode LIKE N'%" + item.ProdCode + "%' GROUP BY ProdCode";
                    }
                    if (string.IsNullOrEmpty(item.ProdCode))
                    {
                        Query = "SELECT TOP(15) ProdCode FROM TTFGCntDaily (NOLOCK) GROUP BY ProdCode";
                    }
                    var data = await connection.QueryAsync<TTFGCntDaily>(Query, queryParameters, commandTimeout: 1500, commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public DataTable WIP_TTFGCntDaily_GetExcel(TTFGCntDaily_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("WIP_Get_Insert_Del_TTFGCntDaily", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                if (item.FromDate != "null" && item.ToDate != "null")
                {
                    DateTime fromdate;
                    fromdate = DateTime.ParseExact(item.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    DateTime todate;
                    todate = DateTime.ParseExact(item.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                    todate = new DateTime(todate.Year, todate.Month, todate.Day);
                    if (fromdate > todate)
                    {
                        throw new NotImplementedException("Please choose Todate bigger Fromdate");
                    }
                    cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value =fromdate;
                    cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = todate;
                }
                cmd.Parameters.Add("@ProdOrder", SqlDbType.NVarChar).Value = item.ProdOrder;
                cmd.Parameters.Add("@ProdCode", SqlDbType.NVarChar).Value = item.ProdCode;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        //public async Task<byte[]> WIP_TTFGCntDaily_GetExcel(TTFGCntDaily_Custom item)
        //{
        //    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
        //    {
        //        var queryParameters = new DynamicParameters();
        //        if (item.FromDate != "null" && item.ToDate != "null")
        //        {
        //            DateTime fromdate;
        //            fromdate = DateTime.ParseExact(item.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
        //            DateTime todate;
        //            todate = DateTime.ParseExact(item.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
        //            fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
        //            todate = new DateTime(todate.Year, todate.Month, todate.Day);
        //            if (fromdate > todate)
        //            {
        //                throw new NotImplementedException("Please choose Todate bigger Fromdate");
        //            }
        //            queryParameters.Add("@FromDate", fromdate);
        //            queryParameters.Add("@ToDate", todate);
        //        }
        //        queryParameters.Add("@ProdOrder", item.ProdOrder);
        //        queryParameters.Add("@ProdCode", item.ProdCode);
        //        queryParameters.Add("@Type", item.Type);
        //        queryParameters.Add("@PageNumber", item.PageNumber);
        //        queryParameters.Add("@PageSize", item.PageSize);
        //        connection.Open();
        //        var data = await connection.QueryAsync<TTFGCntDaily_Custom>("WIP_Get_Insert_Del_TTFGCntDaily", queryParameters, commandTimeout: 1500
        //            , commandType: CommandType.StoredProcedure);


        //        ExcelPackage excel = new ExcelPackage();
        //        var workSheet = excel.Workbook.Worksheets.Add("TTFGCntDaily");
        //        workSheet.Cells[1, 1].Value = "FGDate";
        //        workSheet.Cells[1, 2].Value = "ProdOrder";
        //        workSheet.Cells[1, 3].Value = "ProdCode";
        //        workSheet.Cells[1, 4].Value = "SKUCode";
        //        workSheet.Cells[1, 5].Value = "StyleCode";
        //        workSheet.Cells[1, 6].Value = "ColorName";
        //        workSheet.Cells[1, 7].Value = "ColorCode";
        //        workSheet.Cells[1, 8].Value = "ProdQCQty";
        //        workSheet.Cells[1, 9].Value = "FGQty";
        //        workSheet.Cells[1, 10].Value = "ShipQty";


        //        int recordIndex = 2;
        //        foreach (TTFGCntDaily_Custom detail in data.ToList())
        //        {
        //            workSheet.Cells[recordIndex, 1].Value = detail.FGDate;
        //            workSheet.Cells[recordIndex, 1].Style.Numberformat.Format = "yyyy-MM-dd";
        //            workSheet.Cells[recordIndex, 2].Value = detail.ProdOrder;
        //            workSheet.Cells[recordIndex, 3].Value = detail.ProdCode;
        //            workSheet.Cells[recordIndex, 4].Value = detail.SKUCode;
        //            workSheet.Cells[recordIndex, 5].Value = detail.StyleCode;
        //            workSheet.Cells[recordIndex, 6].Value = detail.Color;
        //            workSheet.Cells[recordIndex, 7].Value = detail.ColorCode;
        //            workSheet.Cells[recordIndex, 8].Value = detail.ProdQCQty;
        //            workSheet.Cells[recordIndex, 9].Value = detail.FGQty;
        //            workSheet.Cells[recordIndex, 10].Value = detail.ShipQty;


        //            recordIndex++;
        //        }
        //        return excel.GetAsByteArray();

        //    }
        //}
        public DataTable WIP_TTFGCntDaily_GetConsExcel(WIB_FG_GR_ConsModel item)
        {
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
 
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("WIP_TTFGCntDaily_GetFG_GR_Cons", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                if (item.FromDate != "null" && item.ToDate != "null")
                {
                    DateTime fromdate;
                    fromdate = DateTime.ParseExact(item.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    DateTime todate;
                    todate = DateTime.ParseExact(item.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                    todate = new DateTime(todate.Year, todate.Month, todate.Day);
                    if (fromdate > todate)
                    {
                        throw new NotImplementedException("Please choose Todate bigger Fromdate");
                    }
                    cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromdate;
                    cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = todate;
                }
                cmd.Parameters.Add("@ProdOrder", SqlDbType.NVarChar).Value = item.ProdInstCD;
                cmd.Parameters.Add("@ProdCode", SqlDbType.NVarChar).Value = item.ProdCode;
                cmd.Parameters.Add("@MatName", SqlDbType.NVarChar).Value = item.MatName;
                cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD;
                cmd.Parameters.Add("@Payment", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.Payment) ? "" : item.Payment;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = item.Type;
                cmd.Parameters.Add("@Costing", SqlDbType.Bit).Value = item.Costing;
                cmd.Parameters.Add("@ClosedOnFGGR", SqlDbType.Bit).Value = item.ClosedOnFGGR;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;



            }
        }
        public DataTable WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup_Excel(TTFGCntDaily_Custom item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup", con);
            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            if (item.FromDate != "null" && item.ToDate != "null")
            {
                DateTime fromdate;
                fromdate = DateTime.ParseExact(item.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                DateTime todate;
                todate = DateTime.ParseExact(item.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                todate = new DateTime(todate.Year, todate.Month, todate.Day);
                if (fromdate > todate)
                {
                    throw new NotImplementedException("Please choose Todate bigger Fromdate");
                }
                cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromdate;
                cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = todate;
            }
            cmd.Parameters.Add("@ProdOrder", SqlDbType.NVarChar).Value = item.ProdOrder;
            cmd.Parameters.Add("@ProdCode", SqlDbType.NVarChar).Value = item.ProdCode;
            cmd.Parameters.Add("@Payment", SqlDbType.NVarChar).Value = item.Payment;
            cmd.Parameters.Add("@ClosedOnFGGR", SqlDbType.Bit).Value = item.ClosedOnFGGR;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;//4
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;

            
        }

        public async Task<IList<WIB_FG_GR_ConsModel>> WIP_TTFGCntDaily_GetFG_GR_Cons(WIB_FG_GR_ConsModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    if (item.FromDate != "null" && item.ToDate != "null")
                    {
                        DateTime fromdate;
                        fromdate = DateTime.ParseExact(item.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        DateTime todate;
                        todate = DateTime.ParseExact(item.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                        todate = new DateTime(todate.Year, todate.Month, todate.Day);
                        if (fromdate > todate)
                        {
                            throw new NotImplementedException("Please choose Todate bigger Fromdate");
                        }
                        queryParameters.Add("@FromDate", fromdate);
                        queryParameters.Add("@ToDate", todate);
                    }
                    queryParameters.Add("@ProdOrder", item.ProdInstCD);
                    queryParameters.Add("@ProdCode", item.ProdCode);
                    queryParameters.Add("@MatName", item.MatName);
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(item.Payment)  ? "" : item.Payment);
                    queryParameters.Add("@Costing", item.Costing);
                    queryParameters.Add("@ClosedOnFGGR", item.ClosedOnFGGR);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_FG_GR_ConsModel>("WIP_TTFGCntDaily_GetFG_GR_Cons", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        public async Task<IList<TTFGCntDaily_Custom>> WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup(TTFGCntDaily_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    if (item.FromDate != "null" && item.ToDate != "null")
                    {
                        DateTime fromdate;
                        fromdate = DateTime.ParseExact(item.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        DateTime todate;
                        todate = DateTime.ParseExact(item.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                        todate = new DateTime(todate.Year, todate.Month, todate.Day);
                        if (fromdate > todate)
                        {
                            throw new NotImplementedException("Please choose Todate bigger Fromdate");
                        }
                        queryParameters.Add("@FromDate", fromdate);
                        queryParameters.Add("@ToDate", todate);
                    }
                    queryParameters.Add("@ProdOrder", item.ProdOrder);
                    queryParameters.Add("@Payment", item.Payment);
                    queryParameters.Add("@ProdCode", item.ProdCode);
                    queryParameters.Add("@ClosedOnFGGR", item.ClosedOnFGGR);
                    queryParameters.Add("@Type", item.Type);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TTFGCntDaily_Custom>("WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
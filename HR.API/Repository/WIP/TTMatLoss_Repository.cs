﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.WIB.MatLoss;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.WIP
{
    public class TTMatLoss_Repository : GenericRepository<TTMatLoss>, ITTMatLoss_Repository
    {
        public TTMatLoss_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TTMatLoss_Repository(PARKERPEntities context)
            : base(context)
        {
        }

        public async Task<bool> WIP_TTMatLoss_Insert(TTMatLoss item)
        {
            await this.Insert_SaveChange(item);
            return await Task.FromResult(true);
        }

        public async Task<IList<TTMatLoss_Custom>> WIP_TTMatLoss_PageSize(TTMatLoss_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD);
                    queryParameters.Add("@ProdCode", item.ProdCode);
                    queryParameters.Add("@SKUCode", item.SKUCode);
                    queryParameters.Add("@MatCD", item.MatCD);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TTMatLoss_Custom>("WIP_TTMatLoss_PageSize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable WIP_TTMatLoss_PageSize_Excel(TTMatLoss_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("WIP_TTMatLoss_PageSize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.ProdInstCD;
                cmd.Parameters.Add("@ProdCode", SqlDbType.NVarChar).Value = item.ProdCode;
                cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}
﻿using Dapper;

using HR.API.IUnitOfWork;
using HR.API.Models.Entity;
using HR.API.Models.WIB.ProdPrice;
using HR.API.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HR.API.Repository.WIP
{
    public class TTProdPrice_Repository : GenericRepository<TTProdPrice>, ITTProdPrice_Repository
    {
        public TTProdPrice_Repository(IUnitOfWork<PARKERPEntities> unitOfWork)
            : base(unitOfWork)
        {
        }
        public TTProdPrice_Repository(PARKERPEntities context)
            : base(context)
        {
        }
        public async Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_Pagesize(TTProdPrice_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD);
                    queryParameters.Add("@ProdCode", item.ProdCode);
                    queryParameters.Add("@BrandCD", item.BrandCD);
                    queryParameters.Add("@SKUCode", item.SKUCode);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<TTProdPrice_Custom>("ERP_TTProdPrice_Pagesize", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable ERP_TTProdPrice_Pagesize_Excel(TTProdPrice_Custom item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_TTProdPrice_Pagesize", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value=item.ProdInstCD;
                cmd.Parameters.Add("@ProdCode", SqlDbType.NVarChar).Value = item.ProdCode;
                cmd.Parameters.Add("@BrandCD", SqlDbType.NVarChar).Value = item.BrandCD;
                cmd.Parameters.Add("@SKUCode", SqlDbType.NVarChar).Value = item.SKUCode;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@PageNumber",SqlDbType.Int).Value = item.PageNumber;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_ByProdInstCD(TTProdPrice_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdInstCD", item.ProdInstCD);
                    connection.Open();
                    var data = await connection.QueryAsync<TTProdPrice_Custom>("ERP_TTProdPrice_ByProdInstCD", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_ByBrandCD()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<TTProdPrice_Custom>("ERP_TTProdPrice_ByBrandCD", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_ProdCode(TTProdPrice_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdCode", item.ProdCode);
                    connection.Open();
                    var data = await connection.QueryAsync<TTProdPrice_Custom>("ERP_TTProdPrice_ProdCode", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public async Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_SKUCode(TTProdPrice_Custom item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@SKUCode", item.SKUCode);
                    connection.Open();
                    var data = await connection.QueryAsync<TTProdPrice_Custom>("ERP_TTProdPrice_SKUCode", queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

    }
}
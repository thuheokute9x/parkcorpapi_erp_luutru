﻿using HR.API.Models.FTP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using RouteAttribute = System.Web.Http.RouteAttribute;
using HR.API.Models.ERP;
using FluentFTP;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.Text.RegularExpressions;
using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.PayDoc;
using HR.API.UnitOfWork;
using HR.API.Models.ERP.TableCustom;


namespace HR.API.Controllers.FTP
{
    [HR.API.App_Start.JwtAuthentication]
    public class FilesController : BaseController
    {
        //private UnitOfWork<PARKERPEntities> unitOfWork = new UnitOfWork<PARKERPEntities>();
        private GenericRepository<STPayDoc_List> repository;
        private ISTPayDoc_ListRepository sTPayDocListRepository;
        public FilesController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STPayDoc_List>(unitOfWork);
            sTPayDocListRepository = new STPayDoc_ListRepository(unitOfWork);
        }
        public void ProcessDirectory(FtpClient client, string targetDirectory, ref DTONode paramDTONode)
        {
            //string[] fileEntries = Directory.GetFiles(targetDirectory);
            FtpListItem[] itemt = client.GetListing(targetDirectory);
            foreach (FtpListItem item in itemt)
            {
                if (item.Type == FtpFileSystemObjectType.File)
                {
                    ProcessFile(item, ref paramDTONode);

                }
            }

            foreach (FtpListItem item in itemt)
            {
                if (item.Type == FtpFileSystemObjectType.Directory)
                {
                    DTONode objDTONode = new DTONode();
                    objDTONode.title = item.Name;
                    objDTONode.key = item.FullName;
                    objDTONode.children = new List<DTONode>();
                    objDTONode.type = "folder";
                    objDTONode.disabled = true;
                    paramDTONode.children.Add(objDTONode);
                }

            }
        }
        public void ProcessFile(FtpListItem item, ref DTONode paramDTONode)
        {
            DTONode objDTONode = new DTONode();
            objDTONode.title = item.Name;
            objDTONode.key = item.FullName;
            objDTONode.type = "file";
            objDTONode.isLeaf = true;
            if (item.Name.Contains(".pdf") || item.Name.Contains(".PDF"))
            {
                objDTONode.isPDF = true;
            }
            paramDTONode.children.Add(objDTONode);
        }

        [HttpPost]
        [Route("ERP_FTP_GetFoderAndFile")]
        public List<DTONode> ERP_FTP_GetFoderAndFile(FileFTPModel file)
            //(string patch, string serername, string user, string pass)
        {
            this.Initialization();
            FtpClient client = new FtpClient(file.Servername);
            client.Credentials = new NetworkCredential(file.Username, file.Password);
            client.Connect();
            DTONode objDTONode = new DTONode();
            objDTONode.title = "192.168.1.6";
            objDTONode.key = "ftp://192.168.1.6";
            objDTONode.children = new List<DTONode>();
            // Get Files
            ProcessDirectory(client, file.Patch, ref objDTONode);
            return objDTONode.children;
        }
        [HttpGet]
        [Route("ERP_FTP_LoginFTP")]
        public async Task<Boolean> ERP_FTP_LoginFTP(string servername, string username,string password)
        {
            try
            {
                this.Initialization();
                FtpClient client = new FtpClient(servername);
                client.Credentials = new NetworkCredential(username, password);
                client.Connect();
                return await Task.FromResult(true);
            }
            catch
            {
                return await Task.FromResult(false);
            }
        }
        [HttpPost]
        [Route("ERP_FTP_CheckFile")]
        public async Task<Boolean> ERP_FTP_CheckFile(FileFTPModel FileFTP)
        {
            try
            {
                this.Initialization();
                FileFTP.Patch = string.Join("%25", FileFTP.Patch.Split('%'));
                FileFTP.Patch = string.Join("%23", FileFTP.Patch.Split('#'));
                FileFTP.Patch = string.Join("%5E", FileFTP.Patch.Split('^'));
                FileFTP.Patch = string.Join("%60", FileFTP.Patch.Split('`'));
                FileFTP.Patch = string.Join("%7B", FileFTP.Patch.Split('{'));
                FileFTP.Patch = string.Join("%7D", FileFTP.Patch.Split('}'));
                FileFTP.Patch = string.Join("%3B", FileFTP.Patch.Split(';'));
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FileFTP.Servername + FileFTP.Patch);
                request.Credentials = new NetworkCredential(FileFTP.Username, FileFTP.Password);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                byte[] buffer = new byte[2048];
                Stream reader = request.GetResponse().GetResponseStream();
                var memoryStream = new MemoryStream();
                reader.CopyTo(memoryStream);
                byte[] byteArray = memoryStream.ToArray();
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_FTP_GetFilePDF")]
        public HttpResponseMessage ERP_FTP_GetFilePDF(FileFTPModel FileFTP)
        {
            try
            {
                this.Initialization();
                FileFTP.Patch = string.Join("%25", FileFTP.Patch.Split('%'));
                FileFTP.Patch = string.Join("%23", FileFTP.Patch.Split('#'));
                FileFTP.Patch = string.Join("%5E", FileFTP.Patch.Split('^'));
                FileFTP.Patch = string.Join("%60", FileFTP.Patch.Split('`'));
                FileFTP.Patch = string.Join("%7B", FileFTP.Patch.Split('{'));
                FileFTP.Patch = string.Join("%7D", FileFTP.Patch.Split('}'));
                FileFTP.Patch = string.Join("%3B", FileFTP.Patch.Split(';'));
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FileFTP.Servername +  FileFTP.Patch);
                request.Credentials = new NetworkCredential(FileFTP.Username, FileFTP.Password);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                byte[] buffer = new byte[2048];
                Stream reader = request.GetResponse().GetResponseStream();
                var memoryStream = new MemoryStream();
                reader.CopyTo(memoryStream);
                byte[] byteArray = memoryStream.ToArray();
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new ByteArrayContent(byteArray);
                response.Content.Headers.ContentLength = byteArray.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                string[] patchList = FileFTP.Patch.Split('/');
                string filename = patchList[patchList.Count() - 1];
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception e)
            {
                var response = new HttpResponseMessage(HttpStatusCode.NotFound);
                response.Content = new StringContent(e.Message);
                return response;
            }

        }
        [HttpGet]
        [Route("ERP_FTP_GetIV")]
        public async Task<IList<FTPIVModel>> ERP_FTP_GetIV()
        {
            try
            {
                this.Initialization();
                List<FTPIVModel> result = new List<FTPIVModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    //queryParameters.Add("@MatCD", string.IsNullOrEmpty(MatCD) || MatCD == "All" ? "" : MatCD.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<FTPIVModel>("ERP_FTP_IVPDF"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_FTP_GetGRDoc")]
        public async Task<IList<GRDocModel>> ERP_FTP_GetGRDoc(GRDocModel GRDoc)
        {
            try
            {
                this.Initialization();
                List<GRDocModel> result = new List<GRDocModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PageSize", GRDoc.PageSize);
                    queryParameters.Add("@PageNumber", GRDoc.PageNumber);
                    queryParameters.Add("@SHIPPER", string.IsNullOrEmpty(GRDoc.SHIPPER) || GRDoc.SHIPPER == "All" ? "" : GRDoc.SHIPPER);
                    queryParameters.Add("@VCD", string.IsNullOrEmpty(GRDoc.VCD) || GRDoc.VCD == "All" ? "" : GRDoc.VCD);
                    queryParameters.Add("@GRDocNo", string.IsNullOrEmpty(GRDoc.GRDocNo) || GRDoc.GRDocNo == "All" ? "" : GRDoc.GRDocNo);
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<GRDocModel>("ERP_FTP_GetGRDoc"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                 result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_FTP_GetPayDoc")]
        public async Task<IList<PayDocModel>> ERP_FTP_GetPayDoc(PayDocModel PayDoc)
        {
            try
            {
                this.Initialization();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PageSize", PayDoc.PageSize);
                    queryParameters.Add("@PageNumber", PayDoc.PageNumber);
                    queryParameters.Add("@VendorCD", string.IsNullOrEmpty(PayDoc.VendorCD) ? "" : PayDoc.VendorCD);
                    queryParameters.Add("@PayType", string.IsNullOrEmpty(PayDoc.PayType) ? "" : PayDoc.PayType);
                    queryParameters.Add("@PayDocCD", string.IsNullOrEmpty(PayDoc.PayDocCD) ? "" : PayDoc.PayDocCD);
                    queryParameters.Add("@PayDocNo", string.IsNullOrEmpty(PayDoc.PayDocNo) ? "" : PayDoc.PayDocNo);
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<PayDocModel>("ERP_FTP_GetPayDoc"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_FTP_Merge")]
        public async Task<HttpResponseMessage> ERP_FTP_Merge(FileMergeRglPayReq infor)
        {
            try
            {
                this.Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                List<Stream> ListFile = new List<Stream>();
                //string[] patchmergelist = FileFTP[0].Patch.Split('/');
                //string finamepatch = patchmergelist[(patchmergelist.Length - 1)];
                //string patchmerge = FileFTP[0].Patch.Replace(finamepatch, "");

                IList<STPayDoc_List_Custom> listFile=await sTPayDocListRepository.ERP_STPayDoc_List_GetByRglPayReqID(infor.RglPayReqID);
                if (listFile == null)
                {
                    //var dt = new HttpResponseMessage(HttpStatusCode.NotFound);
                    //dt.Content = new StringContent("Pleace choose merge file");
                    //return await Task.FromResult(dt);

                    //HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.BadRequest);
                    //message.Content = new StringContent("This resource is not implemented yet.");
                    //return await Task.FromResult(dt);

                }
                if (listFile.Where(x=>!x.isPDF.Value).Any())
                {
                    //var dt = new HttpResponseMessage(HttpStatusCode.BadRequest);
                    //dt.Content = new StringContent("Pleace choose file PDF not file type different");
                    //return await Task.FromResult(dt);
                }
                using (PdfDocument outPdf = new PdfDocument())
                {
                    foreach (STPayDoc_List_Custom item in listFile)
                    {
                        item.FilePath = string.Join("%25", item.FilePath.Split('%'));
                        item.FilePath = string.Join("%23", item.FilePath.Split('#'));
                        item.FilePath = string.Join("%5E", item.FilePath.Split('^'));
                        item.FilePath = string.Join("%60", item.FilePath.Split('`'));
                        item.FilePath = string.Join("%7B", item.FilePath.Split('{'));
                        item.FilePath = string.Join("%7D", item.FilePath.Split('}'));
                        item.FilePath = string.Join("%3B", item.FilePath.Split(';'));
                        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(infor.Servername + item.FilePath);
                        request.Credentials = new NetworkCredential(infor.Username, infor.Password);
                        request.Method = WebRequestMethods.Ftp.DownloadFile;
                        ListFile.Add(request.GetResponse().GetResponseStream());
                        Stream readerpdf = request.GetResponse().GetResponseStream();
                        using (var readStream = new MemoryStream())
                        {
                            readerpdf.CopyTo(readStream);
                            PdfDocument one = PdfReader.Open(readStream, PdfDocumentOpenMode.Import);
                            CopyPages(one, outPdf);
                        }

                    }
                    Stream pdfMerge = new MemoryStream();
                    outPdf.Save(pdfMerge);
                    var memoryStream = new MemoryStream();
                    pdfMerge.CopyTo(memoryStream);
                    byte[] pdfMergebyte = memoryStream.ToArray();
                    string filename = infor.Servername
                     + DateTime.Now.ToString("yyyyMMddhhmmss") + "_Merge.pdf";

                    //FtpWebRequest ftpClient = (FtpWebRequest)FtpWebRequest.Create(filename);
                    //ftpClient.Credentials = new System.Net.NetworkCredential(FileFTP[0].Username, FileFTP[0].Password);
                    //ftpClient.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                    //ftpClient.UseBinary = true;
                    //ftpClient.KeepAlive = true;
                    //using (Stream requestStream = ftpClient.GetRequestStream())
                    //{
                    //    requestStream.Write(pdfMergebyte, 0, pdfMergebyte.Length);
                    //    requestStream.Close();
                    //}
                    //FtpWebResponse uploadResponse = (FtpWebResponse)ftpClient.GetResponse();
                    //uploadResponse.Close();

                    //using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    //{
                    //    var PayDocListID = string.Join(",", FileFTP.Select(x => x.PayDocListID.ToString()).ToList());
                    //    var queryParameters = new DynamicParameters();
                    //    connection.Open();
                    //    string query = "UPDATE dbo.STPayDoc_List SET Merge=1 WHERE PayDocListID IN (" + PayDocListID + ")";
                    //    var data = await connection.QueryAsync<PayDocModel>(query
                    //        , queryParameters, commandTimeout: 1500
                    //        , commandType: CommandType.Text);
                    //}
                    response.Content = new ByteArrayContent(pdfMergebyte);
                    response.Content.Headers.ContentLength = pdfMergebyte.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    GC.SuppressFinalize(this);
                }
                //using (PdfDocument one = PdfReader.Open(@"C:\Users\Administrator\Downloads\dummy.PDF", PdfDocumentOpenMode.Import))
                //using (PdfDocument two = PdfReader.Open(@"C:\Users\Administrator\Downloads\sample.PDF", PdfDocumentOpenMode.Import))
                //using (PdfDocument outPdf = new PdfDocument())
                //{
                //    CopyPages(one, outPdf);
                //    CopyPages(two, outPdf);

                //    outPdf.Save(@"D:\file1and2.pdf");
                //}
                return response;

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_FTP_CheckExist_File_Merge")]
        public async Task<bool> ERP_FTP_CheckExist_File_Merge(FileMergeRglPayReq infor)
        {
            try
            {
                this.Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                List<Stream> ListFile = new List<Stream>();
                IList<STPayDoc_List_Custom> listFile = await sTPayDocListRepository.ERP_STPayDoc_List_GetByRglPayReqID(infor.RglPayReqID);
                if (listFile == null)
                {
                    throw new NotImplementedException("Pleace choose merge file");
                }
                if (listFile.Where(x => !x.isPDF.Value).Any())
                {
                    throw new NotImplementedException("Pleace choose file PDF not file type different");
                }
                using (PdfDocument outPdf = new PdfDocument())
                {
                    foreach (STPayDoc_List_Custom item in listFile)
                    {
                        item.FilePath = string.Join("%25", item.FilePath.Split('%'));
                        item.FilePath = string.Join("%23", item.FilePath.Split('#'));
                        item.FilePath = string.Join("%5E", item.FilePath.Split('^'));
                        item.FilePath = string.Join("%60", item.FilePath.Split('`'));
                        item.FilePath = string.Join("%7B", item.FilePath.Split('{'));
                        item.FilePath = string.Join("%7D", item.FilePath.Split('}'));
                        item.FilePath = string.Join("%3B", item.FilePath.Split(';'));
                        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(infor.Servername + item.FilePath);
                        request.Credentials = new NetworkCredential(infor.Username, infor.Password);
                        request.Method = WebRequestMethods.Ftp.DownloadFile;
                        ListFile.Add(request.GetResponse().GetResponseStream());
                        Stream readerpdf = request.GetResponse().GetResponseStream();
                    }
                }
                return await Task.FromResult(true);

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        void CopyPages(PdfDocument from, PdfDocument to)
        {
            for (int i = 0; i < from.PageCount; i++)
            {
                to.AddPage(from.Pages[i]);
            }
        }
    }
}

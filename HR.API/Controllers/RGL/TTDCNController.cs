﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Repository;
using HR.API.Repository.RGL.DN;
using HR.API.Repository.RGL.RglPayReqDN;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.RGL
{
    [HR.API.App_Start.JwtAuthentication]
    public class TTDCNController : BaseController
    {
        private GenericRepository<TTDCN> repositoryTTDCN;
        private ITTDCN_Repository iTTDCN_Repository;


        private GenericRepository<TTRglPayReqDCN> repository;
        private ITTRglPayReqDN_Repository iTTRglPayReqDN_Repository;
        public TTDCNController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repositoryTTDCN = new GenericRepository<TTDCN>(unitOfWork);
            iTTDCN_Repository = new TTDCN_Repository(unitOfWork);
            repository = new GenericRepository<TTRglPayReqDCN>(unitOfWork);
            iTTRglPayReqDN_Repository = new TTRglPayReqDN_Repository(unitOfWork);
        }


        [HttpGet]
        [Route("ERP_TTDCN_GetDCNNo")]
        public async Task<IList<TTDCN>> ERP_TTDCN_GetDCNNo(string PayDocNo)
        {
            Initialization();
            return await iTTDCN_Repository.ERP_TTDCN_GetDCNNo(PayDocNo);
        }
        [HttpPost]
        [Route("ERP_TTDCN_GetPageSize")]
        public async Task<IList<TTDCN_Custom>> ERP_TTDCN_GetPageSize(TTDCN_Custom item)
        {
            Initialization();
            return await iTTDCN_Repository.ERP_TTDCN_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TTDCN_Update")]
        public async Task<Boolean> ERP_TTDCN_Update(TTDCN item)
        {
            Initialization();
            return await iTTDCN_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TTDCN_Insert")]
        public async Task<Boolean> ERP_TTDCN_Insert(TTDCN item)
        {
            Initialization();
            item.CompanyCD=this.GetDB();
            return await iTTDCN_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TTDCN_Delete")]
        public async Task<Boolean> ERP_TTDCN_Delete(TTDCN item)
        {
            Initialization();
            return await iTTDCN_Repository.Delete_SaveChange(item);
        }


        [HttpGet]
        [Route("ERP_TTRglPayReqDCN_ByDCNID")]
        public async Task<IList<TTRglPayReqDCN_Custom>> ERP_TTRglPayReqDCN_ByDCNID(int DCNID)
        {
            Initialization();
            return await iTTRglPayReqDN_Repository.ERP_TTRglPayReqDCN_ByDCNID(DCNID);
        }
        [HttpPost]
        [Route("ERP_TTRglPayReqDN_Update")]
        public async Task<Boolean> ERP_TTRglPayReqDN_Update(TTRglPayReqDCN item)
        {
            Initialization();
            return await iTTRglPayReqDN_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TTRglPayReqDN_Insert")]
        public async Task<Boolean> ERP_TTRglPayReqDN_Insert(TTRglPayReqDCN item)
        {
            Initialization();
            return await iTTRglPayReqDN_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TTRglPayReqDCN_InsertList")]
        public async Task<Boolean> ERP_TTRglPayReqDCN_InsertList(List<TTRglPayReqDCN_Custom> item)
        {
            Initialization();
            var tTRglPayReqDCNList = (from a in item
                                  select new TTRglPayReqDCN()
                                  {
                                      RglPayReqID = a.RglPayReqID,
                                      VendorCD = a.VendorCD,
                                      DCNID=a.DCNID,
                                      DCNAmount=a.AmountTobe
                                  }).ToList();
            var tTDCNList = (from a in item
                             where a.ClosedOn
                                      select new TTDCN()
                                      {
                                          DCNID = a.DCNID
                                      }).ToList();
            return await iTTRglPayReqDN_Repository.ERP_TTRglPayReqDCN_InsertList(tTRglPayReqDCNList, tTDCNList);
        }
        [HttpPost]
        [Route("ERP_TTRglPayReqDN_Delete")]
        public async Task<Boolean> ERP_TTRglPayReqDN_Delete(TTRglPayReqDCN item)
        {
            Initialization();
            return await iTTRglPayReqDN_Repository.Delete_SaveChange(item);
        }
        
        [HttpPost]
        [Route("ERP_TTRglPayReqDCN_ByRglPayReqIDVendor")]
        public async Task<IList<TTRglPayReqDCN_Custom>> ERP_TTRglPayReqDCN_ByRglPayReqIDVendor(TTRglPayReqDCN_Custom item)
        {
            Initialization();
            return await iTTRglPayReqDN_Repository.ERP_TTRglPayReqDCN_ByRglPayReqIDVendor(item);
        }

        [HttpPost]
        [Route("ERP_TTRglPayReqDCN_CalculatorBalance")]
        public async Task<IList<TTRglPayReqDCN_Custom>> ERP_TTRglPayReqDCN_CalculatorBalance(TTRglPayReqDCN item)
        {
            Initialization();
            return await iTTRglPayReqDN_Repository.ERP_TTRglPayReqDCN_CalculatorBalance(item);
        }
    }
}

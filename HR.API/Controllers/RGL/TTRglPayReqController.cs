﻿using Dapper;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.RglPayReq;
using HR.API.Repository.RglPayReq_List;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TTRglPayReqController : BaseController
    {
        private GenericRepository<TTRglPayReq> repository;
        private ITTRglPayReqRepository iTTRglPayReqRepository;
        private GenericRepository<TTRglPayReq_List> repository_List;
        private ITTRglPayReq_List_Repository iTTRglPayReq_List_Repository;
        public TTRglPayReqController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TTRglPayReq>(unitOfWork);
            iTTRglPayReqRepository = new TTRglPayReqRepository(unitOfWork);
            repository_List = new GenericRepository<TTRglPayReq_List>(unitOfWork);
            iTTRglPayReq_List_Repository = new TTRglPayReq_List_Repository(unitOfWork);
        }

        [HttpPost]
        [Route("ERP_TTRglPayReq_GetPageSize")]
        public async Task<IList<TTRglPayReq_Custom>> ERP_TTRglPayReq_GetPageSize(TTRglPayReq_Custom item)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TTRglPayReq_Insert")]
        public async Task<Boolean> ERP_TTRglPayReq_Insert(TTRglPayReq item)
        {
            Initialization();
            item.RglDate = DateTime.Now;
            return await iTTRglPayReqRepository.Insert_SaveChange(item);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_GetRglPayReqID")]
        public async Task<IList<TTRglPayReq>> ERP_TTRglPayReq_GetRglPayReqID(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_GetRglPayReqID(RglPayReqID);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_List_BulkInsert")]
        public async Task<Boolean> ERP_TTRglPayReq_List_BulkInsert(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReq_List_Repository.ERP_TTRglPayReq_List_BulkInsert(RglPayReqID);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_List_ByRglPayReqID")]
        public async Task<IList<STPayReq_Custom>> ERP_TTRglPayReq_List_ByRglPayReqID(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReq_List_Repository.ERP_TTRglPayReq_List_ByRglPayReqID(RglPayReqID);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_DocGrp")]
        public async Task<IList<RglPayReq_DocGrp>> ERP_TTRglPayReq_DocGrp(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_DocGrp(RglPayReqID);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_VndGrp")]
        public async Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_VndGrp(RglPayReqID);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_Report_WithPayDocNo")]
        public async Task<IList<Rgl_Report_WithPayDocNo>> ERP_TTRglPayReq_Report_WithPayDocNo(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_Report_WithPayDocNo(RglPayReqID);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_Report_WithPayDocNo_Excel")]
        public async Task<HttpResponseMessage> ERP_TTRglPayReq_Report_WithPayDocNo_Excel(string RglPayReqID)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "Regular_Report" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = await iTTRglPayReqRepository.ERP_TTRglPayReq_Report_WithPayDocNo_Excel(RglPayReqID);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                GC.SuppressFinalize(this);
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TTRglPayReq_Report_WithPayReqID")]
        public async Task<IList<ERP_TTRglPayReq_Report_WithPayReqID>> ERP_TTRglPayReq_Report_WithPayReqID(ERP_TTRglPayReq_Report_WithPayReqID RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_Report_WithPayReqID(RglPayReqID);
        }
        [HttpPost]
        [Route("ERP_TTRglPayReq_Report_WithPayReqID_Excel")]
        public HttpResponseMessage ERP_TTRglPayReq_Report_WithPayReqID_Excel(ERP_TTRglPayReq_Report_WithPayReqID item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "RglPayReq_Report";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTTRglPayReqRepository.ERP_TTRglPayReq_Report_WithPayReqID_Excel(item), filename,true);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TTRglPayReq_List_Insert_InsChkTempByUser")]
        public async Task<Boolean> ERP_TTRglPayReq_List_Insert_InsChkTempByUser(TTRglPayReq_List_Custom item)
        {
            Initialization();
            return await iTTRglPayReq_List_Repository.ERP_TTRglPayReq_List_Insert_InsChkTempByUser(item);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_List_Get_InsChkTempByUser")]
        public async Task<IList<TTRglPayReq_List_Custom>> ERP_TTRglPayReq_List_Get_InsChkTempByUser(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReq_List_Repository.ERP_TTRglPayReq_List_Get_InsChkTempByUser(RglPayReqID);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_DocGrp_GetVendor")]
        public async Task<IList<RglPayReq_DocGrp>> ERP_TTRglPayReq_DocGrp_GetVendor(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_DocGrp_GetVendor(RglPayReqID);
        }
        [HttpPost]
        [Route("ERP_TTRglPayReq_DocGrp_GetPayReqID")]
        public async Task<IList<RglPayReq_DocGrp>> ERP_TTRglPayReq_DocGrp_GetPayReqID(RglPayReq_DocGrp item)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_DocGrp_GetPayReqID(item);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_VndGrp_GetVendorCD")]
        public async Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp_GetVendorCD(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_VndGrp_GetVendorCD(RglPayReqID);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_VndGrp_PayDocCD")]
        public async Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp_PayDocCD(string RglPayReqID, string VendorCD)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_VndGrp_PayDocCD(RglPayReqID, VendorCD);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_VndGrp_GetPayDocNo")]
        public async Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_VndGrp_GetPayDocNo(string RglPayReqID, string VendorCD, string PayDocCD)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_VndGrp_GetPayDocNo(RglPayReqID, VendorCD, PayDocCD);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_PayReq_GetPayDocNo")]
        public async Task<IList<RglPayReq_VndGrp>> ERP_TTRglPayReq_PayReq_GetPayDocNo(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_PayReq_GetPayDocNo(RglPayReqID);
        }
        [HttpPost]
        [Route("ERP_TTRglPayReq_PayReq_PayReqID")]
        public async Task<IList<TTRglPayReq_List>> ERP_TTRglPayReq_PayReq_PayReqID(TTRglPayReq_List_Custom item)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_PayReq_PayReqID(item);
        }
        [HttpPost]
        [Route("ERP_TTRglPayReq_List_UpdateFilePath")]
        public async Task<bool> ERP_TTRglPayReq_List_UpdateFilePath(TTRglPayReq_List_Custom item)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_List_UpdateFilePath(item);
        }
        [HttpPost]
        [Route("ERP_TTRglPayReq_List_DeleteFilePath")]
        public async Task<bool> ERP_TTRglPayReq_List_DeleteFilePath(TTRglPayReq_List_Custom item)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_List_DeleteFilePath(item);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_CheckPreviewRglPayReq")]
        public async Task<Result> ERP_TTRglPayReq_CheckPreviewRglPayReq(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_CheckPreviewRglPayReq(RglPayReqID);
        }
        [HttpGet]
        [Route("ERP_TTRglPayReq_List_GetVendorBanInfor")]
        public async Task<IList<TVendor_Custom>> ERP_TTRglPayReq_List_GetVendorBanInfor(string RglPayReqID)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_List_GetVendorBanInfor(RglPayReqID);
        }
        [HttpPost]
        [Route("ERP_TTRglPayReq_UpdateConfirmedOn")]
        public async Task<Boolean> ERP_TTRglPayReq_UpdateConfirmedOn(TTRglPayReq_Custom item)
        {
            Initialization();
            return await iTTRglPayReqRepository.ERP_TTRglPayReq_UpdateConfirmedOn(item);
        }

    }
}
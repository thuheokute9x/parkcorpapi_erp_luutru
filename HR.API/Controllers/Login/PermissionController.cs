﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebApi.Jwt;

using HR.API.Models.Login;
using System.Net.Http.Headers;
using HR.API.Models.WIB.WIP;
using HR.API.Repository;
using HR.API.Models.Entity;
using HR.API.Repository.ERP.ExRateDaily;
using System.Globalization;
using Newtonsoft.Json;
using HR.API.UnitOfWork;
using System.Net.Mail;
using EASendMail;
using StackExchange.Redis;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models.ERP.TempModel.Development;
using HR.API.App_Start;

namespace HR.API.Controllers.Login
{
    //public PARKERPEntities()
    //    : base("name=Entities" + GetDB())
    //{
    //}
    //public static string GetDB()
    //{
    //    var claimslist = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
    //    var claims = claimslist?.ToList();
    //    string Company = "";
    //    if (claims.Count > 1)
    //    {
    //        Company = claims[1].Value;
    //    }
    //    if (string.IsNullOrEmpty(Company))
    //    {
    //        //throw new NotImplementedException("Pleace Login again");
    //        return "";
    //    }
    //    return Company;
    //}
    //[HR.API.App_Start.JwtAuthentication]
    public class PermissionController : BaseController
    {
        //private GenericRepository<TTExRateDaily> repository;
        //private ITTExRateDaily_Repository iTTExRateDaily_Repository;
        public PermissionController()
        {
            //repository = new GenericRepository<TTExRateDaily>(unitOfWork);
            //iTTExRateDaily_Repository = new TTExRateDaily_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERB_CheckLogin")]
        public async Task<UserLoginModel> ERB_CheckLogin(LoginModel item)
        {
            try
            {
                if (string.IsNullOrEmpty(item.Database))
                {
                    throw new NotImplementedException("Please chose Database");
                }
                using (var connection = new SqlConnection(Settings.GetConnectionString(item.Database)))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ID", item.username);
                    queryParameters.Add("@PWD", item.password);
                    connection.Open();
                    var data = await connection.QueryAsync<PermissionModel>("ERP_CheckLogin"
                        , queryParameters
                        , commandType: CommandType.StoredProcedure);
                    var result = data.ToList();
                    if ((result.Count==1 && result[0].IsFailure==true))
                    {
                        throw new NotImplementedException("User or Password is wrong");
                    }
                    UserLoginModel userlogin = new UserLoginModel();
                    userlogin.PermissionList = result;
                    userlogin.Token= JwtManager.GenerateToken(item.Database, result[0].EmpID.ToString(), 1);

                    var simplePrinciple = JwtManager.GetPrincipal(userlogin.Token);
                    var identity = simplePrinciple?.Identity as ClaimsIdentity;



                    var usernameClaim = identity.FindFirst(ClaimTypes.Name).Value;

                    var AuthenticationInstant = identity.FindFirst(ClaimTypes.AuthenticationInstant).Value;


                    return userlogin;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERB_CheckIsOutSide")]
        public async Task<bool> ERB_CheckIsOutSide()
        {
            try
            {
                //using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                //{
                //    var queryParameters = new DynamicParameters();
                //    connection.Open();
                //    var data = await connection.QueryAsync<STPayDocCD>("SELECT TOP(1)PayDocCD FROM dbo.STPayDocCD (NOLOCK)"
                //        , queryParameters
                //        , commandType: CommandType.Text);
                    return await Task.FromResult(true);

                //}
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpGet]
        [Route("GetToken")]
        public Object GetToken()
        {
            //string key = "my_secret_key_12345"; //Secret key which will be used later during validation    
            //var issuer = "http://mysite.com";  //normally this will be your site URL    

            //var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            //var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            ////Create a List of Claims, Keep claims name short    
            //var permClaims = new List<Claim>();
            //permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            //permClaims.Add(new Claim("valid", "1"));
            //permClaims.Add(new Claim("userid", "1"));
            //permClaims.Add(new Claim("name", "bilal"));

            ////Create Security Token object by giving required parameters    
            //var token = new JwtSecurityToken(issuer, //Issure    
            //                issuer,  //Audience    
            //                permClaims,
            //                expires: DateTime.Now.AddMinutes(1),
            //                signingCredentials: credentials);
            //var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);



            //var id = new ClaimsIdentity(permClaims);

            //var ctx = Request.GetOwinContext();
            //var authenticationManager = ctx.Authentication;
            //authenticationManager.SignIn(id);

            //return new { data = jwt_token };

           //return JwtManager.GenerateToken("test",1);
            return null;

        }
        [HttpGet]
        [Route("GetCurr")]
        public async Task<IList<object>> GetCurr()
        {
            Library.PermissionController dt = new Library.PermissionController();
            return await dt.GetCurr(Settings.GetConnectionString(Settings.GetConnectionString("Company")));
        }
        [HttpGet]
        [Route("GetCurr_WithParam")]
        public void GetCurr_WithParam(string fromdateyyyy_MM_dd, string todateyyyy_MM_dd)
        {
            Library.PermissionController dt = new Library.PermissionController();
            dt.GetCurr_WithParam(fromdateyyyy_MM_dd, todateyyyy_MM_dd, Settings.GetConnectionString(Settings.GetConnectionString("Company")));
            
        }

        [HttpGet]
        [Route("tt")]
        public  void tt()
        {

            try
            {
                Parallel.Invoke(Hostmail1, Hostmail2, Hostmail3, Hostmail4, Hostmail5);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public void Hostmail1()
        {
            for (int i = 0; i < 1000; i++)
            {
                string from = "thuheokute9x2@gmail.com"; //From address 
                string to = "jaronfaolak@hotmail.com"; //To address    
                double stt = 100000;
                MailMessage message = new MailMessage(from, to);
                string mailbody = "Hi Nobita,<br><br>We received a request to reset your Facebook password.<br>Enter the following password reset code:<br><br><label style='font - size: 21px; font - weight: 700; border: 1px solid #008cff;borderradius: 10px;padding: 8px 25px 8px 25px;background: #bae2ef;'>123456</label><br><br>Alternatively, you can directly change your password<br><br><a href='http://tvhay.org/' target='_blank' style='background-color: #36a0f4;color: white;padding: 11px 25px;text-align: center;display: inline-block;width: 800px;height: 38px;border-radius: 5px;'>Change Password</a><br><br><div style='font-weight: bold;'>Didn't request this change?</div>If you didn't request a new password, <a href='http://tvhay.org'>let us know</a>.";
                message.Subject = (stt + i).ToString() + " is your Facebook account recovery code";
                message.Body = mailbody;
                message.BodyEncoding = Encoding.UTF8;
                message.IsBodyHtml = true;
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587); //Gmail smtp    
                System.Net.NetworkCredential basicCredential1 = new
                System.Net.NetworkCredential("thuheokute9x2@gmail.com", "leyddzdmlprdqrjr");
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = basicCredential1;
                client.Send(message);
            }
        }
        public void Hostmail2()
        {
            for (int i = 0; i < 1000; i++)
            {
                string from = "n15cccn002@student.ptithcm.edu.vn"; //From address 
                string to = "jaronfaolak@hotmail.com"; //To address    
                double stt = 100000;
                MailMessage message = new MailMessage(from, to);
                string mailbody = "Hi Nobita,<br><br>We received a request to reset your Facebook password.<br>Enter the following password reset code:<br><br><label style='font - size: 21px; font - weight: 700; border: 1px solid #008cff;borderradius: 10px;padding: 8px 25px 8px 25px;background: #bae2ef;'>123456</label><br><br>Alternatively, you can directly change your password<br><br><a href='http://tvhay.org/' target='_blank' style='background-color: #36a0f4;color: white;padding: 11px 25px;text-align: center;display: inline-block;width: 800px;height: 38px;border-radius: 5px;'>Change Password</a><br><br><div style='font-weight: bold;'>Didn't request this change?</div>If you didn't request a new password, <a href='http://tvhay.org'>let us know</a>.";
                message.Subject = (stt + i).ToString() + " is your Facebook account recovery code";
                message.Body = mailbody;
                message.BodyEncoding = Encoding.UTF8;
                message.IsBodyHtml = true;
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587); //Gmail smtp    
                System.Net.NetworkCredential basicCredential1 = new
                System.Net.NetworkCredential("n15cccn002@student.ptithcm.edu.vn", "doaremon");
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = basicCredential1;
                client.Send(message);
            }
        }
        public void Hostmail3()
        {
            for (int i = 0; i < 1000; i++)
            {
                string from = "thuheokute9x3@gmail.com"; //From address 
                string to = "jaronfaolak@hotmail.com"; //To address    
                double stt = 100000;
                MailMessage message = new MailMessage(from, to);
                string mailbody = "Hi Nobita,<br><br>We received a request to reset your Facebook password.<br>Enter the following password reset code:<br><br><label style='font - size: 21px; font - weight: 700; border: 1px solid #008cff;borderradius: 10px;padding: 8px 25px 8px 25px;background: #bae2ef;'>123456</label><br><br>Alternatively, you can directly change your password<br><br><a href='http://tvhay.org/' target='_blank' style='background-color: #36a0f4;color: white;padding: 11px 25px;text-align: center;display: inline-block;width: 800px;height: 38px;border-radius: 5px;'>Change Password</a><br><br><div style='font-weight: bold;'>Didn't request this change?</div>If you didn't request a new password, <a href='http://tvhay.org'>let us know</a>.";
                message.Subject = (stt + i).ToString() + " is your Facebook account recovery code";
                message.Body = mailbody;
                message.BodyEncoding = Encoding.UTF8;
                message.IsBodyHtml = true;
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587); //Gmail smtp    
                System.Net.NetworkCredential basicCredential1 = new
                System.Net.NetworkCredential("thuheokute9x3@gmail.com", "lqyoxhwbynltkvoq");
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = basicCredential1;
                client.Send(message);
            }
        }

        public void Hostmail4()
        {
            for (int i = 0; i < 1000; i++)
            {
                string from = "thuheokute9x@gmail.com"; //From address 
                string to = "jaronfaolak@hotmail.com"; //To address    
                double stt = 100000;
                MailMessage message = new MailMessage(from, to);
                string mailbody = "Hi Nobita,<br><br>We received a request to reset your Facebook password.<br>Enter the following password reset code:<br><br><label style='font - size: 21px; font - weight: 700; border: 1px solid #008cff;borderradius: 10px;padding: 8px 25px 8px 25px;background: #bae2ef;'>123456</label><br><br>Alternatively, you can directly change your password<br><br><a href='http://tvhay.org/' target='_blank' style='background-color: #36a0f4;color: white;padding: 11px 25px;text-align: center;display: inline-block;width: 800px;height: 38px;border-radius: 5px;'>Change Password</a><br><br><div style='font-weight: bold;'>Didn't request this change?</div>If you didn't request a new password, <a href='http://tvhay.org'>let us know</a>.";
                message.Subject = (stt+i).ToString() + " is your Facebook account recovery code";
                message.Body = mailbody;
                message.BodyEncoding = Encoding.UTF8;
                message.IsBodyHtml = true;
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587); //Gmail smtp    
                System.Net.NetworkCredential basicCredential1 = new
                System.Net.NetworkCredential("thuheokute9x@gmail.com", "biazcdqoohlpouyc");
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = basicCredential1;
                client.Send(message);
            }
        }
        public void Hostmail5()
        {
            for (int i = 0; i < 1000; i++)
            {
                string from = "secirity01@gmail.com"; //From address 
                string to = "jaronfaolak@hotmail.com"; //To address    
                double stt = 100000;
                MailMessage message = new MailMessage(from, to);
                string mailbody = "Hi Nobita,<br><br>We received a request to reset your Facebook password.<br>Enter the following password reset code:<br><br><label style='font - size: 21px; font - weight: 700; border: 1px solid #008cff;borderradius: 10px;padding: 8px 25px 8px 25px;background: #bae2ef;'>123456</label><br><br>Alternatively, you can directly change your password<br><br><a href='http://tvhay.org/' target='_blank' style='background-color: #36a0f4;color: white;padding: 11px 25px;text-align: center;display: inline-block;width: 800px;height: 38px;border-radius: 5px;'>Change Password</a><br><br><div style='font-weight: bold;'>Didn't request this change?</div>If you didn't request a new password, <a href='http://tvhay.org'>let us know</a>.";
                message.Subject = (stt + i).ToString() + " is your Facebook account recovery code";
                message.Body = mailbody;
                message.BodyEncoding = Encoding.UTF8;
                message.IsBodyHtml = true;
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587); //Gmail smtp    
                System.Net.NetworkCredential basicCredential1 = new
                System.Net.NetworkCredential("secirity01@gmail.com", "ifjynjjjvmlwnjlh");
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = basicCredential1;
                client.Send(message);
            }
        }

    }
}

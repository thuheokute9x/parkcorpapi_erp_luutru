﻿using AutoMapper;
using Dapper;
using ExcelLibrary.SpreadSheet;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models.WIB.StoreViewModel.GI;
using HR.API.Repository;
using HR.API.Repository.WIP;
using HR.API.UnitOfWork;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.WIP
{
    [HR.API.App_Start.JwtAuthentication]
    public class TTFGCntDailyController : BaseController
    {
        private GenericRepository<TTFGCntDaily> repository;
        private IFGCntDailyRepository iFGCntDailyRepository;
        public TTFGCntDailyController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TTFGCntDaily>(unitOfWork);
            iFGCntDailyRepository = new FGCntDailyRepository(unitOfWork);
        }
        //[HttpGet]
        //[Route("test")]
        //public async Task<IList<TTFGCntDaily_Custom>> test()
        //{
        //    var client = new WebClient("https://api.currencyfreaks.com/historical/2020-06-20?apikey=YOUR_APIKEY&base=GBP&symbols = EUR, USD, PKR, INR");
        //       client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    Console.WriteLine(response.Content);
        //    return await iFGCntDailyRepository.ERP_WIP_TTFGCntDaily_Delete(item);
        //}
        [HttpPost]
        [Route("ERP_WIP_TTFGCntDaily_Delete")]
        public async Task<IList<TTFGCntDaily_Custom>> ERP_WIP_TTFGCntDaily_Delete(TTFGCntDaily_Custom item)
        {
            Initialization();
            return await iFGCntDailyRepository.ERP_WIP_TTFGCntDaily_Delete(item);
        }
        [HttpPost]
        [Route("WIP_TTFGCntDaily_GetFG_GR_ConsExcel")]
        public HttpResponseMessage WIP_TTFGCntDaily_GetFG_GR_ConsExcel(WIB_FG_GR_ConsModel item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "FGGRFromToCons" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes =  ExportDataTableToExcelFromStore( iFGCntDailyRepository.WIP_TTFGCntDaily_GetConsExcel(item), filename, true,false);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_TTFGCntDaily_GetFG_GR_Cons")]
        public async Task<IList<WIB_FG_GR_ConsModel>> WIP_TTFGCntDaily_GetFG_GR_Cons(WIB_FG_GR_ConsModel item)
        {
            Initialization();
            return await iFGCntDailyRepository.WIP_TTFGCntDaily_GetFG_GR_Cons(item);
        }
        [HttpPost]
        [Route("WIP_TTFGCntDaily_GetExcel")]
        public HttpResponseMessage WIP_TTFGCntDaily_GetExcel(TTFGCntDaily_Custom item)
        {
            try
            {
                    Initialization();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    string filename = "TTFGCntDaily" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                    Byte[] bytes =ExportDataTableToExcelFromStore(iFGCntDailyRepository.WIP_TTFGCntDaily_GetExcel(item), "TTFGCntDaily",true);
                    response.Content = new ByteArrayContent(bytes);
                    response.Content.Headers.ContentLength = bytes.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    return response;

               
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup_Excel")]
        public HttpResponseMessage WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup_Excel(TTFGCntDaily_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "FGGRFromTo";
                Byte[] bytes = ExportDataTableToExcelFromStore(iFGCntDailyRepository.WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup_Excel(item), filename,true);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_TTFGCntDaily_GetProdOrder")]
        public async Task<IList<TTFGCntDaily>> WIP_TTFGCntDaily_GetProdOrder(TTFGCntDaily item)
        {
            Initialization();
            return await iFGCntDailyRepository.WIP_TTFGCntDaily_GetProdOrder(item);
        }
        [HttpPost]
        [Route("WIP_TTFGCntDaily_GetProdCode")]
        public async Task<IList<TTFGCntDaily>> WIP_TTFGCntDaily_GetProdCode(TTFGCntDaily item)
        {
            Initialization();
            return await iFGCntDailyRepository.WIP_TTFGCntDaily_GetProdCode(item);
        }
        [HttpPost]
        [Route("WIP_Get_Insert_Del_TTFGCntDaily")]
        public async Task<IList<Object>> WIP_Get_Insert_Del_TTFGCntDaily(TTFGCntDaily_Custom item)
        {
            Initialization();
            return await iFGCntDailyRepository.WIP_Get_Insert_Del_TTFGCntDaily(item);
        }
        [HttpPost]
        [Route("WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup")]
        public async Task<IList<TTFGCntDaily_Custom>> WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup(TTFGCntDaily_Custom item)
        {
            Initialization();
            return await iFGCntDailyRepository.WIP_TTFGCntDaily_GetFG_GR_Cons_NoGroup(item);
        }
        [HttpPost]
        [Route("WIP_TTFGCntDaily_Update")]
        public async Task<bool> WIP_TTFGCntDaily_Update(TTFGCntDaily item)
        {
            Initialization();
            return await iFGCntDailyRepository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("WIP_TTFGCntDaily_Insert")]
        public async Task<List<Result>> WIP_TTFGCntDaily_Insert()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            PARKERPEntities db = new PARKERPEntities();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                string EmpID = httpRequest.Form["EmpID"];
                string dateimport = httpRequest.Form["dateimport"];

                string Path = "";
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        Path = "~/Upload/Excel/"+ DateTime.Now.ToString("yyyymmddhhmmss") + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath(Path);
                        postedFile.SaveAs(filePath);
                    }
                    FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));
                    List<ProdInstItemViewModel> ProdInstItemlist;
                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    {
                        var queryParameters = new DynamicParameters();
                        connection.Open();
                        ProdInstItemlist = connection.Query<ProdInstItemViewModel>("SELECT a.ProdInstCD,b.ProdCode FROM TProdInstItem a (NOLOCK) " +
                        "INNER JOIN dbo.TProd b(NOLOCK) ON a.ProdID = b.ProdID AND b.Discontinue = 0"
                            , queryParameters
                            , commandTimeout: 1500
                            , commandType: CommandType.Text)
                            .ToList();
                        if (ProdInstItemlist == null)
                        {
                            ProdInstItemlist = new List<ProdInstItemViewModel>();
                        }
                    }

                    if (existingFile.Extension.ToString() == ".xls")
                    {
                        return await importXLS(Path, ProdInstItemlist, EmpID);
                    }

                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        ExcelWorkbook workBook = package.Workbook;
                        if (workBook != null)
                        {
                            if (workBook.Worksheets.Count > 0)
                            {
                                //ExcelWorksheet worksheet = workBook.Worksheets.Where(x => x.Name.Contains("FGCntDaily")).FirstOrDefault();
                                ExcelWorksheet worksheet = workBook.Worksheets.FirstOrDefault();
                                //if (worksheet == null)
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SheetName FGCntDaily in file excel" });
                                //    return resultList;
                                //}
                                int rowCount = worksheet.Dimension.End.Row;

                                List<TTFGCntDaily> ListInsert = new List<TTFGCntDaily>();
                                #region Check file exel no header name


                                if (worksheet.Cells[1, 1].Value == null)
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                    return resultList;
                                }
                                if (worksheet.Cells[1, 1].Value.ToString().Trim() != "FGDate")
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel not template import" });
                                    return resultList;
                                }


                                #endregion
                                for (int row = 2; row <= rowCount; row++)
                                {
                                    if (worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null || worksheet.Cells[row, 3].Value == null
                                       || worksheet.Cells[row, 4].Value == null || worksheet.Cells[row, 5].Value == null || worksheet.Cells[row, 6].Value == null)
                                    {
                                        continue;
                                    }
                                    Decimal ProdQCQty;
                                    if (!Decimal.TryParse(worksheet.Cells[row, 4].Value.ToString() == "" ||
                                        worksheet.Cells[row, 4].Value.ToString() == "-" ? "0" : worksheet.Cells[row, 4].Value.ToString(), out ProdQCQty))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "FG Qty row " + row.ToString() + " " + worksheet.Cells[row, 4].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    Decimal FGQty;
                                    if (!Decimal.TryParse(worksheet.Cells[row, 5].Value.ToString()=="" || 
                                        worksheet.Cells[row, 5].Value.ToString() =="-" ?"0": worksheet.Cells[row, 5].Value.ToString(), out FGQty))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "FG Qty row " + row.ToString() + " " + worksheet.Cells[row, 5].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    Decimal ShipQty;
                                    if (!Decimal.TryParse(worksheet.Cells[row, 6].Value.ToString()=="-" || 
                                        worksheet.Cells[row, 6].Value.ToString() == ""?"0": worksheet.Cells[row, 6].Value.ToString(), out ShipQty))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "FG Qty row " + row.ToString() + " " + worksheet.Cells[row, 6].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    double d = double.Parse(worksheet.Cells[row, 1].Value.ToString());
                                    DateTime FGDate = DateTime.FromOADate(d).Date;
                                    //if (FGDate.Year != DateTime.Now.Year)
                                    //{
                                    //    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Pleace import year " + DateTime.Now.Year.ToString() + " in row " + row.ToString() + "" });
                                    //    continue;
                                    //}
                                    string RefVKNo = worksheet.Cells[row, 2].Value.ToString().Trim();
                                    string ProdCode = worksheet.Cells[row, 3].Value.ToString().Trim();

                                    if (ProdInstItemlist.Where(x => x.ProdInstCD.Contains(RefVKNo) && x.ProdCode.Contains(ProdCode)).Count() == 0)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdCode " + ProdCode + " of " + RefVKNo + " in Parkerp" });
                                        continue;
                                    }
                                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                    {
                                        var queryParameters = new DynamicParameters();
                                        queryParameters.Add("@ProdOrder", RefVKNo);
                                        queryParameters.Add("@ProdCode", ProdCode);
                                        queryParameters.Add("@FGDate", FGDate, DbType.DateTime);
                                        queryParameters.Add("@Type", 5);
                                        connection.Open();
                                        var data = await connection.QueryAsync<TTFGCntDaily>("WIP_Get_Insert_Del_TTFGCntDaily"
                                            , queryParameters
                                            , commandTimeout: 1500
                                            , commandType: CommandType.StoredProcedure);
                                        if (data.ToList().Count() > 0)
                                        {
                                            resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Duplicated key ProdOrder " + RefVKNo + " ,ProdCode " + ProdCode + ",FGDate " + FGDate.ToString("yyy/MM/dd") + "" });
                                            continue;
                                        }
                                        var dooubleinsertList = ListInsert.Where(x => x.FGDate.Value.Date == FGDate && x.ProdOrder == RefVKNo && x.ProdCode == ProdCode).ToList();
                                        if (dooubleinsertList!=null && dooubleinsertList.Count>0)
                                        {
                                            resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Duplicated key ProdOrder " + RefVKNo + " ,ProdCode " + ProdCode + ",FGDate " + FGDate.ToString("yyy/MM/dd") + "" });
                                            continue;
                                        }
                                    }
                                    ListInsert.Add(new TTFGCntDaily { FGDate = FGDate, ProdOrder = RefVKNo, ProdCode = ProdCode, FGQty = FGQty, ShipQty = ShipQty });


                                }
                                if (resultList.Count > 0)
                                {
                                    return resultList;
                                }
                                await repository.BulkInsert(ListInsert);
                                //foreach (TTFGCntDaily item in ListInsert)
                                //{
                                //    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                //    {
                                //        var queryParameters = new DynamicParameters();
                                //        queryParameters.Add("@ProdOrder", item.ProdOrder);
                                //        queryParameters.Add("@ProdCode", item.ProdCode);
                                //        queryParameters.Add("@FGDate", item.FGDate, DbType.DateTime);
                                //        queryParameters.Add("@FGQty", item.FGQty);
                                //        queryParameters.Add("@ShipQty", item.ShipQty);
                                //        queryParameters.Add("@CreatedBy", EmpID);
                                //        queryParameters.Add("@Type", 1);
                                //        connection.Open();
                                //        var data = await connection.QueryAsync<Result>("WIP_Get_Insert_Del_TTFGCntDaily"
                                //            , queryParameters
                                //            , commandTimeout: 1500
                                //            , commandType: CommandType.StoredProcedure);
                                //    }
                                //}


                            }
                        }

                    }
                    return resultList;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());

            }
            return resultList;
        }
        public async Task<List<Result>> importXLS(string Path, List<ProdInstItemViewModel> ProdInstItemList, string EmpID)
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            FileStream existingFile = new FileStream(HttpContext.Current.Server.MapPath(Path), FileMode.Open);
            var workbook = Workbook.Load(existingFile);
            var worksheet = workbook.Worksheets.FirstOrDefault();
            //var worksheet = workbook.Worksheets.Where(x => x.Name.Contains("FGCntDaily")).FirstOrDefault();
            //if (worksheet == null)
            //{
            //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SheetName FGCntDaily in file excel" });
            //    return resultList;
            //}
            var cells = worksheet.Cells;
            #region Check file exel no header name


            if (worksheet.Cells[0, 0].Value == null)
            {
                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                return resultList;
            }
            if (worksheet.Cells[0, 0].Value.ToString().Trim() != "FGDate")
            {
                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel not template import" });
                return resultList;
            }


            #endregion
            List<TTFGCntDaily> ListInsert = new List<TTFGCntDaily>();

            for (int row = cells.FirstRowIndex + 1; row <= cells.LastRowIndex; row++)
            {

                if (worksheet.Cells[row, 0].Value == null || worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null || worksheet.Cells[row, 3].Value == null 
                    || worksheet.Cells[row, 4].Value == null || worksheet.Cells[row, 5].Value == null)
                {
                    continue;
                }
                Decimal ProdQCQty;
                if (!Decimal.TryParse(worksheet.Cells[row, 3].Value.ToString(), out ProdQCQty))
                {
                    resultList.Add(new Result { ErrorCode = row + 1, ErrorMessage = "Prod QC Qty row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not number!" });
                    continue;
                }
                Decimal FGQty;
                if (!Decimal.TryParse(worksheet.Cells[row, 4].Value.ToString(), out FGQty))
                {
                    resultList.Add(new Result { ErrorCode = row + 1, ErrorMessage = "FG Qty row " + row.ToString() + " " + worksheet.Cells[row, 4].Value.ToString() + " not number!" });
                    continue;
                }
                Decimal ShipQty;
                if (!Decimal.TryParse(worksheet.Cells[row, 5].Value.ToString(), out ShipQty))
                {
                    resultList.Add(new Result { ErrorCode = row + 1, ErrorMessage = "FG Qty row " + row.ToString() + " " + worksheet.Cells[row, 5].Value.ToString() + " not number!" });
                    continue;
                }
                double d = double.Parse(worksheet.Cells[row, 0].Value.ToString());
                DateTime FGDate = DateTime.FromOADate(d).Date;
                //if (FGDate.Year != DateTime.Now.Year)
                //{
                //    resultList.Add(new Result { ErrorCode = row + 1, ErrorMessage = "Pleace import year " + DateTime.Now.Year.ToString() + " in row " + row.ToString() + "" });
                //    continue;
                //}

                string RefVKNo = worksheet.Cells[row, 1].Value.ToString().Trim();
                string ProdCode = worksheet.Cells[row, 2].Value.ToString().Trim();

                if (ProdInstItemList.Where(x => x.ProdInstCD==RefVKNo && x.ProdCode==ProdCode).ToList().Count() == 0)
                {
                    resultList.Add(new Result { ErrorCode = row + 1, ErrorMessage = "Not find ProdCode " + ProdCode + " of " + RefVKNo + " in Parkerp" });
                    continue;
                }
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ProdOrder", RefVKNo);
                    queryParameters.Add("@ProdCode", ProdCode);
                    queryParameters.Add("@FGDate", FGDate, DbType.DateTime);
                    queryParameters.Add("@Type", 5);
                    connection.Open();
                    var data = await connection.QueryAsync<TTFGCntDaily>("WIP_Get_Insert_Del_TTFGCntDaily"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    if (data.ToList().Count()>0)
                    {
                        resultList.Add(new Result { ErrorCode = row + 1, ErrorMessage = "Duplicated key ProdOrder " + RefVKNo + " ,ProdCode " + ProdCode + ",FGDate "+ FGDate.ToString("yyy/MM/dd") + "" });
                        continue;
                    }
                    var dooubleinsertList = ListInsert.Where(x => x.FGDate.Value.Date == FGDate && x.ProdOrder == RefVKNo && x.ProdCode == ProdCode).ToList();
                    if (dooubleinsertList != null && dooubleinsertList.Count > 0)
                    {
                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Duplicated key ProdOrder " + RefVKNo + " ,ProdCode " + ProdCode + ",FGDate " + FGDate.ToString("yyy/MM/dd") + "" });
                        continue;
                    }
                }


                ListInsert.Add(new TTFGCntDaily { FGDate= FGDate, ProdOrder = RefVKNo, ProdCode = ProdCode,ProdQCQty= ProdQCQty, FGQty = FGQty, ShipQty = ShipQty });

            }
            if (resultList.Count > 0)
            {
                return resultList;

            }
            await repository.BulkInsert(ListInsert);
            //foreach (TTFGCntDaily item in ListInsert)
            //{
            //    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            //    {
            //        var queryParameters = new DynamicParameters();
            //        queryParameters.Add("@ProdOrder", item.ProdOrder);
            //        queryParameters.Add("@ProdCode", item.ProdCode);
            //        queryParameters.Add("@FGDate", item.FGDate, DbType.DateTime);
            //        queryParameters.Add("@FGQty", item.FGQty);
            //        queryParameters.Add("@ShipQty", item.ShipQty);
            //        queryParameters.Add("@CreatedBy", EmpID);
            //        queryParameters.Add("@Type", 1);
            //        connection.Open();
            //        var data = await connection.QueryAsync<Result>("WIP_Get_Insert_Del_TTFGCntDaily"
            //            , queryParameters
            //            , commandTimeout: 1500
            //            , commandType: CommandType.StoredProcedure);
            //    }
            //}
            return resultList;
        }
    }
}

﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;



namespace HR.API.Controllers.WIP
{
    [HR.API.App_Start.JwtAuthentication]
    public class FG_GRController : BaseController
    {
        [HttpGet]
        [Route("WIP_GetFG_GR")]
        public async Task<IList<WIB_FG_GRModel>> WIP_GetFG_GR(int PageNumber, int PageSize)
        {
            try
            {
                List<WIB_FG_GRModel> result = new List<WIB_FG_GRModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", PageNumber);
                    queryParameters.Add("@PageSize", PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_FG_GRModel>("WIP_GetFG_GR"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpGet]
        [Route("WIP_GetFG_GRExcel")]
        public HttpResponseMessage WIP_GetFG_GRExcel()
        {
            try
            {
                //List<WIB_FG_GRModel> result = new List<WIB_FG_GRModel>();
                //using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                //{
                //    var queryParameters = new DynamicParameters();
                //    queryParameters.Add("@Type", 1);
                //    queryParameters.Add("@PageNumber", 0);
                //    queryParameters.Add("@PageSize", 0);
                //    connection.Open();
                //    var data = await connection.QueryAsync<WIB_FG_GRModel>("WIP_GetFG_GR"
                //        , queryParameters
                //        , commandTimeout: 1500
                //        , commandType: CommandType.StoredProcedure);
                //    result = data.ToList();
                //}
                //return result;
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("WIP_GetFG_GR", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "FG_GR_Cons"+".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(table, "FG_GR_Cons");
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                GC.SuppressFinalize(this);
                return response;

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
    }
}

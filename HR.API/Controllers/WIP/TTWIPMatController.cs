﻿using Dapper;
using ExcelLibrary.SpreadSheet;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.WIB.StoreViewModel.GI;
using HR.API.Models.WIB.StoreViewModel.WIP.TPR;
using HR.API.Models.WIB.WIPMat;
using HR.API.Repository;
using HR.API.Repository.WIP;
using HR.API.UnitOfWork;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.WIP
{
    [HR.API.App_Start.JwtAuthentication]
    public class TTWIPMatController : BaseController
    {
        private GenericRepository<TTWIPMat> repository;
        private TTWIPMat_Repository tTWIPMat_Repository;
        public TTWIPMatController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TTWIPMat>(unitOfWork);
            tTWIPMat_Repository = new TTWIPMat_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("WIP_TTWIPMat_PageSize")]
        public async Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_PageSize(TTWIPMat_Custom item)
        {
            Initialization();
            return await tTWIPMat_Repository.WIP_TTWIPMat_PageSize(item);
        }
        [HttpPost]
        [Route("WIP_TTWIPMat_PageSize_Excel")]
        public HttpResponseMessage WIP_TTWIPMat_PageSize_Excel(TTWIPMat_Custom item)
        {
            Initialization();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string filename = "WIPMat";
            Byte[] bytes = ExportDataTableToExcelFromStore(tTWIPMat_Repository.WIP_TTWIPMat_PageSize_Excel(item), "WIPMat",true,false, "WIP_TTWIPMat_PageSize");
            response.Content = new ByteArrayContent(bytes);
            response.Content.Headers.ContentLength = bytes.LongLength;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = filename;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
            GC.SuppressFinalize(this);
            return response;
        }
        [HttpPost]
        [Route("WIP_TTWIPMat_ByMatCD")]
        public async Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_ByMatCD(TTWIPMat_Custom item)
        {
            Initialization();
            return await tTWIPMat_Repository.WIP_TTWIPMat_ByMatCD(item);
        }
        [HttpPost]
        [Route("WIP_TTWIPMat_ByRefVKNo")]
        public async Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_ByRefVKNo(TTWIPMat_Custom item)
        {
            Initialization();
            return await tTWIPMat_Repository.WIP_TTWIPMat_ByRefVKNo(item);
        }
        [HttpPost]
        [Route("WIP_TTWIPMat_Insert")]
        public async Task<List<Result>> WIP_TTWIPMat_Insert()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            PARKERPEntities db = new PARKERPEntities();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                string Path = "";
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        Path = "~/Upload/Excel/WIP" + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath(Path);
                        postedFile.SaveAs(filePath);
                    }
                    FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));
                    //List<TPRViewModel> TPRViewlist = new List<TPRViewModel>(); ;
                    //using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    //{
                    //    var queryParameters = new DynamicParameters();
                    //    connection.Open();
                    //    TPRViewlist = connection.Query<TPRViewModel>("SELECT a.ProdInstCD,c.MatCD FROM TProdInstItem a (NOLOCK) " +
                    //        "INNER JOIN dbo.TCons b (NOLOCK) ON a.ConsID=b.ConsID INNER JOIN dbo.TConsItem c (NOLOCK) ON b.ConsID=c.ConsID GROUP BY a.ProdInstCD,c.MatCD"
                    //        , queryParameters
                    //        , commandTimeout: 1500
                    //        , commandType: CommandType.Text)
                    //        .ToList();
                    //    connection.Close();
                    //    if (TPRViewlist == null)
                    //    {
                    //        TPRViewlist = new List<TPRViewModel>();
                    //    }
                    //}

                    if (existingFile.Extension.ToString() == ".xls")
                    {
                        return await importXLS(Path);
                    }

                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        ExcelWorkbook workBook = package.Workbook;
                        if (workBook != null)
                        {
                            if (workBook.Worksheets.Count > 0)
                            {
                                ExcelWorksheet worksheet = workBook.Worksheets.FirstOrDefault();
                                //if (worksheet == null)
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SheetName WIP_Inventory in file excel" });
                                //    return resultList;
                                //}
                                int rowCount = worksheet.Dimension.End.Row;

                                List<TTWIPMat> ListInsert = new List<TTWIPMat>();
                                #region Check file exel no header name


                                //if (worksheet.Cells[1, 1].Value == null)
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                //    return resultList;
                                //}
                                //if (worksheet.Cells[1, 1].Value.ToString().Trim() != "Prod Order")
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                //    return resultList;
                                //}


                                #endregion
                                for (int row = 2; row <= rowCount; row++)
                                {

                                    if (worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null || worksheet.Cells[row, 3].Value == null || worksheet.Cells[row, 4].Value == null || worksheet.Cells[row, 5].Value == null)
                                    {
                                        continue;
                                    }
                                    decimal Qty;
                                    if (!decimal.TryParse(worksheet.Cells[row, 5].Value.ToString(), out Qty))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Qty row " + row.ToString() + " " + worksheet.Cells[row, 5].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    int Month = 0;
                                    int MonthTemp = 0;
                                    if (!int.TryParse(worksheet.Cells[row, 4].Value.ToString(), out Month))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 4].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    if (MonthTemp == 0)
                                    {
                                        MonthTemp = int.Parse(worksheet.Cells[row, 4].Value.ToString());
                                    }
                                    if (Month > 12 || Month < 1)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 4].Value.ToString() + " not valid!" });
                                        continue;
                                    }
                                    if (MonthTemp != Month)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 4].Value.ToString() + " please import same month!" });
                                        continue;
                                    }

                                    int Year = 0;
                                    int YearTemp = 0;
                                    if (!int.TryParse(worksheet.Cells[row, 3].Value.ToString(), out Year))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    if (YearTemp == 0)
                                    {
                                        YearTemp = int.Parse(worksheet.Cells[row, 3].Value.ToString());
                                    }
                                    if (Year > 2100 || Year < 2000)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not valid!" });
                                        continue;
                                    }
                                    if (MonthTemp != Month)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " please import same month!" });
                                        continue;
                                    }

                                    string RefVKNo = worksheet.Cells[row, 1].Value.ToString().Trim();
                                    string MatCD = worksheet.Cells[row, 2].Value.ToString().Trim();

                                    //if (TPRView.Where(x => x.ProdInstCD.Contains(RefVKNo) && x.MatCD.Contains(MatCD)).Count() == 0)
                                    //{
                                    //    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find MatCD " + MatCD + " of " + RefVKNo + " in Parkerp" });
                                    //    continue;
                                    //}
                                    ListInsert.Add(new TTWIPMat { ProdInstCD = RefVKNo, MatCD = MatCD, Qty = Qty, Month = Month, Year = Year });


                                }
                                if (resultList.Count > 0)
                                {
                                    return resultList;

                                }
                                foreach (TTWIPMat ob in ListInsert)
                                {
                                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                    {
                                        var queryParameters = new DynamicParameters();
                                        connection.Open();
                                        var dt = connection.Query<TTWIPMat>("SELECT WIPMat FROM TTWIPMat (NOLOCK) WHERE ProdInstCD='" + ob.ProdInstCD + "' AND MatCD='" + ob.MatCD + "' AND Year=" + ob.Year + " AND Month=" + ob.Month + ""
                                            , queryParameters
                                            , commandTimeout: 1500
                                            , commandType: CommandType.Text)
                                            .ToList();
                                        if (dt.Any())//Tồn tại thì update
                                        {
                                            connection.Query<TTWIPMat>("UPDATE TTWIPMat SET Qty=" + ob.Qty + ",UpdatedBy=" + GetUserlogin() + ",UpdatedOn=GETDATE() WHERE ProdInstCD='" + ob.ProdInstCD + "' AND MatCD='" + ob.MatCD + "' AND Year=" + ob.Year + " AND Month=" + ob.Month + ""
                                                                        , queryParameters
                                                    , commandTimeout: 1500
                                                    , commandType: CommandType.Text)
                                                    .ToList();
                                        }
                                        else
                                        {
                                            await repository.Insert_SaveChange(ob);
                                        }
                                        connection.Close();
                                    }
                                }


                            }
                        }

                    }
                    return resultList;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());

            }
            return resultList;
        }
        public async Task<List<Result>> importXLS(string Path)
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            FileStream existingFile = new FileStream(HttpContext.Current.Server.MapPath(Path), FileMode.Open);
            var workbook = Workbook.Load(existingFile);
            var worksheet = workbook.Worksheets.FirstOrDefault();
            var cells = worksheet.Cells;
            #region Check file exel no header name


            //if (worksheet.Cells[0, 0].Value == null)
            //{
            //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no header name" });
            //    return resultList;
            //}


            #endregion
            List<TTWIPMat> ListInsert = new List<TTWIPMat>();
            for (int row = cells.FirstRowIndex + 1; row <= cells.LastRowIndex; row++)
            {

                if (worksheet.Cells[row, 0].Value == null || worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null || worksheet.Cells[row, 3].Value == null || worksheet.Cells[row, 4].Value == null)
                {
                    continue;
                }
                decimal Qty;
                if (!decimal.TryParse(worksheet.Cells[row, 4].Value.ToString(), out Qty))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Qty row " + row.ToString() + " " + worksheet.Cells[row, 4].Value.ToString() + " not number!" });
                    continue;
                }
                int Month=0;
                int MonthTemp = 0;
                if (!int.TryParse(worksheet.Cells[row, 3].Value.ToString(), out Month))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not number!" });
                    continue;
                }
                if (MonthTemp == 0)
                {
                    MonthTemp= int.Parse(worksheet.Cells[row, 3].Value.ToString());
                }
                if (Month>12 || Month < 1)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not valid!" });
                    continue;
                }
                if (MonthTemp != Month)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " please import same month!" });
                    continue;
                }

                int Year = 0;
                int YearTemp = 0;
                if (!int.TryParse(worksheet.Cells[row, 2].Value.ToString(), out Year))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 2].Value.ToString() + " not number!" });
                    continue;
                }
                if (YearTemp == 0)
                {
                    YearTemp = int.Parse(worksheet.Cells[row, 2].Value.ToString());
                }
                if (Year > 2100 || Year < 2000)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 2].Value.ToString() + " not valid!" });
                    continue;
                }
                if (MonthTemp != Month)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 2].Value.ToString() + " please import same month!" });
                    continue;
                }

                string RefVKNo = worksheet.Cells[row, 0].Value.ToString().Trim();
                string MatCD = worksheet.Cells[row, 1].Value.ToString().Trim();

                //if (TPRView.Where(x => x.ProdInstCD.Contains(RefVKNo) && x.MatCD.Contains(MatCD)).Count() == 0)
                //{
                //    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find MatCD " + MatCD + " of " + RefVKNo + " in Parkerp" });
                //    continue;
                //}
                ListInsert.Add(new TTWIPMat {  ProdInstCD= RefVKNo, MatCD = MatCD, Qty = Qty, Month = Month ,Year=Year});

            }
            if (resultList.Count > 0)
            {
                return resultList;

            }
            foreach (TTWIPMat ob in ListInsert)
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var dt= connection.Query<TTWIPMat>("SELECT WIPMat FROM TTWIPMat (NOLOCK) WHERE ProdInstCD='"+ ob.ProdInstCD + "' AND MatCD='" + ob.MatCD + "' AND Year="+ ob.Year + " AND Month="+ob.Month+""
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.Text)
                        .ToList();
                    if (dt.Any())//Tồn tại thì update
                    {
                        connection.Query<TTWIPMat>("UPDATE TTWIPMat SET Qty=" + ob.Qty + ",UpdatedBy=" + GetUserlogin() + ",UpdatedOn=GETDATE() WHERE ProdInstCD='" + ob.ProdInstCD + "' AND MatCD='" + ob.MatCD + "' AND Year=" + ob.Year + " AND Month=" + ob.Month + ""
                                , queryParameters
                                , commandTimeout: 1500
                                , commandType: CommandType.Text)
                                .ToList();
                    }
                    else
                    {
                        await repository.Insert_SaveChange(ob);
                    }
                    connection.Close();
                }
            }
            //await repository.BulkInsert(ListInsert);
            //foreach (InventoryWIPModel item in ListInsert)
            //{
            //    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            //    {
            //        var queryParameters = new DynamicParameters();
            //        queryParameters.Add("@RefVKNo", item.ProdInstCD);
            //        queryParameters.Add("@ProdCode", item.ProdCode);
            //        queryParameters.Add("@Total", item.OrderQty);
            //        queryParameters.Add("@FinishedProduct", item.FGQty);
            //        queryParameters.Add("@Type", 1);
            //        connection.Open();
            //        var data = await connection.QueryAsync<Result>("WIP_Get_Insert_Del_TInventory"
            //            , queryParameters
            //            , commandTimeout: 1500
            //            , commandType: CommandType.StoredProcedure);
            //    }
            //}
            return resultList;
        }
        [HttpPost]
        [Route("WIP_TTWIPMat_Get_UnCutSubStorePageSize")]
        public async Task<IList<TTWIPMat_Custom>> WIP_TTWIPMat_Get_UnCutSubStorePageSize(TTWIPMat_Custom item)
        {
            Initialization();
            return await tTWIPMat_Repository.WIP_TTWIPMat_Get_UnCutSubStorePageSize(item);
        }
        [HttpPost]
        [Route("WIP_TTWIPMat_Get_UnCutSubStorePageSize_Excel")]
        public HttpResponseMessage WIP_TTWIPMat_Get_UnCutSubStorePageSize_Excel(TTWIPMat_Custom item)
        {
            Initialization();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string filename = "WIPMat";
            Byte[] bytes = ExportDataTableToExcelFromStore(tTWIPMat_Repository.WIP_TTWIPMat_Get_UnCutSubStorePageSize_Excel(item), "WIPMatGIUnused");
            response.Content = new ByteArrayContent(bytes);
            response.Content.Headers.ContentLength = bytes.LongLength;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = filename;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
            GC.SuppressFinalize(this);
            return response;
        }
        [HttpPost]
        [Route("WIP_TTWIPMat_Insert_UnCutSubStore")]
        public async Task<List<Result>> WIP_TTWIPMat_Insert_UnCutSubStore()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            PARKERPEntities db = new PARKERPEntities();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                string Path = "";
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        Path = "~/Upload/Excel/WIP" + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath(Path);
                        postedFile.SaveAs(filePath);
                    }
                    FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));
                    List<TPRViewModel> TPRViewlist = new List<TPRViewModel>(); ;
                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    {
                        var queryParameters = new DynamicParameters();
                        connection.Open();
                        TPRViewlist = connection.Query<TPRViewModel>("WIP_TTWIPMat_Get_ProdInstCDMatCDAll"
                            , queryParameters
                            , commandTimeout: 1500
                            , commandType: CommandType.StoredProcedure)
                            .ToList();
                        connection.Close();
                        if (TPRViewlist == null)
                        {
                            TPRViewlist = new List<TPRViewModel>();
                        }
                    }

                    if (existingFile.Extension.ToString() == ".xls")
                    {
                        return await importXLS_UnCutSubStore(Path, TPRViewlist);
                    }

                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        ExcelWorkbook workBook = package.Workbook;
                        if (workBook != null)
                        {
                            if (workBook.Worksheets.Count > 0)
                            {
                                ExcelWorksheet worksheet = workBook.Worksheets.FirstOrDefault();
                                //if (worksheet == null)
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SheetName WIP_Inventory in file excel" });
                                //    return resultList;
                                //}
                                int rowCount = worksheet.Dimension.End.Row;

                                List<TTWIPMat> ListInsert = new List<TTWIPMat>();
                                #region Check file exel no header name


                                //if (worksheet.Cells[1, 1].Value == null)
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                //    return resultList;
                                //}
                                //if (worksheet.Cells[1, 1].Value.ToString().Trim() != "Prod Order")
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                //    return resultList;
                                //}


                                #endregion
                                for (int row = 2; row <= rowCount; row++)
                                {
                                    if (worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null || worksheet.Cells[row, 3].Value == null 
                                        || worksheet.Cells[row, 4].Value == null || worksheet.Cells[row, 5].Value == null
                                        || worksheet.Cells[row, 6].Value == null || worksheet.Cells[row, 7].Value == null)
                                    {
                                        continue;
                                    }
                                    decimal QtyUnCut;
                                    if (!decimal.TryParse(worksheet.Cells[row, 6].Value == null || worksheet.Cells[row, 6].Value.ToString()=="" ? "0": worksheet.Cells[row, 6].Value.ToString(), out QtyUnCut))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "QtyUnCut row " + row.ToString() + " " + worksheet.Cells[row, 6].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    decimal QtySubStore;
                                    if (!decimal.TryParse(worksheet.Cells[row, 7].Value == null || worksheet.Cells[row, 7].Value.ToString()=="" ? "0": worksheet.Cells[row, 7].Value.ToString(), out QtySubStore))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "QtySubStore row " + row.ToString() + " " + worksheet.Cells[row, 7].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    //if (QtyUnCut == 0 && QtySubStore == 0)
                                    //{
                                    //    continue;
                                    //}
                                    int Month = 0;
                                    int MonthTemp = 0;
                                    if (!int.TryParse(worksheet.Cells[row, 2].Value.ToString(), out Month))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 2].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    if (MonthTemp == 0)
                                    {
                                        MonthTemp = int.Parse(worksheet.Cells[row, 2].Value.ToString());
                                    }
                                    if (Month > 12 || Month < 1)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 2].Value.ToString() + " not valid!" });
                                        continue;
                                    }
                                    if (MonthTemp != Month)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 2].Value.ToString() + " please import same month!" });
                                        continue;
                                    }

                                    int Year = 0;
                                    int YearTemp = 0;
                                    if (!int.TryParse(worksheet.Cells[row, 1].Value.ToString(), out Year))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 1].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    if (YearTemp == 0)
                                    {
                                        YearTemp = int.Parse(worksheet.Cells[row, 1].Value.ToString());
                                    }
                                    if (Year > 2100 || Year < 2000)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 1].Value.ToString() + " not valid!" });
                                        continue;
                                    }
                                    if (MonthTemp != Month)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 1].Value.ToString() + " please import same month!" });
                                        continue;
                                    }

                                    string RefVKNo = worksheet.Cells[row, 3].Value.ToString().Trim();
                                    string MatCD = worksheet.Cells[row, 4].Value.ToString().Trim();
                                    string PUnit = worksheet.Cells[row, 5].Value.ToString().Trim();
                                    var checkdata = TPRViewlist.FirstOrDefault(x => x.ProdInstCD.Contains(RefVKNo) && x.MatCD.Contains(MatCD) && x.PUnit.Contains(PUnit));
                                    if (checkdata == null)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find MatCD " + MatCD + " PUnit " + PUnit + " of " + RefVKNo + " in Parkerp" });
                                        continue;
                                    }
                                    ListInsert.Add(new TTWIPMat { ProdInstCD = RefVKNo, MatCD = MatCD,Unit=PUnit, QtyUnCut = QtyUnCut, QtySubStore = QtySubStore, Month = Month, Year = Year });


                                }
                                if (!ListInsert.Any())
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Pleace import QtyUnCut and QtySubStore" });
                                }
                                if (resultList.Count > 0)
                                {
                                    return resultList;

                                }

                                foreach (TTWIPMat ob in ListInsert)
                                {
                                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                    {
                                        var queryParameters = new DynamicParameters();
                                        connection.Open();
                                        var checkExsist = connection.Query<TTWIPMat>("SELECT * FROM dbo.TTWIPMat WHERE ProdInstCD='" + ob.ProdInstCD + "' AND MatCD='" + ob.MatCD + "' AND Month=" + ob.Month + " AND Year=" + ob.Year + "  "
                                            , queryParameters
                                            , commandTimeout: 1500
                                            , commandType: CommandType.Text)
                                            .ToList();
                                        if (checkExsist.Any())//Tồn tại thì update
                                        {
                                            TTWIPMat item = checkExsist.FirstOrDefault();
string query = "Update dbo.TTWIPMat Set QtyUnCut=" + ob.QtyUnCut + "*dbo.ConvQtyGR('" + ob.Unit + "','" + item.Unit + "'),QtySubStore=" + ob.QtySubStore + "*dbo.ConvQtyGR('" + ob.Unit + "','" + item.Unit + "')" +
",UpdatedBy=" + GetUserlogin() + ",UpdatedOn=GETDATE() WHERE WIPMat=" + item.WIPMat + "";
                                            connection.Query<TTWIPMat>(query
                                                                , queryParameters
                                                                , commandTimeout: 1500
                                                                , commandType: CommandType.Text)
                                                                .ToList();
                                        }
                                        else
                                        {
                                            await repository.Insert_SaveChange(ob);                                       
                                        }
                                        connection.Close();
                                    }
                                }
                                return resultList;
                            }
                        }

                    }
                    return resultList;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());

            }
            return resultList;
        }
        public async Task<List<Result>> importXLS_UnCutSubStore(string Path, List<TPRViewModel> TPRView)
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            FileStream existingFile = new FileStream(HttpContext.Current.Server.MapPath(Path), FileMode.Open);
            var workbook = Workbook.Load(existingFile);
            var worksheet = workbook.Worksheets.FirstOrDefault();
            var cells = worksheet.Cells;
            #region Check file exel no header name


            //if (worksheet.Cells[0, 0].Value == null)
            //{
            //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no header name" });
            //    return resultList;
            //}


            #endregion
            List<TTWIPMat> ListInsert = new List<TTWIPMat>();
            for (int row = cells.FirstRowIndex + 1; row <= cells.LastRowIndex; row++)
            {

                if (worksheet.Cells[row, 0].Value == null || worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null 
                    || worksheet.Cells[row, 3].Value == null || worksheet.Cells[row, 4].Value == null 
                    || worksheet.Cells[row, 5].Value == null || worksheet.Cells[row, 6].Value == null)
                {
                    continue;
                }
                decimal QtyUnCut;
                if (!decimal.TryParse(worksheet.Cells[row, 5].Value.ToString()==""? "0":worksheet.Cells[row, 5].Value.ToString(), out QtyUnCut))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "QtyUnCut row " + row.ToString() + " " + worksheet.Cells[row, 5].Value.ToString() + " not number!" });
                    continue;
                }
                decimal QtySubStore;
                if (!decimal.TryParse(worksheet.Cells[row, 6].Value.ToString()==""?"0": worksheet.Cells[row, 6].Value.ToString(), out QtySubStore))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "QtySubStore row " + row.ToString() + " " + worksheet.Cells[row, 6].Value.ToString() + " not number!" });
                    continue;
                }
                if (QtyUnCut == 0 && QtySubStore == 0)
                {
                    continue;
                }
                int Month = 0;
                int MonthTemp = 0;
                if (!int.TryParse(worksheet.Cells[row, 1].Value.ToString(), out Month))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 1].Value.ToString() + " not number!" });
                    continue;
                }
                if (MonthTemp == 0)
                {
                    MonthTemp = int.Parse(worksheet.Cells[row, 1].Value.ToString());
                }
                if (Month > 12 || Month < 1)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 1].Value.ToString() + " not valid!" });
                    continue;
                }
                if (MonthTemp != Month)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Month row " + row.ToString() + " " + worksheet.Cells[row, 1].Value.ToString() + " please import same month!" });
                    continue;
                }

                int Year = 0;
                int YearTemp = 0;
                if (!int.TryParse(worksheet.Cells[row, 0].Value.ToString(), out Year))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 0].Value.ToString() + " not number!" });
                    continue;
                }
                if (YearTemp == 0)
                {
                    YearTemp = int.Parse(worksheet.Cells[row, 0].Value.ToString());
                }
                if (Year > 2100 || Year < 2000)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 0].Value.ToString() + " not valid!" });
                    continue;
                }
                if (MonthTemp != Month)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Year row " + row.ToString() + " " + worksheet.Cells[row, 0].Value.ToString() + " please import same month!" });
                    continue;
                }

                string RefVKNo = worksheet.Cells[row, 2].Value.ToString().Trim();
                string MatCD = worksheet.Cells[row, 3].Value.ToString().Trim();
                string PUnit = worksheet.Cells[row, 4].Value.ToString().Trim();

                if (TPRView.Where(x => x.ProdInstCD.Contains(RefVKNo) && x.MatCD.Contains(MatCD) && x.PUnit.Contains(PUnit)).Count() == 0)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find MatCD " + MatCD + " PUnit " + PUnit + " of " + RefVKNo + " in Parkerp" });
                    continue;
                }
                ListInsert.Add(new TTWIPMat { ProdInstCD = RefVKNo, MatCD = MatCD,Unit= PUnit, QtyUnCut = QtyUnCut,QtySubStore=QtySubStore, Month = Month, Year = Year });

            }
            if (!ListInsert.Any())
            {
                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Pleace import QtyUnCut and QtySubStore" });
            }
            if (resultList.Count > 0)
            {
                return resultList;

            }
            foreach (TTWIPMat ob in ListInsert)
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var checkExsist= connection.Query<TTWIPMat>("SELECT * FROM dbo.TTWIPMat (NOLOCK) WHERE ProdInstCD='" + ob.ProdInstCD + "' AND MatCD='" + ob.MatCD + "' AND Month=" + ob.Month + " AND Year=" + ob.Year + "  "
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.Text)
                        .ToList();
                    if (checkExsist.Any())//Tồn tại thì update
                    {
                        TTWIPMat item = checkExsist.FirstOrDefault();
string query = "Update dbo.TTWIPMat Set QtyUnCut=" + ob.QtyUnCut + "*dbo.ConvQtyGR('" + ob.Unit + "','" + item.Unit + "'),QtySubStore=" + ob.QtySubStore + "*dbo.ConvQtyGR('" + ob.Unit + "','" + item.Unit + "')" +
",UpdatedBy=" + GetUserlogin() + ",UpdatedOn=GETDATE() WHERE WIPMat="+ item.WIPMat + "";
                        connection.Query<TTWIPMat>(query
                                            , queryParameters
                                            , commandTimeout: 1500
                                            , commandType: CommandType.Text)
                                            .ToList();
                    }
                    else
                    {
                        await repository.Insert_SaveChange(ob);
                    }
                    connection.Close();
                }
            }
            return resultList;
        }




    }
}

﻿using Dapper;
using ExcelLibrary.SpreadSheet;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.WIB.MatLoss;
using HR.API.Models.WIB.StoreViewModel.GI;
using HR.API.Models.WIB.StoreViewModel.WIP.TPR;
using HR.API.Models.WIB.WIPMat;
using HR.API.Repository;
using HR.API.Repository.WIP;
using HR.API.UnitOfWork;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.WIP
{
    [HR.API.App_Start.JwtAuthentication]
    public class TTMatLossController : BaseController
    {
        private GenericRepository<TTMatLoss> repository;
        private ITTMatLoss_Repository iTTMatLoss_Repository;
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TTMatLoss>(unitOfWork);
            iTTMatLoss_Repository = new TTMatLoss_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("WIP_TTMatLoss_Insert")]
        public async Task<Boolean> WIP_TTMatLoss_Insert(TTMatLoss item)
        {
            Initialization();
            return await iTTMatLoss_Repository.WIP_TTMatLoss_Insert(item);
        }
        [HttpPost]
        [Route("WIP_TTMatLoss_PageSize")]
        public async Task<IList<TTMatLoss_Custom>> WIP_TTWIPMat_Get_UnCutSubStorePageSize(TTMatLoss_Custom item)
        {
            Initialization();
            return await iTTMatLoss_Repository.WIP_TTMatLoss_PageSize(item);
        }
        [HttpPost]
        [Route("WIP_TTMatLoss_PageSize_Excel")]
        public HttpResponseMessage ERP_TTProdPrice_Pagesize_Excel(TTMatLoss_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "WIPMatLossLeather" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTTMatLoss_Repository.WIP_TTMatLoss_PageSize_Excel(item), "MatLoss"); ;
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_TTMatLoss_Import")]
        public async Task<List<Result>> WIP_TTMatLoss_Import()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            PARKERPEntities db = new PARKERPEntities();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                string Path = "";
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        Path = "~/Upload/Excel/WIP" + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath(Path);
                        postedFile.SaveAs(filePath);
                    }
                    FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));
                    List<TPRViewModel> TPRViewlist = new List<TPRViewModel>(); ;
                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    {
                        var queryParameters = new DynamicParameters();
                        connection.Open();
                        TPRViewlist = connection.Query<TPRViewModel>("SELECT a.ProdInstCD,d.ProdCode,c.MatCD FROM TProdInstItem a (NOLOCK) INNER JOIN dbo.TCons b (NOLOCK) ON a.ConsID=b.ConsID INNER JOIN dbo.TConsItem c (NOLOCK) ON b.ConsID=c.ConsID INNER JOIN dbo.TProd d (NOLOCK) ON d.ProdID=a.ProdID INNER JOIN dbo.TMat e ON c.MatCD=e.MatCD INNER JOIN dbo.TMH f ON e.MHID=f.MHID WHERE d.Discontinue=0 AND f.MH1Code='L' GROUP BY a.ProdInstCD,d.ProdCode,c.MatCD"
                            , queryParameters
                            , commandTimeout: 1500
                            , commandType: CommandType.Text)
                            .ToList();
                        connection.Close();
                        if (TPRViewlist == null)
                        {
                            TPRViewlist = new List<TPRViewModel>();
                        }
                    }

                    if (existingFile.Extension.ToString() == ".xls")
                    {
                        return await importXLS_TTMatLoss_Import(Path, TPRViewlist);
                    }

                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        ExcelWorkbook workBook = package.Workbook;
                        if (workBook != null)
                        {
                            if (workBook.Worksheets.Count > 0)
                            {
                                ExcelWorksheet worksheet = workBook.Worksheets.FirstOrDefault();
                                //if (worksheet == null)
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SheetName WIP_Inventory in file excel" });
                                //    return resultList;
                                //}
                                int rowCount = worksheet.Dimension.End.Row;

                                List<TTMatLoss> ListInsert = new List<TTMatLoss>();
                                #region Check file exel no header name


                                //if (worksheet.Cells[1, 1].Value == null)
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                //    return resultList;
                                //}
                                //if (worksheet.Cells[1, 1].Value.ToString().Trim() != "Prod Order")
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                //    return resultList;
                                //}


                                #endregion
                                for (int row = 2; row <= rowCount; row++)
                                {
                                    if (worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null || worksheet.Cells[row, 3].Value == null
                                        || worksheet.Cells[row, 4].Value == null)
                                    {
                                        continue;
                                    }
                                    decimal LossRate;
                                    if (!decimal.TryParse(worksheet.Cells[row, 4].Value == null || worksheet.Cells[row, 4].Value.ToString() == "" ? "0" : worksheet.Cells[row, 4].Value.ToString(), out LossRate))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "LossRate row " + row.ToString() + " " + worksheet.Cells[row, 4].Value.ToString() + " not number!" });
                                        continue;
                                    }
                           

                                    string RefVKNo = worksheet.Cells[row, 1].Value.ToString().Trim();
                                    string ProdCode = worksheet.Cells[row, 2].Value.ToString().Trim();
                                    string MatCD = worksheet.Cells[row, 3].Value.ToString().Trim();

                                    if (TPRViewlist.Where(x => x.ProdInstCD.Contains(RefVKNo) && x.ProdCode.Contains(ProdCode) && x.MatCD.Contains(MatCD)).Count() == 0)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdCode " + ProdCode + " MatCD " + MatCD + " of " + RefVKNo + " in Parkerp" });
                                        continue;
                                    }
                                    ListInsert.Add(new TTMatLoss { ProdInstCD = RefVKNo, MatCD = MatCD, ProdCode = ProdCode, LossRate = LossRate });


                                }
                                if (!ListInsert.Any())
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Pleace import LossRate" });
                                }
                                if (resultList.Count > 0)
                                {
                                    return resultList;

                                }

                                foreach (TTMatLoss ob in ListInsert)
                                {
                                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                    {
                                        var queryParameters = new DynamicParameters();
                                        connection.Open();
                                        var checkExsist = connection.Query<TTMatLoss>("SELECT * FROM dbo.TTMatLoss (NOLOCK) WHERE ProdInstCD='" + ob.ProdInstCD + "' AND MatCD='" + ob.MatCD + "' AND ProdCode='" + ob.ProdCode + "'  "
                                            , queryParameters
                                            , commandTimeout: 1500
                                            , commandType: CommandType.Text)
                                            .ToList();
                                        if (checkExsist.Any())//Tồn tại thì update
                                        {
                                            TTMatLoss item = checkExsist.FirstOrDefault();
                                            string query = "Update dbo.TTMatLoss Set LossRate=" + ob.LossRate + "" +
                                            ",UpdatedBy=" + GetUserlogin() + ",UpdatedOn=GETDATE() WHERE MatLossID=" + item.MatLossID + "";
                                            connection.Query<TTMatLoss>(query
                                                                , queryParameters
                                                                , commandTimeout: 1500
                                                                , commandType: CommandType.Text)
                                                                .ToList();
                                        }
                                        else
                                        {
                                            await repository.Insert_SaveChange(ob);
                                        }
                                        connection.Close();
                                    }
                                }
                                return resultList;
                            }
                        }

                    }
                    return resultList;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());

            }
            return resultList;
        }
        public async Task<List<Result>> importXLS_TTMatLoss_Import(string Path, List<TPRViewModel> TPRView)
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            FileStream existingFile = new FileStream(HttpContext.Current.Server.MapPath(Path), FileMode.Open);
            var workbook = Workbook.Load(existingFile);
            var worksheet = workbook.Worksheets.FirstOrDefault();
            var cells = worksheet.Cells;
            #region Check file exel no header name


            //if (worksheet.Cells[0, 0].Value == null)
            //{
            //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no header name" });
            //    return resultList;
            //}


            #endregion
            List<TTMatLoss> ListInsert = new List<TTMatLoss>();
            for (int row = cells.FirstRowIndex + 1; row <= cells.LastRowIndex; row++)
            {

                if (worksheet.Cells[row, 0].Value == null || worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null
                    || worksheet.Cells[row, 3].Value == null )
                {
                    continue;
                }
                decimal LossRate;
                if (!decimal.TryParse(worksheet.Cells[row, 3].Value.ToString() == "" ? "0" : worksheet.Cells[row, 3].Value.ToString(), out LossRate))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "LossRate row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not number!" });
                    continue;
                }

                string RefVKNo = worksheet.Cells[row, 0].Value.ToString().Trim();
                string ProdCode = worksheet.Cells[row, 1].Value.ToString().Trim();
                string MatCD = worksheet.Cells[row, 2].Value.ToString().Trim();

                if (TPRView.Where(x => x.ProdInstCD.Contains(RefVKNo) && x.MatCD.Contains(MatCD) && x.ProdCode.Contains(ProdCode)).Count() == 0)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdCode " + ProdCode + " MatCD " + MatCD + " of " + RefVKNo + " in Parkerp" });
                    continue;
                }
                ListInsert.Add(new TTMatLoss { ProdInstCD = RefVKNo, MatCD = MatCD, ProdCode = ProdCode, LossRate = LossRate });

            }
            if (!ListInsert.Any())
            {
                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Pleace import LossRate" });
            }
            if (resultList.Count > 0)
            {
                return resultList;

            }
            foreach (TTMatLoss ob in ListInsert)
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var checkExsist = connection.Query<TTMatLoss>("SELECT * FROM dbo.TTMatLoss (NOLOCK) WHERE ProdInstCD='" + ob.ProdInstCD + "' AND MatCD='" + ob.MatCD + "' AND ProdCode='" + ob.ProdCode + "'  "
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.Text)
                        .ToList();
                    if (checkExsist.Any())//Tồn tại thì update
                    {
                        TTMatLoss item = checkExsist.FirstOrDefault();
                        string query = "Update dbo.TTMatLoss Set LossRate=" + ob.LossRate + "" +
                        ",UpdatedBy=" + GetUserlogin() + ",UpdatedOn=GETDATE() WHERE MatLossID=" + item.MatLossID + "";
                        connection.Query<TTMatLoss>(query
                                            , queryParameters
                                            , commandTimeout: 1500
                                            , commandType: CommandType.Text)
                                            .ToList();
                    }
                    else
                    {
                        await repository.Insert_SaveChange(ob);
                    }
                    connection.Close();
                }
            }
            return resultList;
        }

    }
}

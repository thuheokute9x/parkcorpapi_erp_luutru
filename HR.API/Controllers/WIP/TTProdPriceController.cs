﻿using Dapper;
using ExcelLibrary.SpreadSheet;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models.WIB.ProdPrice;
using HR.API.Models.WIB.StoreViewModel.GI;
using HR.API.Repository;
using HR.API.Repository.WIP;
using HR.API.UnitOfWork;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.WIP
{
    [HR.API.App_Start.JwtAuthentication]
    public class TTProdPriceController : BaseController
    {
        private GenericRepository<TTProdPrice> repository;
        private ITTProdPrice_Repository iTTProdPrice_Repository;
        public TTProdPriceController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TTProdPrice>(unitOfWork);
            iTTProdPrice_Repository = new TTProdPrice_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TTProdPrice_InsertList")]
        public async Task<IList<TTProdPrice>> ERP_TTProdPrice_InsertList(List<TTProdPrice> itemlist)
        {
            Initialization();
            List<ProdInstItemViewModel> ProdInstItemlist = new List<ProdInstItemViewModel>();
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                var queryParameters = new DynamicParameters();
                connection.Open();
                string Query = "SELECT a.ProdInstCD,b.ProdCode,SUM(c.Qty) Qty,d.ProdPriceID  FROM TProdInstItem a (NOLOCK) INNER JOIN dbo.TProd b(NOLOCK) ON a.ProdID = b.ProdID AND b.Discontinue = 0 LEFT JOIN dbo.TSalseInfo c(NOLOCK) ON a.ProdInstItemID = c.ProdInstItemID LEFT JOIN dbo.TTProdPrice d(NOLOCK) ON a.ProdInstCD = d.ProdInstCD AND b.ProdCode=d.ProdCode GROUP BY a.ProdInstCD,b.ProdCode,d.ProdPriceID";
                ProdInstItemlist = connection.Query<ProdInstItemViewModel>(Query
                    , queryParameters
                    , commandTimeout: 1500
                    , commandType: CommandType.Text)
                    .ToList();
            }
            foreach (TTProdPrice dt in itemlist)
            {
                ProdInstItemViewModel ProdInstItem = ProdInstItemlist.FirstOrDefault(x => x.ProdInstCD.Contains(dt.ProdInstCD) && x.ProdCode.Contains(dt.ProdCode));
                if (ProdInstItem!=null)
                {
                    dt.SalseQty = ProdInstItem.Qty;
                }
                if (dt.ProdPriceID!=0)
                {
                    await repository.Delete_SaveChange(dt);
                }
            }
            return await repository.BulkInsert(itemlist);
        }
        [HttpPost]
        [Route("ERP_TTProdPrice_Pagesize")]
        public async Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_Pagesize(TTProdPrice_Custom item)
        {
            Initialization();
            return await iTTProdPrice_Repository.ERP_TTProdPrice_Pagesize(item);
        }
        [HttpPost]
        [Route("ERP_TTProdPrice_Pagesize_Excel")]
        public HttpResponseMessage ERP_TTProdPrice_Pagesize_Excel(TTProdPrice_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "ProdPrice" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTTProdPrice_Repository.ERP_TTProdPrice_Pagesize_Excel(item), "ProdPrice"); ;
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;

            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TTProdPrice_ByProdInstCD")]
        public async Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_ByProdInstCD(TTProdPrice_Custom item)
        {
            Initialization();
            return await iTTProdPrice_Repository.ERP_TTProdPrice_ByProdInstCD(item);
        }
        [HttpGet]
        [Route("ERP_TTProdPrice_ByBrandCD")]
        public async Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_ByBrandCD()
        {
            Initialization();
            return await iTTProdPrice_Repository.ERP_TTProdPrice_ByBrandCD();
        }
        [HttpPost]
        [Route("ERP_TTProdPrice_ProdCode")]
        public async Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_ProdCode(TTProdPrice_Custom item)
        {
            Initialization();
            return await iTTProdPrice_Repository.ERP_TTProdPrice_ProdCode(item);
        }
        [HttpPost]
        [Route("ERP_TTProdPrice_SKUCode")]
        public async Task<IList<TTProdPrice_Custom>> ERP_TTProdPrice_SKUCode(TTProdPrice_Custom item)
        {
            Initialization();
            return await iTTProdPrice_Repository.ERP_TTProdPrice_SKUCode(item);
        }
        [HttpPost]
        [Route("ERP_TTProdPrice_Import_Excel")]
        public async Task<Data> ERP_TTProdPrice_Import_Excel()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            Data kq = new Data();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                string Path = "";
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        Path = "~/Upload/Excel/WIP" + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath(Path);
                        postedFile.SaveAs(filePath);
                    }
                    FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));
                    //List<TProdInst> TProdInstList = db.TProdInsts.Where(x => x.Discontinue == false).ToList();
                    //List<TProd> TProdsList = db.TProds.Where(x => x.Discontinue == false).ToList();
                    List<ProdInstItemViewModel> ProdInstItemlist;
                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    {
                        var queryParameters = new DynamicParameters();
                        connection.Open();
                        string Query = "SELECT a.ProdInstCD,b.ProdCode,SUM(c.Qty) Qty,d.ProdPriceID  FROM TProdInstItem a (NOLOCK) INNER JOIN dbo.TProd b(NOLOCK) ON a.ProdID = b.ProdID AND b.Discontinue = 0 LEFT JOIN dbo.TSalseInfo c(NOLOCK) ON a.ProdInstItemID = c.ProdInstItemID LEFT JOIN dbo.TTProdPrice d(NOLOCK) ON a.ProdInstCD = d.ProdInstCD AND b.ProdCode=d.ProdCode GROUP BY a.ProdInstCD,b.ProdCode,d.ProdPriceID";
                        ProdInstItemlist = connection.Query<ProdInstItemViewModel>(Query
                            , queryParameters
                            , commandTimeout: 1500
                            , commandType: CommandType.Text)
                            .ToList();
                        if (ProdInstItemlist == null)
                        {
                            ProdInstItemlist = new List<ProdInstItemViewModel>();
                        }
                    }

                    if (existingFile.Extension.ToString() == ".xls")
                    {
                        return await importXLS(Path, ProdInstItemlist);
                    }

                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        ExcelWorkbook workBook = package.Workbook;
                        if (workBook != null)
                        {
                            if (workBook.Worksheets.Count > 0)
                            {
                                ExcelWorksheet worksheet = workBook.Worksheets.FirstOrDefault();
                                //ExcelWorksheet worksheet = workBook.Worksheets.Where(x => x.Name.Contains("ProdPrice")).FirstOrDefault();
                                //if (worksheet == null)
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SheetName ProdPrice in file excel" });
                                //    return resultList;
                                //}
                                int rowCount = worksheet.Dimension.End.Row;

                                List<TTProdPrice> ListInsert = new List<TTProdPrice>();
                                List<int> TTProdPriceDelete = new List<int>();//Nếu đã tồn tại thì xóa những thằng này
                                #region Check file exel no header name


                                //if (worksheet.Cells[1, 1].Value == null)
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                //    return resultList;
                                //}
                                //if (worksheet.Cells[1, 1].Value.ToString().Trim() != "Prod Order")
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                //    return resultList;
                                //}


                                #endregion
                                for (int row = 2; row <= rowCount; row++)
                                {

                                    if (worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null || worksheet.Cells[row, 3].Value == null || worksheet.Cells[row, 4].Value == null)
                                    {
                                        continue;
                                    }
                                    decimal FOB;
                                    if (!decimal.TryParse(worksheet.Cells[row, 3].Value.ToString(), out FOB))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "FOB row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    decimal MatConst;
                                    if (!decimal.TryParse(worksheet.Cells[row, 4].Value.ToString(), out MatConst))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "MatConst row " + row.ToString() + " " + worksheet.Cells[row, 4].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    string RefVKNo = worksheet.Cells[row, 1].Value.ToString().Trim();
                                    string ProdCode = worksheet.Cells[row, 2].Value.ToString().Trim();
                                    List<ProdInstItemViewModel> ProdInstItem = ProdInstItemlist.Where(x => x.ProdInstCD.Contains(RefVKNo) && x.ProdCode.Contains(ProdCode)).ToList();
                                    if (ProdInstItem.Count() == 0)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdCode " + ProdCode + " of " + RefVKNo + " in Parkerp" });
                                        continue;
                                    }
                                    ProdInstItemViewModel ob = ProdInstItem.FirstOrDefault();
                                    decimal? SalseQty = 0;
                                    if (ob != null)
                                    {
                                        SalseQty = ob.Qty;
                                        if (ob.ProdPriceID != null)
                                        {
                                            TTProdPriceDelete.Add(ob.ProdPriceID.Value);
                                        }

                                    }
                                    ListInsert.Add(new TTProdPrice { ProdInstCD = RefVKNo, ProdCode = ProdCode, SalseQty = SalseQty, FOB = FOB, MatCost = MatConst });

                                }
                                if (ListInsert.Count == 0)
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File import no data" });
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;

                                }
                                var CheckDoule = (from c in ListInsert//check trùng ProdCode và Prod Order
                                                  group c by new
                                                  {
                                                      c.ProdCode,
                                                      c.ProdInstCD,
                                                  } into gcs
                                                  where gcs.Count() > 1
                                                  select new TTProdPrice()
                                                  {
                                                      ProdCode = gcs.Key.ProdCode,
                                                      ProdInstCD = gcs.Key.ProdInstCD
                                                  }).ToList();
                                if (CheckDoule.Count() >0)
                                {
                                    foreach (TTProdPrice item in CheckDoule)
                                    {
                                        resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Double Prod Order " + item.ProdInstCD + " and ProdCode " + item.ProdCode + "" });
                                    }
                                }
                                if (resultList.Count > 0)
                                {
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;

                                }
                                List<TProdInstItem_Custom> TProdInstItem_Custom = new List<TProdInstItem_Custom>();
                                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                {
                                    var queryParameters = new DynamicParameters();
                                    connection.Open();
                                    var data = await connection.QueryAsync<TProdInstItem_Custom>("ERP_TProdInstItem_ByProdInstCD", queryParameters, commandTimeout: 1500
                                        , commandType: CommandType.StoredProcedure);
                                    connection.Close();
                                    TProdInstItem_Custom = data.ToList();
                                }
                                var query =(from a in ListInsert
                                           join b in TProdInstItem_Custom on a.ProdInstCD equals b.ProdInstCD 
                                            where a.ProdCode == b.ProdCode
                                            select new TProdInstItem_Custom { ProdInstCD = b.ProdInstCD
                                            ,BrandCD = b.BrandCD, RangeDesc = b.RangeDesc,
                                                ProdCode = b.ProdCode,
                                                SKUCode = b.SKUCode,
                                                ProdDesc = b.ProdDesc,
                                                Color = b.Color,
                                                FOB = a.FOB,
                                                MatCost = a.MatCost,
                                                ProdPriceID = b.ProdPriceID,
                                            }).ToList();



                                // await repository.BulkInsert(ListInsert);
                                kq.Results = query;
                                kq.Type = 1;
                                return kq;

                            }
                        }

                    }
                    kq.Results = resultList;
                    kq.Type = 0;
                    return kq;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());

            }
            kq.Results = resultList;
            kq.Type = 0;
            return kq;
        }
        public async Task<Data> importXLS(string Path, List<ProdInstItemViewModel> ProdInstItemList)
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            Data kq = new Data();
            FileStream existingFile = new FileStream(HttpContext.Current.Server.MapPath(Path), FileMode.Open);
            var workbook = Workbook.Load(existingFile);
            var worksheet = workbook.Worksheets.FirstOrDefault();
            //var worksheet = workbook.Worksheets.Where(x => x.Name.Contains("ProdPrice")).FirstOrDefault();
            //if (worksheet == null)
            //{
            //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SheetName ProdPrice in file excel" });
            //    return resultList;
            //}
            var cells = worksheet.Cells;
            #region Check file exel no header name


            //if (worksheet.Cells[0, 0].Value == null)
            //{
            //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
            //    return resultList;
            //}
            //if (worksheet.Cells[0, 0].Value.ToString().Trim() != "Prod Order")
            //{
            //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
            //    return resultList;
            //}


            #endregion
            List<TTProdPrice> ListInsert = new List<TTProdPrice>();
            List<int> TTProdPriceDelete = new List<int>();//Nếu đã tồn tại thì xóa những thằng này

            for (int row = cells.FirstRowIndex + 1; row <= cells.LastRowIndex; row++)
            {

                if (worksheet.Cells[row, 0].Value == null || worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null || worksheet.Cells[row, 3].Value == null)
                {
                    continue;
                }
                decimal FOB;
                if (!decimal.TryParse(worksheet.Cells[row, 2].Value.ToString(), out FOB))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "FOB row " + row.ToString() + " " + worksheet.Cells[row, 2].Value.ToString() + " not number!" });
                    continue;
                }
                decimal MatConst;
                if (!decimal.TryParse(worksheet.Cells[row, 3].Value.ToString(), out MatConst))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "MatConst row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not number!" });
                    continue;
                }
                string RefVKNo = worksheet.Cells[row, 0].Value.ToString().Trim();
                string ProdCode = worksheet.Cells[row, 1].Value.ToString().Trim();
                List<ProdInstItemViewModel> ProdInstItem = ProdInstItemList.Where(x => x.ProdInstCD.Contains(RefVKNo) && x.ProdCode.Contains(ProdCode)).ToList();
                if (ProdInstItem.Count() == 0)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdCode " + ProdCode + " of " + RefVKNo + " in Parkerp" });
                    continue;
                }
                //if (ProdInstItemList.Where(x => x.ProdCode.Contains(ProdCode)).Count() == 0)
                //{
                //    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdCode " + ProdCode + " in ERP" });
                //    continue;
                //}
                ProdInstItemViewModel ob = ProdInstItem.FirstOrDefault();
                decimal? SalseQty = 0;
                if (ob != null)
                {
                    SalseQty = ob.Qty;
                    if (ob.ProdPriceID!=null)
                    {
                        TTProdPriceDelete.Add(ob.ProdPriceID.Value);
                    }
                    
                }
                ListInsert.Add(new TTProdPrice { ProdInstCD = RefVKNo, ProdCode = ProdCode,SalseQty= SalseQty, FOB = FOB, MatCost = MatConst });

            }
            if (ListInsert.Count == 0)
            {
                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File import no data" });
                kq.Results = resultList;
                kq.Type = 0;
                return kq;

            }
            var CheckDoule = (from c in ListInsert//check trùng ProdCode và Prod Order
                             group c by new 
                             {
                                 c.ProdCode,
                                 c.ProdInstCD,
                             } into gcs
                             where gcs.Count() > 1
                             select new TTProdPrice()
                             {
                                 ProdCode = gcs.Key.ProdCode,
                                 ProdInstCD = gcs.Key.ProdInstCD
                             }).ToList();
            if (CheckDoule.Count() > 0)
            {
                foreach (TTProdPrice item in CheckDoule)
                {
                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Double Prod Order " + item.ProdInstCD + " and ProdCode " + item.ProdCode + "" });
                }
            }
            if (resultList.Count > 0)
            {
                kq.Results = resultList;
                kq.Type = 0;
                return kq;

            }
            foreach (int dt in TTProdPriceDelete)
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string Query = "DELETE dbo.TTProdPrice WHERE ProdPriceID="+ dt + "";
                    var data = await connection.QueryAsync<Result>(Query
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.Text);
                }
            }
            List<TProdInstItem_Custom> TProdInstItem_Custom = new List<TProdInstItem_Custom>();
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                var queryParameters = new DynamicParameters();
                connection.Open();
                var data = await connection.QueryAsync<TProdInstItem_Custom>("ERP_TProdInstItem_ByProdInstCD", queryParameters, commandTimeout: 1500
                    , commandType: CommandType.StoredProcedure);
                connection.Close();
                TProdInstItem_Custom = data.ToList();
            }
            var query = (from a in ListInsert
                         join b in TProdInstItem_Custom on a.ProdInstCD equals b.ProdInstCD
                         where a.ProdCode == b.ProdCode
                         select new TProdInstItem_Custom
                         {
                             ProdInstCD = b.ProdInstCD
                         ,
                             BrandCD = b.BrandCD,
                             RangeDesc = b.RangeDesc,
                             ProdCode = b.ProdCode,
                             SKUCode = b.SKUCode,
                             ProdDesc = b.ProdDesc,
                             Color = b.Color,
                             FOB = a.FOB,
                             MatCost = a.MatCost,
                             ProdPriceID = b.ProdPriceID,
                         }).ToList();
            //await repository.BulkInsert(ListInsert);
            kq.Results = query;
            kq.Type = 1;
            return kq;
        }

    }
}

﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;



namespace HR.API.Controllers.WIP
{
    [HR.API.App_Start.JwtAuthentication]
    public class FG_GR_ConsController : BaseController
    {
        [HttpGet]
        [Route("WIP_GetFG_GR_Cons")]
        public async Task<IList<WIB_FG_GR_ConsModel>> WIP_GetFG_GR_Cons(string prodcode, string Payment, string RefVKNo, string MatCD, string MatName, int PageNumber,int PageSize)
        {
            try
            {
                List<WIB_FG_GR_ConsModel> result = new List<WIB_FG_GR_ConsModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    //DateTime date;
                    //date = DateTime.ParseExact(monthyear, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    //queryParameters.Add("@Month", date.Month);
                    //queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(Payment) ? "" : Payment);
                    queryParameters.Add("@RefVKNo", (string.IsNullOrEmpty(RefVKNo) || RefVKNo == "All") ? "" : RefVKNo.Trim());
                    queryParameters.Add("@MatCD", (string.IsNullOrEmpty(MatCD) || MatCD == "All") ? "" : MatCD.Trim());
                    queryParameters.Add("@Prodcode", (string.IsNullOrEmpty(prodcode) || prodcode == "All") ? "" : prodcode.Trim());
                    queryParameters.Add("@MatName", (string.IsNullOrEmpty(MatName) || MatName == "All") ? "" : MatName.Trim());
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", PageNumber);
                    queryParameters.Add("@PageSize", PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_FG_GR_ConsModel>("WIP_GetFG_GR_Cons"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_GetFG_GR_ConsExcel")]
        public HttpResponseMessage WIP_GetFG_GR_ConsExcel(WIB_FG_GR_ConsModel item)
        {
            try
            {
                //List<WIB_FG_GR_ConsModel> result = new List<WIB_FG_GR_ConsModel>();
                //using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                //{
                //    var queryParameters = new DynamicParameters();
                //    queryParameters.Add("@Payment", item.Payment);
                //    queryParameters.Add("@RefVKNo",item.RefVKNo);
                //    queryParameters.Add("@Prodcode",item.ProdCode);
                //    queryParameters.Add("@MatCD",item.MatCD);
                //    queryParameters.Add("@MatName", item.MatName);
                //    queryParameters.Add("@Type", 1);
                //    connection.Open();
                //    var data = await connection.QueryAsync<WIB_FG_GR_ConsModel>("WIP_GetFG_GR_Cons"
                //        , queryParameters
                //        , commandTimeout: 1500
                //        , commandType: CommandType.StoredProcedure);
                //    result = data.ToList();
                //}
                //return result;

                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("WIP_GetFG_GR_Cons", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Payment", SqlDbType.NVarChar).Value = item.Payment;
                cmd.Parameters.Add("@RefVKNo", SqlDbType.NVarChar).Value = item.RefVKNo;
                cmd.Parameters.Add("@Prodcode", SqlDbType.NVarChar).Value = item.ProdCode;
                cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = item.MatCD;
                cmd.Parameters.Add("@MatName", SqlDbType.NVarChar).Value = item.MatName;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "FG_GR_Cons" + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(table, "FG_GR_Cons");
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                GC.SuppressFinalize(this);
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }


        [HttpGet]
        [Route("WIP_GetFG_GR_Cons_Sum")]
        public async Task<IList<WIB_FG_GR_ConsModel>> WIP_GetFG_GR_Cons_Sum(string RefVKNo, string MatName, string monthyear, string MatCD, string Payment, int PageNumber, int PageSize)
         {
            try
            {
                List<WIB_FG_GR_ConsModel> result = new List<WIB_FG_GR_ConsModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    //DateTime date;
                    //date = DateTime.ParseExact(monthyear, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    //queryParameters.Add("@Month", date.Month);
                    //queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", PageNumber);
                    queryParameters.Add("@PageSize", PageSize);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(Payment) ? "" : Payment);
                    queryParameters.Add("@RefVKNo", (string.IsNullOrEmpty(RefVKNo) || RefVKNo == "All") ? "" : RefVKNo.Trim());
                    queryParameters.Add("@MatCD", (string.IsNullOrEmpty(MatCD) || MatCD == "All") ? "" : MatCD.Trim());
                    queryParameters.Add("@MatName", (string.IsNullOrEmpty(MatName) || MatName == "All") ? "" : MatName.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_FG_GR_ConsModel>("WIP_GetFG_GR_Cons_SumMATCD"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpGet]
        [Route("WIP_GetFG_GR_Cons_SumExcel")]
        public async Task<IList<WIB_FG_GR_ConsModel>> WIP_GetFG_GR_Cons_SumExcel(string RefVKNo, string MatName, string monthyear, string MatCD, string Payment)
        {
            try
            {
                List<WIB_FG_GR_ConsModel> result = new List<WIB_FG_GR_ConsModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Type", 1);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(Payment)?"": Payment);
                    queryParameters.Add("@RefVKNo", (string.IsNullOrEmpty(RefVKNo) || RefVKNo == "All") ? "" : RefVKNo.Trim());
                    queryParameters.Add("@MatCD", (string.IsNullOrEmpty(MatCD) || MatCD == "All") ? "" : MatCD.Trim());
                    queryParameters.Add("@MatName", (string.IsNullOrEmpty(MatName) || MatName == "All") ? "" : MatName.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_FG_GR_ConsModel>("WIP_GetFG_GR_Cons_SumMATCD"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
    }
}

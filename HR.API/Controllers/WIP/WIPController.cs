﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Models.WIB.WIP;
using HR.API.Repository;
using HR.API.Repository.ERP.ExRateDaily;
using HR.API.UnitOfWork;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.WIP
{
    [HR.API.App_Start.JwtAuthentication]
    public class WIPController : BaseController
    {
        public WIPController()
        {
        }
        [HttpPost]
        [Route("WIP_Get_EWIP_GR_Excel")]
        public HttpResponseMessage WIP_Get_EWIP_GR_Excel(EWIPModel wip)
        {

            try
            {
                
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {

                    DataTable table = new DataTable();
                    SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                    SqlCommand cmd = new SqlCommand("WIP_Get_EWIP_GR", con);
                    cmd.CommandTimeout = 1500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = wip.FromDate;
                    cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = wip.ToDate;
                    cmd.Parameters.Add("@ProdInstCDType", SqlDbType.NVarChar).Value = this.WIP_GetProdInstCDWithCompany();
                    cmd.Parameters.Add("@Payment", SqlDbType.NVarChar).Value = wip.Payment;
                    cmd.Parameters.Add("@ConsType", SqlDbType.NVarChar).Value = wip.ConsType;
                    cmd.Parameters.Add("@Costing", SqlDbType.Bit).Value = wip.Costing;
                    cmd.Parameters.Add("@ClosedOnFGGR", SqlDbType.Bit).Value = wip.ClosedOnFGGR;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        da.Fill(table);
                    }



                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    string filename = "WIP" + DateTime.Today.ToString("yyyyhh") + ".xlsx";
                    Byte[] bytes = ExportDataTableToExcelFromStore(table, filename, true, false);
                    response.Content = new ByteArrayContent(bytes);
                    response.Content.Headers.ContentLength = bytes.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_Get_EWIP_GR")]
        public async Task<IList<EWIPModel>> WIP_Get_EWIP_GR(EWIPModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@FromDate", item.FromDate);
                    queryParameters.Add("@ToDate", item.ToDate);
                    queryParameters.Add("@ProdInstCDType", WIP_GetProdInstCDWithCompany());
                    queryParameters.Add("@Payment", item.Payment);
                    queryParameters.Add("@ConsType", item.ConsType);
                    queryParameters.Add("@Costing", item.Costing);
                    queryParameters.Add("@ClosedOnFGGR", item.ClosedOnFGGR);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<EWIPModel>("WIP_Get_EWIP_GR"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_Get_EWIP_Check_BWIP")]
        public async Task<Boolean> WIP_Get_EWIP_Check_BWIP(EWIPModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    DateTime date = item.FromDate.AddMonths(-1);
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    connection.Open();
                    var data = await connection.QueryAsync<EWIPModel>("WIP_Get_EWIP_Check_BWIP"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    if (data.ToList().Any())
                    {
                        return await Task.FromResult(true);
                    }
                    return await Task.FromResult(false);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_Get_EWIP_Excel")]
        public async Task<HttpResponseMessage> WIP_Get_EWIP_Excel(EWIPModel wip)
        {

            try
            {

                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@FromDate", wip.FromDate);
                    queryParameters.Add("@ToDate", wip.ToDate);
                    queryParameters.Add("@ProdInstCDType", WIP_GetProdInstCDWithCompany());
                    queryParameters.Add("@Payment", wip.Payment);
                    queryParameters.Add("@ConsType", wip.ConsType);
                    queryParameters.Add("@Costing", wip.Costing);
                    queryParameters.Add("@ClosedOnFGGR", wip.ClosedOnFGGR);
                    queryParameters.Add("@Type", 1);
                    connection.Open();
                    var result = await connection.QueryAsync<EWIPModel>("WIP_Get_EWIP"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = result.ToList();
                    ExcelPackage excel = new ExcelPackage();
                    var workSheet = excel.Workbook.Worksheets.Add("WIP");


                    if (result.Any())
                    {
                        var item = result.FirstOrDefault();
                        workSheet.Cells[1, 1, 1, 30].Style.Font.Bold = true;

                        workSheet.Cells[1, 13].Value = "Total";
                        //workSheet.Cells[1, 1, 1, 24].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //workSheet.Cells[1, 1, 1, 24].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffff00"));

                        workSheet.Cells[1, 15].Value = item.SumQtyEWIB;
                        workSheet.Cells[1, 15].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[1, 16].Value = item.SumPRICEEWIB;
                        workSheet.Cells[1, 16].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[1, 17].Value = item.SumQtyBWIP;
                        workSheet.Cells[1, 17].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[1, 18].Value = item.SumPRICEBWIP;
                        workSheet.Cells[1, 18].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[1, 19].Value = item.SumQtyGI;
                        workSheet.Cells[1, 19].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[1, 20].Value = item.SumPRICEGI;
                        workSheet.Cells[1, 20].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[1, 21].Value = item.SumQtyFGGR_Cons;
                        workSheet.Cells[1, 21].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[1, 22].Value = item.SumPRICEFGGR_Cons;
                        workSheet.Cells[1, 22].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[1, 23].Value = item.SumQtyUnCut;
                        workSheet.Cells[1, 23].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[1, 24].Value = item.SumAmtUnCut;
                        workSheet.Cells[1, 24].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[1, 25].Value = item.SumQtySubStore;
                        workSheet.Cells[1, 25].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[1, 26].Value = item.SumAmtSubStore;
                        workSheet.Cells[1, 26].Style.Numberformat.Format = "#0.00";
                    }



                    workSheet.Cells[2, 1, 2, 30].Style.Font.Bold = true;
                    workSheet.Cells[2, 15].Value = "E.WIP";
                    workSheet.Cells[2, 15, 2, 16].Merge = true;
                    workSheet.Cells[2, 17].Value = "B.WIP";
                    workSheet.Cells[2, 17, 2, 18].Merge = true;
                    workSheet.Cells[2, 19].Value = "GI";
                    workSheet.Cells[2, 19, 2, 20].Merge = true;
                    if (wip.ConsType== "NoLoss")
                    {
                        workSheet.Cells[2, 21].Value = "FG-GR ConsNL";
                    }
                    if (wip.ConsType == "LeatherLoss")
                    {
                        workSheet.Cells[2, 21].Value = "FG-GR ConsLL";
                    }
                    if (wip.ConsType == "AllLoss")
                    {
                        workSheet.Cells[2, 21].Value = "FG-GR ConsAL";
                    }
                    workSheet.Cells[2, 21, 2, 22].Merge = true;
                    workSheet.Cells[2, 23].Value = "UnCut";
                    workSheet.Cells[2, 23, 2, 24].Merge = true;
                    workSheet.Cells[2, 25].Value = "SubStore";
                    workSheet.Cells[2, 25, 2, 26].Merge = true;



                    workSheet.Cells[3, 1, 3, 30].Style.Font.Bold = true;
                    workSheet.Cells[3, 1].Value = "ProdOrder";
                    workSheet.Cells[3, 2].Value = "MatCD";
                    workSheet.Cells[3, 3].Value = "MatDesc";
                    workSheet.Cells[3, 4].Value = "MatColor";
                    workSheet.Cells[3, 5].Value = "MH";
                    workSheet.Cells[3, 6].Value = "PUnit";
                                    
                    workSheet.Cells[3, 7].Value = "PriceLot";
                    workSheet.Cells[3, 8].Value = "PricePInfo";
                    workSheet.Cells[3, 9].Value = "Curr";
                                    
                    workSheet.Cells[3, 10].Value = "PriceGR";
                    workSheet.Cells[3, 11].Value = "Curr";
                    workSheet.Cells[3, 12].Value = "Diff(%)";
                    workSheet.Cells[3, 13].Value = "UnitPriceUSD";
                                    
                    workSheet.Cells[3, 14].Value = "Payment";
                    workSheet.Cells[3, 15].Value = "Qty";
                    workSheet.Cells[3, 16].Value = "Amt";
                    workSheet.Cells[3, 17].Value = "Qty";
                    workSheet.Cells[3, 18].Value = "Amt";
                    workSheet.Cells[3, 19].Value = "Qty";
                    workSheet.Cells[3, 20].Value = "Amt";
                    workSheet.Cells[3, 21].Value = "Qty";
                    workSheet.Cells[3, 22].Value = "Amt";
                    workSheet.Cells[3, 23].Value = "Qty";
                    workSheet.Cells[3, 24].Value = "Amt";
                    workSheet.Cells[3, 25].Value = "Qty";
                    workSheet.Cells[3, 26].Value = "Amt";

                    int recordIndex = 4;
                    foreach (EWIPModel item in result)
                    {
                        //if (item.RowID==1)
                        //{
                        //    workSheet.Cells[recordIndex, 1, recordIndex, 23].Style.Font.Bold = true;
                        //    workSheet.Cells[recordIndex, 1].Value = "Total";
                        //    workSheet.Cells[recordIndex, 1, recordIndex, 13].Merge = true;
                        //    workSheet.Cells[recordIndex, 1, recordIndex, 23].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //    workSheet.Cells[recordIndex, 1, recordIndex, 23].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffff00"));

                        //    workSheet.Cells[recordIndex, 14].Value = item.SumQtyEWIB;
                        //    workSheet.Cells[recordIndex, 14].Style.Numberformat.Format = "#0.00";

                        //    workSheet.Cells[recordIndex, 15].Value = item.SumPRICEEWIB;
                        //    workSheet.Cells[recordIndex, 15].Style.Numberformat.Format = "#0.00";

                        //    workSheet.Cells[recordIndex, 16].Value = item.SumQtyBWIP;
                        //    workSheet.Cells[recordIndex, 16].Style.Numberformat.Format = "#0.00";

                        //    workSheet.Cells[recordIndex, 17].Value = item.SumPRICEBWIP;
                        //    workSheet.Cells[recordIndex, 17].Style.Numberformat.Format = "#0.00";

                        //    workSheet.Cells[recordIndex, 18].Value = item.SumQtyGI;
                        //    workSheet.Cells[recordIndex, 18].Style.Numberformat.Format = "#0.00";

                        //    workSheet.Cells[recordIndex, 19].Value = item.SumPRICEGI;
                        //    workSheet.Cells[recordIndex, 19].Style.Numberformat.Format = "#0.00";

                        //    workSheet.Cells[recordIndex, 20].Value = item.SumQtyFGGR_Cons;
                        //    workSheet.Cells[recordIndex, 20].Style.Numberformat.Format = "#0.00";

                        //    workSheet.Cells[recordIndex, 21].Value = item.SumPRICEFGGR_Cons;
                        //    workSheet.Cells[recordIndex, 21].Style.Numberformat.Format = "#0.00";

                        //    workSheet.Cells[recordIndex, 22].Value = item.SumQtyUnCut;
                        //    workSheet.Cells[recordIndex, 22].Style.Numberformat.Format = "#0.00";

                        //    workSheet.Cells[recordIndex, 23].Value = item.SumQtySubStore;
                        //    workSheet.Cells[recordIndex, 23].Style.Numberformat.Format = "#0.00";

                        //    recordIndex++;
                        //}

                        workSheet.Cells[recordIndex, 1].Value = item.ProdInstCD;
                        workSheet.Cells[recordIndex, 2].Value = item.MatCD;
                        workSheet.Cells[recordIndex, 3].Value = item.MatDesc;
                        workSheet.Cells[recordIndex, 4].Value = item.Color;
                        workSheet.Cells[recordIndex, 5].Value = item.MH1;
                        workSheet.Cells[recordIndex, 6].Value = item.PUNIT;

                        workSheet.Cells[recordIndex, 7].Value = item.PriceLotSize;
                        workSheet.Cells[recordIndex, 7].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 8].Value = item.PriceIncoterms;
                        workSheet.Cells[recordIndex, 9].Value = item.CIFCurr;

                        workSheet.Cells[recordIndex, 10].Value = item.PRICEGR;
                        workSheet.Cells[recordIndex, 11].Value = item.CURR;

                        workSheet.Cells[recordIndex, 12].Value = item.Comp;
                        workSheet.Cells[recordIndex, 12].Style.Numberformat.Format = "#0%";

                        workSheet.Cells[recordIndex, 13].Value = item.PRICEUSD;
                        workSheet.Cells[recordIndex, 14].Value = item.Payment;


                        workSheet.Cells[recordIndex, 15].Value = item.QtyEWIB;
                        workSheet.Cells[recordIndex, 15].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 16].Value = item.PRICEEWIB;
                        workSheet.Cells[recordIndex, 16].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 17].Value = item.QtyBWIP;
                        workSheet.Cells[recordIndex, 17].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 18].Value = item.PRICEBWIP;
                        workSheet.Cells[recordIndex, 18].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 19].Value = item.QtyGI;
                        workSheet.Cells[recordIndex, 19].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 20].Value = item.PRICEGI;
                        workSheet.Cells[recordIndex, 20].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 21].Value = item.QtyFGGR_Cons;
                        workSheet.Cells[recordIndex, 21].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 22].Value = item.PRICEFGGR_Cons;
                        workSheet.Cells[recordIndex, 22].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 23].Value = item.QtyUnCut;
                        workSheet.Cells[recordIndex, 23].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 24].Value = item.AmtUnCut;
                        workSheet.Cells[recordIndex, 24].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 25].Value = item.QtySubStore;
                        workSheet.Cells[recordIndex, 25].Style.Numberformat.Format = "#0.00";

                        workSheet.Cells[recordIndex, 26].Value = item.AmtSubStore;
                        workSheet.Cells[recordIndex, 26].Style.Numberformat.Format = "#0.00";

                        recordIndex++;
                    }
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    string filename = "WIP" + DateTime.Today.ToString("yyyyhh") + ".xlsx";
                    Byte[] bytes = excel.GetAsByteArray();
                    response.Content = new ByteArrayContent(bytes);
                    response.Content.Headers.ContentLength = bytes.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_Get_EWIP")]
        public async Task<IList<EWIPModel>> WIP_Get_EWIP(EWIPModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@FromDate", item.FromDate);
                    queryParameters.Add("@ToDate", item.ToDate);
                    queryParameters.Add("@ProdInstCDType", WIP_GetProdInstCDWithCompany());
                    queryParameters.Add("@Payment", item.Payment);
                    queryParameters.Add("@ConsType", item.ConsType);
                    queryParameters.Add("@Costing", item.Costing);
                    queryParameters.Add("@ClosedOnFGGR", item.ClosedOnFGGR);
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@PageSize", item.PageSize);
                    connection.Open();
                    var data = await connection.QueryAsync<EWIPModel>("WIP_Get_EWIP"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpGet]
        [Route("WIB_Get")]
        public async Task<IList<WIBModel>> WIB_Get(string dateimport,string payment,string RefVKNo, string MatCD, string MatName, int PageNumber, int PageSize)
        {
            try
            {
                List<WIBModel> result = new List<WIBModel>();
                DateTime date = DateTime.ParseExact(dateimport, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@PageSize", PageSize);
                    queryParameters.Add("@PageNumber", PageNumber);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(payment) || payment == "All" ? "" : payment.Trim());
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(RefVKNo) || RefVKNo == "All" ? "" : RefVKNo.Trim());
                    queryParameters.Add("@MatCD", string.IsNullOrEmpty(MatCD) || MatCD == "All" ? "" : MatCD.Trim());
                    queryParameters.Add("@MatName", string.IsNullOrEmpty(MatName) || MatName == "All" ? "" : MatName.Trim());
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<WIBModel>("WIP_Get"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpGet]
        [Route("WIB_GetExcel")]
        public async Task<HttpResponseMessage> WIB_GetExcel(string dateimport, string payment, string RefVKNo, string MatCD, string MatName)
        {

            try
            {
                List<WIBModel> result = new List<WIBModel>();
                DateTime date = DateTime.ParseExact(dateimport, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(payment) || payment == "All" ? "" : payment.Trim());
                    queryParameters.Add("@RefVKNo", (string.IsNullOrEmpty(RefVKNo) || RefVKNo == "All") ? "" : RefVKNo.Trim());
                    queryParameters.Add("@MatCD", (string.IsNullOrEmpty(MatCD) || MatCD == "All") ? "" : MatCD.Trim());
                    queryParameters.Add("@MatName", (string.IsNullOrEmpty(MatName) || MatName == "All") ? "" : MatName.Trim());
                    queryParameters.Add("@Type", 1);
                    connection.Open();
                    var data = await connection.QueryAsync<WIBModel>("WIP_Get"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                    ExcelPackage excel = new ExcelPackage();
                    var workSheet = excel.Workbook.Worksheets.Add("WIP");
                    workSheet.Cells[1, 1].Value = "Prod Order";
                    workSheet.Cells[1, 2].Value = "Mat CD";
                    workSheet.Cells[1, 3].Value = "Mat Desc";
                    workSheet.Cells[1, 4].Value = "Mat color";
                    workSheet.Cells[1, 5].Value = "Unit";
                    workSheet.Cells[1, 6].Value = "Unit price";
                    workSheet.Cells[1, 7].Value = "Currency";
                    workSheet.Cells[1, 8].Value = "Payment";
                    workSheet.Cells[1, 9].Value = "GI Qty";
                    workSheet.Cells[1, 10].Value = "GI Amt";
                    workSheet.Cells[1, 11].Value = "FG-GR Cons";
                    workSheet.Cells[1, 12].Value = "FG-GR Amt";
                    workSheet.Cells[1, 13].Value = "WIP Qty";
                    workSheet.Cells[1, 14].Value = "WIP Amt";

                    int recordIndex = 2;
                    foreach (WIBModel item in result)
                    {
                        workSheet.Cells[recordIndex, 1].Value = item.ProdInstCD;
                        workSheet.Cells[recordIndex, 2].Value = item.MatCD;
                        workSheet.Cells[recordIndex, 3].Value = item.MatName;
                        workSheet.Cells[recordIndex, 4].Value = item.Color;
                        workSheet.Cells[recordIndex, 5].Value = item.PUNIT;
                        workSheet.Cells[recordIndex, 6].Value = item.PRICE;
                        workSheet.Cells[recordIndex, 7].Value = item.CURR;
                        workSheet.Cells[recordIndex, 8].Value = item.Payment;
                        workSheet.Cells[recordIndex, 9].Value = item.GIQty;
                        workSheet.Cells[recordIndex, 10].Value = item.GIAmt;
                        workSheet.Cells[recordIndex, 11].Value = item.FGGRQty;
                        workSheet.Cells[recordIndex, 12].Value = item.FGGRAmt;
                        workSheet.Cells[recordIndex, 13].Value = item.WIPQTY;
                        workSheet.Cells[recordIndex, 14].Value = item.WIPAmt;

                        recordIndex++;
                    }
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    string filename = "WIP" + date.ToString("yyyyhh") + ".xlsx";
                    Byte[] bytes = excel.GetAsByteArray();
                    response.Content = new ByteArrayContent(bytes);
                    response.Content.Headers.ContentLength = bytes.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_Get_EWIP_GrProdOrder")]
        public async Task<IList<EWIPModel>> WIP_Get_EWIP_GrProdOrder(EWIPModel wip)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@FromDate", wip.FromDate);
                    queryParameters.Add("@ToDate", wip.ToDate);
                    queryParameters.Add("@ProdInstCDType", this.WIP_GetProdInstCDWithCompany());
                    queryParameters.Add("@Payment", wip.Payment == "''" ? "" : wip.Payment);
                    queryParameters.Add("@Costing", wip.Costing);
                    queryParameters.Add("@ClosedOnFGGR", wip.ClosedOnFGGR);
                    queryParameters.Add("@ConsType", wip.ConsType);
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<EWIPModel>("WIP_Get_EWIP_GrProdOrder"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_Get_EWIP_GrProdOrder_Excel")]
        public HttpResponseMessage ERP_STin_List_GetPriceLastExcel(EWIPModel wip)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string filename = "WIPForProdOrder" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            Byte[] bytes = ExportDataTableToExcelFromStore(WIP_Get_EWIP_GrProdOrderDataTable(wip), "WIPForProdOrder",false,true, "WIP_Get_EWIP_GrProdOrder");
            response.Content = new ByteArrayContent(bytes);
            response.Content.Headers.ContentLength = bytes.LongLength;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = filename;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
            return response;
        }
        public DataTable WIP_Get_EWIP_GrProdOrderDataTable(EWIPModel item)
        {
            try
            {
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("WIP_Get_EWIP_GrProdOrder", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = item.FromDate;
                cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = item.ToDate;
                cmd.Parameters.Add("@ProdInstCDType", SqlDbType.NVarChar).Value = this.WIP_GetProdInstCDWithCompany();
                cmd.Parameters.Add("@Payment", SqlDbType.NVarChar).Value = item.Payment == "''" ? "" : item.Payment;
                cmd.Parameters.Add("@Costing", SqlDbType.Bit).Value = item.Costing;
                cmd.Parameters.Add("@ClosedOnFGGR", SqlDbType.Bit).Value = item.ClosedOnFGGR;
                cmd.Parameters.Add("@ConsType", SqlDbType.NVarChar).Value = item.ConsType;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                return table;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_Get_EWIP_CheckList")]
        public async Task<IList<EWIPModel>> WIP_Get_EWIP_CheckList(EWIPModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@FromDate", item.FromDate);
                    queryParameters.Add("@ToDate", item.ToDate);
                    queryParameters.Add("@ProdInstCDType", this.WIP_GetProdInstCDWithCompany());
                    queryParameters.Add("@Payment", item.Payment);
                    queryParameters.Add("@Costing", item.Costing);
                    queryParameters.Add("@ClosedOnFGGR", item.ClosedOnFGGR);
                    connection.Open();
                    var data = await connection.QueryAsync<EWIPModel>("WIP_Get_EWIP_CheckList"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_EWIP_TransferWIP_InsertGI_ReturnGI")]
        public async Task<bool> WIP_EWIP_TransferWIP_InsertGI_ReturnGI(EWIPModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameterscheck = new DynamicParameters();
                    string Query = "SELECT * FROM dbo.TTWIPMat (NOLOCK) WHERE Year='" + item.ToDate.Year + "' AND Month='" + item.ToDate.Month + "' AND ISNULL(Qty,0)<>0";
                    var checkWIPMat = await connection.QueryAsync<TTWIPMat>(Query
                                    , queryParameterscheck
                                    , commandTimeout: 1500
                                    , commandType: CommandType.Text);
                    if (checkWIPMat.ToList().Any())
                    {
                        throw new NotImplementedException("EWIP of "+item.ToDate.ToString("MMM/yyyy")+ " exists already. Please check ToMonth for Transfer WIP and inform to IT Dept.");
                    }




                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@FromDate", item.FromDate);
                    queryParameters.Add("@ToDate", item.ToDate);
                    queryParameters.Add("@ProdInstCDType", this.WIP_GetProdInstCDWithCompany());
                    queryParameters.Add("@CreatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<EWIPModel>("WIP_EWIP_TransferWIP_InsertGI_ReturnGI"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return await Task.FromResult(true);

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_EWIP_DeleteWIPMaterial_DeleteGI_DeleteGI")]
        public async Task<bool> WIP_EWIP_DeleteWIPMaterial_DeleteGI_DeleteGI(EWIPModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@ToDate", item.ToDate);
                    queryParameters.Add("@CreatedBy", GetUserlogin());
                    connection.Open();
                    var data = await connection.QueryAsync<EWIPModel>("WIP_EWIP_DeleteWIPMaterial_DeleteGI_DeleteGI"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return await Task.FromResult(true);

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

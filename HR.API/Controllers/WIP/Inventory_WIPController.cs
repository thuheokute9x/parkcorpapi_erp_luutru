﻿using Dapper;
using ExcelLibrary.SpreadSheet;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.WIB.StoreViewModel.GI;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.WIP
{
    [HR.API.App_Start.JwtAuthentication]
    public class Inventory_WIPController : BaseController
    {
        [HttpPost]
        [Route("Insert_WIP_TInventory")]
        public async Task<List<Result>> Insert_WIP_TInventory()
        {
            List<Result> resultList = new List<Result>();
            PARKERPEntities db = new PARKERPEntities();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                string EmpID = httpRequest.Form["EmpID"];
                string dateimport = httpRequest.Form["dateimport"];
                DateTime date= DateTime.ParseExact(dateimport, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);

                string Path = "";
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        Path = "~/Upload/Excel/WIP" + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath(Path);
                        postedFile.SaveAs(filePath);
                    }
                    FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));
                    //List<TProdInst> TProdInstList = db.TProdInsts.Where(x => x.Discontinue == false).ToList();
                    //List<TProd> TProdsList = db.TProds.Where(x => x.Discontinue == false).ToList();
                    List<ProdInstItemViewModel> ProdInstItemlist;
                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    {
                        var queryParameters = new DynamicParameters();
                        connection.Open();
                        ProdInstItemlist =  connection.Query<ProdInstItemViewModel>("SELECT a.ProdInstCD,b.ProdCode FROM TProdInstItem a (NOLOCK) "+
                        "INNER JOIN dbo.TProd b(NOLOCK) ON a.ProdID = b.ProdID AND b.Discontinue = 0"
                            , queryParameters
                            , commandTimeout: 1500
                            , commandType: CommandType.Text)
                            .ToList();
                        if (ProdInstItemlist==null)
                        {
                            ProdInstItemlist = new List<ProdInstItemViewModel>();
                        }
                    }

                    if (existingFile.Extension.ToString()==".xls")
                    {
                        return await importXLS(Path, ProdInstItemlist, EmpID,date);
                    }

                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        ExcelWorkbook workBook = package.Workbook;
                        if (workBook != null)
                        {
                            if (workBook.Worksheets.Count > 0)
                            {
                                ExcelWorksheet worksheet = workBook.Worksheets.Where(x=>x.Name.Contains("WIP_Inventory")).FirstOrDefault();
                                if (worksheet==null)
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SheetName WIP_Inventory in file excel" });
                                    return resultList;
                                }
                                int rowCount = worksheet.Dimension.End.Row;

                                List<InventoryWIPModel> ListInsert = new List<InventoryWIPModel>();
                                #region Check file exel no header name


                                if (worksheet.Cells[1, 1].Value == null)
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                    return resultList;
                                }
                                if (worksheet.Cells[1, 1].Value.ToString().Trim() != "Prod Order")
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                    return resultList;
                                }


                                #endregion
                                for (int row = 2; row <= rowCount; row++)
                                {
                                    if (worksheet.Cells[row, 1].Value==null || worksheet.Cells[row, 2].Value == null || worksheet.Cells[row, 3].Value == null || worksheet.Cells[row, 4].Value == null)
                                    {
                                        continue;
                                    }
                                    float OrderQty;
                                    if (!float.TryParse(worksheet.Cells[row, 3].Value.ToString(),out OrderQty))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Order Qty row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    float FGQty;
                                    if (!float.TryParse(worksheet.Cells[row, 4].Value.ToString(), out FGQty))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "FG-GR Qty row " + row.ToString() + " " + worksheet.Cells[row, 4].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    string RefVKNo = worksheet.Cells[row, 1].Value.ToString().Trim();
                                    string ProdCode = worksheet.Cells[row, 2].Value.ToString().Trim();

                                    if (ProdInstItemlist.Where(x=>x.ProdInstCD.Contains(RefVKNo) && x.ProdCode.Contains(ProdCode)).Count()==0)
                                    {
                                        resultList.Add(new Result { ErrorCode=row,ErrorMessage= "Not find ProdCode " + ProdCode +" of "+ RefVKNo + " in Parkerp" });
                                        continue;
                                    }
                                    //if (ProdInstItemlist.Where(x => x.ProdCode.Contains(ProdCode)).Count() == 0)
                                    //{
                                    //    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdCode " + ProdCode + " in ERP" });
                                    //    continue;
                                    //}

                                    ListInsert.Add(new InventoryWIPModel { ProdInstCD= RefVKNo ,ProdCode=ProdCode,OrderQty= OrderQty, FGQty= FGQty });


                                }
                                if (resultList.Count > 0)
                                {
                                    return resultList;

                                }
                                foreach (InventoryWIPModel item in ListInsert)
                                {
                                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                    {
                                        var queryParameters = new DynamicParameters();
                                        queryParameters.Add("@Month", date.Month);
                                        queryParameters.Add("@Year",date.Year);
                                        queryParameters.Add("@RefVKNo", item.ProdInstCD);
                                        queryParameters.Add("@ProdCode", item.ProdCode);
                                        queryParameters.Add("@Total", item.OrderQty);
                                        queryParameters.Add("@FinishedProduct", item.FGQty);
                                        queryParameters.Add("@EmpID", EmpID);
                                        queryParameters.Add("@Type", 1);
                                        connection.Open();
                                        var data = await connection.QueryAsync<Result>("WIP_Get_Insert_Del_TInventory"
                                            , queryParameters
                                            , commandTimeout: 1500
                                            , commandType: CommandType.StoredProcedure);
                                    }
                                }


                            }
                        }

                    }
                    return resultList;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());

            }
            return resultList;
        }


        public async Task<List<Result>> importXLS(string Path,List<ProdInstItemViewModel> ProdInstItemList, string EmpID,DateTime date)
        {

            List<Result> resultList = new List<Result>();
            FileStream existingFile = new FileStream(HttpContext.Current.Server.MapPath(Path), FileMode.Open);
            var workbook = Workbook.Load(existingFile);
            var worksheet = workbook.Worksheets.Where(x => x.Name.Contains("WIP_Inventory")).FirstOrDefault();
            if (worksheet == null)
            {
                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SheetName WIP_Inventory in file excel" });
                return resultList;
            }
            var cells = worksheet.Cells;
            #region Check file exel no header name


            if (worksheet.Cells[0, 0].Value == null)
            {
                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                return resultList;
            }
            if (worksheet.Cells[0, 0].Value.ToString().Trim() != "Prod Order")
            {
                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                return resultList;
            }


            #endregion
            List<InventoryWIPModel> ListInsert = new List<InventoryWIPModel>();

            for (int row = cells.FirstRowIndex + 1; row <= cells.LastRowIndex; row++)
            {

                if (worksheet.Cells[row, 0].Value == null || worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null || worksheet.Cells[row, 3].Value == null)
                {
                    continue;
                }
                float OrderQty;
                if (!float.TryParse(worksheet.Cells[row, 2].Value.ToString(), out OrderQty))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Order Qty row " + row.ToString() + " " + worksheet.Cells[row, 2].Value.ToString() + " not number!" });
                    continue;
                }
                float FGQty;
                if (!float.TryParse(worksheet.Cells[row, 3].Value.ToString(), out FGQty))
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "FG Qty row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not number!" });
                    continue;
                }
                string RefVKNo = worksheet.Cells[row, 0].Value.ToString().Trim();
                string ProdCode = worksheet.Cells[row, 1].Value.ToString().Trim();

                if (ProdInstItemList.Where(x => x.ProdInstCD.Contains(RefVKNo) && x.ProdCode.Contains(ProdCode)).Count() == 0)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdCode " + ProdCode + " of " + RefVKNo + " in Parkerp" });
                    continue;
                }
                //if (ProdInstItemList.Where(x => x.ProdCode.Contains(ProdCode)).Count() == 0)
                //{
                //    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdCode " + ProdCode + " in ERP" });
                //    continue;
                //}

                ListInsert.Add(new InventoryWIPModel { ProdInstCD = RefVKNo, ProdCode = ProdCode, OrderQty = OrderQty, FGQty = FGQty });

            }
            if (resultList.Count > 0)
            {
                return resultList;

            }
            foreach (InventoryWIPModel item in ListInsert)
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@RefVKNo", item.ProdInstCD);
                    queryParameters.Add("@ProdCode", item.ProdCode);
                    queryParameters.Add("@Total", item.OrderQty);
                    queryParameters.Add("@FinishedProduct", item.FGQty);
                    queryParameters.Add("@EmpID", EmpID);
                    queryParameters.Add("@Type", 1);
                    connection.Open();
                    var data = await connection.QueryAsync<Result>("WIP_Get_Insert_Del_TInventory"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                }
            }
            return resultList;
        }
        [HttpGet]
        [Route("WIP_Get_TInventory")]
        public async Task<IList<InventoryWIPModel>> WIP_Get_TInventory(string prodorder,string prodcode, int PageNumber, int PageSize)
        {
            try
            {
                List<InventoryWIPModel> result = new List<InventoryWIPModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Type", 0);
                    queryParameters.Add("@PageSize", PageSize);
                    queryParameters.Add("@PageNumber", PageNumber);
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(prodorder) || prodorder == "All" ? "" : prodorder.Trim());
                    queryParameters.Add("@ProdCode", string.IsNullOrEmpty(prodcode) || prodcode == "All" ? "" : prodcode.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<InventoryWIPModel>("WIP_Get_Insert_Del_TInventory"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }

        }
        [HttpGet]
        [Route("WIP_Get_TInventoryExcel")]
        public async Task<IList<InventoryWIPModel>> WIP_Get_TInventoryExcel(string prodorder, string prodcode)
        {
            try
            {
                List<InventoryWIPModel> result = new List<InventoryWIPModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(prodorder) || prodorder == "All" ? "" : prodorder.Trim());
                    queryParameters.Add("@ProdCode", string.IsNullOrEmpty(prodcode) || prodcode == "All" ? "" : prodcode.Trim());
                    queryParameters.Add("@Type", 3);
                    connection.Open();
                    var data = await connection.QueryAsync<InventoryWIPModel>("WIP_Get_Insert_Del_TInventory"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }

        }
        [HttpGet]
        [Route("WIP_Del_TInventory")]
        public async Task<IList<InventoryWIPModel>> WIP_Del_TInventory(string dateimport)
        {
            try
            {
                DateTime date = DateTime.ParseExact(dateimport, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                List<InventoryWIPModel> result = new List<InventoryWIPModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@Type", 2);
                    connection.Open();
                    var data = await connection.QueryAsync<InventoryWIPModel>("WIP_Get_Insert_Del_TInventory"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
    }
}

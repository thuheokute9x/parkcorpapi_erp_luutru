﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Models.WIB.GI;
using HR.API.Models.WIB.StoreViewModel.WIP.GI;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;



namespace HR.API.Controllers.WIP
{
    [HR.API.App_Start.JwtAuthentication]
    public class GI_WIP_Controller : BaseController
    {
        [HttpGet]
        [Route("WIB_Get_GI_SumMatCD_Excel")]
        public async Task<IList<WIB_GIModel>> WIB_Get_GI_SumMatCD_Excel(string RefVKNo, string MatName, string monthyear, string Payment,string MatCD)
        {

            try
            {
                List<WIB_GIModel> result = new List<WIB_GIModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    DateTime date;
                    date = DateTime.ParseExact(monthyear, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(Payment) || Payment == "All" ? "" : Payment.Trim());
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(RefVKNo) || RefVKNo == "All" ? "" : RefVKNo.Trim());
                    queryParameters.Add("@MatCD", string.IsNullOrEmpty(MatCD) || MatCD == "All" ? "" : MatCD.Trim());
                    queryParameters.Add("@MatName", string.IsNullOrEmpty(MatName) || MatName == "All" ? "" : MatName.Trim());
                    queryParameters.Add("@Type", 1);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_GIModel>("WIP_Get_GI_SumMATCD"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }

        [HttpPost]
        [Route("WIP_Get_GIFromTo_SumMATCDExcel")]
        public async Task<HttpResponseMessage> WIP_Get_GIFromTo_SumMATCDExcel(WIB_GIModel ob)
        {

            try
            {
                List<WIB_GIModel> result = new List<WIB_GIModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    DateTime fromdate;
                    fromdate = DateTime.ParseExact(ob.FromDate, "MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    DateTime todate;
                    todate = DateTime.ParseExact(ob.ToDate, "MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    //if (fromdate> todate)
                    //{
                    //    throw new NotImplementedException("Please choose ToMonth bigger FromMonth");
                    //}
                    queryParameters.Add("@FromDate", fromdate);
                    queryParameters.Add("@ToDate", todate);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(ob.Payment) || ob.Payment == "All" ? "" : ob.Payment.Trim());
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(ob.RefVKNo) || ob.RefVKNo == "All" ? "" : ob.RefVKNo.Trim());
                    queryParameters.Add("@MatCD", string.IsNullOrEmpty(ob.MatCD) || ob.MatCD == "All" ? "" : ob.MatCD.Trim());
                    queryParameters.Add("@MatName", string.IsNullOrEmpty(ob.MatName) || ob.MatName == "All" ? "" : ob.MatName.Trim());
                    queryParameters.Add("@Type", 1);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_GIModel>("WIP_Get_GIFromTo_SumMATCD"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();

                    ExcelPackage excel = new ExcelPackage();

                    var workSheet = excel.Workbook.Worksheets.Add("GIFromTo");
                    workSheet.Cells[1, 1].Value = "ProdOrder";
                    workSheet.Cells[1, 2].Value = "MatCD";
                    workSheet.Cells[1, 3].Value = "MatDesc";
                    workSheet.Cells[1, 4].Value = "MatColor";
                    workSheet.Cells[1, 5].Value = "PUnit";
                    workSheet.Cells[1, 6].Value = "UnitPrice";
                    workSheet.Cells[1, 7].Value = "Curr";
                    workSheet.Cells[1, 8].Value = "Payment";
                    workSheet.Cells[1, 9].Value = "Qty";
                    workSheet.Cells[1, 10].Value = "Amt";


                    int recordIndex = 2;
                    foreach (WIB_GIModel item in result)
                    {
                        workSheet.Cells[recordIndex, 1].Value = item.ProdInstCD;
                        workSheet.Cells[recordIndex, 2].Value = item.MatCD;
                        workSheet.Cells[recordIndex, 3].Value = item.MatName;
                        workSheet.Cells[recordIndex, 4].Value = item.Color;
                        workSheet.Cells[recordIndex, 5].Value = item.PUNIT;
                        workSheet.Cells[recordIndex, 6].Value = item.PRICE;
                        workSheet.Cells[recordIndex, 7].Value = item.CURR;
                        workSheet.Cells[recordIndex, 8].Value = item.Payment;
                        workSheet.Cells[recordIndex, 9].Value = item.OQTY;
                        workSheet.Cells[recordIndex, 10].Value = item.Amt;

                        recordIndex++;
                    }
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    string filename = "GIFromTo" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                    Byte[] bytes = excel.GetAsByteArray();
                    response.Content = new ByteArrayContent(bytes);
                    response.Content.Headers.ContentLength = bytes.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    return response;

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        [HttpGet]
        [Route("WIB_Get_GI_SumMatCD")]
        public async Task<IList<WIB_GIModel>> WIB_Get_GI_SumMatCD(string RefVKNo,string MatName, string monthyear, string Payment, string MatCD,int PageNumber, int PageSize)
        {

            try
            {
                List<WIB_GIModel> result = new List<WIB_GIModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    DateTime date;
                    date = DateTime.ParseExact(monthyear, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@PageSize", PageSize);
                    queryParameters.Add("@PageNumber", PageNumber);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(Payment) || Payment == "All" ? "" : Payment.Trim());
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(RefVKNo) || RefVKNo == "All" ? "" : RefVKNo.Trim());
                    queryParameters.Add("@MatCD", string.IsNullOrEmpty(MatCD) || MatCD == "All" ? "" : MatCD.Trim());
                    queryParameters.Add("@MatName", string.IsNullOrEmpty(MatName) || MatName == "All" ? "" : MatName.Trim());
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_GIModel>("WIP_Get_GI_SumMATCD"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpPost]
        [Route("WIP_Get_GIFromTo_SumMATCD")]
        public async Task<IList<WIB_GIModel>> WIP_Get_GIFromTo_SumMATCD(WIB_GIModel ob)
        {

            try
            {
                List<WIB_GIModel> result = new List<WIB_GIModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    DateTime fromdate;
                    fromdate = DateTime.ParseExact(ob.FromDate, "MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);

                    DateTime todate;
                    todate = DateTime.ParseExact(ob.ToDate, "MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    //if (fromdate> todate)
                    //{
                    //    throw new NotImplementedException("Please choose ToMonth bigger FromMonth");
                    //}

                    queryParameters.Add("@FromDate", fromdate);
                    queryParameters.Add("@ToDate", todate);
                    queryParameters.Add("@PageSize", ob.PageSize);
                    queryParameters.Add("@PageNumber", ob.PageNumber);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(ob.Payment) || ob.Payment == "All" ? "" : ob.Payment.Trim());
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(ob.RefVKNo) || ob.RefVKNo == "All" ? "" : ob.RefVKNo.Trim());
                    queryParameters.Add("@MatCD", string.IsNullOrEmpty(ob.MatCD) || ob.MatCD == "All" ? "" : ob.MatCD.Trim());
                    queryParameters.Add("@MatName", string.IsNullOrEmpty(ob.MatName) || ob.MatName == "All" ? "" : ob.MatName.Trim());
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_GIModel>("WIP_Get_GIFromTo_SumMATCD"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpGet]
        [Route("GetWIB_GI")]
        public async Task<IList<WIB_GIModel>> GetWIB_GI(string monthyear,string Payment, string RefVKNo, string MatCD, string MatName, int PageNumber, int PageSize)
        {

            try
            {
                List<WIB_GIModel> result = new List<WIB_GIModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    DateTime date;
                    date = DateTime.ParseExact(monthyear, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@PageSize", PageSize);
                    queryParameters.Add("@PageNumber", PageNumber);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(Payment) || Payment == "All" ? "" : Payment.Trim());
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(RefVKNo) || RefVKNo=="All" ? "" : RefVKNo.Trim());
                    queryParameters.Add("@MatCD", string.IsNullOrEmpty(MatCD) || MatCD=="All" ? "" : MatCD.Trim());
                    queryParameters.Add("@MatName", string.IsNullOrEmpty(MatName) || MatName=="All" ? "" : MatName.Trim());
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_GIModel>("WIP_Get_GI"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpGet]
        [Route("GetWIB_GIGKPayment")]
        public async Task<IList<CustomsMatViewModel>> GetWIB_GIGKPay(string Payment, string MatCD, int PageNumber, int PageSize)
        {

            try
            {
                List<object> result = new List<object>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();

                    var STCustomsMatList =await connection.QueryAsync<CustomsMatViewModel>("SELECT   a.MATCD,b.MatName,b.Color,a.PUnit,a.Payment " +
                    "FROM STCustomsMat a (NOLOCK) INNER JOIN dbo.TMat b(NOLOCK) ON a.MATCD = b.MatCD"
                    , queryParameters
                    , commandTimeout: 1500
                    , commandType: CommandType.Text);


                    var data = (from a in STCustomsMatList
                                where (String.IsNullOrEmpty(Payment) || a.Payment == Payment)
                                     && (String.IsNullOrEmpty(MatCD) || a.MATCD == MatCD)
                                select a).ToList();
                    if (data != null)
                    {
                        int TotalRows = data.Count();
                        var datapaging = (from a in data
                                          select new CustomsMatViewModel { MATCD = a.MATCD, MatName = a.MatName, Color = a.Color, PUnit = a.PUnit, Payment = a.Payment, TotalRows = TotalRows })
                                          .Skip(PageSize * (PageNumber - 1))
                                          .Take(PageSize).ToList();
                        return datapaging;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpGet]
        [Route("GetWIB_GIGKPaymentExcel")]
        public async Task<IList<CustomsMatViewModel>> GetWIB_GIGKPaymentExcel(string Payment, string MatCD)
        {

            try
            {


                List<object> result = new List<object>();
                PARKERPEntities db = new PARKERPEntities();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var STCustomsMatList = await connection.QueryAsync<CustomsMatViewModel>("SELECT   a.MATCD,b.MatName,b.Color,a.PUnit,a.Payment " +
                    "FROM STCustomsMat a (NOLOCK) INNER JOIN dbo.TMat b(NOLOCK) ON a.MATCD = b.MatCD"
                    , queryParameters
                    , commandTimeout: 1500
                    , commandType: CommandType.Text);

                    var data = (from a in STCustomsMatList
                                where (string.IsNullOrEmpty(Payment) || a.Payment == Payment) && (string.IsNullOrEmpty(MatCD) || a.MATCD == MatCD)
                                select new CustomsMatViewModel { MATCD = a.MATCD, MatName = a.MatName, Color = a.Color, PUnit = a.PUnit, Payment = a.Payment}).ToList();
                    return data;

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpGet]
        [Route("GetWIB_GIExcel")]
        public async Task<IList<WIB_GIModel>> GetWIB_GIExcel(string monthyear, string Payment, string RefVKNo, string MatCD, string MatName)
        {

            try
            {
                List<WIB_GIModel> result = new List<WIB_GIModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    DateTime date;
                    date = DateTime.ParseExact(monthyear, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(Payment) || Payment == "All" ? "" : Payment.Trim());
                    queryParameters.Add("@RefVKNo", (string.IsNullOrEmpty(RefVKNo) || RefVKNo == "All") ? "" : RefVKNo.Trim());
                    queryParameters.Add("@MatCD", (string.IsNullOrEmpty(MatCD) || MatCD =="All") ? "" : MatCD.Trim());
                    queryParameters.Add("@MatName", (string.IsNullOrEmpty(MatName) || MatName =="All") ? "" : MatName.Trim());
                    queryParameters.Add("@Type", 1);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_GIModel>("WIP_Get_GI"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }

        [HttpGet]
        [Route("Get_WIP_MatName")]
        public async Task<IList<WIB_MatNameModel>> Get_WIP_MatName(string monthyear)
        {
            try
            {
                List<WIB_MatNameModel> result = new List<WIB_MatNameModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    DateTime date;
                    date = DateTime.ParseExact(monthyear, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_MatNameModel>("WIP_Get_MatName"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                    result.Add(new WIB_MatNameModel { MatCD = "All",Search= "All" });
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpGet]
        [Route("WIP_Get_ProdOrder")]
        public async Task<IList<WIB_ProdOrderModel>> WIP_Get_ProdOrder(string monthyear)
        {
            try
            {
                List<WIB_ProdOrderModel> result = new List<WIB_ProdOrderModel>();

                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    DateTime date;
                    date = DateTime.ParseExact(monthyear, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_ProdOrderModel>("WIP_Get_ProdOrder"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                    result.Add(new WIB_ProdOrderModel { RefVKNo = "All" });
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpGet]
        [Route("WIP_Get_ProdCode")]
        public async Task<IList<WIB_ProdCoderModel>> WIP_Get_ProdCode()
        {
            try
            {
                List<WIB_ProdCoderModel> result = new List<WIB_ProdCoderModel>();

                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_ProdCoderModel>("WIP_Get_ProdCode"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                    result.Add(new WIB_ProdCoderModel { ProdCode = "All" });
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpGet]
        [Route("WIP_Get_MATCDGKPayment")]
        public async Task<IList<STCustomsMat>> WIP_Get_MATCDGKPayment()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<STCustomsMat>("SELECT MATCD FROM STCustomsMat (NOLOCK)"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.Text);
                    var result = data.ToList();
                    return result;

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpGet]
        [Route("WIP_Get_MATCD")]
        public async Task<IList<WIB_MATCDModel>> WIP_Get_MATCD(string monthyear)
        {
            try
            {
                List<WIB_MATCDModel> result = new List<WIB_MATCDModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    DateTime date;
                    date = DateTime.ParseExact(monthyear, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_MATCDModel>("WIP_Get_MATCD"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                    result.Add(new WIB_MATCDModel { MATCD = "All" });

                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpGet]
        [Route("WIP_Get_GIWithVKImport")]
        public async Task<IList<WIB_GIModel>> WIP_Get_GIWithVKImport()
        {

            try
            {
                List<WIB_GIModel> result = new List<WIB_GIModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    //queryParameters.Add("@PageSize", PageSize);
                    //queryParameters.Add("@PageNumber", PageNumber);
                    //queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(RefVKNo) ? "" : RefVKNo.Trim());
                    //queryParameters.Add("@MatCD", string.IsNullOrEmpty(MatCD) ? "" : MatCD.Trim());
                    //queryParameters.Add("@MatName", string.IsNullOrEmpty(MatName) ? "" : MatName.Trim());
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_GIModel>("WIP_Get_GIWithVKImport"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpGet]
        [Route("GI_WIP_Get_Payment")]
        public object GI_WIP_Get_Payment()
        {

            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    List<STCustomsMat> STCustomsMatList = connection.Query<STCustomsMat>
                    ("SELECT  Payment FROM STCustomsMat (NOLOCK) GROUP BY Payment"
                    , queryParameters
                    , commandTimeout: 1500
                    , commandType: CommandType.Text).ToList();

                    return STCustomsMatList;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }

        [HttpPost]
        [Route("WIP_Get_GI_FromTo")]
        public async Task<IList<WIB_GIModel>> WIP_Get_GI_FromTo(WIB_GIModel item)
        {

            try
            {
                List<WIB_GIModel> result = new List<WIB_GIModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    DateTime fromdate;
                    fromdate = DateTime.ParseExact(item.FromDate, "MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    DateTime todate;
                    todate = DateTime.ParseExact(item.ToDate, "MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    //if (fromdate> todate)
                    //{
                    //    throw new NotImplementedException("Please choose ToMonth bigger FromMonth");
                    //}
                    queryParameters.Add("@FromDate", fromdate);
                    queryParameters.Add("@ToDate", todate);
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@Payment", string.IsNullOrEmpty(item.Payment) || item.Payment == "All" ? "" : item.Payment.Trim());
                    queryParameters.Add("@ProdInstCDType", WIP_GetProdInstCDWithCompany());
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(item.RefVKNo) || item.RefVKNo == "All" ? "" : item.RefVKNo.Trim());
                    queryParameters.Add("@MatCD", string.IsNullOrEmpty(item.MatCD) || item.MatCD == "All" ? "" : item.MatCD.Trim());
                    queryParameters.Add("@MatName", string.IsNullOrEmpty(item.MatName) || item.MatName == "All" ? "" : item.MatName.Trim());
                    queryParameters.Add("@Costing", item.Costing);
                    queryParameters.Add("@IsWFG", item.IsWFG);
                    queryParameters.Add("@ClosedOnFGGR", item.ClosedOnFGGR);
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<WIB_GIModel>("WIP_Get_GI_FromTo"
                        , queryParameters
                        , commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("GetWIB_GIFromToExcel")]
        public HttpResponseMessage GetWIB_GIFromToExcel(WIB_GIModel ob)
        {
            try
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "GIFromTo";
                Byte[] bytes = ExportDataTableToExcelFromStore(WIP_Get_GI_FromTo_Excel(ob), filename,true,false, "WIP_Get_GI_FromTo");
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        public DataTable WIP_Get_GI_FromTo_Excel(WIB_GIModel item)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
            SqlCommand cmd = new SqlCommand("WIP_Get_GI_FromTo", con);
            DateTime fromdate;
            fromdate = DateTime.ParseExact(item.FromDate, "MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
            DateTime todate;
            todate = DateTime.ParseExact(item.ToDate, "MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
            //if (fromdate> todate)
            //{
            //    throw new NotImplementedException("Please choose ToMonth bigger FromMonth");
            //}

            cmd.CommandTimeout = 1500;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromdate;
            cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = todate;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;
            cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
            cmd.Parameters.Add("@Payment", SqlDbType.NVarChar).Value = item.Payment;
            cmd.Parameters.Add("@ProdInstCDType", SqlDbType.NVarChar).Value= WIP_GetProdInstCDWithCompany();
            cmd.Parameters.Add("@RefVKNo", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.RefVKNo) || item.RefVKNo == "All" ? "" : item.RefVKNo.Trim();
            cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.MatCD) || item.MatCD == "All" ? "" : item.MatCD.Trim();
            cmd.Parameters.Add("@MatName", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.MatName) || item.MatName == "All" ? "" : item.MatName.Trim();
            cmd.Parameters.Add("@Costing", SqlDbType.Bit).Value = item.Costing;
            cmd.Parameters.Add("@IsWFG", SqlDbType.Bit).Value = item.IsWFG;
            cmd.Parameters.Add("@ClosedOnFGGR", SqlDbType.Bit).Value = item.ClosedOnFGGR;
            cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
            

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(table);
            }
            return table;
        }

    }
}

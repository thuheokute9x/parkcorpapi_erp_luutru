﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.Conmon
{
    [HR.API.App_Start.JwtAuthentication]
    public class CommonController : BaseController
    {
        [HttpGet]
        [Route("ERP_Common_GetID")]
        public async Task<String> ERP_Common_GetID()
        {
            string id = DateTime.Now.ToString("yyyyMMddHHmmss");
            return await Task.FromResult(id);
        }
        [HttpPost]
        [Route("ERP_CalAmount")]
        public async Task<Double> ERP_CalAmount(dynamic item)
        {
            Double dt= Math.Round(Double.Parse(item.Qty.Value.ToString())* Double.Parse(item.UnitPrice.Value.ToString()),2);
            return await Task.FromResult(dt);
        }
        [HttpGet]
        [Route("Get_Template")]
        public HttpResponseMessage Get_Template(string fileName)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string filePath = HttpContext.Current.Server.MapPath("~/Upload/Excel/TemplateExcel/") + fileName;
            if (!File.Exists(filePath))
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.ReasonPhrase = string.Format("File not found: {0} .", fileName);
                return response;
            }
            byte[] bytes = File.ReadAllBytes(filePath);        
            response.Content = new ByteArrayContent(bytes);
            response.Content.Headers.ContentLength = bytes.LongLength;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileName));
            return response;
        }
    }
}

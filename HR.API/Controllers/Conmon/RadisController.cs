﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TempModel.Development;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace HR.API.Controllers.Conmon
{
    public class RadisController : BaseController
    {
        [HttpGet]
        [Route("LoadAllSelect")]
        public void LoadAllSelect()
        {
            try
            {
                Parallel.Invoke(GetAllProdCodeSelect, GetAllMatCDSelect);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        [Route("LoadAllKeyRedis")]
        public IList<Object> LoadAllKeyRedis()
        {

            using (ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost:6379,allowAdmin=true"))
            {
                IDatabase db = redis.GetDatabase();

                var keys = redis.GetServer("localhost", 6379).Keys();

                string[] keysArr = keys.Select(key => (string)key).ToArray();

                return keysArr;
            }
        }
        [HttpGet]
        [Route("DeleteAllKeyRedis")]
        public IList<Object> DeleteAllKeyRedis()
        {
            using (ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost:6379,allowAdmin=true"))
            {
                IDatabase db = redis.GetDatabase();
                var keys = redis.GetServer("localhost", 6379).Keys();
                string[] keysArr = keys.Select(key => (string)key).ToArray();
                foreach (string key in keysArr)
                {
                    db.KeyDelete(Settings.GetConnectionString("Company") + key);
                }
                return keysArr;
            }
        }
        [HttpGet]
        [Route("DeleteAllSelect")]
        public void DeleteAllSelect()
        {

            try
            {
                IDatabase db = ConnectRedis();
                db.KeyDelete(Settings.GetConnectionString("Company")+ "TProdSelect_GetAll");
                db.KeyDelete(Settings.GetConnectionString("Company") + "TMatSelect_GetAll");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("DeleteRedisByKey")]
        public void DeleteRedisByKey(string key)
        {
            try
            {
                IDatabase db = ConnectRedis();
                db.KeyDelete(key);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetAllProdCodeSelect")]
        public void GetAllProdCodeSelect()//Sài bên ERP_TProd_GetwithProdCodeLike
        {
            IDatabase db = ConnectRedis();
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(Settings.GetConnectionString("Company"))))
                {

                    string Query = "SELECT ProdCode,ProdDesc FROM dbo.TProd (NOLOCK)";
                    var List = new DynamicParameters();
                    connection.Open();
                    var data = connection.Query<TProd>(Query, List, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    string value = JsonConvert.SerializeObject(data);
                    db.StringSet(Settings.GetConnectionString("Company")+ "TProdSelect_GetAll", value);

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }

        [HttpGet]
        [Route("GetAllMatCDSelect")]
        public void GetAllMatCDSelect()//ERP_TMat_GetwithMatCDLike sài bên
        {
            IDatabase db = ConnectRedis();
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(Settings.GetConnectionString("Company"))))
                {

                    string Query = "SELECT MatCD,CASE WHEN Spec1 IS NULL AND Spec2 IS NOT NULL THEN MatName+ ' '+ Spec2 WHEN Spec1 IS NOT NULL AND Spec2 IS NULL  THEN MatName+ ' '+ Spec1 WHEN Spec1 IS NOT NULL AND Spec2 IS NOT NULL  THEN MatName+ ' '+ Spec1 + ' '+ Spec2 ELSE MatName END MatDesc  FROM dbo.TMat (NOLOCK)";
                    var List = new DynamicParameters();
                    connection.Open();
                    var data = connection.Query<TMat_Basic>(Query, List, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    string value = JsonConvert.SerializeObject(data);
                    db.StringSet(Settings.GetConnectionString("Company") + "TMatSelect_GetAll", value);

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        public IDatabase ConnectRedis()
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost");
            return redis.GetDatabase();
        }
    }
}

﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.HaiQuan.CDI;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.HaiQuan
{
    [HR.API.App_Start.JwtAuthentication]
    public class CDIController : BaseController
    {
        private GenericRepository<TTCDI> repository;
        private ICDI_Repository tTCDI_Repository;
        private GenericRepository<TTCDIIV> repositoryTTCDIIV;
        private GenericRepository<TTCDIIVM> repositoryTTCDIIVM;
        public CDIController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TTCDI>(unitOfWork);
            tTCDI_Repository = new CDI_Repository(unitOfWork);
            repositoryTTCDIIV = new GenericRepository<TTCDIIV>(unitOfWork);
            repositoryTTCDIIVM = new GenericRepository<TTCDIIVM>(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TTCDI_Insert")]
        public async Task<Boolean> ERP_TTCDI_Insert(TTCDI item)
        {
            Initialization();
            return await repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TTCDIIV_Insert")]
        public async Task<Boolean> ERP_TTCDIIV_Insert(TTCDIIV item)
        {
            Initialization();
            return await repositoryTTCDIIV.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TTCDIIVM_Insert")]
        public async Task<bool> ERP_TTCDIIVM_Insert(TTCDIIVM item)
        {
            Initialization();
            return await repositoryTTCDIIVM.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TTCDIIVM_InsertList")]
        public async Task<IList<TTCDIIVM>> ERP_TTCDIIVM_InsertList(List<TTCDIIVM> item)
        {
            Initialization();
            return await repositoryTTCDIIVM.BulkInsert(item);
        }
        [HttpPost]
        [Route("ERP_TTCDI_GetCDICDLike")]
        public async Task<IList<TTCDI>> ERP_TTCDI_GetCDICDLike(TTCDI item)
        {
            Initialization();
            return await tTCDI_Repository.ERP_TTCDI_GetCDICDLike(item);
        }
        [HttpPost]
        [Route("ERP_TTCDI_GetCDNoLike")]
        public async Task<IList<TTCDI>> ERP_TTCDI_GetCDNoLike(TTCDI item)
        {
            Initialization();
            return await tTCDI_Repository.ERP_TTCDI_GetCDNoLike(item);
        }
        [HttpPost]
        [Route("ERP_TTCDI_UpDate")]
        public async Task<Boolean> ERP_TTCDI_UpDate(TTCDI item)
        {
            Initialization();
            return await repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TTCDI_GetPageSize")]
        public async Task<IList<Object>> ERP_TTCDI_GetPageSize(TTCDI_Custom item)
        {
            Initialization();
            return await tTCDI_Repository.ERP_TTCDI_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TTCDIIV_GetByCDICD")]
        public async Task<IList<Object>> ERP_TTCDIIV_GetByCDICD(TTCDIIV item)
        {
            Initialization();
            return await tTCDI_Repository.ERP_TTCDIIV_GetByCDICD(item);
        }
        [HttpPost]
        [Route("ERP_TTCDIIVM_InsertFromPO")]
        public async Task<IList<Object>> ERP_TTCDIIVM_InsertFromPO(TMatPOItem_Custom item)
        {
            Initialization();
            return await tTCDI_Repository.ERP_TTCDIIVM_InsertFromPO(item);
        }
        [HttpPost]
        [Route("ERP_TTCDIIVM_InsertFromGR")]
        public async Task<IList<Object>> ERP_TTCDIIVM_InsertFromGR(dynamic item)
        {
            Initialization();
            return await tTCDI_Repository.ERP_TTCDIIVM_InsertFromGR(item);
        }
        [HttpPost]
        [Route("ERP_TTCDIIVM_GetPageSize")]
        public async Task<IList<Object>> ERP_TTCDIIVM_GetPageSize(dynamic item)
        {
            Initialization();
            return await tTCDI_Repository.ERP_TTCDIIVM_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TTCDIIVM_GetPageSizeExcel")]
        public HttpResponseMessage ERP_TTCDIIVM_GetPageSizeExcel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "CDI" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(tTCDI_Repository.ERP_TTCDIIVM_GetPageSizeExcel(item), "CDI", true, false);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TTCDCustomsCD_GetByCustomsCDLike")]
        public async Task<IList<TTCDCustomsCD>> ERP_TTCDCustomsCD_GetByCustomsCDLike(TTCDCustomsCD item)
        {
            Initialization();
            return await tTCDI_Repository.ERP_TTCDCustomsCD_GetByCustomsCDLike(item);
        }
        [HttpPost]
        [Route("ERP_TTCDCustomsCD_GetByCustomsCD")]
        public async Task<TTCDCustomsCD> ERP_TTCDCustomsCD_GetByCustomsCD(TTCDCustomsCD item)
        {
            Initialization();
            return await tTCDI_Repository.ERP_TTCDCustomsCD_GetByCustomsCD(item);
        }
        [HttpPost]
        [Route("ERP_TTCDIIV_GetByIVNoLike")]
        public async Task<IList<TTCDIIV>> ERP_TTCDIIV_GetByIVNoLike(TTCDIIV item)
        {
            Initialization();
            return await tTCDI_Repository.ERP_TTCDIIV_GetByIVNoLike(item);
        }
        [HttpPost]
        [Route("ERP_TTCDIIVM_Report")]
        public async Task<IList<Object>> ERP_TTCDIIVM_Report(dynamic item)
        {
            Initialization();
            return await tTCDI_Repository.ERP_TTCDIIVM_Report(item);
        }
        [HttpPost]
        [Route("ERP_TTCDIIVM_Report_Excel")]
        public HttpResponseMessage ERP_TTCDIIVM_ReportExcel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "CDI" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(tTCDI_Repository.ERP_TTCDIIVM_Report_Excel(item), "CDIReport", true, false);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

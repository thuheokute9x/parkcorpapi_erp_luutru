﻿using HR.API.Models.ERP.TableAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;



namespace HR.API.Controllers.ERP.TableAccess
{
    [HR.API.App_Start.JwtAuthentication]
    public class ZRptController : BaseController
    {
        [HttpGet]
        [Route("ERP_ZRpt_GetAll")]
        public IList<ZRpt> ERP_ZRpt_GetAll()
        {
            try
            {
                List<ZRpt> data = new List<ZRpt>()
                { new ZRpt { RptID=1,Seq=1101,RptCat="MatPO",RptDiv="PO Sheet",Flag="", RptName = "RMatPO_ENG  PO Sheet / English / Header-Shipper, Item-MFG", OpenAsNewObj = true,Avalia="*" },
                  new ZRpt { RptID=2,Seq=1103,RptCat="MatPO",RptDiv="PO Sheet",Flag="MKR",RptName = "RMatPO_KOR_DivMFG   PO Sheet / Korean / Header-MFG, Item-None / Separated by MFG",OpenAsNewObj =true,Avalia=""},
                  new ZRpt { RptID=3,Seq=1102,RptCat="MatPO",RptDiv="PO Sheet",Flag="MKR",RptName = "RMatPO_KOR  PO Sheet / Korean / Header-MFG, Item_None",OpenAsNewObj=true,Avalia=""},
                  new ZRpt { RptID=null,Seq=null,RptCat=null,RptDiv=null,Flag=null,RptName = null,OpenAsNewObj=null,Avalia=""},
                };
                return  data.ToList();
                
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

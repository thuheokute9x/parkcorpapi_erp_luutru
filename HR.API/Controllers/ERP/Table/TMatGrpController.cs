﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.MatGrp;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TMatGrpController : BaseController
    {
        private GenericRepository<TMatGrp> repository;
        private ITMatGrp_Repository iTMatGrp_Repository;
        public TMatGrpController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TMatGrp>(unitOfWork);
            iTMatGrp_Repository = new TMatGrp_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TMatGrp_GetPageSize")]
        public async Task<IList<TMatGrp_Custom>> ERP_TMatGrp_GetPageSize(TMatGrp_Custom item)
        {
            Initialization();
            return await iTMatGrp_Repository.ERP_TMatGrp_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TMatGrp_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TMatGrp_GetPageSize_Excel(TMatGrp_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "TMatGrp";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTMatGrp_Repository.ERP_TMatGrp_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TMatGrp_ByMatGrpCode")]
        public async Task<IList<TMatGrp_Custom>> ERP_TMatGrp_ByMatGrpCode(TMatGrp_Custom item)
        {
            Initialization();
            return await iTMatGrp_Repository.ERP_TMatGrp_ByMatGrpCode(item);
        }
        [HttpGet]
        [Route("ERP_TMatGrp_ByMatGrpID")]
        public async Task<TMatGrp_Custom> ERP_TMatGrp_ByMatGrpID(int MatGrpID)
        {
            Initialization();
            return await iTMatGrp_Repository.ERP_TMatGrp_ByMatGrpID(MatGrpID);
        }
        [HttpPost]
        [Route("ERP_TMatGrp_Update")]
        public async Task<Boolean> ERP_TMatShape_Update(TMatGrp item)
        {
            Initialization();
            return await iTMatGrp_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TMatGrp_Insert")]
        public async Task<Boolean> ERP_TMatShape_Insert(TMatGrp item)
        {
            Initialization();
            return await iTMatGrp_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TMatGrp_Count_DMatMat")]
        public async Task<IList<TMatGrp_Custom>> ERP_TMatGrp_Count_DMatMat(TMatGrp item)
        {
            Initialization();
            return await iTMatGrp_Repository.ERP_TMatGrp_Count_DMatMat(item);
        }
        [HttpPost]
        [Route("ERP_TMatGrp_Tranfer")]
        public async Task<IList<TMatGrp_Custom>> ERP_TMatGrp_Tranfer(TMatGrp item)
        {
            Initialization();
            return await iTMatGrp_Repository.ERP_TMatGrp_Tranfer(item);
        }
        //[HttpPost]
        //[Route("ERP_TMatGrp_Select")]
        //public async Task<TMatGrp_Custom> ERP_TMatGrp_Select(TMatGrp_Custom item)
        //{
        //    return await iTMatGrp_Repository.Insert_SaveChange(item);
        //}

    }
}

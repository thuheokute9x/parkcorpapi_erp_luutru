﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.SPStatus;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TSPStatusController : BaseController
    {
        private GenericRepository<TSPStatu> repository;
        private ITSPStatus_Repository iTSPStatus_Repository;
        public TSPStatusController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TSPStatu>(unitOfWork);
            iTSPStatus_Repository = new TSPStatus_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TSPStatus_GetAll")]
        public async Task<IList<TSPStatu_Custom>> ERP_TSPStatu_GetAll()
        {
            Initialization();
            return await iTSPStatus_Repository.ERP_TSPStatu_GetAll();
        }
        [HttpPost]
        [Route("ERP_TSPStatus_Update")]
        public async Task<Boolean> ERP_TVendor_Update(TSPStatu item)
        {
            Initialization();
            item.UpdatedOn = DateTime.Now;
            return await iTSPStatus_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TSPStatus_Insert")]
        public async Task<Boolean> ERP_TVendor_Insert(TSPStatu item)
        {
            Initialization();
            item.CreatedOn = DateTime.Now;
            return await iTSPStatus_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TSPStatus_Delete")]
        public async Task<Boolean> ERP_TSPStatus_Delete(TSPStatu item)
        {
            Initialization();
            item.CreatedOn = DateTime.Now;
            return await iTSPStatus_Repository.Delete_SaveChange(item);
        }
    }
}

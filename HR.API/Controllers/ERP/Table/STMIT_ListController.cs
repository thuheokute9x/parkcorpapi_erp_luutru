﻿using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.ERP.MIT_List;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STMIT_ListController : BaseController
    {
        private GenericRepository<STMIT_List> repository;
        private ISTMIT_List_Repository iSTMIT_List_Repository;
        public STMIT_ListController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STMIT_List>(unitOfWork);
            iSTMIT_List_Repository = new STMIT_List_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_STMIT_List_SFMIT_GetPageSize")]
        public async Task<IList<Object>> ERP_STMIT_List_SFMIT_GetPageSize(dynamic item)
        {
            Initialization();
            return await iSTMIT_List_Repository.ERP_STMIT_List_SFMIT_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_STMIT_List_SFMIT_GetPageSize_Excel")]
        public HttpResponseMessage ERP_STMIT_List_SFMIT_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "SFMIT" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iSTMIT_List_Repository.ERP_STMIT_List_SFMIT_GetPageSize_Excel(item), "SFMIT", false, false);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_STMIT_List_GetByMatPOItemID")]
        public async Task<IList<Object>> ERP_STMIT_List_GetByMatPOItemID(STMIT_List item)
        {
            Initialization();
            return await iSTMIT_List_Repository.ERP_STMIT_List_GetByMatPOItemID(item);
        }
        [HttpPost]
        [Route("ERP_STMIStatus_GetAll")]
        public async Task<IList<STMIStatu>> ERP_STMIStatus_GetAll(dynamic item)
        {
            Initialization();
            return await iSTMIT_List_Repository.ERP_STMIStatus_GetAll(item);
        }
        [HttpPost]
        [Route("ERP_STMIT_List_InsertReturnExpand")]
        public async Task<IList<Object>> ERP_STMIT_List_InsertReturnExpand(STMIT_List item)
        {
            Initialization();
            return await iSTMIT_List_Repository.ERP_STMIT_List_InsertReturnExpand(item);
        }
        [HttpPost]
        [Route("ERP_STMIT_List_UpdateReturnExpand")]
        public async Task<IList<Object>> ERP_STMIT_List_UpdateReturnExpand(STMIT_List item)
        {
            Initialization();
            return await iSTMIT_List_Repository.ERP_STMIT_List_UpdateReturnExpand(item);
        }
    }
}

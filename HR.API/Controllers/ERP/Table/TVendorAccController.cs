﻿using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.VendorAcc;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TVendorAccController : BaseController
    {
        private GenericRepository<TVendorAcc> repository;
        private ITVendorAcc_Repository iTVendorAcc_Repository;
        public TVendorAccController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TVendorAcc>(unitOfWork);
            iTVendorAcc_Repository = new TVendorAcc_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TVendorAcc_Insert")]
        public async Task<Boolean> ERP_TVendor_Insert(TVendorAcc item)
        {
            Initialization();
            item.CreatedOn = DateTime.Now;
            return await iTVendorAcc_Repository.Insert_SaveChange(item);
        }
        [HttpGet]
        [Route("ERP_TVendorAcc_GetByVendor")]
        public async Task<IList<TVendorAcc>> ERP_TVendorAcc_GetByVendor(string VendorCD)
        {
            Initialization();
            return await iTVendorAcc_Repository.ERP_TVendorAcc_GetByVendor(VendorCD);
        }
        [HttpPost]
        [Route("ERP_TVendorAcc_Delete")]
        public async Task<Boolean> ERP_TVendor_Delete(TVendorAcc item)
        {
            Initialization();
            return await iTVendorAcc_Repository.Delete_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TVendorAcc_Update")]
        public async Task<Boolean> ERP_TVendor_Update(TVendorAcc item)
        {
            Initialization();
            item.UpdatedOn = DateTime.Now;
            return await iTVendorAcc_Repository.Update_SaveChange(item);
        }
        
    }
}

﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.PayReq;
using HR.API.UnitOfWork;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STPayReqController : BaseController
    {
        private GenericRepository<STPayReq> repository;
        private IPayReq_Repository payReq_Repository;
        public STPayReqController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STPayReq>(unitOfWork);
            payReq_Repository = new PayReq_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_PayReq_GetPayReqID")]
        public async Task<IList<STPayReq>> ERP_PayReq_GetPayReqID(string PayReqID)
        {
            Initialization();
            return await payReq_Repository.ERP_STPayReq_GetPayReqID(PayReqID);
        }
        [HttpPost]
        [Route("ERP_STPayReq_Rgl_GetPageSize")]
        public async Task<IList<STPayReq_Custom>> ERP_STPayReq_Rgl_GetPageSize(STPayReq_Custom item)
        {
            Initialization();
            return await payReq_Repository.ERP_STPayReq_Rgl_GetPageSize(item);
        }
    }
}

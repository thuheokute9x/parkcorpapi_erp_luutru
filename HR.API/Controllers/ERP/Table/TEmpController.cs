﻿
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.Emp;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TEmpController : BaseController
    {
        private GenericRepository<TEmp> repository;
        private ITEmpRepository tEmpRepository;
        public TEmpController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TEmp>(unitOfWork);
            tEmpRepository = new TEmpRepository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TEmp_GetWithStaffID")]
        public async Task<IList<TEmp_Custom>> ERP_TEmp_GetWithStaffID()
        {
            Initialization();
            return await tEmpRepository.ERP_TEmp_GetWithStaffID();
        }
        [HttpGet]
        [Route("ERP_TEmp_GetAll")]
        public async Task<IList<TEmp>> ERP_TEmp_GetAll()
        {
            Initialization();
            return await tEmpRepository.ERP_TEmp_GetAll();
        }
        [HttpGet]
        [Route("ERP_TEmp_GetEmpID_EmpName_EmpNo")]
        public async Task<IList<TEmp>> ERP_TEmp_GetEmpID_EmpName_EmpNo()
        {
            Initialization();
            return await tEmpRepository.ERP_TEmp_GetEmpID_EmpName_EmpNo();
        }
        [HttpPost]
        [Route("ERP_TEmp_GetByEmpName_EmpID")]
        public async Task<IList<TEmp_Custom>> ERP_TEmp_GetByEmpName_EmpNo(TEmp_Custom item)
        {
            Initialization();
            return await tEmpRepository.ERP_TEmp_GetByEmpName_EmpID(item);
        }
        [HttpPost]
        [Route("ERP_TEmp_Insert")]
        public async Task<Boolean> ERP_TEmp_Insert(TEmp item)
        {
            Initialization();
            return await tEmpRepository.ERP_TEmp_Insert(item);
        }
        [HttpPost]
        [Route("ERP_TEmp_Update")]
        public async Task<Boolean> ERP_TEmp_Update(TEmp item)
        {
            Initialization();
            return await tEmpRepository.ERP_TEmp_Update(item);
        }
    }
}

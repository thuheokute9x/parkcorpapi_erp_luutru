﻿using Dapper;

using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.PayDoc;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STPayDoc_ListController : BaseController
    {
        private GenericRepository<STPayDoc_List> repository;
        private ISTPayDoc_ListRepository sTPayDocListRepository;
        private GenericRepository<STPayDoc> repositorySTPayDoc;
        private ISTPayDocRepository sTPayDocRepository;

        public STPayDoc_ListController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STPayDoc_List>(unitOfWork);
            sTPayDocListRepository = new STPayDoc_ListRepository(unitOfWork);
            repositorySTPayDoc = new GenericRepository<STPayDoc>(unitOfWork);
            sTPayDocRepository = new STPayDocRepository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_STPayDoc_List_IsExistPayDocCD")]
        public async Task<int> ERP_STPayDoc_List_IsExistPayDocCD(STPayDoc_List_Custom item)
        {
            Initialization();
            return await sTPayDocListRepository.ERP_STPayDoc_List_IsExistPayDocCD(item);
        }
        [HttpPost]
        [Route("ERP_STPayDoc_List_InsertList")]
        public async Task<STPayDoc> ERP_STPayDoc_List_InsertListAsync(List<STPayDoc_List_Custom> item)
        {

            try
            {
                Initialization();
                STPayDoc ob = new STPayDoc();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {

                    connection.Open();
                    var queryParameters = new DynamicParameters();
                    string Query = "";
                    if (item[0].PayDocCD=="DN" || item[0].PayDocCD == "CN" || item[0].PayDocCD == "OT")//Check thêm PayDocCD
                    {
                         Query = "SELECT a.PayDocID FROM dbo.STPayDoc a (NOLOCK) WHERE a.RglPayReqID='" + item[0].RglPayReqID + "' AND a.PayDocNo='" + item[0].PayDocNo + "' AND  a.PayDocCD='" + item[0].PayDocCD + "'";
                    }
                    if (item[0].PayDocCD != "DN" && item[0].PayDocCD != "CN" && item[0].PayDocCD != "OT")
                    {
                        Query = "SELECT PayDocID FROM dbo.STPayDoc (NOLOCK) WHERE RglPayReqID='" + item[0].RglPayReqID + "'" +
                            " AND PayDocNo='" + item[0].PayDocNo + "' AND PayDocCD='" + item[0].PayDocCD_Parent + "'";
                    }
                    //"SELECT PayDocID FROM dbo.STPayDoc (NOLOCK) WHERE RglPayReqID='" + item[0].RglPayReqID + "' AND PayDocNo='" + item[0].PayDocNo + "' "
                    var checkSTPayDoc = connection.Query<STPayDoc>(Query
                    , queryParameters, commandTimeout: 1500
                    , commandType: CommandType.Text);
                    if (checkSTPayDoc.Any())//Co ton tai
                    {
                        ob = checkSTPayDoc.FirstOrDefault();

                        var queryParameterCheck = new DynamicParameters();
                        var checkSTPayDoc_List = connection.Query<STPayDoc_List>("SELECT PayDocListID FROM dbo.STPayDoc_List (NOLOCK) WHERE PayDocID="+item[0].PayDocID+" AND PayDocCD='"+ item[0].PayDocCD + "'"
                        , queryParameterCheck, commandTimeout: 1500
                        , commandType: CommandType.Text);
                        if (checkSTPayDoc_List.Any())
                        {
                            throw new NotImplementedException("PayDocCD " + item[0].PayDocCD + " is exist in PayDocNo " + item[0].PayDocNo + "");
                        }
                        List<STPayDoc_List> Insert_List = new List<STPayDoc_List>();
                        foreach (STPayDoc_List_Custom a in item)
                        {
                            STPayDoc_List ob_List = new STPayDoc_List();
                            ob_List.FilePath = a.FilePath;
                            ob_List.Merge = a.Merge;
                            ob_List.PayDocID = ob.PayDocID;
                            ob_List.PayDocCD = a.PayDocCD;
                            Insert_List.Add(ob_List);
                        }
                        await sTPayDocListRepository.ERP_STPayDoc_List_InsertList(Insert_List);
                        return ob;

                    }
                    if (!checkSTPayDoc.Any())//ko ton tai
                    {

                        ob.RglPayReqID = item[0].RglPayReqID;
                        ob.PayDocVendorCD = item[0].PayDocVendorCD;
                        ob.PayDocCD = item[0].PayDocCD_Parent;
                        ob.PayDocNo = item[0].PayDocNo;
                        ob.PayType = item[0].PayType;
                        ob = await sTPayDocRepository.Insert_ReturnOBJ(ob);


                        List<STPayDoc_List> Insert_List = new List<STPayDoc_List>();
                        foreach (STPayDoc_List_Custom a in item)
                        {
                            STPayDoc_List children = new STPayDoc_List();
                            children.FilePath = a.FilePath;
                            children.Merge = a.Merge;
                            children.PayDocID = ob.PayDocID;
                            children.PayDocCD = a.PayDocCD;
                            Insert_List.Add(children);
                        }

                        await sTPayDocListRepository.ERP_STPayDoc_List_InsertList(Insert_List);
                        return ob;

                    }
                }
                return ob;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }

        }
        [HttpPost]
        [Route("ERP_STPayDoc_List_Get")]
        public async Task<IList<STPayDoc_List_Custom>> ERP_STPayDoc_List_Get(STPayDoc_List_Custom item)
        {
            Initialization();
            return await sTPayDocListRepository.ERP_STPayDoc_List_Get(item);
        }
        [HttpPost]
        [Route("ERP_STPayDoc_List_Delete")]
        public async Task<Boolean> ERP_STPayDoc_List_Delete(STPayDoc_List STPayDoc_List)
        {
            Initialization();
            return await sTPayDocListRepository.ERP_STPayDoc_Delete(STPayDoc_List);
        }
        [HttpPost]
        [Route("ERP_STPayDoc_List_CheckPatchExist")]
        public async Task<Boolean> ERP_STPayDoc_List_CheckPatchExist(STPayDoc_List_Custom item)
        {
            Initialization();
            return await sTPayDocListRepository.ERP_STPayDoc_List_CheckPatchExist(item);
        }
        [HttpPost]
        [Route("ERP_STPayDoc_List_UpdateMerge")]
        public async Task<Boolean> ERP_STPayDoc_List_UpdateMerge(STPayDoc_List_Custom item)
        {
            Initialization();
            return await sTPayDocListRepository.ERP_STPayDoc_List_UpdateMerge(item);
        }
    }
}

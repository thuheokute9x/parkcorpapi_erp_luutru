﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.Style;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TStyleController : BaseController
    {
        private GenericRepository<TStyle> repository;
        private ITStyle_Repository iTStyle_Repository;
        public TStyleController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TStyle>(unitOfWork);
            iTStyle_Repository = new TStyle_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TStyle_GetPageSize")]
        public async Task<IList<TStyle_Custom>> ERP_TStyle_GetPageSize(TStyle_Custom item)
        {
            Initialization();
            return await iTStyle_Repository.ERP_TStyle_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TStyle_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TStyle_GetPageSize_Excel(TStyle_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "TStyle";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTStyle_Repository.ERP_TStyle_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TStyle_GetStyleDesc")]
        public async Task<IList<TStyle>> ERP_TStyle_GetStyleDesc(TStyle item)
        {
            Initialization();
            return await iTStyle_Repository.ERP_TStyle_GetStyleDesc(item);
        }
        [HttpPost]
        [Route("ERP_TStyle_Insert")]
        public async Task<Boolean> ERP_TStyle_Insert(TStyle item)
        {
            Initialization();
            return await iTStyle_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TStyle_Update")]
        public async Task<Boolean> ERP_TRange_Update(TStyle item)
        {
            Initialization();
            return await iTStyle_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TStyle_Delete")]
        public async Task<Boolean> ERP_TRange_Delete(TStyle item)
        {
            Initialization();
            return await iTStyle_Repository.Delete_SaveChange(item);
        }
    }
}

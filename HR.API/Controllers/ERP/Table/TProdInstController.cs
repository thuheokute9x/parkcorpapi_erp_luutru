﻿using Dapper;
using ExcelLibrary.SpreadSheet;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models.WIB.StoreViewModel.GI;
using HR.API.Repository;
using HR.API.Repository.ERP.ProdInst;
using HR.API.UnitOfWork;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TProdInstController : BaseController
    {
        private GenericRepository<TProdInst> repository;
        private ITProdInst_Repository iTProdInst_Repository;
        public TProdInstController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TProdInst>(unitOfWork);
            iTProdInst_Repository = new TProdInst_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TProdInst_GetClosedOnFGGR")]
        public async Task<IList<TProdInst_Custom>> ERP_TProdInst_GetClosedOnFGGR(TProdInst_Custom item)
        {
            Initialization();
            return await iTProdInst_Repository.ERP_TProdInst_GetClosedOnFGGR(item);
        }

        [HttpPost]
        [Route("ERP_TProdInst_GetWorkingFGGR")]
        public async Task<IList<TProdInst_Custom>> ERP_TProdInst_GetWorkingFGGR(TProdInst_Custom item)
        {
            Initialization();
            return await iTProdInst_Repository.ERP_TProdInst_GetWorkingFGGR(item);
        }

        [HttpPost]
        [Route("ERP_TProdInst_UpdateClosedOnFGGR")]
        public async Task<Boolean> ERP_TProdInst_UpdateClosedOnFGGR(TProdInst_Custom item)
        {
            Initialization();
            return await iTProdInst_Repository.ERP_TProdInst_UpdateClosedOnFGGR(item);
        }
        [HttpPost]
        [Route("ERP_TProdInst_GetClosedOnFGGR_Excel")]
        public HttpResponseMessage ERP_TProdInst_GetClosedOnFGGR_Excel(TProdInst_Custom item)
        {
            Initialization();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string filename = "ClosedOnFGGR";
            Byte[] bytes = ExportDataTableToExcelFromStore(iTProdInst_Repository.ERP_TProdInst_GetClosedOnFGGR_Excel(item), "ClosedOnFGGR");
            response.Content = new ByteArrayContent(bytes);
            response.Content.Headers.ContentLength = bytes.LongLength;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = filename;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
            GC.SuppressFinalize(this);
            return response;
        }
        [HttpPost]
        [Route("ERP_TProdInst_GetWorkingFGGR_Excel")]
        public HttpResponseMessage ERP_TProdInst_GetWorkingFGGR_Excel(TProdInst_Custom item)
        {
            Initialization();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string filename = "WorkingFGGR";
            Byte[] bytes = ExportDataTableToExcelFromStore(iTProdInst_Repository.ERP_TProdInst_GetWorkingFGGR_Excel(item), "WorkingFGGR");
            response.Content = new ByteArrayContent(bytes);
            response.Content.Headers.ContentLength = bytes.LongLength;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = filename;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
            GC.SuppressFinalize(this);
            return response;
        }
        [HttpPost]
        [Route("ERP_TProdInst_Import_UpdateClosedOnFGGR")]
        public async Task<Data> ERP_TProdInst_Import_UpdateClosedOnFGGR()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            Data kq = new Data();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                string Path = "";
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        Path = "~/Upload/Excel/WIP" + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath(Path);
                        postedFile.SaveAs(filePath);
                    }
                    FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));
                    //List<TProdInst> TProdInstList = db.TProdInsts.Where(x => x.Discontinue == false).ToList();
                    //List<TProd> TProdsList = db.TProds.Where(x => x.Discontinue == false).ToList();
                    List<ProdInstItemViewModel> ProdInstItemlist;
                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    {
                        var queryParameters = new DynamicParameters();
                        connection.Open();
                        string Query = "SELECT a.ProdInstCD,b.ProdCode,SUM(c.Qty) Qty,d.ProdPriceID  FROM TProdInstItem a (NOLOCK) INNER JOIN dbo.TProd b(NOLOCK) ON a.ProdID = b.ProdID AND b.Discontinue = 0 LEFT JOIN dbo.TSalseInfo c(NOLOCK) ON a.ProdInstItemID = c.ProdInstItemID LEFT JOIN dbo.TTProdPrice d(NOLOCK) ON a.ProdInstCD = d.ProdInstCD AND b.ProdCode=d.ProdCode GROUP BY a.ProdInstCD,b.ProdCode,d.ProdPriceID";
                        ProdInstItemlist = connection.Query<ProdInstItemViewModel>(Query
                            , queryParameters
                            , commandTimeout: 1500
                            , commandType: CommandType.Text)
                            .ToList();
                        if (ProdInstItemlist == null)
                        {
                            ProdInstItemlist = new List<ProdInstItemViewModel>();
                        }
                    }

                    if (existingFile.Extension.ToString() == ".xls")
                    {
                        return await importXLS(Path, ProdInstItemlist);
                    }

                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        ExcelWorkbook workBook = package.Workbook;
                        if (workBook != null)
                        {
                            if (workBook.Worksheets.Count > 0)
                            {
                                ExcelWorksheet worksheet = workBook.Worksheets.FirstOrDefault();
                                //ExcelWorksheet worksheet = workBook.Worksheets.Where(x => x.Name.Contains("ProdPrice")).FirstOrDefault();
                                //if (worksheet == null)
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SheetName ProdPrice in file excel" });
                                //    return resultList;
                                //}
                                int rowCount = worksheet.Dimension.End.Row;

                                List<TProdInst> ListInsert = new List<TProdInst>();
                                List<int> TTProdPriceDelete = new List<int>();//Nếu đã tồn tại thì xóa những thằng này
                                #region Check file exel no header name


                                //if (worksheet.Cells[1, 1].Value == null)
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                //    return resultList;
                                //}
                                //if (worksheet.Cells[1, 1].Value.ToString().Trim() != "Prod Order")
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
                                //    return resultList;
                                //}


                                #endregion
                                for (int row = 2; row <= rowCount; row++)
                                {

                                    if (worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null)
                                    {
                                        continue;
                                    }
                                    string RefVKNo = worksheet.Cells[row, 1].Value.ToString().Trim();
                                    double d = double.Parse(worksheet.Cells[row, 2].Value.ToString());
                                    DateTime ClosedOnFGGR = DateTime.FromOADate(d).Date;
                                    List<ProdInstItemViewModel> ProdInstItem = ProdInstItemlist.Where(x => x.ProdInstCD.Contains(RefVKNo)).ToList();
                                    if (ProdInstItem.Count() == 0)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdOrder " + RefVKNo + " in Parkerp" });
                                        continue;
                                    }
                                    ListInsert.Add(new TProdInst { ProdInstCD = RefVKNo, ClosedOnFGGR = ClosedOnFGGR });
                                }
                                if (ListInsert.Count == 0)
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File import no data" });
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;

                                }
                                if (resultList.Count > 0)
                                {
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;

                                }
                                List<TProdInst> TProdInstItem_Custom = new List<TProdInst>();
                                foreach (TProdInst ob in ListInsert)
                                {
                                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                    {
                                        var queryParameters = new DynamicParameters();
                                        connection.Open();
                                        string Query = "UPDATE TProdInst SET ClosedOnFGGR='" + ob.ClosedOnFGGR.Value.ToString("yyyy-MM-dd") + "',UpdatedBy='" + GetUserlogin() + "',UpdatedOn=GETDATE() WHERE ProdInstCD='" + ob.ProdInstCD + "'";
                                        var data = await connection.QueryAsync<TProdInst>(Query
                                            , queryParameters, commandTimeout: 1500
                                            , commandType: CommandType.Text);
                                        connection.Close();
                                    }
                                }

                                // await repository.BulkInsert(ListInsert);
                                kq.Results = "";
                                kq.Type = 1;
                                return kq;

                            }
                        }

                    }
                    kq.Results = resultList;
                    kq.Type = 0;
                    return kq;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());

            }
            kq.Results = resultList;
            kq.Type = 0;
            return kq;
        }
        public async Task<Data> importXLS(string Path, List<ProdInstItemViewModel> ProdInstItemList)
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            Data kq = new Data();
            FileStream existingFile = new FileStream(HttpContext.Current.Server.MapPath(Path), FileMode.Open);
            var workbook = Workbook.Load(existingFile);
            var worksheet = workbook.Worksheets.FirstOrDefault();
            //var worksheet = workbook.Worksheets.Where(x => x.Name.Contains("ProdPrice")).FirstOrDefault();
            //if (worksheet == null)
            //{
            //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SheetName ProdPrice in file excel" });
            //    return resultList;
            //}
            var cells = worksheet.Cells;
            #region Check file exel no header name


            //if (worksheet.Cells[0, 0].Value == null)
            //{
            //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
            //    return resultList;
            //}
            //if (worksheet.Cells[0, 0].Value.ToString().Trim() != "Prod Order")
            //{
            //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File excel no head name" });
            //    return resultList;
            //}


            #endregion
            List<TProdInst> ListInsert = new List<TProdInst>();

            for (int row = cells.FirstRowIndex + 1; row <= cells.LastRowIndex; row++)
            {

                if (worksheet.Cells[row, 0].Value == null || worksheet.Cells[row, 1].Value == null)
                {
                    continue;
                }
                string RefVKNo = worksheet.Cells[row, 0].Value.ToString().Trim();
                double d = double.Parse(worksheet.Cells[row, 1].Value.ToString());
                DateTime ClosedOnFGGR = DateTime.FromOADate(d).Date;
                List<ProdInstItemViewModel> ProdInstItem = ProdInstItemList.Where(x => x.ProdInstCD.Contains(RefVKNo)).ToList();
                if (ProdInstItem.Count() == 0)
                {
                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdOrder " + RefVKNo + " in Parkerp" });
                    continue;
                }
                ListInsert.Add(new TProdInst { ProdInstCD = RefVKNo, ClosedOnFGGR = ClosedOnFGGR });
            }
            if (ListInsert.Count == 0)
            {
                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File import no data" });
                kq.Results = resultList;
                kq.Type = 0;
                return kq;

            }
            if (resultList.Count > 0)
            {
                kq.Results = resultList;
                kq.Type = 0;
                return kq;

            }
            foreach (TProdInst ob in ListInsert)
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string Query = "UPDATE TProdInst SET ClosedOnFGGR='" + ob.ClosedOnFGGR.Value.ToString("yyyy-MM-dd") + "',UpdatedBy='" + GetUserlogin() + "',UpdatedOn=GETDATE() WHERE ProdInstCD='" + ob.ProdInstCD + "'";
                    var data = await connection.QueryAsync<TProdInst>(Query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    connection.Close();
                }
            }
            kq.Results = "";
            kq.Type = 1;
            return kq;
        }
        [HttpPost]
        [Route("ERP_TProdInst_GetVKwithYear")]
        public async Task<IList<TProdInst_Custom>> ERP_TProdInst_GetVKwithYear(TProdInst_Custom item)
        {
            Initialization();
            return await iTProdInst_Repository.ERP_TProdInst_GetVKwithYear(item);
        }
        [HttpGet]
        [Route("ERP_TProdInstCat_GetAll")]
        public async Task<IList<TProdInstCat>> ERP_TProdInstCat_GetAll()
        {
            Initialization();
            return await iTProdInst_Repository.ERP_TProdInstCat_GetAll();
        }
        [HttpGet]
        [Route("ERP_TCutWay_GetAll")]
        public async Task<IList<TCutWay>> ERP_TCutWay_GetAll()
        {
            Initialization();
            return await iTProdInst_Repository.ERP_TCutWay_GetAll();
        }
        [HttpPost]
        [Route("ERP_TProdInst_GetProductionInstructionPagesize")]//Lưu ý hàm ERP_TProdInst_Insert,ERP_TProdInst_Update có gọi
        public async Task<IList<TProdInst_Custom>> ERP_TProdInst_GetProductionInstructionPagesize(TProdInst_Custom item)
        {
            Initialization();
            return await iTProdInst_Repository.ERP_TProdInst_GetProductionInstructionPagesize(item);
        }
        [HttpPost]
        [Route("ERP_TProdInst_GetProductionInstructionPagesize_Excel")]
        public HttpResponseMessage ERP_TProdInst_GetProductionInstructionPagesize_Excel(TProdInst_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "ProductionInstruction_";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTProdInst_Repository.ERP_TProdInst_GetProductionInstructionPagesize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TProdInst_GenerateProdInstCD")]
        public async Task<TProdInst_Custom> ERP_TProdInst_GenerateProdInstCD(TProdInst_Custom item)
        {
            Initialization();
            return await iTProdInst_Repository.ERP_TProdInst_GenerateProdInstCD(item);
        }
        [HttpPost]
        [Route("ERP_TProdInst_Insert")]
        public async Task<IList<TProdInst_Custom>> ERP_TProdInst_Insert(TProdInst item)
        {
            Initialization();
            item.IDate =DateTime.Today;
            item=await repository.Insert_ReturnOBJ(item);
            return await this.ERP_TProdInst_GetProductionInstructionPagesize(new TProdInst_Custom() { NA=true, StandBy = true 
            ,InProcess = true,Pending = true,Completed = false,Discontinue = false,Close = false,ProdInstCD = ""
            ,ProdInstCatCD = "",InstStatusID = 0,CutWayCD = "",Note = "",Type = 0,PageNumber = 1,PageSize = 100
            });


        }
        [HttpPost]
        [Route("ERP_TProdInst_Update")]
        public async Task<Boolean> ERP_TProdInst_Update(TProdInst item)
        {
            Initialization();
            return await repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TProdInst_TPRProdInst_GetByProdInstCD")]
        public async Task<IList<Object>> ERP_TProdInst_TPRProdInst_GetByProdInstCD(TProdInst_Custom item)
        {
            Initialization();
            return await iTProdInst_Repository.ERP_TProdInst_TPRProdInst_GetByProdInstCD(item);
        }
    }
}

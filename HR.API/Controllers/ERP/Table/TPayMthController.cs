﻿using Dapper;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;



namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TPayMthController : BaseController
    {
        [HttpGet]
        [Route("ERP_TPayMth_GetAll")]
        public async Task<IList<TPayMth>> ERP_TPayMth_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT PayMthCD FROM TPayMth (NOLOCK) WHERE Discontinue=0";
                    var data = await connection.QueryAsync<TPayMth>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

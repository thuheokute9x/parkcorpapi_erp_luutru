﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.Curr;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;



namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TCurrController : BaseController
    {
        private GenericRepository<TCurr> repository;
        private ITCurr_Repository iTCurr_Repository;
        public TCurrController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TCurr>(unitOfWork);
            iTCurr_Repository = new TCurr_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TCurr_GetAll")]
        public async Task<IList<TCurr>> ERP_TCurr_GetAll()
        {
            Initialization();
            return await iTCurr_Repository.ERP_TCurr_GetAll(); 
        }
        [HttpPost]
        [Route("ERP_TCurr_Update")]
        public async Task<Boolean> ERP_TCurr_Update(TCurr item)
        {
            Initialization();
            return await iTCurr_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TCurr_Insert")]
        public async Task<Boolean> ERP_TCurr_Insert(TCurr item)
        {
            Initialization();
            return await iTCurr_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TCurr_Delete")]
        public async Task<Boolean> ERP_TCurr_Delete(TCurr item)
        {
            Initialization();
            return await iTCurr_Repository.Delete_SaveChange(item);
        }
    }
}

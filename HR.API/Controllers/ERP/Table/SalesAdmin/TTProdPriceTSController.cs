﻿using Dapper;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.WIB.StoreViewModel.GI;
using HR.API.Repository;
using HR.API.Repository.ERP.ProdPriceTS;
using HR.API.UnitOfWork;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using HR.API.Models.ERP.TableCustom;

namespace HR.API.Controllers.ERP.Table.SalesAdmin
{
    //[HR.API.App_Start.JwtAuthentication]
    //public class TTProdPriceTSController : BaseController
    //{
    //    private GenericRepository<TTProdPriceT> repository;
    //    private ITTProdPriceTS_Repository iTTProdPriceTS_Repository;
    //    public TTProdPriceTSController()
    //    {
    //    }
    //    public void Initialization()
    //    {
    //        unitOfWork = new UnitOfWork<PARKERPEntities>();
    //        repository = new GenericRepository<TTProdPriceT>(unitOfWork);
    //        iTTProdPriceTS_Repository = new TTProdPriceTS_Repository(unitOfWork);
    //    }
    //    [HttpPost]
    //    [Route("ERP_TTProdPriceTS_GetReport")]
    //    public async Task<IList<Object>> ERP_TTProdPriceTS_GetReport(dynamic item)
    //    {
    //        Initialization();
    //        return await iTTProdPriceTS_Repository.ERP_TTProdPriceTS_GetReport(item);
    //    }
    //    [HttpPost]
    //    [Route("ERP_TTProdPriceTS_GetByProdInstCD")]
    //    public async Task<IList<Object>> ERP_TTProdPriceTS_GetByProdInstCD(dynamic item)
    //    {
    //        Initialization();
    //        return await iTTProdPriceTS_Repository.ERP_TTProdPriceTS_GetByProdInstCD(item);
    //    }
    //    [HttpPost]
    //    [Route("ERP_TTProdPriceTS_GetByProdInstCD_Excel")]
    //    public HttpResponseMessage ERP_TTProdPriceTS_GetByProdInstCD_Excel(dynamic item)
    //    {
    //        Initialization();
    //        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
    //        string filename = "ProdPriceTS";
    //        Byte[] bytes = ExportDataTableToExcelFromStore(iTTProdPriceTS_Repository.ERP_TTProdPriceTS_GetByProdInstCD_Excel(item), "ProdPriceTS", false, false, "ProdPriceTS");
    //        response.Content = new ByteArrayContent(bytes);
    //        response.Content.Headers.ContentLength = bytes.LongLength;
    //        response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
    //        response.Content.Headers.ContentDisposition.FileName = filename;
    //        response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
    //        GC.SuppressFinalize(this);
    //        return response;
    //    }
    //    [HttpPost]
    //    [Route("ERP_TTProdPriceTS_Import_Excel")]
    //    public  Data ERP_TTProdPriceTS_Import_Excel()
    //    {
    //        Initialization();
    //        List<Result> resultList = new List<Result>();
    //        Data kq = new Data();
    //        try
    //        {
    //            var httpRequest = HttpContext.Current.Request;

    //            string Path = "";
    //            foreach (string file in httpRequest.Files)
    //            {
    //                var postedFile = httpRequest.Files[file];
    //                if (postedFile != null && postedFile.ContentLength > 0)
    //                {
    //                    Path = "~/Upload/Excel/WIP" + postedFile.FileName;
    //                    var filePath = HttpContext.Current.Server.MapPath(Path);
    //                    postedFile.SaveAs(filePath);
    //                }
    //                FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));


    //                if (existingFile.Extension.ToString() == ".xls")
    //                {
    //                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Please import file excel type XLSX!" });
    //                    kq.Results = resultList;
    //                    kq.Type = 0;
    //                    return kq;
    //                }
    //                using (ExcelPackage package = new ExcelPackage(existingFile))
    //                {
    //                    ExcelWorkbook workBook = package.Workbook;
    //                    if (workBook != null)
    //                    {
    //                        if (workBook.Worksheets.Count > 0)
    //                        {
    //                            ExcelWorksheet worksheet = workBook.Worksheets.FirstOrDefault();
    //                            int rowCount = worksheet.Dimension.End.Row;

    //                            List<dynamic> ListInsert = new List<dynamic>();
    //                            List<TTSMG> ListTTSMG = new List<TTSMG>();
    //                            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
    //                            {
    //                                var queryParameters = new DynamicParameters();
    //                                //queryParameters.Add("@FACTORY", this.GetDB());
    //                                connection.Open();
    //                                var data =  connection.Query<TTSMG>("SELECT * FROM dbo.TTSMG (NOLOCK)", queryParameters, commandTimeout: 1500
    //                                    , commandType: CommandType.Text);
    //                                ListTTSMG = data.ToList();
    //                            }

    //                            for (int row = 2; row <= rowCount; row++)
    //                            {

    //                                if (worksheet.Cells[row, 1].Value == null)
    //                                {
    //                                    continue;
    //                                }
    //                                int SalseInfoID;
    //                                if (!int.TryParse(worksheet.Cells[row, 1].Value.ToString(), out SalseInfoID))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "SalseInfoID row " + row.ToString() + " " + worksheet.Cells[row, 1].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                string BuyerPONo = worksheet.Cells[row, 2].Value != null?worksheet.Cells[row, 2].Value.ToString().Trim():null;
    //                                string Factory = worksheet.Cells[row, 3].Value!=null?worksheet.Cells[row, 3].Value.ToString().Trim():null;
    //                                string BrandCD = worksheet.Cells[row, 4].Value!=null? worksheet.Cells[row, 4].Value.ToString().Trim():null;
    //                                string ProdInstCD = worksheet.Cells[row, 5].Value!=null? worksheet.Cells[row, 5].Value.ToString().Trim():null;
    //                                string PlanYear = worksheet.Cells[row, 6].Value!=null? worksheet.Cells[row, 6].Value.ToString().Trim():null;
    //                                string PlanMonth = worksheet.Cells[row, 7].Value!=null? worksheet.Cells[row, 7].Value.ToString().Trim():null;
    //                                string ProdCode = worksheet.Cells[row, 8].Value!=null? worksheet.Cells[row, 8].Value.ToString().Trim():null;
    //                                string SMG = worksheet.Cells[row, 9].Value!=null? worksheet.Cells[row, 9].Value.ToString().Trim():null;

    //                                if (!string.IsNullOrEmpty(SMG) && ListTTSMG.Where(x=>x.SMGCode== SMG).FirstOrDefault()==null)
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "SMG row " + row.ToString() + " " + worksheet.Cells[row, 1].Value.ToString() + " not find!" });
    //                                    continue;
    //                                }

    //                                string StyleCode = worksheet.Cells[row, 10].Value!=null? worksheet.Cells[row, 10].Value.ToString().Trim():null;
    //                                string Color = worksheet.Cells[row, 11].Value!=null? worksheet.Cells[row, 11].Value.ToString().Trim():null;
    //                                string SKUCode = worksheet.Cells[row, 12].Value!=null? worksheet.Cells[row, 12].Value.ToString().Trim():null;
    //                                string Qty = worksheet.Cells[row, 13].Value!=null? worksheet.Cells[row, 13].Value.ToString().Trim():null;

    //                                Double Leather;
    //                                if (!Double.TryParse(worksheet.Cells[row, 14].Value.ToString(), out Leather))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Leather row " + row.ToString() + " " + worksheet.Cells[row, 14].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double Textile;
    //                                if (!Double.TryParse(worksheet.Cells[row, 15].Value.ToString(), out Textile))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Textile row " + row.ToString() + " " + worksheet.Cells[row, 15].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double Hardware;
    //                                if (!Double.TryParse(worksheet.Cells[row, 16].Value.ToString(), out Hardware))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Hardware row " + row.ToString() + " " + worksheet.Cells[row, 16].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double Packaging;
    //                                if (!Double.TryParse(worksheet.Cells[row, 17].Value.ToString(), out Packaging))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Packaging row " + row.ToString() + " " + worksheet.Cells[row, 17].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double Others;
    //                                if (!Double.TryParse(worksheet.Cells[row, 18].Value.ToString(), out Others))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Others row " + row.ToString() + " " + worksheet.Cells[row, 18].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double LocalMat;
    //                                if (!Double.TryParse(worksheet.Cells[row, 19].Value.ToString(), out LocalMat))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "LocalMat row " + row.ToString() + " " + worksheet.Cells[row, 19].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double FreightRM;
    //                                if (!Double.TryParse(worksheet.Cells[row, 20].Value.ToString(), out FreightRM))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "FreightRM row " + row.ToString() + " " + worksheet.Cells[row, 20].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double MatFiller;
    //                                if (!Double.TryParse(worksheet.Cells[row, 21].Value.ToString(), out MatFiller))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "MatFiller row " + row.ToString() + " " + worksheet.Cells[row, 21].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double TotalMRO;
    //                                if (!Double.TryParse(worksheet.Cells[row, 22].Value.ToString(), out TotalMRO))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "TotalMRO row " + row.ToString() + " " + worksheet.Cells[row, 22].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double OthersMRO;
    //                                if (!Double.TryParse(worksheet.Cells[row, 23].Value.ToString(), out OthersMRO))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "OthersMRO row " + row.ToString() + " " + worksheet.Cells[row, 23].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double Outsource;
    //                                if (!Double.TryParse(worksheet.Cells[row, 24].Value.ToString(), out Outsource))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Outsource row " + row.ToString() + " " + worksheet.Cells[row, 24].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double TotalMisc;
    //                                if (!Double.TryParse(worksheet.Cells[row, 25].Value.ToString(), out TotalMisc))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "TotalMisc row " + row.ToString() + " " + worksheet.Cells[row, 25].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double MatMisc;
    //                                if (!Double.TryParse(worksheet.Cells[row, 26].Value.ToString(), out MatMisc))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "MatMisc row " + row.ToString() + " " + worksheet.Cells[row, 26].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double Amortisation;
    //                                if (!Double.TryParse(worksheet.Cells[row, 27].Value.ToString(), out Amortisation))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Amortisation row " + row.ToString() + " " + worksheet.Cells[row, 27].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double AdjustmentSP;
    //                                if (!Double.TryParse(worksheet.Cells[row, 28].Value.ToString(), out AdjustmentSP))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "AdjustmentSP row " + row.ToString() + " " + worksheet.Cells[row, 28].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double Labor;
    //                                if (!Double.TryParse(worksheet.Cells[row, 29].Value.ToString(), out Labor))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Labor row " + row.ToString() + " " + worksheet.Cells[row, 29].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double AddCMT;
    //                                if (!Double.TryParse(worksheet.Cells[row, 30].Value.ToString(), out AddCMT))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "AddCMT row " + row.ToString() + " " + worksheet.Cells[row, 30].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double CMT;
    //                                if (!Double.TryParse(worksheet.Cells[row, 31].Value.ToString(), out CMT))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "CMT row " + row.ToString() + " " + worksheet.Cells[row, 31].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                Double MarkUp;
    //                                if (!Double.TryParse(worksheet.Cells[row, 32].Value.ToString(), out MarkUp))
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "MarkUp row " + row.ToString() + " " + worksheet.Cells[row, 32].Value.ToString() + " not number!" });
    //                                    continue;
    //                                }
    //                                ListInsert.Add(new {  SalseInfoID= SalseInfoID, BuyerPONo = BuyerPONo, Factory = Factory,
    //                                    BrandCD = BrandCD,
    //                                    ProdInstCD = ProdInstCD,
    //                                    PlanYear = PlanYear,
    //                                    PlanMonth = PlanMonth,
    //                                    ProdCode = ProdCode,
    //                                    SMG = SMG,              
    //                                    StyleCode= StyleCode,
    //                                    Color= Color,
    //                                    SKUCode= SKUCode,
    //                                    Qty= Qty,
    //                                    Leather= Leather,
    //                                    Textile= Textile,
    //                                    Hardware= Hardware,
    //                                    Packaging= Packaging,
    //                                    Others= Others,
    //                                    LocalMat= LocalMat,
    //                                    FreightRM= FreightRM,
    //                                    MatFiller= MatFiller,
    //                                    TotalMRO= TotalMRO,
    //                                    OthersMRO= OthersMRO,
    //                                    Outsource= Outsource,
    //                                    TotalMisc= TotalMisc,
    //                                    MatMisc= MatMisc,
    //                                    Amortisation= Amortisation,
    //                                    AdjustmentSP= AdjustmentSP,
    //                                    Labor= Labor,
    //                                    AddCMT= AddCMT,
    //                                    CMT= CMT,
    //                                    MarkUp= MarkUp,
    //                                    RowID=row-1,
    //                                });

    //                            }
    //                            if (ListInsert.Count == 0)
    //                            {
    //                                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File import no data" });
    //                                kq.Results = resultList;
    //                                kq.Type = 0;
    //                                return kq;

    //                            }
    //                            var CheckDoule = (from c in ListInsert//check trùng ProdCode và Prod Order
    //                                              group c by new
    //                                              {
    //                                                  c.SalseInfoID
    //                                              } into gcs
    //                                              where gcs.Count() > 1
    //                                              select new TTProdPriceT()
    //                                              {
    //                                                  SalseInfoID = gcs.Key.SalseInfoID,
    //                                              }).ToList();
    //                            if (CheckDoule.Count() > 0)
    //                            {
    //                                foreach (TTProdPriceT item in CheckDoule)
    //                                {
    //                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Double SalseInfoID " + item.SalseInfoID + "" });
    //                                }
    //                            }
    //                            if (resultList.Count > 0)
    //                            {
    //                                kq.Results = resultList;
    //                                kq.Type = 0;
    //                                return kq;

    //                            }
    //                            kq.Results = ListInsert;
    //                            kq.Type = 1;
    //                            return kq;

    //                        }
    //                    }

    //                }
    //                kq.Results = resultList;
    //                kq.Type = 0;
    //                return kq;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw new NotImplementedException(ex.Message.ToString());

    //        }
    //        kq.Results = resultList;
    //        kq.Type = 0;
    //        return kq;
    //    }
    //    [HttpPost]
    //    [Route("ERP_TTProdPriceTS_DeleteInsertList")]
    //    public async Task<bool> ERP_TTProdPriceTS_DeleteInsertList(List<TTProdPriceT> item)
    //    {
    //        Initialization();
    //        return await iTTProdPriceTS_Repository.ERP_TTProdPriceTS_DeleteInsertList(item);
    //    }
    //    [HttpPost]
    //    [Route("ERP_TTProdPriceTS_GetReport_Excel")]
    //    public HttpResponseMessage ERP_TTProdPriceTS_GetReport_Excel(dynamic item)
    //    {
    //        Initialization();
    //        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
    //        string filename = "ProdPriceTS";
    //        Byte[] bytes = ExportDataTableToExcelFromStore(iTTProdPriceTS_Repository.ERP_TTProdPriceTS_GetReport_Excel(item), "ProdPriceTS", true, false, "ProdPriceTS");
    //        response.Content = new ByteArrayContent(bytes);
    //        response.Content.Headers.ContentLength = bytes.LongLength;
    //        response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
    //        response.Content.Headers.ContentDisposition.FileName = filename;
    //        response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
    //        GC.SuppressFinalize(this);
    //        return response;
    //    }
    //    [HttpPost]
    //    [Route("ERP_TTSMG_GetByCustGrpCD")]
    //    public async Task<IList<TTSMG>> ERP_TTSMG_GetByCustGrpCD(dynamic item)
    //    {
    //        Initialization();
    //        return await iTTProdPriceTS_Repository.ERP_TTSMG_GetByCustGrpCD(item);
    //    }
    //    [HttpPost]
    //    [Route("ERP_TTProdPriceTS_UpdateSMG")]
    //    public async Task<bool> ERP_TTProdPriceTS_UpdateSMG(dynamic item)
    //    {
    //        Initialization();
    //        return await iTTProdPriceTS_Repository.ERP_TTProdPriceTS_UpdateSMG(item);
    //    }
    //}
}

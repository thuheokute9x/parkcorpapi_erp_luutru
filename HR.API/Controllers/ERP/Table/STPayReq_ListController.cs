﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Models.ERP.Rgl;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.PayReq_List;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STPayReq_ListController : BaseController
    {
        private GenericRepository<STPayReq_List> repository;
        private ISTPayReq_List_Repository iSTPayReq_List_Repository;
        public STPayReq_ListController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STPayReq_List>(unitOfWork);
            iSTPayReq_List_Repository = new STPayReq_List_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_STPayReq_List_GetPayDocNo")]
        public async Task<IList<STPayReq_List>> ERP_STPayReq_List_GetPayDocNo(STPayReq_List STPayReq_List)//Đặt sai tên ERP_STPayDoc_GetPayDocNo
        {
            Initialization();
            return await iSTPayReq_List_Repository.ERP_STPayReq_List_GetPayDocNo(STPayReq_List);
        }
        [HttpGet]
        [Route("ERP_STPayReq_List_ByPayReqID")]
        public async Task<IList<RglPayReq_DocGrp>> ERP_STPayReq_List_ByPayReqID(string PayReqID)
        {
            Initialization();
            return await iSTPayReq_List_Repository.ERP_STPayReq_List_ByPayReqID(PayReqID);
        }
        [HttpPost]
        [Route("ERP_STPayReq_List_ByPayDocNo")]
        public async Task<STPayReq_List> ERP_STPayReq_List_ByPayDocNo(STPayReq_List STPayReq_List)
        {
            Initialization();
            return await iSTPayReq_List_Repository.ERP_STPayReq_List_ByPayDocNo(STPayReq_List);
        }
    }
}

﻿using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.PayReqExt;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STPayReqExtController : BaseController
    {
        private GenericRepository<STPayReqExt> repository;
        private ISTPayReqExt_Repository iSTPayReqExt_Repository;
        public STPayReqExtController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STPayReqExt>(unitOfWork);
            iSTPayReqExt_Repository = new STPayReqExt_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_STPayReqExt_ByPayReqID")]
        public async Task<IList<STPayReqExt>> ERP_STPayReqExt_ByPayReqID(string PayReqID)
        {
            this.Initialization();
            return await iSTPayReqExt_Repository.ERP_STPayReqExt_ByPayReqID(PayReqID);
        }
    }
}

﻿using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.ERP.Prod;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TProdController : BaseController
    {
        private GenericRepository<TProc> repository;
        private ITProd_Repository iTProd_Repository;
        public TProdController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TProc>(unitOfWork);
            iTProd_Repository = new TProd_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TProd_GetwithProdCodeLike")]
        public async Task<IList<TProd>> ERP_TProd_GetwithProdCodeLike(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_GetwithProdCodeLike(item);
        }
        [HttpPost]
        [Route("ERP_TProd_Getwith_SKUCode_ProdCode_Like")]
        public async Task<IList<Object>> ERP_TProd_Getwith_SKUCode_ProdCode_Like(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_Getwith_SKUCode_ProdCode_Like(item);
        }
        [HttpPost]
        [Route("ERP_TProd_GetwithProdDescLike")]
        public async Task<IList<TProd>> ERP_TProd_GetwithProdDescLike(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_GetwithProdDescLike(item);
        }
        [HttpPost]
        [Route("ERP_TProd_GetwithSKUCodeLike")]
        public async Task<IList<TProd>> ERP_TProd_GetwithSKUCodeLike(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_GetwithSKUCodeLike(item);
        }
        [HttpPost]
        [Route("ERP_TProd_GetColorCodeLike")]
        public async Task<IList<TProd>> ERP_TProd_GetColorCodeLike(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_GetColorCodeLike(item);
        }
        [HttpPost]
        [Route("ERP_TProd_GetStyleCodeLike")]
        public async Task<IList<TProd>> ERP_TProd_GetStyleCodeLike(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_GetStyleCodeLike(item);
        }
        [HttpPost]
        [Route("ERP_TProd_GetwithProdColorLike")]
        public async Task<IList<TProd>> ERP_TProd_GetwithProdColorLike(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_GetwithProdColorLike(item);
        }
        [HttpPost]
        [Route("ERP_TProd_Insert")]
        public async Task<Result> ERP_TProd_Insert(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_Insert(item);
        }
        [HttpPost]
        [Route("ERP_TProd_Update")]
        public async Task<object> ERP_TProd_Update(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_Update(item);
        }
        [HttpPost]
        [Route("ERP_TProd_GetPagesize")]
        public async Task<IList<Object>> ERP_TProd_GetPagesize(dynamic item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_GetPagesize(item);
        }
        [HttpGet]
        [Route("ERP_TStyleGrp_GetAll")]
        public async Task<IList<Object>> ERP_TStyleGrp_GetAll()
        {
            Initialization();
            return await iTProd_Repository.ERP_TStyleGrp_GetAll();
        }
        [HttpGet]
        [Route("ERP_TProd_GenerateProdCode")]
        public async Task<TProd> ERP_TProd_GenerateProdCode()
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_GenerateProdCode();
        }
        [HttpPost]
        [Route("ERP_TProd_GetPagesize_Excel")]
        public HttpResponseMessage ERP_TProd_GetPagesize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "FProd_";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTProd_Repository.ERP_TProd_GetPagesize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TProd_CheckTStyleMulti")]
        public async Task<Boolean> ERP_TProd_CheckTStyleMulti(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_CheckTStyleMulti(item);
        }
        [HttpPost]
        [Route("ERP_TProd_TStyle_UpdateProdDesc")]
        public async Task<Object> ERP_TProd_TStyle_UpdateProdDesc(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_TStyle_UpdateProdDesc(item);
        }
        [HttpPost]
        [Route("ERP_TProd_Update_TStyle_InsertUpdate")]
        public async Task<Object> ERP_TProd_Update_TStyle_InsertUpdate(TProd item)
        {
            Initialization();
            return await iTProd_Repository.ERP_TProd_Update_TStyle_InsertUpdate(item);
        }


        
    }
}

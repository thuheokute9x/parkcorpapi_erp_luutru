﻿using Dapper;
using HR.API.IRepository;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.Vendor;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP
{
    [HR.API.App_Start.JwtAuthentication]
    public class VendorController : BaseController
    {
        private GenericRepository<TVendor> repository;
        private ITVendorRepository vendorRepository;
        public VendorController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TVendor>(unitOfWork);
            vendorRepository = new TVendorRepository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TVendor_GetwithVendorCDLike")]
        public async Task<IList<TVendor>> ERP_TVendor_GetwithVendorCDLike(string VendorCD)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_GetwithVendorCDLike(VendorCD);
        }
        [HttpPost]
        [Route("ERP_TVendor_GetwithVendorCDLike_All")]
        public async Task<IList<TVendor>> ERP_TVendor_GetwithVendorCDLike_All(TVendor item)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_GetwithVendorCDLike_All(item);
        }
        [HttpGet]
        [Route("ERP_TVendor_GetAll")]
        public async Task<IList<TVendor>> ERP_TVendor_GetAll(string VendorCD)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_GetwithVendorCDLike(VendorCD);
        }
        [HttpGet]
        [Route("ERP_TVendor_GetSHIPPER")]
        public async Task<IList<TVendor>> ERP_TVendor_GetSHIPPER(string SHIPPER)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_GetSHIPPER(SHIPPER);
        }
        [HttpPost]
        [Route("ERP_TVendor_GetPageSize")]
        public async Task<IList<TVendor_Custom>> ERP_TVendor_GetPageSize(TVendor_Custom item)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TVendor_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TVendor_GetPageSize_Excel(TVendor_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "Vendor";
                Byte[] bytes = ExportDataTableToExcelFromStore(vendorRepository.ERP_TVendor_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TVendor_FromTPInfo")]
        public async Task<IList<TVendor_Custom>> ERP_TVendor_FromTPInfo(TVendor_Custom item)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_FromTPInfo(item);
        }
        //[HttpPost]
        //[Route("ERP_TVendor_FromTPInfo_NotExistDis")]
        //public async Task<IList<TVendor_Custom>> ERP_TVendor_FromTPInfo_NotExistDis(TVendor_Custom item)
        //{
        //    Initialization();
        //    return await vendorRepository.ERP_TVendor_FromTPInfo_NotExistDis(item);
        //}
        [HttpPost]
        [Route("ERP_TVendor_Update")]
        public async Task<Boolean> ERP_TVendor_Update(TVendor item)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_Update(item);
        }
        [HttpPost]
        [Route("ERP_TVendor_Insert")]
        public async Task<Boolean> ERP_TVendor_Insert(TVendor item)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_Insert(item);
        }
        [HttpPost]
        [Route("ERP_TVendor_CheckTel_AccountNo")]
        public async Task<Result> ERP_TVendor_CheckTel_AccountNo(TVendor item)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_CheckTel_AccountNo(item);
        }
        [HttpPost]
        [Route("ERP_TVendor_GetVendorCD")]
        public async Task<TVendor> ERP_TVendor_GetVendorCD(TVendor VendorCD)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_GetVendorCD(VendorCD);
        }
        [HttpPost]
        [Route("ERP_TVendor_Update_FilePath")]
        public async Task<bool> ERP_TVendor_Update_FilePath(TVendor_Custom item)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_Update_FilePath(item);
        }
        [HttpPost]
        [Route("ERP_TVendor_TPInfo_GetVendorCD_PGrpCD")]
        public async Task<List<Object>> ERP_TVendor_TPInfo_GetVendorCD_PGrpCD(TVendor_Custom item)
        {
            Initialization();
            return await vendorRepository.ERP_TVendor_TPInfo_GetVendorCD_PGrpCD(item);
        }
        [HttpGet]
        [Route("ERP_TPayDoc_GetAll")]
        public async Task<List<TPayDoc>> ERP_TPayDoc_GetAll()
        {
            Initialization();
            return await vendorRepository.ERP_TPayDoc_GetAll();
        }
        [HttpGet]
        [Route("ERP_STCompany_GetByPayReqTo")]
        public async Task<List<STCompany>> ERP_STCompany_GetByPayReqTo()
        {
            Initialization();
            return await vendorRepository.ERP_STCompany_GetByPayReqTo();
        }
    }
}

﻿using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.ERP.ProdInstStatus;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TProdInstStatusController : BaseController
    {
        private GenericRepository<TProdInstStatu> repository;
        private ITProdInstStatus_Repository iTProdInstStatus_Repository;
        public TProdInstStatusController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TProdInstStatu>(unitOfWork);
            iTProdInstStatus_Repository = new TProdInstStatus_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TProdInstStatus_GetAll")]
        public async Task<IList<TProdInstStatu>> ERP_TProdInstStatus_GetAll()
        {
            Initialization();
            return await iTProdInstStatus_Repository.ERP_TProdInstStatus_GetAll();
        }
        [HttpPost]
        [Route("ERP_TProdInstStatus_Update")]
        public async Task<Boolean> ERP_TVendor_Update(TProdInstStatu item)
        {
            Initialization();
            item.UpdatedOn = DateTime.Now;
            return await iTProdInstStatus_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TProdInstStatus_Insert")]
        public async Task<Boolean> ERP_TVendor_Insert(TProdInstStatu item)
        {
            Initialization();
            item.CreatedOn = DateTime.Now;
            return await iTProdInstStatus_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TProdInstStatus_Delete")]
        public async Task<Boolean> ERP_TSPStatus_Delete(TProdInstStatu item)
        {
            Initialization();
            item.CreatedOn = DateTime.Now;
            return await iTProdInstStatus_Repository.Delete_SaveChange(item);
        }
    }
}

﻿using Dapper;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.PayDoc;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;



namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STPayDocController : BaseController
    {
        private GenericRepository<STPayDoc> repository;
        private ISTPayDocRepository sTPayDocRepository;
        public STPayDocController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STPayDoc>(unitOfWork);
            sTPayDocRepository = new STPayDocRepository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_STPayDoc_InsertList")]
        public async Task<List<STPayDoc>> ERP_STPayDoc_InsertList(List<STPayDoc> sTPayDoclist)
        {
            Initialization();
            return await sTPayDocRepository.ERP_STPayDoc_InsertList(sTPayDoclist);
        }
        [HttpPost]
        [Route("ERP_STPayDoc_Get")]
        public async Task<IList<STPayDoc_Custom>> ERP_STPayDoc_Get(STPayDoc_Custom STPayDo)
        {
            Initialization();
            return await sTPayDocRepository.ERP_STPayDoc_Get(STPayDo);
        }
        [HttpPost]
        [Route("ERP_STPayDoc_CheckInserFromGR")]
        public async Task<bool> ERP_STPayDoc_CheckInserFromGR(STPayDoc_Custom STPayDo)
        {
            Initialization();
            return await sTPayDocRepository.ERP_STPayDoc_CheckInserFromGR(STPayDo);
        }
        [HttpPost]
        [Route("ERP_STPayDoc_CheckInserFromPayDoc")]
        public async Task<bool> ERP_STPayDoc_CheckInserFromPayDoc(STPayDoc_Custom STPayDo)
        {
            Initialization();
            return await sTPayDocRepository.ERP_STPayDoc_CheckInserFromPayDoc(STPayDo);
        }
        //[HttpPost]
        //[Route("ERP_STPayDoc_CheckInserFromGR")]
        //public async Task<bool> ERP_STPayDoc_CheckInserFromGR(STPayDoc_Custom STPayDo)
        //{
        //    return await sTPayDocRepository.ERP_STPayDoc_CheckInserFromGR(STPayDo);
        //}
    }
}

﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.Brand;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TBrandController : BaseController
    {
        private GenericRepository<TBrand> repository;
        private ITBrand_Repository iTBrand_Repository;
        public TBrandController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TBrand>(unitOfWork);
            iTBrand_Repository = new TBrand_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TBrand_GetPageSize")]
        public async Task<IList<TBrand_Custom>> ERP_TBrand_GetPageSize(TBrand_Custom item)
        {
            Initialization();
            return await iTBrand_Repository.ERP_TBrand_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TBrand_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TBrand_GetPageSize_Excel(TBrand_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "TMatGrp";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTBrand_Repository.ERP_TBrand_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        [HttpGet]
        [Route("ERP_TBrand_GetAll")]
        public async Task<IList<TBrand_Custom>> ERP_TBrand_GetAll(TBrand_Custom item)
        {
            Initialization();
            return await iTBrand_Repository.ERP_TBrand_GetAll(item);
        }
        [HttpPost]
        [Route("ERP_TBrand_ByCustGrpCD")]
        public async Task<IList<TBrand_Custom>> ERP_TBrand_ByCustGrpCD(TBrand item)
        {
            Initialization();
            return await iTBrand_Repository.ERP_TBrand_ByCustGrpCD(item);
        }

        [HttpPost]
        [Route("ERP_TBrand_ByBrandCD")]
        public async Task<IList<TBrand_Custom>> ERP_TBrand_ByBrandCD(TBrand item)
        {
            Initialization();
            return await iTBrand_Repository.ERP_TBrand_ByBrandCD(item);
        }

        [HttpPost]
        [Route("ERP_TBrand_Update")]
        public async Task<Boolean> ERP_TBrand_Update(TBrand item)
        {
            Initialization();
            return await iTBrand_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TBrand_Insert")]
        public async Task<Boolean> ERP_TBrand_Insert(TBrand item)
        {
            Initialization();
            return await iTBrand_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TBrand_Delete")]
        public async Task<Boolean> ERP_TBrand_Delete(TBrand item)
        {
            Initialization();
            return await iTBrand_Repository.Delete_SaveChange(item);
        }
    }
}

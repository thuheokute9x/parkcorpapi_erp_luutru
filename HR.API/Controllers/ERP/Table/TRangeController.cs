﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.Range;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;



namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TRangeController : BaseController
    {
        private GenericRepository<TRange> repository;
        private ITRange_Repository iTRange_Repository;
        public TRangeController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TRange>(unitOfWork);
            iTRange_Repository = new TRange_Repository(unitOfWork);
        }

        [HttpPost]
        [Route("ERP_TRange_GetPageSize")]
        public async Task<IList<TRange_Custom>> ERP_TRange_GetPageSize(TRange_Custom item)
        {
            Initialization();
            return await iTRange_Repository.ERP_TRange_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TRange_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TRange_GetPageSize_Excel(TRange_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "TRange";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTRange_Repository.ERP_TRange_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        [HttpPost]
        [Route("ERP_TRange_Insert")]
        public async Task<Boolean> ERP_TRange_Insert(TRange item)
        {
            Initialization();
            return await iTRange_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TRange_Update")]
        public async Task<Boolean> ERP_TRange_Update(TRange item)
        {
            Initialization();
            return await iTRange_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TRange_Delete")]
        public async Task<Boolean> ERP_TRange_Delete(TRange item)
        {
            Initialization();
            return await iTRange_Repository.Delete_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TRange_ByRangeDesc")]
        public async Task<IList<TRange>> ERP_TRange_ByRangeDesc(TRange item)
        {
            Initialization();
            return await iTRange_Repository.ERP_TRange_ByRangeDesc(item);
        }
        [HttpPost]
        [Route("ERP_TRange_ByBrandCD")]
        public async Task<IList<TRange_Custom>> ERP_TRange_ByBrandCD(TRange item)
        {
            Initialization();
            return await iTRange_Repository.ERP_TRange_ByBrandCD(item);
        }
        [HttpPost]
        [Route("ERP_TRange_ByRangeID")]
        public async Task<IList<TRange>> ERP_TRange_ByRangeID(TRange item)
        {
            Initialization();
            return await iTRange_Repository.ERP_TRange_ByRangeID(item);
        }
    }
}

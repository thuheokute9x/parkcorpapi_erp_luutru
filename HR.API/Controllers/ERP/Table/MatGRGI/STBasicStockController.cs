﻿using Dapper;
using HR.API.Repository;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository.ERP.BasicStock;
using HR.API.Repository.In;
using HR.API.UnitOfWork;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table.MatGRGI
{
    [HR.API.App_Start.JwtAuthentication]
    public class STBasicStockController : BaseController
    {
        private GenericRepository<STIn> repository;
        private ISTBasicStock_Repository iSTBasicStock_Repository;
        public STBasicStockController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STIn>(unitOfWork);
            iSTBasicStock_Repository = new STBasicStock_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_STBasicStock_InsertList")]
        public async Task<Boolean> ERP_STBasicStock_InsertList(List<STBasicStock> item)
        {
            Initialization();
            return await iSTBasicStock_Repository.ERP_STBasicStock_InsertList(item);
        }
        [HttpPost]
        [Route("ERP_STBasicStock_Import_Excel")]
        public Data ERP_STBasicStock_Import_Excel()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            Data kq = new Data();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                string Path = "";
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        Path = "~/Upload/Excel/WIP" + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath(Path);
                        postedFile.SaveAs(filePath);
                    }
                    FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));



                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        ExcelWorkbook workBook = package.Workbook;
                        if (workBook != null)
                        {
                            if (workBook.Worksheets.Count > 0)
                            {
                                ExcelWorksheet worksheet = workBook.Worksheets.FirstOrDefault();
                                int rowCount = worksheet.Dimension.End.Row;

                                List<dynamic> ListInsert = new List<dynamic>();
                                List<STBasicStock_Custom> ListCheck = new List<STBasicStock_Custom>();
                                List<dynamic> ListCanNotInsertSTBasicStock = new List<dynamic>();
                                string ListMatCD = "";
                                List<TUnit> ListTUnitCheck = new List<TUnit>();
                                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                {
                                    var queryParameters = new DynamicParameters();
                                    //queryParameters.Add("@FACTORY", this.GetDB());
                                    connection.Open();
                                    var data = connection.Query<TUnit>("SELECT UnitCD FROM dbo.TUnit (NOLOCK)", queryParameters, commandTimeout: 1500
                                        , commandType: CommandType.Text);
                                    ListTUnitCheck = data.ToList();

                                }

                                for (int row = 2; row <= rowCount; row++)
                                {

                                    if (worksheet.Cells[row, 1].Value == null)
                                    {
                                        continue;
                                    }
                                    string MatCD = worksheet.Cells[row, 1].Value!=null? worksheet.Cells[row, 1].Value.ToString().Trim():null;


                                    //if (tmat==null)
                                    //{
                                    //    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "MatCD row " + row.ToString() + " " + worksheet.Cells[row, 1].Value.ToString() + " not find!" });
                                    //    continue;
                                    //}

                                    string Unit = worksheet.Cells[row, 2].Value!=null? worksheet.Cells[row, 2].Value.ToString().Trim():null;
                                    TUnit unitcheck = ListTUnitCheck.FirstOrDefault(x=>x.UnitCD== Unit); 
                                    if (unitcheck == null)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Unit row " + row.ToString() + " " + worksheet.Cells[row, 2].Value + " not find!" });
                                        continue;
                                    }
                                    string Qtycheck = worksheet.Cells[row, 3].Value != null ? worksheet.Cells[row, 3].Value.ToString().Trim() : null;
                                    if (Qtycheck == null)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Please input Qty row " + row.ToString() + "!" });
                                        continue;
                                    }

                                    decimal Qty;
                                    if (!decimal.TryParse(worksheet.Cells[row, 3].Value.ToString(), out Qty))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Qty row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    if (Qty==0)//k insert thằng bằng 0
                                    {
                                        continue;
                                    }
                                    string Remark = worksheet.Cells[row, 4].Value!=null? worksheet.Cells[row, 4].Value.ToString().Trim():null;



                                    ListMatCD = ListMatCD+"'" + MatCD+"'" + ",";
                                    ListInsert.Add(new 
                                    {
                                        RowID= row-1,
                                        MatCD = MatCD,
                                        Unit = Unit,
                                        Qty = Qty,
                                        Remark = Remark
                                    });

                                }
                                if (ListInsert.Count == 0)
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File import no data" });
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;

                                }
                                var CheckDoule = (from c in ListInsert//check trùng ProdCode và Prod Order
                                                  group c by new
                                                  {
                                                      c.MatCD
                                                  } into gcs
                                                  where gcs.Count() > 1
                                                  select new STBasicStock()
                                                  {
                                                      MatCD = gcs.Key.MatCD,
                                                  }).ToList();
                                if (CheckDoule.Count() > 0)
                                {
                                    foreach (STBasicStock item in CheckDoule)
                                    {
                                        resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Double MatCD " + item.MatCD + "" });
                                    }
                                }
                                if (resultList.Count > 0)
                                {
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;

                                }
                                dynamic ob = new ExpandoObject();


                                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                {


                                    string query = @"SELECT a.PInfoID,a.MatCD,a.PUnit INTO #TPInfo FROM dbo.TPInfo a (NOLOCK) WHERE a.Discontinue=0 SELECT MAX(PInfoID) PInfoID,MatCD INTO #TPInfoGRID FROM #TPInfo GROUP BY MatCD SELECT MatCD,PUnit INTO #TPInfoGRPUnit FROM #TPInfo GROUP BY MatCD,PUnit ORDER BY MatCD SELECT MatCD INTO #TPInfoGRMutiPunit FROM #TPInfoGRPUnit GROUP BY MatCD HAVING COUNT(MatCD) > 1 DROP TABLE #TPInfoGRPUnit SELECT MatCD,PUnit INTO #TPInfo_PUnitChuan FROM (SELECT a.MatCD,CASE WHEN b.MatCD IS NOT NULL AND a.PUNIT='SET' THEN 'PC' WHEN b.MatCD IS NOT NULL AND a.PUNIT='YRD' THEN 'M' WHEN b.MatCD IS NOT NULL AND a.PUNIT='SQM' THEN 'SQF' ELSE a.PUNIT END PUnit FROM #TPInfo a LEFT JOIN #TPInfoGRMutiPunit b ON a.MatCD=b.MatCD WHERE a.PInfoID IN (SELECT PInfoID FROM #TPInfoGRID)) f GROUP BY MatCD,PUnit SELECT a.MatCD,a.punit,a.PUnit,CASE WHEN b.Spec1 IS NULL AND b.Spec2 IS NOT NULL THEN b.MatName+ ' '+ b.Spec2 WHEN b.Spec1 IS NOT NULL AND b.Spec2 IS NULL THEN b.MatName+ ' '+ b.Spec1 WHEN b.Spec1 IS NOT NULL AND b.Spec2 IS NOT NULL THEN b.MatName+ ' '+ b.Spec1 + ' '+ b.Spec2 ELSE b.MatName END MatDesc ,b.Color FROM #TPInfo_PUnitChuan a INNER JOIN dbo.TMat b (NOLOCK) ON a.MatCD=b.MatCD DROP TABLE #TPInfo,#TPInfoGRMutiPunit,#TPInfo_PUnitChuan,#TPInfoGRID";
                      
                                    var queryParameters = new DynamicParameters();
                                    //queryParameters.Add("@ListMatCD", ListMatCD, DbType.String);
                                    connection.Open();
                                    var data = connection.Query<dynamic>(@query, queryParameters, commandTimeout: 1500
                                        , commandType: CommandType.Text);
                                    int row = 1;
                                    foreach (dynamic item in ListInsert)
                                    {
                                        dynamic tmat = new Object();
                                        tmat = data.ToList().Where(x => x.MatCD == item.MatCD).FirstOrDefault();
                                        Boolean LenKChk = item.MatCD.Length!=8?true:false;
                                        ListCheck.Add(new STBasicStock_Custom()
                                        {
                                            MatCD = item.MatCD,
                                            MatDesc = tmat != null ? tmat.MatDesc : null,
                                            Color = tmat != null ? tmat.Color : null,
                                            UnitCD = item.Unit,
                                            Qty = item.Qty,
                                            PUnit = tmat != null ? tmat.PUnit : null,
                                            UnitChk = tmat != null ? (tmat.PUnit == item.Unit ? false : true) : true,
                                            BLankChk = tmat != null?false:true,
                                            LenKChk = LenKChk
                                        });
                                        if (tmat == null || (tmat.PUnit == "PC" && (item.Unit != "SET" && item.Unit != "PC"))
                                            || (tmat.PUnit == "M" && (item.Unit != "M" && item.Unit != "YRD"))
                                            || (tmat.PUnit == "SQF" && (item.Unit != "SQF" && item.Unit != "SQM")))
                                        {
                                            ListCanNotInsertSTBasicStock.Add(new
                                            {
                                                ErrorCode = row,
                                                MatCD = item.MatCD,
                                                ErrorMessage = tmat != null ? (tmat.PUnit == "PC" && (item.Unit != "SET" && item.Unit != "PC"))
                                                                        || (tmat.PUnit == "M" && (item.Unit != "M" && item.Unit != "YRD"))
                                                                        || (tmat.PUnit == "SQF" && (item.Unit != "SQF" && item.Unit != "SQM"))
                                                                        ? "Please check Unit " + item.Unit + " in MatCD " + item.MatCD : "" : "Not find MatCD " + item.MatCD + " in ERP"
                                            });
                                        }
                                        row++;
                                    }
                                    //var data = connection.Query<dynamic>("ERP_TPInfo_CheckSTBasicStock", queryParameters, commandTimeout: 1500
                                    //    , commandType: CommandType.StoredProcedure);
                                    //tmat = data.FirstOrDefault();
                                }

                                ob.listImport = ListInsert;
                                ob.listCheckMaterialList = ListCheck.OrderByDescending(x=>x.UnitChk).ThenByDescending(x => x.BLankChk).ThenByDescending(x => x.LenKChk).Select((b, index) => new STBasicStock_Custom
                                {
                                    RowID = index + 1,
                                    MatCD = b.MatCD,
                                    MatDesc = b.MatDesc,
                                    Color = b.Color,
                                    UnitCD = b.UnitCD,
                                    Qty = b.Qty,
                                    PUnit = b.PUnit,
                                    UnitChk = b.UnitChk,
                                    BLankChk = b.BLankChk,
                                    LenKChk = b.LenKChk,
                                }).ToList();
                                ob.listCanNotInsertSTBasicStock = ListCanNotInsertSTBasicStock;
                                kq.Results = ob;
                                kq.Type = 1;
                                return kq;

                            }
                        }

                    }
                    kq.Results = resultList;
                    kq.Type = 0;
                    return kq;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());

            }
            kq.Results = resultList;
            kq.Type = 0;
            return kq;
        }
        [HttpPost]
        [Route("ERP_STBasicStock_GetCheckMaterialList_Excel")]
        public HttpResponseMessage ERP_STBasicStock_GetCheckMaterialList_Excel(List<dynamic> list)
        {
            Initialization();
            DataTable dt = new DataTable();
            dt.Columns.Add("MatCD");
            dt.Columns.Add("MatDesc");
            dt.Columns.Add("Color");
            dt.Columns.Add("UnitCD");
            dt.Columns.Add("Qty").DataType = typeof(float);
            dt.Columns.Add("PUnit");
            dt.Columns.Add("UnitChk");
            dt.Columns.Add("BlankChk");
            dt.Columns.Add("LenChk");
            foreach (dynamic item in list)
            {
                DataRow dr = dt.NewRow();
                dr["MatCD"] = item.MatCD;
                dr["MatDesc"] = item.MatDesc;
                dr["Color"] = item.Color;
                dr["UnitCD"] = item.UnitCD;
                dr["Qty"] = float.Parse(item.Qty.ToString());
                dr["PUnit"] = item.PUnit;
                dr["UnitChk"] = item.UnitChk;
                dr["BlankChk"] = item.BLankChk;
                dr["LenChk"] = item.LenKChk;
                dt.Rows.Add(dr);
            }


            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string filename = "CheckMaterialList";
            Byte[] bytes = ExportDataTableToExcelFromStore(dt, "CheckMaterialList", false, false, "CheckMaterialList");
            response.Content = new ByteArrayContent(bytes);
            response.Content.Headers.ContentLength = bytes.LongLength;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = filename;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
            GC.SuppressFinalize(this);
            return response;
        }
        [HttpPost]
        [Route("ERP_STBasicStock_GetByStartYearStartMonth")]
        public async Task<IList<Object>> ERP_STBasicStock_GetByStartYearStartMonth(dynamic item)
        {
            Initialization();
            return await iSTBasicStock_Repository.ERP_STBasicStock_GetByStartYearStartMonth(item);
        }
        [HttpPost]
        [Route("ERP_STBasicStock_GetByStartYearStartMonth_Excel")]
        public HttpResponseMessage ERP_STBasicStock_GetByStartYearStartMonth_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "BasicStock" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iSTBasicStock_Repository.ERP_STBasicStock_GetByStartYearStartMonth_Excel(item), "BasicStock", false, false);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

﻿using HR.API.Repository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository.Out;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STOutController : BaseController
    {
        private GenericRepository<STOut> repository;
        private ISTOut_Repository iSTOut_Repository;
        public STOutController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STOut>(unitOfWork);
            iSTOut_Repository = new STOut_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_STOut_List_ByOUTCD")]
        public async Task<IList<STOut_List_Custom>> ERP_STOut_List_ByOUTCD(STOut_List STIn)
        {
            Initialization();
            return await iSTOut_Repository.ERP_STOut_List_ByOUTCD(STIn);
        }
        [HttpPost]
        [Route("ERP_STOut_GetOUTCD")]
        public async Task<IList<STOut>> ERP_STOut_GetOUTCD(STOut STIn)
        {
            Initialization();
            return await iSTOut_Repository.ERP_STOut_GetOUTCD(STIn);
        }
        [HttpPost]
        [Route("ERP_STVKGrp_GetVKGRPCD")]
        public async Task<IList<STVKGrp>> ERP_STVKGrp_GetVKGRPCD(STVKGrp item)
        {
            Initialization();
            return await iSTOut_Repository.ERP_STVKGrp_GetVKGRPCD(item);
        }
        [HttpPost]
        [Route("ERP_STOut_List_GetGIPageSize")]
        public async Task<IList<STOut_List_Custom>> ERP_STOut_List_GetGIPageSize(STOut_List_Custom item)
        {
            Initialization();
            return await iSTOut_Repository.ERP_STOut_List_GetGIPageSize(item);
        }
        [HttpPost]
        [Route("ERP_STOut_List_GetGIPageSize_Excel")]
        public HttpResponseMessage ERP_STOut_List_GetGIPageSize_Excel(STOut_List_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "SFMatGI" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iSTOut_Repository.ERP_STOut_List_GetGIPageSize_Excel(item), "SFMatGI", true, false);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_STOut_List_AdjustStock")]
        public async Task<Boolean> ERP_STOut_List_AdjustStock(dynamic item)
        {
            Initialization();
            return await iSTOut_Repository.ERP_STOut_List_AdjustStock(item);
        }
        [HttpPost]
        [Route("ERP_STOut_CheckAdjustStock")]
        public async Task<Boolean> ERP_STOut_CheckAdjustStock(dynamic item)
        {
            Initialization();
            return await iSTOut_Repository.ERP_STOut_CheckAdjustStock(item);
        }
    }
}

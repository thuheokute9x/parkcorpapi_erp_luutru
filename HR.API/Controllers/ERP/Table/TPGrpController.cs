﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.PGrp;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TPGrpController : BaseController
    {
        private GenericRepository<TPGrp> repository;
        private ITPGrp_Repository iTPGrp_Repository;
        public TPGrpController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TPGrp>(unitOfWork);
            iTPGrp_Repository = new TPGrp_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TPGrp_GetAll")]
        public async Task<IList<TPGrp>> ERP_TPGrp_GetAll()
        {
            Initialization();
            return await iTPGrp_Repository.ERP_TPGrp_GetAll();
        }
        [HttpPost]
        [Route("ERP_TPGrp_Update")]
        public async Task<Boolean> ERP_TPGrp_Update(TPGrp item)
        {
            Initialization();
            return await iTPGrp_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TPGrp_Insert")]
        public async Task<Boolean> ERP_TPGrp_Insert(TPGrp item)
        {
            Initialization();
            return await iTPGrp_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TPGrp_Delete")]
        public async Task<Boolean> ERP_TPGrp_Delete(TPGrp item)
        {
            Initialization();
            return await iTPGrp_Repository.Delete_SaveChange(item);
        }
    }
}

﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.Country;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TCountryController : BaseController
    {
        private GenericRepository<TCountry> repository;
        private ITCountry_Repository iTCountry_Repository;
        public TCountryController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TCountry>(unitOfWork);
            iTCountry_Repository = new TCountry_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TCountry_GetAll")]
        public async Task<IList<TCountry>> ERP_TCountry_GetAll()
        {
            Initialization();
            return await iTCountry_Repository.ERP_TCountry_GetAll();
        }
        [HttpPost]
        [Route("ERP_TCountry_Update")]
        public async Task<Boolean> ERP_TCountry_Update(TCountry item)
        {
            Initialization();
            return await iTCountry_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TCountry_Insert")]
        public async Task<Boolean> ERP_TCountry_Insert(TCountry item)
        {
            Initialization();
            return await iTCountry_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TCountry_Delete")]
        public async Task<Boolean> ERP_TCountry_Delete(TCountry item)
        {
            Initialization();
            return await iTCountry_Repository.Delete_SaveChange(item);
        }
    }
}

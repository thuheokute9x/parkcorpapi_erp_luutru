﻿using Dapper;
using ExcelLibrary.SpreadSheet;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models.ERP.TempModel.Development;
using HR.API.Repository;
using HR.API.Repository.ERP.DMat;
using HR.API.UnitOfWork;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TDMatController : BaseController
    {
        private GenericRepository<TDMat> repository;
        private ITDMat_Repository tTDMat_Repository;
        public TDMatController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TDMat>(unitOfWork);
            tTDMat_Repository = new TDMat_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TDMat_ByDMatCD")]
        public async Task<IList<TDMat>> ERP_TDMat_ByDMatCD(string DMatCD)
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMat_ByDMatCD(DMatCD);
        }
        [HttpPost]
        [Route("ERP_Material_TDMat")]
        public async Task<IList<TDMat_Custom>> ERP_Material_TDMat(TDMat_Custom DMatCD)
        {
            Initialization();
            return await tTDMat_Repository.ERP_Material_TDMat(DMatCD);
        }
        [HttpPost]
        [Route("ERP_Material_TDMat_Excel")]
        public HttpResponseMessage ERP_Material_TDMat_Excel(TDMat_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "Dev(New) Mat";
                Byte[] bytes = ExportDataTableToExcelFromStore(tTDMat_Repository.ERP_Material_TDMat_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_Material_TDMat_TMat")]
        public async Task<IList<TDMat_Custom>> ERP_Material_TDMat_TMat(TDMat_Custom DMatCD)
        {
            Initialization();
            return await tTDMat_Repository.ERP_Material_TDMat_TMat(DMatCD);
        }
        [HttpPost]
        [Route("ERP_Material_TDMat_TMat_Excel")]
        public HttpResponseMessage ERP_Material_TDMat_TMat_Excel(TDMat_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "Prod(Transferred)Mat";
                Byte[] bytes = ExportDataTableToExcelFromStore(tTDMat_Repository.ERP_Material_TDMat_TMat_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpGet]
        [Route("ERP_TDMat_GenerateDMatCD")]
        public async Task<TDMat_Custom> ERP_TDMat_GenerateDMatCD()
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMat_GenerateDMatCD();
        }
        [HttpPost]
        [Route("ERP_TDMat_Insert")]
        public async Task<TDMat> ERP_TDMat_Insert(TDMat_Custom item)
        {
            Initialization();
            return await tTDMat_Repository.InsertTDMat(item);
        }
        [HttpPost]
        [Route("ERP_TDMat_Update")]
        public async Task<TDMat> ERP_TDMat_Update(TDMat_Custom item)
        {
            Initialization();
            return await tTDMat_Repository.UpdateTDMat(item);
        }
        [HttpPost]
        [Route("ERP_TDMat_Update_NA")]
        public async Task<bool> ERP_TDMat_Update_NA(TDMat item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMat_Update_NA(item);
        }
        [HttpPost]
        [Route("ERP_TMatGrpItem_Update_Flag")]
        public async Task<bool> ERP_TMatGrpItem_Update_Flag(TMatGrpItem item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_TMatGrpItem_Update_Flag(item);
        }
        [HttpPost]
        [Route("ERP_TDMat_Update_Matcat_Price_Spec_Color")]
        public async Task<bool> ERP_TDMat_Update_Matcat_Price_Spec_Color(TDMat item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMat_Update_Matcat_Price_Spec_Color(item);
        }
        [HttpPost]
        [Route("ERP_TDMat_Update_NAList")]
        public async Task<bool> ERP_TDMat_Update_NAList(List<TDMat> item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMat_Update_NAList(item);
        }
        [HttpPost]
        [Route("ERP_TDMat_GetItemByDMatCD")]
        public async Task<TDMat_Custom> ERP_TDMat_GetItemByDMatCD(TDMat_Custom item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMat_GetItemByDMatCD(item);
        }
        [HttpGet]
        [Route("ERP_TDMatStatus_GetAll")]
        public async Task<IList<TDMatStatu>> ERP_TDMatStatus_GetAll()
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMatStatus_GetAll();
        }
        [HttpPost]
        [Route("UpdateTDMat_MatCat_LossRate_Remark")]
        public async Task<Boolean> UpdateTDMat_MatCat_LossRate_Remark(TDMat_Custom item)
        {
            Initialization();
            return await tTDMat_Repository.UpdateTDMat_MatCat_LossRate_Remark(item);
        }
        [HttpPost]
        [Route("ERP_Material_TDMat_Delete")]
        public async Task<bool> ERP_Material_TDMat_Delete(TDMat DMatCD)
        {
            Initialization();
            return await tTDMat_Repository.ERP_Material_TDMat_Delete(DMatCD);
        }
        [HttpPost]
        [Route("ERP_TDMat_ImportOtherDevMaterial")]
        public async Task<bool> ERP_TDMat_ImportOtherDevMaterial(List<TDMat_Custom> item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMat_ImportOtherDevMaterial(item);
        }
        [HttpPost]
        [Route("ERP_TDMat_CheckImportOtherDevMaterial")]
        public async Task<bool> ERP_TDMat_CheckImportOtherDevMaterial(TDMat_Custom item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMat_CheckImportOtherDevMaterial(item);
        }
        [HttpPost]
        [Route("ERP_TDMat_ImportProdTransferredMaterial")]
        public async Task<bool> ERP_TDMat_ImportProdTransferredMaterial(List<TDMat_Custom> item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMat_ImportProdTransferredMaterial(item);
        }
        [HttpPost]
        [Route("ERP_TDMat_CheckImportProdTransferredMaterial")]
        public async Task<Result> ERP_TDMat_CheckImportProdTransferredMaterial(TDMat_Custom item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMat_CheckImportProdTransferredMaterial(item);
        }
        [HttpPost]
        [Route("ERP_Material_TDMat_TMat_ImportProdTransferredMaterial")]
        public async Task<IList<TDMat_Custom>> ERP_Material_TDMat_TMat_ImportProdTransferredMaterial(TDMat_Custom DMatCD)
        {
            Initialization();
            return await tTDMat_Repository.ERP_Material_TDMat_TMat_ImportProdTransferredMaterial(DMatCD);
        }
        [HttpPost]
        [Route("ERP_FDMat_AddPressMaterial_InsertRedis")]
        public async Task<bool> ERP_FDMat_AddPressMaterial_InsertRedis(List<PressModel> item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_FDMat_AddPressMaterial_InsertRedis(item);
        }
        [HttpPost]
        [Route("ERP_FDMat_AddPressMaterial_GetRedis")]
        public async Task<List<PressModel>> ERP_FDMat_AddPressMaterial_GetRedis(PressModel item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_FDMat_AddPressMaterial_GetRedis(item);
        }
        [HttpGet]
        [Route("ERP_FDMat_AddPressMaterial_DeleteRedisByUserlogin")]
        public async Task<bool> ERP_FDMat_AddPressMaterial_DeleteRedisByUserlogin()
        {
            Initialization();
            return await tTDMat_Repository.ERP_FDMat_AddPressMaterial_DeleteRedisByUserlogin();
        }
        [HttpPost]
        [Route("ERP_FDMat_AddPressMaterial_DeleteRedisByMatGrpID")]
        public async Task<bool> ERP_FDMat_AddPressMaterial_DeleteRedisByMatGrpID(PressModel item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_FDMat_AddPressMaterial_DeleteRedisByMatGrpID(item);
        }
        [HttpPost]
        [Route("ERP_FDMat_AddPressMaterial_Insert")]
        public async Task<IList<PressModel>> ERP_FDMat_AddPressMaterial_Insert(List<PressModel> item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_FDMat_AddPressMaterial_Insert(item);
        }
        [HttpPost]
        [Route("WIP_FDMat_LoadExcelImportAddNewSupport")]
        public async Task<List<TDMat_Custom>> WIP_FDMat_LoadExcelImportAddNewSupport()
        {
            Initialization();
            PARKERPEntities db = new PARKERPEntities();
            List<TDMat_Custom> resultList = new List<TDMat_Custom>();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                string Path = "";
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        Path = "~/Upload/Excel/" + DateTime.Now.ToString("yyyymmddhhmmss") + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath(Path);
                        postedFile.SaveAs(filePath);
                    }
                    FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));

                    if (existingFile.Extension.ToString() == ".xls")
                    {
                        return await LoadExcelImportAddNewSupportXLS(Path);
                    }

                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        ExcelWorkbook workBook = package.Workbook;
                        if (workBook != null)
                        {
                            if (workBook.Worksheets.Count > 0)
                            {
                                ExcelWorksheet worksheet = workBook.Worksheets.FirstOrDefault();
                                int rowCount = worksheet.Dimension.End.Row;
                               
                                int MatTypeCD = 1;
                                int MatCat = 2;
                                int MHName = 3;
                                int MatName = 4;
                                int Color = 5;
                                int MUnit = 6;
                                int YUnit = 7;
                                int LossRate = 8;
                                int Costing = 9;
                                int RapMua = 10;
                                int MultiSrc = 11;
                                int AltMat = 12;
                                int PO = 13;
                                int GR = 14;
                                int Class = 15;
                                int UnderDev = 16;
                                int MatShapeName = 17;
                                int MatStdW = 18;
                                int MatStdH = 19;
                                int MatStdUnit = 20;
                                int MfgVendor = 21;
                                int Shipper = 22;
                                int PUnit = 23;
                                int PGrpCD = 24;
                                int PLeadtime = 25;
                                int MOQ = 26;
                                int PurchaseLotSize = 27;
                                int FreightRate = 28;
                                int PriceLotSize = 29;
                                int Incoterms = 30;
                                int EXWPrice = 31;
                                int EXWCurr = 32;
                                int FOBPrice = 33;
                                int FOBCurr = 34;
                                int FOBcPrice = 35;
                                int CIFPrice = 36;
                                int CIFCurr = 37;
                                int ValidFrom = 38;                           



                                for (int row = 2; row <= rowCount; row++)
                                {
                                    if (worksheet.Cells[row, MatTypeCD].Value == null || worksheet.Cells[row, MatCat].Value == null || worksheet.Cells[row, MHName].Value == null
                                       || worksheet.Cells[row, MatName].Value == null || worksheet.Cells[row, Color].Value == null 
                                       || worksheet.Cells[row, MUnit].Value == null || worksheet.Cells[row, YUnit].Value == null)
                                    {
                                        continue;
                                    }
                                    TDMat_Custom ob = new TDMat_Custom();
                                    ob.MatTypeCD = worksheet.Cells[row, MatTypeCD].Value.ToString();
                                    ob.MatCat = worksheet.Cells[row, MatCat].Value.ToString();
                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, MHName].Value.ToString()))
                                    {
                                        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                        {
                                            var queryParameters = new DynamicParameters();
                                            queryParameters.Add("@MHName", worksheet.Cells[row, MHName].Value.ToString());
                                            connection.Open();
                                            var data = await connection.QueryAsync<TMH>("ERP_TMH_GetMHIDByMHName"
                                                , queryParameters
                                                , commandTimeout: 1500
                                                , commandType: CommandType.StoredProcedure);
                                            if (data.FirstOrDefault() != null)
                                            {
                                                ob.MHID = data.FirstOrDefault().MHID;
                                            }
                                        }
                                    }
                                    ob.MatName = worksheet.Cells[row, MatName].Value.ToString();
                                    ob.Color = worksheet.Cells[row, Color].Value.ToString();
                                    ob.MUnit = worksheet.Cells[row, MUnit].Value.ToString();
                                    ob.YUnit = worksheet.Cells[row, YUnit].Value.ToString();
                                    ob.LossRate = string.IsNullOrEmpty(worksheet.Cells[row, LossRate].Value.ToString())?(float?)null: float.Parse(worksheet.Cells[row, LossRate].Value.ToString());
                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, Costing].Value.ToString()))
                                    {
                                        if (worksheet.Cells[row, Costing].Value.ToString()=="0")
                                        {
                                            ob.Costing = false;
                                        }
                                        ob.Costing = true;
                                    }
                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, RapMua].Value.ToString()))
                                    {
                                        if (worksheet.Cells[row, RapMua].Value.ToString() == "0")
                                        {
                                            ob.RapMua = false;
                                        }
                                        ob.RapMua = true;
                                    }
                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, MultiSrc].Value.ToString()))
                                    {
                                        if (worksheet.Cells[row, MultiSrc].Value.ToString() == "0")
                                        {
                                            ob.MultiSrc = false;
                                        }
                                        ob.MultiSrc = true;
                                    }
                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, AltMat].Value.ToString()))
                                    {
                                        if (worksheet.Cells[row, AltMat].Value.ToString() == "0")
                                        {
                                            ob.AltMat = false;
                                        }
                                        ob.AltMat = true;
                                    }
                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, PO].Value.ToString()))
                                    {
                                        if (worksheet.Cells[row, PO].Value.ToString() == "0")
                                        {
                                            ob.PO = false;
                                        }
                                        ob.PO = true;
                                    }
                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, GR].Value.ToString()))
                                    {
                                        if (worksheet.Cells[row, GR].Value.ToString() == "0")
                                        {
                                            ob.GR = false;
                                        }
                                        ob.GR = true;
                                    }

                                    ob.Class = short.Parse(worksheet.Cells[row, Class].Value.ToString());
                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, Class].Value.ToString()))
                                    {
                                        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                        {
                                            var queryParameters = new DynamicParameters();
                                            queryParameters.Add("@MatClassDesc", worksheet.Cells[row, Class].Value.ToString());
                                            connection.Open();
                                            var data = await connection.QueryAsync<STMatClass>("ERP_STMatClass_GetMatClassIDByMatClassDesc"
                                                , queryParameters
                                                , commandTimeout: 1500
                                                , commandType: CommandType.StoredProcedure);
                                            if (data.FirstOrDefault() != null)
                                            {
                                                ob.Class = short.Parse(data.FirstOrDefault().MatClassID.ToString());
                                            }
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, UnderDev].Value.ToString()))
                                    {
                                        if (worksheet.Cells[row, UnderDev].Value.ToString() == "0")
                                        {
                                            ob.UnderDev = false;
                                        }
                                        ob.UnderDev = true;
                                    }
                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, MatShapeName].Value.ToString()))
                                    {
                                        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                        {
                                            var queryParameters = new DynamicParameters();
                                            queryParameters.Add("@MatShape", worksheet.Cells[row, MatShapeName].Value.ToString());
                                            connection.Open();
                                            var data = await connection.QueryAsync<TMatShape>("ERP_TMatShape_GetMatShapeIDByMatShape"
                                                , queryParameters
                                                , commandTimeout: 1500
                                                , commandType: CommandType.StoredProcedure);
                                            if (data.FirstOrDefault()!=null)
                                            {
                                                ob.MatShapeID = data.FirstOrDefault().MatShapeID;
                                            }
                                        }
                                    }

                                    ob.MatStdW = string.IsNullOrEmpty(worksheet.Cells[row, MatStdW].Value.ToString()) ? (float?)null : float.Parse(worksheet.Cells[row, MatStdW].Value.ToString());
                                    ob.MatStdH = string.IsNullOrEmpty(worksheet.Cells[row, MatStdH].Value.ToString()) ? (float?)null : float.Parse(worksheet.Cells[row, MatStdH].Value.ToString());
                                    ob.MatStdUnit = worksheet.Cells[row, MatStdUnit].Value.ToString();
                                    ob.MfgVendor = worksheet.Cells[row, MfgVendor].Value.ToString();
                                    ob.Shipper = worksheet.Cells[row, Shipper].Value.ToString();
                                    ob.PUnit = worksheet.Cells[row, PUnit].Value.ToString();
                                    ob.PGrpCD = worksheet.Cells[row, PGrpCD].Value.ToString();
                                    ob.PLeadtime = short.Parse(worksheet.Cells[row, PLeadtime].Value.ToString());
                                    ob.MOQ = string.IsNullOrEmpty(worksheet.Cells[row, MOQ].Value.ToString()) ? (float?)null : float.Parse(worksheet.Cells[row, MOQ].Value.ToString());
                                    ob.PLotSize = string.IsNullOrEmpty(worksheet.Cells[row, PurchaseLotSize].Value.ToString()) ? (int?)null : int.Parse(worksheet.Cells[row, PurchaseLotSize].Value.ToString());
                                    ob.FreightRate = string.IsNullOrEmpty(worksheet.Cells[row, FreightRate].Value.ToString()) ? (float?)null : float.Parse(worksheet.Cells[row, FreightRate].Value.ToString());
                                    ob.PriceLotSize = string.IsNullOrEmpty(worksheet.Cells[row, PriceLotSize].Value.ToString()) ? (double?)null : double.Parse(worksheet.Cells[row, PriceLotSize].Value.ToString());
                                    ob.Incoterms = worksheet.Cells[row, Incoterms].Value.ToString();
                                    ob.EXWPrice = string.IsNullOrEmpty(worksheet.Cells[row, EXWPrice].Value.ToString()) ? (double?)null : double.Parse(worksheet.Cells[row, EXWPrice].Value.ToString());
                                    ob.EXWCurr = worksheet.Cells[row, EXWCurr].Value.ToString();
                                    ob.FOBPrice = string.IsNullOrEmpty(worksheet.Cells[row, FOBPrice].Value.ToString()) ? (double?)null : double.Parse(worksheet.Cells[row, FOBPrice].Value.ToString());
                                    ob.FOBCurr = worksheet.Cells[row, FOBCurr].Value.ToString();
                                    ob.FOBcPrice = string.IsNullOrEmpty(worksheet.Cells[row, FOBcPrice].Value.ToString()) ? (double?)null : double.Parse(worksheet.Cells[row, FOBcPrice].Value.ToString());
                                    ob.CIFPrice = string.IsNullOrEmpty(worksheet.Cells[row, CIFPrice].Value.ToString()) ? (double?)null : double.Parse(worksheet.Cells[row, CIFPrice].Value.ToString());
                                    ob.CIFCurr = worksheet.Cells[row, CIFCurr].Value.ToString();

                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, ValidFrom].Value.ToString()))
                                    {
                                        double d = double.Parse(worksheet.Cells[row, ValidFrom].Value.ToString());
                                        DateTime Validdate = DateTime.FromOADate(d).Date;
                                        ob.ValidFrom = Validdate;
                                    }

                                    resultList.Add(ob);
                                }
                                return resultList;
                            }
                        }

                    }
                }
                return resultList;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());

            }
        }
        public async Task<List<TDMat_Custom>> LoadExcelImportAddNewSupportXLS(string Path)
        {
            Initialization();
            List<TDMat_Custom> resultList = new List<TDMat_Custom>();
            FileStream existingFile = new FileStream(HttpContext.Current.Server.MapPath(Path), FileMode.Open);
            var workbook = Workbook.Load(existingFile);
            var worksheet = workbook.Worksheets.FirstOrDefault();
            var cells = worksheet.Cells;

            int MatTypeCD = 0;
            int MatCat = 1;
            int MHName = 2;
            int MatName = 3;
            int Color = 4;
            int MUnit = 5;
            int YUnit = 6;
            int LossRate = 7;
            int Costing = 8;
            int RapMua = 9;
            int MultiSrc = 10;
            int AltMat = 11;
            int PO = 12;
            int GR = 13;
            int Class = 14;
            int UnderDev = 15;
            int MatShapeName = 16;
            int MatStdW = 17;
            int MatStdH = 18;
            int MatStdUnit = 19;
            int MfgVendor = 20;
            int Shipper = 21;
            int PUnit = 22;
            int PGrpCD = 23;
            int PLeadtime = 24;
            int MOQ = 25;
            int PurchaseLotSize = 26;
            int FreightRate = 27;
            int PriceLotSize = 28;
            int Incoterms = 29;
            int EXWPrice = 30;
            int EXWCurr = 31;
            int FOBPrice = 32;
            int FOBCurr = 33;
            int FOBcPrice = 34;
            int CIFPrice = 35;
            int CIFCurr = 36;
            int ValidFrom = 37;

            for (int row = cells.FirstRowIndex + 1; row <= cells.LastRowIndex; row++)
            {
                if (worksheet.Cells[row, MatTypeCD].Value == null || worksheet.Cells[row, MatCat].Value == null || worksheet.Cells[row, MHName].Value == null
                       || worksheet.Cells[row, MatName].Value == null || worksheet.Cells[row, Color].Value == null
                       || worksheet.Cells[row, MUnit].Value == null || worksheet.Cells[row, YUnit].Value == null)
                {
                    continue;
                }
                TDMat_Custom ob = new TDMat_Custom();
                ob.MatTypeCD = worksheet.Cells[row, MatTypeCD].Value.ToString();
                ob.MatCat = worksheet.Cells[row, MatCat].Value.ToString();
                if (!string.IsNullOrEmpty(worksheet.Cells[row, MHName].Value.ToString()))
                {
                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@MHName", worksheet.Cells[row, MHName].Value.ToString());
                        connection.Open();
                        var data = await connection.QueryAsync<TMH>("ERP_TMH_GetMHIDByMHName"
                            , queryParameters
                            , commandTimeout: 1500
                            , commandType: CommandType.StoredProcedure);
                        if (data.FirstOrDefault() != null)
                        {
                            ob.MHID = data.FirstOrDefault().MHID;
                            ob.MHName = worksheet.Cells[row, MHName].Value.ToString();
                        }
                    }
                }
                ob.MatName = worksheet.Cells[row, MatName].Value.ToString();
                ob.Color = worksheet.Cells[row, Color].Value.ToString();
                ob.MUnit = worksheet.Cells[row, MUnit].Value.ToString();
                ob.YUnit = worksheet.Cells[row, YUnit].Value.ToString();
                ob.LossRate = string.IsNullOrEmpty(worksheet.Cells[row, LossRate].Value.ToString()) ? (float?)null : float.Parse(worksheet.Cells[row, LossRate].Value.ToString());
                if (!string.IsNullOrEmpty(worksheet.Cells[row, Costing].Value.ToString()))
                {
                    if (worksheet.Cells[row, Costing].Value.ToString() == "0")
                    {
                        ob.Costing = false;
                    }
                    ob.Costing = true;
                }
                if (!string.IsNullOrEmpty(worksheet.Cells[row, RapMua].Value.ToString()))
                {
                    if (worksheet.Cells[row, RapMua].Value.ToString() == "0")
                    {
                        ob.RapMua = false;
                    }
                    ob.RapMua = true;
                }
                if (!string.IsNullOrEmpty(worksheet.Cells[row, MultiSrc].Value.ToString()))
                {
                    if (worksheet.Cells[row, MultiSrc].Value.ToString() == "0")
                    {
                        ob.MultiSrc = false;
                    }
                    ob.MultiSrc = true;
                }
                if (!string.IsNullOrEmpty(worksheet.Cells[row, AltMat].Value.ToString()))
                {
                    if (worksheet.Cells[row, AltMat].Value.ToString() == "0")
                    {
                        ob.AltMat = false;
                    }
                    ob.AltMat = true;
                }
                if (!string.IsNullOrEmpty(worksheet.Cells[row, PO].Value.ToString()))
                {
                    if (worksheet.Cells[row, PO].Value.ToString() == "0")
                    {
                        ob.PO = false;
                    }
                    ob.PO = true;
                }
                if (!string.IsNullOrEmpty(worksheet.Cells[row, GR].Value.ToString()))
                {
                    if (worksheet.Cells[row, GR].Value.ToString() == "0")
                    {
                        ob.GR = false;
                    }
                    ob.GR = true;
                }
                if (worksheet.Cells[row, Class].Value!=null)
                {
                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@MatClassDesc", worksheet.Cells[row, Class].Value.ToString());
                        connection.Open();
                        var data = await connection.QueryAsync<STMatClass>("ERP_STMatClass_GetMatClassIDByMatClassDesc"
                            , queryParameters
                            , commandTimeout: 1500
                            , commandType: CommandType.StoredProcedure);
                        if (data.FirstOrDefault() != null)
                        {
                            ob.Class = short.Parse(data.FirstOrDefault().MatClassID.ToString());
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, UnderDev].Value)))
                {
                    if (worksheet.Cells[row, UnderDev].Value.ToString() == "0")
                    {
                        ob.UnderDev = false;
                    }
                    ob.UnderDev = true;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, MatShapeName].Value)))
                {
                    using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                    {
                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@MatShape", worksheet.Cells[row, MatShapeName].Value.ToString());
                        connection.Open();
                        var data = await connection.QueryAsync<TMatShape>("ERP_TMatShape_GetMatShapeIDByMatShape"
                            , queryParameters
                            , commandTimeout: 1500
                            , commandType: CommandType.StoredProcedure);
                        if (data.FirstOrDefault() != null)
                        {
                            ob.MatShapeID = data.FirstOrDefault().MatShapeID;
                            ob.MatShapeName = worksheet.Cells[row, MatShapeName].Value.ToString();
                        }
                    }
                }

                ob.MatStdW = worksheet.Cells[row, MatStdW].Value==null || worksheet.Cells[row, MatStdW].Value.ToString() == "" ? (float?)null : float.Parse(worksheet.Cells[row, MatStdW].Value.ToString());
                ob.MatStdH = worksheet.Cells[row, MatStdH].Value==null || worksheet.Cells[row, MatStdH].Value.ToString()=="" ? (float?)null : float.Parse(worksheet.Cells[row, MatStdH].Value.ToString());
                ob.MatStdUnit = worksheet.Cells[row, MatStdUnit].Value==null|| worksheet.Cells[row, MatStdUnit].Value.ToString() == "" ? null : worksheet.Cells[row, MatStdUnit].Value.ToString();
                ob.MfgVendor = worksheet.Cells[row, MfgVendor].Value==null|| worksheet.Cells[row, MfgVendor].Value.ToString()=="" ? null :worksheet.Cells[row, MfgVendor].Value.ToString();
                ob.Shipper = worksheet.Cells[row, Shipper].Value==null || worksheet.Cells[row, Shipper].Value.ToString() == "" ? null : worksheet.Cells[row, Shipper].Value.ToString();
                ob.PUnit = worksheet.Cells[row, PUnit].Value==null || worksheet.Cells[row, PUnit].Value.ToString() == "" ? null: worksheet.Cells[row, PUnit].Value.ToString();
                ob.PGrpCD = worksheet.Cells[row, PGrpCD].Value==null|| worksheet.Cells[row, PGrpCD].Value.ToString() == "" ? null: worksheet.Cells[row, PGrpCD].Value.ToString();
                ob.PLeadtime = worksheet.Cells[row, PLeadtime].Value==null ? (short?)null : short.Parse(worksheet.Cells[row, PLeadtime].Value.ToString());

                ob.MOQ = worksheet.Cells[row, MOQ].Value==null|| worksheet.Cells[row, MOQ].Value.ToString() == "" ? (float?)null : float.Parse(worksheet.Cells[row, MOQ].Value.ToString());
                ob.PLotSize = worksheet.Cells[row, PurchaseLotSize].Value==null|| worksheet.Cells[row, PurchaseLotSize].Value.ToString() == "" ? (int?)null : int.Parse(worksheet.Cells[row, PurchaseLotSize].Value.ToString());
                ob.FreightRate = worksheet.Cells[row, FreightRate].Value==null|| worksheet.Cells[row, FreightRate].Value.ToString() == "" ? (float?)null : float.Parse(worksheet.Cells[row, FreightRate].Value.ToString());

                ob.PriceLotSize = worksheet.Cells[row, PriceLotSize].Value==null || worksheet.Cells[row, PriceLotSize].Value.ToString() == "" ? (double?)null : double.Parse(worksheet.Cells[row, PriceLotSize].Value.ToString());
                ob.Incoterms = worksheet.Cells[row, Incoterms].Value==null|| worksheet.Cells[row, Incoterms].Value.ToString() == "" ? null : worksheet.Cells[row, Incoterms].Value.ToString();
                ob.EXWPrice = worksheet.Cells[row, EXWPrice].Value==null|| worksheet.Cells[row, EXWPrice].Value.ToString() == "" ? (double?)null : double.Parse(worksheet.Cells[row, EXWPrice].Value.ToString());
                ob.EXWCurr = worksheet.Cells[row, EXWCurr].Value==null|| worksheet.Cells[row, EXWCurr].Value.ToString() == "" ? null: worksheet.Cells[row, EXWCurr].Value.ToString();
                ob.FOBPrice = worksheet.Cells[row, FOBPrice].Value==null|| worksheet.Cells[row, FOBPrice].Value.ToString() == "" ? (double?)null : double.Parse(worksheet.Cells[row, FOBPrice].Value.ToString());
                ob.FOBCurr = worksheet.Cells[row, FOBCurr].Value==null|| worksheet.Cells[row, FOBCurr].Value.ToString()=="" ? null: worksheet.Cells[row, FOBCurr].Value.ToString();
                ob.FOBcPrice = worksheet.Cells[row, FOBcPrice].Value==null|| worksheet.Cells[row, FOBcPrice].Value.ToString()=="" ? (double?)null : double.Parse(worksheet.Cells[row, FOBcPrice].Value.ToString());
                ob.CIFPrice = worksheet.Cells[row, CIFPrice].Value==null|| worksheet.Cells[row, CIFPrice].Value.ToString() == "" ? (double?)null : double.Parse(worksheet.Cells[row, CIFPrice].Value.ToString());
                ob.CIFCurr = worksheet.Cells[row, CIFCurr].Value==null|| worksheet.Cells[row, CIFCurr].Value.ToString() == "" ? null: worksheet.Cells[row, CIFCurr].Value.ToString();

                if (!string.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, ValidFrom].Value)))
                {
                    double d = double.Parse(worksheet.Cells[row, ValidFrom].Value.ToString());
                    DateTime Validdate = DateTime.FromOADate(d).Date;
                    ob.ValidFrom = Validdate;
                }
                ob.Spec1 = "";
                ob.Spec2 = "";
                resultList.Add(ob);


            }
            return resultList;
        }
        [HttpPost]
        [Route("ERP_TDMat_SearchingMaterial_GetPageSize")]
        public async Task<IList<Object>> ERP_TDMat_SearchingMaterial_GetPageSize(dynamic item)
        {
            Initialization();
            return await tTDMat_Repository.ERP_TDMat_SearchingMaterial_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TDMat_SearchingMaterial_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TDMat_SearchingMaterial_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "SearchingMaterial_DMat";
                Byte[] bytes = ExportDataTableToExcelFromStore(tTDMat_Repository.ERP_TDMat_SearchingMaterial_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

﻿using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.Season;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TSeasonController : BaseController
    {
        private GenericRepository<TSeason> repository;
        private ITSeason_Repository iTSeason_Repository;
        public TSeasonController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TSeason>(unitOfWork);
            iTSeason_Repository = new TSeason_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TSeason_GetAll")]
        public async Task<IList<TSeason>> ERP_TVendor_GetAll()
        {
            Initialization();
            return await iTSeason_Repository.ERP_TSeason_GetAll();
        }
        [HttpPost]
        [Route("ERP_TSeason_Update")]
        public async Task<Boolean> ERP_TVendor_Update(TSeason item)
        {
            Initialization();
            return await iTSeason_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TSeason_Insert")]
        public async Task<Boolean> ERP_TVendor_Insert(TSeason item)
        {
            Initialization();
            return await iTSeason_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TSeason_Delete")]
        public async Task<Boolean> ERP_TSPStatus_Delete(TSeason item)
        {
            Initialization();
            return await iTSeason_Repository.Delete_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TSeason_BySeasonDesc")]
        public async Task<IList<TSeason>> ERP_TSeason_BySeasonDesc(TSeason item)
        {
            Initialization();
            return await iTSeason_Repository.ERP_TSeason_BySeasonDesc(item);
        }
        
    }
}

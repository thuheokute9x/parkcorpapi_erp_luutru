﻿using Dapper;
using ExcelLibrary.SpreadSheet;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.ERP.ConsItem;
using HR.API.UnitOfWork;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;





namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TConsController : BaseController
    {
        private GenericRepository<TConsItem> repository;
        private GenericRepository<TBOMItem> repositoryTBOMItem;
        private ITConsItem_Repository iTConsItem_Repository;
        public TConsController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TConsItem>(unitOfWork);
            repositoryTBOMItem = new GenericRepository<TBOMItem>(unitOfWork);
            iTConsItem_Repository = new TConsItem_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TConsItem_ConsWithPriceTPinfo_GR")]
        public async Task<IList<TConsItem_Custom>> ERP_TConsItem_ConsWithPriceTPinfo_GR(TConsItem_Custom item)
        {
            Initialization();
            return await iTConsItem_Repository.ERP_TConsItem_ConsWithPriceTPinfo_GR(item);
        }
        [HttpPost]
        [Route("ERP_TConsItem_ConsWithPriceTPinfo_GR_Excel")]
        public HttpResponseMessage ERP_TConsItem_ConsWithPriceTPinfo_GR_Excel(TConsItem_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "ProdInstCons" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTConsItem_Repository.ERP_TConsItem_ConsWithPriceTPinfo_GR_Excel(item), "ProdInstCons", true, false);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TConsItem_ProdPriceMatCostCons_Pagesize")]
        public async Task<IList<TConsItem_Custom>> ERP_TConsItem_ProdPriceMatCostCons_Pagesize(TConsItem_Custom item)
        {
            Initialization();
            return await iTConsItem_Repository.ERP_TConsItem_ProdPriceMatCostCons_Pagesize(item);
        }
        [HttpPost]
        [Route("ERP_TConsItem_ProdPriceMatCostCons_Pagesize_Excel")]
        public HttpResponseMessage ERP_TConsItem_ProdPriceMatCostCons_Pagesize_Excel(TConsItem_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "ProductionPriceforMatCost" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTConsItem_Repository.ERP_TConsItem_ProdPriceMatCostCons_Pagesize_Excel(item), "ProductionPriceforMatCost", true, false);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TPRItem_Amount_Report")]
        public async Task<IList<TConsItem_Custom>> ERP_TPRItem_Amount_Report(TConsItem_Custom item)
        {
            Initialization();
            return await iTConsItem_Repository.ERP_TPRItem_Amount_Report(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_Amount_Report_Excel")]
        public HttpResponseMessage ERP_TPRItem_Amount_Report_Excel(TConsItem_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "ProdOrderAmountForMaterials" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTConsItem_Repository.ERP_TPRItem_Amount_Report_Excel(item), "ProdOrderAmountForMaterials"); ;
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;



            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TConsItem_GenerateTPRItem")]
        public async Task<Result> ERP_TConsItem_GenerateTPRItem(dynamic item)
        {
            Initialization();
            return await iTConsItem_Repository.ERP_TConsItem_GenerateTPRItem(item);
        }
        [HttpPost]
        [Route("ERP_TConsItem_PreCheck")]
        public async Task<Result> ERP_TConsItem_PreCheck(dynamic item)
        {
            Initialization();
            return await iTConsItem_Repository.ERP_TConsItem_PreCheck(item);
        }
        [HttpPost]
        [Route("ERP_TConsItem_PreviewCons")]
        public async Task<List<Object>> ERP_TConsItem_PreviewCons(dynamic item)
        {
            Initialization();
            return await iTConsItem_Repository.ERP_TConsItem_PreviewCons(item);
        }


        [HttpPost]
        [Route("ERP_TConsItem_PreviewCons_Excel")]
        public HttpResponseMessage ERP_TConsItem_PreviewCons_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "PreviewCons" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTConsItem_Repository.ERP_TConsItem_PreviewCons_Excel(item), "PreviewCons"); ;
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TConsItem_PreviewCons_GetHeader")]
        public async Task<List<Object>> ERP_TConsItem_PreviewCons_GetHeader(dynamic item)
        {
            Initialization();
            return await iTConsItem_Repository.ERP_TConsItem_PreviewCons_GetHeader(item);
        }
        [HttpPost]
        [Route("ERP_TConsItem_GetPageSize")]
        public async Task<List<Object>> ERP_TConsItem_GetPageSize(dynamic item)
        {
            Initialization();
            return await iTConsItem_Repository.ERP_TConsItem_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TConsItem_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TConsItem_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "ConsItem_Excel" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTConsItem_Repository.ERP_TConsItem_GetPageSize_Excel(item), "ConsItem_Excel"); ;
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TConsItem_Lectra_GetPageSize")]
        public async Task<List<Object>> ERP_TConsItem_Lectra_GetPageSize(dynamic item)
        {
            Initialization();
            return await iTConsItem_Repository.ERP_TConsItem_Lectra_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TConsItem_Lectra_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TConsItem_Lectra_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "ConsItem_Lectra_Excel" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTConsItem_Repository.ERP_TConsItem_Lectra_GetPageSize_Excel(item), "ConsItem_Lectra_Excel"); ;
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TConsItem_Import_ExcelFileBOM")]
        public Data ERP_TConsItem_Import_ExcelFileBOM()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            Data kq = new Data();
            using (PARKERPEntities context = new PARKERPEntities())
            {
                using (var transaction = context.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                        {
                            connection.Open();

                            var queryParametersZMsg = new DynamicParameters();
                            string QueryZMsg = "SELECT * FROM dbo.ZMsg (NOLOCK)";
                            var list = connection.Query<ZMsg>(QueryZMsg, queryParametersZMsg, commandTimeout: 1500
                            , commandType: CommandType.Text);
                            List<ZMsg> errlist = list.ToList();

                            var httpRequest = HttpContext.Current.Request;

                            string IsCheckTBTCntValidCntStyleID = httpRequest.Form["IsCheckTBTCntValidCntStyleID"];
                            string IsCheckversionBOMchangenotexist = httpRequest.Form["IsCheckversionBOMchangenotexist"];

                            string Path = "";
                            foreach (string file in httpRequest.Files)
                            {
                                var postedFile = httpRequest.Files[file];
                                if (postedFile != null && postedFile.ContentLength > 0)
                                {
                                    Path = "~/Upload/Excel/TCons"+ postedFile.FileName;
                                    var filePath = HttpContext.Current.Server.MapPath(Path);
                                    postedFile.SaveAs(filePath);
                                }




                                FileStream existingFile = new FileStream(HttpContext.Current.Server.MapPath(Path), FileMode.Open);
                                //if (existingFile.Extension.ToString() == ".xlsx")
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Please import file excel type xls!" });
                                //    kq.Results = resultList;
                                //    kq.Type = 0;
                                //    return kq;
                                //}
                                var workbook = Workbook.Load(existingFile);
                                //Checking Overview
                                var checkWorksheets_Overview = workbook.Worksheets.FirstOrDefault(x => x.Name == "Overview");
                                TBOM item_tbom = new TBOM();
                                short Version = 0;
                                if (checkWorksheets_Overview == null)
                                {
                                    //Overview sheet is not found. The excel file would not be correct.
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = errlist.FirstOrDefault(x => x.MsgID == 143).Msg1 });
                                    //continue; //cai cu
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;
                                }

                                #region "Overview"

                                var cellsOverview = checkWorksheets_Overview.Cells;
                                string Verstring = checkWorksheets_Overview.Cells[3, 4].Value == null ? null : checkWorksheets_Overview.Cells[3, 4].Value.ToString();
                                if (string.IsNullOrEmpty(Verstring) || !short.TryParse(Verstring, out Version))
                                {
                                    //Critiacal information below is missing.: Version in Overview Sheet
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = errlist.FirstOrDefault(x => x.MsgID == 33).Msg1 });
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;
                                }

                                string ActionAddress = checkWorksheets_Overview.Cells[3, 1].Value == null ? null : checkWorksheets_Overview.Cells[3, 1].Value.ToString();
                                if (string.IsNullOrEmpty(Verstring))
                                {
                                    //Critiacal information below is missing.: Action in Overview Sheet
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = errlist.FirstOrDefault(x => x.MsgID == 34).Msg1 });
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;
                                }

                                string tRemark = checkWorksheets_Overview.Cells[2, 3].Value == null ? null : checkWorksheets_Overview.Cells[2, 3].Value.ToString();

                                string tMatGrpCD = checkWorksheets_Overview.Cells[1, 3].Value == null ? null : checkWorksheets_Overview.Cells[1, 3].Value.ToString();
                                var queryParametersTMatGrp = new DynamicParameters();
                                queryParametersTMatGrp.Add("@MatGrpCode", tMatGrpCD);
                                string QueryTMatGrp = "SELECT * FROM dbo.TMatGrp (NOLOCK) WHERE MatGrpCode=@MatGrpCode";
                                TMatGrp matGrp = connection.QueryFirstOrDefault<TMatGrp>(QueryTMatGrp, queryParametersTMatGrp, commandTimeout: 1500
                                , commandType: CommandType.Text);
                                int tMatGrpID = matGrp == null ? 0 : matGrp.MatGrpID;
                                if (tMatGrpID == 0)
                                {
                                    //Critiacal information below is missing.: Mat Group in Overview Sheet
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = errlist.FirstOrDefault(x => x.MsgID == 35).Msg1 });
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;
                                }



                                string brand = checkWorksheets_Overview.Cells[14, 1].Value == null ? null : checkWorksheets_Overview.Cells[14, 1].Value.ToString();
                                if (string.IsNullOrEmpty(brand))
                                {
                                    //Critiacal information below is missing.: Overview Infomations in Overview Table
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = errlist.FirstOrDefault(x => x.MsgID == 36).Msg1 });
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;
                                }


                                #endregion
                                string ListTBOMID = "";
                                int NotTBTCnt = 0;
                                int NotValidCnt = 0;
                                int NoStyleID = 0;
                                int IsCheckToBeTransferred = 0;
                                List<Result> listsheetProdCode = new List<Result>();
                                for (int row = 14; row <= cellsOverview.LastRowIndex; row++)//where từng dòng trên sheet Overview
                                {
                                    string ToBeTransferred = checkWorksheets_Overview.Cells[row, 10].Value == null ? null : checkWorksheets_Overview.Cells[row, 10].Value.ToString();
                                    if (string.IsNullOrEmpty(ToBeTransferred))
                                    {
                                        NotTBTCnt = NotTBTCnt + 1;
                                        continue;
                                    }
                                    IsCheckToBeTransferred = IsCheckToBeTransferred + 1;
                                    string tUpdatedOnstring = checkWorksheets_Overview.Cells[row, 9].Value == null ? null : checkWorksheets_Overview.Cells[row, 9].Value.ToString();
                                    if (!string.IsNullOrEmpty(tUpdatedOnstring))
                                    {
                                        double d = double.Parse(tUpdatedOnstring);
                                        DateTime tUpdatedOn = string.IsNullOrEmpty(tUpdatedOnstring) ? DateTime.Now.AddDays(1) : DateTime.FromOADate(d).Date;
                                        DateTime tValidatedOn = string.IsNullOrEmpty(tUpdatedOnstring) ? DateTime.ParseExact("1900-01-01", "yyyy-mm-dd", CultureInfo.InvariantCulture) : DateTime.FromOADate(d).Date;
                                        if (!(tValidatedOn >= tUpdatedOn))
                                        {
                                            NotValidCnt = NotValidCnt + 1;
                                            continue;
                                        }
                                    }
                                    if (string.IsNullOrEmpty(tUpdatedOnstring))
                                    {
 
                                        NotValidCnt = NotValidCnt + 1;
                                        continue;
                                        
                                    }

                                    string tStyleID = checkWorksheets_Overview.Cells[row, 16].Value == null ? null : checkWorksheets_Overview.Cells[row, 16].Value.ToString();
                                    if (!string.IsNullOrEmpty(tStyleID))
                                    {
                                        string tWs = this.GetBOMSheet(tStyleID, workbook);
                                        if (string.IsNullOrEmpty(tWs))// 'Cannot find StyleID among the bom Sheets
                                        {
                                            NoStyleID = NoStyleID + 1;
                                            //If tWs Is Nothing Then 'Cannot find StyleID among the bom Sheets
                                            //   NoStyleID = NoStyleID + 1
                                            //Else Khong hieu
                                        }
                                        if (!string.IsNullOrEmpty(tWs))// 'Cannot find StyleID among the bom Sheets
                                        {
                                            var checkWorksheets_ProdCode = workbook.Worksheets.FirstOrDefault(x => x.Name == tWs);


                                            listsheetProdCode.Add(new Result { SheetName=tWs });
                                            string ProdCode = checkWorksheets_ProdCode.Cells[2, 7].Value == null ? null : checkWorksheets_ProdCode.Cells[2, 7].Value.ToString();//H3
                                            string DMatCD = checkWorksheets_ProdCode.Cells[6, 7].Value == null ? null : checkWorksheets_ProdCode.Cells[6, 7].Value.ToString();//H7
                                            if (!string.IsNullOrEmpty(ProdCode) && !string.IsNullOrEmpty(DMatCD))
                                            {
                                                for (int y = 7; y <= 21; y++)
                                                {
                                                    string ProdCodeRange = checkWorksheets_ProdCode.Cells[2, y].Value == null ? null : checkWorksheets_ProdCode.Cells[2, y].Value.ToString();//H3
                                                    if (string.IsNullOrEmpty(ProdCodeRange))
                                                    {
                                                        break;
                                                    }

                                                    if (!string.IsNullOrEmpty(ProdCodeRange))
                                                    {
                                                        if (!ProdCodeRange.StartsWith("P"))
                                                        {
                                                            continue;
                                                        }

                                                        TBOM listBom = new TBOM();
                                                        var queryParameters = new DynamicParameters();
                                                        queryParameters.Add("@ProdCode", ProdCodeRange);
                                                        queryParameters.Add("@Version", Version);
                                                        TBOM data = connection.QueryFirstOrDefault<TBOM>(@"SELECT TOP 1 a.BOMID FROM dbo.TBOM (NOLOCK) a INNER JOIN dbo.TProd (NOLOCK) b ON b.ProdID = a.ProdID WHERE b.ProdCode=@ProdCode AND a.Version=@Version", queryParameters, commandTimeout: 1500
                                                            , commandType: CommandType.Text);
                                                        if (data != null)
                                                        {
                                                            ListTBOMID = ListTBOMID + "'" + data.BOMID.ToString() + "'" + ",";


                                                        }










                                                    }

                                                }

                                            }
                                        }
                                    }

                                }

                                if (IsCheckToBeTransferred == 0)
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Please check To Be Transferred in sheet Overview" });//
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;
                                }

                                if (listsheetProdCode.Count > 0 && listsheetProdCode.Count == NotTBTCnt)
                                {
                                    NotTBTCnt = NotTBTCnt + 1;
                                }
                                if (listsheetProdCode.Count == 0)
                                {
                                    //There is no transaction performed or no record with which transaction will be performed.
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = errlist.FirstOrDefault(x => x.MsgID == 38).Msg1 });
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;
                                }
                                int checkTBTCntValidCntStyleID = NotTBTCnt + NotValidCnt + NoStyleID;
                                if (checkTBTCntValidCntStyleID > 0 && IsCheckTBTCntValidCntStyleID == "1")//105
                                {
                                    resultList.Add(new Result
                                    {
                                        ErrorCode = 1,
                                        ErrorMessage = @"Overview checking results as below. 
                                    </BR> </BR> To be transferred unchecked: " + NotTBTCnt.ToString() + " </BR> Validation checking not completed: $NotValidCnt </BR>" +
                                    " </BR> Style ID not found: $NoStyleID </BR></BR> Do you want to continue except it(them)?"
                                    });
                                    kq.Results = resultList;
                                    kq.Type = 2;
                                    return kq;
                                }
                                if (!string.IsNullOrEmpty(ListTBOMID))
                                {
                                    var queryParametersDeleteTBOMItem = new DynamicParameters();
                                    ListTBOMID = ListTBOMID.Remove(ListTBOMID.Length - 1);
                                    string deleteTBOMItem = "DELETE dbo.TBOMItem WHERE BOMID  IN (" + ListTBOMID + ")";
                                    connection.Query<Object>(deleteTBOMItem, queryParametersDeleteTBOMItem, commandTimeout: 1500
                                    , commandType: CommandType.Text);

                                }
                                kq.Results = listsheetProdCode;
                                kq.Type = 1;
                                return kq;



                            }
                            File.Delete(Path);


                            //for (int row = cellsOverview.FirstRowIndex; row <= cellsOverview.LastRowIndex; row++)
                            //{
                            //}
                            //var worksheetlist = workbook.Worksheets.Where(x => x.Name != "Overview" && x.Name != "Order Info"
                            //&& x.Name != "Consumption List" && x.Name != "FormatPARKBOM");

                            //foreach (var worksheet in worksheetlist)
                            //{
                            //    var cells = worksheet.Cells;
                            //    for (int row = cells.FirstRowIndex + 5; row <= cells.LastRowIndex; row++)
                            //    {

                            //        string ProdCode = worksheet.Cells[2, 7].Value.ToString();//MAT #

                            //        TProd tprod = context.TProds.FirstOrDefault(x => x.ProdCode == ProdCode);
                            //        TEmp temp = context.TEmps.FirstOrDefault(x => x.EmpID == int.Parse(GetUserlogin()));

                            //        TBOM tbom = new TBOM();
                            //        tbom.StyleID = tprod.StyleID;
                            //        tbom.ProdID = tprod.ProdID;
                            //        tbom.Version = 0;//////////////
                            //        tbom.BOMRemark = "";
                            //        tbom.MatGrpID = 0;/////////
                            //        tbom.PatternMaker = "";
                            //        tbom.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                            //        tbom.MaterialStaff = "";
                            //        tbom.SampleMaker = "";
                            //        tbom.PatternFileName = "";
                            //        tbom.PatternCreatedOn = DateTime.ParseExact("1899-12-30", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                            //        tbom.BOMUpdatedOn = DateTime.Now;


                            //        tbom.TransferNeeded = false;
                            //        tbom.HasCons = true;
                            //        tbom.PrsMarginFabric = worksheet.Cells[2, 9].Value == null ? default(float) : float.Parse(worksheet.Cells[2, 9].Value.ToString());
                            //        tbom.PrsMarginLeather = worksheet.Cells[3, 9].Value == null ? default(float) : float.Parse(worksheet.Cells[3, 9].Value.ToString());
                            //        tbom.SampleMaker = "";
                            //        tbom.PatternFileName = "";
                            //        tbom.MaterialStaff = "";
                            //        tbom.SampleMaker = "";
                            //        tbom.PatternFileName = "";



                            //        string DMatCD_MAT = worksheet.Cells[row, 6].Value.ToString();//MAT #
                            //        string DMatCD_TIM = worksheet.Cells[row, 7].Value.ToString();//TIM #
                            //        string MATERIALDESC = worksheet.Cells[row, 8].Value.ToString();
                            //        string POSITION = worksheet.Cells[row, 9].Value.ToString();
                            //        string MeasureUnit = worksheet.Cells[row, 10].Value.ToString();
                            //        string LONG_WEIGHT_BAGS_UNIT = worksheet.Cells[row, 11].Value.ToString();
                            //        string WIDE = worksheet.Cells[row, 12].Value.ToString();
                            //        string EA = worksheet.Cells[row, 13].Value.ToString();
                            //        string AutomaticCalculatedYield = worksheet.Cells[row, 14].Value.ToString();
                            //        string ManualYield = worksheet.Cells[row, 15].Value.ToString();
                            //        string YieldUnit = worksheet.Cells[row, 16].Value.ToString();
                            //        string PatternNo = worksheet.Cells[row, 17].Value.ToString();
                            //        string PRS = worksheet.Cells[row, 18].Value.ToString();
                            //        string LECTRA = worksheet.Cells[row, 19].Value.ToString();
                            //        string ROL = worksheet.Cells[row, 20].Value.ToString();
                            //        string NEST = worksheet.Cells[row, 21].Value.ToString();
                            //        string RAPMUA = worksheet.Cells[row, 22].Value.ToString();
                            //        string RC_L = worksheet.Cells[row, 23].Value.ToString();
                            //        string RC_S = worksheet.Cells[row, 24].Value.ToString();
                            //        string REMARK = worksheet.Cells[row, 25].Value.ToString();
                            //        string PROCESS = worksheet.Cells[row, 26].Value.ToString();




                            //        if (worksheet.Cells[row, 0].Value == null || worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null
                            //            || worksheet.Cells[row, 3].Value == null)
                            //        {
                            //            continue;
                            //        }
                            //        decimal LossRate;
                            //        if (!decimal.TryParse(worksheet.Cells[row, 3].Value.ToString() == "" ? "0" : worksheet.Cells[row, 3].Value.ToString(), out LossRate))
                            //        {
                            //            resultList.Add(new Result { ErrorCode = row, ErrorMessage = "LossRate row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not number!" });
                            //            continue;
                            //        }

                            //        string RefVKNot = worksheet.Cells[row, 0].Value.ToString().Trim();
                            //        string MatCD = worksheet.Cells[row, 2].Value.ToString().Trim();

                            //        //if (TPRView.Where(x => x.ProdInstCD.Contains(RefVKNo) && x.MatCD.Contains(MatCD) && x.ProdCode.Contains(ProdCode)).Count() == 0)
                            //        //{
                            //        //    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Not find ProdCode " + ProdCode + " MatCD " + MatCD + " of " + RefVKNo + " in Parkerp" });
                            //        //    continue;
                            //        //}
                            //        //ListInsert.Add(new TTMatLoss { ProdInstCD = RefVKNo, MatCD = MatCD, ProdCode = ProdCode, LossRate = LossRate });

                            //    }
                            //}


                        }
                        kq.Results = resultList;
                        kq.Type = 0;
                        return kq;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new NotImplementedException(ex.Message.ToString());

                    }
                }
            }

        }
        [HttpPost]
        [Route("ERP_TConsItem_Import_ExcelFileBOM_CheckOneSheet")]
        public Data ERP_TConsItem_Import_ExcelFileBOM_CheckOneSheet()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            Data kq = new Data();
            using (PARKERPEntities context = new PARKERPEntities())
            {
                using (var transaction = context.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                        {
                            connection.Open();

                            var queryParametersZMsg = new DynamicParameters();
                            string QueryZMsg = "SELECT * FROM dbo.ZMsg (NOLOCK)";
                            var list = connection.Query<ZMsg>(QueryZMsg, queryParametersZMsg, commandTimeout: 1500
                            , commandType: CommandType.Text);
                            List<ZMsg> errlist = list.ToList();

                            var httpRequest = HttpContext.Current.Request;

                            string SheetProdCode = httpRequest.Form["SheetProdCode"];
                            string FirstTranfer = httpRequest.Form["FirstTranfer"];
                            string IsCheckversionBOMchangenotexist = httpRequest.Form["IsCheckversionBOMchangenotexist"];
                            string FileName = httpRequest.Form["FileName"];

                            string Path = "";


                            Path = "~/Upload/Excel/TCons" + FileName;
                            //if (FirstTranfer=="true")
                            //{
                            //    var filePath = HttpContext.Current.Server.MapPath(Path);
                            //    postedFile.SaveAs(filePath);
                            //}

                                


                                FileStream existingFile = new FileStream(HttpContext.Current.Server.MapPath(Path), FileMode.Open);
                                //if (existingFile.Extension.ToString() == ".xlsx")
                                //{
                                //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Please import file excel type xls!" });
                                //    kq.Results = resultList;
                                //    kq.Type = 0;
                                //    return kq;
                                //}
                                var workbook = Workbook.Load(existingFile);
                                var checkWorksheets_Overview = workbook.Worksheets.FirstOrDefault(x => x.Name == "Overview");
                                string ActionAddress = checkWorksheets_Overview.Cells[3, 1].Value == null ? null : checkWorksheets_Overview.Cells[3, 1].Value.ToString();
                                string Verstring = checkWorksheets_Overview.Cells[3, 4].Value == null ? null : checkWorksheets_Overview.Cells[3, 4].Value.ToString();
                                short Version = short.Parse(Verstring);



                                var WorksheetName = workbook.Worksheets.FirstOrDefault(x => x.Name == SheetProdCode);

                                //int TBOMID = 0;
                                //int Row = 0;
                                //string DMatCDCheck = "";
                                //tStyleID = CLng(RngOVRow.Cells(SysOpt("StyleIDColOffset") + 1).Value)
                                //If tStyleID<> "" Then
                                //   Set tWs = Nothing
                                //    Set tWs = GetBOMSheet(tStyleID, tWB)
                                //    If tWs Is Nothing Then 'Cannot find StyleID among the bom Sheets
                                //        If Msg(29, vbOKCancel + vbQuestion, "$OverviewRowNo", i - 1) <> vbOK Then
                                //            TransferBOM = Replace(GetMsg(29), "$OverviewRowNo", i - 1) & ":Canceled"
                                //            GoTo GoRollback
                                //        End If
                                //    Else

                                string ProdCode = WorksheetName.Cells[2, 7].Value == null ? null : WorksheetName.Cells[2, 7].Value.ToString();//H3
                                string DMatCD = WorksheetName.Cells[6, 7].Value == null ? null : WorksheetName.Cells[6, 7].Value.ToString();//H7
                                //k hieu k lam
                                //BOMItemCnt = tWs.Range(RngDMatCD, RngDMatCD.End(-4121)).Cells.Count '-4121=xlDown
                                //If BOMItemCnt = 1 And RngDMatCD.Value & "" = "" Then
                                //    BOMItemCnt = 0
                                //    Msg 31, vbExclamation, "$SheetName", tWs.Name
                                //    tAns = Msg(32, vbYesNoCancel + vbQuestion)
                                //Else
                                //    tAns = vbYes
                                //End If    
                                if (!string.IsNullOrEmpty(ProdCode) && !string.IsNullOrEmpty(DMatCD))
                                {
                                    for (int y = 7; y <= 21; y++)
                                    {
                                        string ProdCodeRange = WorksheetName.Cells[2, y].Value == null ? null : WorksheetName.Cells[2, y].Value.ToString();//H3
                                        if (string.IsNullOrEmpty(ProdCodeRange))
                                        {
                                            break;
                                        }

                                        //if (ProdCodeRange.StartsWith("P"))
                                        //{

                                        //}

                                        var queryParametersTProd = new DynamicParameters();
                                        queryParametersTProd.Add("@ProdCode", ProdCodeRange);
                                        string QueryTProd = "SELECT * FROM dbo.TProd (NOLOCK) WHERE ProdCode=@ProdCode";
                                        TProd tprod = connection.QueryFirstOrDefault<TProd>(QueryTProd, queryParametersTProd, commandTimeout: 1500
                                        , commandType: CommandType.Text);

                                        if (tprod == null && y == 7)
                                        {
                                            //$SheetName 시트에 등록되지 않은 Product가 존재합니다.
                                            resultList.Add(new Result { ErrorCode = 0, ErrorMessage = errlist.FirstOrDefault(x => x.MsgID == 44).Msg1.Replace("$SheetName", SheetProdCode) });
                                            kq.Results = resultList;
                                            kq.Type = 0;
                                            return kq;
                                        }
                                        if (string.IsNullOrEmpty(ProdCodeRange))
                                        {
                                            continue;
                                        }
                                        var queryParametersTBOM = new DynamicParameters();
                                        queryParametersTBOM.Add("@ProdID", tprod.ProdID);
                                        queryParametersTBOM.Add("@Version", Version);
                                        string QueryTBOM = "SELECT * FROM dbo.TBOM (NOLOCK) WHERE ProdID=@ProdID AND Version=@Version";
                                        TBOM tbom = connection.QueryFirstOrDefault<TBOM>(QueryTBOM, queryParametersTBOM, commandTimeout: 1500
                                        , commandType: CommandType.Text);

                                        if (ActionAddress == "Create BOM")
                                        {
                                            if (tbom != null)
                                            {
                                                //This version of  BOM of product '$ProdCode' exists already.If you want to replace this vesion of BOM with new one, the 'action' of the 'Overview' sheet should be 'Change BOM'.
                                                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = errlist.FirstOrDefault(x => x.MsgID == 30).Msg1.Replace("$ProdCode", ProdCodeRange) });
                                                kq.Results = resultList;
                                                kq.Type = 0;
                                                return kq;
                                            }
                                            //if (tbom == null)
                                            //{
                                            //    TBOM tBOM = new TBOM();
                                            //    tBOM.StyleID = StyleID;
                                            //    tBOM.ProdID = tprod.ProdID;
                                            //    tBOM.Version = Version;
                                            //    tBOM.MatGrpID = matGrp.MatGrpID;
                                            //    tBOM.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                                            //    double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                                            //    tBOM.UpdatedOn = DateTime.FromOADate(d).Date;
                                            //    TBOMID = tBOM.BOMID;
                                            //}
                                        }
                                        if (ActionAddress == "Change BOM")
                                        {
                                            if (tbom == null && IsCheckversionBOMchangenotexist == "1")//==1 la kiem tras hoi yes no
                                            {
                                                //This version of BOM to be changed not exists in the system.Do you want to create new BOM of the product '$ProdCode'?
                                                resultList.Add(new Result { ErrorCode = 2, ErrorMessage = errlist.FirstOrDefault(x => x.MsgID == 45).Msg1.Replace("$ProdCode", ProdCodeRange) });
                                                kq.Results = resultList;
                                                kq.Type = 2;
                                                return kq;
                                            }
                                            //if (tbom == null && IsCheckversionBOMchangenotexist == "0")//==0 la khong kiem tra va tao moi
                                            //{
                                            //    TBOM tBOM = new TBOM();
                                            //    tBOM.StyleID = StyleID;
                                            //    tBOM.ProdID = tprod.ProdID;
                                            //    tBOM.Version = Version;
                                            //    tBOM.MatGrpID = matGrp.MatGrpID;
                                            //    tBOM.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                                            //    double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                                            //    tBOM.UpdatedOn = DateTime.FromOADate(d).Date;
                                            //    TBOMID = tBOM.BOMID;
                                            //}
                                            //if (tbom != null)
                                            //{
                                            //    tbom.StyleID = StyleID;
                                            //    tbom.ProdID = tprod.ProdID;
                                            //    tbom.Version = Version;
                                            //    tbom.MatGrpID = matGrp.MatGrpID;
                                            //    tbom.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                                            //    double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                                            //    tbom.UpdatedOn = DateTime.FromOADate(d).Date;
                                            //    TBOMID = tbom.BOMID;
                                            //}

                                        }
                                        //On Error Resume Next khong hieu khong lam
                                        //RngOVRow.Cells(SysOpt("TransferredOnColOffset") + 1) = tNow
                                        //If Err = 1004 Then
                                        //    MsgCode = 110
                                        //    GoTo GoRollback
                                        //End If


                                        //var cells = WorksheetName.Cells;
                                        //var queryParametersDeleteTBOMItem = new DynamicParameters();

                                        //string QueryTDMat = "SELECT DMatCD,MatCD,MfgVendor,Shipper FROM dbo.TDMat (NOLOCK)";
                                        //var listTDMat = connection.Query<TDMat>(QueryTDMat, queryParametersDeleteTBOMItem, commandTimeout: 1500
                                        //, commandType: CommandType.Text);
                                        //listTDMat = listTDMat.ToList();


                                        //for (int row = cells.FirstRowIndex + 6; row <= cells.LastRowIndex; row++)
                                        //{


                                        //    string DMatCD_MAT = WorksheetName.Cells[row, y].Value.ToString();//MAT #
                                        //    if (!DMatCD_MAT.StartsWith("D"))
                                        //    {
                                        //        continue;
                                        //    }
                                        //    TDMat TDMat = listTDMat.FirstOrDefault(x => x.DMatCD == DMatCD_MAT);
                                        //    try
                                        //    {
                                        //        Row = row;
                                        //        TBOMItem tBOMItem = new TBOMItem();
                                        //        tBOMItem.BOMID = TBOMID;
                                        //        tBOMItem.BOMItemNo = WorksheetName.Cells[row, 42].Value == null ? null : WorksheetName.Cells[row, 42].Value.ToString();
                                        //        tBOMItem.DMatCD = DMatCD_MAT;
                                        //        tBOMItem.MatCD = TDMat.MatCD;
                                        //        DMatCDCheck = DMatCD_MAT;
                                        //        tBOMItem.Position = WorksheetName.Cells[row, 23].Value == null ? null : WorksheetName.Cells[row, 23].Value.ToString();
                                        //        tBOMItem.PressOnly = (WorksheetName.Cells[row, 32].Value == null ? null : WorksheetName.Cells[row, 32].Value.ToString()) == null ? false : true;
                                        //        tBOMItem.Lectra = (WorksheetName.Cells[row, 33].Value == null ? null : WorksheetName.Cells[row, 33].Value.ToString()) == null ? false : true;
                                        //        tBOMItem.ROL = (WorksheetName.Cells[row, 34].Value == null ? null : WorksheetName.Cells[row, 34].Value.ToString()) == null ? false : true;
                                        //        tBOMItem.NestNo = WorksheetName.Cells[row, 35].Value == null ? null : (int?)int.Parse(WorksheetName.Cells[row, 35].Value.ToString());
                                        //        tBOMItem.RapMua = (WorksheetName.Cells[row, 36].Value == null ? null : WorksheetName.Cells[row, 36].Value.ToString()) == null ? false : true;
                                        //        tBOMItem.RCL = (WorksheetName.Cells[row, 37].Value == null ? null : WorksheetName.Cells[row, 37].Value.ToString()) == null ? false : true;
                                        //        tBOMItem.RCS = (WorksheetName.Cells[row, 38].Value == null ? null : WorksheetName.Cells[row, 38].Value.ToString()) == null ? false : true;
                                        //        tBOMItem.Length = WorksheetName.Cells[row, 25].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 25].Value.ToString());
                                        //        tBOMItem.Width = WorksheetName.Cells[row, 26].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 26].Value.ToString());
                                        //        tBOMItem.Qty = WorksheetName.Cells[row, 27].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 27].Value.ToString());
                                        //        tBOMItem.YieldPrs = WorksheetName.Cells[row, 29].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 29].Value.ToString());
                                        //        tBOMItem.PttnCode = WorksheetName.Cells[row, 31].Value == null ? null : WorksheetName.Cells[row, 31].Value.ToString();
                                        //        tBOMItem.MfgVendor = TDMat.MfgVendor;
                                        //        tBOMItem.Shipper = TDMat.Shipper;
                                        //        tBOMItem.Remark = WorksheetName.Cells[row, 39].Value == null ? null : WorksheetName.Cells[row, 39].Value.ToString();
                                        //    }
                                        //    catch (Exception)
                                        //    {
                                        //        //A BOM Sheet has an error as below. Style ID: $StyleID Row No: $RowNo Dev Material Code: $DMatCD Error: $ErrDesc
                                        //        resultList.Add(new Result
                                        //        {
                                        //            ErrorCode = 0,
                                        //            ErrorMessage = errlist.FirstOrDefault(x => x.MsgID == 70).Msg1.Replace("$StyleID", StyleID.ToString())
                                        //            .Replace("$RowNo", Row.ToString())
                                        //            .Replace("$DMatCD", DMatCDCheck)
                                        //        });
                                        //        kq.Results = resultList;
                                        //        kq.Type = 0;
                                        //        return kq;
                                        //    }                                          
                                        //}
                                    }

                                }

                            

                        }
                        kq.Results = resultList;
                        kq.Type = 1;
                        return kq;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new NotImplementedException(ex.Message.ToString());

                    }
                }
            }

        }
        [HttpPost]
        [Route("ERP_TConsItem_Import_ExcelFileBOM_InserAllOneSheet")]
        public async Task<Data> ERP_TConsItem_Import_ExcelFileBOM_InserAllOneSheet()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            Data kq = new Data();
            using (PARKERPEntities context = new PARKERPEntities())
            {
                using (var transaction = context.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                        {
                            connection.Open();

                            var queryParametersZMsg = new DynamicParameters();
                            string QueryZMsg = "SELECT * FROM dbo.ZMsg (NOLOCK)";
                            var list = await connection.QueryAsync<ZMsg>(QueryZMsg, queryParametersZMsg, commandTimeout: 1500
                            , commandType: CommandType.Text);
                            List<ZMsg> errlist = list.ToList();

                            var httpRequest = HttpContext.Current.Request;
                            string FileName = httpRequest.Form["FileName"];

                            List<string> ListSheetProdCode = httpRequest.Form["ListSheetProdCode"].Split(',').ToList();

                            string Path = "";


                            Path = "~/Upload/Excel/TCons" + FileName;
                            //var filePath = HttpContext.Current.Server.MapPath(Path);
                            //postedFile.SaveAs(filePath);


                            FileStream existingFile = new FileStream(HttpContext.Current.Server.MapPath(Path), FileMode.Open);
                            //if (existingFile.Extension.ToString() == ".xlsx")
                            //{
                            //    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Please import file excel type xls!" });
                            //    kq.Results = resultList;
                            //    kq.Type = 0;
                            //    return kq;
                            //}
                            var workbook = Workbook.Load(existingFile);
                            var checkWorksheets_Overview = workbook.Worksheets.FirstOrDefault(x => x.Name == "Overview");
                            string ActionAddress = checkWorksheets_Overview.Cells[3, 1].Value == null ? null : checkWorksheets_Overview.Cells[3, 1].Value.ToString();
                            string Verstring = checkWorksheets_Overview.Cells[3, 4].Value == null ? null : checkWorksheets_Overview.Cells[3, 4].Value.ToString();
                            short Version = short.Parse(Verstring);
                            int tNoProdDMatCnt = 0;


                            string tMatGrpCD = checkWorksheets_Overview.Cells[1, 3].Value == null ? null : checkWorksheets_Overview.Cells[1, 3].Value.ToString();
                            var queryParametersTMatGrp = new DynamicParameters();
                            queryParametersTMatGrp.Add("@MatGrpCode", tMatGrpCD);
                            string QueryTMatGrp = "SELECT * FROM dbo.TMatGrp (NOLOCK) WHERE MatGrpCode=@MatGrpCode";
                            TMatGrp matGrp = await connection.QueryFirstOrDefaultAsync<TMatGrp>(QueryTMatGrp, queryParametersTMatGrp, commandTimeout: 1500
                            , commandType: CommandType.Text);


                            int Userlogin = int.Parse(GetUserlogin());
                            var queryParametersTEmp = new DynamicParameters();
                            queryParametersTEmp.Add("@EmpID", Userlogin);
                            string QueryTEmp = "SELECT * FROM dbo.TEmp (NOLOCK) WHERE EmpID=@EmpID";
                            TEmp temp = await connection.QueryFirstOrDefaultAsync<TEmp>(QueryTEmp, queryParametersTEmp, commandTimeout: 1500
                            , commandType: CommandType.Text);
                            foreach (string SheetProdCode in ListSheetProdCode)
                            {

                                var WorksheetName = workbook.Worksheets.FirstOrDefault(x => x.Name == SheetProdCode);
                                string tStyleID = WorksheetName.Cells[3, 26].Value == null ? null : WorksheetName.Cells[3, 26].Value.ToString();
                                int StyleID = int.Parse(tStyleID);


                                int TBOMID = 0;
                                int Row = 0;
                                string DMatCDCheck = "";
                                string ProdCode = WorksheetName.Cells[2, 7].Value == null ? null : WorksheetName.Cells[2, 7].Value.ToString();//H3
                                string DMatCD = WorksheetName.Cells[6, 7].Value == null ? null : WorksheetName.Cells[6, 7].Value.ToString();//H7   
                                if (!string.IsNullOrEmpty(ProdCode) && !string.IsNullOrEmpty(DMatCD))
                                {
                                    for (int y = 7; y <= 21; y++)
                                    {
                                        string ProdCodeRange = WorksheetName.Cells[2, y].Value == null ? null : WorksheetName.Cells[2, y].Value.ToString();//H3

                                        if (string.IsNullOrEmpty(ProdCodeRange))
                                        {
                                            break;
                                        }


                                        var queryParametersTProd = new DynamicParameters();
                                        queryParametersTProd.Add("@ProdCode", ProdCodeRange);
                                        string QueryTProd = "SELECT * FROM dbo.TProd (NOLOCK) WHERE ProdCode=@ProdCode";
                                        TProd tprod = connection.QueryFirstOrDefault<TProd>(QueryTProd, queryParametersTProd, commandTimeout: 1500
                                        , commandType: CommandType.Text);


                                        var queryParametersTBOM = new DynamicParameters();
                                        queryParametersTBOM.Add("@ProdID", tprod.ProdID);
                                        queryParametersTBOM.Add("@Version", Version);
                                        string QueryTBOM = "SELECT * FROM dbo.TBOM (NOLOCK) WHERE ProdID=@ProdID AND Version=@Version";
                                        TBOM tbom = connection.QueryFirstOrDefault<TBOM>(QueryTBOM, queryParametersTBOM, commandTimeout: 1500
                                        , commandType: CommandType.Text);
                                        if (ActionAddress == "Create BOM")
                                        {
                                            if (tbom == null)
                                            {
                                                TBOM tBOM = new TBOM();
                                                tBOM.StyleID = StyleID;
                                                tBOM.ProdID = tprod.ProdID;
                                                tBOM.Version = Version;
                                                tBOM.BOMRemark = "";
                                                tBOM.MatGrpID = matGrp.MatGrpID;
                                                tBOM.PatternMaker = "";
                                                tBOM.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                                                tBOM.MaterialStaff = "";
                                                tBOM.SampleMaker = "";
                                                tBOM.PatternFileName = "";
                                                tBOM.PatternCreatedOn = DateTime.ParseExact("1899-12-30", "yyyy-mm-dd", CultureInfo.InvariantCulture);
                                                double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                                                tBOM.BOMUpdatedOn = DateTime.FromOADate(d).Date;
                                                tBOM.HasCons = false;
                                                tBOM.CreatedBy = int.Parse(GetUserlogin());
                                                tBOM.CreatedOn = DateTime.Now;
                                                tBOM.UpdatedOn = DateTime.Now;
                                                context.TBOMs.Add(tBOM);
                                                context.SaveChanges();
                                                TBOMID = tBOM.BOMID;
                                            }
                                        }
                                        if (ActionAddress == "Change BOM")
                                        {
                                            if (tbom == null)//==0 la khong kiem tra va tao moi
                                            {
                                                TBOM tBOM = new TBOM();
                                                tBOM.StyleID = StyleID;
                                                tBOM.ProdID = tprod.ProdID;
                                                tBOM.Version = Version;
                                                tBOM.BOMRemark = "";
                                                tBOM.MatGrpID = matGrp.MatGrpID;
                                                tBOM.PatternMaker = "";
                                                tBOM.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                                                tBOM.MaterialStaff = "";
                                                tBOM.SampleMaker = "";
                                                tBOM.PatternFileName = "";
                                                tBOM.PatternCreatedOn = DateTime.ParseExact("1899-12-30", "yyyy-mm-dd", CultureInfo.InvariantCulture);
                                                double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                                                tBOM.BOMUpdatedOn = DateTime.FromOADate(d).Date;
                                                tBOM.HasCons = false;
                                                tBOM.CreatedBy = int.Parse(GetUserlogin());
                                                tBOM.CreatedOn = DateTime.Now;
                                                tBOM.UpdatedOn = DateTime.Now;
                                                context.TBOMs.Add(tBOM);
                                                context.SaveChanges();
                                                TBOMID = tBOM.BOMID;
                                            }
                                            if (tbom != null)
                                            {
                                                tbom.StyleID = StyleID;
                                                tbom.ProdID = tprod.ProdID;
                                                tbom.Version = Version;
                                                tbom.BOMRemark = "";
                                                tbom.MatGrpID = matGrp.MatGrpID;
                                                tbom.PatternMaker = "";
                                                tbom.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                                                tbom.MaterialStaff = "";
                                                tbom.SampleMaker = "";
                                                tbom.PatternFileName = "";
                                                tbom.PatternCreatedOn = DateTime.ParseExact("1899-12-30", "yyyy-mm-dd", CultureInfo.InvariantCulture);
                                                double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                                                tbom.BOMUpdatedOn = DateTime.FromOADate(d).Date;
                                                tbom.HasCons = false;
                                                tbom.UpdatedBy = int.Parse(GetUserlogin());
                                                tbom.UpdatedOn = DateTime.Now;
                                                context.SaveChanges();
                                                TBOMID = tbom.BOMID;
                                            }

                                        }
                                        //On Error Resume Next khong hieu khong lam
                                        //RngOVRow.Cells(SysOpt("TransferredOnColOffset") + 1) = tNow
                                        //If Err = 1004 Then
                                        //    MsgCode = 110
                                        //    GoTo GoRollback
                                        //End If
                                        var cells = WorksheetName.Cells;


                                        for (int row = cells.FirstRowIndex + 6; row <= cells.LastRowIndex; row++)
                                        {


                                            string DMatCD_MAT = WorksheetName.Cells[row, y].Value.ToString();//MAT #

                                            var queryParametersDeleteTBOMItem = new DynamicParameters();
                                            queryParametersDeleteTBOMItem.Add("@DMatCD", DMatCD_MAT);
                                            string QueryTDMat = "SELECT DMatCD,MatCD,MfgVendor,Shipper FROM dbo.TDMat (NOLOCK) WHERE DMatCD=@DMatCD";
                                            TDMat TDMat = connection.QueryFirstOrDefault<TDMat>(QueryTDMat, queryParametersDeleteTBOMItem, commandTimeout: 1500
                                            , commandType: CommandType.Text);
                                            //listTDMat = listTDMat.ToList();
                                            //TDMat TDMat = listTDMat.FirstOrDefault(x => x.DMatCD == DMatCD_MAT);

                                            if (!DMatCD_MAT.StartsWith("D"))
                                            {
                                                continue;
                                            }
                                            if (TDMat == null || string.IsNullOrEmpty(TDMat.MatCD))
                                            {
                                                tNoProdDMatCnt = tNoProdDMatCnt + 1;
                                                continue;
                                            }

                                            Row = row;
                                            TBOMItem tBOMItem = new TBOMItem();
                                            tBOMItem.BOMID = TBOMID;
                                            tBOMItem.BOMItemNo = WorksheetName.Cells[row, 42].Value == null ? null : WorksheetName.Cells[row, 42].Value.ToString();
                                            tBOMItem.DMatCD = DMatCD_MAT;
                                            tBOMItem.MatCD = TDMat.MatCD;
                                            DMatCDCheck = DMatCD_MAT;
                                            tBOMItem.Position = WorksheetName.Cells[row, 23].Value == null ? null : WorksheetName.Cells[row, 23].Value.ToString();
                                            if (!string.IsNullOrEmpty(tBOMItem.Position) && tBOMItem.Position.Length > 50)
                                            {
                                                tBOMItem.Position = null;
                                            }
                                            tBOMItem.PressOnly = (WorksheetName.Cells[row, 32].Value == null ? null : WorksheetName.Cells[row, 32].Value.ToString()) == null ? false : true;
                                            tBOMItem.Lectra = (WorksheetName.Cells[row, 33].Value == null ? null : WorksheetName.Cells[row, 33].Value.ToString()) == null ? false : true;
                                            tBOMItem.ROL = (WorksheetName.Cells[row, 34].Value == null ? null : WorksheetName.Cells[row, 34].Value.ToString()) == null ? false : true;
                                            tBOMItem.NestNo = WorksheetName.Cells[row, 35].Value == null ? null : (int?)int.Parse(WorksheetName.Cells[row, 35].Value.ToString());
                                            tBOMItem.RapMua = (WorksheetName.Cells[row, 36].Value == null ? null : WorksheetName.Cells[row, 36].Value.ToString()) == null ? false : true;
                                            tBOMItem.RCL = (WorksheetName.Cells[row, 37].Value == null ? null : WorksheetName.Cells[row, 37].Value.ToString()) == null ? false : true;
                                            tBOMItem.RCS = (WorksheetName.Cells[row, 38].Value == null ? null : WorksheetName.Cells[row, 38].Value.ToString()) == null ? false : true;
                                            tBOMItem.Length = WorksheetName.Cells[row, 25].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 25].Value.ToString());
                                            tBOMItem.Width = WorksheetName.Cells[row, 26].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 26].Value.ToString());
                                            tBOMItem.Qty = WorksheetName.Cells[row, 27].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 27].Value.ToString());
                                            tBOMItem.YieldPrs = WorksheetName.Cells[row, 29].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 29].Value.ToString());
                                            tBOMItem.PttnCode = WorksheetName.Cells[row, 31].Value == null ? null : WorksheetName.Cells[row, 31].Value.ToString();
                                            if (!string.IsNullOrEmpty(tBOMItem.PttnCode) && tBOMItem.PttnCode.Length > 15)
                                            {
                                                tBOMItem.PttnCode = null;
                                            }
                                            tBOMItem.ProcCD = "";
                                            tBOMItem.MfgVendor = TDMat.MfgVendor;
                                            tBOMItem.Shipper = TDMat.Shipper;
                                            tBOMItem.Remark = WorksheetName.Cells[row, 39].Value == null ? null : WorksheetName.Cells[row, 39].Value.ToString();
                                            if (!string.IsNullOrEmpty(tBOMItem.Remark) && tBOMItem.Remark.Length > 120)
                                            {
                                                tBOMItem.Remark = null;
                                            }

                                            tBOMItem.CreatedBy = Userlogin;
                                            tBOMItem.CreatedOn = DateTime.Now;
                                            tBOMItem.UpdatedOn = DateTime.Now;
                                            context.TBOMItems.Add(tBOMItem);

                                            //                                            !BOMID = tBOMID
                                            //                                            !BOMItemNo = iRngDMatCD.Offset(0, SysOpt("BOMItemNoColNo")).Value & ""
                                            //                                            !DMatCD = tDMatCD
                                            //                                            !MatCD = tMatCD
                                            //                                            !Position = iRngDMatCD.Offset(0, SysOpt("PositionColNo")).Value & ""
                                            //                                            !PressOnly = iRngDMatCD.Offset(0, SysOpt("PressOnlyColNo")).Value & "" <> ""
                                            //                                            !Lectra = iRngDMatCD.Offset(0, SysOpt("LectraColNo")).Value & "" <> ""
                                            //                                            !ROL = iRngDMatCD.Offset(0, SysOpt("ROLColNo")).Value & "" <> ""
                                            //                                            !RapMua = iRngDMatCD.Offset(0, SysOpt("RapMuaColNo")).Value & "" <> ""
                                            //                                            !NestNo = iRngDMatCD.Offset(0, SysOpt("NestNoColNo")).Value & "" <> ""
                                            //                                            !RCL = iRngDMatCD.Offset(0, SysOpt("RCLColNo")).Value & "" <> ""
                                            //                                            !RCS = iRngDMatCD.Offset(0, SysOpt("RCSColNo")).Value & "" <> ""
                                            //                                            !Length = Val(iRngDMatCD.Offset(0, SysOpt("LengthColNo")))
                                            //                                            !Width = Val(iRngDMatCD.Offset(0, SysOpt("WidthColNo")))
                                            //                                            !QTY = Val(iRngDMatCD.Offset(0, SysOpt("QtyColNo")))
                                            //                                            !YieldPrs = Val(iRngDMatCD.Offset(0, SysOpt("YieldPressColNo")))
                                            //                                            !PttnCode = iRngDMatCD.Offset(0, SysOpt("PttnCodeColNo")).Value & ""
                                            //                                            !ProcCD = IIf(tApplyProcCD, iRngDMatCD.Offset(0, SysOpt("ProcCDColNo")), Null)
                                            //'                                            !Vendor = iRngDMatCD.Offset(0, SysOpt("")) & ""
                                            //'                                            !Shipper = iRngDMatCD.Offset(0, SysOpt("")) & ""
                                            //                                            !Remark = iRngDMatCD.Offset(0, SysOpt("BOMItemRemarkColNo")) & ""
                                            //                                            !CreatedBy = EmpID
                                            //                                            !CreatedOn = tNow
                                            //                                            !UpdatedOn = tNow

                                        }
                                    }
                                }


                                //await ERP_TConsItem_Import_ExcelFileBOM_InserAllOneSheet(SheetProdCode, context, workbook, Version, ActionAddress, matGrp, temp, tNoProdDMatCnt);

                            }
                            //Parallel.ForEach(ListSheetProdCode, async SheetProdCode =>
                            //{
                            //    await ERP_TConsItem_Import_ExcelFileBOM_InserAllOneSheet(SheetProdCode, context, workbook, Version, ActionAddress, matGrp, temp, tNoProdDMatCnt);
                            //});
                            await context.SaveChangesAsync();
                            transaction.Commit();
                            var queryParameters = new DynamicParameters();
                            queryParameters.Add("@FileName", FileName);
                            queryParameters.Add("@CreatedBy", this.GetUserlogin());
                            var GenerateTConsTConsItem = await connection.QueryAsync<TDMat>("ERP_TCons_TConsItem_GenerateTConsTConsItem", queryParameters, commandTimeout: 1500
                            , commandType: CommandType.StoredProcedure);

                            if (tNoProdDMatCnt > 0)
                            {
                                //$Cnt rows those have material not transferred were not uploaded to the BOM.
                                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = errlist.FirstOrDefault(x => x.MsgID == 141).Msg1.Replace("$Cnt", tNoProdDMatCnt.ToString()) });
                                kq.Results = resultList;
                                kq.Type = 3;
                                return kq;
                            }
                            kq.Results = resultList;
                            kq.Type = 1;
                            return kq;
                        }

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new NotImplementedException(ex.Message.ToString());

                    }
                }                
            }

        }
        public async Task ERP_TConsItem_Import_ExcelFileBOM_InserAllOneSheet(string SheetProdCode, PARKERPEntities context,Workbook workbook,short Version
            ,string ActionAddress, TMatGrp matGrp,TEmp temp, int tNoProdDMatCnt)
        {
                var WorksheetName = workbook.Worksheets.FirstOrDefault(x => x.Name == SheetProdCode);
                string tStyleID = WorksheetName.Cells[3, 26].Value == null ? null : WorksheetName.Cells[3, 26].Value.ToString();
                int StyleID = int.Parse(tStyleID);


                int TBOMID = 0;
                int Row = 0;
                string DMatCDCheck = "";
                string ProdCode = WorksheetName.Cells[2, 7].Value == null ? null : WorksheetName.Cells[2, 7].Value.ToString();//H3
                string DMatCD = WorksheetName.Cells[6, 7].Value == null ? null : WorksheetName.Cells[6, 7].Value.ToString();//H7   
                if (!string.IsNullOrEmpty(ProdCode) && !string.IsNullOrEmpty(DMatCD))
                {
                    for (int y = 7; y <= 21; y++)
                    {
                    //string ProdCodeRange = WorksheetName.Cells[2, y].Value == null ? null : WorksheetName.Cells[2, y].Value.ToString();//H3

                    //if (string.IsNullOrEmpty(ProdCodeRange))
                    //{
                    //    break;
                    //}


                    //var queryParametersTProd = new DynamicParameters();
                    //queryParametersTProd.Add("@ProdCode", ProdCodeRange);
                    //string QueryTProd = "SELECT * FROM dbo.TProd (NOLOCK) WHERE ProdCode=@ProdCode";
                    //TProd tprod =await connection.QueryFirstOrDefaultAsync<TProd>(QueryTProd, queryParametersTProd, commandTimeout: 1500
                    //, commandType: CommandType.Text);


                    //var queryParametersTBOM = new DynamicParameters();
                    //queryParametersTBOM.Add("@ProdID", tprod.ProdID);
                    //queryParametersTBOM.Add("@Version", Version);
                    //string QueryTBOM = "SELECT * FROM dbo.TBOM (NOLOCK) WHERE ProdID=@ProdID AND Version=@Version";
                    //TBOM tbom =await connection.QueryFirstOrDefaultAsync<TBOM>(QueryTBOM, queryParametersTBOM, commandTimeout: 1500
                    //, commandType: CommandType.Text);
                    //if (ActionAddress == "Create BOM")
                    //{
                    //    if (tbom == null)
                    //    {
                    //        TBOM tBOM = new TBOM();
                    //        tBOM.StyleID = StyleID;
                    //        tBOM.ProdID = tprod.ProdID;
                    //        tBOM.Version = Version;
                    //        tBOM.BOMRemark = "";
                    //        tBOM.MatGrpID = matGrp.MatGrpID;
                    //        tBOM.PatternMaker = "";
                    //        tBOM.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                    //        tBOM.MaterialStaff = "";
                    //        tBOM.SampleMaker = "";
                    //        tBOM.PatternFileName = "";
                    //        tBOM.PatternCreatedOn = DateTime.ParseExact("1899-12-30", "yyyy-mm-dd", CultureInfo.InvariantCulture);
                    //        double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                    //        tBOM.BOMUpdatedOn = DateTime.FromOADate(d).Date;
                    //        tBOM.HasCons = false;
                    //        tBOM.CreatedBy = int.Parse(GetUserlogin());
                    //        tBOM.CreatedOn = DateTime.Now;
                    //        tBOM.UpdatedOn = DateTime.Now;
                    //        context.TBOMs.Add(tBOM);
                    //        await context.SaveChangesAsync();
                    //        TBOMID = tBOM.BOMID;
                    //    }
                    //}
                    //if (ActionAddress == "Change BOM")
                    //{
                    //    if (tbom == null)//==0 la khong kiem tra va tao moi
                    //    {
                    //        TBOM tBOM = new TBOM();
                    //        tBOM.StyleID = StyleID;
                    //        tBOM.ProdID = tprod.ProdID;
                    //        tBOM.Version = Version;
                    //        tBOM.BOMRemark = "";
                    //        tBOM.MatGrpID = matGrp.MatGrpID;
                    //        tBOM.PatternMaker = "";
                    //        tBOM.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                    //        tBOM.MaterialStaff = "";
                    //        tBOM.SampleMaker = "";
                    //        tBOM.PatternFileName = "";
                    //        tBOM.PatternCreatedOn = DateTime.ParseExact("1899-12-30", "yyyy-mm-dd", CultureInfo.InvariantCulture);
                    //        double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                    //        tBOM.BOMUpdatedOn = DateTime.FromOADate(d).Date;
                    //        tBOM.HasCons = false;
                    //        tBOM.CreatedBy = int.Parse(GetUserlogin());
                    //        tBOM.CreatedOn = DateTime.Now;
                    //        tBOM.UpdatedOn = DateTime.Now;
                    //        context.TBOMs.Add(tBOM);
                    //        await context.SaveChangesAsync();
                    //        TBOMID = tBOM.BOMID;
                    //    }
                    //    if (tbom != null)
                    //    {
                    //        tbom.StyleID = StyleID;
                    //        tbom.ProdID = tprod.ProdID;
                    //        tbom.Version = Version;
                    //        tbom.BOMRemark = "";
                    //        tbom.MatGrpID = matGrp.MatGrpID;
                    //        tbom.PatternMaker = "";
                    //        tbom.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                    //        tbom.MaterialStaff = "";
                    //        tbom.SampleMaker = "";
                    //        tbom.PatternFileName = "";
                    //        tbom.PatternCreatedOn = DateTime.ParseExact("1899-12-30", "yyyy-mm-dd", CultureInfo.InvariantCulture);
                    //        double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                    //        tbom.BOMUpdatedOn = DateTime.FromOADate(d).Date;
                    //        tbom.HasCons = false;
                    //        tbom.UpdatedBy = int.Parse(GetUserlogin());
                    //        tbom.UpdatedOn = DateTime.Now;
                    //        await context.SaveChangesAsync();
                    //        TBOMID = tbom.BOMID;
                    //    }

                    //}
                    ////On Error Resume Next khong hieu khong lam
                    ////RngOVRow.Cells(SysOpt("TransferredOnColOffset") + 1) = tNow
                    ////If Err = 1004 Then
                    ////    MsgCode = 110
                    ////    GoTo GoRollback
                    ////End If
                    //var cells = WorksheetName.Cells;


                    //for (int row = cells.FirstRowIndex + 6; row <= cells.LastRowIndex; row++)
                    //{


                    //    string DMatCD_MAT = WorksheetName.Cells[row, y].Value.ToString();//MAT #

                    //    var queryParametersDeleteTBOMItem = new DynamicParameters();
                    //    queryParametersDeleteTBOMItem.Add("@DMatCD", DMatCD_MAT);
                    //    string QueryTDMat = "SELECT DMatCD,MatCD,MfgVendor,Shipper FROM dbo.TDMat (NOLOCK) WHERE DMatCD=@DMatCD";
                    //    TDMat TDMat =await connection.QueryFirstOrDefaultAsync<TDMat>(QueryTDMat, queryParametersDeleteTBOMItem, commandTimeout: 1500
                    //    , commandType: CommandType.Text);
                    //    //listTDMat = listTDMat.ToList();
                    //    //TDMat TDMat = listTDMat.FirstOrDefault(x => x.DMatCD == DMatCD_MAT);

                    //    if (!DMatCD_MAT.StartsWith("D"))
                    //    {
                    //        continue;
                    //    }
                    //    if (TDMat == null || string.IsNullOrEmpty(TDMat.MatCD))
                    //    {
                    //        tNoProdDMatCnt = tNoProdDMatCnt + 1;
                    //        continue;
                    //    }

                    //    Row = row;
                    //    TBOMItem tBOMItem = new TBOMItem();
                    //    tBOMItem.BOMID = TBOMID;
                    //    tBOMItem.BOMItemNo = WorksheetName.Cells[row, 42].Value == null ? null : WorksheetName.Cells[row, 42].Value.ToString();
                    //    tBOMItem.DMatCD = DMatCD_MAT;
                    //    tBOMItem.MatCD = TDMat.MatCD;
                    //    DMatCDCheck = DMatCD_MAT;
                    //    tBOMItem.Position = WorksheetName.Cells[row, 23].Value == null ? null : WorksheetName.Cells[row, 23].Value.ToString();
                    //    if (!string.IsNullOrEmpty(tBOMItem.Position) && tBOMItem.Position.Length > 50)
                    //    {
                    //        tBOMItem.Position = null;
                    //    }
                    //    tBOMItem.PressOnly = (WorksheetName.Cells[row, 32].Value == null ? null : WorksheetName.Cells[row, 32].Value.ToString()) == null ? false : true;
                    //    tBOMItem.Lectra = (WorksheetName.Cells[row, 33].Value == null ? null : WorksheetName.Cells[row, 33].Value.ToString()) == null ? false : true;
                    //    tBOMItem.ROL = (WorksheetName.Cells[row, 34].Value == null ? null : WorksheetName.Cells[row, 34].Value.ToString()) == null ? false : true;
                    //    tBOMItem.NestNo = WorksheetName.Cells[row, 35].Value == null ? null : (int?)int.Parse(WorksheetName.Cells[row, 35].Value.ToString());
                    //    tBOMItem.RapMua = (WorksheetName.Cells[row, 36].Value == null ? null : WorksheetName.Cells[row, 36].Value.ToString()) == null ? false : true;
                    //    tBOMItem.RCL = (WorksheetName.Cells[row, 37].Value == null ? null : WorksheetName.Cells[row, 37].Value.ToString()) == null ? false : true;
                    //    tBOMItem.RCS = (WorksheetName.Cells[row, 38].Value == null ? null : WorksheetName.Cells[row, 38].Value.ToString()) == null ? false : true;
                    //    tBOMItem.Length = WorksheetName.Cells[row, 25].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 25].Value.ToString());
                    //    tBOMItem.Width = WorksheetName.Cells[row, 26].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 26].Value.ToString());
                    //    tBOMItem.Qty = WorksheetName.Cells[row, 27].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 27].Value.ToString());
                    //    tBOMItem.YieldPrs = WorksheetName.Cells[row, 29].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 29].Value.ToString());
                    //    tBOMItem.PttnCode = WorksheetName.Cells[row, 31].Value == null ? null : WorksheetName.Cells[row, 31].Value.ToString();
                    //    if (!string.IsNullOrEmpty(tBOMItem.PttnCode) && tBOMItem.PttnCode.Length > 15)
                    //    {
                    //        tBOMItem.PttnCode = null;
                    //    }
                    //    tBOMItem.ProcCD = "";
                    //    tBOMItem.MfgVendor = TDMat.MfgVendor;
                    //    tBOMItem.Shipper = TDMat.Shipper;
                    //    tBOMItem.Remark = WorksheetName.Cells[row, 39].Value == null ? null : WorksheetName.Cells[row, 39].Value.ToString();
                    //    if (!string.IsNullOrEmpty(tBOMItem.Remark) && tBOMItem.Remark.Length > 120)
                    //    {
                    //        tBOMItem.Remark = null;
                    //    }

                    //    tBOMItem.CreatedBy = int.Parse(GetUserlogin());
                    //    tBOMItem.CreatedOn = DateTime.Now;
                    //    tBOMItem.UpdatedOn = DateTime.Now;
                    //    context.TBOMItems.Add(tBOMItem);

                    //    //                                            !BOMID = tBOMID
                    //    //                                            !BOMItemNo = iRngDMatCD.Offset(0, SysOpt("BOMItemNoColNo")).Value & ""
                    //    //                                            !DMatCD = tDMatCD
                    //    //                                            !MatCD = tMatCD
                    //    //                                            !Position = iRngDMatCD.Offset(0, SysOpt("PositionColNo")).Value & ""
                    //    //                                            !PressOnly = iRngDMatCD.Offset(0, SysOpt("PressOnlyColNo")).Value & "" <> ""
                    //    //                                            !Lectra = iRngDMatCD.Offset(0, SysOpt("LectraColNo")).Value & "" <> ""
                    //    //                                            !ROL = iRngDMatCD.Offset(0, SysOpt("ROLColNo")).Value & "" <> ""
                    //    //                                            !RapMua = iRngDMatCD.Offset(0, SysOpt("RapMuaColNo")).Value & "" <> ""
                    //    //                                            !NestNo = iRngDMatCD.Offset(0, SysOpt("NestNoColNo")).Value & "" <> ""
                    //    //                                            !RCL = iRngDMatCD.Offset(0, SysOpt("RCLColNo")).Value & "" <> ""
                    //    //                                            !RCS = iRngDMatCD.Offset(0, SysOpt("RCSColNo")).Value & "" <> ""
                    //    //                                            !Length = Val(iRngDMatCD.Offset(0, SysOpt("LengthColNo")))
                    //    //                                            !Width = Val(iRngDMatCD.Offset(0, SysOpt("WidthColNo")))
                    //    //                                            !QTY = Val(iRngDMatCD.Offset(0, SysOpt("QtyColNo")))
                    //    //                                            !YieldPrs = Val(iRngDMatCD.Offset(0, SysOpt("YieldPressColNo")))
                    //    //                                            !PttnCode = iRngDMatCD.Offset(0, SysOpt("PttnCodeColNo")).Value & ""
                    //    //                                            !ProcCD = IIf(tApplyProcCD, iRngDMatCD.Offset(0, SysOpt("ProcCDColNo")), Null)
                    //    //'                                            !Vendor = iRngDMatCD.Offset(0, SysOpt("")) & ""
                    //    //'                                            !Shipper = iRngDMatCD.Offset(0, SysOpt("")) & ""
                    //    //                                            !Remark = iRngDMatCD.Offset(0, SysOpt("BOMItemRemarkColNo")) & ""
                    //    //                                            !CreatedBy = EmpID
                    //    //                                            !CreatedOn = tNow
                    //    //                                            !UpdatedOn = tNow

                    //}
                    await FoearchProdCode(context,y,WorksheetName,Version,ActionAddress,StyleID,matGrp,temp,TBOMID,tNoProdDMatCnt,DMatCDCheck,Row);
                }
                }          
        }
        public async Task FoearchProdCode(PARKERPEntities context,int y, Worksheet WorksheetName, short Version,
            string ActionAddress,int StyleID, TMatGrp matGrp, TEmp temp,int TBOMID, int tNoProdDMatCnt,string DMatCDCheck,int Row)
        {
            string ProdCodeRange = WorksheetName.Cells[2, y].Value == null ? null : WorksheetName.Cells[2, y].Value.ToString();//H3
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                connection.Open();
                if (string.IsNullOrEmpty(ProdCodeRange))
                {
                    return;
                    //break;
                }


                var queryParametersTProd = new DynamicParameters();
                queryParametersTProd.Add("@ProdCode", ProdCodeRange);
                string QueryTProd = "SELECT * FROM dbo.TProd (NOLOCK) WHERE ProdCode=@ProdCode";
                TProd tprod = await connection.QueryFirstOrDefaultAsync<TProd>(QueryTProd, queryParametersTProd, commandTimeout: 1500
                , commandType: CommandType.Text);


                var queryParametersTBOM = new DynamicParameters();
                queryParametersTBOM.Add("@ProdID", tprod.ProdID);
                queryParametersTBOM.Add("@Version", Version);
                string QueryTBOM = "SELECT * FROM dbo.TBOM (NOLOCK) WHERE ProdID=@ProdID AND Version=@Version";
                TBOM tbom = await connection.QueryFirstOrDefaultAsync<TBOM>(QueryTBOM, queryParametersTBOM, commandTimeout: 1500
                , commandType: CommandType.Text);
                if (ActionAddress == "Create BOM")
                {
                    if (tbom == null)
                    {
                        TBOM tBOM = new TBOM();
                        tBOM.StyleID = StyleID;
                        tBOM.ProdID = tprod.ProdID;
                        tBOM.Version = Version;
                        tBOM.BOMRemark = "";
                        tBOM.MatGrpID = matGrp.MatGrpID;
                        tBOM.PatternMaker = "";
                        tBOM.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                        tBOM.MaterialStaff = "";
                        tBOM.SampleMaker = "";
                        tBOM.PatternFileName = "";
                        tBOM.PatternCreatedOn = DateTime.ParseExact("1899-12-30", "yyyy-mm-dd", CultureInfo.InvariantCulture);
                        double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                        tBOM.BOMUpdatedOn = DateTime.FromOADate(d).Date;
                        tBOM.HasCons = false;
                        tBOM.CreatedBy = int.Parse(GetUserlogin());
                        tBOM.CreatedOn = DateTime.Now;
                        tBOM.UpdatedOn = DateTime.Now;
                        context.TBOMs.Add(tBOM);
                        await context.SaveChangesAsync();
                        TBOMID = tBOM.BOMID;
                    }
                }
                if (ActionAddress == "Change BOM")
                {
                    if (tbom == null)//==0 la khong kiem tra va tao moi
                    {
                        TBOM tBOM = new TBOM();
                        tBOM.StyleID = StyleID;
                        tBOM.ProdID = tprod.ProdID;
                        tBOM.Version = Version;
                        tBOM.BOMRemark = "";
                        tBOM.MatGrpID = matGrp.MatGrpID;
                        tBOM.PatternMaker = "";
                        tBOM.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                        tBOM.MaterialStaff = "";
                        tBOM.SampleMaker = "";
                        tBOM.PatternFileName = "";
                        tBOM.PatternCreatedOn = DateTime.ParseExact("1899-12-30", "yyyy-mm-dd", CultureInfo.InvariantCulture);
                        double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                        tBOM.BOMUpdatedOn = DateTime.FromOADate(d).Date;
                        tBOM.HasCons = false;
                        tBOM.CreatedBy = int.Parse(GetUserlogin());
                        tBOM.CreatedOn = DateTime.Now;
                        tBOM.UpdatedOn = DateTime.Now;
                        context.TBOMs.Add(tBOM);
                        await context.SaveChangesAsync();
                        TBOMID = tBOM.BOMID;
                    }
                    if (tbom != null)
                    {
                        tbom.StyleID = StyleID;
                        tbom.ProdID = tprod.ProdID;
                        tbom.Version = Version;
                        tbom.BOMRemark = "";
                        tbom.MatGrpID = matGrp.MatGrpID;
                        tbom.PatternMaker = "";
                        tbom.BOMMaker = temp.EmpNo + " " + temp.EmpName;
                        tbom.MaterialStaff = "";
                        tbom.SampleMaker = "";
                        tbom.PatternFileName = "";
                        tbom.PatternCreatedOn = DateTime.ParseExact("1899-12-30", "yyyy-mm-dd", CultureInfo.InvariantCulture);
                        double d = double.Parse(WorksheetName.Cells[4, 26].Value.ToString());
                        tbom.BOMUpdatedOn = DateTime.FromOADate(d).Date;
                        tbom.HasCons = false;
                        tbom.UpdatedBy = int.Parse(GetUserlogin());
                        tbom.UpdatedOn = DateTime.Now;
                        await context.SaveChangesAsync();
                        TBOMID = tbom.BOMID;
                    }

                }
                //On Error Resume Next khong hieu khong lam
                //RngOVRow.Cells(SysOpt("TransferredOnColOffset") + 1) = tNow
                //If Err = 1004 Then
                //    MsgCode = 110
                //    GoTo GoRollback
                //End If
                var cells = WorksheetName.Cells;
                int fromfor = cells.FirstRowIndex + 6;
                int tofor = cells.LastRowIndex;
                Parallel.For(fromfor, tofor, async row =>
                {
                    await FoearchBomItem(context, y, row, WorksheetName, Version, ActionAddress, matGrp, temp, tNoProdDMatCnt, DMatCDCheck, Row, TBOMID);
                });
                //for (int row = cells.FirstRowIndex + 6; row <= cells.LastRowIndex; row++)
                //{
                //    await FoearchBomItem(context, y, row, WorksheetName, Version, ActionAddress, matGrp, temp, tNoProdDMatCnt, DMatCDCheck, Row, TBOMID);
                //}
                connection.Close();
            }
        }
        public async Task FoearchBomItem(PARKERPEntities context,int y,int row, Worksheet WorksheetName, short Version
    , string ActionAddress, TMatGrp matGrp, TEmp temp, int tNoProdDMatCnt, string DMatCDCheck, int Row,int TBOMID)
        {
            string DMatCD_MAT = WorksheetName.Cells[row, y].Value.ToString();//MAT #
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParametersDeleteTBOMItem = new DynamicParameters();
                    queryParametersDeleteTBOMItem.Add("@DMatCD", DMatCD_MAT);
                    string QueryTDMat = "SELECT DMatCD,MatCD,MfgVendor,Shipper FROM dbo.TDMat (NOLOCK) WHERE DMatCD=@DMatCD";
                    TDMat TDMat = await connection.QueryFirstOrDefaultAsync<TDMat>(QueryTDMat, queryParametersDeleteTBOMItem, commandTimeout: 1500
                    , commandType: CommandType.Text);
                    //listTDMat = listTDMat.ToList();
                    //TDMat TDMat = listTDMat.FirstOrDefault(x => x.DMatCD == DMatCD_MAT);

                    if (!DMatCD_MAT.StartsWith("D"))
                    {
                        return;
                        //continue;
                    }
                    if (TDMat == null || string.IsNullOrEmpty(TDMat.MatCD))
                    {
                        tNoProdDMatCnt = tNoProdDMatCnt + 1;
                        return;
                        //continue;
                    }

                    Row = row;
                    TBOMItem tBOMItem = new TBOMItem();
                    tBOMItem.BOMID = TBOMID;
                    tBOMItem.BOMItemNo = WorksheetName.Cells[row, 42].Value == null ? null : WorksheetName.Cells[row, 42].Value.ToString();
                    tBOMItem.DMatCD = DMatCD_MAT;
                    tBOMItem.MatCD = TDMat.MatCD;
                    DMatCDCheck = DMatCD_MAT;
                    tBOMItem.Position = WorksheetName.Cells[row, 23].Value == null ? null : WorksheetName.Cells[row, 23].Value.ToString();
                    if (!string.IsNullOrEmpty(tBOMItem.Position) && tBOMItem.Position.Length > 50)
                    {
                        tBOMItem.Position = null;
                    }
                    tBOMItem.PressOnly = (WorksheetName.Cells[row, 32].Value == null ? null : WorksheetName.Cells[row, 32].Value.ToString()) == null ? false : true;
                    tBOMItem.Lectra = (WorksheetName.Cells[row, 33].Value == null ? null : WorksheetName.Cells[row, 33].Value.ToString()) == null ? false : true;
                    tBOMItem.ROL = (WorksheetName.Cells[row, 34].Value == null ? null : WorksheetName.Cells[row, 34].Value.ToString()) == null ? false : true;
                    tBOMItem.NestNo = WorksheetName.Cells[row, 35].Value == null ? null : (int?)int.Parse(WorksheetName.Cells[row, 35].Value.ToString());
                    tBOMItem.RapMua = (WorksheetName.Cells[row, 36].Value == null ? null : WorksheetName.Cells[row, 36].Value.ToString()) == null ? false : true;
                    tBOMItem.RCL = (WorksheetName.Cells[row, 37].Value == null ? null : WorksheetName.Cells[row, 37].Value.ToString()) == null ? false : true;
                    tBOMItem.RCS = (WorksheetName.Cells[row, 38].Value == null ? null : WorksheetName.Cells[row, 38].Value.ToString()) == null ? false : true;
                    tBOMItem.Length = WorksheetName.Cells[row, 25].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 25].Value.ToString());
                    tBOMItem.Width = WorksheetName.Cells[row, 26].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 26].Value.ToString());
                    tBOMItem.Qty = WorksheetName.Cells[row, 27].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 27].Value.ToString());
                    tBOMItem.YieldPrs = WorksheetName.Cells[row, 29].Value == null ? null : (float?)float.Parse(WorksheetName.Cells[row, 29].Value.ToString());
                    tBOMItem.PttnCode = WorksheetName.Cells[row, 31].Value == null ? null : WorksheetName.Cells[row, 31].Value.ToString();
                    if (!string.IsNullOrEmpty(tBOMItem.PttnCode) && tBOMItem.PttnCode.Length > 15)
                    {
                        tBOMItem.PttnCode = null;
                    }
                    tBOMItem.ProcCD = "";
                    tBOMItem.MfgVendor = TDMat.MfgVendor;
                    tBOMItem.Shipper = TDMat.Shipper;
                    tBOMItem.Remark = WorksheetName.Cells[row, 39].Value == null ? null : WorksheetName.Cells[row, 39].Value.ToString();
                    if (!string.IsNullOrEmpty(tBOMItem.Remark) && tBOMItem.Remark.Length > 120)
                    {
                        tBOMItem.Remark = null;
                    }

                    tBOMItem.CreatedBy = int.Parse(GetUserlogin());
                    tBOMItem.CreatedOn = DateTime.Now;
                    tBOMItem.UpdatedOn = DateTime.Now;
                    context.TBOMItems.Add(tBOMItem);
                    await context.SaveChangesAsync();


                    //                                            !BOMID = tBOMID
                    //                                            !BOMItemNo = iRngDMatCD.Offset(0, SysOpt("BOMItemNoColNo")).Value & ""
                    //                                            !DMatCD = tDMatCD
                    //                                            !MatCD = tMatCD
                    //                                            !Position = iRngDMatCD.Offset(0, SysOpt("PositionColNo")).Value & ""
                    //                                            !PressOnly = iRngDMatCD.Offset(0, SysOpt("PressOnlyColNo")).Value & "" <> ""
                    //                                            !Lectra = iRngDMatCD.Offset(0, SysOpt("LectraColNo")).Value & "" <> ""
                    //                                            !ROL = iRngDMatCD.Offset(0, SysOpt("ROLColNo")).Value & "" <> ""
                    //                                            !RapMua = iRngDMatCD.Offset(0, SysOpt("RapMuaColNo")).Value & "" <> ""
                    //                                            !NestNo = iRngDMatCD.Offset(0, SysOpt("NestNoColNo")).Value & "" <> ""
                    //                                            !RCL = iRngDMatCD.Offset(0, SysOpt("RCLColNo")).Value & "" <> ""
                    //                                            !RCS = iRngDMatCD.Offset(0, SysOpt("RCSColNo")).Value & "" <> ""
                    //                                            !Length = Val(iRngDMatCD.Offset(0, SysOpt("LengthColNo")))
                    //                                            !Width = Val(iRngDMatCD.Offset(0, SysOpt("WidthColNo")))
                    //                                            !QTY = Val(iRngDMatCD.Offset(0, SysOpt("QtyColNo")))
                    //                                            !YieldPrs = Val(iRngDMatCD.Offset(0, SysOpt("YieldPressColNo")))
                    //                                            !PttnCode = iRngDMatCD.Offset(0, SysOpt("PttnCodeColNo")).Value & ""
                    //                                            !ProcCD = IIf(tApplyProcCD, iRngDMatCD.Offset(0, SysOpt("ProcCDColNo")), Null)
                    //'                                            !Vendor = iRngDMatCD.Offset(0, SysOpt("")) & ""
                    //'                                            !Shipper = iRngDMatCD.Offset(0, SysOpt("")) & ""
                    //                                            !Remark = iRngDMatCD.Offset(0, SysOpt("BOMItemRemarkColNo")) & ""
                    //                                            !CreatedBy = EmpID
                    //                                            !CreatedOn = tNow
                    //                                            !UpdatedOn = tNow
                }
            }
            catch (Exception exception)
            {
                string a = exception.ToString();
            }

        }
        public string GetBOMSheet(string tStyleID, Workbook workbook)
        {
            var worksheetlist = workbook.Worksheets.Where(x => x.Name != "Overview" && x.Name != "Order Info"
                                && x.Name != "Consumption List" && x.Name != "FormatPARKBOM");
            foreach (var worksheet in worksheetlist)
            {
                string tStyleIDcheck = worksheet.Cells[3, 26].Value == null ? null : worksheet.Cells[3, 26].Value.ToString();
                if (tStyleID == tStyleIDcheck)
                {
                    return worksheet.Name;
                }
            }
            return "";
        }
        [HttpPost]
        [Route("ERP_TConsLog_PageSize")]
        public async Task<List<Object>> ERP_TConsLog_PageSize(dynamic item)
        {
            Initialization();
            return await iTConsItem_Repository.ERP_TConsLog_PageSize(item);
        }
        [HttpPost]
        [Route("ERP_TConsLog_PageSize_Excel")]
        public HttpResponseMessage ERP_TConsLog_PageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "ConsLog" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTConsItem_Repository.ERP_TConsLog_PageSize_Excel(item), "ConsLog"); ;
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

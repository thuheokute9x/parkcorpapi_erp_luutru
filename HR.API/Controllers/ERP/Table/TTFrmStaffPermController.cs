﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.FrmStaffPerm;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TTFrmStaffPermController : BaseController
    {
        private GenericRepository<TTFrmStaffPerm> repository;
        private ITTFrmStaffPermRepository frmStaffPer;
        public TTFrmStaffPermController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TTFrmStaffPerm>(unitOfWork);
            frmStaffPer = new TTFrmStaffPermRepository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TTFrmStaffPerm_InsertList")]
        public async Task<IList<TTFrmStaffPerm_Custom>> ERP_TTFrmStaffPerm_InsertList(List<TTFrmStaffPerm> itemList)
        {
            Initialization();
            return await frmStaffPer.ERP_TTFrmStaffPerm_InsertList(itemList);
        }
        [HttpPost]
        [Route("ERP_TTFrmStaffPerm_Get")]
        public async Task<IList<TTFrmStaffPerm_Custom>> ERP_TTFrmStaffPerm_Get(TTFrmStaffPerm_Custom itemt)
        {
            Initialization();
            return await frmStaffPer.ERP_TTFrmStaffPerm_Get(itemt);
        }
        [HttpPost]
        [Route("ERP_TTFrmStaffPerm_Delete")]
        public async Task<bool> ERP_TTFrmStaffPerm_Delete(TTFrmStaffPerm itemt)
        {
            Initialization();
            return await frmStaffPer.ERP_TTFrmStaffPerm_Delete(itemt);
        }
        [HttpPost]
        [Route("ERP_TTFrmStaffPerm_Update")]
        public async Task<bool> ERP_TTFrmStaffPerm_Update(TTFrmStaffPerm itemt)
        {
            Initialization();
            return await frmStaffPer.ERP_TTFrmStaffPerm_Update(itemt);
        }
        [HttpPost]
        [Route("ERT_TTMenuWebERP_GetHierachy")]
        public async Task<IList<TTMenuWebERP_Custom>> ERT_TTMenuWebERP_GetHierachy(TTMenuWebERP item)
        {
            Initialization();
            return await frmStaffPer.ERT_TTMenuWebERP_GetHierachy(item);
        }
        [HttpPost]
        [Route("ERT_TTMenuWebERP_GetHierachyByStaffID")]
        public async Task<IList<TTFrmStaffPerm_Custom>> ERT_TTMenuWebERP_GetHierachyByStaffID(TTMenuWebERP_Custom item)
        {
            Initialization();
            return await frmStaffPer.ERT_TTMenuWebERP_GetHierachyByStaffID(item);
        }
        [HttpPost]
        [Route("ERP_TTFrmStaffPerm_CopyPer")]
        public async Task<bool> ERP_TTFrmStaffPerm_CopyPer(TTFrmStaffPerm_Custom item)
        {
            Initialization();
            return await frmStaffPer.ERP_TTFrmStaffPerm_CopyPer(item);
        }
        [HttpPost]
        [Route("ERT_TTFrmStaffPerm_DeleteByStaffIDMenuID")]
        public async Task<bool> ERT_TTFrmStaffPerm_DeleteByStaffIDMenuID(TTFrmStaffPerm_Custom item)
        {
            Initialization();
            return await frmStaffPer.ERT_TTFrmStaffPerm_DeleteByStaffIDMenuID(item);
        }
    }
}

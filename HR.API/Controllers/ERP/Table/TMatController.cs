﻿using Dapper;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Models.ERP.TempModel.Development;
using HR.API.Repository;
using HR.API.Repository.Mat;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TMatController : BaseController
    {
        private GenericRepository<TMat> repository;
        private ITMatRepository iTMatRepository;
        public TMatController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TMat>(unitOfWork);
            iTMatRepository = new TMatRepository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TMat_Transfer")]
        public async Task<Data> ERP_TMat_Transfer(TDMat_Custom item)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_Transfer(item);
        }
        [HttpPost]
        [Route("ERP_TMat_GetwithMatDescLike")]
        public async Task<IList<TMat_Custom>> ERP_TMat_GetwithMatDescLike(TMat_Custom item)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_GetwithMatDescLike(item);
        }
        [HttpPost]
        [Route("ERP_TMat_GetwithMatCDLike")]
        public async Task<IList<TMat_Basic>> ERP_TMat_GetwithMatCDLike(TMat_Custom item)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_GetwithMatCDLike(item);
        }
        [HttpGet]
        [Route("ERP_TMat_GetMatName")]
        public async Task<IList<TMat>> ERP_TMat_GetMatName(string MatName)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_GetMatName(MatName);
        }
        [HttpPost]
        [Route("ERP_TMat_GetItemByMatCD")]
        public async Task<TMat_Custom> ERP_TMat_GetItemByMatCD(TMat_Custom item)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_GetItemByMatCD(item);
        }
        [HttpPost]
        [Route("ERP_TMat_GetRapMua")]
        public async Task<IList<TMat_Custom>> ERP_TMat_GetRapMua(TMat_Custom item)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_GetRapMua(item);
        }
        [HttpPost]
        [Route("ERP_TMat_GetPageSize")]
        public async Task<IList<TMat_Custom>> ERP_TMat_GetPageSize(TMat_Custom item)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TMat_Update_FromFMat")]
        public async Task<TMat> ERP_TMat_Update_FromFMat(TMat item)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_Update_FromFMat(item);
        }
        [HttpPost]
        [Route("ERP_TMat_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TMat_GetPageSize_Excel(TMat_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "FMat";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTMatRepository.ERP_TMat_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TMat_CheckChangeYUnit")]
        public async Task<int> ERP_TMat_CheckChangeYUnit(TMat item)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_CheckChangeYUnit(item);
        }
        [HttpPost]
        [Route("ERP_TMat_UpdateYUnit")]
        public async Task<Boolean> ERP_TMat_UpdateYUnit(TMat item)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_UpdateYUnit(item);
        }
        [HttpGet]
        [Route("ERP_STMatClass_GetAll")]
        public async Task<IList<STMatClass>> ERP_STMatClass_GetAll()
        {
            Initialization();
            return await iTMatRepository.ERP_STMatClass_GetAll();
        }
        [HttpPost]
        [Route("ERP_TMat_GetMatDescByMatCD")]
        public async Task<TMat> ERP_TMat_GetMatDescByMatCD(TMat item)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_GetMatDescByMatCD(item);
        }
        [HttpPost]
        [Route("ERP_TMat_SearchingMaterial_GetPageSize")]
        public async Task<IList<Object>> ERP_TMat_SearchingMaterial_GetPageSize(dynamic item)
        {
            Initialization();
            return await iTMatRepository.ERP_TMat_SearchingMaterial_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TMat_SearchingMaterial_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TMat_SearchingMaterial_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "SearchingMaterial_TMat";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTMatRepository.ERP_TMat_SearchingMaterial_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

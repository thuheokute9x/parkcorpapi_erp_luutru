﻿using HR.API.Repository;
using HR.API.Models.Entity;
using HR.API.Repository.ERP.BOM;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TBOMController : BaseController
    {
        private GenericRepository<TBOM> repository;
        private ITBOM_Repository iTBOM_Repository;
        public TBOMController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TBOM>(unitOfWork);
            iTBOM_Repository = new TBOM_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TBOM_GetVersionByProdID")]
        public async Task<IList<Object>> ERP_TBOM_GetVersionByProdID(TBOM item)
        {
            Initialization();
            return await iTBOM_Repository.ERP_TBOM_GetVersionByProdID(item);
        }
        [HttpPost]
        [Route("ERP_TBOM_GetPageSize")]
        public async Task<IList<Object>> ERP_TBOM_GetPageSize(dynamic item)
        {
            Initialization();
            return await iTBOM_Repository.ERP_TBOM_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TBOM_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TBOM_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "TBOM";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTBOM_Repository.ERP_TBOM_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TBOMItem_GetPageSize")]
        public async Task<IList<Object>> ERP_TBOMItem_GetPageSize(dynamic item)
        {
            Initialization();
            return await iTBOM_Repository.ERP_TBOMItem_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TBOMItem_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TBOMItem_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "TBOMItem";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTBOM_Repository.ERP_TBOMItem_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TBOM_SearchingProductionByMatCD_GetPageSize")]
        public async Task<IList<Object>> ERP_TBOM_SearchingProductionByMatCD_GetPageSize(dynamic item)
        {
            Initialization();
            return await iTBOM_Repository.ERP_TBOM_SearchingProductionByMatCD_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TBOM_SearchingProductionByMatCD_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TBOM_SearchingProductionByMatCD_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "TBOMItem";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTBOM_Repository.ERP_TBOM_SearchingProductionByMatCD_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

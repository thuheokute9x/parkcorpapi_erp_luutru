﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.Frm;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    //[Authorize]
    [HR.API.App_Start.JwtAuthentication]
    public class ZFrmController : BaseController
    {
        private GenericRepository<ZFrm> repository;
        private IZFrmRepository iZFrmRepository;
        public ZFrmController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<ZFrm>(unitOfWork);
            iZFrmRepository = new ZFrmRepository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_ZFrm_GetWithType")]
        public async Task<IList<ZFrm>> ERP_ZFrm_GetWithType(ZFrm item)
        {
            Initialization();
            return await iZFrmRepository.ERP_ZFrm_GetWithType(item);
        }
        [HttpPost]
        [Route("ERP_ZFrm_GetPageSize")]
        public async Task<IList<ZFrm_Custom>> ERP_ZFrm_GetPageSize(ZFrm_Custom item)
        {
            Initialization();
            return await iZFrmRepository.ERP_ZFrm_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_ZFrm_Update")]
        public async Task<bool> ERP_ZFrm_Update(ZFrm item)
        {
            Initialization();
            return await iZFrmRepository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_ZFrm_InSert")]
        public async Task<bool> ERP_ZFrm_InSert(ZFrm item)
        {
            Initialization();
            return await iZFrmRepository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_ZFrm_Delete")]
        public async Task<bool> ERP_ZFrm_Delete(ZFrm item)
        {
            Initialization();
            return await iZFrmRepository.Delete_SaveChange(item);
        }
        [HttpGet]
        [Route("ERP_ZFrm_GetWithB")]
        public async Task<IList<ZFrm_Custom>> ERP_ZFrm_GetWithB()
        {
            Initialization();
            return await iZFrmRepository.ERP_ZFrm_GetWithB();
        }
    }
}

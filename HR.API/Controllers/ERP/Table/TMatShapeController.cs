﻿using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.MatShape;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TMatShapeController : BaseController
    {
        private GenericRepository<TMatShape> repository;
        private ITMatShape_Repository iTMatShape_Repository;
        public TMatShapeController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TMatShape>(unitOfWork);
            iTMatShape_Repository = new TMatShape_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TMatShape_GetAll")]
        public async Task<IList<TMatShape>> ERP_TMatShape_GetAll()
        {
            Initialization();
            return await iTMatShape_Repository.ERP_TMatShape_GetAll();
        }
        [HttpPost]
        [Route("ERP_TMatShape_Update")]
        public async Task<Boolean> ERP_TMatShape_Update(TMatShape item)
        {
            Initialization();
            return await iTMatShape_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TMatShape_Insert")]
        public async Task<Boolean> ERP_TMatShape_Insert(TMatShape item)
        {
            Initialization();
            return await iTMatShape_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TMatShape_Delete")]
        public async Task<Boolean> ERP_TMatShape_Delete(TMatShape item)
        {
            Initialization();
            return await iTMatShape_Repository.Delete_SaveChange(item);
        }
    }
}

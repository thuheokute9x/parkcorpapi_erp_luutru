﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.Staff;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;



namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class ZStaffController : BaseController
    {
        private GenericRepository<ZStaff> repository;
        private GenericRepository<TXLAutho> repositoryTXLAutho;
        private IZStaffRepository iZStaffRepository;
        public ZStaffController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<ZStaff>(unitOfWork);
            iZStaffRepository = new ZStaffRepository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_ZStaff_UpdatePassWord")]
        public async Task<bool> ERP_ZStaff_UpdatePassWord(ZStaff_Custom item)
        {
            Initialization();
            return await iZStaffRepository.ERP_ZStaff_UpdatePassWord(item);
        }
        [HttpPost]
        [Route("ERP_ZStaff_Update")]
        public async Task<bool> ERP_ZFrm_Update(ZStaff item)
        {
            Initialization();
            return await iZStaffRepository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_ZStaff_InSert")]
        public async Task<bool> ERP_ZFrm_InSert(ZStaff item)
        {
            Initialization();
            return await iZStaffRepository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_ZStaff_GetByID_EmpName_EmpNo")]
        public async Task<IList<ZStaff_Custom>> ERP_ZStaff_GetByID_EmpName_EmpNo(ZStaff_Custom item)
        {
            Initialization();
            return await iZStaffRepository.ERP_ZStaff_GetByID_EmpName_EmpNo(item);
        }
        [HttpPost]
        [Route("ERP_TXLAutho_GetCostSheet")]
        public async Task<IList<TXLAutho_Custom>> ERP_TXLAutho_GetCostSheet(TXLAutho_Custom item)
        {
            Initialization();
            return await iZStaffRepository.ERP_TXLAutho_GetCostSheet(item);
        }
        [HttpPost]
        [Route("ERP_TXLAutho_Insert")]
        public async Task<Boolean> ERP_TXLAutho_Insert(TXLAutho item)
        {

            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                var queryParameters = new DynamicParameters();
                string query = "SELECT EmpID FROM dbo.ZStaff (NOLOCK) WHERE StaffID=" + item.StaffID + "";
                connection.Open();
                var data = await connection.QueryAsync<ZStaff>(query
                    , queryParameters, commandTimeout: 1500
                    , commandType: CommandType.Text);
                if (!data.Any())
                {
                    throw new NotImplementedException("Not find StaffID");
                }
                item.EmpID = data.FirstOrDefault().EmpID;
                unitOfWork = new UnitOfWork<PARKERPEntities>();
                repositoryTXLAutho = new GenericRepository<TXLAutho>(unitOfWork);
                return await repositoryTXLAutho.Insert_SaveChange(item);
            }


        }
        [HttpPost]
        [Route("ERP_TXLAutho_Update")]
        public async Task<Boolean> ERP_TXLAutho_Update(TXLAutho item)
        {
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                var queryParameters = new DynamicParameters();
                string query = "SELECT EmpID FROM dbo.ZStaff (NOLOCK) WHERE StaffID=" + item.StaffID + "";
                connection.Open();
                var data = await connection.QueryAsync<ZStaff>(query
                    , queryParameters, commandTimeout: 1500
                    , commandType: CommandType.Text);
                if (!data.Any())
                {
                    throw new NotImplementedException("Not find StaffID");
                }
                item.EmpID = data.FirstOrDefault().EmpID;
                unitOfWork = new UnitOfWork<PARKERPEntities>();
                repositoryTXLAutho = new GenericRepository<TXLAutho>(unitOfWork);
                return await repositoryTXLAutho.Update_SaveChange(item);
            }
        }
        [HttpPost]
        [Route("ERP_TXLAutho_Delete")]
        public async Task<Boolean> ERP_TXLAutho_Delete(TXLAutho item)
        {
            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
            {
                unitOfWork = new UnitOfWork<PARKERPEntities>();
                repositoryTXLAutho = new GenericRepository<TXLAutho>(unitOfWork);
                return await repositoryTXLAutho.Delete_SaveChange(item);
            }
        }
    }
}

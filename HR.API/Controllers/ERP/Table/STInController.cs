﻿using Dapper;
using HR.API.Repository;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository.In;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STInController : BaseController
    {
        private GenericRepository<STIn> repository;
        private ISTIn_Repository iSTIn_Repository;
        public STInController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STIn>(unitOfWork);
            iSTIn_Repository = new STIn_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_STIn_GetwithIV")]
        public async Task<IList<STIn>> ERP_STIn_GetwithIV(STIn STIn)
        {
            Initialization();
            return await iSTIn_Repository.ERP_STIn_GetwithIV(STIn);
        }
        [HttpPost]
        [Route("ERP_STIn_List_RGL_GetByVenDorSHIPPERIV")]
        public async Task<IList<STIn_List_Custom>> ERP_STIn_List_RGL_GetByVenDorSHIPPERIV(STIn_List_Custom STIn)
        {
            Initialization();
            return await iSTIn_Repository.ERP_STIn_List_RGL_GetByVenDorSHIPPERIV(STIn);
        }
        [HttpPost]
        [Route("ERP_STin_List_GetPriceLast")]
        public async Task<IList<STIn_List_Custom>> ERP_STin_List_GetPriceLast(STIn_List_Custom STIn)
        {
            Initialization();
            return await iSTIn_Repository.ERP_STin_List_GetPriceLast(STIn);
        }
        [HttpPost]
        [Route("ERP_STin_List_GetPriceLastExcel")]
        public HttpResponseMessage ERP_STin_List_GetPriceLastExcel(STIn_List_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "MatPrice" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iSTIn_Repository.ERP_STin_List_GetPriceLastExcel(item), "MatPrice"); 
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_STIn_GetPageSize")]
        public async Task<IList<Object>> ERP_STIn_GetPageSize(dynamic STIn)
        {
            Initialization();
            return await iSTIn_Repository.ERP_STIn_GetPageSize(STIn);
        }
        [HttpPost]
        [Route("ERP_STIn_GetPageSize_Excel")]
        public HttpResponseMessage ERP_STIn_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "STIn" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iSTIn_Repository.ERP_STIn_GetPageSize_Excel(item), "STIn");
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_STIn_List_GetPageSize")]
        public async Task<IList<Object>> ERP_STIn_List_GetPageSize(dynamic STIn)
        {
            Initialization();
            return await iSTIn_Repository.ERP_STIn_List_GetPageSize(STIn);
        }
        [HttpPost]
        [Route("ERP_STPlant_GetPLANTDESCLike")]
        public async Task<IList<Object>> ERP_STPlant_GetPLANTDESCLike(dynamic item)
        {
            Initialization();
            return await iSTIn_Repository.ERP_STPlant_GetPLANTDESCLike(item);
        }
        [HttpPost]
        [Route("ERP_STin_GenerateIV")]
        public async Task<string> ERP_STin_GenerateIV(STIn item)
        {
            Initialization();
            return await iSTIn_Repository.ERP_STin_GenerateIV(item);
        }
        [HttpPost]
        [Route("ERP_STin_Insert")]
        public async Task<bool> ERP_STin_Insert(STIn item)
        {
            Initialization();
            return await iSTIn_Repository.ERP_STin_Insert(item);
        }

    }
}

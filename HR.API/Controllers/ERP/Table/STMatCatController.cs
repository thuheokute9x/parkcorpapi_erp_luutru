﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STMatCatController : BaseController
    {
        [HttpGet]
        [Route("ERP_STMatCat_GetAll")]
        public async Task<IList<STMatCat>> ERP_STMatCat_GetAll()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    string query = "SELECT Name,Remark,Definition FROM dbo.STMatCat (NOLOCK)";
                    var data = await connection.QueryAsync<STMatCat>(query, queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

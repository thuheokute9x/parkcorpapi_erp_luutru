﻿using Dapper;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;



namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STPayTypeController : BaseController
    {
        [HttpGet]
        [Route("ERP_STPayType_GetAll")]
        public async Task<IList<STPayType>> ERP_STPayType_GetAll()
        {
            try
            {
                List<STPayType> result = new List<STPayType>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<STPayType>("SELECT Paytype,PayTypeDesc FROM STPayType (NOLOCK)"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
    }
}

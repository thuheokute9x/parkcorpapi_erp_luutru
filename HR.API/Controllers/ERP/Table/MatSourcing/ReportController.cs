﻿using Dapper;
using HR.API.Models.ERP.Report.StoreViewModel;
using HR.API.Models.ERP.TempModel.MatSourcing;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;



namespace HR.API.Controllers.ERP.Table.MatSourcing
{
    [HR.API.App_Start.JwtAuthentication]
    public class ReportController : BaseController
    {
        [HttpPost]
        [Route("ERP_Report_PaymentCompareList")]
        public async Task<IList<PaymentCompareListModel>> ERP_Report_PaymentCompareList(PaymentCompareListModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Outsourcing", item.Outsourcing);
                    queryParameters.Add("@ProdInstCD", string.IsNullOrEmpty(item.RefVKNo) ? "" : item.RefVKNo);
                    queryParameters.Add("@VendorCD", string.IsNullOrEmpty(item.Vendor) ? "" : item.Vendor);              
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<PaymentCompareListModel>("ERP_Report_PaymentCompareList"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_Report_PaymentCompareListExcel")]
        public HttpResponseMessage ERP_Report_SQMatStockMonthlyExcel(PaymentCompareListModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {

                    DataTable table = new DataTable();
                    SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                    SqlCommand cmd = new SqlCommand("ERP_Report_PaymentCompareList", con);
                    cmd.CommandTimeout = 1500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Outsourcing", SqlDbType.NVarChar).Value = item.Outsourcing;
                    cmd.Parameters.Add("@ProdInstCD", SqlDbType.NVarChar).Value = item.RefVKNo;
                    cmd.Parameters.Add("@VendorCD", SqlDbType.NVarChar).Value = item.Vendor;
                    cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;
                    cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;


                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        da.Fill(table);
                    }

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    string filename = "PaymentCompareList";
                    Byte[] bytes = ExportDataTableToExcelFromStore(table, filename, true, false);
                    response.Content = new ByteArrayContent(bytes);
                    response.Content.Headers.ContentLength = bytes.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

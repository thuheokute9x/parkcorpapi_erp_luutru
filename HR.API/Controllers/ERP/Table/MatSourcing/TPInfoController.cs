﻿using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.ERP.PInfo;
using HR.API.Repository.In;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table.MatSourcing
{
    [HR.API.App_Start.JwtAuthentication]
    public class TPInfoController : BaseController
    {
        private GenericRepository<TPInfo> repository;
        private ITPInfo_Repository iTPInfo_Repository;
        public TPInfoController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TPInfo>(unitOfWork);
            iTPInfo_Repository = new TPInfo_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TPInfo_GetPageSize")]
        public async Task<IList<TPInfo_Custom>> ERP_TPInfo_GetPageSize(TPInfo_Custom item)
        {
            Initialization();
            return await iTPInfo_Repository.ERP_TPInfo_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TPInfo_GetByMatCD_MfgVendor_Shipper")]
        public async Task<IList<TPInfo_Custom>> ERP_TPInfo_GetByMatCD_MfgVendor_Shipper(TPInfo_Custom item)
        {
            Initialization();
            return await iTPInfo_Repository.ERP_TPInfo_GetByMatCD_MfgVendor_Shipper(item);
        }
        [HttpPost]
        [Route("ERP_TPInfo_Update")]
        public async Task<Boolean> ERP_TPInfo_Update(TPInfo item)
        {
            Initialization();
            return await iTPInfo_Repository.ERP_TPInfo_Update(item);
        }
        [HttpPost]
        [Route("ERP_TPInfo_Insert")]
        public async Task<TPInfo> ERP_TPInfo_Insert(TPInfo item)
        {
            Initialization();
            return await iTPInfo_Repository.ERP_TPInfo_Insert(item);
        }
        [HttpPost]
        [Route("ERP_TPInfo_CheckUpdate_Discontinue")]
        public async Task<Result> ERP_TPInfo_CheckUpdate_Discontinue(TPInfo item)
        {
            Initialization();
            return await iTPInfo_Repository.ERP_TPInfo_CheckUpdate_Discontinue(item);
        }
        [HttpPost]
        [Route("ERP_TPInfo_GetPUnitByMatCD")]
        public async Task<IList<Object>> ERP_TPInfo_GetPUnitByMatCD(TPInfo item)
        {
            Initialization();
            return await iTPInfo_Repository.ERP_TPInfo_GetPUnitByMatCD(item);
        }
    }
}

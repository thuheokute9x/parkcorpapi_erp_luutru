﻿using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.ERP.MH;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;



namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TMHController : BaseController
    {
        private GenericRepository<TMH> repository;
        private ITMH_Repository iTMH_Repository;
        public TMHController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TMH>(unitOfWork);
            iTMH_Repository = new TMH_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TMH_ByMHCode")]
        public async Task<IList<TMH>> ERP_TMH_ByMHCode(string MHCode)
        {
            Initialization();
            return await iTMH_Repository.ERP_TMH_ByMHCode(MHCode);
        }
        [HttpPost]
        [Route("ERP_TMH_Update")]
        public async Task<Boolean> ERP_TMH_Update(TMH item)
        {
            Initialization();
            return await iTMH_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TMH_Insert")]
        public async Task<Boolean> ERP_TMH_Insert(TMH item)
        {
            Initialization();
            return await iTMH_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TMH_Delete")]
        public async Task<Boolean> ERP_TMH_Delete(TMH item)
        {
            Initialization();
            return await iTMH_Repository.Delete_SaveChange(item);
        }
    }
}

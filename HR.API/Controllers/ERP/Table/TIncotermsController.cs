﻿using Dapper;
using HR.API.Models.Entity;
using HR.API.Repository.Incoterms;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

using HR.API.Repository;

namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TIncotermsController : BaseController
    {
        private GenericRepository<TIncoterm> repository;
        private ITIncoterms_Repository iTIncoterms_Repository;
        public TIncotermsController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TIncoterm>(unitOfWork);
            iTIncoterms_Repository = new TIncoterms_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TIncoterms_GetAll")]
        public async Task<IList<TIncoterm>> ERP_TIncoterms_GetAll()
        {
            Initialization();
            return await iTIncoterms_Repository.ERP_TIncoterms_GetAll();
        }
        [HttpPost]
        [Route("ERP_TIncoterms_Update")]
        public async Task<Boolean> ERP_TIncoterms_Update(TIncoterm item)
        {
            Initialization();
            return await iTIncoterms_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TIncoterms_Insert")]
        public async Task<Boolean> ERP_TIncoterms_Insert(TIncoterm item)
        {
            Initialization();
            return await iTIncoterms_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TIncoterms_Delete")]
        public async Task<Boolean> ERP_TIncoterms_Delete(TIncoterm item)
        {
            Initialization();
            return await iTIncoterms_Repository.Delete_SaveChange(item);
        }
    }
}

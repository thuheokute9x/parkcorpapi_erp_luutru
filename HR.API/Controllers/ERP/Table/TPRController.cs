﻿using Dapper;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.ProcCat;
using HR.API.Repository.Style;
using HR.API.Repository.TTPR;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TPRController : BaseController
    {
        private GenericRepository<TPR> repository;
        private GenericRepository<TPRProdInst> repositoryTPRProdInst;
        private ITPR_Repository iTPR_Repository;
        public TPRController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TPR>(unitOfWork);
            repositoryTPRProdInst = new GenericRepository<TPRProdInst>(unitOfWork);
            iTPR_Repository = new TPR_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TPR_GetWithRefVKNo")]
        public async Task<IList<TPR>> ERP_TPR_GetWithRefVKNo(string RefVKNo)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPR_GetWithRefVKNo(RefVKNo);
        }
        [HttpPost]
        [Route("ERP_TPR_GetRefVKNoLike")]
        public async Task<IList<Object>> ERP_TPR_GetRefVKNoLike(TPR item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPR_GetRefVKNoLike(item);
        }
        [HttpPost]
        [Route("ERP_TPR_GetWithRefVKNo_ExcludeSP")]
        public async Task<IList<TPR>> ERP_TPR_GetWithRefVKNo_ExcludeSP(TPR item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPR_GetWithRefVKNo_ExcludeSP(item);
        }
        [HttpGet]
        [Route("ERP_TPRStatus_GetTPRStatus")]
        public async Task<IList<TPRStatu>> ERP_TPRStatus_GetTPRStatus()
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRStatus_GetTPRStatus();
        }
        [HttpPost]
        [Route("ERP_TPR_GetQtyTBP_OverpPageSize")]//Lưu ý hàm ERP_TPR_Insert có gọi
        public async Task<IList<TPR_Custom>> ERP_TPR_GetQtyTBP_OverpPageSize(TPR_Custom item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPR_GetQtyTBP_OverpPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TPR_GetQtyTBP_OverpPageSize_Excel")]
        public HttpResponseMessage ERP_TPR_GetQtyTBP_OverpPageSize_Excel(TPR_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "PurchaseRequisite_";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTPR_Repository.ERP_TPR_GetQtyTBP_OverpPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TPR_Insert")]
        public async Task<IList<TPR_Custom>> ERP_TPR_Insert(TPR item)
        {
            Initialization();
            item = await repository.Insert_ReturnOBJ(item);
            return await this.ERP_TPR_GetQtyTBP_OverpPageSize(new TPR_Custom() {
                PRID = 0,StandBy = true,InProcess = true,Pending = true,Completed = false,Canceled = false
                ,CloseOn = false,RefVKNo = "",Remark = "",Type = 0,PageNumber = 1,PageSize = 100
            });
        }
        [HttpPost]
        [Route("ERP_TPR_CheckExistRefVKNo")]
        public async Task<Boolean> ERP_TPR_CheckExistRefVKNo(dynamic item)
        {
            Initialization();
            return await this.iTPR_Repository.ERP_TPR_CheckExistRefVKNo(item);
        }
        [HttpPost]
        [Route("ERP_TPR_Update")]
        public async Task<Boolean> ERP_TPR_Update(TPR item)
        {
            Initialization();
            return await repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TPRProdInst_InsertAndUpdateTPR")]
        public async Task<Result> ERP_TPRProdInst_InsertAndUpdateTPR(TPRProdInst item)
        {
            Initialization();
            Result dt = await iTPR_Repository.ERP_TPR_UpdateRefVKNo(item);
            if (dt.ErrorCode == 1)
            {
                return dt;
            }
            await repositoryTPRProdInst.Insert_SaveChange(item);
            return dt;

        }
        [HttpPost]
        [Route("ERP_TPRItem_GetMaterials")]
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_GetMaterials(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_GetMaterials(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_Radis_Sel_UserLogin_PRID")]
        public async Task<Boolean> ERP_TPRItem_Radis_Sel_UserLogin_PRID(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_Radis_Sel_UserLogin_PRID(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_GetMaterials_Excel")]
        public HttpResponseMessage ERP_TPRItem_GetMaterials_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "PRItem_Materials";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTPR_Repository.ERP_TPRItem_GetMaterials_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }










        [HttpPost]
        [Route("ERP_TMatPOItem_GetMaterialsByMatPOItemID")]
        public async Task<IList<Object>> ERP_TMatPOItem_GetMaterialsByMatPOItemID(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TMatPOItem_GetMaterialsByMatPOItemID(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_Update_PO_AltMat_MfgVendor_Shipper_PGrp_PLotSize_PLeadtime_MOQ_MOQEnabled_StdQty_PUnit_Remark")]
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_PO_AltMat_MfgVendor_Shipper_PGrp_PLotSize_PLeadtime_MOQ_MOQEnabled_StdQty_PUnit_Remark(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_Update_PO_AltMat_MfgVendor_Shipper_PGrp_PLotSize_PLeadtime_MOQ_MOQEnabled_StdQty_PUnit_Remark(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_Update_LeftOverQty")]
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_LeftOverQty(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_Update_LeftOverQty(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_Update_AddQty")]
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_AddQty(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_Update_AddQty(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_Update_LossQty")]
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_LossQty(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_Update_LossQty(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_Update_LossRate")]
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_LossRate(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_Update_LossRate(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_Update_MOQ")]
        public async Task<IList<TPRItem_Custom>> ERP_TPRItem_Update_MOQ(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_Update_MOQ(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_RefreshPOInfo")]
        public async Task<Boolean> ERP_TPRItem_RefreshPOInfo(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_RefreshPOInfo(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_VerityVendor")]
        public async Task<Result> ERP_TPRItem_VerityVendor(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_VerityVendor(item);
        }
        [HttpPost]
        [Route("ERP_TPRProdInst_Delete")]
        public async Task<Boolean> ERP_TPRProdInst_Delete(TPRProdInst item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRProdInst_Delete(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_GetVendorWithPRID")]
        public async Task<IList<Object>> ERP_TPRItem_GetVendorWithPRID(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_GetVendorWithPRID(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_GetShipperWithPRID")]
        public async Task<IList<Object>> ERP_TPRItem_GetShipperWithPRID(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_GetShipperWithPRID(item);
        }
        [HttpPost]
        [Route("ERP_TPRItem_GetPGrpWithPRID")]
        public async Task<IList<Object>> ERP_TPRItem_GetPGrpWithPRID(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPRItem_GetPGrpWithPRID(item);
        }
        [HttpPost]
        [Route("ERP_TPR_UpdateRefVKNo_RefreshVKNo")]
        public async Task<IList<Object>> ERP_TPR_UpdateRefVKNo_RefreshVKNo(dynamic item)
        {
            Initialization();
            return await iTPR_Repository.ERP_TPR_UpdateRefVKNo_RefreshVKNo(item);
        }
    }
}

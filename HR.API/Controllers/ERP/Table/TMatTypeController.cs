﻿using Dapper;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;



namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TMatTypeController : BaseController
    {
        [HttpGet]
        [Route("ERP_TMatType_ByForMat")]
        public async Task<IList<TMatType>> ERP_TMatType_ByForMat(int ForMat)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    string query = "SELECT MatTypeCD,MatTypeDesc,ForProd,ForMat,Discontinue " +
                        "FROM TMatType (NOLOCK) WHERE ForMat="+ ForMat + " AND Discontinue=0";
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<TMatType>(query
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
    }
}

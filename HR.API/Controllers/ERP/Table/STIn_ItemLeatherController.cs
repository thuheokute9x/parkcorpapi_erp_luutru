﻿using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.ERP.In_ItemLeather;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STIn_ItemLeatherController :  BaseController
    {
        private GenericRepository<STIn_ItemLeather> repository;
        private ISTIn_ItemLeather_Repository iSTIn_ItemLeather_Repository;
        public STIn_ItemLeatherController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STIn_ItemLeather>(unitOfWork);
            iSTIn_ItemLeather_Repository = new STIn_ItemLeather_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_Report_STIn_ItemLeather_SFLeatherGI")]
        public async Task<IList<Object>> ERP_Report_STIn_ItemLeather_SFLeatherGI(dynamic item)
        {
            Initialization();
            return await iSTIn_ItemLeather_Repository.ERP_Report_STIn_ItemLeather_SFLeatherGI(item);
        }
        [HttpPost]
        [Route("ERP_Report_STIn_ItemLeather_SFLeatherGI_Excel")]
        public HttpResponseMessage ERP_Report_STIn_ItemLeather_SFLeatherGI_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "SFLeatherGI" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iSTIn_ItemLeather_Repository.ERP_Report_STIn_ItemLeather_SFLeatherGI_Excel(item), "SFLeatherGI");
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_Report_STIn_ItemLeather_SFLeather")]
        public async Task<IList<Object>> ERP_Report_STIn_ItemLeather_SFLeather(dynamic item)
        {
            Initialization();
            return await iSTIn_ItemLeather_Repository.ERP_Report_STIn_ItemLeather_SFLeather(item);
        }
        [HttpPost]
        [Route("ERP_Report_STIn_ItemLeather_SFLeather_Excel")]
        public HttpResponseMessage ERP_Report_STIn_ItemLeather_SFLeather_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "SFLeather" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iSTIn_ItemLeather_Repository.ERP_Report_STIn_ItemLeather_SFLeather_Excel(item), "SFLeather");
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_Report_STIn_ItemLeather_SFLeatherGR")]
        public async Task<IList<Object>> ERP_Report_STIn_ItemLeather_SFLeatherGR(dynamic item)
        {
            Initialization();
            return await iSTIn_ItemLeather_Repository.ERP_Report_STIn_ItemLeather_SFLeatherGR(item);
        }
        [HttpPost]
        [Route("ERP_Report_STIn_ItemLeather_SFLeatherGR_Excel")]
        public HttpResponseMessage ERP_Report_STIn_ItemLeather_SFLeatherGR_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "SFLeatherGR" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(iSTIn_ItemLeather_Repository.ERP_Report_STIn_ItemLeather_SFLeatherGR_Excel(item), "SFLeather");
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_Report_STIn_ItemLeather_GetByIVListNo")]
        public async Task<IList<Object>> ERP_Report_STIn_ItemLeather_GetByIVListNo(dynamic item)
        {
            Initialization();
            return await iSTIn_ItemLeather_Repository.ERP_Report_STIn_ItemLeather_GetByIVListNo(item);
        }
    }
}

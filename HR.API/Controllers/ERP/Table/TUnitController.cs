﻿using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.Unit;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TUnitController : BaseController
    {
        private GenericRepository<TUnit> repository;
        private ITUnit_Repository iTUnit_Repository;
        public TUnitController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TUnit>(unitOfWork);
            iTUnit_Repository = new TUnit_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TUnit_GetAll")]
        public async Task<IList<TUnit>> ERP_TUnit_GetAll()
        {
            Initialization();
            return await iTUnit_Repository.ERP_TUnit_GetAll();
        }
        [HttpPost]
        [Route("ERP_TUnit_Insert")]
        public async Task<Boolean> ERP_TUnit_Insert(TUnit item)
        {
            Initialization();
            return await iTUnit_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TUnit_Update")]
        public async Task<Boolean> ERP_TUnit_Update(TUnit item)
        {
            Initialization();
            return await iTUnit_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TUnit_Delete")]
        public async Task<Boolean> ERP_TUnit_Delete(TUnit item)
        {
            Initialization();
            return await iTUnit_Repository.Delete_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TUnit_ByConvUnit")]
        public async Task<IList<TUnit>> ERP_TUnit_ByConvUnit(TUnit item)
        {
            Initialization();
            return await iTUnit_Repository.ERP_TUnit_ByConvUnit(item);
        }
        [HttpGet]
        [Route("ERP_TTCDUnit_GetAll")]
        public async Task<IList<object>> ERP_TTCDUnit_GetAll()
        {
            Initialization();
            return await iTUnit_Repository.ERP_TTCDUnit_GetAll();
        }
        [HttpPost]
        [Route("ERP_TUnit_GetConvertCDUnit")]
        public async Task<TTCDIIVM> ERP_TUnit_GetConvertCDUnit(TTCDIIVM item)
        {
            Initialization();
            return await iTUnit_Repository.ERP_TUnit_GetConvertCDUnit(item);
        }
    }
}

﻿using Dapper;
using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;



namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STPayDocCDController : BaseController
    {
        [HttpGet]
        [Route("ERP_STPayDocCD_GetAll")]
        public async Task<IList<STPayDocCD>> ERP_STPayDocCD_GetAll()
        {
            try
            {
                List<STPayDocCD> result = new List<STPayDocCD>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    connection.Open();
                    var data = await connection.QueryAsync<STPayDocCD>("SELECT PayDocCD,PayDocSN,PayDocCD +'/ ' +PayDocDesc AS PayDocDesc" +
                        " FROM dbo.STPayDocCD (NOLOCK) ORDER BY PayDocSN ASC"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        //[HttpGet]
        //[Route("ERP_STPayDocCD_GetAll")]
        //public async Task<IList<STPayDoc>> ERP_STPayDocCD_GetAll()
        //{
        //    try
        //    {
        //        List<STPayDocCD> result = new List<STPayDocCD>();
        //        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
        //        {
        //            var queryParameters = new DynamicParameters();
        //            connection.Open();
        //            var data = await connection.QueryAsync<STPayDocCD>("SELECT PayDocCD,PayDocSN,PayDocDesc FROM dbo.STPayDocCD (NOLOCK)"
        //                , queryParameters, commandTimeout: 1500
        //                , commandType: CommandType.Text);
        //            result = data.ToList();
        //        }
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new NotImplementedException(ex.ToString());
        //    }
        //}
    }
}

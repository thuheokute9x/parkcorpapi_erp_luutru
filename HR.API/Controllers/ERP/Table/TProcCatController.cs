﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.ProcCat;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TProcCatController : BaseController
    {
        private GenericRepository<TProcCat> repository;
        private ITProcCat_Repository iTProcCat_Repository;
        public TProcCatController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TProcCat>(unitOfWork);
            iTProcCat_Repository = new TProcCat_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TProcCat_GetAll")]
        public async Task<IList<TProcCat_Custom>> ERP_TProc_GetAll()
        {
            Initialization();
            return await iTProcCat_Repository.ERP_TProcCat_GetAll();
        }
        [HttpGet]
        [Route("ERP_TProcCat_GetProcCatCD")]
        public async Task<IList<TProcCat>> ERP_TProcCat_GetProcCatCD()
        {
            Initialization();
            return await iTProcCat_Repository.ERP_TProcCat_GetProcCatCD();
        }
        [HttpPost]
        [Route("ERP_TProcCat_Update")]
        public async Task<Boolean> ERP_TProc_Update(TProcCat item)
        {
            Initialization();
            return await iTProcCat_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TProcCat_Insert")]
        public async Task<Boolean> ERP_TProc_Insert(TProcCat item)
        {
            Initialization();
            return await iTProcCat_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TProcCat_Delete")]
        public async Task<Boolean> ERP_TProc_Delete(TProcCat item)
        {
            Initialization();
            return await iTProcCat_Repository.Delete_SaveChange(item);
        }
    }
}

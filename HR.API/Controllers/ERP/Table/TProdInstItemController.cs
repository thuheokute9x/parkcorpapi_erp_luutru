﻿using Dapper;
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.ERP.ProdInstItem;
using HR.API.UnitOfWork;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TProdInstItemController : BaseController
    {
        private GenericRepository<TProdInstItem> repository;
        private ITProdInstItem_Repository iTProdInstItem_Repository;
        public TProdInstItemController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TProdInstItem>(unitOfWork);
            iTProdInstItem_Repository = new TProdInstItem_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_ByProdInstCD")]
        public async Task<IList<TProdInstItem_Custom>> ERP_TProdInstItem_ByProdInstCD(TProdInstItem item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TProdInstItem_ByProdInstCD(item);
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_ByProdInstCD_Excel")]
        public async Task<HttpResponseMessage> ERP_TProdInstItem_ByProdInstCD_Excel(TProdInstItem item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "ProdInstItem_ByProdOrder" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                Byte[] bytes = await iTProdInstItem_Repository.ERP_TProdInstItem_ByProdInstCD_Excel(item);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                GC.SuppressFinalize(this);
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_Delete")]
        public async Task<Boolean> ERP_TProdInstItem_Delete(TProdInstItem item)
        {
            Initialization();
            return await iTProdInstItem_Repository.Delete_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_GetByProdInstCD")]
        public async Task<IList<Object>> ERP_TProdInstItem_GetByProdInstCD(dynamic item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TProdInstItem_GetByProdInstCD(item);
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_GetByProdInstCD_Excel")]
        public HttpResponseMessage ERP_TProdInstItem_GetByProdInstCD_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "ProdInstItem";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTProdInstItem_Repository.ERP_TProdInstItem_GetByProdInstCD_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_GetByProdInstCD_TemplateExcel")]
        public HttpResponseMessage ERP_TProdInstItem_GetByProdInstCD_TemplateExcel(dynamic item)
        {
            try
            {
                Initialization();
                DataTable dt= iTProdInstItem_Repository.ERP_TProdInstItem_GetByProdInstCD_Excel(item);

                ExcelPackage excel = new ExcelPackage();
                var workSheet = excel.Workbook.Worksheets.Add("ProdInstCD_TemplateExcel");
                int start = 1;
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    workSheet.Cells[1, start, 1, start].Style.Font.Bold = true;
                    if ((i>9 && i<19) ||i==6)
                    {
                        workSheet.Cells[1, start, 1, start].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        workSheet.Cells[1, start, 1, start].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                    }

                    
                    workSheet.Cells[1, start].Value = dt.Columns[i].ColumnName.Replace("_Format2", "").Replace("_Format4", "").Replace("_Percent0", "").Replace("_Percent2", "").Replace("_Percent3", "")
                    .Replace("_Double0", "").Replace("_Double1", "").Replace("_Double2", "").Replace("_Double3", "").Replace("_Double4", "");
                    //workSheet.Cells["A1:" + Columns[start - 1] + "1"].AutoFilter = true;
                    start++;
                }
                start = 2;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        if (dt.Columns[k].ColumnName.ToString().Contains("_Percent0"))
                        {
                            Double value;
                            bool successfullyParsed = double.TryParse(dt.Rows[i][k].ToString(), out value);
                            if (successfullyParsed)
                            {
                                workSheet.Cells[start + i, k + 1].Value = value / 100;
                            }
                            workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0%";
                            continue;
                        }
                        if (dt.Columns[k].ColumnName.ToString().Contains("_Percent2"))
                        {
                            Double value;
                            bool successfullyParsed = double.TryParse(dt.Rows[i][k].ToString(), out value);
                            if (successfullyParsed)
                            {
                                workSheet.Cells[start + i, k + 1].Value = value / 100;
                            }
                            workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00%";
                            continue;
                        }
                        if (dt.Columns[k].ColumnName.ToString().Contains("_Percent3"))
                        {
                            Double value;
                            bool successfullyParsed = double.TryParse(dt.Rows[i][k].ToString(), out value);
                            if (successfullyParsed)
                            {
                                workSheet.Cells[start + i, k + 1].Value = value / 100;
                            }
                            workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.000%";
                            continue;
                        }
                        if (dt.Columns[k].ColumnName.ToString().Contains("_Format2"))
                        {
                            workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                            workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.00";
                            continue;
                        }
                        if (dt.Columns[k].ColumnName.ToString().Contains("_Format4"))
                        {
                            workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                            workSheet.Cells[start + i, k + 1].Style.Numberformat.Format = "#0.0000";
                            continue;
                        }

                        workSheet.Cells[start + i, k + 1].Value = dt.Rows[i][k];
                        if (k == 15)
                        {
                            workSheet.Column(k + 1).Style.Numberformat.Format = "mm/dd/yyyy";
                        }
                    }
                }



                iTProdInstItem_Repository.ERP_TCust_GetCustIDCustDesc_Excel(excel);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "ProdInstItem";
                Byte[] bytes = excel.GetAsByteArray();
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_DeleteZeroQty_ReturnData")]
        public async Task<IList<Object>> ERP_TProdInstItem_DeleteZeroQty_ReturnData(TProdInstItem_Custom item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TProdInstItem_DeleteZeroQty_ReturnData(item);
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_TSalseInfo_InsertList")]
        public async Task<bool> ERP_TProdInstItem_TSalseInfo_InsertList(List<TProdInstItem> item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TProdInstItem_TSalseInfo_InsertList(item);
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_TSalseInfo_Insert")]
        public async Task<object> ERP_TProdInstItem_TSalseInfo_Insert(dynamic item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TProdInstItem_TSalseInfo_Insert(item);
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_Update")]
        public async Task<bool> ERP_TProdInstItem_Update(TProdInstItem item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TProdInstItem_Update(item);
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_UpdateProdCode")]
        public async Task<Object> ERP_TProdInstItem_UpdateProdCode(TProdInstItem item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TProdInstItem_UpdateProdCode(item);
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_UpdateVersion")]
        public async Task<Object> ERP_TProdInstItem_UpdateVersion(TProdInstItem item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TProdInstItem_UpdateVersion(item);
        }

        [HttpPost]
        [Route("ERP_TSalseInfo_UpdateBuyerPONo")]
        public async Task<Object> ERP_TSalseInfo_UpdateBuyerPONo(TSalseInfo item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TSalseInfo_UpdateBuyerPONo(item);
        }
        [HttpPost]
        [Route("ERP_TSalseInfo_UpdateQty")]
        public async Task<Object> ERP_TSalseInfo_UpdateQty(dynamic item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TSalseInfo_UpdateQty(item);
        }
        [HttpPost]
        [Route("ERP_TSalseInfo_UpdatePrice")]
        public async Task<Object> ERP_TSalseInfo_UpdatePrice(dynamic item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TSalseInfo_UpdatePrice(item);
        }
        [HttpPost]
        [Route("ERP_TSalseInfo_UpdateSoldTo")]
        public async Task<Object> ERP_TSalseInfo_UpdateSoldTo(dynamic item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TSalseInfo_UpdateSoldTo(item);
        }
        [HttpPost]
        [Route("ERP_TSalseInfo_UpdateShipTo")]
        public async Task<Object> ERP_TSalseInfo_UpdateShipTo(dynamic item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TSalseInfo_UpdateShipTo(item);
        }

        [HttpPost]
        [Route("ERP_TSalseInfo_UpdatePCPerCarton")]
        public async Task<Object> ERP_TSalseInfo_UpdatePCPerCarton(dynamic item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TSalseInfo_UpdatePCPerCarton(item);
        }
        [HttpPost]
        [Route("ERP_TSalseInfo_UpdateRemark")]
        public async Task<Object> ERP_TSalseInfo_UpdateRemark(dynamic item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TSalseInfo_UpdateRemark(item);
        }
        [HttpPost]
        [Route("ERP_TSalseInfo_UpdateSKUCodeBuyer")]
        public async Task<Object> ERP_TSalseInfo_UpdateSKUCodeBuyer(dynamic item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TSalseInfo_UpdateSKUCodeBuyer(item);
        }
        [HttpPost]
        [Route("ERP_TSalseInfo_UpdateReqETD")]
        public async Task<Object> ERP_TSalseInfo_UpdateReqETD(dynamic item)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TSalseInfo_UpdateReqETD(item);
        }
        [HttpGet]
        [Route("ERP_TCust_GetAll")]
        public async Task<IList<Object>> ERP_TCust_GetAll()
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TCust_GetAll();
        }
        [HttpPost]
        [Route("ERP_TSalseInfo_Import_Excel")]
        public Data ERP_TSalseInfo_Import_Excel()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            Data kq = new Data();
            using (PARKERPEntities context = new PARKERPEntities())
            {
                using (var transaction = context.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        var httpRequest = HttpContext.Current.Request;
                        string ViewAndUpdatePrice = httpRequest.Form["ViewAndUpdatePrice"];

                        string Path = "";
                        foreach (string file in httpRequest.Files)
                        {
                            var postedFile = httpRequest.Files[file];
                            if (postedFile != null && postedFile.ContentLength > 0)
                            {
                                Path = "~/Upload/Excel/TSalseInfo" + postedFile.FileName;
                                var filePath = HttpContext.Current.Server.MapPath(Path);
                                postedFile.SaveAs(filePath);
                            }
                            FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));


                            if (existingFile.Extension.ToString() == ".xls")
                            {
                                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Please import file excel type XLSX!" });
                                kq.Results = resultList;
                                kq.Type = 0;
                                return kq;
                            }
                            using (ExcelPackage package = new ExcelPackage(existingFile))
                            {
                                ExcelWorkbook workBook = package.Workbook;
                                if (workBook != null)
                                {
                                    if (workBook.Worksheets.Count > 0)
                                    {
                                        ExcelWorksheet worksheet = workBook.Worksheets.FirstOrDefault();
                                        int rowCount = worksheet.Dimension.End.Row;

                                        List<TProdInstItem> ListTProdInstItemInsert = new List<TProdInstItem>();
                                        List<TSalseInfo> ListInsert = new List<TSalseInfo>();
                                        List<TCust> ListTCust = new List<TCust>();
                                        List<TBOM> ListTBOM = new List<TBOM>();
                                        using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                        {
                                            var queryParameters = new DynamicParameters();
                                            //queryParameters.Add("@FACTORY", this.GetDB());
                                            connection.Open();
                                            var data = connection.Query<TCust>("SELECT CustID,CustDesc FROM dbo.TCust (NOLOCK)", queryParameters, commandTimeout: 1500
                                                , commandType: CommandType.Text);
                                            ListTCust = data.ToList();


                                            var queryParametersTBOM = new DynamicParameters();
                                            var dataTBOM = connection.Query<TBOM>("SELECT BOMID,ProdID,Version FROM dbo.TBOM (NOLOCK)", queryParametersTBOM, commandTimeout: 1500
                                                , commandType: CommandType.Text);
                                            ListTBOM = dataTBOM.ToList();
                                        }

                                        for (int row = 2; row <= rowCount; row++)
                                        {

                                            if (worksheet.Cells[row, 1].Value == null || worksheet.Cells[row, 2].Value == null || worksheet.Cells[row, 13].Value == null
                                                || worksheet.Cells[row, 16].Value == null)
                                            {
                                                continue;
                                            }

                                            int SalseInfoID;
                                            if (!int.TryParse(worksheet.Cells[row, 1].Value.ToString(), out SalseInfoID))
                                            {
                                                resultList.Add(new Result { ErrorCode = row, ErrorMessage = "SalseInfoID row " + row.ToString() + " " + worksheet.Cells[row, 1].Value.ToString() + " not number!" });
                                                continue;
                                            }
                                            int ProdInstItemID;
                                            if (!int.TryParse(worksheet.Cells[row, 2].Value.ToString(), out ProdInstItemID))
                                            {
                                                resultList.Add(new Result { ErrorCode = row, ErrorMessage = "ProdInstItemID row " + row.ToString() + " " + worksheet.Cells[row, 2].Value.ToString() + " not number!" });
                                                continue;
                                            }



                                            int ProdID;
                                            if (!int.TryParse(worksheet.Cells[row, 6].Value.ToString(), out ProdID))
                                            {
                                                resultList.Add(new Result { ErrorCode = row, ErrorMessage = "ProdID row " + row.ToString() + " " + worksheet.Cells[row, 6].Value.ToString() + " not number!" });
                                                continue;
                                            }
                                            short Version;
                                            short? VersionNull=null;
                                            int? BOMID=null;
                                            if (worksheet.Cells[row, 7].Value != null && worksheet.Cells[row, 7].Value.ToString() != "")
                                            {
                                                if (!short.TryParse(worksheet.Cells[row, 7].Value.ToString(), out Version))
                                                {
                                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Version row " + row.ToString() + " " + worksheet.Cells[row, 7].Value.ToString() + " not number!" });
                                                    continue;
                                                }

                                                TBOM TBOMitem = ListTBOM.FirstOrDefault(x => x.ProdID == ProdID && x.Version == Version);
                                                if (TBOMitem == null)
                                                {
                                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Version row " + row.ToString() + " " + worksheet.Cells[row, 7].Value.ToString() + " not find ProdID "+ worksheet.Cells[row, 6].Value.ToString() + " in TBOM!" });
                                                    continue;
                                                }
                                                VersionNull = Version;
                                                BOMID = TBOMitem.BOMID;
                                            }

                                            string BuyerPONo = worksheet.Cells[row, 11].Value != null ? worksheet.Cells[row, 11].Value.ToString().Trim() : null;

                                            float Price = 0;
                                            float? PriceNull = null;
                                            if (worksheet.Cells[row, 12].Value != null && worksheet.Cells[row, 12].Value.ToString() != "")
                                            {
                                                if (!float.TryParse(worksheet.Cells[row, 12].Value.ToString(), out Price))
                                                {
                                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Price row " + row.ToString() + " " + worksheet.Cells[row, 12].Value.ToString() + " not number!" });
                                                    continue;
                                                }
                                                PriceNull = Price;
                                            }
                                            float Qty = 0;
                                            if (worksheet.Cells[row, 13].Value != null && worksheet.Cells[row, 13].Value.ToString() != "")
                                            {
                                                if (!float.TryParse(worksheet.Cells[row, 13].Value.ToString(), out Qty))
                                                {
                                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Qty row " + row.ToString() + " " + worksheet.Cells[row, 13].Value.ToString() + " not number!" });
                                                    continue;
                                                }
                                            }
                                            string SoldToDesc = worksheet.Cells[row, 14].Value != null ? worksheet.Cells[row, 14].Value.ToString().Trim() : null;
                                            int SoldTo=0;
                                            TCust SoldToitem = ListTCust.FirstOrDefault(x => x.CustDesc == SoldToDesc);
                                            if (!string.IsNullOrEmpty(SoldToDesc) && SoldToitem == null)
                                            {
                                                resultList.Add(new Result { ErrorCode = row, ErrorMessage = "SoldToDesc row " + row.ToString() + " " + worksheet.Cells[row, 14].Value.ToString() + " not find!" });
                                                continue;
                                            }
                                            if (SoldToitem != null)
                                            {
                                                SoldTo = SoldToitem.CustID;
                                            }
 

                                            string ShipToDesc = worksheet.Cells[row, 15].Value != null ? worksheet.Cells[row, 15].Value.ToString().Trim() : null;
                                            int ShipTo=0;
                                            TCust ShipToitem = ListTCust.FirstOrDefault(x => x.CustDesc == ShipToDesc);
                                            if (!string.IsNullOrEmpty(ShipToDesc) && ListTCust.FirstOrDefault(x => x.CustDesc == ShipToDesc) == null)
                                            {
                                                resultList.Add(new Result { ErrorCode = row, ErrorMessage = "ShipToDesc row " + row.ToString() + " " + worksheet.Cells[row, 15].Value.ToString() + " not find!" });
                                                continue;
                                            }
                                            if (ShipToitem!=null)
                                            {
                                                ShipTo = ShipToitem.CustID;
                                            }

                                            double d = double.Parse(worksheet.Cells[row, 16].Value.ToString());
                                            DateTime ReqETD = DateTime.FromOADate(d).Date;



                                            float PCPerCarton = 0;
                                            float? PCPerCartonNull = null;
                                            if (worksheet.Cells[row, 17].Value != null && worksheet.Cells[row, 17].Value.ToString() != "")
                                            {
                                                if (!float.TryParse(worksheet.Cells[row, 17].Value.ToString(), out PCPerCarton))
                                                {
                                                    resultList.Add(new Result { ErrorCode = row, ErrorMessage = "PCPerCarton row " + row.ToString() + " " + worksheet.Cells[row, 17].Value.ToString() + " not number!" });
                                                    continue;
                                                }
                                                PCPerCartonNull = PCPerCarton;
                                            }

                                            string Remark = worksheet.Cells[row, 18].Value != null ? worksheet.Cells[row, 18].Value.ToString().Trim() : null;
                                            string SKUCodeBuyer = worksheet.Cells[row, 19].Value != null ? worksheet.Cells[row, 19].Value.ToString().Trim() : null;
                                            ListInsert.Add(new TSalseInfo
                                            {
                                                SalseInfoID = SalseInfoID,
                                                BuyerPONo = BuyerPONo,
                                                Price = PriceNull,
                                                Qty = Qty,
                                                SoldTo = SoldTo,
                                                ShipTo = ShipTo,
                                                ReqETD = ReqETD,
                                                PCPerCarton = PCPerCartonNull,
                                                Remark = Remark,
                                                SKUCodeBuyer = SKUCodeBuyer,
                                            });

                                            ListTProdInstItemInsert.Add(new TProdInstItem
                                            {
                                                ProdInstItemID = ProdInstItemID,
                                                Version = VersionNull,
                                                BOMID= BOMID,
                                                ProdID= ProdID,
                                            });

                                        }
                                        if (ListInsert.Count == 0)
                                        {
                                            resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File import no data" });
                                            kq.Results = resultList;
                                            kq.Type = 0;
                                            return kq;

                                        }
                                        var CheckDoule = (from c in ListInsert//check trùng SalseInfoID
                                                          group c by new
                                                          {
                                                              c.SalseInfoID
                                                          } into gcs
                                                          where gcs.Count() > 1
                                                          select new TSalseInfo()
                                                          {
                                                              SalseInfoID = gcs.Key.SalseInfoID,
                                                          }).ToList();
                                        if (CheckDoule.Count() > 0)
                                        {
                                            foreach (TSalseInfo item in CheckDoule)
                                            {
                                                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Double SalseInfoID " + item.SalseInfoID + "" });
                                            }
                                        }
                                        if (resultList.Count > 0)
                                        {
                                            kq.Results = resultList;
                                            kq.Type = 0;
                                            return kq;

                                        }


                                        int UpdatedBy= int.Parse(GetUserlogin());
                                        DateTime UpdatedOn = DateTime.Now;
                                        string db = this.GetDB();
                                        foreach (TProdInstItem item in ListTProdInstItemInsert)
                                        {



                                            TProdInstItem itemupdate = context.TProdInstItems.FirstOrDefault(x => x.ProdInstItemID == item.ProdInstItemID);
                                            if (itemupdate == null)
                                            {
                                                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find ProdInstItemID " + item.ProdInstItemID.ToString() });
                                                kq.Results = resultList;
                                                kq.Type = 0;
                                                return kq;
                                            }
                                            using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                            {
                                                var queryParameters = new DynamicParameters();
                                                queryParameters.Add("@BOMID", item.BOMID);
                                                queryParameters.Add("@ProdID", item.ProdID);
                                                connection.Open();
                                                TCon dataTCon = connection.QueryFirstOrDefault<TCon>(@"SELECT ConsID FROM dbo.TCons (NOLOCK) 
                                                WHERE BOMID=@BOMID AND ProdID=@ProdID", queryParameters, commandTimeout: 1500
                                                    , commandType: CommandType.Text);
                                                if (dataTCon != null)
                                                {
                                                    //resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find ConsID in BOMID " + item.BOMID.ToString() + " and ProdID " + item.ProdID.ToString() });
                                                    //kq.Results = resultList;
                                                    //kq.Type = 0;
                                                    //return kq;
                                                    itemupdate.Version = item.Version;
                                                    itemupdate.BOMID = item.BOMID;
                                                    itemupdate.ConsID = dataTCon.ConsID;

                                                }

                                                itemupdate.UpdatedBy = UpdatedBy;
                                                itemupdate.UpdatedOn = UpdatedOn;

                                            }

                                        }
                                        foreach (TSalseInfo item in ListInsert)
                                        {
                                            TSalseInfo itemupdate = context.TSalseInfoes.FirstOrDefault(x => x.SalseInfoID == item.SalseInfoID);
                                            if (itemupdate == null)
                                            {
                                                resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Not find SalseInfoID " + item.SalseInfoID.ToString()});
                                                kq.Results = resultList;
                                                kq.Type = 0;
                                                return kq;
                                            }
                                            itemupdate.BuyerPONo = item.BuyerPONo;
                                            itemupdate.Price = ViewAndUpdatePrice == "true" ? item.Price : itemupdate.Price;
                                            itemupdate.Qty = item.Qty;
                                            itemupdate.SoldTo = item.SoldTo;
                                            itemupdate.ShipTo = item.ShipTo;
                                            itemupdate.ReqETD = item.ReqETD;
                                            itemupdate.PCPerCarton = item.PCPerCarton;
                                            itemupdate.Remark = item.Remark;
                                            if (db=="PBC")
                                            {
                                                itemupdate.SKUCodeBuyer = item.SKUCodeBuyer;
                                            }
                                            itemupdate.UpdatedBy = UpdatedBy;
                                            itemupdate.UpdatedOn = UpdatedOn;
                                        }

                                        context.SaveChanges();
                                        transaction.Commit();
                                        kq.Type = 1;
                                        return kq;

                                    }
                                }

                            }
                        }
                        kq.Results = resultList;
                        kq.Type = 0;
                        return kq;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new NotImplementedException(ex.Message.ToString());

                    }
                }
            }

        }
        [HttpPost]
        [Route("ERP_TProdInstItem_Import_Excel")]
        public Data ERP_TProdInstItem_Import_Excel()
        {
            Initialization();
            List<Result> resultList = new List<Result>();
            Data kq = new Data();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                string Path = "";
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        Path = "~/Upload/Excel/WIP" + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath(Path);
                        postedFile.SaveAs(filePath);
                    }
                    FileInfo existingFile = new FileInfo(HttpContext.Current.Server.MapPath(Path));


                    if (existingFile.Extension.ToString() == ".xls")
                    {
                        resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Please import file excel type XLSX!" });
                        kq.Results = resultList;
                        kq.Type = 0;
                        return kq;
                    }
                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        ExcelWorkbook workBook = package.Workbook;
                        if (workBook != null)
                        {
                            if (workBook.Worksheets.Count > 0)
                            {
                                ExcelWorksheet worksheet = workBook.Worksheets.FirstOrDefault();
                                int rowCount = worksheet.Dimension.End.Row;

                                List<dynamic> ListInsert = new List<dynamic>();
                                List<TProd_Custom> ListTProd_Custom = new List<TProd_Custom>();
                                List<TCust> ListTCust = new List<TCust>();
                                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                                {



                                    var queryParameters = new DynamicParameters();
                                    connection.Open();
                                    var data = connection.Query<TProd_Custom>(@"SELECT a.ProdID,a.ProdCode,a.SKUCode,b.SeasonDesc,a.ProdDesc,a.Color,c.RangeDesc 
                                            FROM dbo.TProd a (NOLOCK)
                                            LEFT JOIN dbo.TSeason b (NOLOCK) ON b.SeasonID = a.SeasonID
                                            LEFT JOIN dbo.TRange c (NOLOCK) ON c.RangeID = a.RangeID"
                                        , queryParameters, commandTimeout: 1500
                                        , commandType: CommandType.Text);
                                    ListTProd_Custom = data.ToList();


                                    var queryParametersTCust = new DynamicParameters();
                                    var dataTCust = connection.Query<TCust>("SELECT CustID,CustDesc FROM dbo.TCust (NOLOCK)", queryParametersTCust, commandTimeout: 1500
                                        , commandType: CommandType.Text);
                                    ListTCust = dataTCust.ToList();




                                }

                                for (int row = 2; row <= rowCount; row++)
                                {

                                    if (worksheet.Cells[row, 1].Value == null)
                                    {
                                        continue;
                                    }
                                    string ProdCode = worksheet.Cells[row, 1].Value != null ? worksheet.Cells[row, 1].Value.ToString().Trim() : null;


                                    TProd_Custom TProd_CustomItem = ListTProd_Custom.Where(x => x.ProdCode == ProdCode).FirstOrDefault();
                                    if (TProd_CustomItem == null)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "ProdCode row " + row.ToString() + " " + worksheet.Cells[row, 1].Value.ToString() + " not find!" });
                                        continue;
                                    }
                                    int ProdID = TProd_CustomItem.ProdID;
                                    string SKUCode = TProd_CustomItem.SKUCode;
                                    string SeasonDesc = TProd_CustomItem.SeasonDesc;
                                    string ProdDesc = TProd_CustomItem.ProdDesc;
                                    string Color = TProd_CustomItem.Color;
                                    string RangeDesc = TProd_CustomItem.RangeDesc;



                                    string BuyerPONo = worksheet.Cells[row, 2].Value != null ? worksheet.Cells[row, 2].Value.ToString().Trim() : null;
                                    float Qty;
                                    if (!float.TryParse(worksheet.Cells[row, 3].Value.ToString(), out Qty))
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "Qty row " + row.ToString() + " " + worksheet.Cells[row, 3].Value.ToString() + " not number!" });
                                        continue;
                                    }
                                    string SoldToDesc = worksheet.Cells[row, 4].Value != null ? worksheet.Cells[row, 4].Value.ToString().Trim() : null;
                                    TCust SoldToItem = ListTCust.Where(x => x.CustDesc == SoldToDesc).FirstOrDefault();
                                    if (!string.IsNullOrEmpty(SoldToDesc) && SoldToItem == null)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "SoldTo row " + row.ToString() + " " + worksheet.Cells[row, 4].Value.ToString() + " not find!" });
                                        continue;
                                    }
                                    int? SoldTo = SoldToItem == null ? 0 : SoldToItem.CustID;


                                    string ShipToDesc = worksheet.Cells[row, 5].Value != null ? worksheet.Cells[row, 5].Value.ToString().Trim() : null;
                                    TCust ShipToItem = ListTCust.Where(x => x.CustDesc == ShipToDesc).FirstOrDefault();
                                    if (!string.IsNullOrEmpty(ShipToDesc) && ShipToItem == null)
                                    {
                                        resultList.Add(new Result { ErrorCode = row, ErrorMessage = "ShipTo row " + row.ToString() + " " + worksheet.Cells[row, 5].Value.ToString() + " not find!" });
                                        continue;
                                    }
                                    int? ShipTo = ShipToItem == null ? 0 : ShipToItem.CustID;
                                    DateTime ReqETD=DateTime.Today; ;
                                    if (worksheet.Cells[row, 6].Value != null)
                                    {
                                        double d = double.Parse(worksheet.Cells[row, 6].Value.ToString());
                                        ReqETD = DateTime.FromOADate(d).Date;
                                    }

                                    string Remark = worksheet.Cells[row, 7].Value != null ? worksheet.Cells[row, 7].Value.ToString().Trim() : null;
                                    string SKUCodeBuyer = worksheet.Cells[row, 8].Value != null ? worksheet.Cells[row, 8].Value.ToString().Trim() : null;

 


                                    ListInsert.Add(new
                                    {
                                        ProdID= ProdID,
                                        ProdCode = ProdCode,
                                        SKUCode= SKUCode,
                                        SeasonDesc = SeasonDesc,
                                        ProdDesc = ProdDesc,
                                        Color = Color,
                                        RangeDesc = RangeDesc,
                                        BuyerPONo = BuyerPONo,
                                        Qty = Qty,
                                        SoldTo = SoldTo,
                                        SoldToDesc = SoldToDesc,
                                        ShipTo = ShipTo,
                                        ShipToDesc = ShipToDesc,
                                        ReqETD = ReqETD,
                                        ReqETDFormat = ReqETD.ToString("dd/MMM/yyyy"),
                                        Remark = Remark,
                                        SKUCodeBuyer = SKUCodeBuyer,                                       
                                        RowID = row - 1,
                                    });

                                }
                                if (ListInsert.Count == 0)
                                {
                                    resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "File import no data" });
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;

                                }
                                //var CheckDoule = (from c in ListInsert//check trùng ProdCode và Prod Order
                                //                  group c by new
                                //                  {
                                //                      c.SalseInfoID
                                //                  } into gcs
                                //                  where gcs.Count() > 1
                                //                  select new TTProdPriceT()
                                //                  {
                                //                      SalseInfoID = gcs.Key.SalseInfoID,
                                //                  }).ToList();
                                //if (CheckDoule.Count() > 0)
                                //{
                                //    foreach (TTProdPriceT item in CheckDoule)
                                //    {
                                //        resultList.Add(new Result { ErrorCode = 0, ErrorMessage = "Double SalseInfoID " + item.SalseInfoID + "" });
                                //    }
                                //}
                                if (resultList.Count > 0)
                                {
                                    kq.Results = resultList;
                                    kq.Type = 0;
                                    return kq;

                                }
                                kq.Results = ListInsert;
                                kq.Type = 1;
                                return kq;

                            }
                        }

                    }
                    kq.Results = resultList;
                    kq.Type = 0;
                    return kq;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());

            }
            kq.Results = resultList;
            kq.Type = 0;
            return kq;
        }
        [HttpPost]
        [Route("ERP_TProdInstItem_Import")]
        public async Task<bool> ERP_TProdInstItem_Import(List<TProdInstItem_Custom> listitem)
        {
            Initialization();
            return await iTProdInstItem_Repository.ERP_TProdInstItem_Import(listitem);
        }
        [HttpGet]
        [Route("ERP_TProdInstItem_Import_Template")]
        public HttpResponseMessage ERP_TProdInstItem_Import_Template(string fileName)
        {
            Initialization();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string filePath = HttpContext.Current.Server.MapPath("~/Upload/Excel/TemplateExcel/") + fileName;
            if (!File.Exists(filePath))
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.ReasonPhrase = string.Format("File not found: {0} .", fileName);
                return response;
            }
            FileInfo newFile = new FileInfo(filePath);

            ExcelPackage excel = new ExcelPackage(newFile);
            iTProdInstItem_Repository.ERP_TCust_GetCustIDCustDesc_Excel(excel);


            byte[] bytes = excel.GetAsByteArray();
            response.Content = new ByteArrayContent(bytes);
            response.Content.Headers.ContentLength = bytes.LongLength;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileName));
            return response;
        }
    }
}

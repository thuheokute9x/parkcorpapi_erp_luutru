﻿using HR.API.Repository;
using HR.API.Models.Entity;
using HR.API.Repository.ERP.VKGrp;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using HR.API.Models.ERP.TableCustom;

namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class STVKGrpController : BaseController
    {
        private GenericRepository<STVKGrp> repository;
        private ISTVKGrp_Repository iSTVKGrp_Repository;
        public STVKGrpController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<STVKGrp>(unitOfWork);
            iSTVKGrp_Repository = new STVKGrp_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_STVKGrp_GetPageSize")]
        public async Task<IList<Object>> ERP_STVKGrp_GetPageSize(dynamic item)
        {
            Initialization();
            return await iSTVKGrp_Repository.ERP_STVKGrp_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_STVKGrp_GetPageSize_Excel")]
        public HttpResponseMessage ERP_STVKGrp_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "STVKGrp";
                Byte[] bytes = ExportDataTableToExcelFromStore(iSTVKGrp_Repository.ERP_STVKGrp_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_STVKGrp_Item_GetPageSize")]
        public async Task<IList<Object>> ERP_STVKGrp_Item_GetPageSize(dynamic item)
        {
            Initialization();
            return await iSTVKGrp_Repository.ERP_STVKGrp_Item_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_STVKGrp_Item_GetPageSize_Excel")]
        public HttpResponseMessage ERP_STVKGrp_Item_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "STVKGrp_Item";
                Byte[] bytes = ExportDataTableToExcelFromStore(iSTVKGrp_Repository.ERP_STVKGrp_Item_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_STVKGrp_Cons_GetPageSize")]
        public async Task<IList<Object>> ERP_STVKGrp_Cons_GetPageSize(dynamic item)
        {
            Initialization();
            return await iSTVKGrp_Repository.ERP_STVKGrp_Cons_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_STVKGrp_Cons_GetPageSize_Excel")]
        public HttpResponseMessage ERP_STVKGrp_Cons_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "STVKGrp_Cons";
                Byte[] bytes = ExportDataTableToExcelFromStore(iSTVKGrp_Repository.ERP_STVKGrp_Cons_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_SQVKGrp_Srch_List_PageSize")]
        public async Task<IList<Object>> ERP_SQVKGrp_Srch_List_PageSize(dynamic item)
        {
            Initialization();
            return await iSTVKGrp_Repository.ERP_SQVKGrp_Srch_List_PageSize(item);
        }
        [HttpPost]
        [Route("ERP_SQVKGrp_Srch_List_InserRadis")]
        public async Task<Boolean> ERP_SQVKGrp_Srch_List_InserRadis(List<TProd_Custom> item)
        {
            Initialization();
            return await iSTVKGrp_Repository.ERP_SQVKGrp_Srch_List_InserRadis(item);
        }
        [HttpPost]
        [Route("ERP_STVKGrp_Item_FromRediscache_GetPageSize")]
        public async Task<IList<TProd_Custom>> ERP_STVKGrp_Item_FromRediscache_GetPageSize(dynamic item)
        {
            Initialization();
            return await iSTVKGrp_Repository.ERP_STVKGrp_Item_FromRediscache_GetPageSize(item);
        }
    }
}

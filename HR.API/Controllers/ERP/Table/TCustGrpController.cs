﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.Curr;
using HR.API.Repository.ERP.CustGrp;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TCustGrpController : BaseController
    {
        private GenericRepository<TCustGrp> repository;
        private ITCustGrp_Repository iTCustGrp_Repository;
        public TCustGrpController()
        {

        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TCustGrp>(unitOfWork);
            iTCustGrp_Repository = new TCustGrp_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TCustGrp_GetAll")]
        public async Task<IList<Object>> ERP_TCustGrp_GetAll()
        {
            Initialization();
            return await iTCustGrp_Repository.ERP_TCustGrp_GetAll();
        }
        [HttpGet]
        [Route("ERP_TCustGrp_GetAllWithAddItemNull")]
        public async Task<IList<Object>> ERP_TCustGrp_GetAllWithAddItemNull()
        {
            Initialization();
            return await iTCustGrp_Repository.ERP_TCustGrp_GetAllWithAddItemNull();
        }
        [HttpPost]
        [Route("ERP_TCustGrp_Insert")]
        public async Task<bool> ERP_TCustGrp_Insert(TCustGrp item)
        {
            Initialization();
            return await iTCustGrp_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TCustGrp_Update")]
        public async Task<bool> ERP_TCustGrp_Update(TCustGrp item)
        {
            Initialization();
            return await iTCustGrp_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TCust_GetByCustGrpCD")]
        public async Task<IList<Object>> ERP_TCust_GetByCustGrpCD(TCust item)
        {
            Initialization();
            return await iTCustGrp_Repository.ERP_TCust_GetByCustGrpCD(item);
        }
        [HttpPost]
        [Route("ERP_TCust_Insert")]
        public async Task<IList<Object>> ERP_TCust_Insert(TCust item)
        {
            Initialization();
            return await iTCustGrp_Repository.ERP_TCust_Insert(item);
        }
        [HttpPost]
        [Route("ERP_TCust_Update")]
        public async Task<IList<Object>> ERP_TCust_Update(TCust_Custom item)
        {
            Initialization();
            return await iTCustGrp_Repository.ERP_TCust_Update(item);
        }
    }
}

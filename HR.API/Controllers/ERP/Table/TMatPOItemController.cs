﻿
using HR.API.Models;
using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom;
using HR.API.Repository;
using HR.API.Repository.ERP.MatPOItem;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TMatPOItemController : BaseController
    {
        private GenericRepository<TMatPOItem> repository;
        private ITMatPOItem_Repository iTMatPOItem_Repository;
        public TMatPOItemController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TMatPOItem>(unitOfWork);
            iTMatPOItem_Repository = new TMatPOItem_Repository(unitOfWork);
        }
        [HttpPost]
        [Route("ERP_TMatPOItem_GetMatPOCDLike")]
        public async Task<IList<TMatPOItem>> ERP_TMatPOItem_GetMatPOCDLike(TMatPOItem_Custom item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPOItem_GetMatPOCDLike(item);
        }
        [HttpPost]
        [Route("ERP_TMatPOItem_GetMatPOCDLikeWithVK")]
        public async Task<IList<Object>> ERP_TMatPOItem_GetMatPOCDLikeWithVK(TMatPOItem_Custom item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPOItem_GetMatPOCDLikeWithVK(item);
        }
        [HttpPost]
        [Route("ERP_TMatPO_GetPageSize")]
        public async Task<IList<Object>> ERP_TMatPO_GetPageSize(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_GetPageSize(item);
        }
        [HttpPost]
        [Route("ERP_TMatPOItem_GetByMatPOCD")]
        public async Task<IList<Object>> ERP_TMatPOItem_GetByMatPOCD(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPOItem_GetByMatPOCD(item);
        }
        [HttpPost]
        [Route("ERP_TMatPOItem_GetByMatPOCD_Excel")]
        public HttpResponseMessage ERP_TMatPOItem_GetByMatPOCD_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "MatPOItem";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTMatPOItem_Repository.ERP_TMatPOItem_GetByMatPOCD_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

        [HttpPost]
        [Route("ERP_TMatPOItem_InsertList")]
        public async Task<bool> ERP_TMatPOItem_InsertList(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPOItem_InsertList(item);
        }
        [HttpGet]
        [Route("ERP_TMatPO_ExtPayMethod_GetAll")]
        public async Task<IList<Object>> ERP_TMatPO_ExtPayMethod_GetAll()
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_ExtPayMethod_GetAll();
        }
        [HttpPost]
        [Route("ERP_TMatPO_ExtPayMethod_Insert")]
        public async Task<IList<Object>> ERP_TMatPO_ExtPayMethod_Insert(TMatPO_ExtPayMethod item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_ExtPayMethod_Insert(item);
        }
        [HttpPost]
        [Route("ERP_TMatPO_ExtPayMethod_Update")]
        public async Task<IList<Object>> ERP_TMatPO_ExtPayMethod_Update(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_ExtPayMethod_Update(item);
        }
        [HttpPost]
        [Route("ERP_TMatPO_ExtPayMethod_Delete")]
        public async Task<IList<Object>> ERP_TMatPO_ExtPayMethod_Delete(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_ExtPayMethod_Delete(item);
        }
        [HttpGet]
        [Route("ERP_TMatPO_ExtDeliverTo_GetAll")]
        public async Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_GetAll()
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_ExtDeliverTo_GetAll();
        }
        [HttpPost]
        [Route("ERP_TMatPO_ExtDeliverTo_Insert")]
        public async Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_Insert(TMatPO_ExtDeliverTo item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_ExtDeliverTo_Insert(item);
        }
        [HttpPost]
        [Route("ERP_TMatPO_ExtDeliverTo_Update")]
        public async Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_Update(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_ExtDeliverTo_Update(item);
        }
        [HttpPost]
        [Route("ERP_TMatPO_ExtDeliverTo_Delete")]
        public async Task<IList<Object>> ERP_TMatPO_ExtDeliverTo_Delete(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_ExtDeliverTo_Delete(item);
        }




        [HttpGet]
        [Route("ERP_TMatPO_AttentionTo_GetAll")]
        public async Task<IList<Object>> ERP_TMatPO_AttentionTo_GetAll()
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_AttentionTo_GetAll();
        }
        [HttpPost]
        [Route("ERP_TMatPO_AttentionTo_Insert")]
        public async Task<IList<Object>> ERP_TMatPO_AttentionTo_Insert(TMatPO_AttentionTo item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_AttentionTo_Insert(item);
        }
        [HttpPost]
        [Route("ERP_TMatPO_AttentionTo_Update")]
        public async Task<IList<Object>> ERP_TMatPO_AttentionTo_Update(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_AttentionTo_Update(item);
        }
        [HttpPost]
        [Route("ERP_TMatPO_AttentionTo_Delete")]
        public async Task<IList<Object>> ERP_TMatPO_AttentionTo_Delete(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_AttentionTo_Delete(item);
        }
        [HttpGet]
        [Route("ERP_TMatPO_GenerateMatPOCD")]
        public async Task<TMatPO> ERP_TMatPO_GenerateMatPOCD()
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_GenerateMatPOCD();
        }
        [HttpPost]
        [Route("ERP_TMatPOItem_UpdatePOQty")]
        public async Task<IList<Object>> ERP_TMatPOItem_UpdatePOQty(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPOItem_UpdatePOQty(item);
        }
        [HttpPost]
        [Route("ERP_TMatPOItem_UpdatePUnit")]
        public async Task<bool> ERP_TMatPOItem_UpdatePUnit(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPOItem_UpdatePUnit(item);
        }
        [HttpPost]
        [Route("ERP_TMatPOItem_UpdateRemark")]
        public async Task<bool> ERP_TMatPOItem_UpdateRemark(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPOItem_UpdateRemark(item);
        }
        //[HttpPost]
        //[Route("ERP_TMatPO_IssueSelectedPO")]
        //public async Task<bool> ERP_TMatPO_IssueSelectedPO(List<TMatPO> itemlist)
        //{
        //    Initialization();
        //    return await iTMatPOItem_Repository.ERP_TMatPO_IssueSelectedPO(itemlist);
        //}
        [HttpPost]
        [Route("ERP_TMatPO_CheckTMatPOItemExist")]
        public async Task<Result> ERP_TMatPO_CheckTMatPOItemExist(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_CheckTMatPOItemExist(item);
        }
        [HttpPost]
        [Route("ERP_TMatPO_IssueSelectedPO")]
        public HttpResponseMessage ERP_TMatPO_IssueSelectedPO(TMatPO_Custom item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "MPO";
                Byte[] bytes = iTMatPOItem_Repository.ERP_TMatPO_IssueSelectedPO(item);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TMatPOItem_GeneratePOWithSelected")]
        public async Task<TMatPO_Custom> ERP_TMatPOItem_GeneratePOWithSelected(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPOItem_GeneratePOWithSelected(item);
        }
        [HttpPost]
        [Route("ERP_TMatPO_Insert")]
        public async Task<bool> ERP_TMatPO_Insert(TMatPO item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_Insert(item);
        }
        [HttpPost]
        [Route("ERP_TMatPO_Update")]
        public async Task<bool> ERP_TMatPO_Update(TMatPO item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_Update(item);
        }
        [HttpPost]
        [Route("ERP_TMatPOItem_GetManualTrue")]
        public async Task<IList<Object>> ERP_TMatPOItem_GetManualTrue(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPOItem_GetManualTrue(item);
        }
        [HttpPost]
        [Route("ERP_TMatPOItem_GetManualTrue_Excel")]
        public HttpResponseMessage ERP_TMatPOItem_GetManualTrue_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "MatPOItemManual";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTMatPOItem_Repository.ERP_TMatPOItem_GetManualTrue_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }


        [HttpPost]
        [Route("ERP_TMatPOItem_GetManualTrueByMatCD")]
        public async Task<IList<Object>> ERP_TMatPOItem_GetManualTrueByMatCD(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPOItem_GetManualTrueByMatCD(item);
        }
        [HttpPost]
        [Route("ERP_TMatPO_GetPageSize_Excel")]
        public HttpResponseMessage ERP_TMatPO_GetPageSize_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "MatPO";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTMatPOItem_Repository.ERP_TMatPO_GetPageSize_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TMatPOItem_Delete")]
        public async Task<Boolean> ERP_TMatPOItem_Delete(TMatPOItem item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPOItem_Delete(item);
        }
        [HttpPost]
        [Route("ERP_Report__TMatPOItem_SQMatPOItem")]
        public async Task<IList<Object>> ERP_Report__TMatPOItem_SQMatPOItem(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_Report__TMatPOItem_SQMatPOItem(item);
        }
        [HttpPost]
        [Route("ERP_Report__TMatPOItem_SQMatPOItem_Excel")]
        public HttpResponseMessage ERP_Report__TMatPOItem_SQMatPOItem_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "SQMatPOItem";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTMatPOItem_Repository.ERP_Report__TMatPOItem_SQMatPOItem_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_Report_TMatPOItem_MatPOItemBalanceList")]
        public async Task<IList<Object>> ERP_Report_TMatPOItem_MatPOItemBalanceList(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_Report_TMatPOItem_MatPOItemBalanceList(item);
        }
        [HttpPost]
        [Route("ERP_Report_TMatPOItem_MatPOItemBalanceList_Excel")]
        public HttpResponseMessage ERP_Report_TMatPOItem_MatPOItemBalanceList_Excel(dynamic item)
        {
            try
            {
                Initialization();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "MatPOItemBalanceList";
                Byte[] bytes = ExportDataTableToExcelFromStore(iTMatPOItem_Repository.ERP_Report_TMatPOItem_MatPOItemBalanceList_Excel(item), filename);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_TMatPO_UpdateRangeReqETDAttentionTo")]
        public async Task<Boolean> ERP_TMatPO_UpdateRangeReqETDAttentionTo(dynamic item)
        {
            Initialization();
            return await iTMatPOItem_Repository.ERP_TMatPO_UpdateRangeReqETDAttentionTo(item);
        }
    }
}

﻿using HR.API.Models.Entity;
using HR.API.Repository;
using HR.API.Repository.Proc;
using HR.API.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Table
{
    [HR.API.App_Start.JwtAuthentication]
    public class TProcController : BaseController
    {
        private GenericRepository<TProc> repository;
        private ITProc_Repository iTProc_Repository;
        public TProcController()
        {
        }
        public void Initialization()
        {
            unitOfWork = new UnitOfWork<PARKERPEntities>();
            repository = new GenericRepository<TProc>(unitOfWork);
            iTProc_Repository = new TProc_Repository(unitOfWork);
        }
        [HttpGet]
        [Route("ERP_TProc_GetAll")]
        public async Task<IList<TProc>> ERP_TProc_GetAll()
        {
            Initialization();
            return await iTProc_Repository.ERP_TProc_GetAll();
        }
        [HttpPost]
        [Route("ERP_TProc_Update")]
        public async Task<Boolean> ERP_TProc_Update(TProc item)
        {
            Initialization();
            return await iTProc_Repository.Update_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TProc_Insert")]
        public async Task<Boolean> ERP_TProc_Insert(TProc item)
        {
            Initialization();
            return await iTProc_Repository.Insert_SaveChange(item);
        }
        [HttpPost]
        [Route("ERP_TProc_Delete")]
        public async Task<Boolean> ERP_TProc_Delete(TProc item)
        {
            Initialization();
            return await iTProc_Repository.Delete_SaveChange(item);
        }
    }
}

﻿using Dapper;
using ExcelLibrary.SpreadSheet;
using HR.API.App_Start;
using HR.API.Models.ERP;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Report
{
    [HR.API.App_Start.JwtAuthentication]
    public class SQMatStockMonthlyController : BaseController
    {
        [HttpGet]
        [Route("ERP_Report_SQMatStockMonthly")]
        public async Task<IList<SQMatStockMonthlyModel>> ERP_Report_SQMatStockMonthly(string Outsourcing, string datemonth, int PageNumber, int PageSize)
        {
            try
            {
                DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@Outsourcing", Outsourcing);
                    queryParameters.Add("@PageSize", PageSize);
                    queryParameters.Add("@PageNumber", PageNumber);
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<SQMatStockMonthlyModel>("ERP_Report_SQMatStockMonthly_List"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpGet]
        [Route("ERP_Report_SQMatStockMonthlyExcel")]
        public HttpResponseMessage ERP_Report_SQMatStockMonthlyExcel(string Outsourcing, string datemonth)
        {
            try
            {
                DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);

                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_Report_SQMatStockMonthly_List", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = date.Month;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = date.Year;
                cmd.Parameters.Add("@Outsourcing", SqlDbType.NVarChar).Value = Outsourcing;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }


                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    string filename = "MatStockMonthly";
                    Byte[] bytes = ExportDataTableToExcelFromStore(table, filename, true, false);
                    response.Content = new ByteArrayContent(bytes);
                    response.Content.Headers.ContentLength = bytes.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    return response;
                
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpGet]
        [Route("ERP_Report_SQMatStockMonthlyV2")]
        public async Task<IList<SQMatStockMonthlyModel>> ERP_Report_SQMatStockMonthlyV2(string Outsourcing, string datemonth, int PageNumber, int PageSize)
        {
            try
            {
                List<SQMatStockMonthlyModel> result = new List<SQMatStockMonthlyModel>();
                DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@Outsourcing", Outsourcing);
                    queryParameters.Add("@PageSize", PageSize);
                    queryParameters.Add("@PageNumber", PageNumber);
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<SQMatStockMonthlyModel>("ERP_Report_SQMatStockMonthly_List_V2"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpGet]
        [Route("ERP_Report_SQMatStockMonthlyV2Excel")]
        public HttpResponseMessage ERP_Report_SQMatStockMonthlyV2Excel(string Outsourcing, string datemonth)
        {
            try
            {
                DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {

                    DataTable table = new DataTable();
                    SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                    SqlCommand cmd = new SqlCommand("ERP_Report_SQMatStockMonthly_List_V2", con);
                    cmd.CommandTimeout = 1500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Month", SqlDbType.Int).Value = date.Month;
                    cmd.Parameters.Add("@Year", SqlDbType.Int).Value = date.Year;
                    cmd.Parameters.Add("@Outsourcing", SqlDbType.NVarChar).Value = Outsourcing;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;


                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        da.Fill(table);
                    }

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    string filename = "MatStockMonthly";
                    Byte[] bytes = ExportDataTableToExcelFromStore(table, filename, true, false);
                    response.Content = new ByteArrayContent(bytes);
                    response.Content.Headers.ContentLength = bytes.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpGet]
        [Route("ERP_Report_SQMatStockMonthlyGR")]
        public async Task<IList<SQMatStockMonthlyGroupModel>> ERP_Report_SQMatStockMonthlyGR(string outsourcing, string datemonth)
        {
            try
            {
                List<SQMatStockMonthlyGroupModel> result = new List<SQMatStockMonthlyGroupModel>();
                DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@Month", date.Month);
                    queryParameters.Add("@Year", date.Year);
                    queryParameters.Add("@Outsourcing", outsourcing);
                    queryParameters.Add("@Type", 2);
                    connection.Open();
                    var data = await connection.QueryAsync<SQMatStockMonthlyGroupModel>("ERP_Report_SQMatStockMonthly_List"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpGet]
        [Route("ERP_Report_SQMatStockMonthlyGR_Excel")]
        public HttpResponseMessage ERP_Report_SQMatStockMonthlyGR_Excel(string outsourcing, string datemonth)
        {
            try
            {
                DateTime date = DateTime.ParseExact(datemonth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                DataTable table = new DataTable();
                SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                SqlCommand cmd = new SqlCommand("ERP_Report_SQMatStockMonthly_List", con);
                cmd.CommandTimeout = 1500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Month", SqlDbType.Int).Value = date.Month;
                cmd.Parameters.Add("@Year", SqlDbType.Int).Value = date.Year;
                cmd.Parameters.Add("@Outsourcing", SqlDbType.NVarChar).Value = outsourcing;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 3;
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(table);
                }
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                string filename = "MatStockMonthly" + date.ToString("yyyy/HH") + ".xlsx";
                Byte[] bytes = ExportDataTableToExcelFromStore(table, "Categories Group",true,false);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.LongLength;
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                GC.SuppressFinalize(this);
                return response;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }

    }
}

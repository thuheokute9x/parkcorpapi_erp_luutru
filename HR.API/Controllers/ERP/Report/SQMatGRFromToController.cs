﻿using Dapper;
using HR.API.Models.ERP.Report.StoreViewModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace HR.API.Controllers.ERP.Report
{
    [HR.API.App_Start.JwtAuthentication]
    public class SQMatGRFromToController : BaseController
    {
        [HttpPost]
        [Route("ERP_Report_SFMatGR_FromTo")]
        public async Task<IList<SQMatGRFromToModel>> ERP_Report_SFMatGR_FromTo(SQMatGRFromToModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    if (item.FromDate != "null" && item.ToDate != "null")
                    {
                        DateTime fromdate;
                        fromdate = DateTime.ParseExact(item.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        DateTime todate;
                        todate = DateTime.ParseExact(item.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                        todate = new DateTime(todate.Year, todate.Month, todate.Day);
                        if (fromdate > todate)
                        {
                            throw new NotImplementedException("Please choose Todate bigger Fromdate");
                        }
                        queryParameters.Add("@FromDate", fromdate);
                        queryParameters.Add("@ToDate", todate);
                    }
                    queryParameters.Add("@OutSourcing", item.OutSourcing);
                    queryParameters.Add("@IV", string.IsNullOrEmpty(item.IV) ? "" : item.IV);
                    queryParameters.Add("@VCD", string.IsNullOrEmpty(item.VCD) ? "" : item.VCD);
                    queryParameters.Add("@SHIPPER", string.IsNullOrEmpty(item.SHIPPER) ? "" : item.SHIPPER);
                    queryParameters.Add("@MatCD", string.IsNullOrEmpty(item.MatCD) ? "" : item.MatCD);
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(item.RefVKNo) ? "" : item.RefVKNo);
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<SQMatGRFromToModel>("ERP_Report_SFMatGR_FromTo_List"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_Report_SFMatGR_FromTo_Excel")]
        public HttpResponseMessage ERP_Report_SFMatGR_FromTo_Excel(SQMatGRFromToModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {

                    DataTable table = new DataTable();
                    SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                    SqlCommand cmd = new SqlCommand("ERP_Report_SFMatGR_FromTo_List", con);
                    cmd.CommandTimeout = 1500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (item.FromDate != "null" && item.ToDate != "null")
                    {
                        DateTime fromdate;
                        fromdate = DateTime.ParseExact(item.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        DateTime todate;
                        todate = DateTime.ParseExact(item.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                        todate = new DateTime(todate.Year, todate.Month, todate.Day);
                        if (fromdate > todate)
                        {
                            throw new NotImplementedException("Please choose Todate bigger Fromdate");
                        }
                        cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromdate;
                        cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = todate;
                    }
                    cmd.Parameters.Add("@OutSourcing", SqlDbType.NVarChar).Value = item.OutSourcing;
                    cmd.Parameters.Add("@IV", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.IV) ? "" : item.IV;
                    cmd.Parameters.Add("@VCD", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.VCD) ? "" : item.VCD;
                    cmd.Parameters.Add("@SHIPPER", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.SHIPPER) ? "" : item.SHIPPER;
                    cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.MatCD) ? "" : item.MatCD;
                    cmd.Parameters.Add("@RefVKNo", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.RefVKNo) ? "" : item.RefVKNo;
                    cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;
                    cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;


                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        da.Fill(table);
                    }

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    string filename = "MatGR_FromTo";
                    Byte[] bytes = ExportDataTableToExcelFromStore(table, filename, true, false);
                    response.Content = new ByteArrayContent(bytes);
                    response.Content.Headers.ContentLength = bytes.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_Report_SFMatGR_FromTo_List_PUnitFromMatStock")]
        public async Task<IList<SQMatGRFromToModel>> ERP_Report_SFMatGR_FromTo_List_PUnitFromMatStock(SQMatGRFromToModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    if (item.FromDate != "null" && item.ToDate != "null")
                    {
                        DateTime fromdate;
                        fromdate = DateTime.ParseExact(item.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        DateTime todate;
                        todate = DateTime.ParseExact(item.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                        todate = new DateTime(todate.Year, todate.Month, todate.Day);
                        if (fromdate > todate)
                        {
                            throw new NotImplementedException("Please choose Todate bigger Fromdate");
                        }
                        queryParameters.Add("@FromDate", fromdate);
                        queryParameters.Add("@ToDate", todate);
                    }
                    queryParameters.Add("@OutSourcing", item.OutSourcing);
                    queryParameters.Add("@IV", string.IsNullOrEmpty(item.IV) ? "" : item.IV);
                    queryParameters.Add("@VCD", string.IsNullOrEmpty(item.VCD) ? "" : item.VCD);
                    queryParameters.Add("@SHIPPER", string.IsNullOrEmpty(item.SHIPPER) ? "" : item.SHIPPER);
                    queryParameters.Add("@MatCD", string.IsNullOrEmpty(item.MatCD) ? "" : item.MatCD);
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(item.RefVKNo) ? "" : item.RefVKNo);
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<SQMatGRFromToModel>("ERP_Report_SFMatGR_FromTo_List_PUnitFromMatStock"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_Report_SFMatGR_FromTo_List_PUnitFromMatStock_Excel")]
        public HttpResponseMessage ERP_Report_SFMatGR_FromTo_List_PUnitFromMatStock_Excel(SQMatGRFromToModel item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {

                    DataTable table = new DataTable();
                    SqlConnection con = new SqlConnection(Settings.GetConnectionString(GetDB()));
                    SqlCommand cmd = new SqlCommand("ERP_Report_SFMatGR_FromTo_List_PUnitFromMatStock", con);
                    cmd.CommandTimeout = 1500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (item.FromDate != "null" && item.ToDate != "null")
                    {
                        DateTime fromdate;
                        fromdate = DateTime.ParseExact(item.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        DateTime todate;
                        todate = DateTime.ParseExact(item.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
                        todate = new DateTime(todate.Year, todate.Month, todate.Day);
                        if (fromdate > todate)
                        {
                            throw new NotImplementedException("Please choose Todate bigger Fromdate");
                        }
                        cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromdate;
                        cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = todate;
                    }
                    cmd.Parameters.Add("@OutSourcing", SqlDbType.NVarChar).Value = item.OutSourcing;
                    cmd.Parameters.Add("@IV", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.IV) ? "" : item.IV;
                    cmd.Parameters.Add("@VCD", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.VCD) ? "" : item.VCD;
                    cmd.Parameters.Add("@SHIPPER", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.SHIPPER) ? "" : item.SHIPPER;
                    cmd.Parameters.Add("@MatCD", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.MatCD) ? "" : item.MatCD;
                    cmd.Parameters.Add("@RefVKNo", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(item.RefVKNo) ? "" : item.RefVKNo;
                    cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = item.PageSize;
                    cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = item.PageNumber;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;


                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        da.Fill(table);
                    }

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    string filename = "MatGR_FromTo";
                    Byte[] bytes = ExportDataTableToExcelFromStore(table, filename, true, false);
                    response.Content = new ByteArrayContent(bytes);
                    response.Content.Headers.ContentLength = bytes.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_STIn_GetStartMonthFinishMonth")]
        public async Task<Object> ERP_STIn_GetStartMonthFinishMonth(dynamic item)
        {
            try
            {
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    DateTime fromdate;
                    fromdate = DateTime.ParseExact(item.date.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@date", fromdate);
                    connection.Open();
                    var data = await connection.QueryFirstAsync<Object>("SELECT CONVERT(DATETIME,CAST(YEAR(@date) AS varchar(4)) + '-' + CAST(MONTH(@date) AS varchar(4)) + '-1') FromMonth,DATEADD(DAY,-1,DATEADD(month,1,CONVERT(DATETIME,CAST(YEAR(@date) AS varchar(4)) + '-' + CAST(MONTH(@date) AS varchar(4)) + '-1'))) ToMonth"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.Text);
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message.ToString());
            }
        }
    }
}

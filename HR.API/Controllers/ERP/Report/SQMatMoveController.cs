﻿using Dapper;
using HR.API.Models.ERP;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Security.Claims;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;


namespace HR.API.Controllers.ERP.Report
{
    [HR.API.App_Start.JwtAuthentication]
    public class SQMatMoveController : BaseController
    {
        [HttpPost]
        [Route("ERP_Report_SQMatMove")]
        public async Task<IList<SQMatMoveModel>> ERP_Report_SQMatMove(SQMatMoveModel item)
        {
            try
            {
                List<SQMatMoveModel> result = new List<SQMatMoveModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {
                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@PageSize", item.PageSize);
                    queryParameters.Add("@PageNumber", item.PageNumber);
                    queryParameters.Add("@RefVKNo", item.RefVKNo);
                    queryParameters.Add("@MfgVendor", string.IsNullOrEmpty(item.MfgVendor) ? "" : item.MfgVendor.Trim());
                    queryParameters.Add("@MatCD", string.IsNullOrEmpty(item.MatCD) ? "" : item.MatCD.Trim());
                    queryParameters.Add("@PGrp", string.IsNullOrEmpty(item.PGrp) ? "" : item.PGrp.Trim());
                    queryParameters.Add("@Type", 0);
                    connection.Open();
                    var data = await connection.QueryAsync<SQMatMoveModel>("ERP_Report_SQMatMove_List"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }
        [HttpPost]
        [Route("ERP_Report_SQMatMoveExcel")]
        public async Task<HttpResponseMessage> ERP_Report_SQMatMoveExcel(SQMatMoveModel ob)
        {
            try
            {
                List<SQMatMoveModel> result = new List<SQMatMoveModel>();
                using (var connection = new SqlConnection(Settings.GetConnectionString(GetDB())))
                {

                    var queryParameters = new DynamicParameters();
                    queryParameters.Add("@RefVKNo", string.IsNullOrEmpty(ob.RefVKNo) || ob.RefVKNo == "All" ? "" : ob.RefVKNo.Trim());
                    queryParameters.Add("@MfgVendor", string.IsNullOrEmpty(ob.MfgVendor) || ob.MfgVendor == "All" ? "" : ob.MfgVendor.Trim());
                    queryParameters.Add("@MatCD", string.IsNullOrEmpty(ob.MatCD) || ob.MatCD == "All" ? "" : ob.MatCD.Trim());
                    queryParameters.Add("@PGrp", string.IsNullOrEmpty(ob.PGrp) || ob.PGrp == "All" ? "" : ob.PGrp.Trim());
                    queryParameters.Add("@Type", 1);
                    connection.Open();
                    var data = await connection.QueryAsync<SQMatMoveModel>("ERP_Report_SQMatMove_List"
                        , queryParameters, commandTimeout: 1500
                        , commandType: CommandType.StoredProcedure);
                    result = data.ToList();


                    ExcelPackage excel = new ExcelPackage();
                    var workSheet = excel.Workbook.Worksheets.Add("MatMove");
                    workSheet.Cells[1, 1, 1, 30].Style.Font.Bold = true;
                    workSheet.Cells[1, 1].Value = "ProdOrder";
                    workSheet.Cells[1, 2].Value = "MfgVendor";
                    workSheet.Cells[1, 3].Value = "Shipper";
                    workSheet.Cells[1, 4].Value = "MatCD";
                    workSheet.Cells[1, 5].Value = "MatDesc";
                    workSheet.Cells[1, 6].Value = "Color";
                    workSheet.Cells[1, 7].Value = "PUnit";

                    workSheet.Cells[1, 8].Value = "LossRate";
                    workSheet.Cells[1, 9].Value = "LossQty";
                    workSheet.Cells[1, 10].Value = "AddQty";

                    workSheet.Cells[1, 11].Value = "LeftOverQty";
                    workSheet.Cells[1, 12].Value = "StdQtyReal";
                    workSheet.Cells[1, 13].Value = "StdQty";
                    workSheet.Cells[1, 14].Value = "ActQty";
                    workSheet.Cells[1, 15].Value = "POQty";
                    workSheet.Cells[1, 16].Value = "GRQty";
                    workSheet.Cells[1, 17].Value = "BalancePO";
                    workSheet.Cells[1, 18].Value = "GINormal";
                    workSheet.Cells[1, 19].Value = "GINormal - StdQtyReal";
                    workSheet.Cells[1, 20].Value = "GIOver";
                    workSheet.Cells[1, 21].Value = "GITrial";
                    workSheet.Cells[1, 22].Value = "GIDefMat";
                    workSheet.Cells[1, 23].Value = "GIDefWork";
                    workSheet.Cells[1, 24].Value = "GIDefLost";
                    workSheet.Cells[1, 25].Value = "GIDefBuyer";
                    workSheet.Cells[1, 26].Value = "GITotal";
                    workSheet.Cells[1, 27].Value = "LossRateReal";
                    workSheet.Cells[1, 28].Value = "MatReturn";

                    int recordIndex = 2;
                    foreach (SQMatMoveModel item in result)
                    {
                        workSheet.Cells[recordIndex, 1].Value = item.RefVKNo;
                        workSheet.Cells[recordIndex, 2].Value = item.MfgVendor;
                        workSheet.Cells[recordIndex, 3].Value = item.Shipper;
                        workSheet.Cells[recordIndex, 4].Value = item.MatCD;
                        workSheet.Cells[recordIndex, 5].Value = item.MatDesc;
                        workSheet.Cells[recordIndex, 6].Value = item.Color;
                        workSheet.Cells[recordIndex, 7].Value = item.PUnit;

                        workSheet.Cells[recordIndex, 8].Value = item.LossRate / 100;
                        workSheet.Cells[recordIndex, 8].Style.Numberformat.Format = "#0.00%";
                        workSheet.Cells[recordIndex, 9].Value = item.LossQty;
                        workSheet.Cells[recordIndex, 10].Value = item.AddQty;

                        workSheet.Cells[recordIndex, 11].Value = item.LeftOverQty;
                        workSheet.Cells[recordIndex, 12].Value = item.StdQtyReal;
                        workSheet.Cells[recordIndex, 13].Value = item.StdQty;
                        workSheet.Cells[recordIndex, 14].Value = item.ActQty;
                        workSheet.Cells[recordIndex, 15].Value = item.POQty;
                        workSheet.Cells[recordIndex, 16].Value = item.GRQty;
                        workSheet.Cells[recordIndex, 17].Value = item.BalancePO;
                        workSheet.Cells[recordIndex, 18].Value = item.GINormal;
                        workSheet.Cells[recordIndex, 19].Value = item.GINormal_StdQtyReal;
                        workSheet.Cells[recordIndex, 20].Value = item.GIOver;
                        workSheet.Cells[recordIndex, 21].Value = item.GITrial;
                        workSheet.Cells[recordIndex, 22].Value = item.GIDefMat;
                        workSheet.Cells[recordIndex, 23].Value = item.GIDefWork;
                        workSheet.Cells[recordIndex, 24].Value = item.GIDefLost;
                        workSheet.Cells[recordIndex, 25].Value = item.GIDefBuyer;
                        workSheet.Cells[recordIndex, 26].Value = item.GITotal;
                        workSheet.Cells[recordIndex, 27].Value = item.LossRateReal/100;
                        workSheet.Cells[recordIndex, 27].Style.Numberformat.Format = "#0.00%";
                        workSheet.Cells[recordIndex, 28].Value = item.MatReturn;
                        recordIndex++;
                    }
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    string filename = "MatMove" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                    Byte[] bytes = excel.GetAsByteArray();
                    response.Content = new ByteArrayContent(bytes);
                    response.Content.Headers.ContentLength = bytes.LongLength;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = filename;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));
                    return response;
                
                }


            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.ToString());
            }
        }

    }
}

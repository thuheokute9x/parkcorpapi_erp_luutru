//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HR.API.Models.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TRqGIMat
    {
        public int RqGIMatID { get; set; }
        public int RqGIID { get; set; }
        public string MatCD { get; set; }
        public float MatQty { get; set; }
    
        public virtual TRqGI TRqGI { get; set; }
    }
}

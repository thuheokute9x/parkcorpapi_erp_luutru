//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HR.API.Models.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TPR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TPR()
        {
            this.TMatPOes = new HashSet<TMatPO>();
            this.TPRProdInsts = new HashSet<TPRProdInst>();
        }
    
        public int PRID { get; set; }
        public Nullable<int> PRStatusID { get; set; }
        public short StatusID { get; set; }
        public string RefVKNo { get; set; }
        public string Remark { get; set; }
        public Nullable<bool> ModifiedCons { get; set; }
        public bool Closed { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TMatPO> TMatPOes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TPRProdInst> TPRProdInsts { get; set; }
    }
}

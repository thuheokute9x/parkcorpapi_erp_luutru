//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HR.API.Models.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TPay
    {
        public int PayID { get; set; }
        public string Beneficialry { get; set; }
        public string PayDocType { get; set; }
        public Nullable<int> PIID { get; set; }
        public Nullable<int> IVID { get; set; }
        public System.DateTime DocDate { get; set; }
        public short CrDr { get; set; }
        public float PayAmt { get; set; }
        public string CurrCD { get; set; }
        public System.DateTime DueDate { get; set; }
        public string PayMthCD { get; set; }
        public int PayCondID { get; set; }
        public short PayTerm { get; set; }
        public string AccountNo { get; set; }
        public string RefDocNo { get; set; }
        public string LCNo { get; set; }
        public string LCVerifCD { get; set; }
        public Nullable<System.DateTime> PaidOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    
        public virtual TIV TIV { get; set; }
        public virtual TPayCond TPayCond { get; set; }
        public virtual TPayMth TPayMth { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HR.API.Models.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class WTFGMonthCnt
    {
        public int MonthCntID { get; set; }
        public Nullable<int> LIYear { get; set; }
        public Nullable<short> LIMonth { get; set; }
        public Nullable<short> LIDay { get; set; }
        public string FactoryCD { get; set; }
        public string LineNo { get; set; }
        public string CustGrpCD { get; set; }
        public string ProdInstCD { get; set; }
        public string ProdCode { get; set; }
        public string StyleCode { get; set; }
        public string ColorCode { get; set; }
        public Nullable<int> FGCount { get; set; }
        public Nullable<int> OT { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HR.API.Models.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class STCustomsMat_New
    {
        public string MATCD { get; set; }
        public int CustomsCD { get; set; }
        public string PUnit { get; set; }
        public string CustomsUnit { get; set; }
        public Nullable<double> ConvFactor { get; set; }
        public string Payment { get; set; }
        public string Remark { get; set; }
    }
}

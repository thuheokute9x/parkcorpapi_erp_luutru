//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HR.API.Models.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TPlantType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TPlantType()
        {
            this.TPlants = new HashSet<TPlant>();
        }
    
        public int PlantTypeID { get; set; }
        public string TypeName { get; set; }
        public bool ApplyMAPrice { get; set; }
        public bool GIDoc { get; set; }
        public bool GRDoc { get; set; }
        public bool SubcontList { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TPlant> TPlants { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HR.API.Models.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TTMenuWebERP
    {
        public int MenuID { get; set; }
        public string FormName { get; set; }
        public string FormNameDesc { get; set; }
        public Nullable<int> Level { get; set; }
        public Nullable<int> Range { get; set; }
        public Nullable<int> ParentID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool Discontinue { get; set; }
        public Nullable<bool> IsCheck { get; set; }
    }
}

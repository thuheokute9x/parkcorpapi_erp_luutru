//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HR.API.Models.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TAssyCon
    {
        public int AssyConsID { get; set; }
        public int AssyBOMID { get; set; }
        public int AssyProdID { get; set; }
        public short Version { get; set; }
        public string AMatCD { get; set; }
        public string MatCD { get; set; }
        public float YieldPrs { get; set; }
        public string ProcCD { get; set; }
    
        public virtual TBOM TBOM { get; set; }
        public virtual TProc TProc { get; set; }
        public virtual TProd TProd { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HR.API.Models.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class STPrepDateLineItemComponent
    {
        public int DatePrepLICIDID { get; set; }
        public System.DateTime DatePrepLICID { get; set; }
        public int PrepLICID { get; set; }
        public Nullable<int> C01 { get; set; }
        public Nullable<int> C02 { get; set; }
        public Nullable<int> C03 { get; set; }
        public Nullable<int> C04 { get; set; }
        public Nullable<int> C05 { get; set; }
        public Nullable<int> C06 { get; set; }
        public Nullable<int> C07 { get; set; }
        public Nullable<int> C08 { get; set; }
        public Nullable<int> C09 { get; set; }
        public Nullable<int> C10 { get; set; }
        public Nullable<int> C11 { get; set; }
        public Nullable<int> C12 { get; set; }
        public Nullable<int> C13 { get; set; }
        public Nullable<int> C14 { get; set; }
        public Nullable<int> C15 { get; set; }
        public Nullable<int> QtyTG { get; set; }
        public Nullable<int> Qty { get; set; }
        public Nullable<float> HourWorking { get; set; }
        public string Remark { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}

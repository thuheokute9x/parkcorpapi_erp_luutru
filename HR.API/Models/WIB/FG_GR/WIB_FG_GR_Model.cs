﻿namespace HR.API.Controllers
{
    public class WIB_FG_GRModel
    {
        public int TotalRows { get; set; }
        public int RowID { get; set; }
        public string BrandDesc { get; set; }
        public string ProdInstCD { get; set; }
        public string SKUCode { get; set; }
        public float? OrderQty { get; set; }
        public float? FGQty { get; set; }
        public float? Balance { get; set; }
        public string ProdCode { get; set; }
        //public float? PARKERP { get; set; }
        //public float? QTYCuting { get; set; }
        //public float? QTYSubStore { get; set; }
        //public string CompletePO { get; set; }

    }
}

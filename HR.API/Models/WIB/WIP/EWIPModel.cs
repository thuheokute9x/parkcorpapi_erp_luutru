﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.WIB.WIP
{
    public class EWIPModel
    {
        public string ProdInstCD { get; set; }
        public string MatCD { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public string MH1 { get; set; }
        public string PUNIT { get; set; }

        public double? PriceLotSize { get; set; }
        public string PriceLotSizestring
        {
            get
            {
                if (this.PriceLotSize.HasValue)
                {
                    if (this.PriceLotSize.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceLotSize.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PriceLotSize.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PriceLotSize.Value);
                }
                else
                    return "0";
            }
        }

        public double? PriceIncoterms { get; set; }
        public string PriceIncotermsstring
        {
            get
            {
                if (this.PriceIncoterms.HasValue)
                {
                    if (this.PriceIncoterms.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceIncoterms.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceIncoterms.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceIncoterms.Value);
                }
                else
                    return "0";
            }
        }

        public double? PriceIncotermsUSD { get; set; }
        public string PriceIncotermsUSDstring
        {
            get
            {
                if (this.PriceIncotermsUSD.HasValue)
                {
                    if (this.PriceIncotermsUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceIncotermsUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceIncotermsUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceIncotermsUSD.Value);
                }
                else
                    return "";
            }
        }

        public string CIFCurr { get; set; }

        public double? CIFPriceUSD { get; set; }
        public string CIFPriceUSDstring
        {
            get
            {
                if (this.CIFPriceUSD.HasValue)
                {
                    if (this.CIFPriceUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.CIFPriceUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.CIFPriceUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.CIFPriceUSD.Value);
                }
                else
                    return "0";
            }
        }

        public string CURR { get; set; }
        public DateTime IDATE { get; set; }
        public string IDATEConvert { get; set; }
        public double? PRICEGR { get; set; }
        public string PRICEGRstring
        {
            get
            {
                if (this.PRICEGR.HasValue)
                {
                    if (this.PRICEGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICEGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICEGR.Value);
                }
                else
                    return "0";
            }
        }
        public double? Comp { get; set; }
        public string Compstring
        {
            get
            {
                if (this.Comp.HasValue)
                {
                    if (this.Comp.Value == 0)
                    {
                        return "0" + '%';
                    }
                    int value;
                    if (int.TryParse(this.Comp.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0}" + '%', this.Comp.Value);
                    }
                    return string.Format("{0:#,##0}" + '%', this.Comp.Value);
                }
                else
                    return "";
            }
        }
        public double? PRICE { get; set; }
        public string PRICEstring
        {
            get
            {
                if (this.PRICE.HasValue)
                {
                    if (this.PRICE.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICE.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICE.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICE.Value);
                }
                else
                    return "0";
            }
        }
        public double? PRICEUSD { get; set; }
        public string PRICEUSDstring
        {
            get
            {
                if (this.PRICEUSD.HasValue)
                {
                    if (this.PRICEUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICEUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICEUSD.Value);
                }
                else
                    return "0";
            }
        }
        public double? ExRate { get; set; }
        public string ExRatestring
        {
            get
            {
                if (this.ExRate.HasValue)
                {
                    if (this.ExRate.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ExRate.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ExRate.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ExRate.Value);
                }
                else
                    return "0";
            }
        }

        public string Payment { get; set; }
        public double? QtyEWIB { get; set; }
        public string QtyEWIBstring
        {
            get
            {
                if (this.QtyEWIB.HasValue)
                {
                    if (this.QtyEWIB.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyEWIB.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.QtyEWIB.Value);
                    }
                    return string.Format("{0:#,0.00}", this.QtyEWIB.Value);
                }
                else
                    return "0";
            }
        }
        public double? PRICEEWIB { get; set; }
        public string PRICEEWIBstring
        {
            get
            {
                if (this.PRICEEWIB.HasValue)
                {
                    if (this.PRICEEWIB.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEEWIB.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PRICEEWIB.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PRICEEWIB.Value);
                }
                else
                    return "0";
            }
        }


        public double? QtyBWIP { get; set; }
        public string QtyBWIPstring
        {
            get
            {
                if (this.QtyBWIP.HasValue)
                {
                    if (this.QtyBWIP.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyBWIP.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.QtyBWIP.Value);
                    }
                    return string.Format("{0:#,0.00}", this.QtyBWIP.Value);
                }
                else
                    return "0";
            }
        }

        public double? PRICEBWIP { get; set; }
        public string PRICEBWIPstring
        {
            get
            {
                if (this.PRICEBWIP.HasValue)
                {
                    if (this.PRICEBWIP.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEBWIP.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PRICEBWIP.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PRICEBWIP.Value);
                }
                else
                    return "0";
            }
        }

        public double? QtyGI { get; set; }
        public string QtyGIstring
        {
            get
            {
                if (this.QtyGI.HasValue)
                {
                    if (this.QtyGI.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyGI.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.QtyGI.Value);
                    }
                    return string.Format("{0:#,0.00}", this.QtyGI.Value);
                }
                else
                    return "0";
            }
        }


        public double? PRICEGI { get; set; }
        public string PRICEGIstring
        {
            get
            {
                if (this.PRICEGI.HasValue)
                {
                    if (this.PRICEGI.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEGI.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PRICEGI.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PRICEGI.Value);
                }
                else
                    return "0";
            }
        }


        public double? QtyFGGR_Cons { get; set; }
        public string QtyFGGR_Consstring
        {
            get
            {
                if (this.QtyFGGR_Cons.HasValue)
                {
                    if (this.QtyFGGR_Cons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyFGGR_Cons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.QtyFGGR_Cons.Value);
                    }
                    return string.Format("{0:#,0.00}", this.QtyFGGR_Cons.Value);
                }
                else
                    return "0";
            }
        }

        public double? PRICEFGGR_Cons { get; set; }
        public string PRICEFGGR_Consstring
        {
            get
            {
                if (this.PRICEFGGR_Cons.HasValue)
                {
                    if (this.PRICEFGGR_Cons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEFGGR_Cons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PRICEFGGR_Cons.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PRICEFGGR_Cons.Value);
                }
                else
                    return "0";
            }
        }

        public double? QtyUnCut { get; set; }
        public string QtyUnCutstring
        {
            get
            {
                if (this.QtyUnCut.HasValue)
                {
                    if (this.QtyUnCut.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyUnCut.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.QtyUnCut.Value);
                    }
                    return string.Format("{0:#,0.00}", this.QtyUnCut.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtUnCut { get; set; }
        public string PriceUnCutstring
        {
            get
            {
                if (this.AmtUnCut.HasValue)
                {
                    if (this.AmtUnCut.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtUnCut.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtUnCut.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtUnCut.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAmtUnCut { get; set; }
        public string SumAmtUnCutstring
        {
            get
            {
                if (this.SumAmtUnCut.HasValue)
                {
                    if (this.SumAmtUnCut.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtUnCut.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmtUnCut.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmtUnCut.Value);
                }
                else
                    return "";
            }
        }

        public double? QtySubStore { get; set; }
        public string QtySubStorestring
        {
            get
            {
                if (this.QtySubStore.HasValue)
                {
                    if (this.QtySubStore.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtySubStore.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.QtySubStore.Value);
                    }
                    return string.Format("{0:#,0.00}", this.QtySubStore.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtSubStore { get; set; }
        public string AmtSubStorestring
        {
            get
            {
                if (this.AmtSubStore.HasValue)
                {
                    if (this.AmtSubStore.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtSubStore.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtSubStore.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtSubStore.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAmtSubStore { get; set; }
        public string SumAmtSubStorestring
        {
            get
            {
                if (this.SumAmtSubStore.HasValue)
                {
                    if (this.SumAmtSubStore.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtSubStore.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmtSubStore.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmtSubStore.Value);
                }
                else
                    return "";
            }
        }
        public double? SumQtyEWIB { get; set; }
        public string SumQtyEWIBstring
        {
            get
            {
                if (this.SumQtyEWIB.HasValue)
                {
                    if (this.SumQtyEWIB.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumQtyEWIB.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumQtyEWIB.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumQtyEWIB.Value);
                }
                else
                    return "0";
            }
        }

        public double? SumPRICEEWIB { get; set; }
        public string SumPRICEEWIBstring
        {
            get
            {
                if (this.SumPRICEEWIB.HasValue)
                {
                    if (this.SumPRICEEWIB.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPRICEEWIB.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumPRICEEWIB.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumPRICEEWIB.Value);
                }
                else
                    return "0";
            }
        }

        public double? SumQtyBWIP { get; set; }
        public string SumQtyBWIPstring
        {
            get
            {
                if (this.SumQtyBWIP.HasValue)
                {
                    if (this.SumQtyBWIP.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumQtyBWIP.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumQtyBWIP.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumQtyBWIP.Value);
                }
                else
                    return "0";
            }
        }

        public double? SumPRICEBWIP { get; set; }
        public string SumPRICEBWIPstring
        {
            get
            {
                if (this.SumPRICEBWIP.HasValue)
                {
                    if (this.SumPRICEBWIP.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPRICEBWIP.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumPRICEBWIP.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumPRICEBWIP.Value);
                }
                else
                    return "0";
            }
        }

        public double? SumQtyGI { get; set; }
        public string SumQtyGIstring
        {
            get
            {
                if (this.SumQtyGI.HasValue)
                {
                    if (this.SumQtyGI.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumQtyGI.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumQtyGI.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumQtyGI.Value);
                }
                else
                    return "";
            }
        }


        public double? SumPRICEGI { get; set; }
        public string SumPRICEGIstring
        {
            get
            {
                if (this.SumPRICEGI.HasValue)
                {
                    if (this.SumPRICEGI.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPRICEGI.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumPRICEGI.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumPRICEGI.Value);
                }
                else
                    return "";
            }
        }

        public double? SumQtyFGGR_Cons { get; set; }
        public string SumQtyFGGR_Consstring
        {
            get
            {
                if (this.SumQtyFGGR_Cons.HasValue)
                {
                    if (this.SumQtyFGGR_Cons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumQtyFGGR_Cons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumQtyFGGR_Cons.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumQtyFGGR_Cons.Value);
                }
                else
                    return "";
            }
        }

        public double? SumPRICEFGGR_Cons { get; set; }
        public string SumPRICEFGGR_Consstring
        {
            get
            {
                if (this.SumPRICEFGGR_Cons.HasValue)
                {
                    if (this.SumPRICEFGGR_Cons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPRICEFGGR_Cons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumPRICEFGGR_Cons.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumPRICEFGGR_Cons.Value);
                }
                else
                    return "";
            }
        }

        public double? SumQtyUnCut { get; set; }
        public string SumQtyUnCutstring
        {
            get
            {
                if (this.SumQtyUnCut.HasValue)
                {
                    if (this.SumQtyUnCut.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumQtyUnCut.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumQtyUnCut.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumQtyUnCut.Value);
                }
                else
                    return "";
            }
        }

        public double? SumQtySubStore { get; set; }
        public string SumQtySubStorestring
        {
            get
            {
                if (this.SumQtySubStore.HasValue)
                {
                    if (this.SumQtySubStore.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumQtySubStore.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumQtySubStore.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumQtySubStore.Value);
                }
                else
                    return "";
            }
        }















        #region GroupProdOrder
        public double? PRICEBWIP_Group { get; set; }
        public string PRICEBWIP_Groupstring
        {
            get
            {
                if (this.PRICEBWIP_Group.HasValue)
                {
                    if (this.PRICEBWIP_Group.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEBWIP_Group.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PRICEBWIP_Group.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PRICEBWIP_Group.Value);
                }
                else
                    return "";
            }
        }

        public double? PRICEGI_Group { get; set; }
        public string PRICEGI_Groupstring
        {
            get
            {
                if (this.PRICEGI_Group.HasValue)
                {
                    if (this.PRICEGI_Group.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEGI_Group.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PRICEGI_Group.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PRICEGI_Group.Value);
                }
                else
                    return "";
            }
        }

        public double? PRICEUnCut_Group { get; set; }
        public string PRICEUnCut_Groupstring
        {
            get
            {
                if (this.PRICEUnCut_Group.HasValue)
                {
                    if (this.PRICEUnCut_Group.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEUnCut_Group.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PRICEUnCut_Group.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PRICEUnCut_Group.Value);
                }
                else
                    return "";
            }
        }

        public double? PRICEEWIB_Group { get; set; }
        public string PRICEEWIB_Groupstring
        {
            get
            {
                if (this.PRICEEWIB_Group.HasValue)
                {
                    if (this.PRICEEWIB_Group.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEEWIB_Group.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PRICEEWIB_Group.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PRICEEWIB_Group.Value);
                }
                else
                    return "";
            }
        }

        public double? PPLMatCost { get; set; }
        public string PPLMatCoststring
        {
            get
            {
                if (this.PPLMatCost.HasValue)
                {
                    if (this.PPLMatCost.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PPLMatCost.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PPLMatCost.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PPLMatCost.Value);
                }
                else
                    return "";
            }
        }

        public double? StdMatCostLoss { get; set; }
        public string StdMatCostLossstring
        {
            get
            {
                if (this.StdMatCostLoss.HasValue)
                {
                    if (this.StdMatCostLoss.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.StdMatCostLoss.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.StdMatCostLoss.Value);
                    }
                    return string.Format("{0:#,0.00}", this.StdMatCostLoss.Value);
                }
                else
                    return "";
            }
        }

        public double? StdMatCostAL { get; set; }
        public string StdMatCostALstring
        {
            get
            {
                if (this.StdMatCostAL.HasValue)
                {
                    if (this.StdMatCostAL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.StdMatCostAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.StdMatCostAL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.StdMatCostAL.Value);
                }
                else
                    return "";
            }
        }

        public double? ActMatCost { get; set; }
        public string ActMatCoststring
        {
            get
            {
                if (this.ActMatCost.HasValue)
                {
                    if (this.ActMatCost.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ActMatCost.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.ActMatCost.Value);
                    }
                    return string.Format("{0:#,0.00}", this.ActMatCost.Value);
                }
                else
                    return "";
            }
        }

        public double? PPL_Act { get; set; }
        public string PPL_Actstring
        {
            get
            {
                if (this.PPL_Act.HasValue)
                {
                    if (this.PPL_Act.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PPL_Act.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PPL_Act.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PPL_Act.Value);
                }
                else
                    return "";
            }
        }

        public double? PPL_Act_Percent_PPL { get; set; }
        public string PPL_Act_Percent_PPLstring
        {
            get
            {
                if (this.PPL_Act_Percent_PPL.HasValue)
                {
                    if (this.PPL_Act_Percent_PPL.Value == 0)
                    {
                        return "0" + "%";
                    }
                    int value;
                    if (int.TryParse(this.PPL_Act_Percent_PPL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0}", this.PPL_Act_Percent_PPL.Value) + "%";
                    }
                    return string.Format("{0:#,0}", this.PPL_Act_Percent_PPL.Value) + "%";
                }
                else
                    return "";
            }
        }

        public double? Std_Act { get; set; }
        public string Std_Actstring
        {
            get
            {
                if (this.Std_Act.HasValue)
                {
                    if (this.Std_Act.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Std_Act.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Std_Act.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Std_Act.Value);
                }
                else
                    return "";
            }
        }

        public double? Std_Act_Percent_PPL { get; set; }
        public string Std_Act_Percent_PPLstring
        {
            get
            {
                if (this.Std_Act_Percent_PPL.HasValue)
                {
                    if (this.Std_Act_Percent_PPL.Value == 0)
                    {
                        return "0" + "%";
                    }
                    int value;
                    if (int.TryParse(this.Std_Act_Percent_PPL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0}", this.Std_Act_Percent_PPL.Value) + "%";
                    }
                    return string.Format("{0:#,0}", this.Std_Act_Percent_PPL.Value) + "%";
                }
                else
                    return "";
            }
        }

        public double? PPL_StdAL { get; set; }
        public string PPL_StdALstring
        {
            get
            {
                if (this.PPL_StdAL.HasValue)
                {
                    if (this.PPL_StdAL.Value == 0)
                    {
                        return "";
                    }
                    int value;
                    if (int.TryParse(this.PPL_StdAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PPL_StdAL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PPL_StdAL.Value);
                }
                else
                    return "";
            }
        }
        public double? PPL_StdAL_Percent_PPL { get; set; }
        public string PPL_StdAL_Percent_PPLstring
        {
            get
            {
                if (this.PPL_StdAL_Percent_PPL.HasValue)
                {
                    if (this.PPL_StdAL_Percent_PPL.Value == 0)
                    {
                        return "0" + "%";
                    }
                    int value;
                    if (int.TryParse(this.PPL_StdAL_Percent_PPL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0}", this.PPL_StdAL_Percent_PPL.Value) + "%";
                    }
                    return string.Format("{0:#,0}", this.PPL_StdAL_Percent_PPL.Value) + "%";
                }
                else
                    return "";
            }
        }
        public double? OrderQtySum { get; set; }
        public string OrderQtySumstring
        {
            get
            {
                if (this.OrderQtySum.HasValue)
                {
                    if (this.OrderQtySum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.OrderQtySum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.OrderQtySum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.OrderQtySum.Value);
                }
                else
                    return "";
            }
        }

        public double? FGQtySum { get; set; }
        public string FGQtySumstring
        {
            get
            {
                if (this.FGQtySum.HasValue)
                {
                    if (this.FGQtySum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.FGQtySum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.FGQtySum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.FGQtySum.Value);
                }
                else
                    return "";
            }
        }

        public double? SumPRICEBWIP_Group { get; set; }
        public string SumPRICEBWIP_Groupstring
        {
            get
            {
                if (this.SumPRICEBWIP_Group.HasValue)
                {
                    if (this.SumPRICEBWIP_Group.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPRICEBWIP_Group.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumPRICEBWIP_Group.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumPRICEBWIP_Group.Value);
                }
                else
                    return "";
            }
        }
        public double? SumPRICEGI_Group { get; set; }
        public string SumPRICEGI_Groupstring
        {
            get
            {
                if (this.SumPRICEGI_Group.HasValue)
                {
                    if (this.SumPRICEGI_Group.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPRICEGI_Group.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumPRICEGI_Group.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumPRICEGI_Group.Value);
                }
                else
                    return "";
            }
        }
        public double? SumPRICEUnCut_Group { get; set; }
        public string SumPRICEUnCut_Groupstring
        {
            get
            {
                if (this.SumPRICEUnCut_Group.HasValue)
                {
                    if (this.SumPRICEUnCut_Group.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPRICEUnCut_Group.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumPRICEUnCut_Group.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumPRICEUnCut_Group.Value);
                }
                else
                    return "";
            }
        }
        public double? SumPRICEEWIB_Group { get; set; }
        public string SumPRICEEWIB_Groupstring
        {
            get
            {
                if (this.SumPRICEEWIB_Group.HasValue)
                {
                    if (this.SumPRICEEWIB_Group.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPRICEEWIB_Group.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumPRICEEWIB_Group.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumPRICEEWIB_Group.Value);
                }
                else
                    return "";
            }
        }
        public double? SumPPLMatCost { get; set; }
        public string SumPPLMatCoststring
        {
            get
            {
                if (this.SumPPLMatCost.HasValue)
                {
                    if (this.SumPPLMatCost.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPPLMatCost.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumPPLMatCost.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumPPLMatCost.Value);
                }
                else
                    return "";
            }
        }
        public double? SumStdMatCostLoss { get; set; }
        public string SumStdMatCostLossstring
        {
            get
            {
                if (this.SumStdMatCostLoss.HasValue)
                {
                    if (this.SumStdMatCostLoss.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumStdMatCostLoss.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumStdMatCostLoss.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumStdMatCostLoss.Value);
                }
                else
                    return "";
            }
        }
        public double? SumStdMatCostAL { get; set; }
        public string SumStdMatCostALstring
        {
            get
            {
                if (this.SumStdMatCostAL.HasValue)
                {
                    if (this.SumStdMatCostAL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumStdMatCostAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumStdMatCostAL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumStdMatCostAL.Value);
                }
                else
                    return "";
            }
        }

        public double? SumActMatCost { get; set; }
        public string SumActMatCoststring
        {
            get
            {
                if (this.SumActMatCost.HasValue)
                {
                    if (this.SumActMatCost.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumActMatCost.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumActMatCost.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumActMatCost.Value);
                }
                else
                    return "";
            }
        }

        public double? SumPPL_Act { get; set; }
        public string SumPPL_Actstring
        {
            get
            {
                if (this.SumPPL_Act.HasValue)
                {
                    if (this.SumPPL_Act.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPPL_Act.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumPPL_Act.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumPPL_Act.Value);
                }
                else
                    return "";
            }
        }

        public double? SumPPL_Act_Percent_PPL { get; set; }
        public string SumPPL_Act_Percent_PPLstring
        {
            get
            {
                if (this.SumPPL_Act_Percent_PPL.HasValue)
                {
                    if (this.SumPPL_Act_Percent_PPL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPPL_Act_Percent_PPL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0}", this.SumPPL_Act_Percent_PPL.Value);
                    }
                    return string.Format("{0:#,0}", this.SumPPL_Act_Percent_PPL.Value);
                }
                else
                    return "";
            }
        }


        public double? SumStd_Act { get; set; }
        public string SumStd_Actstring
        {
            get
            {
                if (this.SumStd_Act.HasValue)
                {
                    if (this.SumStd_Act.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumStd_Act.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumStd_Act.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumStd_Act.Value);
                }
                else
                    return "";
            }
        }
        public double? SumStd_Act_Percent_PPL { get; set; }
        public string SumStd_Act_Percent_PPLstring
        {
            get
            {
                if (this.SumStd_Act_Percent_PPL.HasValue)
                {
                    if (this.SumStd_Act_Percent_PPL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumStd_Act_Percent_PPL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0}", this.SumStd_Act_Percent_PPL.Value);
                    }
                    return string.Format("{0:#,0}", this.SumStd_Act_Percent_PPL.Value);
                }
                else
                    return "";
            }
        }

        public double? SumPPL_StdAL { get; set; }
        public string SumPPL_StdALstring
        {
            get
            {
                if (this.SumPPL_StdAL.HasValue)
                {
                    if (this.SumPPL_StdAL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPPL_StdAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumPPL_StdAL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumPPL_StdAL.Value);
                }
                else
                    return "";
            }
        }
        public double? SumPPL_StdAL_Percent_PPL { get; set; }
        public string SumPPL_StdAL_Percent_PPLstring
        {
            get
            {
                if (this.SumPPL_StdAL_Percent_PPL.HasValue)
                {
                    if (this.SumPPL_StdAL_Percent_PPL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumPPL_StdAL_Percent_PPL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0}", this.SumPPL_StdAL_Percent_PPL.Value) ;
                    }
                    return string.Format("{0:#,0}", this.SumPPL_StdAL_Percent_PPL.Value);
                }
                else
                    return "";
            }
        }
        public double? SumOrderQtySum { get; set; }
        public string SumOrderQtySumstring
        {
            get
            {
                if (this.SumOrderQtySum.HasValue)
                {
                    if (this.SumOrderQtySum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumOrderQtySum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumOrderQtySum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumOrderQtySum.Value);
                }
                else
                    return "";
            }
        }

        public double? SumFGQtySum { get; set; }
        public string SumFGQtySumstring
        {
            get
            {
                if (this.SumFGQtySum.HasValue)
                {
                    if (this.SumFGQtySum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumFGQtySum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumFGQtySum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumFGQtySum.Value);
                }
                else
                    return "";
            }
        }
        public Boolean Leather { get; set; }
        public Boolean Costing { get; set; }
        public string ConsType { get; set; }

        #endregion

        public string List { get; set; }
        public string Condition { get; set; }
        public string Result { get; set; }
        public int IsErr { get; set; }

        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Boolean ClosedOnFGGR { get; set; }
        


    }
}
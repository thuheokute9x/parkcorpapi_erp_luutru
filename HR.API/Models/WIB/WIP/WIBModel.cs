﻿namespace HR.API.Controllers
{
    public class WIBModel
    {
        public string ProdInstCD { get; set; }
        public string MatCD { get; set; }
        public string MatName { get; set; }
        public string Color { get; set; }
        public string PUNIT { get; set; }
        public double? PRICE { get; set; }
        public string PRICEstring
        {
            get
            {
                if (this.PRICE.HasValue)
                {
                    if (this.PRICE.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICE.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICE.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICE.Value);
                }
                else
                    return "0";
            }
        }
        public double? GIQty { get; set; }
        public string GIQtystring
        {
            get
            {
                if (this.GIQty.HasValue)
                {
                    if (this.GIQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GIQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GIQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.GIQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? GIAmt { get; set; }
        public string GIAmtstring
        {
            get
            {
                if (this.GIAmt.HasValue)
                {
                    if (this.GIAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GIAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GIAmt.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.GIAmt.Value);
                }
                else
                    return "0";
            }
        }
        public double? FGGRQty { get; set; }
        public string FGGRQtystring
        {
            get
            {
                if (this.FGGRQty.HasValue)
                {
                    if (this.FGGRQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.FGGRQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.FGGRQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.FGGRQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? FGGRAmt { get; set; }
        public string FGGRAmtstring
        {
            get
            {
                if (this.FGGRAmt.HasValue)
                {
                    if (this.FGGRAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.FGGRAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.FGGRAmt.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.FGGRAmt.Value);
                }
                else
                    return "0";
            }
        }
        public double? WIPQTY { get; set; }
        public string WIPQTYstring
        {
            get
            {
                if (this.WIPQTY.HasValue)
                {
                    if (this.WIPQTY.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.WIPQTY.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.WIPQTY.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.WIPQTY.Value);
                }
                else
                    return "0";
            }
        }
        public double? WIPAmt { get; set; }
        public string WIPAmtstring
        {
            get
            {
                if (this.WIPAmt.HasValue)
                {
                    if (this.WIPAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.WIPAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.WIPAmt.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.WIPAmt.Value);
                }
                else
                    return "0";
            }
        }
        public string CURR { get; set; }
        public string Payment { get; set; }
        public int TotalRows { get; set; }
        public int RowID { get; set; }
        //public float? PARKERP { get; set; }
        //public float? QTYCuting { get; set; }
        //public float? QTYSubStore { get; set; }
        //public string CompletePO { get; set; }

    }
}

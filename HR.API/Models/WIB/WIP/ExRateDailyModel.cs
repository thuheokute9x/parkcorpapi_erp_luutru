﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.WIB.WIP
{
    public class ExRateDailyModel
    {
        //public string base { get; set; }
        public string end_date { get; set; }
        public dynamic rates { get; set; }
        public string start_date { get; set; }
        public bool success { get; set; }
        public bool timeseries { get; set; }
    }
}
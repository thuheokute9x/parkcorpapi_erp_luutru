﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.WIB.WIPMat
{
    public class TTWIPMat_Custom: TTWIPMat
    {
        public string ProdOrder { get; set; }
        public string RefVKNo { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public string PUNIT { get; set; }
        public string PUnit { get; set; }
        public Double? PRICE { get; set; }
        public string Qtystring
        {
            get
            {
                if (this.Qty.HasValue)
                {
                    if (this.Qty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Qty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Qty.Value);
                    }
                    return string.Format("{0:#,##0.##########################}", this.Qty.Value);

                }
                else
                    return "0";
            }
        }
        public string PRICEstring
        {
            get
            {
                if (this.PRICE.HasValue)
                {
                    if (this.PRICE.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICE.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICE.Value);
                    }
                    return string.Format("{0:#,##0.##########################}", this.PRICE.Value);

                }
                else
                    return "0";
            }
        }
        public Double? Amt { get; set; }
        public string Amtstring
        {
            get
            {
                if (this.Amt.HasValue)
                {
                    if (this.Amt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Amt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Amt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Amt.Value);

                }
                else
                    return "0";
            }
        }
        public string QtyUnCutstring
        {
            get
            {
                if (this.QtyUnCut.HasValue)
                {
                    if (this.QtyUnCut.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyUnCut.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QtyUnCut.Value);
                    }
                    return string.Format("{0:#,##0.##########################}", this.QtyUnCut.Value);

                }
                else
                    return "0";
            }
        }
        public string QtySubStorestring
        {
            get
            {
                if (this.QtySubStore.HasValue)
                {
                    if (this.QtySubStore.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtySubStore.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QtySubStore.Value);
                    }
                    return string.Format("{0:#,##0.##########################}", this.QtySubStore.Value);

                }
                else
                    return "0";
            }
        }































        public string UnitWIP { get; set; }
        public double? QtyWIP { get; set; }
        public string QtyWIPstring
        {
            get
            {
                if (this.QtyWIP.HasValue)
                {
                    if (this.QtyWIP.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyWIP.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QtyWIP.Value);
                    }
                    return string.Format("{0:#,##0.######}", this.QtyWIP.Value);

                }
                else
                    return "0";
            }
        }

        public double? PriceLotSize { get; set; }
        public string PriceLotSizestring
        {
            get
            {
                if (this.PriceLotSize.HasValue)
                {
                    if (this.PriceLotSize.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceLotSize.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceLotSize.Value);
                    }
                    return string.Format("{0:#,##0.######}", this.PriceLotSize.Value);

                }
                else
                    return "0";
            }
        }

        public double? PriceTPInfo { get; set; }
        public string PriceTPInfostring
        {
            get
            {
                if (this.PriceTPInfo.HasValue)
                {
                    if (this.PriceTPInfo.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceTPInfo.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceTPInfo.Value);
                    }
                    return string.Format("{0:#,##0.######}", this.PriceTPInfo.Value);

                }
                else
                    return "0";
            }
        }

        public string CurrTPInfo { get; set; }

        public double? PriceGR { get; set; }
        public string PriceGRstring
        {
            get
            {
                if (this.PriceGR.HasValue)
                {
                    if (this.PriceGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceGR.Value);
                    }
                    return string.Format("{0:#,##0.######}", this.PriceGR.Value);

                }
                else
                    return "0";
            }
        }

        public string CurrGR { get; set; }

        public double? Comp { get; set; }
        public string Compstring
        {
            get
            {
                if (this.Comp.HasValue)
                {
                    if (this.Comp.Value == 0)
                    {
                        return "0" + "%";
                    }
                    int value;
                    if (int.TryParse(this.Comp.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0}" + "%", this.Comp.Value);
                    }
                    return string.Format("{0:#,0}" +"%", this.Comp.Value);

                }
                else
                    return "";
            }
        }

        public double? UnitPRICEUSD { get; set; }
        public string UnitPRICEUSDstring
        {
            get
            {
                if (this.UnitPRICEUSD.HasValue)
                {
                    if (this.UnitPRICEUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.UnitPRICEUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.UnitPRICEUSD.Value);
                    }
                    return string.Format("{0:#,##0.######}", this.UnitPRICEUSD.Value);

                }
                else
                    return "0";
            }
        }

        public string Payment { get; set; }

        public double? QtyPUnit { get; set; }
        public string QtyPUnitstring
        {
            get
            {
                if (this.QtyPUnit.HasValue)
                {
                    if (this.QtyPUnit.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyPUnit.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QtyPUnit.Value);
                    }
                    return string.Format("{0:#,##0.######}", this.QtyPUnit.Value);

                }
                else
                    return "";
            }
        }

        public double? AmtWIP { get; set; }
        public string AmtWIPstring
        {
            get
            {
                if (this.AmtWIP.HasValue)
                {
                    if (this.AmtWIP.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtWIP.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtWIP.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtWIP.Value);

                }
                else
                    return "";
            }
        }
        public double? Total_QtyPUnit { get; set; }
        public string Total_QtyPUnitstring
        {
            get
            {
                if (this.Total_QtyPUnit.HasValue)
                {
                    if (this.Total_QtyPUnit.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Total_QtyPUnit.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Total_QtyPUnit.Value);
                    }
                    return string.Format("{0:#,##0.######}", this.Total_QtyPUnit.Value);

                }
                else
                    return "";
            }
        }
        public double? Total_AmtWIP { get; set; }
        public string Total_AmtWIPstring
        {
            get
            {
                if (this.Total_AmtWIP.HasValue)
                {
                    if (this.Total_AmtWIP.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Total_AmtWIP.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Total_AmtWIP.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Total_AmtWIP.Value);

                }
                else
                    return "";
            }
        }


        public int TotalRows { get; set; }
        public int RowID { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public DateTime? Date { get; set; }
    }
}
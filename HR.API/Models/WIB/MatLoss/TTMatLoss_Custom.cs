﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.WIB.MatLoss
{
    public class TTMatLoss_Custom: TTMatLoss
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public string BrandCD { get; set; }
        public string ProdOrder { get; set; }
        public string ProdDesc { get; set; }
        public string SKUCode { get; set; }
        public string ColorName { get; set; }
        public string ColorCode { get; set; }
        public double? OrderQty { get; set; }
        public string OrderQtystring
        {
            get
            {
                if (this.OrderQty.HasValue)
                {
                    if (this.OrderQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.OrderQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.OrderQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.OrderQty.Value);
                }
                else
                    return "";
            }
        }
        public string MatDesc { get; set; }



        public string MatColor { get; set; }
        public string PUnit { get; set; }
        public string VendorCD { get; set; }
        public double? ConsNL { get; set; }
        public string ConsNLstring
        {
            get
            {
                if (this.ConsNL.HasValue)
                {
                    if (this.ConsNL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsNL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsNL.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsNL.Value);
                }
                else
                    return "";
            }
        }

        public string LossRatestring
        {
            get
            {
                    if (this.LossRate == 0)
                    {
                        return "0%";
                    }
                    int value;
                    if (int.TryParse(this.LossRate.ToString(), out value))
                    {
                        return string.Format("{0:#,##0}" + '%', this.LossRate);
                    }
                    return string.Format("{0:#,##0}" + '%', this.LossRate);
            }
        }
        public double? ConsLL { get; set; }
        public string ConsLLstring
        {
            get
            {
                if (this.ConsLL.HasValue)
                {
                    if (this.ConsLL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsLL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsLL.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsLL.Value);
                }
                else
                    return "";
            }
        }
        public double? ConsOrder { get; set; }
        public string ConsOrderstring
        {
            get
            {
                if (this.ConsOrder.HasValue)
                {
                    if (this.ConsOrder.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsOrder.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsOrder.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsOrder.Value);
                }
                else
                    return "";
            }
        }
        public Boolean IsLossRate { get; set; }
        public Double? LossRateNull { get; set; }
        

    }
}
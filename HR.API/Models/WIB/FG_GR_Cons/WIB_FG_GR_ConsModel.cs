﻿namespace HR.API.Controllers
{
    public class WIB_FG_GR_ConsModel
    {
        public int TotalRows { get; set; }
        public int RowID { get; set; }
        public string ProdInstCD { get; set; }
        public string ProdCode { get; set; } 
        public string SKUCode { get; set; }
        public string MatCD { get; set; }
        public string MatName { get; set; }
        public string Color { get; set; }
        public string MH1 { get; set; }
        public string PUNIT { get; set; }
        public double? QTY { get; set; }
        public string QTYstring
        {
            get
            {
                if (this.QTY.HasValue)
                {
                    if (this.QTY.Value==0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QTY.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QTY.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QTY.Value); 

                }
                else
                    return "0";
            }
        }
        public double? FGQty { get; set; }
        public string FGQtystring
        {
            get
            {
                if (this.FGQty.HasValue)
                {
                    if (this.FGQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.FGQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.FGQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.FGQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? ShipQty { get; set; }
        public string ShipQtystring
        {
            get
            {
                if (this.ShipQty.HasValue)
                {
                    if (this.ShipQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ShipQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ShipQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ShipQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? Consumption { get; set; }
        public string Consumptionstring
        {
            get
            {
                if (this.Consumption.HasValue)
                {
                    if (this.Consumption.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Consumption.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Consumption.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Consumption.Value);
                }
                else
                    return "0";
            }
        }
        public double? ConsLL { get; set; }
        public string ConsLLstring
        {
            get
            {
                if (this.ConsLL.HasValue)
                {
                    if (this.ConsLL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsLL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsLL.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsLL.Value);
                }
                else
                    return "0";
            }
        }
        public double? ConsAL { get; set; }
        public string ConsALstring
        {
            get
            {
                if (this.ConsAL.HasValue)
                {
                    if (this.ConsAL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsAL.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsAL.Value);
                }
                else
                    return "0";
            }
        }
        public double? Loss { get; set; }
        public string Lossstring
        {
            get
            {
                if (this.Loss.HasValue)
                {
                    if (this.Loss.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Loss.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Loss.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Loss.Value);
                }
                else
                    return "0";
            }
        }
        public double? ConsumptionLoss { get; set; }
        public string ConsumptionLossstring
        {
            get
            {
                if (this.ConsumptionLoss.HasValue)
                {
                    if (this.ConsumptionLoss.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsumptionLoss.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsumptionLoss.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsumptionLoss.Value);
                }
                else
                    return "0";
            }
        }
        public double? ConsumptionFinal { get; set; }
        public string ConsumptionFinalstring
        {
            get
            {
                if (this.ConsumptionFinal.HasValue)
                {
                    if (this.ConsumptionFinal.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsumptionFinal.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsumptionFinal.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsumptionFinal.Value);
                }
                else
                    return "0";
            }
        }
        public double? FGGRCons { get; set; }
        public string FGGRConsstring
        {
            get
            {
                if (this.FGGRCons.HasValue)
                {
                    if (this.FGGRCons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.FGGRCons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.FGGRCons.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.FGGRCons.Value);
                }
                else
                    return "0";
            }
        }
        public double? FGGRConsLL { get; set; }
        public string FGGRConsLLstring
        {
            get
            {
                if (this.FGGRConsLL.HasValue)
                {
                    if (this.FGGRConsLL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.FGGRConsLL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.FGGRConsLL.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.FGGRConsLL.Value);
                }
                else
                    return "0";
            }
        }
        public double? FGGRConsAL { get; set; }
        public string FGGRConsALstring
        {
            get
            {
                if (this.FGGRConsAL.HasValue)
                {
                    if (this.FGGRConsAL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.FGGRConsAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.FGGRConsAL.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.FGGRConsAL.Value);
                }
                else
                    return "0";
            }
        }
        public double? ShipCons { get; set; }
        public string ShipConsstring
        {
            get
            {
                if (this.ShipCons.HasValue)
                {
                    if (this.ShipCons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ShipCons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ShipCons.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ShipCons.Value);
                }
                else
                    return "0";
            }
        }
        public double? PRICE { get; set; }
        public string PRICEstring
        {
            get
            {
                if (this.PRICE.HasValue)
                {
                    if (this.PRICE.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICE.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICE.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICE.Value);
                }
                else
                    return "0";
            }
        }
        public double? Amt { get; set; }
        public string Amtstring
        {
            get
            {
                if (this.Amt.HasValue)
                {
                    if (this.Amt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Amt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Amt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Amt.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtLL { get; set; }
        public string AmtLLstring
        {
            get
            {
                if (this.AmtLL.HasValue)
                {
                    if (this.AmtLL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtLL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtLL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtLL.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtAL { get; set; }
        public string AmtALstring
        {
            get
            {
                if (this.AmtAL.HasValue)
                {
                    if (this.AmtAL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtAL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtAL.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtShip { get; set; }
        public string AmtShipstring
        {
            get
            {
                if (this.AmtShip.HasValue)
                {
                    if (this.AmtShip.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtShip.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtShip.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtShip.Value);
                }
                else
                    return "0";
            }
        }
        public string CURR { get; set; }
        public string Payment { get; set; }

        public double? PriceLotSize { get; set; }
        public string PriceLotSizestring
        {
            get
            {
                if (this.PriceLotSize.HasValue)
                {
                    if (this.PriceLotSize.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceLotSize.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceLotSize.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceLotSize.Value);
                }
                else
                    return "0";
            }
        }

        public double? PriceIncoterms { get; set; }
        public string PriceIncotermsstring
        {
            get
            {
                if (this.PriceIncoterms.HasValue)
                {
                    if (this.PriceIncoterms.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceIncoterms.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceIncoterms.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceIncoterms.Value);
                }
                else
                    return "0";
            }
        }

        public string CurrTPInfor { get; set; }

        public double? PriceIncotermsUSD { get; set; }
        public string PriceIncotermsUSDstring
        {
            get
            {
                if (this.PriceIncotermsUSD.HasValue)
                {
                    if (this.PriceIncotermsUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceIncotermsUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceIncotermsUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceIncotermsUSD.Value);
                }
                else
                    return "0";
            }
        }

        public double? PRICEGR { get; set; }
        public string PRICEGRstring
        {
            get
            {
                if (this.PRICEGR.HasValue)
                {
                    if (this.PRICEGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICEGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICEGR.Value);
                }
                else
                    return "0";
            }
        }
        public string CurrGR { get; set; }

        public double? PRICEGRUSD { get; set; }
        public string PRICEGRUSDstring
        {
            get
            {
                if (this.PRICEGRUSD.HasValue)
                {
                    if (this.PRICEGRUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEGRUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICEGRUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICEGRUSD.Value);
                }
                else
                    return "0";
            }
        }

        public double? UnitPriceUSD { get; set; }
        public string UnitPriceUSDstring
        {
            get
            {
                if (this.UnitPriceUSD.HasValue)
                {
                    if (this.UnitPriceUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.UnitPriceUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.UnitPriceUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.UnitPriceUSD.Value);
                }
                else
                    return "0";
            }
        }

        public double? ConsumptionAmt { get; set; }
        public string ConsumptionAmtstring
        {
            get
            {
                if (this.ConsumptionAmt.HasValue)
                {
                    if (this.ConsumptionAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsumptionAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0.#######}", this.ConsumptionAmt.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsumptionAmt.Value);
                }
                else
                    return "";
            }
        }
        public double? ConsLLAmt { get; set; }
        public string ConsLLAmtstring
        {
            get
            {
                if (this.ConsLLAmt.HasValue)
                {
                    if (this.ConsLLAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsLLAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0.#######}", this.ConsLLAmt.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsLLAmt.Value);
                }
                else
                    return "";
            }
        }
        public double? ConsALAmt { get; set; }
        public string ConsALAmtstring
        {
            get
            {
                if (this.ConsALAmt.HasValue)
                {
                    if (this.ConsALAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsALAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0.#######}", this.ConsALAmt.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsALAmt.Value);
                }
                else
                    return "";
            }
        }

        public double? SumConsumption { get; set; }
        public string SumConsumptionstring
        {
            get
            {
                if (this.SumConsumption.HasValue)
                {
                    if (this.SumConsumption.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumConsumption.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0.#######}", this.SumConsumption.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumConsumption.Value);
                }
                else
                    return "";
            }
        }

        public double? SumConsumptionAmt { get; set; }
        public string SumConsumptionAmtstring
        {
            get
            {
                if (this.SumConsumptionAmt.HasValue)
                {
                    if (this.SumConsumptionAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumConsumptionAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumConsumptionAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumConsumptionAmt.Value);
                }
                else
                    return "";
            }
        }

        public double? SumFGGRCons { get; set; }
        public string SumFGGRConsstring
        {
            get
            {
                if (this.SumFGGRCons.HasValue)
                {
                    if (this.SumFGGRCons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumFGGRCons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0.#######}", this.SumFGGRCons.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumFGGRCons.Value);
                }
                else
                    return "";
            }
        }
        public double? SumAmt { get; set; }
        public string SumAmtstring
        {
            get
            {
                if (this.SumAmt.HasValue)
                {
                    if (this.SumAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmt.Value);
                }
                else
                    return "";
            }
        }




        public double? SumConsLL { get; set; }
        public string SumConsLLstring
        {
            get
            {
                if (this.SumConsLL.HasValue)
                {
                    if (this.SumConsLL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumConsLL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0.#######}", this.SumConsLL.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumConsLL.Value);
                }
                else
                    return "";
            }
        }

        public double? SumConsLLAmt { get; set; }
        public string SumConsLLAmtstring
        {
            get
            {
                if (this.SumConsLLAmt.HasValue)
                {
                    if (this.SumConsLLAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumConsLLAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumConsLLAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumConsLLAmt.Value);
                }
                else
                    return "";
            }
        }

        public double? SumFGGRConsLL { get; set; }
        public string SumFGGRConsLLstring
        {
            get
            {
                if (this.SumFGGRConsLL.HasValue)
                {
                    if (this.SumFGGRConsLL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumFGGRConsLL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0.#######}", this.SumFGGRConsLL.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumFGGRConsLL.Value);
                }
                else
                    return "";
            }
        }
        public double? SumAmtLL { get; set; }
        public string SumAmtLLstring
        {
            get
            {
                if (this.SumAmtLL.HasValue)
                {
                    if (this.SumAmtLL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtLL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmtLL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmtLL.Value);
                }
                else
                    return "";
            }
        }






        public double? SumConsAL { get; set; }
        public string SumConsALstring
        {
            get
            {
                if (this.SumConsAL.HasValue)
                {
                    if (this.SumConsAL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumConsAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0.#######}", this.SumConsAL.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumConsAL.Value);
                }
                else
                    return "";
            }
        }

        public double? SumConsALAmt { get; set; }
        public string SumConsALAmtstring
        {
            get
            {
                if (this.SumConsALAmt.HasValue)
                {
                    if (this.SumConsALAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumConsALAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumConsALAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumConsALAmt.Value);
                }
                else
                    return "";
            }
        }

        public double? SumFGGRConsAL { get; set; }
        public string SumFGGRConsALstring
        {
            get
            {
                if (this.SumFGGRConsAL.HasValue)
                {
                    if (this.SumFGGRConsAL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumFGGRConsAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0.#######}", this.SumFGGRConsAL.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumFGGRConsAL.Value);
                }
                else
                    return "";
            }
        }
        public double? SumAmtAL { get; set; }
        public string SumAmtALstring
        {
            get
            {
                if (this.SumAmtAL.HasValue)
                {
                    if (this.SumAmtAL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmtAL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmtAL.Value);
                }
                else
                    return "";
            }
        }
        public double? Comp { get; set; }
        public string Compstring
        {
            get
            {
                if (this.Comp.HasValue)
                {
                    if (this.Comp.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Comp.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0}" + "%", this.Comp.Value);
                    }
                    return string.Format("{0:#,##0}" + "%", this.Comp.Value);
                }
                else
                    return "0";
            }
        }
        public bool Costing { get; set; }
        public bool ClosedOnFGGR { get; set; }

        public string RefVKNo { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }

    }
}

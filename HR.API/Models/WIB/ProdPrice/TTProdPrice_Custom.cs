﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.WIB.ProdPrice
{
    public class TTProdPrice_Custom: TTProdPrice
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        
        public string RefVKNo { get; set; }
        public string SKUCode { get; set; }
        public string StyleCode { get; set; }
        public string ColorCode { get; set; }
        public string ProdDesc { get; set; }
        public string BrandCD { get; set; }
        public string BrandDesc { get; set; }
        public string Color { get; set; }
        public string RangeDesc { get; set; }
        public string StyleDesc { get; set; }
        public string FOBstring
        {
            get
            {
                if (this.FOB.HasValue)
                {
                    if (this.FOB.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.FOB.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.FOB.Value);
                    }
                    return string.Format("{0:#,##0.##########################}", this.FOB.Value);

                }
                else
                    return "";
            }
        }
        public string MatCoststring
        {
            get
            {
                if (this.MatCost.HasValue)
                {
                    if (this.MatCost.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.MatCost.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.MatCost.Value);
                    }
                    return string.Format("{0:#,##0.##########################}", this.MatCost.Value);

                }
                else
                    return "";
            }
        }
        public string SalseQtystring
        {
            get
            {
                if (this.SalseQty.HasValue)
                {
                    if (this.SalseQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SalseQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SalseQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SalseQty.Value);

                }
                else
                    return "";
            }
        }
    }
}
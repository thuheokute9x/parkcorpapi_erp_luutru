﻿using System;

namespace HR.API.Controllers
{
    public class InventoryWIPModel
    {
        public int TotalRows { get; set; }
        public int RowID { get; set; }
        public int ID { get; set; }
        public string ProdInstCD { get; set; }
        public string ProdCode { get; set; }
        public float? OrderQty { get; set; }
        public float? FGQty { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }

    }
}

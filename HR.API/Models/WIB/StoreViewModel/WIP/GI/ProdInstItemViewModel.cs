﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.WIB.StoreViewModel.GI
{
    public class ProdInstItemViewModel
    {
        public string ProdInstCD { get; set; }
        public string ProdCode { get; set; }
        public decimal? Qty { get; set; }
        public int? ProdPriceID { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.WIB.StoreViewModel.WIP.GI
{
    public class CustomsMatViewModel
    {
        public string MATCD { get; set; }
        public string MatName { get; set; }
        public string Color { get; set; }
        public string PUnit { get; set; }
        public string Payment { get; set; }
        public int TotalRows { get; set; }
        public int RowID { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.WIB.StoreViewModel.WIP.TPR
{
    public class TPRViewModel
    {
        public string ProdInstCD { get; set; }
        public string ProdCode { get; set; }
        public string MatCD { get; set; }
        public string PUnit { get; set; }

    }
}
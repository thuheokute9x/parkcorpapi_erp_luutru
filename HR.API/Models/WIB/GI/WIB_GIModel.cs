﻿using System;
using System.Globalization;

namespace HR.API.Controllers
{
    public class WIB_GIModel
    {
        public string ProdInstCD { get; set; }
        //public string ProdCode { get; set; }
        public string MatCD { get; set; }
        public string MatName { get; set; }
        public string Color { get; set; }
        public string PUNIT { get; set; }
        public string UnitGI { get; set; }
        public double? PRICE { get; set; }
        public string PRICEstring
        {
            get
            {
                if (this.PRICE.HasValue)
                {
                    if (this.PRICE.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICE.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICE.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICE.Value);
                }
                else
                    return "0";
            }
        }
        public double? OQTY { get; set; }
        public string OQTYstring
        {
            get
            {
                if (this.OQTY.HasValue)
                {
                    if (this.OQTY.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.OQTY.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.OQTY.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.OQTY.Value);
                }
                else
                    return "0";
            }
        }
        public double? QTYGI { get; set; }
        public string QTYGIstring
        {
            get
            {
                if (this.QTYGI.HasValue)
                {
                    if (this.QTYGI.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QTYGI.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QTYGI.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QTYGI.Value);
                }
                else
                    return "0";
            }
        }
        public double? Amt { get; set; }
        public string Amtstring
        {
            get
            {
                if (this.Amt.HasValue)
                {
                    if (this.Amt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Amt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Amt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Amt.Value);
                }
                else
                    return "0";
            }
        }
        public string CURR { get; set; }
        public string CurrTPInfor { get; set; }
        public string CurrGR { get; set; }
        public string Payment { get; set; }


        public double? PriceLotSize { get; set; }
        public string PriceLotSizestring
        {
            get
            {
                if (this.PriceLotSize.HasValue)
                {
                    if (this.PriceLotSize.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceLotSize.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceLotSize.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceLotSize.Value);
                }
                else
                    return "0";
            }
        }

        public double? PriceIncoterms { get; set; }
        public string PriceIncotermsstring
        {
            get
            {
                if (this.PriceIncoterms.HasValue)
                {
                    if (this.PriceIncoterms.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceIncoterms.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceIncoterms.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceIncoterms.Value);
                }
                else
                    return "0";
            }
        }

        public double? PriceIncotermsUSD { get; set; }
        public string PriceIncotermsUSDstring
        {
            get
            {
                if (this.PriceIncotermsUSD.HasValue)
                {
                    if (this.PriceIncotermsUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceIncotermsUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceIncotermsUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceIncotermsUSD.Value);
                }
                else
                    return "0";
            }
        }

        public double? PRICEGR { get; set; }
        public string PRICEGRstring
        {
            get
            {
                if (this.PRICEGR.HasValue)
                {
                    if (this.PRICEGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICEGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICEGR.Value);
                }
                else
                    return "0";
            }
        }

        public double? Comp { get; set; }
        public string Compstring
        {
            get
            {
                if (this.Comp.HasValue)
                {
                    if (this.Comp.Value == 0)
                    {
                        return "0" + "%";
                    }
                    int value;
                    if (int.TryParse(this.Comp.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0}" + "%", this.Comp.Value);
                    }
                    return string.Format("{0:#,##0}" + "%", this.Comp.Value);
                }
                else
                    return "";
            }
        }

        public double? UnitPriceUSD { get; set; }
        public string UnitPriceUSDstring
        {
            get
            {
                if (this.UnitPriceUSD.HasValue)
                {
                    if (this.UnitPriceUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.UnitPriceUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.UnitPriceUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.UnitPriceUSD.Value);
                }
                else
                    return "0";
            }
        }

        public double? QtyPUnit { get; set; }
        public string QtyPUnitstring
        {
            get
            {
                if (this.QtyPUnit.HasValue)
                {
                    if (this.QtyPUnit.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyPUnit.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QtyPUnit.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QtyPUnit.Value);
                }
                else
                    return "0";
            }
        }

        public double? Sum_QtyPUnit { get; set; }
        public string Sum_QtyPUnitstring
        {
            get
            {
                if (this.Sum_QtyPUnit.HasValue)
                {
                    if (this.Sum_QtyPUnit.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_QtyPUnit.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Sum_QtyPUnit.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Sum_QtyPUnit.Value);
                }
                else
                    return "0";
            }
        }

        public double? Sum_Amt { get; set; }
        public string Sum_Amtstring
        {
            get
            {
                if (this.Sum_Amt.HasValue)
                {
                    if (this.Sum_Amt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_Amt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0.##}", this.Sum_Amt.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.Sum_Amt.Value);
                }
                else
                    return "0";
            }
        }

        public Boolean ClosedOnFGGR { get; set; }
        public Boolean IsWFG { get; set; }

        public Boolean Costing { get; set; }

        public DateTime? ODATE { get; set; }
        public int TotalRows { get; set; }
        public int RowID { get; set; }

        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string RefVKNo { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        //public float? PARKERP { get; set; }
        //public float? QTYCuting { get; set; }
        //public float? QTYSubStore { get; set; }
        //public string CompletePO { get; set; }

    }
}

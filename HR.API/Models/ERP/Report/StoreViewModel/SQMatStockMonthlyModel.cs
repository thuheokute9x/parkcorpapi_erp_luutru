﻿using System;

namespace HR.API.Models.ERP
{
    public class SQMatStockMonthlyModel
    {
        public string MatCD { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public string MH1 { get; set; }
        public bool OutSourcing { get; set; }
        public string Vendor { get; set; }
        public string PUnit { get; set; }
        public string BrandCD { get; set; }

        public double? PriceLotSizePInfo { get; set; }
        public string PriceLotSizePInfostring
        {
            get
            {
                if (this.PriceLotSizePInfo.HasValue)
                {
                    if (this.PriceLotSizePInfo.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceLotSizePInfo.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceLotSizePInfo.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceLotSizePInfo.Value);
                }
                else
                    return "0";
            }
        }
        public string CIFCurr { get; set; }

        public double? PriceGR { get; set; }
        public string PriceGRstring
        {
            get
            {
                if (this.PriceGR.HasValue)
                {
                    if (this.PriceGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceGR.Value);
                }
                else
                    return "0";
            }
        }

        public string CURRGR { get; set; }

        public double? Comp { get; set; }
        public string Compstring
        {
            get
            {
                if (this.Comp.HasValue)
                {
                    if (this.Comp.Value == 0)
                    {
                        return "0" + "%";
                    }
                    int value;
                    if (int.TryParse(this.Comp.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0}" + "%", this.Comp.Value);
                    }
                    return string.Format("{0:#,0}" +"%", this.Comp.Value);
                }
                else
                    return "";
            }
        }
        public string Payment { get; set; }

        public double? PriceIncotermsPInfo { get; set; }
        public string PriceIncotermsPInfostring
        {
            get
            {
                if (this.PriceIncotermsPInfo.HasValue)
                {
                    if (this.PriceIncotermsPInfo.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceIncotermsPInfo.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceIncotermsPInfo.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceIncotermsPInfo.Value);
                }
                else
                    return "0";
            }
        }

        public double? LastPricePInfo { get; set; }
        public string LastPricePInfostring
        {
            get
            {
                if (this.LastPricePInfo.HasValue)
                {
                    if (this.LastPricePInfo.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.LastPricePInfo.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.LastPricePInfo.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.LastPricePInfo.Value);
                }
                else
                    return "0";
            }
        }
        public double? LastPriceGR { get; set; }
        public string LastPriceGRstring
        {
            get
            {
                if (this.LastPriceGR.HasValue)
                {
                    if (this.LastPriceGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.LastPriceGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.LastPriceGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.LastPriceGR.Value);
                }
                else
                    return "0";
            }
        }
        public double? UnitPrice { get; set; }
        public string UnitPricestring
        {
            get
            {
                if (this.UnitPrice.HasValue)
                {
                    if (this.UnitPrice.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.UnitPrice.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.UnitPrice.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.UnitPrice.Value);
                }
                else
                    return "0";
            }
        }
        public double? QtyBS { get; set; }
        public string QtyBSstring
        {
            get
            {
                if (this.QtyBS.HasValue)
                {
                    if (this.QtyBS.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyBS.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QtyBS.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QtyBS.Value);
                }
                else
                    return "0";
            }
        }
        public double? QTYGR { get; set; }
        public string QTYGRstring
        {
            get
            {
                if (this.QTYGR.HasValue)
                {
                    if (this.QTYGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QTYGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QTYGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QTYGR.Value);
                }
                else
                    return "0";
            }
        }
        public double? QTYGI { get; set; }
        public string QTYGIstring
        {
            get
            {
                if (this.QTYGI.HasValue)
                {
                    if (this.QTYGI.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QTYGI.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QTYGI.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QTYGI.Value);
                }
                else
                    return "0";
            }
        }
        public double? QtyEndingS { get; set; }
        public string QtyEndingSstring
        {
            get
            {
                if (this.QtyEndingS.HasValue)
                {
                    if (this.QtyEndingS.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyEndingS.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QtyEndingS.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QtyEndingS.Value);
                }
                else
                    return "0";
            }
        }

        public double? AmtBS { get; set; }
        public string AmtBSstring
        {
            get
            {
                if (this.AmtBS.HasValue)
                {
                    if (this.AmtBS.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtBS.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtBS.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtBS.Value);
                }
                else
                    return "0";
            }
        }
        public double? AmtGR { get; set; }
        public string AmtGRstring
        {
            get
            {
                if (this.AmtGR.HasValue)
                {
                    if (this.AmtGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtGR.Value);
                }
                else
                    return "0";
            }
        }
        public double? AmtGI { get; set; }
        public string AmtGIstring
        {
            get
            {
                if (this.AmtGI.HasValue)
                {
                    if (this.AmtGI.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtGI.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtGI.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtGI.Value);
                }
                else
                    return "0";
            }
        }
        public double? AmtES { get; set; }
        public string AmtESstring
        {
            get
            {
                if (this.AmtES.HasValue)
                {
                    if (this.AmtES.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtES.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtES.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtES.Value);
                }
                else
                    return "0";
            }
        }
        public double? QtyInspect { get; set; }
        public string QtyInspectstring
        {
            get
            {
                if (this.QtyInspect.HasValue)
                {
                    if (this.QtyInspect.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyInspect.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QtyInspect.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QtyInspect.Value);
                }
                else
                    return "0";
            }
        }
        public double? QtyDiff { get; set; }
        public string QtyDiffstring
        {
            get
            {
                if (this.QtyDiff.HasValue)
                {
                    if (this.QtyDiff.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyDiff.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QtyDiff.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QtyDiff.Value);
                }
                else
                    return "0";
            }
        }
        public DateTime? DateInspect { get; set; }







        public double? QtyEU { get; set; }
        public string QtyEUstring
        {
            get
            {
                if (this.QtyEU.HasValue)
                {
                    if (this.QtyEU.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyEU.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QtyEU.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QtyEU.Value);
                }
                else
                    return "0";
            }
        }
        public double? QtyESSum { get; set; }
        public string QtyESSumstring
        {
            get
            {
                if (this.QtyESSum.HasValue)
                {
                    if (this.QtyESSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyESSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QtyESSum.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QtyESSum.Value);
                }
                else
                    return "0";
            }
        }
        public double? AmtEU { get; set; }
        public string AmtEUstring
        {
            get
            {
                if (this.AmtEU.HasValue)
                {
                    if (this.AmtEU.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtEU.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtEU.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtEU.Value);
                }
                else
                    return "0";
            }
        }
        public double? AmtESSum { get; set; }
        public string AmtESSumstring
        {
            get
            {
                if (this.AmtESSum.HasValue)
                {
                    if (this.AmtESSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtESSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtESSum.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtESSum.Value);
                }
                else
                    return "0";
            }
        }

        public double? SumAllQtyBS { get; set; }
        public string SumAllQtyBSstring
        {
            get
            {
                if (this.SumAllQtyBS.HasValue)
                {
                    if (this.SumAllQtyBS.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllQtyBS.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllQtyBS.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllQtyBS.Value);
                }
                else
                    return "";
            }
        }
        public double? SumAllQTYGR { get; set; }
        public string SumAllQTYGRstring
        {
            get
            {
                if (this.SumAllQTYGR.HasValue)
                {
                    if (this.SumAllQTYGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllQTYGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllQTYGR.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllQTYGR.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllQTYGI { get; set; }
        public string SumAllQTYGIstring
        {
            get
            {
                if (this.SumAllQTYGI.HasValue)
                {
                    if (this.SumAllQTYGI.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllQTYGI.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllQTYGI.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllQTYGI.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllQtyEndingS { get; set; }
        public string SumAllQtyEndingSstring
        {
            get
            {
                if (this.SumAllQtyEndingS.HasValue)
                {
                    if (this.SumAllQtyEndingS.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllQtyEndingS.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllQtyEndingS.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllQtyEndingS.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllAmtBS { get; set; }
        public string SumAllAmtBSstring
        {
            get
            {
                if (this.SumAllAmtBS.HasValue)
                {
                    if (this.SumAllAmtBS.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllAmtBS.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllAmtBS.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllAmtBS.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllAmtGR { get; set; }
        public string SumAllAmtGRstring
        {
            get
            {
                if (this.SumAllAmtGR.HasValue)
                {
                    if (this.SumAllAmtGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllAmtGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllAmtGR.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllAmtGR.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllAmtGI { get; set; }
        public string SumAllAmtGIstring
        {
            get
            {
                if (this.SumAllAmtGI.HasValue)
                {
                    if (this.SumAllAmtGI.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllAmtGI.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllAmtGI.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllAmtGI.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllAmtES { get; set; }
        public string SumAllAmtESstring
        {
            get
            {
                if (this.SumAllAmtES.HasValue)
                {
                    if (this.SumAllAmtES.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllAmtES.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllAmtES.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllAmtES.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllQtyInspect { get; set; }
        public string SumAllQtyInspectstring
        {
            get
            {
                if (this.SumAllQtyInspect.HasValue)
                {
                    if (this.SumAllQtyInspect.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllQtyInspect.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllQtyInspect.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllQtyInspect.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllQtyDiff { get; set; }
        public string SumAllQtyDiffstring
        {
            get
            {
                if (this.SumAllQtyDiff.HasValue)
                {
                    if (this.SumAllQtyDiff.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllQtyDiff.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllQtyDiff.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllQtyDiff.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllQtyEU { get; set; }
        public string SumAllQtyEUstring
        {
            get
            {
                if (this.SumAllQtyEU.HasValue)
                {
                    if (this.SumAllQtyEU.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllQtyEU.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllQtyEU.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllQtyEU.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllQtyESSum { get; set; }
        public string SumAllQtyESSumstring
        {
            get
            {
                if (this.SumAllQtyESSum.HasValue)
                {
                    if (this.SumAllQtyESSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllQtyESSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllQtyESSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllQtyESSum.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllAmtEU { get; set; }
        public string SumAllAmtEUstring
        {
            get
            {
                if (this.SumAllAmtEU.HasValue)
                {
                    if (this.SumAllAmtEU.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllAmtEU.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllAmtEU.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllAmtEU.Value);
                }
                else
                    return "";
            }
        }

        public double? SumAllAmtESSum { get; set; }
        public string SumAllAmtESSumstring
        {
            get
            {
                if (this.SumAllAmtESSum.HasValue)
                {
                    if (this.SumAllAmtESSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAllAmtESSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAllAmtESSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAllAmtESSum.Value);
                }
                else
                    return "";
            }
        }
        public string DateInspectConvert { get; set; }
        public int TotalRows { get; set; }
        public int RowID { get; set; }
        //public string CURR { get; set; }
        //public string Payment { get; set; }
        //public int TotalRows { get; set; }
        //public int RowID { get; set; }
        //public float? PARKERP { get; set; }
        //public float? QTYCuting { get; set; }
        //public float? QTYSubStore { get; set; }
        //public string CompletePO { get; set; }

    }
}

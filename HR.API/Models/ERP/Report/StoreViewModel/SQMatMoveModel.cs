﻿using System;

namespace HR.API.Models.ERP
{
    public class SQMatMoveModel
    {
        public string RefVKNo { get; set; }
        public string MfgVendor { get; set; }
        public string Shipper { get; set; }
        public string MatCD { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public string PUnit { get; set; }
        public double? LossRate { get; set; }
        public string LossRatestring
        {
            get
            {
                if (this.LossRate.HasValue)
                {
                    if (this.LossRate.Value == 0)
                    {
                        return "0%";
                    }
                    return Math.Round(this.LossRate.Value, 2).ToString() + "%";

                    //if (int.TryParse(this.LossRateReal.Value.ToString(), out value))
                    //{
                    //}
                    //return string.Format("{0:#,##0.#######}", this.LossRateReal.Value);
                }
                else
                    return "0%";
            }
        }
        public double? LossQty { get; set; }
        public string LossQtystring
        {
            get
            {
                if (this.LossQty.HasValue)
                {
                    if (this.LossQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.LossQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.LossQty.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.LossQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? AddQty { get; set; }
        public string AddQtystring
        {
            get
            {
                if (this.AddQty.HasValue)
                {
                    if (this.AddQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AddQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AddQty.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.AddQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? LeftOverQty { get; set; }
        public string LeftOverQtystring
        {
            get
            {
                if (this.LeftOverQty.HasValue)
                {
                    if (this.LeftOverQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.LeftOverQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.LeftOverQty.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.LeftOverQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? StdQtyReal { get; set; }
        public string StdQtyRealstring
        {
            get
            {
                if (this.StdQtyReal.HasValue)
                {
                    if (this.StdQtyReal.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.StdQtyReal.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.StdQtyReal.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.StdQtyReal.Value);
                }
                else
                    return "0";
            }
        }
        public double? StdQty { get; set; }
        public string StdQtystring
        {
            get
            {
                if (this.StdQty.HasValue)
                {
                    if (this.StdQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.StdQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.StdQty.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.StdQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? ActQty { get; set; }
        public string ActQtystring
        {
            get
            {
                if (this.ActQty.HasValue)
                {
                    if (this.ActQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ActQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ActQty.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.ActQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? POQty { get; set; }
        public string POQtystring
        {
            get
            {
                if (this.POQty.HasValue)
                {
                    if (this.POQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.POQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.POQty.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.POQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? GRQty { get; set; }
        public string GRQtystring
        {
            get
            {
                if (this.GRQty.HasValue)
                {
                    if (this.GRQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GRQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GRQty.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GRQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? BalancePO { get; set; }
        public string BalancePOstring
        {
            get
            {
                if (this.BalancePO.HasValue)
                {
                    if (this.BalancePO.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.BalancePO.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.BalancePO.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.BalancePO.Value);
                }
                else
                    return "0";
            }
        }
        public double? GINormal { get; set; }
        public string GINormalstring
        {
            get
            {
                if (this.GINormal.HasValue)
                {
                    if (this.GINormal.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GINormal.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GINormal.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GINormal.Value);
                }
                else
                    return "0";
            }
        }
        public double? GINormal_StdQtyReal { get; set; }
        public string GINormal_StdQtyRealstring
        {
            get
            {
                if (this.GINormal_StdQtyReal.HasValue)
                {
                    if (this.GINormal_StdQtyReal.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GINormal_StdQtyReal.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GINormal_StdQtyReal.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GINormal_StdQtyReal.Value);
                }
                else
                    return "0";
            }
        }
        public double? GIOver { get; set; }
        public string GIOverstring
        {
            get
            {
                if (this.GIOver.HasValue)
                {
                    if (this.GIOver.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GIOver.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GIOver.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GIOver.Value);
                }
                else
                    return "0";
            }
        }

        public double? GITrial { get; set; }
        public string GITrialstring
        {
            get
            {
                if (this.GITrial.HasValue)
                {
                    if (this.GITrial.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GITrial.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GITrial.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GITrial.Value);
                }
                else
                    return "0";
            }
        }
        public double? GIDefMat { get; set; }
        public string GIDefMatstring
        {
            get
            {
                if (this.GIDefMat.HasValue)
                {
                    if (this.GIDefMat.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GIDefMat.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GIDefMat.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GIDefMat.Value);
                }
                else
                    return "0";
            }
        }
        public double? GIDefWork { get; set; }
        public string GIDefWorkstring
        {
            get
            {
                if (this.GIDefWork.HasValue)
                {
                    if (this.GIDefWork.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GIDefWork.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GIDefWork.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GIDefWork.Value);
                }
                else
                    return "0";
            }
        }
        public double? GIDefLost { get; set; }
        public string GIDefLoststring
        {
            get
            {
                if (this.GIDefLost.HasValue)
                {
                    if (this.GIDefLost.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GIDefLost.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GIDefLost.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GIDefLost.Value);
                }
                else
                    return "0";
            }
        }
        public double? GIDefBuyer { get; set; }
        public string GIDefBuyerstring
        {
            get
            {
                if (this.GIDefBuyer.HasValue)
                {
                    if (this.GIDefBuyer.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GIDefBuyer.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GIDefBuyer.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GIDefBuyer.Value);
                }
                else
                    return "0";
            }
        }
        public double? GITotal { get; set; }
        public string GITotalstring
        {
            get
            {
                if (this.GITotal.HasValue)
                {
                    if (this.GITotal.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GITotal.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GITotal.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GITotal.Value);
                }
                else
                    return "0";
            }
        }
        public double? LossRateReal { get; set; }
        public string LossRateRealstring
        {
            get
            {
                if (this.LossRateReal.HasValue)
                {
                    if (this.LossRateReal.Value == 0)
                    {
                        return "0%";
                    }
                    return Math.Round(this.LossRateReal.Value, 2).ToString() + "%";

                    //if (int.TryParse(this.LossRateReal.Value.ToString(), out value))
                    //{
                    //}
                    //return string.Format("{0:#,##0.#######}", this.LossRateReal.Value);
                }
                else
                    return "0%";
            }
        }
        public double? MatReturn { get; set; }
        public string MatReturnstring
        {
            get
            {
                if (this.MatReturn.HasValue)
                {
                    if (this.MatReturn.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.MatReturn.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.MatReturn.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.MatReturn.Value);
                }
                else
                    return "0";
            }
        }
        public int TotalRows { get; set; }
        public int RowID { get; set; }



        public string PGrp { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

    }
}

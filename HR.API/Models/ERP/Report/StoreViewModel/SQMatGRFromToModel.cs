﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.Report.StoreViewModel
{
    public class SQMatGRFromToModel
    {
        public int PRID { get; set; }
        public string RefVKNo { get; set; }
        public string MatCD { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public string CustItemNo1 { get; set; }
        public string IV { get; set; }
        public string BL { get; set; }
        public string IDATE { get; set; }
        public string MatPOCD { get; set; }

        public string OUTCD { get; set; }
        public string VCD { get; set; }
        public string SHIPPER { get; set; }
        public double? QTY { get; set; }
        public string QTYstring
        {
            get
            {
                if (this.QTY.HasValue)
                {
                    if (this.QTY.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QTY.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.QTY.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.QTY.Value);
                }
                else
                    return "0";
            }
        }
        public double? QTYSum { get; set; }
        public string QTYSumstring
        {
            get
            {
                if (this.QTYSum.HasValue)
                {
                    if (this.QTYSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QTYSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.QTYSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.QTYSum.Value);
                }
                else
                    return "";
            }
        }
        public string PUNIT { get; set; }
        public double? PRICE { get; set; }
        public string PRICEstring
        {
            get
            {
                if (this.PRICE.HasValue)
                {
                    if (this.PRICE.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICE.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICE.Value);
                    }
                    return string.Format("{0:#,##0.############}", this.PRICE.Value);
                }
                else
                    return "0";
            }
        }

        public double? PRICEUSD { get; set; }
        public string PRICEUSDstring
        {
            get
            {
                if (this.PRICEUSD.HasValue)
                {
                    if (this.PRICEUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICEUSD.Value);
                    }
                    return string.Format("{0:#,##0.############}", this.PRICEUSD.Value);
                }
                else
                    return "0";
            }
        }

        public string CURR { get; set; }
        public double? AmtGR { get; set; }
        public string AmtGRstring
        {
            get
            {
                if (this.AmtGR.HasValue)
                {
                    if (this.AmtGR.Value == 0)
                    {
                        return "0.00";
                    }
                    int value;
                    if (int.TryParse(this.AmtGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtGR.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtGR.Value);
                }
                else
                    return "";
            }
        }

        public double? AmtGRSum { get; set; }
        public string AmtGRSumstring
        {
            get
            {
                if (this.AmtGRSum.HasValue)
                {
                    if (this.AmtGRSum.Value == 0)
                    {
                        return "0.00";
                    }
                    int value;
                    if (int.TryParse(this.AmtGRSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtGRSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtGRSum.Value);
                }
                else
                    return "";
            }
        }

        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string OutSourcing { get; set; }
        public double? ConvFactorGR { get; set; }
        public string ConvFactorGRstring
        {
            get
            {
                if (this.ConvFactorGR.HasValue)
                {
                    if (this.ConvFactorGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConvFactorGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConvFactorGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConvFactorGR.Value);
                }
                else
                    return "0";
            }
        }
        public double? ConvertQtyGR { get; set; }
        public string ConvertQtyGRstring
        {
            get
            {
                if (this.ConvertQtyGR.HasValue)
                {
                    if (this.ConvertQtyGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConvertQtyGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConvertQtyGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConvertQtyGR.Value);
                }
                else
                    return "0";
            }
        }
        public string PUnitGRChuan { get; set; }
        public double? ConvertQtyGRSum { get; set; }
        public string ConvertQtyGRSumstring
        {
            get
            {
                if (this.ConvertQtyGRSum.HasValue)
                {
                    if (this.ConvertQtyGRSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConvertQtyGRSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.ConvertQtyGRSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.ConvertQtyGRSum.Value);
                }
                else
                    return "";
            }
        }
    }
}
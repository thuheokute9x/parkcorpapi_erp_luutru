﻿using System;

namespace HR.API.Models.ERP
{
    public class SQMatStockMonthlyGroupModel
    {
        public string MH1 { get; set; }
        public double? Count { get; set; }
        public string Countstring
        {
            get
            {
                if (this.Count.HasValue)
                {
                    if (this.Count.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Count.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Count.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Count.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtBS { get; set; }
        public string AmtBSstring
        {
            get
            {
                if (this.AmtBS.HasValue)
                {
                    if (this.AmtBS.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtBS.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtBS.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtBS.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtGR { get; set; }
        public string AmtGRstring
        {
            get
            {
                if (this.AmtGR.HasValue)
                {
                    if (this.AmtGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtGR.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtGR.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtGI { get; set; }
        public string AmtGIstring
        {
            get
            {
                if (this.AmtGI.HasValue)
                {
                    if (this.AmtGI.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtGI.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtGI.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtGI.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtES { get; set; }
        public string AmtESstring
        {
            get
            {
                if (this.AmtES.HasValue)
                {
                    if (this.AmtES.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtES.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtES.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtES.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtGRNow { get; set; }
        public string AmtGRNowstring
        {
            get
            {
                if (this.AmtGRNow.HasValue)
                {
                    if (this.AmtGRNow.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtGRNow.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtGRNow.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtGRNow.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtGINow { get; set; }
        public string AmtGINowstring
        {
            get
            {
                if (this.AmtGINow.HasValue)
                {
                    if (this.AmtGINow.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtGINow.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtGINow.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtGINow.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtEU { get; set; }
        public string AmtEUstring
        {
            get
            {
                if (this.AmtEU.HasValue)
                {
                    if (this.AmtEU.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtEU.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtEU.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtEU.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtESSum { get; set; }
        public string AmtESSumstring
        {
            get
            {
                if (this.AmtESSum.HasValue)
                {
                    if (this.AmtESSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtESSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtESSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtESSum.Value);
                }
                else
                    return "";
            }
        }

        public double? ToTal_CountSum { get; set; }
        public string ToTal_CountSumstring
        {
            get
            {
                if (this.ToTal_CountSum.HasValue)
                {
                    if (this.ToTal_CountSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ToTal_CountSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.ToTal_CountSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.ToTal_CountSum.Value);
                }
                else
                    return "";
            }
        }
        public double? ToTal_AmtBSSum { get; set; }
        public string ToTal_AmtBSSumstring
        {
            get
            {
                if (this.ToTal_AmtBSSum.HasValue)
                {
                    if (this.ToTal_AmtBSSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ToTal_AmtBSSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.ToTal_AmtBSSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.ToTal_AmtBSSum.Value);
                }
                else
                    return "";
            }
        }

        public double? ToTal_AmtGRSum { get; set; }
        public string ToTal_AmtGRSumstring
        {
            get
            {
                if (this.ToTal_AmtGRSum.HasValue)
                {
                    if (this.ToTal_AmtGRSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ToTal_AmtGRSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.ToTal_AmtGRSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.ToTal_AmtGRSum.Value);
                }
                else
                    return "";
            }
        }

        public double? ToTal_AmtGISum { get; set; }
        public string ToTal_AmtGISumstring
        {
            get
            {
                if (this.ToTal_AmtGISum.HasValue)
                {
                    if (this.ToTal_AmtGISum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ToTal_AmtGISum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.ToTal_AmtGISum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.ToTal_AmtGISum.Value);
                }
                else
                    return "";
            }
        }

        public double? ToTal_AmtESSum { get; set; }
        public string ToTal_AmtESSumstring
        {
            get
            {
                if (this.ToTal_AmtESSum.HasValue)
                {
                    if (this.ToTal_AmtESSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ToTal_AmtESSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.ToTal_AmtESSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.ToTal_AmtESSum.Value);
                }
                else
                    return "";
            }
        }

        public double? ToTal_AmtEUSum { get; set; }
        public string ToTal_AmtEUSumstring
        {
            get
            {
                if (this.ToTal_AmtEUSum.HasValue)
                {
                    if (this.ToTal_AmtEUSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ToTal_AmtEUSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.ToTal_AmtEUSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.ToTal_AmtEUSum.Value);
                }
                else
                    return "";
            }
        }

        public double? ToTal_AmtESSumSum { get; set; }
        public string ToTal_AmtESSumSumstring
        {
            get
            {
                if (this.ToTal_AmtESSumSum.HasValue)
                {
                    if (this.ToTal_AmtESSumSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ToTal_AmtESSumSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.ToTal_AmtESSumSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.ToTal_AmtESSumSum.Value);
                }
                else
                    return "";
            }
        }
        public int RowID { get; set; }


    }
}

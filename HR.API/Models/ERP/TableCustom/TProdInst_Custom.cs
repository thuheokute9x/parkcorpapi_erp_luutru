﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TProdInst_Custom: TProdInst
    {
        public string RefVKNo { get; set; }
        public string ClosedOnFGGRStringFormat { get; set; }
        public Boolean ClosedOnFGGRBit { get; set; }
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public Boolean IsDisClosedOnFGGR { get; set; }
        public string WorkingFGGR { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        
        public string ProdInstStatus { get; set; }
        public Boolean NA { get; set; }
        public Boolean StandBy { get; set; }
        public Boolean InProcess { get; set; }
        public Boolean Pending { get; set; }
        public Boolean Completed { get; set; }
        public Boolean Canceled { get; set; }
        public Boolean Close { get; set; }
        public DateTime? IDate_From { get; set; }
        public DateTime? IDate_To { get; set; }
        public DateTime? EDate_From { get; set; }
        public DateTime? EDate_To { get; set; }
        public int? PRID { get; set; }
        public string MatRqstDateConvert { get; set; }
        public string IDateConvert { get; set; }
        public string EDateConvert { get; set; }
        public DateTime? PlanYearDate { get; set; }
        public DateTime? PlanMonthDate { get; set; }
        public string CutWayCap { get; set; }
        public Boolean NG { get; set; }
        public string CreatedBy_Name { get; set; }
        public string CreatedOnConvert { get; set; }
        public string UpdatedBy_Name { get; set; }
        public string UpdatedOnConvert { get; set; }
        public int NumberofIssuedPO { get; set; }

    }
}
﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TPInfo_Custom: TPInfo
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public DateTime BasicDate { get; set; }
        public Boolean IncDiscontinued { get; set; }
        public Boolean MfgVendorDiscontinued { get; set; }
        public Boolean ShipperDiscontinued { get; set; }
        public string MatShape { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public int FirstMatGrpID { get; set; }
        public int MHID { get; set; }
        public string ValidFromConvert { get; set; }
        public string CustItemNo1 { get; set; }
        public string MatStdWstring
        {
            get
            {
                int value;
                if (int.TryParse(this.MatStdW.ToString(), out value))
                {
                    return string.Format("{0:0.00}", this.MatStdW);
                }
                return string.Format("{0:0.00}", this.MatStdW);
            }
        }
        public string MatStdHstring
        {
            get
            {
                int value;
                if (int.TryParse(this.MatStdH.ToString(), out value))
                {
                    return string.Format("{0:0.00}", this.MatStdH);
                }
                return string.Format("{0:0.00}", this.MatStdH);
            }
        }
        public string MOQstring
        {
            get
            {
                int value;
                if (int.TryParse(this.MOQ.ToString(), out value))
                {
                    return string.Format("{0:0.00}", this.MOQ);
                }
                return string.Format("{0:0.00}", this.MOQ);
            }
        }
        public string FreightRatestring
        {
            get
            {
                int value;
                if (int.TryParse(this.FreightRate.ToString(), out value))
                {
                    return string.Format("{0:0.00}" + '%', this.FreightRate);
                }
                return string.Format("{0:0.00}" + '%', this.FreightRate);
            }
        }
        public string InsuranceRatestring
        {
            get
            {
                int value;
                if (int.TryParse(this.InsuranceRate.ToString(), out value))
                {
                    return string.Format("{0:0.00}" + '%', this.InsuranceRate);
                }
                return string.Format("0.00" + '%', this.InsuranceRate);
            }
        }
        public string CommissionRatestring
        {
            get
            {
                int value;
                if (int.TryParse(this.CommissionRate.ToString(), out value))
                {
                    return string.Format("{0:0.00}" + '%', this.CommissionRate);
                }
                return string.Format("0.00" + '%', this.CommissionRate);
            }
        }
        public string LossRatestring
        {
            get
            {
                int value;
                if (int.TryParse(this.CommissionRate.ToString(), out value))
                {
                    return string.Format("{0:0.00}" + '%', this.CommissionRate);
                }
                return string.Format("0.00" + '%', this.CommissionRate);
            }
        }
        public string OtherCostRatestring
        {
            get
            {

                int value;
                if (int.TryParse(this.OtherCostRate.ToString(), out value))
                {
                    return string.Format("{0:0.00}" + '%', this.OtherCostRate);
                }
                return string.Format("0.00" + '%', this.OtherCostRate);
            }
        }

    }
}
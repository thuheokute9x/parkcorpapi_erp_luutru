﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TSPStatu_Custom: TSPStatu
    {
        public bool Select { get; set; }
        public string Status { get; set; }
    }
}
﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models
{
    public class STPayDoc_Custom : STPayDoc
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public string PayDocIDList { get; set; }
        public bool Select { get; set; } = false;
        public bool IsPayDocItem { get; set; } = false;
        public string HasPayDoc { get; set; }

    }
}
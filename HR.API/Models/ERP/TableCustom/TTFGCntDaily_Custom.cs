﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TTFGCntDaily_Custom : TTFGCntDaily
    {
        [NotMapped]
        public string FGDateString { get; set; }
        [NotMapped]
        public string BrandCD { get; set; }
        public string SKUCode { get; set; }
        [NotMapped]
        public string StyleCode { get; set; }
        [NotMapped]
        public string Color { get; set; }
        [NotMapped]
        public string ColorCode { get; set; }
        [NotMapped]
        public int RowID { get; set; }
        [NotMapped]
        public int TotalRows { get; set; }
        [NotMapped]
        public int PageNumber { get; set; }
        [NotMapped]
        public int PageSize { get; set; }
        [NotMapped]
        public int Type { get; set; }
        [NotMapped]
        public string FromDate { get; set; }
        [NotMapped]
        public string ToDate { get; set; }

        public double? FOB { get; set; }
        public string FOBstring
        {
            get
            {
                if (this.FOB.HasValue)
                {
                    if (this.FOB.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.FOB.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.FOB.Value);
                    }
                    return string.Format("{0:#,0.00}", this.FOB.Value);

                }
                else
                    return "0";
            }
        }
        public double? MatCost { get; set; }
        public string MatCoststring
        {
            get
            {
                if (this.MatCost.HasValue)
                {
                    if (this.MatCost.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.MatCost.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.MatCost.Value);
                    }
                    return string.Format("{0:#,0.00}", this.MatCost.Value);

                }
                else
                    return "0";
            }
        }
        public double? MatCostKorea { get; set; }
        public string MatCostKoreastring
        {
            get
            {
                if (this.MatCostKorea.HasValue)
                {
                    if (this.MatCostKorea.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.MatCostKorea.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.MatCostKorea.Value);
                    }
                    return string.Format("{0:#,0.00}", this.MatCostKorea.Value);

                }
                else
                    return "0";
            }
        }
        public double? MatCostKoreaLL { get; set; }
        public string MatCostKoreaLLstring
        {
            get
            {
                if (this.MatCostKoreaLL.HasValue)
                {
                    if (this.MatCostKoreaLL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.MatCostKoreaLL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.MatCostKoreaLL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.MatCostKoreaLL.Value);

                }
                else
                    return "0";
            }
        }
        public double? MatCostKoreaAL { get; set; }
        public string MatCostKoreaALstring
        {
            get
            {
                if (this.MatCostKoreaAL.HasValue)
                {
                    if (this.MatCostKoreaAL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.MatCostKoreaAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.MatCostKoreaAL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.MatCostKoreaAL.Value);

                }
                else
                    return "0";
            }
        }
        public double? SalseQty { get; set; }
        public string SalseQtystring
        {
            get
            {
                if (this.SalseQty.HasValue)
                {
                    if (this.SalseQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SalseQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SalseQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SalseQty.Value);

                }
                else
                    return "0";
            }
        }
        public double? GRQtyAllPeriod { get; set; }
        public string GRQtyAllPeriodstring
        {
            get
            {
                if (this.GRQtyAllPeriod.HasValue)
                {
                    if (this.GRQtyAllPeriod.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GRQtyAllPeriod.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GRQtyAllPeriod.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.GRQtyAllPeriod.Value);

                }
                else
                    return "0";
            }
        }
        public double? Balance { get; set; }
        public string Balancestring
        {
            get
            {
                if (this.Balance.HasValue)
                {
                    if (this.Balance.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Balance.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Balance.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Balance.Value);

                }
                else
                    return "0";
            }
        }
        public double? GRQtyPeriod { get; set; }
        public string GRQtyPeriodstring
        {
            get
            {
                if (this.GRQtyPeriod.HasValue)
                {
                    if (this.GRQtyPeriod.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GRQtyPeriod.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GRQtyPeriod.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.GRQtyPeriod.Value);

                }
                else
                    return "0";
            }
        }
        public double? GRFOB { get; set; }
        public string GRFOBstring
        {
            get
            {
                if (this.GRFOB.HasValue)
                {
                    if (this.GRFOB.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GRFOB.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.GRFOB.Value);
                    }
                    return string.Format("{0:#,0.00}", this.GRFOB.Value);

                }
                else
                    return "0";
            }
        }
        public double? GRStdMatAmt { get; set; }
        public string GRStdMatAmtstring
        {
            get
            {
                if (this.GRStdMatAmt.HasValue)
                {
                    if (this.GRStdMatAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GRStdMatAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.GRStdMatAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.GRStdMatAmt.Value);

                }
                else
                    return "";
            }
        }
        public double? GRMatCostConsAmt { get; set; }
        public string GRMatCostConsAmtstring
        {
            get
            {
                if (this.GRMatCostConsAmt.HasValue)
                {
                    if (this.GRMatCostConsAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GRMatCostConsAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.GRMatCostConsAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.GRMatCostConsAmt.Value);

                }
                else
                    return "";
            }
        }
        public double? GRMatCostLLAmt { get; set; }
        public string GRMatCostLLAmtstring
        {
            get
            {
                if (this.GRMatCostLLAmt.HasValue)
                {
                    if (this.GRMatCostLLAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GRMatCostLLAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.GRMatCostLLAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.GRMatCostLLAmt.Value);

                }
                else
                    return "";
            }
        }

        public double? GRMatCostALAmt { get; set; }
        public string GRMatCostALAmtstring
        {
            get
            {
                if (this.GRMatCostALAmt.HasValue)
                {
                    if (this.GRMatCostALAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GRMatCostALAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.GRMatCostALAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.GRMatCostALAmt.Value);

                }
                else
                    return "";
            }
        }


        public string Payment { get; set; }
        public double? Sum_MatCostKoreaAL { get; set; }
        public string Sum_MatCostKoreaALstring
        {
            get
            {
                if (this.Sum_MatCostKoreaAL.HasValue)
                {
                    if (this.Sum_MatCostKoreaAL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_MatCostKoreaAL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Sum_MatCostKoreaAL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Sum_MatCostKoreaAL.Value);

                }
                else
                    return "";
            }
        }

        public double? Sum_SalseQty { get; set; }
        public string Sum_SalseQtystring
        {
            get
            {
                if (this.Sum_SalseQty.HasValue)
                {
                    if (this.Sum_SalseQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_SalseQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Sum_SalseQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Sum_SalseQty.Value);

                }
                else
                    return "";
            }
        }
        public double? Sum_GRQtyAllPeriod { get; set; }
        public string Sum_GRQtyAllPeriodstring
        {
            get
            {
                if (this.Sum_GRQtyAllPeriod.HasValue)
                {
                    if (this.Sum_GRQtyAllPeriod.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_GRQtyAllPeriod.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Sum_GRQtyAllPeriod.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Sum_GRQtyAllPeriod.Value);

                }
                else
                    return "";
            }
        }

        public double? Sum_Balance { get; set; }
        public string Sum_Balancestring
        {
            get
            {
                if (this.Sum_Balance.HasValue)
                {
                    if (this.Sum_Balance.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_Balance.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Sum_Balance.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Sum_Balance.Value);

                }
                else
                    return "";
            }
        }

        public double? Sum_GRQtyPeriod { get; set; }
        public string Sum_GRQtyPeriodstring
        {
            get
            {
                if (this.Sum_GRQtyPeriod.HasValue)
                {
                    if (this.Sum_GRQtyPeriod.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_GRQtyPeriod.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Sum_GRQtyPeriod.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Sum_GRQtyPeriod.Value);

                }
                else
                    return "";
            }
        }
        public double? Sum_GRFOB { get; set; }
        public string Sum_GRFOBstring
        {
            get
            {
                if (this.Sum_GRFOB.HasValue)
                {
                    if (this.Sum_GRFOB.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_GRFOB.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Sum_GRFOB.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Sum_GRFOB.Value);

                }
                else
                    return "";
            }
        }
        public double? Sum_GRStdMatAmt { get; set; }
        public string Sum_GRStdMatAmtstring
        {
            get
            {
                if (this.Sum_GRStdMatAmt.HasValue)
                {
                    if (this.Sum_GRStdMatAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_GRStdMatAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Sum_GRStdMatAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Sum_GRStdMatAmt.Value);

                }
                else
                    return "";
            }
        }
        public double? Sum_GRMatCostConsAmt { get; set; }
        public string Sum_GRMatCostConsAmtstring
        {
            get
            {
                if (this.Sum_GRMatCostConsAmt.HasValue)
                {
                    if (this.Sum_GRMatCostConsAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_GRMatCostConsAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Sum_GRMatCostConsAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Sum_GRMatCostConsAmt.Value);

                }
                else
                    return "";
            }
        }
        public double? Sum_GRMatCostLLAmt { get; set; }
        public string Sum_GRMatCostLLAmtstring
        {
            get
            {
                if (this.Sum_GRMatCostLLAmt.HasValue)
                {
                    if (this.Sum_GRMatCostLLAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_GRMatCostLLAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Sum_GRMatCostLLAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Sum_GRMatCostLLAmt.Value);

                }
                else
                    return "";
            }
        }
        public double? Sum_GRMatCostALAmt { get; set; }
        public string Sum_GRMatCostALAmtstring
        {
            get
            {
                if (this.Sum_GRMatCostALAmt.HasValue)
                {
                    if (this.Sum_GRMatCostALAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Sum_GRMatCostALAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Sum_GRMatCostALAmt.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Sum_GRMatCostALAmt.Value);

                }
                else
                    return "";
            }
        }
        public Boolean Costing { get; set; }
        public Boolean ClosedOnFGGR { get; set; }
    }
}
﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class STBasicStock_Custom: STBasicStock
    {
        public int RowID { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public string UnitCD { get; set; }
        public string PUnit { get; set; }
        public bool UnitChk { get; set; }
        public bool BLankChk { get; set; }
        public bool LenKChk { get; set; }
        
    }
}
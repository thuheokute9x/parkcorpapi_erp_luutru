﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TConsItem_Custom: TConsItem
    {
        public string ProdOrder { get; set; }
        public string ProdCode { get; set; }
        public string MatCat { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public string PUNIT { get; set; }
        public double? Cons { get; set; }
        public string Consstring
        {
            get
            {
                if (this.Cons.HasValue)
                {
                    if (this.Cons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Cons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Cons.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Cons.Value);
                }
                else
                    return "0";
            }
        }
        public string LossRatestring
        {
            get
            {
                    if (this.LossRate == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.LossRate.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.LossRate);
                    }
                    return string.Format("{0:#,##0.#######}", this.LossRate);
            }
        }
        public string LossQtystring
        {
            get
            {
                if (this.LossQty == 0)
                {
                    return "0";
                }
                int value;
                if (int.TryParse(this.LossQty.ToString(), out value))
                {
                    return string.Format("{0:#,###}", this.LossQty);
                }
                return string.Format("{0:#,##0.#######}", this.LossQty);
            }
        }
        public double? SalseQty { get; set; }
        public string SalseQtystring
        {
            get
            {
                if (this.SalseQty.HasValue)
                {
                    if (this.SalseQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SalseQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SalseQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SalseQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? ConsQty { get; set; }
        public string ConsQtystring
        {
            get
            {
                if (this.ConsQty.HasValue)
                {
                    if (this.ConsQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsQty.Value);
                }
                else
                    return "0";
            }
        }

        public double? LossRateQty { get; set; }
        public string LossRateQtystring
        {
            get
            {
                if (this.LossRateQty.HasValue)
                {
                    if (this.LossRateQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.LossRateQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.LossRateQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.LossRateQty.Value);
                }
                else
                    return "0";
            }
        }
        public double? ConsSum { get; set; }
        public string ConsSumstring
        {
            get
            {
                if (this.ConsSum.HasValue)
                {
                    if (this.ConsSum.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsSum.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsSum.Value);
                }
                else
                    return "0";
            }
        }

        public double? PriceLotSizeTPInfo { get; set; }
        public string PriceLotSizeTPInfostring
        {
            get
            {
                if (this.PriceLotSizeTPInfo.HasValue)
                {
                    if (this.PriceLotSizeTPInfo.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceLotSizeTPInfo.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceLotSizeTPInfo.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceLotSizeTPInfo.Value);
                }
                else
                    return "0";
            }
        }

        public double? PriceIncotermsTPInfo { get; set; }
        public string PriceIncotermsTPInfostring
        {
            get
            {
                if (this.PriceIncotermsTPInfo.HasValue)
                {
                    if (this.PriceIncotermsTPInfo.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceIncotermsTPInfo.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceIncotermsTPInfo.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceIncotermsTPInfo.Value);
                }
                else
                    return "0";
            }
        }
        public string CURRTPInfo { get; set; }

        public double? PriceTPInfo { get; set; }
        public string PriceTPInfostring
        {
            get
            {
                if (this.PriceTPInfo.HasValue)
                {
                    if (this.PriceTPInfo.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceTPInfo.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceTPInfo.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceTPInfo.Value);
                }
                else
                    return "0";
            }
        }

        public double? PriceUSDTPInfo { get; set; }
        public string PriceUSDTPInfostring
        {
            get
            {
                if (this.PriceUSDTPInfo.HasValue)
                {
                    if (this.PriceUSDTPInfo.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceUSDTPInfo.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceUSDTPInfo.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceUSDTPInfo.Value);
                }
                else
                    return "0";
            }
        }

        public double? PriceGR { get; set; }
        public string PriceGRstring
        {
            get
            {
                if (this.PriceGR.HasValue)
                {
                    if (this.PriceGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceGR.Value);
                }
                else
                    return "0";
            }
        }

        public string CURR { get; set; }

        public double? PriceUSDGR { get; set; }
        public string PriceUSDGRstring
        {
            get
            {
                if (this.PriceUSDGR.HasValue)
                {
                    if (this.PriceUSDGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceUSDGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceUSDGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceUSDGR.Value);
                }
                else
                    return "0";
            }
        }

        public double? AmtPriceGR { get; set; }
        public string AmtPriceGRstring
        {
            get
            {
                if (this.AmtPriceGR.HasValue)
                {
                    if (this.AmtPriceGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtPriceGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtPriceGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtPriceGR.Value);
                }
                else
                    return "0";
            }
        }

        public double? AmtPriceGRUSD { get; set; }
        public string AmtPriceGRUSDstring
        {
            get
            {
                if (this.AmtPriceGRUSD.HasValue)
                {
                    if (this.AmtPriceGRUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtPriceGRUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtPriceGRUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtPriceGRUSD.Value);
                }
                else
                    return "0";
            }
        }

        public double? AmtTPInfo { get; set; }
        public string AmtTPInfostring
        {
            get
            {
                if (this.AmtTPInfo.HasValue)
                {
                    if (this.AmtTPInfo.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtTPInfo.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtTPInfo.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtTPInfo.Value);
                }
                else
                    return "0";
            }
        }

        public double? AmtTPInfoUSD { get; set; }
        public string AmtTPInfoUSDstring
        {
            get
            {
                if (this.AmtTPInfoUSD.HasValue)
                {
                    if (this.AmtTPInfoUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtTPInfoUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtTPInfoUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtTPInfoUSD.Value);
                }
                else
                    return "0";
            }
        }
        public double? ConsSumGRUSD { get; set; }
        public string ConsSumGRUSDstring
        {
            get
            {
                if (this.ConsSumGRUSD.HasValue)
                {
                    if (this.ConsSumGRUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsSumGRUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsSumGRUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsSumGRUSD.Value);
                }
                else
                    return "0";
            }
        }

        public double? ConsSumTPInfoUSD { get; set; }
        public string ConsSumTPInfoUSDstring
        {
            get
            {
                if (this.ConsSumTPInfoUSD.HasValue)
                {
                    if (this.ConsSumTPInfoUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsSumTPInfoUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsSumTPInfoUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsSumTPInfoUSD.Value);
                }
                else
                    return "0";
            }
        }

        public double? ConsSumUSD { get; set; }
        public string ConsSumUSDstring
        {
            get
            {
                if (this.ConsSumUSD.HasValue)
                {
                    if (this.ConsSumUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsSumUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsSumUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsSumUSD.Value);
                }
                else
                    return "0";
            }
        }

        public string PGrpCD { get; set; }
        public string Incoterms { get; set; }




        public string BrandCD { get; set; }
        public string RangeDesc { get; set; }
        public string ProdDesc { get; set; }
        public string SKUCode { get; set; }
        public string StyleCode { get; set; }


        public string ColorName { get; set; }
        public string ColorCode { get; set; }
        public double? OrderQty { get; set; }
        public string OrderQtystring
        {
            get
            {
                if (this.OrderQty.HasValue)
                {
                    if (this.OrderQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.OrderQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.OrderQty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.OrderQty.Value);
                }
                else
                    return "";
            }
        }
        public double? MatCost { get; set; }

        public string MatCoststring
        {
            get
            {
                if (this.MatCost.HasValue)
                {
                    if (this.MatCost.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.MatCost.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.MatCost.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.MatCost.Value);
                }
                else
                    return "";
            }
        }


        public double? MatCostCons { get; set; }

        public string MatCostConsstring
        {
            get
            {
                if (this.MatCostCons.HasValue)
                {
                    if (this.MatCostCons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.MatCostCons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.MatCostCons.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.MatCostCons.Value);
                }
                else
                    return "";
            }
        }
        public double? Diff { get; set; }

        public string Diffstring
        {
            get
            {
                if (this.Diff.HasValue)
                {
                    if (this.Diff.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Diff.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Diff.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.Diff.Value);
                }
                else
                    return "";
            }
        }


        public double? DiffPercent { get; set; }

        public string DiffPercentstring
        {
            get
            {
                if (this.DiffPercent.HasValue)
                {
                    if (this.DiffPercent.Value == 0)
                    {
                        return "0" + "%";
                    }
                    int value;
                    if (int.TryParse(this.DiffPercent.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0}" +"%", this.DiffPercent.Value);
                    }
                    return string.Format("{0:#,##0}" + "%", this.DiffPercent.Value);
                }
                else
                    return "";
            }
        }



        public double? POAmt { get; set; }

        public string POAmtstring
        {
            get
            {
                if (this.POAmt.HasValue)
                {
                    if (this.POAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.POAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.POAmt.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.POAmt.Value);
                }
                else
                    return "";
            }
        }

        public double? ActAmt { get; set; }

        public string ActAmtstring
        {
            get
            {
                if (this.ActAmt.HasValue)
                {
                    if (this.ActAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ActAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ActAmt.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.ActAmt.Value);
                }
                else
                    return "";
            }
        }

        public double? GIAmt { get; set; }

        public string GIAmtstring
        {
            get
            {
                if (this.GIAmt.HasValue)
                {
                    if (this.GIAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GIAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GIAmt.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GIAmt.Value);
                }
                else
                    return "";
            }
        }
        public double? GRAmt { get; set; }

        public string GRAmtstring
        {
            get
            {
                if (this.GRAmt.HasValue)
                {
                    if (this.GRAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.GRAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.GRAmt.Value);
                    }
                    return string.Format("{0:#,##0.##}", this.GRAmt.Value);
                }
                else
                    return "";
            }
        }
        public string ProdOrderFrom { get; set; }
        public string ProdOrderTo { get; set; }

        public Boolean Costing { get; set; }


        #region ERP_TConsItem_ConsWithPriceTPinfo_GR definition




        public string MatColor { get; set; }
        public string MH { get; set; }
        public bool Outsourcing { get; set; }
        public string Outsourcingstring { get; set; }

        public Double? ConsLoss { get; set; }
        public string ConsLossstring
        {
            get
            {
                if (this.ConsLoss.HasValue)
                {
                    if (this.ConsLoss.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.ConsLoss.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.ConsLoss.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.ConsLoss.Value);
                }
                else
                    return "";
            }
        }
        public Double? PriceLot { get; set; }
        public string PriceLotstring
        {
            get
            {
                if (this.PriceLot.HasValue)
                {
                    if (this.PriceLot.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceLot.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.PriceLot.Value);
                    }
                    return string.Format("{0:#,0.00}", this.PriceLot.Value);
                }
                else
                    return "";
            }
        }
        public Double? PricePInfo { get; set; }
        public string PricePInfostring
        {
            get
            {
                if (this.PricePInfo.HasValue)
                {
                    if (this.PricePInfo.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PricePInfo.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PricePInfo.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PricePInfo.Value);
                }
                else
                    return "";
            }
        }
        public string CurrPInfo { get; set; }
        public Double? PRICEGR { get; set; }
        public string PRICEGRstring
        {
            get
            {
                if (this.PRICEGR.HasValue)
                {
                    if (this.PRICEGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICEGR.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICEGR.Value);
                }
                else
                    return "";
            }
        }
        public string CurrGR { get; set; }
        public double? Comp { get; set; }

        public string Compstring
        {
            get
            {
                if (this.Comp.HasValue)
                {
                    if (this.Comp.Value == 0)
                    {
                        return "0" + "%";
                    }
                    int value;
                    if (int.TryParse(this.Comp.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,##0}" + "%", this.Comp.Value);
                    }
                    return string.Format("{0:#,##0}" + "%", this.Comp.Value);
                }
                else
                    return "";
            }
        }
        public Double? PRICEUSD { get; set; }
        public string PRICEUSDstring
        {
            get
            {
                if (this.PRICEUSD.HasValue)
                {
                    if (this.PRICEUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICEUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICEUSD.Value);
                }
                else
                    return "";
            }
        }
        public string Payment { get; set; }
        public Double? AmtCons { get; set; }
        public string AmtConsstring
        {
            get
            {
                if (this.AmtCons.HasValue)
                {
                    if (this.AmtCons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtCons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtCons.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtCons.Value);
                }
                else
                    return "";
            }
        }
        public Double? AmtConsLoss { get; set; }
        public string AmtConsLossstring
        {
            get
            {
                if (this.AmtConsLoss.HasValue)
                {
                    if (this.AmtConsLoss.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtConsLoss.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtConsLoss.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtConsLoss.Value);
                }
                else
                    return "";
            }
        }
        public Double? QtyConsNL { get; set; }
        public string QtyConsNLstring
        {
            get
            {
                if (this.QtyConsNL.HasValue)
                {
                    if (this.QtyConsNL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyConsNL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.QtyConsNL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.QtyConsNL.Value);
                }
                else
                    return "";
            }
        }
        public Double? QtyConsLoss { get; set; }
        public string QtyConsLossstring
        {
            get
            {
                if (this.QtyConsLoss.HasValue)
                {
                    if (this.QtyConsLoss.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.QtyConsLoss.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.QtyConsLoss.Value);
                    }
                    return string.Format("{0:#,0.00}", this.QtyConsLoss.Value);
                }
                else
                    return "";
            }
        }

        public Double? AmtSalesConsNL { get; set; }
        public string AmtSalesConsNLstring
        {
            get
            {
                if (this.AmtSalesConsNL.HasValue)
                {
                    if (this.AmtSalesConsNL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtSalesConsNL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtSalesConsNL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtSalesConsNL.Value);
                }
                else
                    return "";
            }
        }

        public Double? AmtSalesConsLoss { get; set; }
        public string AmtSalesConsLossstring
        {
            get
            {
                if (this.AmtSalesConsLoss.HasValue)
                {
                    if (this.AmtSalesConsLoss.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtSalesConsLoss.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtSalesConsLoss.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtSalesConsLoss.Value);
                }
                else
                    return "";
            }
        }

        public Double? SumAmtConsNL { get; set; }
        public string SumAmtConsNLstring
        {
            get
            {
                if (this.SumAmtConsNL.HasValue)
                {
                    if (this.SumAmtConsNL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtConsNL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumAmtConsNL.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumAmtConsNL.Value);
                }
                else
                    return "";
            }
        }

        public Double? SumAmtConsLoss { get; set; }
        public string SumAmtConsLossstring
        {
            get
            {
                if (this.SumAmtConsLoss.HasValue)
                {
                    if (this.SumAmtConsLoss.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtConsLoss.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumAmtConsLoss.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumAmtConsLoss.Value);
                }
                else
                    return "";
            }
        }

        public Double? SumAmtSalesConsNL { get; set; }
        public string SumAmtSalesConsNLstring
        {
            get
            {
                if (this.SumAmtSalesConsNL.HasValue)
                {
                    if (this.SumAmtSalesConsNL.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtSalesConsNL.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmtSalesConsNL.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmtSalesConsNL.Value);
                }
                else
                    return "";
            }
        }

        public Double? SumAmtSalesConsLoss { get; set; }
        public string SumAmtSalesConsLossstring
        {
            get
            {
                if (this.SumAmtSalesConsLoss.HasValue)
                {
                    if (this.SumAmtSalesConsLoss.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtSalesConsLoss.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmtSalesConsLoss.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmtSalesConsLoss.Value);
                }
                else
                    return "";
            }
        }



        #endregion

        public Double? AmtMatCost { get; set; }
        public string AmtMatCoststring
        {
            get
            {
                if (this.AmtMatCost.HasValue)
                {
                    if (this.AmtMatCost.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtMatCost.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtMatCost.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtMatCost.Value);
                }
                else
                    return "";
            }
        }
        public Double? AmtMatCostCons { get; set; }
        public string AmtMatCostConsstring
        {
            get
            {
                if (this.AmtMatCostCons.HasValue)
                {
                    if (this.AmtMatCostCons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtMatCostCons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtMatCostCons.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtMatCostCons.Value);
                }
                else
                    return "";
            }
        }

        public Double? SumAmtMatCost { get; set; }
        public string SumAmtMatCoststring
        {
            get
            {
                if (this.SumAmtMatCost.HasValue)
                {
                    if (this.SumAmtMatCost.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtMatCost.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmtMatCost.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmtMatCost.Value);
                }
                else
                    return "";
            }
        }
        public Double? SumAmtMatCostCons { get; set; }
        public string SumAmtMatCostConsstring
        {
            get
            {
                if (this.SumAmtMatCostCons.HasValue)
                {
                    if (this.SumAmtMatCostCons.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtMatCostCons.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmtMatCostCons.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmtMatCostCons.Value);
                }
                else
                    return "";
            }
        }

        public int TotalRows { get; set; }
        public int RowID { get; set; }
        public string RefVKNo { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
    }
}
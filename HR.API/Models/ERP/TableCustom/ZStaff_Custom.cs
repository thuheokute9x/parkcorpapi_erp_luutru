﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class ZStaff_Custom : ZStaff
    {
        public int Type { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public string PassWordOld { get; set; }
        public string EmpName { get; set; }
        public string EmpNo { get; set; }

    }
}
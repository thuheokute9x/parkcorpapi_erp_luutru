﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class STPayDoc_List_Custom : STPayDoc_List
    {
        public string PayDocDesc { get; set; }
        public bool? isPDF
        {
            get
            {
                if (!string.IsNullOrEmpty(this.FilePath) && ( this.FilePath.Contains(".pdf") || this.FilePath.Contains(".PDF")))
                {
                    return true;
                }
                else
                    return false;
            }
        }
        public string RglPayReqID { get; set; }
        public string PayDocVendorCD { get; set; }
        public string PayDocCD_Parent { get; set; }
        public string PayDocNo { get; set; }
        public string PayType { get; set; }
        public int PayDocSN { get; set; }
        

    }
}
﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class STOut_List_Custom: STOut_List
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public Double Surchar { get; set; }




        
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string VendorCD { get; set; }
        public string ProdOrder { get; set; }
        public DateTime ODATE { get; set; }
        public string ODATEConvert { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public string CustItemNo1 { get; set; }
        public bool UnitDiff { get; set; }
        public Double ConvFactorGI { get; set; }
        public string ConvQtyGI { get; set; }
        public string ConvUnitGI { get; set; }

        public double? OQTYSum { get; set; }
        public string OQTYSumstring
        {
            get
            {
                if (this.OQTYSum.HasValue)
                {
                    if (this.OQTYSum.Value == 0)
                    {
                        return "0.00";
                    }
                    int value;
                    if (int.TryParse(this.OQTYSum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.OQTYSum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.OQTYSum.Value);
                }
                else
                    return "";
            }
        }

        public double? ConvQtyGISum { get; set; }
        public string ConvQtyGISumstring
        {
            get
            {
                if (this.ConvQtyGISum.HasValue)
                {
                    if (this.ConvQtyGISum.Value == 0)
                    {
                        return "0.00";
                    }
                    int value;
                    if (int.TryParse(this.ConvQtyGISum.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.ConvQtyGISum.Value);
                    }
                    return string.Format("{0:#,0.00}", this.ConvQtyGISum.Value);
                }
                else
                    return "";
            }
        }

        public string PRIDMATCD { get; set; }
        public string RName { get; set; }

    }
}
﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TBrand_Custom: TBrand
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int Type { get; set; }
        public string BrandCDCustGrpCD { get; set; }
    }
}
﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TTMenuWebERP_Custom: TTMenuWebERP
    {
        public int StaffID { get; set; }
        public bool expand { get; set; } = false;
        public string key { get; set; }
        public string title { get; set; }
        public bool? isLeaf { get; set; }
        public bool? disabled { get; set; }
    }
}
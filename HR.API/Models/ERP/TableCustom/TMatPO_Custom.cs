﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TMatPO_Custom: TMatPO
    {
        public string PRID_Custom { get; set; }
        public string PRINTTYPE { get; set; }
        public string VendorName { get; set; }
        public string VendorNamePrintMPO { get; set; }
        public string ShipTo1 { get; set; }
        public string ShipTo2 { get; set; }
        public string ShipTo3 { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string PostingDateConvert { get; set; }
        public string PostingDateReportConvert { get; set; }
        public string PostingDateReportTiengHanConvert { get; set; }
        public string ReqETDConvert { get; set; }
        public string ReqETDReportConvert { get; set; }
        public string ReqETDReportTiengHanConvert { get; set; }
    }
}
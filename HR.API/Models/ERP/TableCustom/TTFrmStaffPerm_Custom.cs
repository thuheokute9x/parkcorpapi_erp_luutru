﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TTFrmStaffPerm_Custom : TTFrmStaffPerm
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int RowID { get; set; }
        public int Type { get; set; }
        public int StaffIDCopy { get; set; }
        


        public int TotalRows { get; set; }
        public bool IsDataInsert { get; set; }
        public string EmpName { get; set; }
        public string EmpID { get; set; }
        public string PWD { get; set; }
        public bool expand { get; set; } = false;
        public int MenuID { get; set; }
        public string FormNameDesc { get; set; }
        public int Level { get; set; }
        public int Range { get; set; }
        public int IsCheck { get; set; }
        public int ParentID { get; set; }
    }
}
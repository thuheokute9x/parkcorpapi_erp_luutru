﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class STIn_List_Custom : STIn_List
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public string SHIPPER { get; set; }
        public bool Select { get; set; }

        public string MatDesc { get; set; }
        public string Color { get; set; }

        public string PRICEstring
        {
            get
            {

                    if (this.PRICE == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICE.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICE);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICE);
            }
        }

        public double? PRICEUSD { get; set; }

        public string PRICEUSDstring
        {
            get
            {
                if (this.PRICEUSD.HasValue)
                {
                    if (this.PRICEUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PRICEUSD.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PRICEUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PRICEUSD.Value);
                }
                else
                    return "0";
            }
        }

        public double? PriceLotSize { get; set; }

        public string PriceLotSizestring
        {
            get
            {
                if (this.PriceLotSize.HasValue)
                {
                    if (this.PriceLotSize.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceLotSize.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceLotSize.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceLotSize.Value);
                }
                else
                    return null;
            }
        }

        public double? PriceIncoterms { get; set; }

        public string PriceIncotermsstring
        {
            get
            {
                if (this.PriceIncoterms.HasValue)
                {
                    if (this.PriceIncoterms.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.PriceIncoterms.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.PriceIncoterms.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.PriceIncoterms.Value);
                }
                else
                    return null;
            }
        }

        public bool ISTPInfor { get; set; }
    }
}
﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TMatGrp_Custom: TMatGrp
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public bool Select { get; set; }
        public string RangeDesc { get; set; }
        public string SeasonDesc { get; set; }
        public string CountDMatCDAndMatCD { get; set; }
        
    }
}
﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TPRItem_Custom: TPRItem
    {
        public bool expand { get; set; }
        public bool Sel { get; set; }
        public int AltMat_SmallInt { get; set; }
        public bool AltMat_Bit { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public string TBPQty { get; set; }
        public decimal TBPQtyFloat { get; set; }
        public string CustItemNo1 { get; set; }
        public string YUnit { get; set; }
        public int MHID { get; set; }
        public string MH1 { get; set; }
        public decimal NotReady { get; set; }
        public decimal SumAltMat { get; set; }
        public decimal MOQNum { get; set; }
        public decimal Applied { get; set; }
        public decimal TBPNum { get; set; }
        public decimal OverNum { get; set; }


        public Nullable<float> ActQty { get; set; }
        public string RefVKNo { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int Type { get; set; }

    }
}
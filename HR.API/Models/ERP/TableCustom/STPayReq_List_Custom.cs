﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class STPayReq_List_Custom: STPayReq_List
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public string PayDocIDList { get; set; }
        public bool Select { get; set; } = false;
        public string Qtystring
        {
            get
            {
                if (this.Qty.HasValue)
                {
                    if (this.Qty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Qty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Qty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Qty.Value);

                }
                else
                    return "0";
            }
        }
        public string Pricestring
        {
            get
            {
                if (this.Price.HasValue)
                {
                    if (this.Price.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Price.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Price.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Price.Value);

                }
                else
                    return "0";
            }
        }
        public string Amountstring
        {
            get
            {
                if (this.Amount.HasValue)
                {
                    if (this.Amount.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Amount.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Amount.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Amount.Value);

                }
                else
                    return "0";
            }
        }
    }
}
﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TMatPOItem_Custom: TMatPOItem
    {
        public int RowID { get; set; }
        public int RowGRVendor { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public string RefVKNo { get; set; }
        public string MatDesc { get; set; }
        public string MatDescColor { get; set; }
        public string PGrpCD { get; set; }
        public Nullable<float> ActQty { get; set; }
        public string CustItemNo1 { get; set; }
        public string CustItemNo2 { get; set; }
        public string VendorName { get; set; }
    }
}
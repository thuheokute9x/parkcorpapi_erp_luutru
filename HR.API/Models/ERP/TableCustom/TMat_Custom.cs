﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TMat_Custom: TMat
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int Type { get; set; }
        public int MatGrpID { get; set; }
        public string MatGrpDesc { get; set; }
        public int MatGrpItemID { get; set; }
        public Boolean Flag { get; set; }
        public string MatDesc { get; set; }
        public string MatGrpCode { get; set; }
        public string MH { get; set; }
        public string EmpNameCreatedBy { get; set; }
        public string EmpNameUpdatedBy { get; set; }

        public string CreatedOnstring
        {
            get
            {
                if (this.CreatedOn.HasValue)
                {

                    return this.CreatedOn.Value.ToString("dd/MMM/yyyy hh:mmtt");
                }
                else
                    return "";
            }
        }
        public string UpdateOnstring
        {
            get
            {
                if (this.UpdatedOn.HasValue)
                {

                    return this.UpdatedOn.Value.ToString("dd/MMM/yyyy hh:mmtt");
                }
                else
                    return "";
            }
        }

        public string DMatCD { get; set; }






        public string MatCat { get; set; }   
        public string Tag { get; set; }
        public bool UnderDev { get; set; }
        public Nullable<System.DateTime> ETAFromVendor { get; set; }
        public Nullable<System.DateTime> ETDToCust { get; set; }
        public string Couier { get; set; }
        public string TrackingNo { get; set; }
        public Nullable<short> SpecStatus { get; set; }
        public Nullable<System.DateTime> SpecApprovedOn { get; set; }
        public Nullable<short> QualityStatus { get; set; }
        public Nullable<System.DateTime> QualityApprovedOn { get; set; }
        public string TestReportNo { get; set; }
        public string DevRemark { get; set; }
        public bool ChangePInfo { get; set; }
        public Nullable<int> MatShapeID { get; set; }
        public Nullable<float> MatStdW { get; set; }
        public Nullable<float> MatStdH { get; set; }
        public string MatStdUnit { get; set; }
        public string MfgVendor { get; set; }
        public string Shipper { get; set; }
        public string ShipFrom { get; set; }
        public string PUnit { get; set; }
        public string PGrpCD { get; set; }
        public string VendorItemNo { get; set; }
        public Nullable<short> PLeadtime { get; set; }
        public Nullable<float> MOQ { get; set; }
        public string Incoterms { get; set; }
        public Nullable<int> PLotSize { get; set; }
        public Nullable<float> FreightRate { get; set; }
        public Nullable<float> InsuranceRate { get; set; }
        public Nullable<float> CommissionRate { get; set; }
        public Nullable<float> OtherCostRate { get; set; }
        public Nullable<double> PriceLotSize { get; set; }
        public Nullable<double> EXWPrice { get; set; }
        public string EXWCurr { get; set; }
        public Nullable<double> FOBPrice { get; set; }
        public string FOBCurr { get; set; }
        public Nullable<double> FOBcPrice { get; set; }
        public Nullable<double> CIFPrice { get; set; }
        public string CIFCurr { get; set; }
        public Nullable<double> BuyerPrice { get; set; }
        public string BuyerCurr { get; set; }
        public Nullable<float> MinPackingQty { get; set; }
        public Nullable<float> CBM { get; set; }
        public Nullable<float> Weight { get; set; }
        public Nullable<System.DateTime> ValidFrom { get; set; }
        public string Remark { get; set; }
        public string LossRatestring
        {
            get
            {
                    return string.Format("{0:0.00}" + '%', this.LossRate);
            }
        }


        public int CostingInt { get; set; }
        public int GRInt { get; set; }
        public int RapMuaInt { get; set; }
        public int POInt { get; set; }
        public int DiscontinueInt { get; set; }
        public string MatClassDesc { get; set; }
        public int PlanID { get; set; }


        public string MatCDList { get; set; }

    }
}
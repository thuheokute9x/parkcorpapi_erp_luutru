﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TDMat_Custom: TDMat
    {
        public string MatCDList { get; set; }
        public string DMatCDList { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int Type { get; set; }
        public int MatGrpID { get; set; }
        public int MatGrpItemID { get; set; }
        public Boolean Flag { get; set; }
        public string MatDesc { get; set; }
        public string MatGrpCode { get; set; }
        public string SrcMatGrpDesc { get; set; }
        public string FirstMatGrpDesc { get; set; }
        public string MH { get; set; }
        public string MHName { get; set; }  
        public string EmpNameCreatedBy { get; set; }
        public string EmpNameUpdatedBy { get; set; }

        public string CreatedOnstring
        {
            get
            {
                if (this.CreatedOn.HasValue)
                {

                    return this.CreatedOn.Value.ToString("dd/MMM/yyyy hh:mmtt");
                }
                else
                    return "";
            }
        }
        public string UpdateOnstring
        {
            get
            {
                if (this.UpdatedOn.HasValue)
                {

                    return this.UpdatedOn.Value.ToString("dd/MMM/yyyy hh:mmtt");
                }
                else
                    return "";
            }
        }
        public string NANull { get; set; }
        public bool Discontinue { get; set; }
        public bool Sel { get; set; }
        public int SrcMatGrpID { get; set; }
        public string MatGrpDesc { get; set; }
        public string MatShapeName { get; set; }
        public string MatShape { get; set; }
        public string ETAFromVendorConvert { get; set; }
        public string ETDToCustConvert { get; set; }
        public string SpecApprovedOnConvert { get; set; }
        public string QualityApprovedOnConvert { get; set; }
        public string LossRatestring
        {
            get
            {
                if (this.LossRate.HasValue)
                {
                    return string.Format("{0:#,0.000}", this.LossRate.Value);
                }
                else
                    return "";
            }
        }
        public string FreightRatestring
        {
            get
            {
                if (this.FreightRate.HasValue)
                {
                    return string.Format("{0:#,0.000}", this.FreightRate.Value);
                }
                else
                    return "";
            }
        }
        public string InsuranceRatestring
        {
            get
            {
                if (this.InsuranceRate.HasValue)
                {
                    return string.Format("{0:#,0.000}", this.InsuranceRate.Value);
                }
                else
                    return "";
            }
        }
        public string CommissionRatestring
        {
            get
            {
                if (this.CommissionRate.HasValue)
                {
                    return string.Format("{0:#,0.000}", this.CommissionRate.Value);
                }
                else
                    return "";
            }
        }
        public string OtherCostRatestring
        {
            get
            {
                if (this.OtherCostRate.HasValue)
                {
                    return string.Format("{0:#,0.000}", this.OtherCostRate.Value);
                }
                else
                    return "";
            }
        }
        public string ValidFromConvert { get; set; }
        public string MatClassDesc { get; set; }
        public int MultiSrcInt { get; set; }
        public int OutSourcingInt { get; set; }
        public bool NG { get; set; }
    }
}
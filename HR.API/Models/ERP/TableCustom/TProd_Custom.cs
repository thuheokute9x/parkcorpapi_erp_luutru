﻿using HR.API.Models.Entity;
using HR.API.Models.ERP.TableCustom.BaseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TProd_Custom : TProd
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public int DiscontinueInt { get; set; }
        public Boolean ViewBOMVersion { get; set; }
        public String ProdInstCD { get; set; }
        public String SeasonDesc { get; set; }
        public String RangeDesc { get; set; }
        public int PRID { get; set; }
        public String VKGRPCD { get; set; }
        public String RefVKNo { get; set; }
        public int ProdInstItemID { get; set; }
        public int Version { get; set; }
        public int BOMID { get; set; }
        public int ConsID { get; set; }
        public Double SalesAmt { get; set; }
        public Double ProdAmt { get; set; }
        public Double RemainAmt { get; set; }
        public String Remark { get; set; }
    }
}
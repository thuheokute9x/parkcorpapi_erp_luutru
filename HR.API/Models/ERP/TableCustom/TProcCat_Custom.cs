﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TProcCat_Custom: TProcCat
    {
        public bool expand { get; set; } = false;
    }
}
﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class STPayReq_Custom : STPayReq
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public bool Select { get; set; }
        public bool Checked { get; set; }
        public string RglPayReqID { get; set; }

        public bool HasSchg { get; set; }
        public string VendorName { get; set; }
        public string EmpName { get; set; }
        public string PayReqDateConvert { get; set; }
        public string PayDueDateConvert { get; set; }
        public bool expand { get; set; } = false;


        public Double? TotalPOExtRgl { get; set; }
        public string TotalPOExtRglstring
        {
            get
            {
                if (this.TotalPOExtRgl.HasValue)
                {
                    if (this.TotalPOExtRgl.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.TotalPOExtRgl.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.TotalPOExtRgl.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.TotalPOExtRgl.Value);

                }
                else
                    return "0";
            }
        }
        public Double? TotalPORgl { get; set; }
        public string TotalPORglstring
        {
            get
            {
                if (this.TotalPORgl.HasValue)
                {
                    if (this.TotalPORgl.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.TotalPORgl.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.TotalPORgl.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.TotalPORgl.Value);

                }
                else
                    return "0";
            }
        }
        public Double? TotalExtRgl { get; set; }
        public string TotalExtRglstring
        {
            get
            {
                if (this.TotalExtRgl.HasValue)
                {
                    if (this.TotalExtRgl.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.TotalExtRgl.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.TotalExtRgl.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.TotalExtRgl.Value);

                }
                else
                    return "0";
            }
        }
        public string CurrRgl { get; set; }
        public string FilePath { get; set; }
        public bool? isPDF
        {
            get
            {
                if (!string.IsNullOrEmpty(this.FilePath) && (this.FilePath.Contains(".pdf") || this.FilePath.Contains(".PDF")))
                {
                    return true;
                }
                else
                    return false;
            }
        }

    }
}
﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TProdInstItem_Custom: TProdInstItem
    {
        public string RangeDesc { get; set; }
        public string ProdCode { get; set; }
        public string SKUCode { get; set; }
        public string BrandCD { get; set; }
        public string ProdDesc { get; set; }
        public string Color { get; set; }
        public int ProdPriceID { get; set; }
        public decimal? FOB { get; set; }
        public decimal? MatCost { get; set; }
        public string BuyerPONo { get; set; }
        public float Qty { get; set; }
        public int SoldTo { get; set; }
        public int ShipTo { get; set; }
        public DateTime ReqETD { get; set; }
        public string SKUCodeBuyer { get; set; }
        public int SalseInfoID { get; set; }
    }
}
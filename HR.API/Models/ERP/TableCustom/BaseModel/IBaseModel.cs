﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.API.Models.ERP.TableCustom.BaseModel
{
    interface IBaseModel
    {
         int RowID { get; set; }
         int TotalRows { get; set; }
         int PageNumber { get; set; }
         int PageSize { get; set; }
         int Type { get; set; }
    }
}

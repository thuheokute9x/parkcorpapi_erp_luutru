﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TXLAutho_Custom : TXLAutho
    {
        public string EmpName { get; set; }
        public string EmpNo { get; set; }
        public string Dept { get; set; }
        
    }
}
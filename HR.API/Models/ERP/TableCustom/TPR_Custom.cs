﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TPR_Custom:TPR
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public DateTime? CreatedOn_From { get; set; }
        public DateTime? CreatedOn_To { get; set; }
        public int? TBPCnt { get; set; }
        public int? OverCnt { get; set; }
        public int? POCnt { get; set; }
        public int? MOQSum { get; set; }
        public int? Applie { get; set; }
        public Boolean StandBy { get; set; }
        public Boolean InProcess { get; set; }
        public Boolean Pending { get; set; }
        public Boolean Completed { get; set; }
        public Boolean Canceled { get; set; }
        public Boolean CloseOn { get; set; }
        public string PRCODE { get; set; }

        public int? OverCnt_Ready { get; set; }
        public int? TBPCnt_Ready { get; set; }
        public int? OverCnt_NotReady { get; set; }
        public int? TBPCnt_NotReady { get; set; }
        public int? OverCnt_AltMat { get; set; }
        public int? TBPCnt_AltMat { get; set; }
    }
}
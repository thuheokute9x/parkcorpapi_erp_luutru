﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableCustom
{
    public class TVendor_Custom: TVendor
    {
        public int PInfoID { get; set; }
        public bool? isPDF
        {
            get
            {
                if (!string.IsNullOrEmpty(this.FilePath) && (this.FilePath.Contains(".pdf") || this.FilePath.Contains(".PDF")))
                {
                    return true;
                }
                else
                    return false;
            }
        }
        public string TPGrp_ContactTo { get; set; }
        public bool POByShipper { get; set; }
        public bool POByMfgVendor { get; set; }
        public string BankUpdatedConvert { get; set; }
        public string MatCD { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int Type { get; set; }
    }
}
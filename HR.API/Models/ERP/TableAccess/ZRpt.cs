﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TableAccess
{
    public class ZRpt
    {
        public int? RptID { get; set; }
        public int? Seq { get; set; }
        public string RptCat { get; set; }
        public string RptDiv { get; set; }
        public string Flag { get; set; }
        public string RptName { get; set; }
        public string RptDesc { get; set; }
        public bool? OpenAsNewObj { get; set; }
        public string Avalia { get; set; }
    }
}
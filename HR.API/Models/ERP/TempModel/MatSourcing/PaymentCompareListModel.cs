﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TempModel.MatSourcing
{
    public class PaymentCompareListModel
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int Type { get; set; }
        public string RefVKNo { get; set; }
        public string Vendor { get; set; }
        public double? AmtPO { get; set; }
        public string AmtPOstring
        {
            get
            {
                if (this.AmtPO.HasValue)
                {
                    if (this.AmtPO.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtPO.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtPO.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtPO.Value);
                }
                else
                    return "";
            }
        }
        public double? AmtGR { get; set; }
        public string AmtGRstring
        {
            get
            {
                if (this.AmtGR.HasValue)
                {
                    if (this.AmtGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmtGR.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmtGR.Value);
                }
                else
                    return "";
            }
        }
        public double? AmountPayReq { get; set; }
        public string AmountPayReqstring
        {
            get
            {
                if (this.AmountPayReq.HasValue)
                {
                    if (this.AmountPayReq.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmountPayReq.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.AmountPayReq.Value);
                    }
                    return string.Format("{0:#,0.00}", this.AmountPayReq.Value);
                }
                else
                    return "";
            }
        }





        public double? SumAmtPO { get; set; }
        public string SumAmtPOstring
        {
            get
            {
                if (this.SumAmtPO.HasValue)
                {
                    if (this.SumAmtPO.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtPO.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmtPO.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmtPO.Value);
                }
                else
                    return "";
            }
        }
        public double? SumAmtGR { get; set; }
        public string SumAmtGRstring
        {
            get
            {
                if (this.SumAmtGR.HasValue)
                {
                    if (this.SumAmtGR.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtGR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmtGR.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmtGR.Value);
                }
                else
                    return "";
            }
        }
        public double? SumAmountPayReq { get; set; }
        public string SumAmountPayReqstring
        {
            get
            {
                if (this.SumAmountPayReq.HasValue)
                {
                    if (this.SumAmountPayReq.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmountPayReq.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmountPayReq.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmountPayReq.Value);
                }
                else
                    return "";
            }
        }
        public double? Diff_PO_GR { get; set; }
        public string Diff_PO_GRstring
        {
            get
            {
                if (this.Diff_PO_GR.HasValue)
                {
                    if (this.Diff_PO_GR.Value == 0)
                    {
                        return "0" + "%";
                    }
                    int value;
                    if (int.TryParse(this.Diff_PO_GR.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0}" + "%", this.Diff_PO_GR.Value);
                    }
                    return string.Format("{0:#,0}" + "%", this.Diff_PO_GR.Value);
                }
                else
                    return "";
            }
        }
        public double? Diff_GR_PayReq { get; set; }
        public string Diff_GR_PayReqstring
        {
            get
            {
                if (this.Diff_GR_PayReq.HasValue)
                {
                    if (this.Diff_GR_PayReq.Value == 0)
                    {
                        return "0" + "%";
                    }
                    int value;
                    if (int.TryParse(this.Diff_GR_PayReq.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0}" + "%", this.Diff_GR_PayReq.Value);
                    }
                    return string.Format("{0:#,0}" + "%", this.Diff_GR_PayReq.Value);
                }
                else
                    return "";
            }
        }
        public string Outsourcing { get; set; }
    }
}
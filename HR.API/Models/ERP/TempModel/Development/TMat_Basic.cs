﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TempModel.Development
{
    public class TMat_Basic
    {
        public string MatCD { get; set; }
        public string MatDesc { get; set; }
    }
}
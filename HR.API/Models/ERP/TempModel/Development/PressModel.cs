﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.TempModel.Development
{
    public class PressModel
    {
        public int MatGrpID { get; set; }
        public int MatGrpID_Old { get; set; }
        public string DMatCD { get; set; }
        public string MatCD { get; set; }
        public string MatDesc { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public string ProdDesc { get; set; }
        public string PatternNo { get; set; }
        public string Incoterms { get; set; }
        public double EXWPrice { get; set; }
        public string EXWCurr { get; set; }
        public double FOBPrice { get; set; }
        public string FOBCurr { get; set; }
        public double FOBcPrice { get; set; }
        public float CommissionRate { get; set; }
        public double CIFPrice { get; set; }
        public string CIFCurr { get; set; }
        public double BuyerPrice { get; set; }
        public string BuyerCurr { get; set; }
        public string MUnit { get; set; }
        public bool Ischange { get; set; } = true;
        
    }
}
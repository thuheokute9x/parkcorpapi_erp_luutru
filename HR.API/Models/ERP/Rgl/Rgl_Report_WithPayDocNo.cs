﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.Rgl
{
    public class Rgl_Report_WithPayDocNo
    {
        public Boolean expand { get; set; } = true;
        public string RglPayReqID { get; set; }
        public string VendorCD { get; set; }
        public int RowID { get; set; }
        public string PayType { get; set; }
        public string PayDocCD { get; set; }
        public string PayDocNo { get; set; }
        public string Curr { get; set; }
        public Double? Amt { get; set; }
        public string Amtstring
        {
            get
            {
                if (this.Amt.HasValue)
                {
                    if (this.Amt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Amt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Amt.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Amt.Value);

                }
                else
                    return "0";
            }
        }
        public Double? AmtSurcharge { get; set; }
        public string AmtSurchargestring
        {
            get
            {
                if (this.AmtSurcharge.HasValue)
                {
                    if (this.AmtSurcharge.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtSurcharge.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtSurcharge.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtSurcharge.Value);

                }
                else
                    return "0";
            }
        }

        public Double? AmtMat { get; set; }
        public string AmtMatstring
        {
            get
            {
                if (this.AmtMat.HasValue)
                {
                    if (this.AmtMat.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtMat.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtMat.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtMat.Value);

                }
                else
                    return "0";
            }
        }




        public Double? SumAmtMat { get; set; }
        public string SumAmtMatstring
        {
            get
            {
                if (this.SumAmtMat.HasValue)
                {
                    if (this.SumAmtMat.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtMat.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumAmtMat.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumAmtMat.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumSurcharge { get; set; }
        public string SumSurchargestring
        {
            get
            {
                if (this.SumSurcharge.HasValue)
                {
                    if (this.SumSurcharge.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumSurcharge.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumSurcharge.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumSurcharge.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumAmt_Vendor { get; set; }
        public string SumAmt_Vendorstring
        {
            get
            {
                if (this.SumAmt_Vendor.HasValue)
                {
                    if (this.SumAmt_Vendor.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmt_Vendor.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumAmt_Vendor.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumAmt_Vendor.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumAmtSurcharge_Vendor { get; set; }
        public string SumAmtSurcharge_Vendorstring
        {
            get
            {
                if (this.SumAmtSurcharge_Vendor.HasValue)
                {
                    if (this.SumAmtSurcharge_Vendor.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtSurcharge_Vendor.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumAmtSurcharge_Vendor.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumAmtSurcharge_Vendor.Value);

                }
                else
                    return "0";
            }
        }

        public Double? SumAmtMat_Vendor { get; set; }
        public string SumAmtMat_Vendorstring
        {
            get
            {
                if (this.SumAmtMat_Vendor.HasValue)
                {
                    if (this.SumAmtMat_Vendor.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtMat_Vendor.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumAmtMat_Vendor.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumAmtMat_Vendor.Value);

                }
                else
                    return "0";
            }
        }


        public Double? Total { get; set; }
        public string Totalstring
        {
            get
            {
                if (this.Total.HasValue)
                {
                    if (this.Total.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Total.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Total.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Total.Value);

                }
                else
                    return "0";
            }
        }
        public string CurrAll { get; set; }

        public int CountVendor { get; set; }
        public int STTWithVendor { get; set; }
        public Double? DN { get; set; }
        public string DNstring
        {
            get
            {
                if (this.DN.HasValue)
                {
                    if (this.DN.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.DN.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.DN.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.DN.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumDN { get; set; }
        public string SumDNstring
        {
            get
            {
                if (this.SumDN.HasValue)
                {
                    if (this.SumDN.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumDN.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumDN.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumDN.Value);

                }
                else
                    return "0";
            }
        }
        public Double? CN { get; set; }
        public string CNstring
        {
            get
            {
                if (this.CN.HasValue)
                {
                    if (this.CN.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.CN.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.CN.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.CN.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumCN { get; set; }
        public string SumCNstring
        {
            get
            {
                if (this.SumCN.HasValue)
                {
                    if (this.SumCN.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumCN.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumCN.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumCN.Value);

                }
                else
                    return "0";
            }
        }
        public Double? OT { get; set; }
        public string OTstring
        {
            get
            {
                if (this.OT.HasValue)
                {
                    if (this.OT.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.OT.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.OT.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.OT.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumOT { get; set; }
        public string SumOTstring
        {
            get
            {
                if (this.SumOT.HasValue)
                {
                    if (this.SumOT.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumOT.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumOT.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumOT.Value);

                }
                else
                    return "0";
            }
        }





        public Double? SumAmtMatNotUSD { get; set; }
        public string SumAmtMatNotUSDstring
        {
            get
            {
                if (this.SumAmtMatNotUSD.HasValue)
                {
                    if (this.SumAmtMatNotUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtMatNotUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumAmtMatNotUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumAmtMatNotUSD.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumSurchargeNotUSD { get; set; }
        public string SumSurchargeNotUSDstring
        {
            get
            {
                if (this.SumSurchargeNotUSD.HasValue)
                {
                    if (this.SumSurchargeNotUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumSurchargeNotUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumSurchargeNotUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumSurchargeNotUSD.Value);

                }
                else
                    return "0";
            }
        }
        public Double? TotalNotUSD { get; set; }
        public string TotalNotUSDstring
        {
            get
            {
                if (this.TotalNotUSD.HasValue)
                {
                    if (this.TotalNotUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.TotalNotUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.TotalNotUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.TotalNotUSD.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumDNNotUSD { get; set; }
        public string SumDNNotUSDstring
        {
            get
            {
                if (this.SumDNNotUSD.HasValue)
                {
                    if (this.SumDNNotUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumDNNotUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumDNNotUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumDNNotUSD.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumCNNotUSD { get; set; }
        public string SumCNNotUSDstring
        {
            get
            {
                if (this.SumCNNotUSD.HasValue)
                {
                    if (this.SumCNNotUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumCNNotUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumCNNotUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumCNNotUSD.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumOTNotUSD { get; set; }
        public string SumOTNotUSDstring
        {
            get
            {
                if (this.SumOTNotUSD.HasValue)
                {
                    if (this.SumOTNotUSD.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumOTNotUSD.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumOTNotUSD.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumOTNotUSD.Value);

                }
                else
                    return "0";
            }
        }
    }
}
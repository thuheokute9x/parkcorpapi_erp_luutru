﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.Rgl
{
    public class TTRglPayReq_Custom: TTRglPayReq
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public bool Select { get; set; }
        public string EmpName { get; set; }
        public string RglDateConvert { get; set; }
        public Boolean IsconfirmCEO { get; set; }
        public string ConfirmedCEOOnConvert { get; set; }
        public string ConfirmedManagerOnConvert { get; set; }
                      

    }
}
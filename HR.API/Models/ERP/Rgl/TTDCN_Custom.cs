﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.Rgl
{
    public class TTDCN_Custom : TTDCN
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public bool Select { get; set; }
        public string Amountstring
        {
            get
            {

                if (this.Amount == 0)
                {
                    return "0";
                }
                int value;
                if (int.TryParse(this.Amount.ToString(), out value))
                {
                    return string.Format("{0:#,###}", this.Amount);
                }
                return string.Format("{0:#,##0.#######}", this.Amount);


            }
        }
        public string ClosedOnString { get; set; }
    }
}
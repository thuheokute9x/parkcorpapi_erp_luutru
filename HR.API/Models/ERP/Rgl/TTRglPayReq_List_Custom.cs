﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.Rgl
{
    public class TTRglPayReq_List_Custom: TTRglPayReq_List
    {
        public bool Checked { get; set; }
        public bool HasSchg { get; set; }
        public string VendorCD { get; set; }
        public string VendorName { get; set; }
        public int? EmpNo { get; set; }
        public string EmpName { get; set; }
        public string PayReqDate { get; set; }
        public string PayDueDate { get; set; }
        public string PayReqTo { get; set; }
        public bool PaidOn { get; set; }
        public string Remark { get; set; }

        public bool? isPDF
        {
            get
            {
                if (!string.IsNullOrEmpty(this.FilePath) && (this.FilePath.Contains(".pdf") || this.FilePath.Contains(".PDF")))
                {
                    return true;
                }
                else
                    return false;
            }
        }

    }
}
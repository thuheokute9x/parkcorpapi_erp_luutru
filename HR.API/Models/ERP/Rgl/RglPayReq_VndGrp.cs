﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.Rgl
{
    public class RglPayReq_VndGrp
    {
        public Boolean expand { get; set; } = false;
        public Boolean HasDN { get; set; }
        public Boolean HasCN { get; set; }
        public Boolean HasDocOT { get; set; }
        public Boolean HasSurcharge { get; set; }
        public string RglPayReqID { get; set; }
        public string VendorCD { get; set; }
        public string Curr { get; set; }



        public Double? SumAmt { get; set; }
        public string SumAmtstring
        {
            get
            {
                if (this.SumAmt.HasValue)
                {
                    if (this.SumAmt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumAmt.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumAmt.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumAmtSurcharge { get; set; }
        public string SumAmtSurchargestring
        {
            get
            {
                if (this.SumAmtSurcharge.HasValue)
                {
                    if (this.SumAmtSurcharge.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtSurcharge.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumAmtSurcharge.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumAmtSurcharge.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumDN { get; set; }
        public string SumDNstring
        {
            get
            {
                if (this.SumDN.HasValue)
                {
                    if (this.SumDN.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumDN.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumDN.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumDN.Value);

                }
                else
                    return "0";
            }
        }

        public Double? SumOT { get; set; }
        public string SumOTstring
        {
            get
            {
                if (this.SumOT.HasValue)
                {
                    if (this.SumOT.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumOT.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumOT.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumOT.Value);

                }
                else
                    return "0";
            }
        }

        public Double? SumCN { get; set; }
        public string SumCNstring
        {
            get
            {
                if (this.SumCN.HasValue)
                {
                    if (this.SumCN.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumCN.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumCN.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumCN.Value);

                }
                else
                    return "0";
            }
        }

        public Double? Total { get; set; }
        public string Totalstring
        {
            get
            {
                if (this.Total.HasValue)
                {
                    if (this.Total.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Total.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Total.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Total.Value);

                }
                else
                    return "0";
            }
        }






        public Double? SumAmtMatAll { get; set; }
        public string SumAmtMatAllstring
        {
            get
            {
                if (this.SumAmtMatAll.HasValue)
                {
                    if (this.SumAmtMatAll.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtMatAll.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumAmtMatAll.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumAmtMatAll.Value);

                }
                else
                    return "0";
            }
        }

        public Double? SumSurchargeAll { get; set; }
        public string SumSurchargeAllstring
        {
            get
            {
                if (this.SumSurchargeAll.HasValue)
                {
                    if (this.SumSurchargeAll.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumSurchargeAll.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumSurchargeAll.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumSurchargeAll.Value);

                }
                else
                    return "0";
            }
        }

        public Double? DNAll { get; set; }
        public string DNAllstring
        {
            get
            {
                if (this.DNAll.HasValue)
                {
                    if (this.DNAll.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.DNAll.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.DNAll.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.DNAll.Value);

                }
                else
                    return "0";
            }
        }

        public Double? CNAll { get; set; }
        public string CNAllstring
        {
            get
            {
                if (this.CNAll.HasValue)
                {
                    if (this.CNAll.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.CNAll.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.CNAll.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.CNAll.Value);

                }
                else
                    return "0";
            }
        }

        public Double? OTAll { get; set; }
        public string OTAllstring
        {
            get
            {
                if (this.OTAll.HasValue)
                {
                    if (this.OTAll.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.OTAll.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.OTAll.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.OTAll.Value);

                }
                else
                    return "0";
            }
        }

        public Double? TotalAll { get; set; }
        public string TotalAllstring
        {
            get
            {
                if (this.TotalAll.HasValue)
                {
                    if (this.TotalAll.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.TotalAll.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.TotalAll.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.TotalAll.Value);

                }
                else
                    return "0";
            }
        }

        public string CurrAll { get; set; }
        public string PayDocNo { get; set; }
        public string PayDocCD { get; set; }
        public List<TTRglPayReqDCN_Custom> children { get; set; } = new List<TTRglPayReqDCN_Custom>();


    }
}
﻿using HR.API.Models.ERP.TableCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.Rgl
{
    public class RglPayReq_DocGrp
    {
        public Boolean expand { get; set; } = false;
        public int PayDocID { get; set; }
        public Boolean HasSC { get; set; }
        public Boolean HasPI { get; set; }
        public Boolean HasIV { get; set; }
        public Boolean HasPL { get; set; }
        public Boolean HasWB { get; set; }
        public string RglPayReqID { get; set; }
        public string VendorCD { get; set; }
        public int RowID { get; set; }
        public string PayType { get; set; }
        public string PayDocCD { get; set; }
        public string PayDocNo { get; set; }
        public string RefVKNo { get; set; }
        public string MatPOCD { get; set; }
        public string Vendor { get; set; }
        public string Shipper { get; set; }
        public string MatCD { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public int MatPOItemID { get; set; }
        public string Grade { get; set; }
        public string PUnit { get; set; }
        public Double? Qty { get; set; }
        public string Qtystring
        {
            get
            {
                if (this.Qty.HasValue)
                {
                    if (this.Qty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Qty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Qty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Qty.Value);

                }
                else
                    return "0";
            }
        }
        public Double? Price { get; set; }
        public string Pricestring
        {
            get
            {
                if (this.Price.HasValue)
                {
                    if (this.Price.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Price.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Price.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Price.Value);

                }
                else
                    return "0";
            }
        }
        public Double? Amount { get; set; }
        public string Amountstring
        {
            get
            {
                if (this.Amount.HasValue)
                {
                    if (this.Amount.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Amount.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Amount.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Amount.Value);

                }
                else
                    return "0";
            }
        }
        public string Curr { get; set; }
        public string ETDConvert { get; set; }
        public string ETAConvert { get; set; }
        public string Remark { get; set; }


        public Double? Amt { get; set; }
        public string Amtstring
        {
            get
            {
                if (this.Amt.HasValue)
                {
                    if (this.Amt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Amt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Amt.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Amt.Value);

                }
                else
                    return "0";
            }
        }
        public Double? AmtSurcharge { get; set; }
        public string AmtSurchargestring
        {
            get
            {
                if (this.AmtSurcharge.HasValue)
                {
                    if (this.AmtSurcharge.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtSurcharge.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtSurcharge.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtSurcharge.Value);

                }
                else
                    return "0";
            }
        }

        public Double? AmtMat { get; set; }
        public string AmtMatstring
        {
            get
            {
                if (this.AmtMat.HasValue)
                {
                    if (this.AmtMat.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.AmtMat.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.AmtMat.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.AmtMat.Value);

                }
                else
                    return "0";
            }
        }


        public Double? TotalExt { get; set; }
        public string TotalExtstring
        {
            get
            {
                if (this.TotalExt.HasValue)
                {
                    if (this.TotalExt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.TotalExt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.TotalExt.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.TotalExt.Value);

                }
                else
                    return "0";
            }
        }
        public Double? TotalPO { get; set; }
        public string TotalPOstring
        {
            get
            {
                if (this.TotalPO.HasValue)
                {
                    if (this.TotalPO.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.TotalPO.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.TotalPO.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.TotalPO.Value);

                }
                else
                    return "0";
            }
        }
        public Double? TotalPOExt { get; set; }
        public string TotalPOExtstring
        {
            get
            {
                if (this.TotalPOExt.HasValue)
                {
                    if (this.TotalPOExt.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.TotalPOExt.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.TotalPOExt.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.TotalPOExt.Value);

                }
                else
                    return "0";
            }
        }

        public Double? SumAmtMatDocGrp { get; set; }
        public string SumAmtMatDocGrpstring
        {
            get
            {
                if (this.SumAmtMatDocGrp.HasValue)
                {
                    if (this.SumAmtMatDocGrp.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmtMatDocGrp.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumAmtMatDocGrp.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumAmtMatDocGrp.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumSurchargeDocGrp { get; set; }
        public string SumSurchargeDocGrpstring
        {
            get
            {
                if (this.SumSurchargeDocGrp.HasValue)
                {
                    if (this.SumSurchargeDocGrp.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumSurchargeDocGrp.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.SumSurchargeDocGrp.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.SumSurchargeDocGrp.Value);

                }
                else
                    return "0";
            }
        }
        public Double? TotalDocGrp { get; set; }
        public string TotalDocGrpstring
        {
            get
            {
                if (this.TotalDocGrp.HasValue)
                {
                    if (this.TotalDocGrp.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.TotalDocGrp.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.TotalDocGrp.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.TotalDocGrp.Value);

                }
                else
                    return "0";
            }
        }
        public string CurrDocGrp { get; set; }
        public List<STPayDoc_List_Custom> children { get; set; } = new List<STPayDoc_List_Custom>();
    }
}
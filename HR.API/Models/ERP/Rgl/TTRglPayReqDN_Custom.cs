﻿using HR.API.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.Rgl
{
    public class TTRglPayReqDCN_Custom: TTRglPayReqDCN
    {
        public string DCNAmountstring
        {
            get
            {

                    if (this.DCNAmount == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.DCNAmount.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.DCNAmount);
                    }
                    return string.Format("{0:#,##0.#######}", this.DCNAmount);


            }
        }
        public string PayDocCD { get; set; }
        public string PayDocNo { get; set; }
        public string FilePath { get; set; }
        public bool? isPDF
        {
            get
            {
                if (!string.IsNullOrEmpty(this.FilePath) && (this.FilePath.Contains(".pdf") || this.FilePath.Contains(".PDF")))
                {
                    return true;
                }
                else
                    return false;
            }
        }
        public int PayDocID { get; set; }
        public int PayDocListID { get; set; }
        public double Amount { get; set; }
        public string Amountstring
        {
            get
            {

                if (this.Amount == 0)
                {
                    return "0";
                }
                int value;
                if (int.TryParse(this.Amount.ToString(), out value))
                {
                    return string.Format("{0:#,###}", this.Amount);
                }
                return string.Format("{0:#,##0.#######}", this.Amount);


            }
        }

        public double AmtUsed { get; set; }
        public string AmtUsedstring
        {
            get
            {

                if (this.AmtUsed == 0)
                {
                    return "0";
                }
                int value;
                if (int.TryParse(this.AmtUsed.ToString(), out value))
                {
                    return string.Format("{0:#,###}", this.AmtUsed);
                }
                return string.Format("{0:#,##0.#######}", this.AmtUsed);


            }
        }

        public double AmtBalance { get; set; }
        public string AmtBalancestring
        {
            get
            {

                if (this.AmtBalance == 0)
                {
                    return "0";
                }
                int value;
                if (int.TryParse(this.AmtBalance.ToString(), out value))
                {
                    return string.Format("{0:#,###}", this.AmtBalance);
                }
                return string.Format("{0:#,##0.#######}", this.AmtBalance);


            }
        }
        public double AmtBalanceTem { get; set; } 

        public double AmountTobe { get; set; }

        public double Total { get; set; }
        public string Totalstring
        {
            get
            {

                if (this.Total == 0)
                {
                    return "0";
                }
                int value;
                if (int.TryParse(this.Total.ToString(), out value))
                {
                    return string.Format("{0:#,###}", this.Total);
                }
                return string.Format("{0:#,##0.#######}", this.Total);


            }
        }
        public Boolean ClosedOn { get; set; }
        public Boolean Merge { get; set; }
        public int PayDocSN { get; set; }
        public string Remark { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.ERP.Rgl
{
    public class ERP_TTRglPayReq_Report_WithPayReqID
    {
        public int RowID { get; set; }
        public int TotalRows { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public string RglPayReqID { get; set; }
        public string RglDateConvert { get; set; }
        public string ConfirmedOnConvert { get; set; }
        public string PayReqID { get; set; }
        public string PayReqDateConvert { get; set; }
        public string PayDueDateConvert { get; set; }
        public string PayType { get; set; }
        public string PayDocCD { get; set; }
        public string PayDocNo { get; set; }
        public string RefVKNo { get; set; }
        public string MatPOCD { get; set; }
        public string Vendor { get; set; }
        public string Shipper { get; set; }
        public string MatCD { get; set; }
        public string MatDesc { get; set; }
        public string Color { get; set; }
        public int MatPOItemID { get; set; }
        public string Grade { get; set; }
        public string PUnit { get; set; }
        public Double? Qty { get; set; }
        public string Qtystring
        {
            get
            {
                if (this.Qty.HasValue)
                {
                    if (this.Qty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Qty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Qty.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Qty.Value);

                }
                else
                    return "0";
            }
        }
        public Double? Price { get; set; }
        public string Pricestring
        {
            get
            {
                if (this.Price.HasValue)
                {
                    if (this.Price.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Price.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,###}", this.Price.Value);
                    }
                    return string.Format("{0:#,##0.#######}", this.Price.Value);

                }
                else
                    return "0";
            }
        }
        public Double? Amount { get; set; }
        public string Amountstring
        {
            get
            {
                if (this.Amount.HasValue)
                {
                    if (this.Amount.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.Amount.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.Amount.Value);
                    }
                    return string.Format("{0:#,0.00}", this.Amount.Value);

                }
                else
                    return "0";
            }
        }
        public string Curr { get; set; }
        public string ETDConvert { get; set; }
        public string ETAConvert { get; set; }
        public string Remark { get; set; }

        public Double? SumQty { get; set; }
        public string SumQtystring
        {
            get
            {
                if (this.SumQty.HasValue)
                {
                    if (this.SumQty.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumQty.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumQty.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumQty.Value);

                }
                else
                    return "0";
            }
        }
        public Double? SumAmount { get; set; }
        public string SumAmountstring
        {
            get
            {
                if (this.SumAmount.HasValue)
                {
                    if (this.SumAmount.Value == 0)
                    {
                        return "0";
                    }
                    int value;
                    if (int.TryParse(this.SumAmount.Value.ToString(), out value))
                    {
                        return string.Format("{0:#,0.00}", this.SumAmount.Value);
                    }
                    return string.Format("{0:#,0.00}", this.SumAmount.Value);

                }
                else
                    return "0";
            }
        }
    }
}
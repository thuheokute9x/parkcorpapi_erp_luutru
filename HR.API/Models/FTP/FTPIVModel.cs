﻿using System;

namespace HR.API.Models.ERP
{
    public class FTPIVModel
    {
        public string PayDocCD { get; set; }
        public string PayDocCDDESS { get; set; }
        public string Patch { get; set; }
        public double? LeftOverQty { get; set; }
        public int TotalRows { get; set; }
        public int RowID { get; set; }

    }
}

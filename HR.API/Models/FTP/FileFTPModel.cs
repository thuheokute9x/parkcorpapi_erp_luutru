﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.FTP
{
    public class FileFTPModel
    {
        public string Patch { get; set; }
        public string Servername { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int PayDocListID { get; set; }
    }
}
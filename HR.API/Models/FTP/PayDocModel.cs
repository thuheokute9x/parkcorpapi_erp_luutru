﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.FTP
{
    public class PayDocModel
    {
        public bool CheckInsert { get; set; } = false;
        public string VendorCD { get; set; }
        public string PayType { get; set; }
        public string PayDocCD { get; set; }
        public string PayDocNo { get; set; }
        public int TotalRows { get; set; }
        public int RowID { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
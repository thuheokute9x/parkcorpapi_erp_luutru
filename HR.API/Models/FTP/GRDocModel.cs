﻿using System;

namespace HR.API.Models.ERP
{
    public class GRDocModel
    {
        public bool CheckInsert { get; set; } = false;
        public string SHIPPER { get; set; }
        public string VCD { get; set; }
        public string GRDocNo { get; set; }
        public string PayType { get; set; }
        public int TotalRows { get; set; }
        public int RowID { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

    }
}

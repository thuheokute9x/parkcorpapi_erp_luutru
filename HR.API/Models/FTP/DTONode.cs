﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace HR.API.Models.FTP
{
    public class DTONode
    {
        [Key]
        public string key { get; set; }
        public string title { get; set; }
        //public string expandedIcon { get; set; }
        //public string collapsedIcon { get; set; }
        public string type { get; set; }
        public bool? isPDF { get; set; }
        public string icon { get; set; }
        public bool? isLeaf {get; set; }
        public bool? Checked { get; set; }
        public bool? selected { get; set; }
        public bool? selectable { get; set; }
        public bool? disabled { get; set; }
        public bool? disableCheckbox { get; set; }
        public bool? expanded { get; set; }

        public List<DTONode> children { get; set; }
        //public int parentId { get; set; }
    }
}
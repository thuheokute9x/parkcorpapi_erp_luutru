﻿using System.Collections.Generic;

namespace HR.API.Models
{
    public class Result
    {
        public int ErrorCode { get; set; } = 0;
        public string ErrorMessage { get; set; }
        public string SheetName { get; set; }
    }
}
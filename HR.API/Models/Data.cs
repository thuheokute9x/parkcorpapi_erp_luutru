﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models
{
    public class Data
    {
        public dynamic Results { get; set; }
        public int Type { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.API.Models.Login
{
    public class LoginModel
    {
        public string username { get; set; }
        public string password { get; set; }
        public string Database { get; set; }
    }
}
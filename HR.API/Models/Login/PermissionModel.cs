﻿using System.Collections.Generic;

namespace HR.API.Controllers
{
    public class PermissionModel
    {
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public string FormName { get; set; }
        public bool FrmView { get; set; }
        public bool FrmInsert { get; set; }
        public bool FrmUpdate { get; set; }
        public bool FrmDelete { get; set; }
        public bool BtnEnable { get; set; }
        public bool IsFailure { get; set; }
    }
    public class UserLoginModel
    {
        public List<PermissionModel> PermissionList { get; set; } = new List<PermissionModel>();
        public string Token { get; set; }
    }
}

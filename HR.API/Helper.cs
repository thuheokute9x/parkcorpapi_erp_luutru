﻿using System;
using System.Globalization;
using System.IO;
using System.Web;

namespace HR.API
{
    public static class Helper
    {
        public static string PatchImage = "C:/LEVANTHU/HR_LNT/dist/assets/images/";
        public static string FormatDate(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            return DateTime.ParseExact(input, "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd");
        }
        public static DateTime? FormatDateddMMyyyy(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }

            return DateTime.ParseExact(input, "dd/MM/yyyy", CultureInfo.CurrentCulture);
        }

        public static string GetImageLink(string fileImageName, string idEmploy)
        {
            var pathFolder = HttpContext.Current.Server.MapPath("~/Images/" + idEmploy);

            if (!Directory.Exists(pathFolder))
            {
                Directory.CreateDirectory(pathFolder);
            }

            return HttpContext.Current.Server.MapPath("~/Images/" + idEmploy);
        }
    }
}
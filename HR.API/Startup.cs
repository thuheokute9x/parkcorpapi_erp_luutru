﻿using System.IO;
using System.Web.Http;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Owin.Security.OAuth;
using System;

[assembly: OwinStartup(typeof(HR.API.Startup))]

namespace HR.API
{
    // More information about https://github.com/drwatson1/AspNet-WebApi/wiki
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Use DotNetEnv v1.1.0 due to it is the only version with out dependencies
            var envFilePath = System.IO.Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, ".env");
            if (File.Exists(envFilePath))
            {
                DotNetEnv.Env.Load(envFilePath);
            }

            var corsOptions = CorsConfig.ConfigureCors(Settings.AllowedOrigins);
            app.UseCors(corsOptions);

            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            HttpConfiguration config = new HttpConfiguration();

            config.Filters.Add(new ExceptionFilter());

            AutofacConfig.Configure(config);

            FormatterConfig.Configure(config);
            RouteConfig.Configure(config);
            //LoggerConfig.Configure(config);
            OptionsMessageHandlerConfig.Configure(config);



            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "v1/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional });
            SwaggerConfig.Configure(config);

            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = "http://mysite.com", //some string, normally web url,  
                        ValidAudience = "http://mysite.com",
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("my_secret_key_12345"))
                    }
                });

            app.UseAutofacMiddleware(AutofacConfig.Container);
            app.UseAutofacWebApi(config);

            app.PreventResponseCaching();

            app.UseAuthentication();
            app.UseWebApi(config);
        }

    }
}
